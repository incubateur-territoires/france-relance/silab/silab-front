# :woman: :speech_balloon: User story

> **en tant que** ... **je peux** ... **afin de** ...

# :spy: Cas de test

> _exemple d'application de la user story_

# :heavy_check_mark: Definition Of Ready :

-   Product owner
    -   [ ] Titre le plus court possible et compréhensible
    -   [ ] User Story
    -   [ ] Scénario de test fonctionnel
    -   [ ] Priorité attribuée
    -   [ ] Epic attribuée
    -   [ ] Label `Affinage::Attente DEVs`
-   Developers
    -   [ ] Dépendances identifiées (blocked by, travail caché)
    -   [ ] Redécoupage si nécessaire (voir avec PO)
    -   [ ] Poids estimé
    -   [ ] Label `Affinage::Ready`

# :books: Ressources
