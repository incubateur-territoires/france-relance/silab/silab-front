# [SI]Lab

**[SI]Lab** est une application Web **Open Source** permettant aux agents des **collectivités territoriales** d'accéder aux données & fonctionnalités de leurs logiciels métiers en itinérance.

Initié en 2019 par **Grand Poitiers Communauté Urbaine**, le projet a été sélectionné en 2021 lors de la consultation nationale menée par l'Incubateur des Territoires dans le cadre du Plan de Relance. A ce titre, le projet a été gratifié d'un financement pour permettre son développement.

## Front

### Le fonctionnel

- [Le projet](front/doc/fonctionnel/projet.md)
- [Lexique](front/doc/fonctionnel/lexique.md)
- [Lexique spécifique Grand Poitiers Communauté Urbaine](front/doc/fonctionnel/lexique_gpcu.md)

#### Processus liés aux ODS de type GMAO

- Process d'une [demande](front/doc/fonctionnel/process_demande.md)
- Process d'une [intervention](front/doc/fonctionnel/process_intervention.md)

#### Schémas de requetes à l'API Silab-back

- [Scénarios d'appels à l'API](front/doc/fonctionnel/api_shema_request.md)

#### La sécurité

- [Contrôle d'accès](front/doc/fonctionnel/access_control.md)

### Le technique - _partie front_

[![pipeline status](https://gitlab.com/incubateur-territoires/france-relance/silab/silab-front/badges/main/pipeline.svg)](https://gitlab.com/incubateur-territoires/france-relance/silab/silab-front/-/commits/main)
[![Quality gate](https://sonarcloud.io/api/project_badges/measure?project=incubateur-territoires_silab-front&metric=alert_status)](https://sonarcloud.io/summary/overall?id=incubateur-territoires_silab-front)

#### Installation

- [Déploiement en production](front/doc/technique/deploy_production.md)

#### Design

- [Charte graphique](front/doc/technique/charte_graphique.md)
- [Prototypage](https://www.figma.com/proto/1b0J6taIJb5VZH6VpR3kaj/Silab-V2) (réalisé sous **Figma**)

#### API Silab-back

- [Model conceptuel](front/doc/technique/class_diagram_API.svg)
- [Spécification des routes](front/doc/technique/route_specification.md)

#### Développement

- [Installation de l'environnement de dev](front/doc/technique/installation.md)
- [Environnement](front/doc/technique/environnement.md)
- [Développement](front/doc/technique/developpement.md)
- [Flow de développement/déploiement pour les développeurs Silab](front/doc/technique/workflow.md)
- [Lexique GitLab](front/doc/technique/lexique_gitlab.md)
- [Scripts npm](front/doc/technique/scripts.md)
- [CI/CD](front/doc/technique/ci_cd.md)
- [Definition Of Done](front/doc/technique/DoD.md)

### Le technique - _partie back_

- [Coding standards](api/docs/coding_standards.md)
- [Ged Alfresco](front/doc/technique/ged_alfresco.md)

### L'équipe Scrum

| Scrum Master                 | Product Owners                                          | Developers                                                   |
| ---------------------------- | ------------------------------------------------------- | ------------------------------------------------------------ |
| @PierreFigueroaGrandPoitiers | &1 : @PierreFigueroaGrandPoitiers<br>&8 : @FredLeVan_GP | @yassir.bak<br>@hugo.daclon<br>@sjamain-gp<br>@Jules.Steiner |

:calendar: Sprint d'un mois avec déploiement continu

## Back [![pipeline status](https://gitlab.com/incubateur-territoires/france-relance/silab/silab-back/badges/main/pipeline.svg)](https://gitlab.com/incubateur-territoires/france-relance/silab/silab-back/-/commits/main) [![Coverage](https://sonarcloud.io/api/project_badges/measure?project=incubateur-territoires_silab-back&metric=coverage)](https://sonarcloud.io/summary/new_code?id=incubateur-territoires_silab-back) [![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=incubateur-territoires_silab-back)](https://sonarcloud.io/summary/new_code?id=incubateur-territoires_silab-back) [![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=incubateur-territoires_silab-back)

Dépôt pour la partie back de **[SI]lab** v2

### Sommaire

- [Installation de l'environnement de dev [⮫suivant]](api/docs/installation.md)
- [Environnement sans Docker](api/docs/environnement-sans-docker.md)
- [Développement](api/docs/developpement.md)
- [CI/CD](https://gitlab.com/incubateur-territoires/france-relance/silab/silab-back/-/ci/editor?branch_name=main&tab=2)
- [Déploiement en production](api/docs/environnements.md#production)
- [CONTRIBUTING](api/docs/CONTRIBUTING.md)
- [Configuration de silab](api/docs/configuration.md)
