<?php

$finder = (new PhpCsFixer\Finder())
    ->exclude('var')
    ->exclude('migrations')
    ->exclude('tests')
    ->in(__DIR__);

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        'php_unit_method_casing' => ['case' => 'snake_case'],
    ])
    ->setLineEnding("\n")
    ->setFinder($finder);
