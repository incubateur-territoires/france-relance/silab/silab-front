# SOURCE : Hugo Daclon
alias hs='history | grep'
alias apku='apk update'
alias apki='apk add'
alias apkui='apku && apki'
alias npmr='npm run'
# SOURCE : https://www.cyberciti.biz/tips/bash-aliases-mac-centos-linux-unix.html
## Colorize the ls output ##
alias ls='ls --color=auto'
## Use a long listing format ##
alias ll='ls -la'
## Show hidden files ##
alias l.='ls -d .* --color=auto'
## get rid of command not found ##
alias cd..='cd ..'
## a quick way to get out of current directory ##
alias ..='cd ..'
alias ...='cd ../../../'
alias ....='cd ../../../../'
alias .....='cd ../../../../'
alias .4='cd ../../../../'
alias .5='cd ../../../../..'
## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
# create parent dir by default
alias mkdir='mkdir -pv'
# handy short cuts #
alias h='history'
alias j='jobs -l'
alias path='echo -e ${PATH//:/\\n}'
alias now='date +"%T"'
alias nowtime=now
alias nowdate='date +"%d-%m-%Y"'
# Stop after sending count ECHO_REQUEST packets #
alias ping='ping -c 5'
# do not delete / or prompt if deleting more than 3 files at a time #
alias rm='rm -i'
# confirmation #
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'
## this one saved by butt so many times ##
alias wget='wget -c'
