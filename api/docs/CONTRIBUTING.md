Contribuer
==========

Conventions
-----------

L'uniformité du code est assuré par [`php-cs-fixer`](https://cs.symfony.com/).

A tout moment la commande suivante peut être lancé pour corriger le style du code :

```shell
docker compose exec php composer fix
```

Un certain nombre de règles sont également vérifié par `phpstan`, qui peut-être lancer à tout moment via :

```shell
docker compose exec php composer analyse
```

___En savoir plus sur ces règles :__ <https://phpstan.org/user-guide/rule-levels>_

> __Note :__  
> Les _hooks_ de `pre-commit` fonctionne grâce à `brainmaestro/composer-git-hooks`
> qui est actuellement en version experimentale pour __Symfony__ 6 :
> <https://github.com/BrainMaestro/composer-git-hooks/pull/124>
