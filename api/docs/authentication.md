Authentication
==============

AzureAD
-------

Pour valider un `access_token` ou un `id_token` __Microsoft__ utiliser : 
 - https://jwt.ms/ instead of https://jwt.io/.


### OpenIdConnect

La configuration __OpenId__ d'un __tenant__ en particulier est disponible à l'adresse suivante :
`https://login.microsoftonline.com/{tenant_id}/v2.0/.well-known/openid-configuration`

__Par exemple :__
https://login.microsoftonline.com/df424770-d48a-41aa-b4dd-733812cc7fab/v2.0/.well-known/openid-configuration

OAuth 2.0 *on-behalf-of flow*
-----------------------------
> https://learn.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-on-behalf-of-flow

À l'heure actuelle l'__app__ and l'__API__ __SILAB__ utilise le même `clientId` 
et sont donc considérés comme une seule et même application au sein d'__AzureAD__

Ces 2 applications devraient être distinguées pour mettre en place la sécurisation de l'API.
> Soit 2 __Application id__ (ou *client id*) distinct.

- un pour __SILAB App__
  - ayant dans ses *scopes* l'*application id* de __SILAB API__ et le droit de s'authentifier
- et un autre pour __SILAB API__
  - ayant dans ses *scopes* uniquement l'accès aux API dont elle a besoin, par exemple : 
    - *Lire les informations de profil d'un utilisateur sur __Microsoft Graph__.*

> Si __SILAB App__ demande accès au *scope* __SILAB API__ alors un *token* en version `2.0` sera délivré.
> Ce qui permettra à __SILAB API__ de le valider seul.

### Flow

![](https://raw.githubusercontent.com/MicrosoftDocs/azure-docs/main/articles/active-directory/azuread-dev/media/v1-oauth2-on-behalf-of-flow/active-directory-protocols-oauth-on-behalf-of-flow.png)

### Pitfalls

> __Note :__ 
> ```
> RuntimeException: The client_id / audience is invalid!
> ```
> L'erreur ci-dessus, provient de la librairie `thenetworg/oauth2-azure`.
> arrive lorsqu'un `accessToken` est utilisé sur l'__SILAB API__ alors qu'il devrait uniquement être utilisé 
> pour l'*audience* présente dans les `claims` du token.
> 
> __Exemple :__ l'audience `00000003-0000-0000-c000-000000000000` représente le droit de requêtter
> l'API __Microsoft Graph__.

Si la validation de la signature d'un __access token__ échoue...

> Microsoft-developed APIs like Microsoft Graph or APIs in Azure have other proprietary token formats. These proprietary formats might be encrypted tokens, JWTs, or special JWT-like tokens that won't validate.

> Clients must treat access tokens as opaque strings because the contents of the token are intended for the API only. For validation and debugging purposes only, developers can decode JWTs using a site like jwt.ms. Tokens that are received for a Microsoft API might not always be a JWT and can't always be decoded.
> https://learn.microsoft.com/en-us/azure/active-directory/develop/access-tokens

Vérifier dans les `claims` que vous n'essayez pas de valider un *token* en version `1.0`.
La signature d'un *token* en version `1.0` __ne peut pas être validée__.
Un *token* en version `1.0` peut uniquement être utilisé pour contacter les services __Microsoft__ et ne doit pas être utilisé pour sécuriser une API.
Pour sécuriser une API un *token* version `1.0` devrait être utilisé.

```
Invalid token issuer (tokenClaims[iss]https://sts.windows.net/df424770-d48a-41aa-b4dd-733812cc7fab/, tenant[issuer] https://login.microsoftonline.com/df424770-d48a-41aa-b4dd-733812cc7fab/v2.0)
```
> Voir https://github.com/AzureAD/microsoft-authentication-library-for-js/issues/560#issuecomment-466583013
> Cette erreur signifie probablement que la version du jeton n'est pas celle attendue.
> Les jetons `1.0` sont fournient par https://sts.windows.net/ tandis que les jetons `2.0` sont fournis par https://login.microsoftonline.com/.

### Resources
- __ROPC__ https://learn.microsoft.com/en-us/azure/active-directory/develop/v2-oauth-ropc
- https://www.voitanos.io/blog/validating-azure-ad-generated-oauth-tokens/
- Collection de requêtes d'exemple pour __Postman__ : https://app.getpostman.com/run-collection/f77994d794bab767596d
