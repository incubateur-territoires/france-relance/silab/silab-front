# Coding standards

[[_TOC_]]

## SQL

example regroupant les pratiques

```SQL
with
    ACTOR_EMAILS as (
        select PHONE.ACTOR_ID, JSON_ARRAYAGG(PHONE.PHONENUM returning clob) as EMAILS from "$dbSchema".CSSY_PHONE PHONE where PHONE.TYPE=''EMAIL'' group by PHONE.ACTOR_ID
    )
    ,
    ACTOR_PHONES as (
        select PHONE.ACTOR_ID, JSON_ARRAYAGG(PHONE.PHONENUM returning clob) as PHONENUMBERS from "$dbSchema".CSSY_PHONE PHONE where PHONE.TYPE NOT IN (''EMAIL'',''FAX'') group by PHONE.ACTOR_ID
    )
select
    ACTOR.ID as DB_CARL_GMAOACTEUR_ID,
    ACTOR.FULLNAME as DB_CARL_GMAOACTEUR_FULLNAME,
    coalesce(ACTOR_EMAILS.EMAILS, to_clob(''[]'')) as DB_CARL_GMAOACTEUR_EMAILS,
    coalesce(ACTOR_PHONES.PHONENUMBERS, to_clob(''[]'')) as DB_CARL_GMAOACTEUR_PHONENUMBERS
from "$dbSchema".CSSY_ACTOR ACTOR
    left join ACTOR_EMAILS on ACTOR_EMAILS.ACTOR_ID = ACTOR.ID
    left join ACTOR_PHONES on ACTOR_PHONES.ACTOR_ID = ACTOR.ID
```

### renommage des tables requêtées

On supprime le préfixe de la table (ex: ~~CSSY_~~ ACTOR).  
On conserve le nom de l'entité tel que définit sur la base (ex: `CSSY_ACTOR`->`ACTOR`), ou le nommage silab correspondant s'il existe (ex: `CSWO_WO`->`INTERVENTION`).

### with plutot que subquery

### nommage des tables with

Les table with étant généralement des table de jointure, on nomme avec les deux entité concernées en mettant l'entité "principale" en premier `ENTITEPRINCIPALE_ENTITERELATION`, ex : `ACTOR_EMAILS`

### Join plutot que where

### casse

Mots clefs en minuscule, ex : `from`, `left join`

Nom des objets en majuscule, ex : `TABLE.COLONNE as "NOM_RESERVE"`

*Note, cela est grandement dû a la couche de compatibilité postgres<->oracle, ou la casse des objet doit être absoluement identique, de fait, en majuscule*