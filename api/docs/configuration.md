# Configuration de Silab

## Connecteur Carl

:warning: Le nombre maximal de résultats renvoyés par Carl est régit par le paramètre général `ApiMaxResults`: Système>Paramètres>Configuration-du-système>Général. Ce paramètre contraint le nombre de résultats renvoyés par silab.  
La valeur de `ApiMaxResults` doit donc être égale ou supérieur à la limite de résultats fixée dans Silab pour le connecteur Carl.
