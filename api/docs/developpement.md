# Développement

**[SI]lab** _backend_ est basé sur un _starter_ [api-platform](https://api-platform.com/).

## Données

Vous pouvez ajouter un `*.sql` dans `api/docker/database/docker-entrypoint-initdb.d/` 
afin qu'il soit chargé automatiquement si la BDD n'existe pas encore.
> (voir https://hub.docker.com/_/postgres)

## Environnement

> Voir [Environnement](./environnement.md)

## Structure du projet

```shell
/srv/api
├── .devcontainer/  # dossier contenant la configuration du dev Container
├── .vscode/        # dossier contenant la configuration du workspace et les extensions recommandées
├── bin/            # dossier contenant les binaires utilitaires php
├── config/         # dossier contenant la configuration symfony
├── docker/         # fichiers nécessaires au container
│   ├── devcontainer    # dossier contenant les fichiers nécéssaires au service php en environnement de dev
│   ├── php/            # dossier contenant les fichiers nécéssaires au service php
│   └── webserver/      # dossier contenant les fichiers nécéssaires au service webserver
├── docs/           # documentation
├── migrations/     # dossier contenant les scripts de migration de la DB
├── public/         # resources statiques du projet (non versionnée et générée par les auto-scripts composer)
├── scripts/        # scripts utilisé pour le CICD du projet
├── src/            # sources du projet (architecture de symfony)
├── templates/      # templates twig pour des interfaces dans le back (architecture de symfony)
├── tests/          # test PHPUnit
├── var/            # dossier contenant le cache et les logs symfony
└── vendor/         # ressources des packages composer
```

## Développement des API métier

Les appels vers les API métier sont écrites et testées via thunderClient.

> Voir [thunderclient](./thunderclient.md)
