# Environnement api sans docker pour dev

[⮪sommaire](../README.md)

## Installer les technologies en local 🚧 process en cours de définition

-   avec scoop (https://scoop.sh/)

```bash
#Installer scoop
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression

#Installer php 8.1 (Voir dans le docker pour avoir la version actuelle du projet)
scoop bucket add versions
scoop install versions/php81

#Installer composer
scoop install main/composer

#Installer symfony cli (serveur)
scoop install symfony-cli
```

-   sans scoop
    -   Installer php (https://www.php.net/downloads.php)
    -   Installer composer (https://getcomposer.org/download/)
    -   Installer symfony cli (https://symfony.com/download) (Section Binaries)

## Modifier php.ini

-   avec scoop

> Aller à cette endroit : C:\Users\prenom.nom\scoop\apps\php81\current\cli\php.ini
> Ouvrir le fichier avec vscode et modifier ces éléments :

```ini
memory_limit = 256M #(ligne 435)

#...

#Décommenter ces éléments (enlever ;)
#Attention il manque Imagik et oracle
extension=openssl
extension=pdo_pgsql
extension=pgsql
extension=intl

#Voir la section configurer xdebug
#Ajouter cette ligne
zend_extension=xdebug
```

## Configurer xdebug avec scoop

> Installer xdebug depuis https://xdebug.org/download#releases (voir la version de php du projet)
> Renomer le fichier 'php_xdebug.dll' et l'ajouter ce ficher dans /scoop/apps/php81/current/ext

> Ajouter cette section dans Module Settings du php.ini

```ini
[XDebug]
xdebug.mode = debug
xdebug.start_with_request = yes
xdebug.remote_port=9003
xdebug.remote_enable = 1
xdebug.remote_autostart = 1
```

> Dans vscode installer PHP Debug
> Ouvrir l'onglet Run and Debug (triangle avec un insecte)
> Ouvir api/.vscode/lauch.json

```json
//Supprimer ça
"pathMappings": {
				"/srv/api": "${workspaceFolder}"
			}
```

## Ajouter l'extension ACPU

Aller sur le site https://www.48design.com/en/news/2023/10/06/php_apcu-dll-5.1.22-windows-binaries-php-8.2/#download
Et prendre la version de php TS x 64 (tester les autres si ça marche pas)
Ajouter ce fichier dans les extensions php dossierphp/ext

Puis ajouter l'extension dans le fichier php.ini

```
extension=apcu
```

## Ajouter la configuration en local

> Dans le répertoire silab-front/api/
> Créer un fichier docker-compose.local.yml et y ajouter :

```yml
#Si vous aviez déjà créé une BDD avec le script docker-compose.yml, les données seront gardées

version: "3.8"

services:
    database:
        build:
            context: .
            dockerfile: Dockerfile-db-local
            args:
                POSTGRES_VERSION: ${POSTGRES_VERSION:-14}
        environment:
            POSTGRES_DB: app
            POSTGRES_PASSWORD: ChangeMe
            POSTGRES_USER: app
        volumes:
            - db-data:/var/lib/postgresql/data
        ports:
            - "5432:5432"

volumes:
    db-data:
```

Dupliquer le fichier Dockerfile-db et le renommner Dockerfile-db-local

Puis le modifier ainsi :

```
EXPOSE 5432:5432
```

> Faire cette commande pour ajouter/modifier la BDD

```bash
docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d
```

> Faire ces commandes pour trouver l'adresse ip de la BDD

```bash
#Vérifier que le conteneur de la BDD est up et trouver son nom
docker ps
```

> Ajouter cette ligne en haut du fichier env.local

```env
#Mettre l'adresse ip de la BDD
DATABASE_URL="postgresql://app:ChangeMe@localhost:5432/app?serverVersion=14&charset=utf8"
```

## Lancer le serveur

> Ouvrir le terminal de commande (attention avec scoop il faut ouvrir le terminal windows dans le menu démarer)

```bash
#Aller dans le répertoire silab-front\api

#Installer les dépendances
composer install

#Faire les migrations
symfony console d:m:m

#Lancer le serveur
export POSTGRES_USER=app
symfony server:start  --port=5001
```

> Penser à mettre l'url dans le front en http:// (pas de s)
