# Environnements docker

[⮪sommaire](../README.md)

Il existe aujourd'hui 2 environnements de déploiement Silab avec Docker.

## Développement

note:  
> Si vous utilisez l'IDE [VS Code],
> je vous recommande d'utiliser [la fonctionalité dev-container](/docs/dev-container.md).

### Contenu

- 2 conteneurs:
  - `php` contient un bind-mount du code source, quelques packages utilitaires et permet de faire tourner le projet avec le server symfony avec un HRM (hot-reload-module)
  - `database` contient la DB PostgreSQL utilisée par le projet
- 1 volume :
  - db-data : permet de perssister la DB.

### Déploiement

```bash
docker compose up -d

# il est possible de configurer plusieurs variables d'environnement :
XDEBUG_IDEKEY # permet de configurer l'IDEKEY que XDEBUG utilisera (obligatoire si vous voulez utiliser le step-debugger)
BASE_PATH # permet de changer le chemin racine utilisé pour la résolution des URLs (défaut = /)
API_PORT # permet de changer le port de l'hôte sur lequel l'api sera disponnible (défaut = 5001)
POSTGRES_USER # le username de la DB (défaut = app)
POSTGRES_PASSWORD # le password du user (défaut = ChangeMe)
POSTGRES_VERSION # la version PostgreSQL (défaut = 14)
POSTGRES_DB # le nom de la DB (défaut = app)
```

> Concernant la variable XDEBUG_IDEKEY, les valeurs dépendent de l'IDE et sont parfois configurable (sur VSCODE par exemple). Généralement `PHPSTORM`,  `XDEBUG_ECLIPSE`, `netbean` ou `macgdbp` sont utilisés par les IDEs du même nom.

## Production

Cet environnement est utilisé pour un déploiement de l'api à l'exterieur.

### TL;DR

> Il contient 3 conteneurs et 4 volumes.
>
> Pour déployer il faut :
>
> 1. configurer les variables d'environnement docker compose
> 2. construire les images
> 3. construire les conteneurs

### Contenu

- 3 conteneurs :
  - php : Basé sur php-fpm, il contient tout le projet Silab-back
  - webserver : Basé sur l'image [abdenour/nginx-distroless-unprivileged][nginx], il s'occupe de servir le projet.
  - database : la base de donnée Postgres
- 4 volumes :
  - `webserver-logs` et `php-logs` qui sont les fichiers de logs des 2 conteneurs
  - `php-fpm-socket`: le socket unix qui permet la communication entre les 2 conteneurs.
  - `db-data`: permet de persister la base de Donnée Postgres.

### Déploiement

1. Configurer les variables d'environnements nécéssaires à docker compose pour la création des conteneurs. Elles sont listée dans le `docker-compose.env.template`
2. Construire les images
    1. *Et les pousser sur le registry si voulu.*
3. Créer et démarrer les conteneurs.

#### Configurer les variables d'environnement

- Soit créer fichier docker-compose.env (ignoré par git) à partir du template et ajouter le paramètre `--envfile docker-compose.env` aux commandes docker compose.
- Soit les ajouters au terminal avant de lancer la commande, avec la commande `export` sur linux ou la commande `SET` sur windows.

:warning: Vous aurez surement besoin de vous connecter au registry pour tirer et pousser les images

#### Commandes docker

:information_source: : si vous n'avez pas utilisé de fichier pour les variables d'environnement vous pouvez,
sur toutes les commandes, supprimer la ligne contenant l'argument `--env-file`

##### Construire les images

```shell
docker compose \
  --env-file docker-compose.env \
  -f docker-compose.yml \
  -f docker-compose.prod.yml \
  -p silab-back-production \
  build --pull
```

##### Pousser les images

```shell
docker compose \
  --env-file docker-compose.env \
  -f docker-compose.yml \
  -f docker-compose.prod.yml \
  -p silab-back-production \
  push
```

##### Tirer les images

**cas d'usage :** l'image est déjà sur le registry et vous ne voullez pas la reconstruire

```shell
docker compose \
  --env-file docker-compose.env \
  -f docker-compose.yml \
  -f docker-compose.prod.yml \
  -p silab-back-production \
  pull
```

##### Construire les conteneurs et les démarrer

```shell
docker compose \
  --env-file docker-compose.env \
  -f docker-compose.yml \
  -f docker-compose.prod.yml \
  -p silab-back-production \
  up -d \
  --no-build \
  --force-recreate \
  --remove-orphans

```

[nginx]: https://hub.docker.com/r/abdennour/nginx-distroless-unprivileged
[VS Code]: https://code.visualstudio.com/
