# Installation de l'environnement de développement

[⮪sommaire](../README.md)

## TL;DR

2 possibilitées :

1. VSCODE
   1. Cloner le projet (ssh de préférence)
   2. Ouvrir le dossier du projet
   3. Cliquer sur le boutton `reopen in container` de la notification qui apparait.
2. Autres > voir [#Développement](environnements.md#Developpement).

## environnement docker compose

Bien que ce projet ai une installation simplifié avec vscode vous pouvez utiliser
d'autre IDE avec l'environnement docker compose.

> voir [#Développement](environnements.md#Developpement).

## environnement VScode

### Prérequis

#### Windows

- vscode
- [L'agent openSSH ainsi qu'une paire de clefs](installer_openssh.md) (dont la publique est installé sur git)
- [Docker Desktop](https://docs.docker.com/desktop/windows/install/)
- [Git](https://git-scm.com/download/win)

#### Linux

- vscode
- L'agent `openSSH` ainsi qu'une paire de clefs (dont la publique est installé sur git)
- [Docker ou Docker Desktop](https://docs.docker.com/engine/install/)
- [Git](https://git-scm.com/download/linux)

### Installation

Pour commencer cloner le projet (vous pouvez le faire depuis VS Code sans problème).

Une fois le git clone effectué, ouvrez, le dossier contenant le projet dans VS Code,
une notification devrait apparaître vous disant de ré-ouvrir le dossier dans le devContainer, faites-le.
L'environnement de développement et désormais en train de se créer.

### Extensions recommandées

Une fois l'environnement créé, une notification devrait apparaître vous proposant
d'installer les extensions recommandées. La liste de ces extensions se trouve dans
`.vscode\extensions.json` et sont particulièrement adaptées au projet
(les extensions considérées nécessaires sont installées automatiquement).

### Lancement

Plusieurs choses se sont faites automatiquement lors de la l'installation.

1. Un environnement basé sur l'image [node](https://hub.docker.com/_/node) à été créé.  
   _[Voir la liste des packets utilitaires installés](utils.md)_
2. Les [dépendances de sonarlint](https://marketplace.visualstudio.com/items?itemName=SonarSource.sonarlint-vscode#requirements)
   on été installées.
3. Les extensions nécessaires ont été installées.
4. Les paramètres vscode obligatoires ont été configurés.
5. Les dépendances nodes du projet ont été installées.
6. Le server web symfony s'est lancé via une task et silab-back est accessible sur <http://localhost:5001>

### Utilisation

Vous pouvez maintenant travailler complètement à l’intérieur du container,
sans avoir par exemple à installer node sur votre machine.  
Vous pouvez créer un fichier `.vscode\settings.json` ou `.vscode\*.code-workspace`
pour rajouter une configuration personnalisée, ces fichiers ne seront pas commit.

**_⚠ Attention :_** _Si vous modifier le container, les fichiers dans `docker/`
ou le `.devcontainer/devcontainer.json` vous devrez reconstruire le dev Container :  
Ouvrez la palette de commande (<kbd>F1</kbd> ou <kbd>Ctrl</kbd>+<kbd>⇧ Shift</kbd>+<kbd>P</kbd>)
puis lancez la commande `Remote-Containers: Rebuild Container`._
