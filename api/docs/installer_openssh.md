# Installer l'agent openssh et ajouter sa clef ssh

[⮪retour](installation.md)

## Installation

- Si vous avez installé le git Bash, OpenSSH devrait être installé.
    Vous pouvez tester avec la commande suivante :

```powershell
get-service -DisplayName *OpenSSH*
```

Si vous avez une erreur, suivez les sections _Install ..._ et
_Start and configure OpenSSH Server_ de la [doc officielle microsoft](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse)

## Lancement automatique de l'agent

L'agent SSH doit est nécessaire pour pouvoir interagir avec git en ssh depuis le devContainer.
Donc, pour éviter de le lancer à chaque fois on peut configurer le service pour qu'il soit démarré automatiquement.

Depuis un powershell **administrateur**, lancez la commande suivante :

```powershell
Set-Service -Name ssh-agent -StartupType Automatic
```

Vous pouvez désormais démarrer le service si celui-ci n'est pas déjà lancé avec `Start-Service ssh-agent`

Enfin, ajoutez votre clef ssh, ex : `ssh-add $HOME/.ssh/id_rsa`
