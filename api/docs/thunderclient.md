# Utilisation de thunderClient

## première utilisation

Copiez le fichier `thunder-tests/thunderClient.env.local.template` vers `thunder-tests/thunderClient.env.local` et remplacez les placeholders.

## Variables d'environnement

Les variables stockées dans `thunder-tests/thunderClient.env.local` ne sont pas versionnées, vous y mettrez les secrets et les éléments spécifiques à votre instance de carl.

Les variables temporaires sont stockées par les scripts dans l'environnement **Local Env**, qui n'est pas versionnné, ex : `CARL_API_ACCESS_TOKEN`.

Pour le reste, les variables d'environnement partagées et versionnées sont à mettre dans l'environnement **Global Env**.

## arborescence

L'arborescence des requêtes suit la hiérarchie des epics/user stories.

A la racine se trouve les requêtes transverses, ex: l'authentification.
