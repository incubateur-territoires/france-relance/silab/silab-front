<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221115134103 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE greeting_id_seq CASCADE');
        $this->addSql('CREATE TABLE intervention_service_offer (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE intervention_service_offer ADD CONSTRAINT FK_9EA0E615BF396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE greeting');
        $this->addSql('ALTER TABLE service_offer RENAME COLUMN discr TO type');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE greeting_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE greeting (id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE intervention_service_offer DROP CONSTRAINT FK_9EA0E615BF396750');
        $this->addSql('DROP TABLE intervention_service_offer');
        $this->addSql('ALTER TABLE service_offer RENAME COLUMN type TO discr');
    }
}
