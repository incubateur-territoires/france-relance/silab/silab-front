<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221121175836 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE link_service_offer DROP link');
        $this->addSql('ALTER TABLE link_service_offer DROP is_target_blank');
        $this->addSql('ALTER TABLE service_offer ADD link VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE service_offer DROP link');
        $this->addSql('ALTER TABLE link_service_offer ADD link VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE link_service_offer ADD is_target_blank BOOLEAN NOT NULL');
    }
}
