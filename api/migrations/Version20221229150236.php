<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221229150236 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE service_offer ADD slug VARCHAR(255)');
        // On doit remplir les champs avec quelque chose de non null et d'unique
        // avant d'appliquer les contraintes
        // on créé des slugs à partir des titres
        $serviceOffers = $this->connection->fetchAllAssociative('SELECT id, title FROM service_offer');
        $uniqueValue = 0;
        foreach ($serviceOffers as $serviceOffer) {
            $serviceOfferSlug = [
                'id' => $serviceOffer['id'],
                'slug' => urlencode($serviceOffer['title']).$uniqueValue,
            ];
            $this->addSql('UPDATE service_offer SET slug = :slug WHERE id = :id', $serviceOfferSlug);
            ++$uniqueValue;
        }
        $this->addSql('ALTER TABLE service_offer ALTER slug SET NOT NULL');
        $this->addSql('ALTER TABLE service_offer ALTER link SET NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX slug ON service_offer (slug)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX slug');
        $this->addSql('ALTER TABLE service_offer DROP slug');
        $this->addSql('ALTER TABLE service_offer ALTER link DROP NOT NULL');
    }
}
