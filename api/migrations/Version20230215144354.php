<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230215144354 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE intervention (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE logistic (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE redirect (id INT NOT NULL, redirect_url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT FK_D11814ABBF396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE logistic ADD CONSTRAINT FK_54BD53BDBF396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE redirect ADD CONSTRAINT FK_C30C9E2BBF396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention_service_offer DROP CONSTRAINT fk_9ea0e615bf396750');
        $this->addSql('DROP TABLE intervention_service_offer');
        $this->addSql('ALTER TABLE service_offer RENAME COLUMN type TO template');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE intervention_service_offer (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE intervention_service_offer ADD CONSTRAINT fk_9ea0e615bf396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention DROP CONSTRAINT FK_D11814ABBF396750');
        $this->addSql('ALTER TABLE logistic DROP CONSTRAINT FK_54BD53BDBF396750');
        $this->addSql('ALTER TABLE redirect DROP CONSTRAINT FK_C30C9E2BBF396750');
        $this->addSql('DROP TABLE intervention');
        $this->addSql('DROP TABLE logistic');
        $this->addSql('DROP TABLE redirect');
        $this->addSql('ALTER TABLE service_offer RENAME COLUMN template TO type');
    }
}
