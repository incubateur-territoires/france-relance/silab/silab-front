<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230307084744 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'bascules les anciennes offres de services vers le template redirect';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE service_offer set template = \'redirect\' where template not in (\'logistic\', \'redirect\', \'intervention\')');
    }

    public function down(Schema $schema): void
    {
        // pas de down car cela ne mettrai pas la bdd dans un état correct dans tous les cas, cette migration arrive trop tard
    }
}
