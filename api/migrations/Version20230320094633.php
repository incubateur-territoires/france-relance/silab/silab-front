<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230320094633 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE carl_configuration_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE gmao_configuration_intervention_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE carl_configuration (id INT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE carl_configuration_intervention (id INT NOT NULL, carl_configuration_id INT NOT NULL, cost_center JSON DEFAULT NULL, directions JSON DEFAULT NULL, action_type JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CB236D31F7C55B56 ON carl_configuration_intervention (carl_configuration_id)');
        $this->addSql('CREATE TABLE gmao_configuration_intervention (id INT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD CONSTRAINT FK_CB236D31F7C55B56 FOREIGN KEY (carl_configuration_id) REFERENCES carl_configuration (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD CONSTRAINT FK_CB236D31BF396750 FOREIGN KEY (id) REFERENCES gmao_configuration_intervention (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention_service_offer ADD gmao_configuration_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE intervention_service_offer ADD CONSTRAINT FK_9EA0E6156C9802C7 FOREIGN KEY (gmao_configuration_id) REFERENCES gmao_configuration_intervention (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9EA0E6156C9802C7 ON intervention_service_offer (gmao_configuration_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention_service_offer DROP CONSTRAINT FK_9EA0E6156C9802C7');
        $this->addSql('DROP SEQUENCE carl_configuration_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE gmao_configuration_intervention_id_seq CASCADE');
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP CONSTRAINT FK_CB236D31F7C55B56');
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP CONSTRAINT FK_CB236D31BF396750');
        $this->addSql('DROP TABLE carl_configuration');
        $this->addSql('DROP TABLE carl_configuration_intervention');
        $this->addSql('DROP TABLE gmao_configuration_intervention');
        $this->addSql('DROP INDEX IDX_9EA0E6156C9802C7');
        $this->addSql('ALTER TABLE intervention_service_offer DROP gmao_configuration_id');
    }
}
