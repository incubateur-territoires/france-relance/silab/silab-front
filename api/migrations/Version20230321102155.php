<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230321102155 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE gmao_configuration_intervention ADD title VARCHAR(255)');
        $gmaoConfigurationInterventions = $this->connection->fetchAllAssociative('SELECT id FROM gmao_configuration_intervention');
        foreach ($gmaoConfigurationInterventions as $gmaoConfigurationIntervention) {
            $gmaoConfigurationInterventionDatas = [
                'id' => $gmaoConfigurationIntervention['id'],
                'title' => 'changeMe',
            ];
            $this->addSql('UPDATE gmao_configuration_intervention SET title = :title WHERE id = :id', $gmaoConfigurationInterventionDatas);
        }

        $this->addSql('ALTER TABLE gmao_configuration_intervention ALTER title SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE gmao_configuration_intervention DROP title');
    }
}
