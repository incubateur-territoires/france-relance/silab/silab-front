<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230322151427 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention RENAME COLUMN cost_center TO cost_centers');
        $this->addSql('ALTER TABLE carl_configuration_intervention RENAME COLUMN action_type TO action_types');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention RENAME COLUMN cost_centers TO cost_center');
        $this->addSql('ALTER TABLE carl_configuration_intervention RENAME COLUMN action_types TO action_type');
    }
}
