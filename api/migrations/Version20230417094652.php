<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230417094652 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // ATTENTION, cette migration perd les précédents réglages sur les actiontypes
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD action_types_map JSON');
        $this->addSql("UPDATE carl_configuration_intervention SET action_types_map = '{}'");
        $this->addSql('ALTER TABLE carl_configuration_intervention ALTER action_types_map SET NOT NULL');

        $this->addSql('ALTER TABLE carl_configuration_intervention DROP action_types');

        $this->addSql("UPDATE logistic_service_offer SET warehouse_id = 'merci de préciser un id de magasin' WHERE warehouse_id IS NULL");
        $this->addSql('ALTER TABLE logistic_service_offer ALTER warehouse_id SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD action_types JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP action_types_map');
        $this->addSql('ALTER TABLE logistic_service_offer ALTER warehouse_id DROP NOT NULL');
    }
}
