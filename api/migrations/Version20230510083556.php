<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230510083556 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'ajout du champ correspondant à $photoAvantInterventionDoctypeId dans CarlConfigurationIntervention';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD photo_avant_intervention_doctype_id VARCHAR(255)');
        $this->addSql("UPDATE carl_configuration_intervention SET photo_avant_intervention_doctype_id = 'IMG'");
        $this->addSql('ALTER TABLE carl_configuration_intervention ALTER photo_avant_intervention_doctype_id SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP photo_avant_intervention_doctype_id');
    }
}
