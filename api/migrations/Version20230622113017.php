<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230622113017 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "Retire 'api/' à la fin du base_url des client carl. ATTENTION le down rajoute suffixe automatiquement l'url par 'api/'.";
    }

    public function up(Schema $schema): void
    {
        $carlClients = $this->connection->fetchAllAssociative('SELECT id, base_url FROM carl_configuration');
        foreach ($carlClients as $carlClient) {
            // Si le base url se termine par 'api/' alors retire cette partie
            $newBaseUrl = str_ends_with($carlClient['base_url'], 'api/')
                ? substr($carlClient['base_url'], 0, strlen($carlClient['base_url']) - 4)
                : $carlClient['base_url'];

            $this->addSql(
                'UPDATE carl_configuration SET base_url = :base_url WHERE id = :id',
                [
                    'id' => $carlClient['id'],
                    'base_url' => $newBaseUrl,
                ]);
        }
    }

    public function down(Schema $schema): void
    {
        $carlClients = $this->connection->fetchAllAssociative('SELECT id, base_url FROM carl_configuration');
        foreach ($carlClients as $carlClient) {
            $this->addSql(
                'UPDATE carl_configuration SET base_url = :base_url WHERE id = :id',
                [
                    'id' => $carlClient['id'],
                    'base_url' => $carlClient['base_url'].'api/',
                ]);
        }
    }
}
