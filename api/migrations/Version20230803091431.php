<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230803091431 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Retrait du slug qui est inutile';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX slug');
        $this->addSql('ALTER TABLE service_offer DROP slug');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE service_offer ADD slug VARCHAR(255) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX slug ON service_offer (slug)');
    }
}
