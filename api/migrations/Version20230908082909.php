<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230908082909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE service_offer SET link=\'liste-equipements\' WHERE link=\'liste_equipements\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE service_offer SET link=\'liste_equipements\' WHERE link=\'liste-equipements\'');
    }
}
