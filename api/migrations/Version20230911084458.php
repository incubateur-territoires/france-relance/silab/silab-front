<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230911084458 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD include_equipements_children_with_type JSON DEFAULT \'["building", "zone"]\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP include_equipements_children_with_type');
    }
}
