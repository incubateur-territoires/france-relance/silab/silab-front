<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230919092709 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Création de l\'ODS Elus';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE elus_service_offer (id INT NOT NULL, configuration JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE elus_service_offer ADD CONSTRAINT FK_A81E9FF2BF396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE elus_service_offer DROP CONSTRAINT FK_A81E9FF2BF396750');
        $this->addSql('DROP TABLE elus_service_offer');
    }
}
