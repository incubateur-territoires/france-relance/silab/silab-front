<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231019100028 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Remet le bon nom pour la table de la classe CarlClient';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration RENAME TO carl_client');
        $this->addSql('ALTER SEQUENCE carl_configuration_id_seq RENAME TO carl_client_id_seq');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_client RENAME TO carl_configuration');
        $this->addSql('ALTER SEQUENCE carl_client_id_seq RENAME TO carl_configuration_id_seq');
    }
}
