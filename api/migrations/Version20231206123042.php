<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231206123042 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD photo_apres_intervention_doctype_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE carl_configuration_intervention ALTER photo_avant_intervention_doctype_id SET DEFAULT \'IMG\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP photo_apres_intervention_doctype_id');
        $this->addSql('ALTER TABLE carl_configuration_intervention ALTER photo_avant_intervention_doctype_id DROP DEFAULT');
    }
}
