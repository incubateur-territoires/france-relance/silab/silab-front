<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231213103853 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Spécifique Poitiers, ajout des valeur actuelles des "id de photo après" pour les Carl gmao configuration intervention, pour faciliter le déploiement';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("update carl_configuration_intervention set photo_apres_intervention_doctype_id='18c3c534dcc-8e0' where carl_client_id=3"); // carl INTEG
        $this->addSql("update carl_configuration_intervention set photo_apres_intervention_doctype_id='18c607cdf90-ea5' where carl_client_id=2"); // carl TEST
        $this->addSql("update carl_configuration_intervention set photo_apres_intervention_doctype_id='18c3c38653e-5a47' where carl_client_id=1"); // carl PROD
    }

    public function down(Schema $schema): void
    {
    }
}
