<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240102141053 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Spécifique Poitiers, MAJ des valeur actuelles des "id de photo après" pour les Carl gmao configuration intervention, pour faciliter le déploiement';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("update carl_configuration_intervention set photo_apres_intervention_doctype_id='18c3c38653e-5a47' where carl_client_id=3"); // carl INTEG
    }

    public function down(Schema $schema): void
    {
    }
}
