<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240103081825 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE message ADD start_date DATE NOT NULL');
        $this->addSql('ALTER TABLE message ADD end_date DATE NOT NULL');
        $this->addSql('ALTER TABLE message DROP starting_date');
        $this->addSql('ALTER TABLE message DROP ending_date');
        $this->addSql('ALTER TABLE message RENAME COLUMN message_body TO body');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE message ADD starting_date DATE NOT NULL');
        $this->addSql('ALTER TABLE message ADD ending_date DATE NOT NULL');
        $this->addSql('ALTER TABLE message DROP start_date');
        $this->addSql('ALTER TABLE message DROP end_date');
        $this->addSql('ALTER TABLE message RENAME COLUMN body TO message_body');
    }
}
