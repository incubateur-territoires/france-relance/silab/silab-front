<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use App\Shared\Security\Role\ScopedRoleHierarchy;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240104134943 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Cette migration nettoie chez les user tous les roles qui appartiennent à des offres de services qui n\'existe plus';
    }

    public function up(Schema $schema): void
    {
        $serviceOffersIds = $this->connection->fetchFirstColumn('SELECT id FROM service_offer');
        $users = $this->connection->fetchAllAssociative('SELECT id, roles FROM public.user');
        foreach ($users as $user) {
            $userRoles = json_decode($user['roles'], true);
            $existingServiceOfferRoles = [];
            foreach ($serviceOffersIds as $serviceOfferId) {
                $existingServiceOfferRoles = array_merge(
                    $existingServiceOfferRoles,
                    array_filter($userRoles,
                        function ($userRole) use ($serviceOfferId) {
                            return preg_match(ServiceOffer::getRoleRegexpByServiceOfferId($serviceOfferId), $userRole);
                        }
                    )
                );
            }
            $allServiceOfferRoles = array_filter($userRoles,
                function ($userRole) {
                    return preg_match(ScopedRoleHierarchy::SCOPED_ROLE_DECOMPOSITION_REGEX, $userRole);
                }
            );

            $orphanServiceOfferRoles = array_diff($allServiceOfferRoles, $existingServiceOfferRoles);

            $cleanedUserRoles = array_diff($userRoles, $orphanServiceOfferRoles);

            $this->addSql(
                'UPDATE public.user SET roles = :cleaned_user_roles WHERE id = :id',
                [
                    'cleaned_user_roles' => json_encode(array_values($cleanedUserRoles)),
                    'id' => $user['id'],
                ]
            );
        }
    }

    public function down(Schema $schema): void
    {
        // aucun retour possible ni intéressant
    }
}
