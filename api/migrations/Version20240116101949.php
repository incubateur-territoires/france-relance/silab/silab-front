<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240116101949 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE service_offer SET link='equipements' WHERE link='liste-equipements'");
        $this->addSql("UPDATE service_offer SET link='interventions' WHERE link='liste-interventions'");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE service_offer SET link='liste-equipements' WHERE link='equipements'");
        $this->addSql("UPDATE service_offer SET link='liste-interventions' WHERE link='interventions'");
    }
}
