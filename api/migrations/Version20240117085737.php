<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240117085737 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE logistic_service_offer SET type_dinventaire_id = '1461792fbca-3e93' WHERE type_dinventaire_id IS NULL");
    }

    public function down(Schema $schema): void
    {
    }
}
