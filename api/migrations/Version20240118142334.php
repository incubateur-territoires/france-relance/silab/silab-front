<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240118142334 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE carl_configuration_intervention SET equipement_view_url_sub_path = 'equipment/box-DET.faces?CUSTOM_FORM=U%2CG1481a248ccb-e7257%2CF14786ccea5f-d099' WHERE equipement_view_url_sub_path IS NULL");
    }

    public function down(Schema $schema): void
    {
    }
}
