<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240123135656 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE service_offer SET link='intervention-service-offer/interventions' WHERE link NOT IN ('equipements','creer-une-intervention')");
        $this->addSql("UPDATE service_offer SET link='intervention-service-offer/équipements' WHERE link='equipements'");
        $this->addSql("UPDATE service_offer SET link='intervention-service-offer/interventions/créer' WHERE link='creer-une-intervention'");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE service_offer SET link='equipements' WHERE link='intervention-service-offer/équipements'");
        $this->addSql("UPDATE service_offer SET link='interventions' WHERE link='intervention-service-offer/interventions'");
        $this->addSql("UPDATE service_offer SET link='creer-une-intervention' WHERE link='intervention-service-offer/interventions/créer'");
    }
}
