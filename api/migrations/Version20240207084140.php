<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240207084140 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Remplacement des codes par des id dans CarlConfigurationIntervention';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE carl_configuration_intervention SET cost_centers = json_build_object('1', '15373b7a25f-b231') WHERE cost_centers->>'1' = 'DAEEP_PROPRETE'");
        $this->addSql("UPDATE carl_configuration_intervention SET directions = json_build_object('1', '180f2f264b3-35ff3') WHERE directions->>'1' = 'DAEEP_PROPRETE'");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE carl_configuration_intervention SET cost_centers = json_build_object('1', 'DAEEP_PROPRETE') WHERE cost_centers->>'1' = '15373b7a25f-b231'");
        $this->addSql("UPDATE carl_configuration_intervention SET directions = json_build_object('1', 'DAEEP_PROPRETE') WHERE directions->>'1' = '180f2f264b3-35ff3'");
    }

}
