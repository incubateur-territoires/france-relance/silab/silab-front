<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240209132634 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "Ajoute une colonne environnement à la table service_offer avec une valeur par défaut de 'PROD'";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE service_offer ADD environnement VARCHAR(255) DEFAULT \'PROD\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE service_offer DROP environnement');
    }
}
