<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240212101825 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Bascule de tous les liens des ODS intervention vers la vue globale';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE service_offer SET link='intervention-service-offer/index' WHERE template='intervention'");
    }

    public function down(Schema $schema): void
    {
    }
}
