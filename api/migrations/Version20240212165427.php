<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240212165427 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Fix les link des logistic service offer que la migration Version20240123135656 avait modifié par erreur';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE service_offer SET link='demandes-de-fournitures' WHERE template='logistic'");
    }

    public function down(Schema $schema): void
    {
    }
}
