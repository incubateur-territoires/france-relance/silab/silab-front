<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240219130436 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "Parcours toutes les offres de services et insère l'environnement qui convient en fonction de l'instance indiquée dans le titre de l'offre de service";
    }

    public function up(Schema $schema): void
    {
        $connection = $this->connection;

        $serviceOffersStatement = $connection->executeQuery('SELECT id, title, environnement FROM service_offer');
        $serviceOffers = $serviceOffersStatement->fetchAllAssociative();

        $integRegex = '/ \(INTEG\)|\(INTEG\)| INTEG\b|\bINTEG\b/i';
        $testRegex = '/ \(TEST\)|\(TEST\)| TEST\b|\bTEST\b/i';

        foreach ($serviceOffers as $serviceOffer) {
            $environment = $serviceOffer['environnement'];
            $title = $serviceOffer['title'];
            if (preg_match($integRegex, $serviceOffer['title'])) {
                $environment = 'INTEG';
                $title = preg_replace($integRegex, '', $serviceOffer['title']);
            } elseif (preg_match($testRegex, $serviceOffer['title'])) {
                $environment = 'TEST';
                $title = preg_replace($testRegex, '', $serviceOffer['title']);
            }
            // Mettre à jour la colonne environnement et title
            $connection->executeStatement(
                'UPDATE service_offer SET environnement = :environment, title = :title WHERE id = :id',
                ['environment' => $environment, 'title' => $title, 'id' => $serviceOffer['id']]
            );
        }
    }

    public function down(Schema $schema): void
    {
        // migration down inintéressante car c'est simplement le complémment de la migration Version20240209132634
    }
}
