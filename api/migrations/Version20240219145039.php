<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240219145039 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Retire les environnement des titres des GmaoConfigurationIntervention';
    }

    public function up(Schema $schema): void
    {
        $connection = $this->connection;

        $gmaoConfigurationInterventions = $connection->executeQuery('SELECT id, title FROM gmao_configuration_intervention')->fetchAllAssociative();

        $envRegex = '/( ?- ?)?( \((INTEG|TEST|PROD)\)|\((INTEG|TEST|PROD)\)| (INTEG|TEST|PROD)\b|\b(INTEG|TEST|PROD)\b)/i';

        foreach ($gmaoConfigurationInterventions as $gmaoConfigurationIntervention) {
            $newTitle = preg_replace($envRegex, '', $gmaoConfigurationIntervention['title']);

            $connection->executeStatement(
                'UPDATE gmao_configuration_intervention SET title = :title WHERE id = :id',
                ['title' => $newTitle, 'id' => $gmaoConfigurationIntervention['id']]
            );
        }
    }

    public function down(Schema $schema): void
    {
        $connection = $this->connection;

        $gmaoConfigurationInterventionTitlesWithClientTitlesQuery = 'SELECT gmao_configuration_intervention.id, gmao_configuration_intervention.title as "gmaoTitle", carl_client.title as "clientTitle"
            FROM gmao_configuration_intervention
            INNER JOIN carl_configuration_intervention ON gmao_configuration_intervention.id = carl_configuration_intervention.id
            INNER JOIN carl_client ON carl_configuration_intervention.carl_client_id = carl_client.id';
        $gmaoConfigurationsWithClientsTitles = $connection->executeQuery($gmaoConfigurationInterventionTitlesWithClientTitlesQuery)->fetchAllAssociative();

        foreach ($gmaoConfigurationsWithClientsTitles as $gmaoConfigurationWithClientTitle) {
            $environnementMatches = [];
            preg_match('/INTEG|TEST|PROD/i', $gmaoConfigurationWithClientTitle['clientTitle'], $environnementMatches);

            $newTitle = $gmaoConfigurationWithClientTitle['gmaoTitle'].(count($environnementMatches) > 0 ? ' - '.$environnementMatches[0] : '');
            $connection->executeStatement(
                'UPDATE gmao_configuration_intervention SET title = :title WHERE id = :id',
                ['title' => $newTitle, 'id' => $gmaoConfigurationWithClientTitle['id']]
            );
        }
    }
}