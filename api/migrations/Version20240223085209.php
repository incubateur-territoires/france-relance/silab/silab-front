<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240223085209 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Mise à jour des liens des ODS logistiques pour matcher la nouvelle organisation du router front';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE service_offer SET link = 'logistic-service-offer/index' WHERE template = 'logistic'");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE service_offer SET link = 'demandes-de-fournitures' WHERE template = 'logistic'");
    }
}
