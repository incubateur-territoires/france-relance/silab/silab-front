<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240223100431 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Mise à jour des liens des ODS elus pour matcher la nouvelle organisation du router front';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE service_offer SET link = 'elus-service-offer/index' WHERE template = 'elus'");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE service_offer SET link = 'liste-interventions' WHERE template = 'elus'");
    }
}
