<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240301144131 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout des Ged a carlClient';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE carl_client_ged_client (carl_client_id INT NOT NULL, ged_client_id INT NOT NULL, PRIMARY KEY(carl_client_id, ged_client_id))');
        $this->addSql('CREATE INDEX IDX_AB2F846DD7206B30 ON carl_client_ged_client (carl_client_id)');
        $this->addSql('CREATE INDEX IDX_AB2F846D9BF5C46C ON carl_client_ged_client (ged_client_id)');
        $this->addSql('ALTER TABLE carl_client_ged_client ADD CONSTRAINT FK_AB2F846DD7206B30 FOREIGN KEY (carl_client_id) REFERENCES carl_client (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE carl_client_ged_client ADD CONSTRAINT FK_AB2F846D9BF5C46C FOREIGN KEY (ged_client_id) REFERENCES ged_client (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE carl_client ADD ged_document_writer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE carl_client ADD CONSTRAINT FK_669E4C9473F38A5A FOREIGN KEY (ged_document_writer_id) REFERENCES ged_client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_669E4C9473F38A5A ON carl_client (ged_document_writer_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_client_ged_client DROP CONSTRAINT FK_AB2F846DD7206B30');
        $this->addSql('ALTER TABLE carl_client_ged_client DROP CONSTRAINT FK_AB2F846D9BF5C46C');
        $this->addSql('DROP TABLE carl_client_ged_client');
        $this->addSql('ALTER TABLE carl_client DROP CONSTRAINT FK_669E4C9473F38A5A');
        $this->addSql('DROP INDEX IDX_669E4C9473F38A5A');
        $this->addSql('ALTER TABLE carl_client DROP ged_document_writer_id');
    }
}
