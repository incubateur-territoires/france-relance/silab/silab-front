<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240304092708 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Transfert des clients GED des ODS vers les clients carl';
    }

    public function up(Schema $schema): void
    {
        $connection = $this->connection;

        $interventionServiceOfferAndCorrespondingGedAndCarlClientIds = $connection->executeQuery(
            'SELECT intervention_service_offer.id AS intervention_service_offer_id, ged_client_id, carl_client_id
                FROM intervention_service_offer
                INNER JOIN carl_configuration_intervention ON intervention_service_offer.gmao_configuration_id = carl_configuration_intervention.id
                INNER JOIN carl_client ON carl_configuration_intervention.carl_client_id = carl_client.id'
        )->fetchAllAssociative();

        foreach ($interventionServiceOfferAndCorrespondingGedAndCarlClientIds as $interventionServiceOfferAndCorrespondingGedAndCarlClientId) {
            $connection->executeStatement(
                'INSERT INTO carl_client_ged_client (ged_client_id, carl_client_id) VALUES ( :ged_client_id, :carl_client_id)
                ON CONFLICT DO NOTHING',
                [
                    'ged_client_id' => $interventionServiceOfferAndCorrespondingGedAndCarlClientId['ged_client_id'],
                    'carl_client_id' => $interventionServiceOfferAndCorrespondingGedAndCarlClientId['carl_client_id'],
                ]
            );

            $connection->executeStatement(
                'UPDATE carl_client SET ged_document_writer_id = :ged_document_writer_id WHERE carl_client.id = :carl_client_id',
                [
                    'ged_document_writer_id' => $interventionServiceOfferAndCorrespondingGedAndCarlClientId['ged_client_id'],
                    'carl_client_id' => $interventionServiceOfferAndCorrespondingGedAndCarlClientId['carl_client_id'],
                ]
            );
        }
    }

    public function down(Schema $schema): void
    {
        $connection = $this->connection;

        $connection->executeQuery('DELETE FROM carl_client_ged_client');
    }
}
