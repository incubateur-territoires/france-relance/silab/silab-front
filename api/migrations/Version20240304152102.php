<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240304152102 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Déplacement du paramètre du dossier de dépôt Alfresco directement dans le client.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE alfresco_ged_client ADD drop_folder VARCHAR(255)');
        $this->addSql("UPDATE alfresco_ged_client SET drop_folder = 'alfresco/api/-default-/public/cmis/versions/1.1/browser/root/Sites/sipatrimoine/documentLibrary/APP_SILAB/Incoming'");
        $this->addSql('ALTER TABLE alfresco_ged_client ALTER drop_folder SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE alfresco_ged_client DROP drop_folder');
    }
}
