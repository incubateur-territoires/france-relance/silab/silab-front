<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240304162613 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Retrait des anciens champs liés à la ged';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention_service_offer DROP CONSTRAINT fk_9ea0e6159bf5c46c');
        $this->addSql('DROP INDEX idx_9ea0e6159bf5c46c');
        $this->addSql('ALTER TABLE intervention_service_offer DROP ged_client_id');
        $this->addSql('ALTER TABLE intervention_service_offer DROP ged_client_configuration');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention_service_offer ADD ged_client_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE intervention_service_offer ADD ged_client_configuration JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE intervention_service_offer ADD CONSTRAINT fk_9ea0e6159bf5c46c FOREIGN KEY (ged_client_id) REFERENCES ged_client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_9ea0e6159bf5c46c ON intervention_service_offer (ged_client_id)');
    }
}
