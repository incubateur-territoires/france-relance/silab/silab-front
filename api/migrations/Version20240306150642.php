<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240306150642 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout de la configuration et insertion de la config actuelle silab GP';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE alfresco_ged_client ADD metadada_mappings JSON DEFAULT \'{}\' NOT NULL');
        $this->addSql('UPDATE alfresco_ged_client SET metadada_mappings = \'{"interventionServiceOffer:object:description":{"property":"cm:description","secondaryObjectType":"P:cm:titled"},"interventionServiceOffer:object:title":{"property":"cm:title","secondaryObjectType":"P:cm:titled"},"serviceOffer:title":{"property":"silab2:mainApp","secondaryObjectType":"P:silab2:main"},"interventionServiceOffer:intervention:code":{"property":"carl:woCode","secondaryObjectType":"P:carl:WO"},"interventionServiceOffer:demandeIntervention:code":{"property":"carl:mrCode","secondaryObjectType":"P:carl:MR"}}\' WHERE instance_url = \'https://gedtest.grandpoitiers.fr\'');
        $this->addSql('UPDATE alfresco_ged_client SET metadada_mappings = \'{"interventionServiceOffer:object:description":{"property":"cm:description","secondaryObjectType":"P:cm:titled"},"interventionServiceOffer:object:title":{"property":"cm:title","secondaryObjectType":"P:cm:titled"},"serviceOffer:title":{"property":"silab:mainApp","secondaryObjectType":"P:silab:main"},"interventionServiceOffer:intervention:code":{"property":"carl:woCode","secondaryObjectType":"P:carl:WO"},"interventionServiceOffer:demandeIntervention:code":{"property":"carl:mrCode","secondaryObjectType":"P:carl:MR"}}\' WHERE instance_url = \'https://ged.grandpoitiers.fr\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE alfresco_ged_client DROP metadada_mappings');
    }
}
