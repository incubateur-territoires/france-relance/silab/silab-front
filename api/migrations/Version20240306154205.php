<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240306154205 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout des Ged alfresco prod en lecture sur les client carl test et integ';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('INSERT INTO carl_client_ged_client SELECT carl_client.id, (SELECT id FROM alfresco_ged_client WHERE instance_url=\'https://ged.grandpoitiers.fr\' LIMIT 1) FROM carl_client WHERE base_url IN (\'https://patrimoine-integ.grandpoitiers.fr/gmao/\', \'https://patrimoine-test.grandpoitiers.fr/gmao/\') AND (SELECT id FROM alfresco_ged_client WHERE instance_url=\'https://ged.grandpoitiers.fr\' LIMIT 1) IS NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM carl_client_ged_client USING carl_client, alfresco_ged_client WHERE carl_client_ged_client.carl_client_id = carl_client.id AND carl_client_ged_client.ged_client_id = alfresco_ged_client.id AND alfresco_ged_client.instance_url=\'https://ged.grandpoitiers.fr\' AND carl_client.base_url IN (\'https://patrimoine-integ.grandpoitiers.fr/gmao/\', \'https://patrimoine-test.grandpoitiers.fr/gmao/\')');
    }
}
