<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240307130511 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout champ photo_illustration_equipement_doctype_ids dans client carl';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_client ADD photo_illustration_equipement_doctype_ids JSON DEFAULT \'[]\' NOT NULL');
        $this->addSql('UPDATE carl_client SET photo_illustration_equipement_doctype_ids = \'["IMG"]\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_client DROP photo_illustration_equipement_doctype_ids');
    }
}
