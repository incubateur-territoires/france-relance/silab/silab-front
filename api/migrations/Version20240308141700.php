<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240308141700 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajoute les clients mezzoteam api v2 en BDD.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE mezzoteam_v2_ged_client (id INT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, instance_url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE mezzoteam_v2_ged_client ADD CONSTRAINT FK_78433D06BF396750 FOREIGN KEY (id) REFERENCES ged_client (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE carl_client DROP photo_illustration_equipement_doctype_ids');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE mezzoteam_v2_ged_client DROP CONSTRAINT FK_78433D06BF396750');
        $this->addSql('DROP TABLE mezzoteam_v2_ged_client');
        $this->addSql('ALTER TABLE carl_client ADD photo_illustration_equipement_doctype_ids JSON DEFAULT \'[]\' NOT NULL');
    }
}
