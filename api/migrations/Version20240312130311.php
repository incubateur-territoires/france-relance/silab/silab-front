<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240312130311 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Remise des valeurs par défautl GP suite à pb migration';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE carl_client SET photo_illustration_equipement_doctype_ids = \'["IMG"]\'');
    }

    public function down(Schema $schema): void
    {
    }
}
