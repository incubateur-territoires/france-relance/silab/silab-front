<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240321152837 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "Ajout de l'attribut mr_view_url_sub_path  carl_configuration_intervention";
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD mr_view_url_sub_path VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP mr_view_url_sub_path');
    }
}
