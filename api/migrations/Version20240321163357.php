<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240321163357 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "Met à jour le champ mr_view_url_sub_path pour avoir un lien Carl pour les demandes d'intervention";
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("UPDATE carl_configuration_intervention SET mr_view_url_sub_path = 'works/MR-DET.faces?CUSTOM_FORM=U%2CG161d5d948f4-4e8a%2CF1695a3078b6-a4e57' WHERE mr_view_url_sub_path IS NULL");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
