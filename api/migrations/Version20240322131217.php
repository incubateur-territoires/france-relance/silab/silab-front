<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240322131217 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Abandon des redirectserviceoffer';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE redirect_service_offer DROP CONSTRAINT fk_7e62e14fbf396750');
        $this->addSql('DROP TABLE redirect_service_offer');
        $this->addSql("delete from service_offer where template='redirect'");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE redirect_service_offer (id INT NOT NULL, redirect_url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE redirect_service_offer ADD CONSTRAINT fk_7e62e14fbf396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
