<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240403073305 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Augmentation des champs des messages';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE message ALTER title TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE message ALTER body TYPE VARCHAR(400)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE message ALTER title TYPE VARCHAR(50)');
        $this->addSql('ALTER TABLE message ALTER body TYPE VARCHAR(255)');
    }
}
