<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240404144107 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'cost center est éclaté en diférent paramètre : ecriture, lecture et depuis un eqpt';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD cost_center_id_for_intervention_creation VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD use_equipment_cost_center_for_intervention_creation BOOLEAN DEFAULT true NOT NULL');
        $this->addSql('ALTER TABLE carl_configuration_intervention RENAME COLUMN cost_centers TO intervention_cost_center_ids_filter');
        $this->addSql("UPDATE carl_configuration_intervention SET cost_center_id_for_intervention_creation = (intervention_cost_center_ids_filter->>'1') WHERE intervention_cost_center_ids_filter->>'1' IS NOT NULL");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP cost_center_id_for_intervention_creation');
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP use_equipment_cost_center_for_intervention_creation');
        $this->addSql('ALTER TABLE carl_configuration_intervention RENAME COLUMN intervention_cost_center_ids_filter TO cost_centers');
    }
}
