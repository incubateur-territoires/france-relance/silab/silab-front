<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240408143709 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'intervention_cost_center_ids_filter obligatoire';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention ALTER intervention_cost_center_ids_filter SET DEFAULT \'[]\'');
        $this->addSql("UPDATE carl_configuration_intervention SET intervention_cost_center_ids_filter = '[]' WHERE intervention_cost_center_ids_filter IS NULL");
        $this->addSql('ALTER TABLE carl_configuration_intervention ALTER intervention_cost_center_ids_filter SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE carl_configuration_intervention ALTER intervention_cost_center_ids_filter DROP DEFAULT');
        $this->addSql('ALTER TABLE carl_configuration_intervention ALTER intervention_cost_center_ids_filter DROP NOT NULL');
    }
}
