<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240410121029 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Découpage de equipementViewUrlSubPath en box... et material... et ajout d un subpath par defaut pour les confs déjà créées';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE carl_configuration_intervention ADD material_view_url_sub_path VARCHAR(255) DEFAULT NULL");
        $this->addSql("UPDATE carl_configuration_intervention SET material_view_url_sub_path = 'equipment/material-DET.faces?CUSTOM_FORM=U%2CG1481a248ccb-e7257%2CF14786ccea5f-d099'");
        $this->addSql('ALTER TABLE carl_configuration_intervention RENAME COLUMN equipement_view_url_sub_path TO box_view_url_sub_path');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP material_view_url_sub_path');
        $this->addSql('ALTER TABLE carl_configuration_intervention RENAME COLUMN box_view_url_sub_path TO equipement_view_url_sub_path');
    }
}
