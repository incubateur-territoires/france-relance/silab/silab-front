<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240412134709 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "Ajoute une colonne carl_attributes_preset_for_intervention_creation pour stocker les attributs prédéfinis pour la création d'interventions dans la table carl_configuration_intervention";
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD carl_attributes_preset_for_intervention_creation JSON DEFAULT \'{}\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP carl_attributes_preset_for_intervention_creation');
    }
}
