<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240415153805 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "Mes à jour le champ des attributs prédéfinis pour la création d'interventions des configurations Carl intervention";
    }

    public function up(Schema $schema): void
    {
        // Met à jour le champ des attributs prédéfinis pour la création d'interventions pour Propreté
        $this->addSql('UPDATE carl_configuration_intervention SET carl_attributes_preset_for_intervention_creation = \'{"xtraTxt06":"PROPRETE"}\' WHERE id IN (1, 2, 8)');

        // Met à jour le champ des attributs prédéfinis pour la création d'interventions pour DPB
        $this->addSql('UPDATE carl_configuration_intervention SET carl_attributes_preset_for_intervention_creation = \'{"xtraTxt06":"IMMOBILIER"}\' WHERE id IN (15)');

        // Met à jour le champ des attributs prédéfinis pour la création d'interventions pour UTEU et STEP
        $this->addSql('UPDATE carl_configuration_intervention SET carl_attributes_preset_for_intervention_creation = \'{"xtraTxt06":"ASS"}\' WHERE id IN (10, 11, 16)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE carl_configuration_intervention SET carl_attributes_preset_for_intervention_creation = \'{}\'');
    }
}
