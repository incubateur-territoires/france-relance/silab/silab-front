<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240417130317 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Créé/active l\'extension oracle_fdw et le schema carl servant à isoler les foreign tables issues de la db carl.Ajoute les champs de conf db aux clients carl.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE EXTENSION IF NOT EXISTS oracle_fdw');
        $this->addSql('CREATE SCHEMA IF NOT EXISTS carl');
        $this->addSql('ALTER TABLE carl_client ADD db_server VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE carl_client ADD db_shema VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE carl_client ADD db_user VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE carl_client ADD db_password VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $carlClients = $this->connection->fetchAllAssociative('SELECT id FROM public.carl_client');
        foreach ($carlClients as $carlClient) {
            $serverName = 'carl_'.$carlClient['id'].'_db_oracle';

            // on drop le serveur et tous les objets liés
            $this->addSql('DROP SERVER IF EXISTS '.$serverName.' CASCADE');
        }
        $this->addSql('DROP SCHEMA IF EXISTS carl');
        $this->addSql('DROP EXTENSION IF EXISTS oracle_fdw');
        $this->addSql('ALTER TABLE carl_client DROP db_server');
        $this->addSql('ALTER TABLE carl_client DROP db_shema');
        $this->addSql('ALTER TABLE carl_client DROP db_user');
        $this->addSql('ALTER TABLE carl_client DROP db_password');
    }
}
