<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240515151946 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration vide servant simplement à mettre à jour les foreign tables des clients Carl suite aux derniers ajouts portant sur les demandes d\'interventions et les acteurs (via les listeners)';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
