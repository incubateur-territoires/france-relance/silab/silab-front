<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240521151050 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration vide servant simplement à déclancher la mise à jour des foreign tables des clients Carl suite aux derniers ajouts portant sur les documents (via les listeners)';
    }

    public function up(Schema $schema): void
    {
    }

    public function down(Schema $schema): void
    {
    }
}
