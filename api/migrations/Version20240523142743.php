<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240523142743 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration vide servant simplement à mettre à jour les foreign tables des clients Carl suite a l\'ajout des adresses sur les demandes d\'interventions et les interventions';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
