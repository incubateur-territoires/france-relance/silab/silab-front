<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240611083713 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Crée la table siilab_configuration avec une seule ligne et un ID fixe';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE siilab_configuration (
            id INT NOT NULL,
            map_center_latitude DOUBLE PRECISION DEFAULT 46.584545 NOT NULL,
            map_center_longitude DOUBLE PRECISION DEFAULT 0.338348 NOT NULL,
            PRIMARY KEY(id)
        )');

        $this->addSql('INSERT INTO siilab_configuration (id) VALUES (1)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE siilab_configuration');
    }
}
