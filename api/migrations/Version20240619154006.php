<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240619154006 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration vide servant simplement à mettre à jour les foreign tables de GmaoActor';
    }

    public function up(Schema $schema): void
    {
    }

    public function down(Schema $schema): void
    {
    }
}
