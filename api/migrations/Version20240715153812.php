<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240715153812 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajoute un champ pour le stockage du matériel consommé avec une initialisation pour gp, et un champ pour la gestion du réapprovisionnement';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_client ADD champ_carl_pour_stockage_materiel_consomme VARCHAR(255) DEFAULT NULL');
        $this->addSql("UPDATE carl_client SET champ_carl_pour_stockage_materiel_consomme = 'poiXtraTxt20'"); // initialisation pour GP
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD gestion_recompletement_actif BOOLEAN DEFAULT false NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_client DROP champ_carl_pour_stockage_materiel_consomme');
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP gestion_recompletement_actif');
    }
}
