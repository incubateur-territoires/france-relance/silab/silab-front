<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240725092246 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'corrige l\'emplacement du champ gestion_recompletement_actif';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention_service_offer ADD gestion_recompletement_actif BOOLEAN DEFAULT false NOT NULL');
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP gestion_recompletement_actif');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention_service_offer DROP gestion_recompletement_actif');
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD gestion_recompletement_actif BOOLEAN DEFAULT false NOT NULL');
    }
}
