<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240729113905 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout des champs sur les clients Carl pour la visualisation des liens vers les formulaires Carl';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_client ADD wo_view_url_sub_path VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE carl_client ADD mr_view_url_sub_path VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE carl_client ADD box_view_url_sub_path VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE carl_client ADD material_view_url_sub_path VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_client DROP wo_view_url_sub_path');
        $this->addSql('ALTER TABLE carl_client DROP mr_view_url_sub_path');
        $this->addSql('ALTER TABLE carl_client DROP box_view_url_sub_path');
        $this->addSql('ALTER TABLE carl_client DROP material_view_url_sub_path');
    }
}
