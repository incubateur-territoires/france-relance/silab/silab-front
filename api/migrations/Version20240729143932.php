<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240729143932 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Vide les champs pour les liens Carl de la table carl configuration intervention et ajoute les sous-chemins pour les liens des clients Carl';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'UPDATE carl_configuration_intervention
            SET wo_view_url_sub_path = NULL,
            mr_view_url_sub_path = NULL,
            box_view_url_sub_path = NULL,
            material_view_url_sub_path = NULL'
        );

        $this->addSql(
            "UPDATE carl_client
            SET wo_view_url_sub_path = 'works/WO-DET.faces?SELECTED_TAB=pa_WO_DET_general&CUSTOM_FORM=U18416516e5b-6e39%2CG16192e69efe-35bd8%2CF16192e69efe-8ff7',
            mr_view_url_sub_path = 'works/MR-DET.faces?SELECTED_TAB=pa_MR_DET_general&CUSTOM_FORM=U18416516e5b-6e39%2CG16b8f1a449b-8ec2d2a%2CF1695a3078b6-a4e57',
            box_view_url_sub_path = 'equipment/box-DET.faces?SELECTED_TAB=pa_Box_DET_Gen&CUSTOM_FORM=U18416516e5b-6e39%2CG1481a248ccb-e7274%2CF14786ccea5f-9b27',
            material_view_url_sub_path = 'equipment/material-DET.faces?SELECTED_TAB=pa_Material_DET_gen&CUSTOM_FORM=U18416516e5b-6e39%2CG1481a248ccb-e727d%2CF14786ccea5f-d099'"
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            "UPDATE carl_configuration_intervention
            SET wo_view_url_sub_path = 'works/WO-DET.faces?SELECTED_TAB=pa_WO_DET_general&CUSTOM_FORM=U18416516e5b-6e39%2CG16192e69efe-35bd8%2CF16192e69efe-8ff7',
            mr_view_url_sub_path = 'works/MR-DET.faces?SELECTED_TAB=pa_MR_DET_general&CUSTOM_FORM=U18416516e5b-6e39%2CG16b8f1a449b-8ec2d2a%2CF1695a3078b6-a4e57',
            box_view_url_sub_path = 'equipment/box-DET.faces?SELECTED_TAB=pa_Box_DET_Gen&CUSTOM_FORM=U18416516e5b-6e39%2CG1481a248ccb-e7274%2CF14786ccea5f-9b27',
            material_view_url_sub_path = 'equipment/material-DET.faces?SELECTED_TAB=pa_Material_DET_gen&CUSTOM_FORM=U18416516e5b-6e39%2CG1481a248ccb-e727d%2CF14786ccea5f-d099'"
        );

        $this->addSql(
            'UPDATE carl_client
            SET wo_view_url_sub_path = NULL,
            mr_view_url_sub_path = NULL,
            box_view_url_sub_path = NULL,
            material_view_url_sub_path = NULL'
        );
    }
}
