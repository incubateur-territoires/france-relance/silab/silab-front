<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240729153221 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout de la configuration du formulaire de création des activités';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention_service_offer ADD configuration_formulaire_activite_date_de_debut VARCHAR(255) DEFAULT \'automatique\' NOT NULL');
        $this->addSql('ALTER TABLE intervention_service_offer ADD configuration_formulaire_activite_heure_par_defaut_en_minute INT DEFAULT 480 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention_service_offer DROP configuration_formulaire_activite_date_de_debut');
        $this->addSql('ALTER TABLE intervention_service_offer DROP configuration_formulaire_activite_heure_par_defaut_en_minute');
    }
}
