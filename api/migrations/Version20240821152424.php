<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240821152424 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Cette migration sert à ajouter des notes dans gmao_configuration_intervention';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gmao_configuration_intervention ADD notes TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SCHEMA carl');
        $this->addSql('ALTER TABLE gmao_configuration_intervention DROP notes');
    }
}
