<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240822074159 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajoute une colonne notes à la table service_offer';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE service_offer ADD notes TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE service_offer DROP notes');
    }
}
