<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240916090748 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout d\'un champ pour le modèle des description sur les formulaires des DIs';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE intervention_service_offer ADD modele_de_description_pour_demande_intervention TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE intervention_service_offer DROP modele_de_description_pour_demande_intervention');
    }
}
