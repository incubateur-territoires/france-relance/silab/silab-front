<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240920064331 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout d\'un boolean pour la gestion des dates de début et de fin souhaitées pour les DIs';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE intervention_service_offer ADD gestion_dates_souhaitees_pour_demande_intervention_actif BOOLEAN DEFAULT false NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE intervention_service_offer DROP gestion_dates_souhaitees_pour_demande_intervention_actif');
    }
}
