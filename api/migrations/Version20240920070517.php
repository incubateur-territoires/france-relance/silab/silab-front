<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240920070517 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout d\'un champ pour stocker le nom de la colonne carl liée à la date de début souhaitée d\'une DI';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_client ADD champ_carl_pour_stockage_date_debut_souhaitee_di VARCHAR(255) DEFAULT NULL');
        $this->addSql("UPDATE carl_client SET champ_carl_pour_stockage_date_debut_souhaitee_di = 'xtraDate02'"); // initialisation pour GP
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_client DROP champ_carl_pour_stockage_date_debut_souhaitee_di');
    }
}
