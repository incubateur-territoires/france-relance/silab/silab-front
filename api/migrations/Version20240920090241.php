<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240920090241 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout d\'un boolean pour la gestion des dates de début et de fin pour les Interventions';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention_service_offer ADD gestion_dates_pour_intervention_actif BOOLEAN DEFAULT false NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention_service_offer DROP gestion_dates_pour_intervention_actif');
    }
}
