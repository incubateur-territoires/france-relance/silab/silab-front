<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240927081313 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajoute l\'information de timezone sur les champ de dates des messages pour éviter les problèmes de décalage';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE message ALTER start_date TYPE TIMESTAMP(0) WITH TIME ZONE');
        $this->addSql('ALTER TABLE message ALTER end_date TYPE TIMESTAMP(0) WITH TIME ZONE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE message ALTER start_date TYPE DATE');
        $this->addSql('ALTER TABLE message ALTER end_date TYPE DATE');
    }
}
