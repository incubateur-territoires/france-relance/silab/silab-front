<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241114130312 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout de l\'extension fuzzystrmatch';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE EXTENSION IF NOT EXISTS fuzzystrmatch');
    }

    public function down(Schema $schema): void
    {
    }
}
