<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Shared\Carl\ValueObject\CarlEqptIdAndType;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241118151141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Convertion des id des équipements vers le nouveau format';
    }

    public function up(Schema $schema): void
    {
        $interventionServiceOffers = $this->connection->fetchAllAssociative('SELECT intervention_service_offer.id, intervention_service_offer.available_equipements_ids FROM intervention_service_offer INNER JOIN carl_configuration_intervention on carl_configuration_intervention.id = intervention_service_offer.gmao_configuration_id;');
        foreach ($interventionServiceOffers as $interventionServiceOffer) {
            $anciensIdEquipementAuFormatSiilab = json_decode($interventionServiceOffer['available_equipements_ids']);

            $nouveauxIdEquipements = array_map(
                function ($idAncienFormat) {
                    return CarlEqptIdAndType::fromJson($idAncienFormat)->getId();
                },
                $anciensIdEquipementAuFormatSiilab
            );

            $sqlParams = [
                'id' => $interventionServiceOffer['id'],
                'nouveauxIdEquipements' => json_encode($nouveauxIdEquipements),
            ];

            $this->addSql('UPDATE intervention_service_offer SET available_equipements_ids = :nouveauxIdEquipements WHERE id = :id', $sqlParams);
        }
    }

    /**
     * Note: cette migration down n'est pas parfaite et génèrerat des problèmes sur un rollback pour certaines opérations nécéssitant les types.
     */
    public function down(Schema $schema): void
    {
        $interventionServiceOffers = $this->connection->fetchAllAssociative('SELECT intervention_service_offer.id, intervention_service_offer.available_equipements_ids FROM intervention_service_offer INNER JOIN carl_configuration_intervention on carl_configuration_intervention.id = intervention_service_offer.gmao_configuration_id;');
        foreach ($interventionServiceOffers as $interventionServiceOffer) {
            $nouveauxIdEquipements = json_decode($interventionServiceOffer['available_equipements_ids']);

            $idEquipementAuFormatSiilab = array_map(
                function ($idNouveauFormat) {
                    return (new CarlEqptIdAndType(id: $idNouveauFormat, type: 'deprecated'))->getSilabId();
                },
                $nouveauxIdEquipements
            );

            $sqlParams = [
                'id' => $interventionServiceOffer['id'],
                'idEquipementsAncienFormat' => json_encode($idEquipementAuFormatSiilab),
            ];

            $this->addSql('UPDATE intervention_service_offer SET available_equipements_ids = :idEquipementsAncienFormat WHERE id = :id', $sqlParams);
        }
    }
}
