<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241209094454 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Renommage de gmaoConfiguration vers gmaoConfigurationLogistique';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE logistic_service_offer DROP CONSTRAINT fk_26cd5ce96c9802c7');
        $this->addSql('DROP INDEX idx_26cd5ce96c9802c7');
        $this->addSql('ALTER TABLE logistic_service_offer RENAME COLUMN gmao_configuration_id TO gmao_configuration_logistique_id');
        $this->addSql('ALTER TABLE logistic_service_offer ADD CONSTRAINT FK_26CD5CE9B2E77568 FOREIGN KEY (gmao_configuration_logistique_id) REFERENCES logisticserviceoffer_gmao_configuration (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_26CD5CE9B2E77568 ON logistic_service_offer (gmao_configuration_logistique_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE logistic_service_offer DROP CONSTRAINT FK_26CD5CE9B2E77568');
        $this->addSql('DROP INDEX IDX_26CD5CE9B2E77568');
        $this->addSql('ALTER TABLE logistic_service_offer RENAME COLUMN gmao_configuration_logistique_id TO gmao_configuration_id');
        $this->addSql('ALTER TABLE logistic_service_offer ADD CONSTRAINT fk_26cd5ce96c9802c7 FOREIGN KEY (gmao_configuration_id) REFERENCES logisticserviceoffer_gmao_configuration (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_26cd5ce96c9802c7 ON logistic_service_offer (gmao_configuration_id)');
    }
}
