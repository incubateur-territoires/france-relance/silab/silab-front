<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250210131334 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creation des tables pour la configuration des tableaux de mesures';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE odsint_configuration_tableau_de_mesures_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE odsint_emplacement_de_compteur_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE odsint_ligne_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE odsint_sous_cellule_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE odsint_configuration_tableau_de_mesures (id INT NOT NULL, titre VARCHAR(255) NOT NULL, intervention_service_offer_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3D75612AA25E53B7 ON odsint_configuration_tableau_de_mesures (intervention_service_offer_id)');
        $this->addSql('CREATE TABLE odsint_emplacement_de_compteur (id INT NOT NULL, id_compteur VARCHAR(255) NOT NULL, ligne_id INT NOT NULL, sous_cellule_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EE5234A85A438E76 ON odsint_emplacement_de_compteur (ligne_id)');
        $this->addSql('CREATE INDEX IDX_EE5234A87051C84C ON odsint_emplacement_de_compteur (sous_cellule_id)');
        $this->addSql('CREATE UNIQUE INDEX emplacement_dans_le_tableau ON odsint_emplacement_de_compteur (ligne_id, sous_cellule_id)');
        $this->addSql('CREATE TABLE odsint_ligne (id INT NOT NULL, titre VARCHAR(255) NOT NULL, ordre INT NOT NULL, configuration_tableau_de_mesures_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9DEC09C576ADCF19 ON odsint_ligne (configuration_tableau_de_mesures_id)');
        $this->addSql('CREATE UNIQUE INDEX ordre_dans_le_tableau ON odsint_ligne (ordre, configuration_tableau_de_mesures_id)');
        $this->addSql('CREATE TABLE odsint_sous_cellule (id INT NOT NULL, titre VARCHAR(255) NOT NULL, ordre INT NOT NULL, configuration_tableau_de_mesures_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_13572F1476ADCF19 ON odsint_sous_cellule (configuration_tableau_de_mesures_id)');
        $this->addSql('CREATE UNIQUE INDEX ordre_dans_les_cellules_du_tableau ON odsint_sous_cellule (ordre, configuration_tableau_de_mesures_id)');
        $this->addSql('ALTER TABLE odsint_configuration_tableau_de_mesures ADD CONSTRAINT FK_3D75612AA25E53B7 FOREIGN KEY (intervention_service_offer_id) REFERENCES intervention_service_offer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE odsint_emplacement_de_compteur ADD CONSTRAINT FK_EE5234A85A438E76 FOREIGN KEY (ligne_id) REFERENCES odsint_ligne (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE odsint_emplacement_de_compteur ADD CONSTRAINT FK_EE5234A87051C84C FOREIGN KEY (sous_cellule_id) REFERENCES odsint_sous_cellule (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE odsint_ligne ADD CONSTRAINT FK_9DEC09C576ADCF19 FOREIGN KEY (configuration_tableau_de_mesures_id) REFERENCES odsint_configuration_tableau_de_mesures (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE odsint_sous_cellule ADD CONSTRAINT FK_13572F1476ADCF19 FOREIGN KEY (configuration_tableau_de_mesures_id) REFERENCES odsint_configuration_tableau_de_mesures (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE odsint_configuration_tableau_de_mesures_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE odsint_emplacement_de_compteur_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE odsint_ligne_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE odsint_sous_cellule_id_seq CASCADE');
        $this->addSql('ALTER TABLE odsint_configuration_tableau_de_mesures DROP CONSTRAINT FK_3D75612AA25E53B7');
        $this->addSql('ALTER TABLE odsint_emplacement_de_compteur DROP CONSTRAINT FK_EE5234A85A438E76');
        $this->addSql('ALTER TABLE odsint_emplacement_de_compteur DROP CONSTRAINT FK_EE5234A87051C84C');
        $this->addSql('ALTER TABLE odsint_ligne DROP CONSTRAINT FK_9DEC09C576ADCF19');
        $this->addSql('ALTER TABLE odsint_sous_cellule DROP CONSTRAINT FK_13572F1476ADCF19');
        $this->addSql('DROP TABLE odsint_configuration_tableau_de_mesures');
        $this->addSql('DROP TABLE odsint_emplacement_de_compteur');
        $this->addSql('DROP TABLE odsint_ligne');
        $this->addSql('DROP TABLE odsint_sous_cellule');
    }
}
