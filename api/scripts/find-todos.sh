#!/bin/sh
# adapted from https://betterprogramming.pub/want-to-avoid-forgotten-todos-in-your-project-lets-do-it-with-git-hooks-6a1835f26cf5
TODOS_FOUND=$(grep -i -rn --exclude-dir=vendor --exclude-dir=public --exclude-dir=var -P '(\/\/|\*|\s|^|#)\s*todo(\s|$|\*|:)' . || true)

if [ ! -z "$TODOS_FOUND" ] # To check if TODOS_FOUND is not empty
then

 echo  "Your project has some TODOs: "
    printf %s "$TODOS_FOUND"
 echo \n
 exit 1
fi
