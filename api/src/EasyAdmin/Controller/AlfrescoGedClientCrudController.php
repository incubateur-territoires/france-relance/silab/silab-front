<?php

namespace App\EasyAdmin\Controller;

use App\EasyAdmin\Form\JsonEditorType;
use App\Shared\Ged\Entity\AlfrescoGedClient;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class AlfrescoGedClientCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return AlfrescoGedClient::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('title'),
            TextField::new('instanceUrl'),
            TextField::new('username'),
            TextField::new('password')->setFormType(PasswordType::class)
                ->hideOnIndex()
                ->hideOnDetail(),
            TextField::new('dropFolder'),
            CodeEditorField::new('metadadaMappings')
                ->setFormType(JsonEditorType::class)
                ->hideOnIndex(), // Note: ça plante sur l'index, jsp pk
        ];
    }
}
