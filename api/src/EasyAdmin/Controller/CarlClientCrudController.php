<?php

namespace App\EasyAdmin\Controller;

use App\EasyAdmin\Form\JsonEditorType;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Ged\Repository\GedClientRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CarlClientCrudController extends AbstractCrudController
{
    public function __construct(private GedClientRepository $gedClientRepository)
    {
    }

    public static function getEntityFqcn(): string
    {
        return CarlClient::class;
    }

    public function configureFields(string $pageName): iterable
    {
        // clients GED disponibles
        $gedClients = $this->gedClientRepository->findAll();
        $gedClientsChoice = ['aucun' => null];
        foreach ($gedClients as $gedClient) {
            $gedClientsChoice[$gedClient->getTitle()] = $gedClient;
        }

        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('title'),
            TextField::new('baseUrl'),
            TextField::new('login'),
            TextField::new('password')
                ->hideOnIndex()
                ->hideOnDetail(),
            TextField::new('dbServer'),
            TextField::new('dbShema'),
            TextField::new('dbUser'),
            TextField::new('dbPassword')
                ->hideOnIndex()
                ->hideOnDetail(),
            IntegerField::new('pagelimit'),
            CodeEditorField::new('priorityMapping')
                ->setFormType(JsonEditorType::class)
                ->hideOnIndex(), // Note: ça plante sur l'index, jsp pk
            ChoiceField::new('gedDocumentWriter')
                ->allowMultipleChoices(false)
                ->setChoices($gedClientsChoice)
                ->hideOnDetail()
                ->hideOnIndex(),
            TextField::new('gedDocumentWriter')->hideOnForm(),
            ArrayField::new('photoIllustrationEquipementDoctypeIds'),
            ChoiceField::new('gedDocumentReaders')
                ->allowMultipleChoices(true)
                ->setChoices($gedClientsChoice)
                ->hideOnDetail()
                ->hideOnIndex(),
            TextField::new('champCarlPourStockageMaterielConsomme'),
            TextField::new('champCarlPourStockageDateDebutSouhaiteeDI'),
            TextField::new('woViewUrlSubPath'),
            TextField::new('mrViewUrlSubPath'),
            TextField::new('boxViewUrlSubPath'),
            TextField::new('materialViewUrlSubPath'),
        ];
    }
}
