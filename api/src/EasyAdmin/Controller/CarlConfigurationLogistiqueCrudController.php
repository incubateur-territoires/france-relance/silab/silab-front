<?php

namespace App\EasyAdmin\Controller;

use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\Shared\Carl\Repository\CarlClientRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CarlConfigurationLogistiqueCrudController extends AbstractCrudController
{
    public function __construct(private CarlClientRepository $carlClientRepository)
    {
    }

    public static function getEntityFqcn(): string
    {
        return CarlConfigurationLogistique::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $availableCarlClients = $this->carlClientRepository->findAll();
        $carlClientsChoices = [];
        foreach ($availableCarlClients as $carlClient) {
            $carlClientsChoices[$carlClient->getTitle()] = $carlClient;
        }

        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('title'),
            ChoiceField::new('carlClient')
                ->allowMultipleChoices(false)
                ->setChoices($carlClientsChoices)
                ->hideOnDetail()->hideOnIndex(),
        ];
    }
}
