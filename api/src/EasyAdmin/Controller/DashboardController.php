<?php

namespace App\EasyAdmin\Controller;

use App\ElusServiceOffer\ElusServiceOffer\Entity\ElusServiceOffer;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Ged\Entity\AlfrescoGedClient;
use App\Shared\Ged\Entity\MezzoteamV2GedClient;
use App\Shared\Message\Entity\Message;
use App\Shared\SiilabConfiguration\Entity\SiilabConfiguration;
use App\Shared\User\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle("Panneau d'administration");
    }

    public function configureMenuItems(): iterable
    {
        $listIcon = 'fas fa-list';
        yield MenuItem::linkToCrud('Utilisateurs', $listIcon, User::class);
        yield MenuItem::linkToCrud('Messages', $listIcon, Message::class);
        yield MenuItem::linkToCrud('Siilab Configuration', $listIcon, SiilabConfiguration::class);
        yield MenuItem::section('Intervention');
        yield MenuItem::linkToCrud('ODS Intervention', $listIcon, InterventionServiceOffer::class);
        yield MenuItem::linkToCrud('Configuration Carl', $listIcon, CarlConfigurationIntervention::class);
        yield MenuItem::section('Logistique');
        yield MenuItem::linkToCrud('ODS Logistique', $listIcon, LogisticServiceOffer::class);
        yield MenuItem::linkToCrud('Configuration Carl', $listIcon, CarlConfigurationLogistique::class);
        yield MenuItem::section('POC');
        yield MenuItem::linkToCrud('ODS Elus', $listIcon, ElusServiceOffer::class);
        yield MenuItem::section('Services');
        yield MenuItem::linkToCrud('Client - Carl', $listIcon, CarlClient::class);
        yield MenuItem::linkToCrud('Client - Ged Alfresco', $listIcon, AlfrescoGedClient::class);
        yield MenuItem::linkToCrud('Client - Ged Mezzoteam (API V2)', $listIcon, MezzoteamV2GedClient::class);
    }
}
