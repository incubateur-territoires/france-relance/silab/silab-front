<?php

namespace App\EasyAdmin\Controller;

use App\InterventionServiceOffer\Gmao\Repository\GmaoConfigurationInterventionRepository;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\DateDeDébut;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class InterventionCrudController extends AbstractCrudController
{
    public function __construct(private GmaoConfigurationInterventionRepository $gmaoConfigurationInterventionRepository)
    {
    }

    public static function getEntityFqcn(): string
    {
        return InterventionServiceOffer::class;
    }

    public function configureFields(string $pageName): iterable
    {
        // configurations gmao disponibles
        $gmaoConfigurationInterventions = $this->gmaoConfigurationInterventionRepository->findAll();
        $gmaoConfigurationInterventionsChoice = [];
        foreach ($gmaoConfigurationInterventions as $gmaoConfigurationIntervention) {
            $gmaoConfigurationInterventionsChoice[$gmaoConfigurationIntervention->getTitle()] = $gmaoConfigurationIntervention;
        }

        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('notes'),
            TextField::new('title'),
            TextField::new('environnement'),
            TextField::new('link'),
            TextField::new('image'),
            ChoiceField::new('gmaoConfiguration')
                ->allowMultipleChoices(false)
                ->setChoices($gmaoConfigurationInterventionsChoice)
                ->hideOnDetail()
                ->hideOnIndex(),
            TextField::new('gmaoConfiguration')->hideOnForm(),
            ArrayField::new('availableEquipementsIds')
                ->hideOnDetail()
                ->hideOnIndex(),
            ArrayField::new('availableEquipements')
                ->hideOnForm(),
            BooleanField::new('gestionRecompletementActif'),
            ChoiceField::new('configurationFormulaireActivite.dateDeDebut')->setChoices(DateDeDébut::cases())
                ->hideOnDetail()
                ->hideOnIndex(),
            NumberField::new('configurationFormulaireActivite.heureParDefautEnMinute'),
        ];
    }
}
