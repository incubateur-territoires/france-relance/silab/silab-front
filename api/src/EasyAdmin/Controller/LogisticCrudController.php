<?php

namespace App\EasyAdmin\Controller;

use App\LogisticServiceOffer\Gmao\Repository\GmaoConfigurationLogistiqueRepository;
use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class LogisticCrudController extends AbstractCrudController
{
    public function __construct(private GmaoConfigurationLogistiqueRepository $gmaoConfigurationRepository)
    {
    }

    public static function getEntityFqcn(): string
    {
        return LogisticServiceOffer::class;
    }

    public function configureFields(string $pageName): iterable
    {
        // configurations gmao disponibles
        $gmaoConfigurations = $this->gmaoConfigurationRepository->findAll();
        $gmaoConfigurationChoice = [];
        foreach ($gmaoConfigurations as $gmaoConfiguration) {
            $gmaoConfigurationChoice[$gmaoConfiguration->getTitle()] = $gmaoConfiguration;
        }

        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('notes'),
            TextField::new('title'),
            TextField::new('environnement'),
            TextField::new('link'),
            TextField::new('image'),
            TextField::new('warehouseId'),
            TextField::new('typeDInventaireId'),
            ChoiceField::new('gmaoConfiguration')
                ->allowMultipleChoices(false)
                ->setChoices($gmaoConfigurationChoice)
                ->hideOnDetail()
                ->hideOnIndex(),
            TextField::new('gmaoConfiguration')->hideOnForm(),
        ];
    }
}
