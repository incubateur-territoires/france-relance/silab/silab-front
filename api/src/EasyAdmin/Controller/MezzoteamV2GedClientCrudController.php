<?php

namespace App\EasyAdmin\Controller;

use App\Shared\Ged\Entity\MezzoteamV2GedClient;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class MezzoteamV2GedClientCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return MezzoteamV2GedClient::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('title'),
            TextField::new('instanceUrl'),
            TextField::new('username'),
            TextField::new('password')
                ->hideOnIndex()
                ->hideOnDetail(),
        ];
    }
}
