<?php

namespace App\ElusServiceOffer\Dashboard\Entity;

use ApiPlatform\Metadata\Get;
use App\ElusServiceOffer\Dashboard\State\GetDataAnalysisDataHandler;

#[Get(
    uriTemplate: '/elus-service-offers/{serviceOfferId}/data-analysis/{id}',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_ELUS_CONSULTER_STATISTIQUES')",
    provider: GetDataAnalysisDataHandler::class
)]
class DataAnalysis
{
    public function __construct(
        private ?int $id = null,
        private ?string $url = null,
    ) {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
