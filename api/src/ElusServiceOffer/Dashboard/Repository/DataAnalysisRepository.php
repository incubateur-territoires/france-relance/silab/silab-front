<?php

namespace App\ElusServiceOffer\Dashboard\Repository;

use App\ElusServiceOffer\Dashboard\Entity\DataAnalysis;
use App\ElusServiceOffer\Shared\Metabase\MetabaseClient;

class DataAnalysisRepository
{
    public function __construct(private MetabaseClient $metabaseClient)
    {
    }

    /**
     * @param array<mixed> $lockedParameters
     */
    public function find(int $id, array $lockedParameters): DataAnalysis
    {
        $url = $this->metabaseClient->getDashboardIframeUrl(
            dashboardId: $id,
            expirationMinutes: 48 * 60,
            lockedParameters: $lockedParameters
        );

        return new DataAnalysis($id, $url);
    }
}
