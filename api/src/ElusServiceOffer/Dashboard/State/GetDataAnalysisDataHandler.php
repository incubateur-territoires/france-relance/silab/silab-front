<?php

namespace App\ElusServiceOffer\Dashboard\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\ElusServiceOffer\Dashboard\Entity\DataAnalysis;
use App\ElusServiceOffer\Dashboard\Repository\DataAnalysisRepository;
use App\ElusServiceOffer\ElusServiceOffer\Repository\ElusServiceOfferRepository;
use App\ElusServiceOffer\Shared\Metabase\MetabaseClient;
use App\Shared\Security\Role\ScopedRoleHierarchyInterface;
use App\Shared\User\Service\CurrentUserContextInterface;

final class GetDataAnalysisDataHandler implements ProviderInterface
{
    public function __construct(
        private ElusServiceOfferRepository $elusServiceOfferRepository,
        private CurrentUserContextInterface $currentUserContext,
        private ScopedRoleHierarchyInterface $scopeRoleHierarchy
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): DataAnalysis
    {
        assert($operation instanceof Get);

        $currentUser = $this->currentUserContext->getCurrentUser();

        $serviceOfferId = $uriVariables['serviceOfferId'];

        $metabaseClient = new MetabaseClient($this->elusServiceOfferRepository->find($serviceOfferId));

        $currentUser->setReachableRoles($this->scopeRoleHierarchy->getReachableRoleNames($currentUser->getRoles()));

        $isAdmin = in_array('SERVICEOFFER_'.$serviceOfferId.'_ROLE_SERVICEOFFER_ADMIN', $currentUser->getReachableRoles());

        $roleAccesTerritoireRegex = '/^SERVICEOFFER_'.$serviceOfferId.'_ROLE_ELUS_SUPERVISER_TERRITOIRE_(?<territoire>.+)$/';

        $territoiresPourCloisonnement = [
            'CROUTELLE' => 'Croutelle',
            'BUXEROLLES' => 'Buxerolles',
            'BEAUMONT_ST-CYR' => 'Beaumont Saint-Cyr',
            'BERUGES' => 'Béruges',
            'BIARD' => 'Biard',
            'BIGNOUX' => 'Bignoux',
            'BONNES' => 'Bonnes',
            'CELLE-LEVESCAULT' => 'Celle-Lévescault',
            'CHASSENEUIL_DU_POITOU' => 'Chasseneuil-du-Poitou',
            'CHAUVIGNY' => 'Chauvigny',
            'CLOUÉ' => 'Cloué',
            'CLOUE' => 'Cloué',
            'COULOMBIERS' => 'Coulombiers',
            'CURZAY-SUR-VONNE' => 'Curzay-sur-Vonne',
            'DISSAY' => 'Dissay',
            'FONTAINE-LE-COMTE' => 'Fontaine-le-Comte',
            'JARDRES' => 'Jardres',
            'JAUNAY-MARIGNY' => 'Jaunay-Marigny',
            'JAZENEUIL' => 'Jazeneuil',
            'LA_CHAPELLE-MOULIERE' => 'La Chapelle-Moulière',
            'LA_PUYE' => 'La Puye',
            'LAVOUX' => 'Lavoux',
            'LIGUGE' => 'Ligugé',
            'LINIERS' => 'Liniers',
            'LUSIGNAN' => 'Lusignan',
            'MIGNALOUX-BEAUVOIR' => 'Mignaloux-Beauvoir',
            'MIGNE-AUXANCES' => 'Migné-Auxances',
            'MONTAMISE' => 'Montamisé',
            'POUILLE' => 'Pouillé',
            'ROUILLE' => 'Rouillé',
            'SAINT-BENOIT' => 'Saint-Benoit',
            'SAINTE-RADEGONDE' => 'Sainte-Radegonde',
            'SAINT-GEORGES' => 'Saint-Georges-Les-Baillargeaux',
            'SAINT-JULIEN-LARS' => "Saint-Julien-l'Ars",
            'SAINT-SAUVANT' => 'Saint-Sauvant',
            'SANXAY' => 'Sanxay',
            'SAVIGNY-LEVESCAULT' => 'Savigny-Lévescault',
            'SEVRES-ANXAUMONT' => 'Sèvres-Anxaumont',
            'SMARVES' => 'Smarves',
            'TERCE' => 'Tercé',
            'VOUNEUIL-SOUS-BIARD' => 'Vouneuil-sous-Biard',
        ];

        $lockedParameters = ['commune' => []];

        if ($isAdmin) {
            $hasTerritoryRole = false;

            foreach ($currentUser->getRoles() as $role) {
                if (preg_match($roleAccesTerritoireRegex, $role, $matches)) {
                    $hasTerritoryRole = true;
                    $lockedParameters['commune'][] = $territoiresPourCloisonnement[$matches['territoire']];
                }
            }

            if (!$hasTerritoryRole) {
                $lockedParameters = [];
            }
        } else {
            foreach ($currentUser->getRoles() as $role) {
                if (preg_match($roleAccesTerritoireRegex, $role, $matches)) {
                    $lockedParameters['commune'][] = $territoiresPourCloisonnement[$matches['territoire']];
                }
            }

            if (empty($lockedParameters['commune'])) {
                $lockedParameters = [null];
            }
        }

        $dataAnalysisRepository = new DataAnalysisRepository($metabaseClient);

        return $dataAnalysisRepository->find($uriVariables['id'], $lockedParameters);
    }
}
