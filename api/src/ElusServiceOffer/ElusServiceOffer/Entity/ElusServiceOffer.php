<?php

namespace App\ElusServiceOffer\ElusServiceOffer\Entity;

use App\ElusServiceOffer\ElusServiceOffer\Repository\ElusServiceOfferRepository;
use App\ServiceOffer\Role\ValueObject\Role;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ElusServiceOfferRepository::class)]
class ElusServiceOffer extends ServiceOffer
{
    /** @var array<string,mixed>|null $configuration */
    #[ORM\Column(nullable: true)]
    private ?array $configuration = null;

    /**
     * @return array<string,mixed>|null
     */
    public function getConfiguration(): ?array
    {
        return $this->configuration;
    }

    /**
     * @param array<string,mixed>|null $configuration
     */
    public function setConfiguration(?array $configuration): self
    {
        $this->configuration = $configuration;

        return $this;
    }

    public function getTemplate(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return array<Role>
     */
    public function getAvailableRoles(): array
    {
        $availableRoles = [
            new Role(value: 'SERVICEOFFER_'.$this->getId().'_ROLE_ELUS_ELU', libelle: 'Elu', description: "Peut accéder à l'offre de service"),
        ];

        // On propose dynamiquement un rôle pour chaque territoire disponible
        foreach ($this->getTerritoires() as $territoire) {
            $availableRoles[] = new Role(
                value: 'SERVICEOFFER_'.$this->getId().'_ROLE_ELUS_SUPERVISER_TERRITOIRE_'.$territoire,
                libelle: "Territoire : $territoire",
                description: "Peut consulter les interventions du territoire $territoire"
            );
        }

        return array_merge(parent::getAvailableRoles(), $availableRoles);
    }

    /**
     * @return array<string>
     */
    public function getTerritoires(): array
    {
        return $this->getConfiguration()['territoires'];
    }
}
