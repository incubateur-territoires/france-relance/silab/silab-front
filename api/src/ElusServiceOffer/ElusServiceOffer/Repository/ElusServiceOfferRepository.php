<?php

namespace App\ElusServiceOffer\ElusServiceOffer\Repository;

use App\ElusServiceOffer\ElusServiceOffer\Entity\ElusServiceOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ElusServiceOffer>
 *
 * @method ElusServiceOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method ElusServiceOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method ElusServiceOffer[]    findAll()
 * @method ElusServiceOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ElusServiceOfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ElusServiceOffer::class);
    }

    //    /**
    //     * @return ElusServiceOffer[] Returns an array of ElusServiceOffer objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('e.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ElusServiceOffer
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
