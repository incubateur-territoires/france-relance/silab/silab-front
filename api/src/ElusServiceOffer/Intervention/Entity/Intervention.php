<?php

namespace App\ElusServiceOffer\Intervention\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\ElusServiceOffer\Intervention\State\GetCollectionInterventionDataHandler;
use App\ElusServiceOffer\Intervention\State\GetInterventionDataHandler;
use App\ElusServiceOffer\Service\ReverseGeocoding;
use App\Shared\ApiPlatform\Filter\SearchFilter;
use App\Shared\Location\Entity\Coordinates;
use App\Shared\Location\Exception\AddressNotFoundException;
use Symfony\Component\Serializer\Annotation\Groups;

#[GetCollection(
    uriTemplate: '/elus-service-offers/{serviceOfferId}/interventions',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_ELUS_CONSULTER_INTERVENTIONS')",
    provider: GetCollectionInterventionDataHandler::class,
    normalizationContext: ['groups' => [self::GROUP_LISTER_INTERVENTIONS]]
)]
#[Get()]
#[Get(
    uriTemplate: '/elus-service-offers/{serviceOfferId}/interventions/{id}',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_ELUS_CONSULTER_INTERVENTIONS')",
    provider: GetInterventionDataHandler::class,
    normalizationContext: ['groups' => [self::GROUP_LISTER_INTERVENTIONS, self::GROUP_DETAILS_INTERVENTION]]
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'beginStatusChangedAt' => 'exact',
        'beginCreatededAt' => 'exact',
        'endStatusChangedAt' => 'exact',
        'endCreatededAt' => 'exact',
        'statuses' => 'exact',
        'regroupment' => 'exact',
    ]
)]
class Intervention
{
    public const GROUP_LISTER_INTERVENTIONS = 'Lister les interventions';
    public const GROUP_DETAILS_INTERVENTION = "Afficher les détails d'une intervention";

    public function __construct(
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $id = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $code = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $title = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $currentStatus = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $regroupment = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?Coordinates $coordinates = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $address = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?\DateTime $createdAt = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $imageUrl = null,
        #[Groups([self::GROUP_DETAILS_INTERVENTION])]
        private ?\DateTime $beginDate = null,
        #[Groups([self::GROUP_DETAILS_INTERVENTION])]
        private ?\DateTime $endDate = null,
        #[Groups([self::GROUP_DETAILS_INTERVENTION])]
        private ?string $comment = null,
        #[Groups([self::GROUP_DETAILS_INTERVENTION])]
        private ?string $createdBy = null,
        /** @var ?array<mixed> $history */
        #[Groups([self::GROUP_DETAILS_INTERVENTION])]
        private ?array $history = [],
        #[Groups([self::GROUP_DETAILS_INTERVENTION])]
        private ?bool $isCurrentStatus = null,
        #[Groups([self::GROUP_DETAILS_INTERVENTION])]
        private ?string $linkToCarl = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?\DateTime $statusChangedAt = null,
        #[Groups([self::GROUP_DETAILS_INTERVENTION])]
        private ?string $localisation = null
    ) {
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setcode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCurrentStatus(): ?string
    {
        return $this->currentStatus;
    }

    public function setCurrentStatus(string $currentStatus): self
    {
        $this->currentStatus = $currentStatus;

        return $this;
    }

    public function getRegroupment(): ?string
    {
        return $this->regroupment;
    }

    public function setRegroupment(string $regroupment): self
    {
        $this->regroupment = $regroupment;

        return $this;
    }

    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }

    public function setCoordinates(?Coordinates $coordinates): self
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getBeginDate(): ?\DateTime
    {
        return $this->beginDate;
    }

    public function setBeginDate(\DateTime $beginDate): self
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTime $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return ?array<mixed>
     */
    public function getHistory(): ?array
    {
        return $this->history;
    }

    /**
     * @param ?array<mixed> $history
     *
     * @return $this
     */
    public function setHistory(?array $history): self
    {
        $this->history = $history;

        return $this;
    }

    public function getLinkToCarl(): ?string
    {
        return $this->linkToCarl;
    }

    public function setLinkToCarl(?string $linkToCarl): self
    {
        $this->linkToCarl = $linkToCarl;

        return $this;
    }

    public function getIsCurrentStatus(): ?bool
    {
        return $this->isCurrentStatus;
    }

    public function setIsCurrentStatus(?bool $isCurrentStatus): self
    {
        $this->isCurrentStatus = $isCurrentStatus;

        return $this;
    }

    public function setStatusChangedAt(\DateTime $statusChangedAt): self
    {
        $this->statusChangedAt = $statusChangedAt;

        return $this;
    }

    public function getStatusChangedAt(): ?\DateTime
    {
        return $this->statusChangedAt;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(?string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function setLocalisationFromCoordinates(): self
    {
        $latitude = $this->coordinates->getLat();
        $longitude = $this->coordinates->getLng();

        try {
            $reverseGeocoding = new ReverseGeocoding();
            $address = $reverseGeocoding->getAddressFromCoordinates(
                $latitude,
                $longitude
            );
        } catch (AddressNotFoundException $e) {
            $address = '';
        }

        $this->setLocalisation($address);

        return $this;
    }
}
