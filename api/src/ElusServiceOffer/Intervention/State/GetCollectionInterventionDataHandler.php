<?php

namespace App\ElusServiceOffer\Intervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\ElusServiceOffer\ElusServiceOffer\Repository\ElusServiceOfferRepository;
use App\ElusServiceOffer\Intervention\Entity\Intervention;
use App\ElusServiceOffer\Intervention\Repository\InterventionRepository;
use App\Shared\Security\Role\ScopedRoleHierarchyInterface;
use App\Shared\User\Service\CurrentUserContextInterface;

final class GetCollectionInterventionDataHandler implements ProviderInterface
{
    public function __construct(
        private ElusServiceOfferRepository $elusServiceOfferRepository,
        private CurrentUserContextInterface $currentUserContext,
        private ScopedRoleHierarchyInterface $scopeRoleHierarchy,
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<Intervention>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);
        $criteria = $context['filters'] ?? [];

        $elusServiceOffer = $this->elusServiceOfferRepository->find($uriVariables['serviceOfferId']);

        $interventionRepository = new InterventionRepository(
            $elusServiceOffer,
        );

        $currentUser = $this->currentUserContext->getCurrentUser();

        $currentUserRoles = $this->scopeRoleHierarchy->getReachableRoleNames($currentUser->getRoles());

        $roleAccesTerritoireRegex = '/^SERVICEOFFER_'.$uriVariables['serviceOfferId'].'_ROLE_ELUS_SUPERVISER_TERRITOIRE_(?<territoire>.+)$/';

        // on extrait les territoires autorisés depuis les rôles de l'utilisateur
        $territoiresPourCloisonnement = array_reduce(
            $currentUserRoles,
            function ($territoiresDéjàAutorisés, $roleActuel) use ($roleAccesTerritoireRegex) {
                if (preg_match($roleAccesTerritoireRegex, $roleActuel, $regexTerritoireConcernéMatches)) {
                    $territoiresDéjàAutorisés[] = $regexTerritoireConcernéMatches['territoire'];
                }

                return $territoiresDéjàAutorisés;
            },
            []
        );

        $criteria['city'] = $territoiresPourCloisonnement;

        return $interventionRepository->findAll($criteria);
    }
}
