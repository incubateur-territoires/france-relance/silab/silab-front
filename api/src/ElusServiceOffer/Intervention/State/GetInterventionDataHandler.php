<?php

namespace App\ElusServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\ElusServiceOffer\ElusServiceOffer\Repository\ElusServiceOfferRepository;
use App\ElusServiceOffer\Intervention\Entity\Intervention;
use App\ElusServiceOffer\Intervention\Repository\InterventionRepository;

final class GetInterventionDataHandler implements ProviderInterface
{
    public function __construct(
        private ElusServiceOfferRepository $elusServiceOfferRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): Intervention
    {
        $elusServiceOffer = $this->elusServiceOfferRepository->find($uriVariables['serviceOfferId']);

        $interventionRepository = new InterventionRepository(
            $elusServiceOffer,
        );

        return $interventionRepository->find($uriVariables['id']);
    }
}
