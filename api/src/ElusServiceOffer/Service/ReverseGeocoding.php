<?php

namespace App\ElusServiceOffer\Service;

use App\Shared\Exception\RuntimeException;
use App\Shared\Location\Exception\AddressNotFoundException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ReverseGeocoding
{
    private HttpClientInterface $httpClient;

    public function __construct()
    {
        $this->httpClient = HttpClient::create();
    }

    /**
     * Récupère l'adresse à partir des coordonnées en utilisant l'API de géocodage inversé de l'IGN.
     *
     * @param float $latitude  La coordonnée de latitude
     * @param float $longitude La coordonnée de longitude
     *
     * @return string L'adresse correspondant aux coordonnées fournies
     *
     * @throws AddressNotFoundException Si aucune adresse n'est trouvée pour les coordonnées spécifiées
     * @throws RuntimeException         Si une erreur survient lors de la récupération de l'adresse
     */
    public function getAddressFromCoordinates(float $latitude, float $longitude): string
    {
        $geoCodingApiUrl = $_ENV['GEOPORTAIL_GEOCODING_API_URL'];

        $queryParams = [
            'lat' => $latitude,
            'lon' => $longitude,
            'index' => 'address',
            'limit' => 1,
        ];

        $apiUrlWithParams = $geoCodingApiUrl.'?'.http_build_query($queryParams);

        try {
            $response = $this->httpClient->request('GET', $apiUrlWithParams);
            $responseData = $response->toArray();
        } catch (\Exception $e) {
            return '';
            // Note importante : nous commentons cette exception en hotfix car nous rencontrons des problèmes, visiblement de restriction de nombre d'appels/minute sur l'api https://wxs.ign.fr/essentiels/geoportail/geocodage/rest/0.1/reverse qui bloque notre CI
            // throw new RuntimeException("Une erreur est survenue lors de la récupération de l'adresse", 0, $e);
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $address = $propertyAccessor->getValue($responseData, '[features][0][properties][label]');

        if (is_null($address)) {
            throw new AddressNotFoundException('Aucune adresse trouvée pour les coordonnées spécifiées');
        }

        return $address;
    }
}
