<?php

namespace App\ElusServiceOffer\Shared\Oracle;

use App\ElusServiceOffer\ElusServiceOffer\Entity\ElusServiceOffer;
use App\Shared\Exception\RuntimeException;

class OraclePDOFactory
{
    public static function fromElusServiceOffer(ElusServiceOffer $elusServiceOffer): \PDO
    {
        $dbConfiguration = $elusServiceOffer->getConfiguration()['database']['configuration'];
        $dsn = 'oci:dbname='.$dbConfiguration['tns'].';charset=utf8';
        $username = $dbConfiguration['username'];
        $password = $dbConfiguration['password'];

        try {
            $pdo = new \PDO($dsn, $username, $password);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            return $pdo;
        } catch (\PDOException $e) {
            throw new RuntimeException('Erreur de connexion : '.$e->getMessage(), 500);
        }
    }
}
