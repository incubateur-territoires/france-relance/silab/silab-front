<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteAction;
use App\InterventionServiceOffer\Intervention\Entity\Activite\Operation;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlActiviteActionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlActiviteActionRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlActiviteAction implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\ManyToOne()]
        #[ORM\JoinColumn(nullable: true)]
        private ?DbCarlGmaoActeur $createdBy,
        #[ORM\Column(length: 250, nullable: true)]
        private ?string $comment,
        #[ORM\ManyToOne()]
        #[ORM\JoinColumn(nullable: true)]
        private ?DbCarlEquipement $equipement,
        #[ORM\Column(nullable: true)]
        private ?\DateTimeImmutable $dateDebut,
        #[ORM\Column(precision: 10)]
        private ?int $ordering,
        #[ORM\Column(length: 33, nullable: true)]
        private ?string $processId,
        #[ORM\Column(length: 60, nullable: true)]
        private ?string $processLabel,
        #[ORM\Column()]
        private bool $isPlanned,
        #[ORM\Column()]
        private bool $isMandatory,
        #[ORM\Column(length: 33, nullable: true)]
        private ?string $interventionId,
        #[ORM\Column()]
        private bool $isDone,
        #[ORM\ManyToOne()]
        private ?DbCarlGmaoActeur $technician,
        #[ORM\Column(nullable: true)]
        private ?float $duree,
        #[ORM\Column(length: 60)]
        private string $type
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_activite_action"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
    CREATE FOREIGN TABLE $dbCarlTableName (
                id                          varchar(33)           OPTIONS (key 'true')  NOT NULL,
                comment                     varchar(255),
                equipement_id               varchar(33),
                date_debut                  timestamptz(3),
                ordering                    numeric(10,0),
                process_id                  varchar(33),
                process_label               varchar(60),
                is_planned                  boolean,
                is_mandatory                boolean,
                intervention_id             varchar(33),
                is_done                     boolean,
                technician_id               varchar(33),
                duree                       numeric,
                created_by_id               varchar(33),
                type                        varchar(60)
           ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with
                WOPROCESS as (
                    select
                        OPERATION.ID as ID,
                        OPERATION."COMMENT" as "COMMENT",
                        COALESCE(OPERATION.MATERIAL_ID, OPERATION.BOX_ID) as EQUIPMENT_ID,
                        FROM_TZ(OPERATION.REALISEDDATE, ''GMT'') as DATE_DEBUT,--attention, carl stocke en GMT et pas sur le fuseau de la DB
                        OPERATION.ORDERING as ORDERING,
                        PROCESS.ID as PROCESS_ID,
                        PROCESS.DESCRIPTION as PROCESS_LABEL,
                        case when OPERATION.EXPWOPROCESSBEAN_ID is not null then 1 else 0 end as IS_PLANNED,
                        OPERATION.MANDATORY as IS_MANDATORY,
                        OPERATION.WO_ID as WO_ID,
                        case when OPERATION.REALISEDDATE is not null then 1 else 0 end as IS_DONE,
                        TECHNICIAN.ACTOR_ID as ACTOR_ID,
                        (OPERATION.DURATION * 60) as DUREE,
                        ACTOR.ID as CREATED_BY_ID,
                        ''woprocess'' as "TYPE"
                    from
                        "$dbSchema".CSWO_WOPROCESS OPERATION
                        left join "$dbSchema".CSWO_PROCESS PROCESS on PROCESS.ID = OPERATION.PROCESS_ID
                        left join "$dbSchema".CSRE_TECHNICIAN TECHNICIAN on TECHNICIAN.ID = OPERATION.TECHNICIAN_ID
                        left join "$dbSchema".CSSY_ACTOR ACTOR on ACTOR.CODE = OPERATION.UOWNER
                    where PROCESS.PROCESSCLASS != ''DIAG''
                ),
                OCCUPATION as (
                    select
                        OCCUPATION.ID as ID,
                        null as "COMMENT",
                        null as EQUIPEMENT_ID,
                        FROM_TZ(OCCUPATION.OCCUPATIONDATE, ''GMT'') as DATE_DEBUT,--attention, carl stocke en GMT et pas sur le fuseau de la DB
                        null as ORDERING,
                        null as PROCESS_ID,
                        null as PROCESS_LABEL,
                        0 as IS_PLANNED,
                        0 as IS_MANDATORY,
                        OCCUPATION.WO_ID as WO_ID,
                        case when OCCUPATION.OCCUPATIONDATE is not null then 1 else 0 end as IS_DONE,
                        TECHNICIAN.ACTOR_ID as ACTOR_ID,
                        (OCCUPATION.DURATION * 60) as DUREE,
                        ACTOR.ID as CREATED_BY,
                        ''occupation'' as "TYPE"

                    from
                        "$dbSchema".CSWO_OCCUPATION OCCUPATION
                        left join "$dbSchema".CSRE_TECHNICIAN TECHNICIAN on TECHNICIAN.ID = OCCUPATION.TECHNICIAN_ID
                        left join "$dbSchema".CSSY_ACTOR ACTOR on ACTOR.CODE = OCCUPATION.UOWNER
                    where OCCUPATION.WOPROCESS_ID is null
                )
            select
                * from WOPROCESS
            union
            select
                * from OCCUPATION
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCreatedBy(): ?DbCarlGmaoActeur
    {
        return $this->createdBy;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    public function getRealisedDate(): ?\DateTimeImmutable
    {
        return $this->dateDebut;
    }

    public function getEquipement(): ?DbCarlEquipement
    {
        return $this->equipement;
    }

    public function isPlanned(): bool
    {
        return $this->isPlanned;
    }

    public function isMandatory(): bool
    {
        return $this->isMandatory;
    }

    public function getProcessId(): ?string
    {
        return $this->processId;
    }

    public function getProcessLabel(): ?string
    {
        return $this->processLabel;
    }

    public function getInterventionId(): ?string
    {
        return $this->interventionId;
    }

    public function isDone(): bool
    {
        return $this->isDone();
    }

    public function getTechnician(): ?DbCarlGmaoActeur
    {
        return $this->technician;
    }

    public function getDuree(): ?float
    {
        return $this->duree;
    }

    /**
     * entité carl : woprocess || occupation.
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function toActiviteAction(
        InterventionServiceOffer $serviceOffer,
        CarlConfigurationIntervention $gmaoCarl
    ): ActiviteAction {
        return new ActiviteAction(
            id: $this->getId(),
            createdBy: $this->getCreatedBy()?->toGmaoActeur(),
            description: $this->getComment() ?? $this->getProcessLabel(),
            operation: is_null($this->getProcessId()) ? null : new Operation(
                id: $this->getProcessId(),
                label: $this->getProcessLabel() ?? ''
            ),
            equipementCible: $this->getEquipement()?->toEquipement(serviceOffer: $serviceOffer, gmaoCarl: $gmaoCarl),
            isPlanifie: $this->isPlanned(),
            isObligatoire: $this->isMandatory(),
            dateDeDebut: $this->getRealisedDate(),
            intervenant: $this->getTechnician()?->toGmaoActeur(),
            duree: is_null($this->getDuree()) ? null : intval(round($this->getDuree()))
        );
    }

    /**
     * @param array<DbCarlActiviteAction> $dbCarlActiviteActions
     *
     * @return array<ActiviteAction>
     */
    public static function toActiviteActionArray(
        array $dbCarlActiviteActions,
        InterventionServiceOffer $serviceOffer,
        CarlConfigurationIntervention $gmaoCarl
    ): array {
        return array_map(
            function (DbCarlActiviteAction $dbCarlActiviteAction) use ($serviceOffer, $gmaoCarl) {
                return $dbCarlActiviteAction->toActiviteAction($serviceOffer, $gmaoCarl);
            },
            $dbCarlActiviteActions
        );
    }
}
