<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\Carl\Exception\OperationProcessIdIsNullException;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteDiagnostique;
use App\InterventionServiceOffer\Intervention\Entity\Activite\Operation;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlActiviteActionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlActiviteActionRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlActiviteDiagnostique implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\ManyToOne()]
        #[ORM\JoinColumn(nullable: true)]
        private ?DbCarlGmaoActeur $createdBy,
        #[ORM\Column(length: 250)]
        private ?string $comment,
        #[ORM\ManyToOne()]
        #[ORM\JoinColumn(nullable: true)]
        private ?DbCarlEquipement $equipement,
        #[ORM\Column()]
        private ?\DateTimeImmutable $realisedDate,
        #[ORM\Column(precision: 10)]
        private int $ordering,
        #[ORM\Column(length: 33)]
        private ?string $processId,
        #[ORM\Column(length: 60)]
        private ?string $processLabel,
        #[ORM\Column()]
        private bool $isPlanned,
        #[ORM\Column()]
        private bool $isMandatory,
        #[ORM\Column(length: 33)]
        private ?string $interventionId,
        #[ORM\Column()]
        private bool $isDone,
        #[ORM\OneToOne(targetEntity: DbCarlDemandeIntervention::class, inversedBy: 'dbCarlActiviteDiagnostique')]
        #[ORM\JoinColumn(nullable: true)]
        private ?DbCarlDemandeIntervention $demandeInterventionGeneree,
        #[ORM\Column(length: 33)]
        private ?string $interventionIdGeneree,
        #[ORM\Column(length: 33)]
        private ?string $evaluationId,
        #[ORM\ManyToOne()]
        private ?DbCarlGmaoActeur $technician,
        #[ORM\Column(nullable: true)]
        private ?int $duree,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_activite_diagnostique"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
    CREATE FOREIGN TABLE $dbCarlTableName (
                id                                  varchar(33)           OPTIONS (key 'true')  NOT NULL,
                comment                             varchar(255),
                equipement_id                       varchar(33),
                realised_date                       timestamptz(3),
                ordering                            numeric(10,0),
                process_id                          varchar(33),
                process_label                       varchar(60),
                is_planned                          boolean,
                is_mandatory                        boolean,
                intervention_id                     varchar(33),
                is_done                             boolean,
                demande_intervention_generee_id     varchar(33),
                intervention_id_generee             varchar(33),
                evaluation_id                       varchar(60),
                technician_id                       varchar(33),
                duree                               numeric,
                created_by_id                       varchar(33)
           ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            select
                OPERATION.ID,
                OPERATION."COMMENT",
                COALESCE(OPERATION.MATERIAL_ID, OPERATION.BOX_ID) as EQUIPMENT_ID,
                FROM_TZ(OPERATION.REALISEDDATE, ''GMT'') as REALISEDDATE,
                OPERATION.ORDERING,
                PROCESS.ID as PROCESS_ID,
                PROCESS.DESCRIPTION as PROCESS_LABEL ,
                case when OPERATION.EXPWOPROCESSBEAN_ID is not null then 1 else 0 end as IS_PLANNED,
                OPERATION.MANDATORY as IS_MANDATORY,
                OPERATION.WO_ID,
                case when OPERATION.REALISEDDATE is not null then 1 else 0 end as IS_DONE,
                OPERATION.CREATEDMR_ID,
                OPERATION.CREATEDWO_ID,
                EVALUATION.ID as EVALUATION,
                TECHNICIAN.ACTOR_ID as ACTOR_ID,
                (OPERATION.DURATION * 60) as DUREE,
                ACTOR.ID as CREATED_BY_ID

            from "$dbSchema".CSWO_WOPROCESS OPERATION
            left join "$dbSchema".CSWO_PROCESS PROCESS on PROCESS.ID = OPERATION.PROCESS_ID
            left join "$dbSchema".CSEQ_EVALUATION EVALUATION on EVALUATION.ID = OPERATION.EVALUATION_ID
            left join "$dbSchema".CSRE_TECHNICIAN TECHNICIAN on TECHNICIAN.ID = OPERATION.TECHNICIAN_ID
            left join "$dbSchema".CSSY_ACTOR ACTOR on ACTOR.CODE = OPERATION.UOWNER
            where PROCESS.PROCESSCLASS = ''DIAG''
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCreatedBy(): ?DbCarlGmaoActeur
    {
        return $this->createdBy;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getOrdering(): int
    {
        return $this->ordering;
    }

    public function getRealisedDate(): ?\DateTimeImmutable
    {
        return $this->realisedDate;
    }

    public function getEquipement(): ?DbCarlEquipement
    {
        return $this->equipement;
    }

    public function isPlanned(): bool
    {
        return $this->isPlanned;
    }

    public function isMandatory(): bool
    {
        return $this->isMandatory;
    }

    public function getProcessId(): ?string
    {
        return $this->processId;
    }

    public function getProcessLabel(): ?string
    {
        return $this->processLabel;
    }

    public function isDone(): bool
    {
        return $this->isDone();
    }

    public function getEvaluationId(): ?string
    {
        return $this->evaluationId;
    }

    public function getInterventionIdGeneree(): ?string
    {
        return $this->interventionIdGeneree;
    }

    public function getDemandeInterventionGeneree(): ?DbCarlDemandeIntervention
    {
        return $this->demandeInterventionGeneree;
    }

    public function getTechnician(): ?DbCarlGmaoActeur
    {
        return $this->technician;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function toActiviteDiagnostique(
        InterventionServiceOffer $serviceOffer,
        CarlConfigurationIntervention $gmaoCarl
    ): ActiviteDiagnostique {
        if (is_null($this->getProcessId())) {
            throw new OperationProcessIdIsNullException($this->getId());
        }

        return new ActiviteDiagnostique(
            id: $this->getId(),
            createdBy: $this->getCreatedBy()?->toGmaoActeur(),
            description: $this->getComment() ?? $this->getProcessLabel() ?? '',
            operation: new Operation(id: $this->getProcessId(), label: $this->getProcessLabel()),
            equipementCible: $this->getEquipement()?->toEquipement(serviceOffer: $serviceOffer, gmaoCarl: $gmaoCarl),
            isPlanifie: $this->isPlanned(),
            isObligatoire: $this->isMandatory(),
            dateDeDebut: $this->getRealisedDate(),
            evaluation: $this->getEvaluationId() ? $gmaoCarl->getCarlClient()->getSilabEvaluation($this->getEvaluationId()) : null,
            interventionIdGeneree: $this->getInterventionIdGeneree(),
            demandeInterventionIdGeneree: $this->getDemandeInterventionGeneree()?->getId(),
            intervenant: $this->getTechnician()?->toGmaoActeur(),
            duree: is_null($this->getDuree()) ? null : intval(round($this->getDuree()))
        );
    }

    /**
     * @param array<DbCarlActiviteDiagnostique> $dbCarlActiviteDiagnostiques
     *
     * @return array<ActiviteDiagnostique>
     */
    public static function toActiviteDiagnostiqueArray(
        array $dbCarlActiviteDiagnostiques,
        InterventionServiceOffer $serviceOffer,
        CarlConfigurationIntervention $gmaoCarl
    ): array {
        return array_map(
            function (DbCarlActiviteDiagnostique $dbCarlActiviteDiagnostique) use ($serviceOffer, $gmaoCarl) {
                return $dbCarlActiviteDiagnostique->toActiviteDiagnostique($serviceOffer, $gmaoCarl);
            },
            $dbCarlActiviteDiagnostiques
        );
    }
}
