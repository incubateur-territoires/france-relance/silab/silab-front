<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\Equipement\Entity\Compteur\Compteur;
use App\InterventionServiceOffer\Equipement\Entity\Compteur\CompteurType;
use App\InterventionServiceOffer\Equipement\Entity\Mesure\Mesure;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlCompteurRepository;
use App\Shared\Exception\RuntimeException;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlCompteurRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlCompteur implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 20)]
        private string $code,
        #[ORM\Column(length: 60)]
        private ?string $label,
        #[ORM\Column(length: 255)]
        private ?string $unite,
        #[ORM\Column(precision: 15, scale: 4)]
        private float $valMaxi,
        #[ORM\Column(precision: 15, scale: 4)]
        private float $valMini,
        #[ORM\Column(precision: 10)]
        private int $decimale,
        #[ORM\Column(nullable: true, precision: 15, scale: 4)]
        private ?float $lastMeasureValue,
        #[ORM\Column(nullable: true, length: 255)]
        private ?string $lastMeasureCreatedBy,
        #[ORM\Column(nullable: true)]
        private ?\DateTimeImmutable $lastMeasureCreatedAt,
        #[ORM\OneToMany(targetEntity: DbCarlCompteurDbCarlEquipement::class, mappedBy: 'dbCarlCompteur')]
        private Collection $dbCarlCompteurDbCarlEquipement,
        #[ORM\Column(nullable: true, length: 20)]
        private ?string $codeModele,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_compteur"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id        varchar(33)           OPTIONS (key 'true')  NOT NULL,
            code      varchar(20),
            label  varchar(60),
            unite  varchar(255),
            last_measure_value numeric(15,4),
            last_measure_created_at timestamptz(3),
            last_measure_created_by varchar(255),
            val_mini numeric(15,4),
            val_maxi numeric(15,4),
            decimale numeric(10,0),
            code_modele varchar(20)
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
        with MEASURE_ACTOR_UNIQUE_EMAIL as (
            select PHONE.ACTOR_ID, min(PHONE.PHONENUM) as EMAIL from "$dbSchema".CSSY_PHONE PHONE where PHONE.TYPE=''EMAIL'' group by PHONE.ACTOR_ID
        )
        select
            MEASUREPOINT.ID,
            MEASUREPOINT.CODE,
            MEASUREPOINT.DESCRIPTION as LABEL,
            UNIT.DESCRIPTION as UNITE,
            MEASUREREADING.MEASURE as DERNIERE_MESURE_VALEUR,
            FROM_TZ(MEASUREREADING.DATEMEASURE, ''GMT'') as DERNIERE_MESURE_CREATEDAT,--attention, carl stocke en GMT et pas sur le fuseau de la DB
            MEASURE_ACTOR_UNIQUE_EMAIL.EMAIL as DERNIERE_MESURE_CREATEDBY,
            MEASUREPOINT.VALMINI,
            MEASUREPOINT.VALMAXI,
            MEASUREPOINT.DECIMALE,
            MEASUREPATTERN.CODE as CODE_MODELE
        from
            "$dbSchema".CSEQ_MEASUREPOINT MEASUREPOINT
            left join "$dbSchema".CSEQ_MEASUREPATTERN MEASUREPATTERN on MEASUREPATTERN.ID = MEASUREPOINT.MEASUREPATTERN_ID
            left join "$dbSchema".CSSY_UNIT UNIT on UNIT.CODE = MEASUREPOINT.UNIT
            left join "$dbSchema".CSEQ_MEASUREREADING MEASUREREADING on MEASUREREADING.ID = MEASUREPOINT.LASTREADING_ID
            left join MEASURE_ACTOR_UNIQUE_EMAIL on MEASURE_ACTOR_UNIQUE_EMAIL.ACTOR_ID = MEASUREREADING.CREATEDBY_ID
    )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function getUnite(): ?string
    {
        return $this->unite;
    }

    public function getValMaxi(): float
    {
        return $this->valMaxi;
    }

    public function getValMini(): float
    {
        return $this->valMini;
    }

    public function getDecimale(): int
    {
        return $this->decimale;
    }

    public function getLastMeasureValue(): ?float
    {
        return $this->lastMeasureValue;
    }

    public function getLastMeasureCreatedBy(): ?string
    {
        return $this->lastMeasureCreatedBy;
    }

    public function getLastMeasureCreatedAt(): ?\DateTimeImmutable
    {
        return $this->lastMeasureCreatedAt;
    }

    public function getDbCarlCompteurDbCarlEquipement(): Collection
    {
        return $this->dbCarlCompteurDbCarlEquipement;
    }

    public function getCodeModele(): ?string
    {
        return $this->codeModele;
    }

    public function toCompteur(InterventionServiceOffer $serviceOffer, CarlConfigurationIntervention $gmaoCarl): Compteur
    {
        $type = CompteurType::Flottant;

        if (0 === $this->getDecimale()) {
            $type = CompteurType::Entier;
        }

        // On se base sur le type/modèle de compteur pour savoir si c'est binaire
        // mais on vérfie quand même la bonne configuration
        if ('COMPTEUR_BINAIRE' === $this->getCodeModele()) {
            if (
                CompteurType::Entier !== $type
                || 0. !== $this->getValMini()
                || 1. !== $this->getValMaxi()
            ) {
                throw new RuntimeException('Le compteur binaire '.$this->getCode().' est mal configuré, vérifiez les valeurs authorisées.');
            }
            $type = CompteurType::Binaire;
        }

        $equipement =
                array_values(
                    array_filter(
                        $this->getDbCarlCompteurDbCarlEquipement()->toArray(),
                        function ($dbCarlCompteurDbCarlEquipement) {
                            return true === $dbCarlCompteurDbCarlEquipement->getDirectEqpt();
                        }
                    )
                )[0]->getDbCarlEquipement()->toEquipement(serviceOffer: $serviceOffer, gmaoCarl: $gmaoCarl);

        return new Compteur(
            id: $this->getId(),
            code: $this->getCode(),
            label: $this->getLabel() ?? '',
            unite: $this->getUnite() ?? '',
            equipement: $equipement,
            type: $type,
            derniereMesure: is_null($this->getLastMeasureValue()) ?
                null :
                new Mesure(
                    valeur: $this->getLastMeasureValue(),
                    createdAt: $this->getLastMeasureCreatedAt(),
                    createdBy: $this->getLastMeasureCreatedBy() ?? '',
                ),
            valeurMinimum: $this->getValMini(),
            valeurMaximum: $this->getValMaxi()
        );
    }

    /**
     * @param array<DbCarlCompteur> $dbCarlCompteurs
     *
     * @return array<Compteur>
     */
    public static function toCompteurArray(array $dbCarlCompteurs, InterventionServiceOffer $serviceOffer, CarlConfigurationIntervention $gmaoCarl): array
    {
        return array_map(
            function ($dbCarlCompteur) use ($serviceOffer, $gmaoCarl) {
                return $dbCarlCompteur->toCompteur($serviceOffer, $gmaoCarl);
            },
            $dbCarlCompteurs
        );
    }
}
