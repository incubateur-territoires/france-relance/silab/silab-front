<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlCompteurDbCarlEquipementRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlCompteurDbCarlEquipementRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlCompteurDbCarlEquipement implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\ManyToOne(inversedBy: 'dbCarlCompteurDbCarlEquipement')]
        #[JoinColumn()]
        private DbCarlCompteur $dbCarlCompteur,
        #[ORM\Id]
        #[ORM\Column()]
        private bool $directEqpt,
        #[ORM\Id]
        #[ORM\ManyToOne(inversedBy: 'dbCarlCompteurDbCarlEquipement')]
        #[JoinColumn()]
        private DbCarlEquipement $dbCarlEquipement,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_compteur_db_carl_equipement"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
    CREATE FOREIGN TABLE $dbCarlTableName (
                db_carl_compteur_id        varchar(33)           OPTIONS (key 'true')  NOT NULL,
                db_carl_equipement_id        varchar(33)           OPTIONS (key 'true')  NOT NULL,
                direct_eqpt        boolean           OPTIONS (key 'true')  NOT NULL
           ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with
                EQUIPEMENT_ANCESTOR as (
                    select
                        EQUIPMENT.ID as ID,
                        CONNECT_BY_ROOT EQUIPMENT.ID as ANCESTOR_ID,
                        LEVEL
                    from "$dbSchema".CSEQ_LINKEQUIPMENT LINKEQPT
                        left join "$dbSchema".CSEQ_EQUIPMENT EQUIPMENT on EQUIPMENT.ID = LINKEQPT.CHILD_ID
                    connect by prior LINKEQPT.CHILD_ID = LINKEQPT.PARENT_ID and SYSDATE < LINKEQPT.LINKEND and SYSDATE > LINKEQPT.LINKBEGIN
                )

            select unique
                MEASUREPOINT.ID as DB_CARL_COMPTEUR_ID,
                EQUIPEMENT_ANCESTOR.ANCESTOR_ID as DB_CARL_EQUIPEMENT_ID,
                case EQUIPEMENT_ANCESTOR."LEVEL" when 1 THEN 1 ELSE 0 END as "IS_DIRECT_PARENT"

            from "$dbSchema".CSEQ_MEASUREPOINT MEASUREPOINT
                inner join EQUIPEMENT_ANCESTOR on EQUIPEMENT_ANCESTOR.ID = MEASUREPOINT.MATERIAL_ID
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getDbCarlCompteur(): DbCarlCompteur
    {
        return $this->dbCarlCompteur;
    }

    public function getDirectEqpt(): bool
    {
        return $this->directEqpt;
    }

    public function getDbCarlEquipement(): DbCarlEquipement
    {
        return $this->dbCarlEquipement;
    }
}
