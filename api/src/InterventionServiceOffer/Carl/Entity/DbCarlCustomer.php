<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlCustomerRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlCustomerRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlCustomer implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 40)]
        private string $code,
        #[ORM\Column(length: 60)]
        private ?string $label,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_customer"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id              varchar(33)     OPTIONS (key 'true')  NOT NULL,
            code            varchar(40)     NOT NULL,
            label           varchar(60)
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            select
                CUSTOMER.ID as CUSTOMER_ID,
                CUSTOMER.CODE as CUSTOMER_CODE,
                CUSTOMER.DESCRIPTION as CUSTOMER_LABEL
            from "$dbSchema".CSCU_CUSTOMER CUSTOMER
            where CUSTOMER.XTRATXT01=''COMMUNE''
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }
}
