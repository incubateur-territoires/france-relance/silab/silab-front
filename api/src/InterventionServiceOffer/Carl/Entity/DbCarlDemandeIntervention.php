<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Entity\Status\DemandeInterventionStatus;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlDemandeInterventionRepository;
use App\Shared\Location\Entity\Coordinates;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlDemandeInterventionRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlDemandeIntervention implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 20)]
        private string $code,
        #[ORM\Column(length: 60)]
        private ?string $title,
        #[ORM\Column(length: 255)]
        private string $statusCode,
        #[ORM\Column(length: 255)]
        private ?string $statusLabel,
        #[ORM\Column(length: 255)]
        private ?string $statusCreatedBy,
        #[ORM\Column()]
        private \DateTime $statusCreatedAt,
        #[ORM\Column(type: 'text')]
        private ?string $description,
        #[ORM\OneToMany(targetEntity: DbCarlDocument::class, mappedBy: 'dbCarlDemandeIntervention')]
        private Collection $dbCarlDocuments,
        #[ORM\ManyToOne()]
        #[ORM\JoinColumn(nullable: false)]
        private DbCarlGmaoActeur $createdBy,
        #[ORM\Column()]
        private \DateTime $createdAt,
        #[ORM\Column()]
        private \DateTime $updatedAt,
        #[ORM\ManyToOne()]
        private ?DbCarlEquipement $directDbCarlEquipement,
        #[ORM\OneToMany(targetEntity: DbCarlDemandeInterventionDbCarlEquipement::class, mappedBy: 'dbCarlDemandeIntervention')]
        private Collection $dbCarlEquipements,
        #[ORM\Column(length: 1540)]
        private ?string $address,
        #[ORM\Column(precision: 9, scale: 6)]
        private ?float $latitude,
        #[ORM\Column(precision: 9, scale: 6)]
        private ?float $longitude,
        #[ORM\ManyToOne()]
        private ?DbCarlIntervention $dbCarlIntervention,
        #[ORM\Column()]
        private ?\DateTimeImmutable $expectedBeginAt,
        #[ORM\Column()]
        private ?\DateTimeImmutable $expectedEndAt,
        #[ORM\OneToOne(targetEntity: DbCarlActiviteDiagnostique::class, mappedBy: 'demandeInterventionGeneree')]
        private ?DbCarlActiviteDiagnostique $dbCarlActiviteDiagnostique
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_demande_intervention"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlDemandeInterventionTableName = self::getTableName($carlClient->getId());

        $sqlColonneChampCarlPourStockageDateDebutSouhaiteeDI = is_null($carlClient->getChampCarlPourStockageDateDebutSouhaiteeDI()) ? 'null' : 'FROM_TZ(DEMANDE_INTERVENTION.'.$carlClient->getChampCarlPourStockageDateDebutSouhaiteeDI().", ''GMT'')";

        return <<<DEMANDE_INTERVENTION_FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlDemandeInterventionTableName (
            id                              varchar(33)     OPTIONS (key 'true')  NOT NULL,
            code                            varchar(20),
            title                           varchar(60),
            status_code                     varchar(255),
            status_label                    varchar(255),
            status_created_by               varchar(255),
            status_created_at               timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            description                     text,
            created_by_id                   varchar(33),
            created_at                      timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            updated_at                      timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            direct_db_carl_equipement_id    varchar(33),
            address                         varchar(1540),
            latitude                        numeric(9,6),
            longitude                       numeric(9,6),
            db_carl_intervention_id         varchar(33),
            expected_begin_at               timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            expected_end_at                 timestamptz(3)--attention, carl stocke en GMT et pas sur le fuseau de la DB
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with DEMANDE_INTERVENTION_DIRECT_EQUIPEMENT as (
                select
                    DEMANDE_INTERVENTION_EQUIPMENT.MR_ID as DEMANDE_INTERVENTION_ID,
                    coalesce(MAX(DEMANDE_INTERVENTION_MATERIAL.ID),MAX(DEMANDE_INTERVENTION_BOX.ID)) as EQUIPEMENT_ID
                from "$dbSchema".CSWO_MREQPT DEMANDE_INTERVENTION_EQUIPMENT
                    left join "$dbSchema".CSEQ_MATERIAL DEMANDE_INTERVENTION_MATERIAL on DEMANDE_INTERVENTION_MATERIAL.ID = DEMANDE_INTERVENTION_EQUIPMENT.EQPT_ID
                    left join "$dbSchema".CSEQ_BOX DEMANDE_INTERVENTION_BOX on DEMANDE_INTERVENTION_BOX.ID = DEMANDE_INTERVENTION_EQUIPMENT.EQPT_ID
                where DEMANDE_INTERVENTION_EQUIPMENT.DIRECTEQPT = 1
                group by DEMANDE_INTERVENTION_EQUIPMENT.MR_ID
            ),
            DEMANDE_INTERVENTION_STEP as (
                select STEP.CODE as CODE, STEP.DESCRIPTION as DESCRIPTION from "$dbSchema".CSSY_STEP STEP
                inner join "$dbSchema".CSSY_STATUSFLOW STATUSFLOW on STATUSFLOW.ID = STEP.STATUSFLOW_ID
                inner join "$dbSchema".CSSY_OBJECTINFO OBJECTINFO on OBJECTINFO.STATUSFLOW = STATUSFLOW.CODE and OBJECTINFO.CODE = ''MR''
            ),
            DEMANDE_INTERVENTION_ADDRESS as (
                select
                    ((CASE when MR_ADDRESS."NUMBER" = -1 THEN '''' ELSE TO_CHAR(MR_ADDRESS."NUMBER") END) || '' '' || MR_ADDRESS.MENTION || '' '' || MR_ADDRESS.ADDRESS1 || '' ''|| MR_ADDRESS.ADDRESS2 ||'' ''|| MR_ADDRESS.ADDRESS3 || '' '' || MR_ADDRESS.ZIPCODE || '' '' || MR_ADDRESS.CITY) as ADDRESS,
                    MR_ADDRESS.MR_ID as DEMANDE_INTERVENTION_ID
                from "$dbSchema".CSWO_MRADDRESS MR_ADDRESS
            )
            select
                DEMANDE_INTERVENTION.ID as ID,
                DEMANDE_INTERVENTION.CODE as CODE,
                DEMANDE_INTERVENTION.DESCRIPTION as TITLE,
                DEMANDE_INTERVENTION.STATUS_CODE as STATUS_CODE,
                DEMANDE_INTERVENTION_STEP.DESCRIPTION as STATUS_LABEL,
                STATUS_CREATEDBY_ACTOR.FULLNAME as STATUS_CREATED_BY,
                FROM_TZ(DEMANDE_INTERVENTION.STATUS_CHANGEDDATE, ''GMT'') as STATUS_CREATED_AT,
                DEMANDE_INTERVENTION_DESCRIPTION.RAWDESCRIPTION as DESCRIPTION,
                DEMANDE_INTERVENTION.CREATEDBY_ID as CREATED_BY_ID,
                FROM_TZ(DEMANDE_INTERVENTION.CREATIONDATE, ''GMT'') as CREATED_AT,
                FROM_TZ(DEMANDE_INTERVENTION.MODIFYDATE, ''GMT'') as UPDATED_AT,
                DEMANDE_INTERVENTION_DIRECT_EQUIPEMENT.EQUIPEMENT_ID as RELATED_EQUIPEMENT_ID,
                DEMANDE_INTERVENTION_ADDRESS.ADDRESS,
                DEMANDE_INTERVENTION.LATITUDE as LATITUDE,
                DEMANDE_INTERVENTION.LONGITUDE as LONGITUDE,
                DEMANDE_INTERVENTION.WO_ID as DB_CARL_INTERVENTION_ID,
                $sqlColonneChampCarlPourStockageDateDebutSouhaiteeDI as EXPECTED_BEGIN_AT,
                FROM_TZ(DEMANDE_INTERVENTION.EXPEND, ''GMT'') as EXPECTED_END_AT
            from "$dbSchema".CSWO_MR DEMANDE_INTERVENTION
                left join DEMANDE_INTERVENTION_STEP on DEMANDE_INTERVENTION_STEP.CODE = DEMANDE_INTERVENTION.STATUS_CODE
                left join "$dbSchema".CSSY_ACTOR STATUS_CREATEDBY_ACTOR on STATUS_CREATEDBY_ACTOR.ID = DEMANDE_INTERVENTION.STATUS_CHANGEDBY_ID
                left join "$dbSchema".CSSY_DESCRIPTION DEMANDE_INTERVENTION_DESCRIPTION on DEMANDE_INTERVENTION_DESCRIPTION.ID = DEMANDE_INTERVENTION.LONGDESC_ID
                left join DEMANDE_INTERVENTION_DIRECT_EQUIPEMENT on DEMANDE_INTERVENTION_DIRECT_EQUIPEMENT.DEMANDE_INTERVENTION_ID = DEMANDE_INTERVENTION.ID
                left join DEMANDE_INTERVENTION_ADDRESS on DEMANDE_INTERVENTION_ADDRESS.DEMANDE_INTERVENTION_ID = DEMANDE_INTERVENTION.ID
        )')
DEMANDE_INTERVENTION_FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getStatusCode(): string
    {
        return $this->statusCode;
    }

    public function getStatusLabel(): ?string
    {
        return $this->statusLabel;
    }

    public function getStatusCreatedBy(): ?string
    {
        return $this->statusCreatedBy;
    }

    public function getStatusCreatedAt(): \DateTime
    {
        return $this->statusCreatedAt;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getDbCarlDocuments(): Collection
    {
        return $this->dbCarlDocuments;
    }

    public function getCreatedBy(): DbCarlGmaoActeur
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function getDirectDbCarlEquipement(): ?DbCarlEquipement
    {
        return $this->directDbCarlEquipement;
    }

    public function getDbCarlEquipements(): Collection
    {
        return $this->dbCarlEquipements;
    }

    public function getAddress(): ?string
    {
        if (is_null($this->address)) {
            return null;
        }

        return preg_replace('!\s+!', ' ', $this->address);
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function getDbCarlIntervention(): ?DbCarlIntervention
    {
        return $this->dbCarlIntervention;
    }

    public function getExpectedBeginAt(): ?\DateTimeImmutable
    {
        return $this->expectedBeginAt;
    }

    public function getExpectedEndAt(): ?\DateTimeImmutable
    {
        return $this->expectedEndAt;
    }

    public function getDbCarlActiviteDiagnostique(): ?DbCarlActiviteDiagnostique
    {
        return $this->dbCarlActiviteDiagnostique;
    }

    public function toDemandeIntervention(InterventionServiceOffer $serviceOffer, CarlConfigurationIntervention $gmaoCarl): DemandeIntervention
    {
        $images = [];
        foreach ($this->getDbCarlDocuments()->toArray() as $dbCarlDocument) {
            switch ($dbCarlDocument->getTypeId()) {
                case $gmaoCarl->getPhotoAvantInterventionDoctypeId():
                    $images[] = $dbCarlDocument->toImage();
                    break;
                default:
                    break;
            }
        }

        return new DemandeIntervention(
            title: $this->getTitle() ?? '',
            description: $this->getDescription(),
            relatedEquipement: $this->getDirectDbCarlEquipement()?->toEquipement(serviceOffer: $serviceOffer, gmaoCarl: $gmaoCarl),
            id: $this->getId(),
            code: $this->getCode(),
            currentStatus: new DemandeInterventionStatus(
                code: CarlClient::getSilabStatusCodeFromCarlStatusCode($this->getStatusCode(), CarlClient::SILAB_TO_CARL_DEMANDEINTERVENTION_STATUS_MAPPING),
                label: $this->getStatusLabel() ?? 'étape inconnue',
                createdBy: $this->getStatusCreatedBy() ?? 'acteur sans nom',
                createdAt: $this->getStatusCreatedAt()
            ),
            images: $images,
            createdBy: $this->getCreatedBy()->toGmaoActeur(),
            createdAt: $this->getCreatedAt(),
            updatedAt: $this->getUpdatedAt(),
            coordonnees: !is_null($this->getLatitude()) && !is_null($this->getLongitude())
                ? new Coordinates(lat: $this->getLatitude(), lng: $this->getLongitude())
                : null,
            address: $this->getAddress(),
            documentsJoints: $this->getDbCarlDocuments()->map(
                function (DbCarlDocument $dbCarlDocument) {
                    return $dbCarlDocument->toDocument();
                }
            )->toArray(),
            gmaoObjectViewUrl: null,
            dateDeDebutSouhaitee: $this->getExpectedBeginAt(),
            dateDeFinSouhaitee: $this->getExpectedEndAt(),
            raisonSiNonSupprimable: is_null($this->getDbCarlActiviteDiagnostique()) ? null : 'Cette demande a été créée suite à un problème relevé lors d\'un diagnostique sur Carl, ce qui empêche sa suppression.'
        );
    }
}
