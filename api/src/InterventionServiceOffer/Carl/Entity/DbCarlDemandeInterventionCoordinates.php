<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeInterventionCoordinates;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlDemandeInterventionCoordinatesRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlDemandeInterventionCoordinatesRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlDemandeInterventionCoordinates implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 60)]
        private ?string $title,
        #[ORM\Column(length: 255)]
        private string $statusCode,
        #[ORM\Column(length: 20)]
        private string $code,
        #[ORM\Column()]
        private \DateTime $updatedAt,
        #[ORM\Column(length: 33)]
        private float $latitude,
        #[ORM\Column(precision: 9, scale: 6)]
        private float $longitude,
        #[ORM\OneToMany(targetEntity: DbCarlDemandeInterventionDbCarlEquipement::class, mappedBy: 'dbCarlDemandeInterventionCoordinates')]
        private Collection $dbCarlEquipements,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_demande_intervention_coordinates"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id                      varchar(33)     OPTIONS (key 'true') NOT NULL,
            title                   varchar(60),
            status_code             varchar(255),
            code                    varchar(20),
            updated_at              timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            latitude                numeric(9,6) NOT NULL,
            longitude               numeric(9,6) NOT NULL
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
        with
            DEMANDE_INTERVENTION_DIRECT_EQUIPEMENT as (
                select
                    DEMANDE_INTERVENTION_EQUIPMENT.MR_ID as DEMANDE_INTERVENTION_ID,
                    coalesce(MAX(DEMANDE_INTERVENTION_MATERIAL.ID),MAX(DEMANDE_INTERVENTION_BOX.ID)) as EQUIPEMENT_ID
                from "$dbSchema".CSWO_MREQPT DEMANDE_INTERVENTION_EQUIPMENT
                    left join "$dbSchema".CSEQ_MATERIAL DEMANDE_INTERVENTION_MATERIAL on DEMANDE_INTERVENTION_MATERIAL.ID = DEMANDE_INTERVENTION_EQUIPMENT.EQPT_ID
                    left join "$dbSchema".CSEQ_BOX DEMANDE_INTERVENTION_BOX on DEMANDE_INTERVENTION_BOX.ID = DEMANDE_INTERVENTION_EQUIPMENT.EQPT_ID
                where DEMANDE_INTERVENTION_EQUIPMENT.DIRECTEQPT = 1
                group by DEMANDE_INTERVENTION_EQUIPMENT.MR_ID
            )
            select
                DEMANDE_INTERVENTION.ID as ID,
                DEMANDE_INTERVENTION.DESCRIPTION as TITLE,
                DEMANDE_INTERVENTION.STATUS_CODE as STATUS_CODE,
                DEMANDE_INTERVENTION.CODE as CODE,
                FROM_TZ(DEMANDE_INTERVENTION.MODIFYDATE, ''GMT'') as UPDATED_AT,
                coalesce(DEMANDE_INTERVENTION.LATITUDE, DIRECT_EQUIPEMENT.LATITUDE) as LATITUDE,
                coalesce(DEMANDE_INTERVENTION.LONGITUDE, DIRECT_EQUIPEMENT.LONGITUDE) as LONGITUDE
            from "$dbSchema".CSWO_MR DEMANDE_INTERVENTION
                left join DEMANDE_INTERVENTION_DIRECT_EQUIPEMENT on DEMANDE_INTERVENTION_DIRECT_EQUIPEMENT.DEMANDE_INTERVENTION_ID = DEMANDE_INTERVENTION.ID
                left join "$dbSchema".CSEQ_EQUIPMENT DIRECT_EQUIPEMENT on DIRECT_EQUIPEMENT.ID = DEMANDE_INTERVENTION_DIRECT_EQUIPEMENT.EQUIPEMENT_ID
            where
                coalesce(DEMANDE_INTERVENTION.LATITUDE, DIRECT_EQUIPEMENT.LATITUDE) IS NOT NULL
                and
                coalesce(DEMANDE_INTERVENTION.LONGITUDE, DIRECT_EQUIPEMENT.LONGITUDE) IS NOT NULL
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getStatusCode(): string
    {
        return $this->statusCode;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function toDemandeInterventionCoordinates(): DemandeInterventionCoordinates
    {
        return new DemandeInterventionCoordinates(
            id: $this->id,
            label: trim($this->code.' '.($this->title ?? '')),
            latitude: $this->latitude,
            longitude: $this->longitude
        );
    }
}
