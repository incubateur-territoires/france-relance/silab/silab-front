<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlDemandeInterventionDbCarlEquipementRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlDemandeInterventionDbCarlEquipementRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlDemandeInterventionDbCarlEquipement implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\ManyToOne()]
        #[JoinColumn()]
        private DbCarlDemandeIntervention $dbCarlDemandeIntervention,
        #[ORM\Id]
        #[ORM\ManyToOne()]
        #[JoinColumn()]
        private DbCarlDemandeInterventionCoordinates $dbCarlDemandeInterventionCoordinates,
        #[ORM\Id]
        #[ORM\Column()]
        private bool $directEqpt,
        #[ORM\Id]
        #[ORM\ManyToOne()]
        #[JoinColumn()]
        private DbCarlEquipement $dbCarlEquipement,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_demande_intervention_db_carl_equipement"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            db_carl_demande_intervention_id     varchar(33)     OPTIONS (key 'true')  NOT NULL,
            db_carl_demande_intervention_coordinates_id     varchar(33)     OPTIONS (key 'true')  NOT NULL,
            db_carl_equipement_id       varchar(33)     OPTIONS (key 'true')  NOT NULL,
            direct_eqpt                 boolean         OPTIONS (key 'true')  NOT NULL
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            select
                DEMANDE_INTERVENTION_EQUIPMENT.MR_ID as DB_CARL_DEMANDE_INTERVENTION_ID,
                DEMANDE_INTERVENTION_EQUIPMENT.MR_ID as DB_CARL_DEMANDE_INTERVENTION_COORDINATES_ID,
                DEMANDE_INTERVENTION_EQUIPMENT.EQPT_ID as DB_CARL_EQUIPEMENT_ID,
                DEMANDE_INTERVENTION_EQUIPMENT.DIRECTEQPT as "DIRECT_EQPT"
            from "$dbSchema".CSWO_MREQPT DEMANDE_INTERVENTION_EQUIPMENT
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getDbCarlDemandeIntervention(): DbCarlDemandeIntervention
    {
        return $this->dbCarlDemandeIntervention;
    }

    public function getDirectEqpt(): bool
    {
        return $this->directEqpt;
    }

    public function getDbCarlEquipement(): DbCarlEquipement
    {
        return $this->dbCarlEquipement;
    }
}
