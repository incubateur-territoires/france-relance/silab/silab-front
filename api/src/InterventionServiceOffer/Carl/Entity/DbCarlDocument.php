<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlDocumentRepository;
use App\Shared\Document\Entity\Document;
use App\Shared\Document\Entity\DocumentInterface;
use App\Shared\Document\Entity\Image;
use App\Shared\Document\Entity\ImageInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlDocumentRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlDocument implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 150)]
        private ?string $title,
        /**
         * @deprecated à l'avenir le documentId est plus intéressant, viser l'abandon du documentlink_id dans la foreign table
         */
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $documentlinkId,
        #[ORM\Id]
        #[ORM\ManyToOne(inversedBy: 'dbCarlDocuments')]
        #[JoinColumn()]
        private ?DbCarlEquipement $dbCarlEquipement,
        #[ORM\Id]
        #[ORM\ManyToOne(inversedBy: 'dbCarlDocuments')]
        #[JoinColumn()]
        private ?DbCarlIntervention $dbCarlIntervention,
        #[ORM\Id]
        #[ORM\ManyToOne(inversedBy: 'dbCarlDocuments')]
        #[JoinColumn()]
        private ?DbCarlDemandeIntervention $dbCarlDemandeIntervention,
        #[ORM\Column(length: 33)]
        private string $typeId,
        #[ORM\Column(length: 33)]
        private string $storageType,
        #[ORM\Column(length: 510)]
        private ?string $path,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_document"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id                      varchar(33)     OPTIONS (key 'true')  NOT NULL,
            title                   varchar(150),
            documentlink_id         varchar(33)     OPTIONS (key 'true')  NOT NULL,
            db_carl_equipement_id       varchar(33)     OPTIONS (key 'true')  NOT NULL,
            db_carl_intervention_id       varchar(33)     OPTIONS (key 'true')  NOT NULL,
            db_carl_demande_intervention_id       varchar(33)     OPTIONS (key 'true')  NOT NULL,
            type_id                 varchar(33),
            storage_type            varchar(255),
            path                    varchar(510)
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            select
                DOCUMENT.ID as ID,
                DOCUMENT.DESCRIPTION as TITLE,
                DOCUMENTLINK.ID as DOCUMENTLINK_ID,
                case DOCUMENTLINK.ENTITYCLASS when ''com.carl.xnet.equipment.backend.bean.EquipmentBean'' THEN DOCUMENTLINK.ENTITYID ELSE null END DB_CARL_EQUIPEMENT_ID,
                case DOCUMENTLINK.ENTITYCLASS when ''com.carl.xnet.works.backend.bean.WOBean'' THEN DOCUMENTLINK.ENTITYID ELSE null END DB_CARL_INTERVENTION_ID,
                case DOCUMENTLINK.ENTITYCLASS when ''com.carl.xnet.works.backend.bean.AbstractMRBean'' THEN DOCUMENTLINK.ENTITYID ELSE null END DB_CARL_DEMANDE_ID,
                DOCUMENT.TYPE_ID as TYPE_ID,
                ATTACHMENT.ATTACHMENTTYPE as STORAGE_TYPE,
                CONCAT(ATTACHMENT.PATH, ATTACHMENT.FILENAME) as PATH
            from "$dbSchema".CSSY_DOCUMENTLINK DOCUMENTLINK
            inner join "$dbSchema".CSSY_DOCUMENT DOCUMENT on DOCUMENT.ID = DOCUMENTLINK.DOCUMENT_ID
            inner join "$dbSchema".CSSY_ATTACHMENT ATTACHMENT on ATTACHMENT.ID = DOCUMENT.ATTACHMENT_ID
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDocumentlinkId(): string
    {
        return $this->documentlinkId;
    }

    public function getDbCarlEquipement(): ?DbCarlEquipement
    {
        return $this->dbCarlEquipement;
    }

    public function getTypeId(): string
    {
        return $this->typeId;
    }

    public function getStorageType(): string
    {
        return $this->storageType;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function toImage(): ImageInterface
    {
        return new Image(
            id: $this->getDocumentlinkId(),
            url: $this->getPath()
        );
    }

    public function toDocument(): DocumentInterface
    {
        return new Document(
            id: $this->getDocumentlinkId(),
            name: $this->getTitle() ?? 'fichier sans nom',
            url: $this->getPath() ?? ''
        );
    }
}
