<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\Carl\Exception\EquipementDescendantIsNotActive;
use App\InterventionServiceOffer\Carl\Exception\ServiceOfferObligatoirePourInclureLesAncetresException;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\AncetreEquipement;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Equipement\Entity\Status\EquipementStatus;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlEquipementRepository;
use App\Shared\Location\Entity\Coordinates;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlEquipementRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlEquipement implements DbCarlEntitySqlBuilder
{
    public const CARL_EQUIPEMENT_STATUS_CODE_TO_SIILAB_EQUIPEMENT_STATUS_CODE_MAPPING = [
        'ACTIVE' => 'active',
        'VALIDATE' => 'active',
        'BROKENDOWN' => 'active',
        'MAINTENANCE' => 'active',
        'REQUEST' => 'inactive',
        'REMOVE' => 'inactive',
        'INACTIVE' => 'inactive',
        'AWAITINGVALID' => 'inactive',
        'CANCEL' => 'inactive',
        'REJECTED' => 'inactive',
        'PRE_REFORME' => 'inactive',
    ];

    public const SILAB_EQUIPEMENT_STATUS_CODE_TO_COMPATIBLE_CARL_EQUIPEMENT_STATUS_CODE_MAPPING = [
        'active' => ['ACTIVE', 'VALIDATE', 'BROKENDOWN', 'MAINTENANCE'],
        'inactive' => ['REQUEST', 'REMOVE', 'INACTIVE', 'AWAITINGVALID', 'CANCEL', 'REJECTED', 'PRE_REFORME'],
    ];

    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 33)]
        private string $typeId,
        #[ORM\Column(length: 20)]
        private string $code,
        #[ORM\Column(length: 60)]
        private ?string $label,
        #[ORM\Column(type: 'text')]
        private ?string $comment,
        #[ORM\Column(length: 255)]
        private ?string $xtratxt09,
        #[ORM\Column(precision: 9, scale: 6)]
        private ?float $latitude,
        #[ORM\Column(precision: 9, scale: 6)]
        private ?float $longitude,
        #[ORM\Column(length: 255)]
        private string $statusCode,
        #[ORM\Column(length: 255)]
        private ?string $statusLabel,
        #[ORM\Column(length: 255)]
        private ?string $statusCreatedBy,
        #[ORM\Column(type: 'boolean')]
        private bool $interventionsAutorisees,
        #[ORM\Column()]
        private \DateTimeImmutable $statusCreatedAt,
        #[ORM\OneToMany(targetEntity: DbCarlDocument::class, mappedBy: 'dbCarlEquipement')]
        private Collection $dbCarlDocuments,
        #[ORM\OneToMany(targetEntity: DbCarlCompteurDbCarlEquipement::class, mappedBy: 'dbCarlEquipement')]
        private Collection $dbCarlCompteurDbCarlEquipement,
        #[ORM\OneToMany(targetEntity: DbCarlIntervention::class, mappedBy: 'directDbCarlEquipement')]
        private Collection $dbCarlInterventions,
        #[ORM\OneToMany(targetEntity: DbCarlDemandeIntervention::class, mappedBy: 'directDbCarlEquipement')]
        private Collection $dbCarlDemandeInterventions,
        #[ORM\OneToMany(targetEntity: DbCarlEquipementDbCarlEquipement::class, mappedBy: 'dbCarlEquipementDescendant')]
        private Collection $dbCarlEquipementDbCarlEquipementAncestors,
        #[ORM\OneToMany(targetEntity: DbCarlEquipementDbCarlEquipement::class, mappedBy: 'dbCarlEquipementAncestor')]
        private Collection $dbCarlEquipementDbCarlEquipementDescendants
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_equipement"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
        CREATE FOREIGN TABLE $dbCarlTableName (
            id          varchar(33)           OPTIONS (key 'true')  NOT NULL,
            type_id          varchar(33) NOT NULL,
            code        varchar(20),
            label       varchar(60),
            comment     text,
            xtratxt09   varchar(255),
            latitude    numeric(9,6),
            longitude   numeric(9,6),
            status_code             varchar(255)                            NOT NULL,
            status_label            varchar(255),
            status_created_by       varchar(255),
            status_created_at       timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            interventions_autorisees boolean
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
        with
            EQUIPMENT_EXTRA_FIELDS as (
                select
                    BOXEQPT.ID, BOXEQPT.COMMENT_ID as COMMENT_ID, BOXEQPT.WOAUTHORIZED AS INTERVENTIONS_AUTORISEES from "$dbSchema".CSEQ_BOX BOXEQPT
                union all
                select
                    MATEQPT.ID, MATEQPT.LONGDESC_ID as COMMENT_ID, 1 AS INTERVENTIONS_AUTORISEES from "$dbSchema".CSEQ_MATERIAL MATEQPT
            ),
            EQUIPEMENT_STEP as (
                select
                    STRUCTURE.ID as EQUIPEMENT_STRUCTURE_ID, STEP.CODE as STEP_CODE, STEP.DESCRIPTION as STEP_DESCRIPTION
                from "$dbSchema".CSEQ_STRUCTURE STRUCTURE
                left join "$dbSchema".CSSY_OBJECTINFO OBJECTINFO on OBJECTINFO.CODE = STRUCTURE.ENTITY_ID
                left join "$dbSchema".CSSY_STATUSFLOW STATUSFLOW on STATUSFLOW.CODE = OBJECTINFO.STATUSFLOW
                left join "$dbSchema".CSSY_STEP STEP on STEP.STATUSFLOW_ID = STATUSFLOW.ID
                )
        select
                EQUIPMENT.ID,
                LOWER(STRUCTURE.ENTITY_ID) as TYPE_ID,
                EQUIPMENT.CODE,
                EQUIPMENT.DESCRIPTION as LABEL,
                EQUIPEMENT_DESCRIPTION.RAWDESCRIPTION as "COMMENT",
                EQUIPMENT.XTRATXT09 as XTRATXT09,
                EQUIPMENT.LATITUDE as LATITUDE,
                EQUIPMENT.LONGITUDE as LONGITUDE,
                EQUIPMENT.STATUS_CODE as STATUS_CODE,
                EQUIPEMENT_STEP.STEP_DESCRIPTION as STATUS_LABEL,
                STATUS_CREATEDBY_ACTOR.FULLNAME as STATUS_CREATED_BY,
                FROM_TZ(EQUIPMENT.STATUS_CHANGEDDATE, ''GMT'') as STATUS_CREATED_AT,
                coalesce(EQUIPMENT_EXTRA_FIELDS.INTERVENTIONS_AUTORISEES, 0) AS INTERVENTIONS_AUTORISEES --il n y a pas que du CSEQ_BOX et du CSEQ_MATERIAL
        from "$dbSchema".CSEQ_EQUIPMENT EQUIPMENT
        left join "$dbSchema".CSEQ_STRUCTURE STRUCTURE on STRUCTURE.ID = EQUIPMENT.STRUCTURE_ID
        left join EQUIPMENT_EXTRA_FIELDS on EQUIPMENT_EXTRA_FIELDS.ID = EQUIPMENT.ID
        left join "$dbSchema".CSSY_DESCRIPTION EQUIPEMENT_DESCRIPTION on EQUIPEMENT_DESCRIPTION.ID = EQUIPMENT_EXTRA_FIELDS.COMMENT_ID
        left join "$dbSchema".CSSY_ACTOR STATUS_CREATEDBY_ACTOR on STATUS_CREATEDBY_ACTOR.ID = EQUIPMENT.STATUS_CHANGEDBY_ID
        left join EQUIPEMENT_STEP on EQUIPEMENT_STEP.EQUIPEMENT_STRUCTURE_ID = EQUIPMENT.STRUCTURE_ID and EQUIPEMENT_STEP.STEP_CODE = EQUIPMENT.STATUS_CODE
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTypeId(): string
    {
        return $this->typeId;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): string
    {
        return $this->label ?? '';
    }

    public function getComment(): string
    {
        return $this->comment ?? '';
    }

    public function getXtratxt09(): ?string
    {
        return $this->xtratxt09;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function getDbCarlDocuments(): Collection
    {
        return $this->dbCarlDocuments;
    }

    public function getCoordinates(): ?Coordinates
    {
        if (is_null($this->getLatitude()) || is_null($this->getLongitude())) {
            return null;
        }

        return new Coordinates($this->getLatitude(), $this->getLongitude());
    }

    public function getStatusCode(): string
    {
        return $this->statusCode;
    }

    public function getStatusLabel(): ?string
    {
        return $this->statusLabel;
    }

    public function getStatusCreatedBy(): ?string
    {
        return $this->statusCreatedBy;
    }

    public function getStatusCreatedAt(): \DateTimeImmutable
    {
        return $this->statusCreatedAt;
    }

    public function getInterventionsAutorisees(): bool
    {
        return $this->interventionsAutorisees;
    }

    /**
     * @return Collection<DbCarlEquipementDbCarlEquipement>
     */
    public function getDbCarlEquipementDbCarlEquipementAncestors(): Collection
    {
        return $this->dbCarlEquipementDbCarlEquipementAncestors;
    }

    public function toEquipement(CarlConfigurationIntervention $gmaoCarl, bool $includeAncetres = false, ?InterventionServiceOffer $serviceOffer = null): Equipement
    {
        if ($includeAncetres && is_null($serviceOffer)) {// Amélioration: en réalité on pourrait remonter jusqu'en haut de l'arbre de tous les équipements en l'absence de serviceoffer
            throw new ServiceOfferObligatoirePourInclureLesAncetresException(codeEquipement: $this->getCode());
        }
        $ancetres = $includeAncetres ? $this->getLignéesAncetres($serviceOffer) : null;

        return new Equipement(
            id: $this->getId(),
            code: $this->getCode(),
            label: $this->getLabel(),
            comment: $this->getComment(),
            coordinates: $this->getCoordinates(),
            currentStatus: new EquipementStatus(
                code: self::CARL_EQUIPEMENT_STATUS_CODE_TO_SIILAB_EQUIPEMENT_STATUS_CODE_MAPPING[$this->getStatusCode()],
                label: $this->getStatusLabel() ?? '',
                createdBy: $this->getStatusCreatedBy() ?? '',
                createdAt: $this->getStatusCreatedAt()
            ),
            imagesIllustration: array_reduce(
                $this->getDbCarlDocuments()->toArray(),
                function ($images, $dbCarlDocument) use ($gmaoCarl) {
                    assert($dbCarlDocument instanceof DbCarlDocument);
                    if (
                        in_array(
                            $dbCarlDocument->getTypeId(),
                            $gmaoCarl->getCarlClient()->getPhotoIllustrationEquipementDoctypeIds()
                        )
                    ) {
                        $images[] = $dbCarlDocument->toImage();
                    }

                    return $images;
                },
                []
            ),
            documentsJoints: $this->getDbCarlDocuments()->map(
                function (DbCarlDocument $dbCarlDocument) {
                    return $dbCarlDocument->toDocument();
                },
            )->toArray(),
            ancetres: $ancetres,
            gmaoObjectViewUrl: null,
            interventionsAutorisees: $this->getInterventionsAutorisees()
        );
    }

    /**
     * @return array<string,array<AncetreEquipement>>
     */
    private function getLignéesAncetres(InterventionServiceOffer $serviceOffer): array
    {
        return $this->getDbCarlEquipementDbCarlEquipementAncestors()->reduce(
            function (array $lignées, DbCarlEquipementDbCarlEquipement $ancêtre) use ($serviceOffer) {
                // on prend le départ de chaque lignée en partant des racines
                try {
                    if ($this->isEligibleRoot($ancêtre, $serviceOffer)) {
                        $lignées[$ancêtre->getDbCarlEquipementAncestorStructureId()] =
                            [
                                new AncetreEquipement(
                                    id: $ancêtre->getDbCarlEquipementAncestor()->getId(),
                                    label: $ancêtre->getDbCarlEquipementAncestorLabel()
                                ),
                                ...$this->getLignéeAncetres($ancêtre->getDbCarlEquipementAncestor()->getId()),
                            ];
                    }
                } catch (EquipementDescendantIsNotActive $e) {
                    // Il est possible de trouver des lignées qui comportent des éléments inactifs et on veut les écarter. C'est difficile à faire en SQL.
                }

                return $lignées;
            },
            []
        );
    }

    private function isEligibleRoot(DbCarlEquipementDbCarlEquipement $ancêtre, InterventionServiceOffer $serviceOffer): bool
    {
        // SOIT:
        if (empty($serviceOffer->getAvailableEquipementsIds())) {
            // 1 - Il n'y a pas d'équipements définits dans l'offre de service:
            //     -> alors part des équipements racines de carl
            return $ancêtre->getIsRoot();
        } else {
            // 2 - Nous avons des équipements définits dans l'offre de service :
            //     -> alors on prend comme racine les équipements définits dans l'ods
            return in_array(
                $ancêtre->getDbCarlEquipementAncestor()->getId(),
                $serviceOffer->getAvailableEquipementsIds()
            );
        }
    }

    /**
     * @return array<AncetreEquipement>
     */
    private function getLignéeAncetres(string $idRacine): array
    {
        // on va chercher un equipement ayant pour père notre racine
        /** @var DbCarlEquipementDbCarlEquipement|null */
        $descendantNode = $this->getDbCarlEquipementDbCarlEquipementAncestors()
            ->findFirst(
                function ($_key, DbCarlEquipementDbCarlEquipement $descendant) use ($idRacine) {
                    return $descendant->getDbCarlEquipementAncestorParentId() === $idRacine;
                }
            );

        // si on à pas trouvé de descendant, c'est qu'on à atteint le bas de la lignée
        if (is_null($descendantNode)) {
            return [];
        }

        if (!in_array($descendantNode->getDbCarlEquipementDescendantStatusCode(), self::SILAB_EQUIPEMENT_STATUS_CODE_TO_COMPATIBLE_CARL_EQUIPEMENT_STATUS_CODE_MAPPING['active'])) {
            throw new EquipementDescendantIsNotActive();
        }

        return [
            new AncetreEquipement(
                id: $descendantNode->getDbCarlEquipementAncestor()->getId(),
                label: $descendantNode->getDbCarlEquipementAncestorLabel()
            ),
            ...$this->getLignéeAncetres($descendantNode->getDbCarlEquipementAncestor()->getId()),
        ];
    }
}
