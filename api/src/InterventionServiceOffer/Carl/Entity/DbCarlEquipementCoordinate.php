<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\Equipement\Entity\Equipement\EquipementCoordinates;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlEquipementRepository;
use App\Shared\Location\Entity\Coordinates;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlEquipementRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlEquipementCoordinate implements DbCarlEntitySqlBuilder
{
    public const CARL_EQUIPEMENT_STATUS_CODE_TO_SIILAB_EQUIPEMENT_STATUS_CODE_MAPPING = [
        'ACTIVE' => 'active',
        'VALIDATE' => 'active',
        'BROKENDOWN' => 'active',
        'MAINTENANCE' => 'active',
        'REQUEST' => 'inactive',
        'REMOVE' => 'inactive',
        'INACTIVE' => 'inactive',
        'AWAITINGVALID' => 'inactive',
        'CANCEL' => 'inactive',
        'REJECTED' => 'inactive',
        'PRE_REFORME' => 'inactive',
    ];

    public const SILAB_EQUIPEMENT_STATUS_CODE_TO_COMPATIBLE_CARL_EQUIPEMENT_STATUS_CODE_MAPPING = [
        'active' => ['ACTIVE', 'VALIDATE', 'BROKENDOWN', 'MAINTENANCE'],
        'inactive' => ['REQUEST', 'REMOVE', 'INACTIVE', 'AWAITINGVALID', 'CANCEL', 'REJECTED', 'PRE_REFORME'],
    ];

    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 33)]
        private string $typeId,
        #[ORM\Column(length: 20)]
        private string $code,
        #[ORM\Column(length: 60)]
        private ?string $label,
        #[ORM\Column(precision: 9, scale: 6)]
        private float $latitude,
        #[ORM\Column(precision: 9, scale: 6)]
        private float $longitude,
        #[ORM\Column(length: 255)]
        private string $statusCode,
        #[Ignore] // empêche les références circulaires
        #[ORM\OneToMany(targetEntity: DbCarlEquipementDbCarlEquipement::class, mappedBy: 'dbCarlEquipementCoordinateDescendant')]
        private Collection $dbCarlEquipementCoordinateDbCarlEquipementAncestors,
        #[Ignore] // empêche les références circulaires
        #[ORM\OneToMany(targetEntity: DbCarlEquipementDbCarlEquipement::class, mappedBy: 'dbCarlEquipementCoordinateAncestor')]
        private Collection $dbCarlEquipementCoordinateDbCarlEquipementDescendants// y a t'il besoin
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_equipement_coordinate"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
        CREATE FOREIGN TABLE $dbCarlTableName (
            id              varchar(33)     OPTIONS (key 'true')    NOT NULL,
            type_id         varchar(33)                             NOT NULL,
            code            varchar(20)                                     ,
            label           varchar(60)                                     ,
            latitude        numeric(9,6)                            NOT NULL,
            longitude       numeric(9,6)                            NOT NULL,
            status_code     varchar(255)                            NOT NULL
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
       
        select
                EQUIPMENT.ID,
                LOWER(STRUCTURE.ENTITY_ID) as TYPE_ID,
                EQUIPMENT.CODE,
                EQUIPMENT.DESCRIPTION as LABEL,
                EQUIPMENT.LATITUDE as LATITUDE,
                EQUIPMENT.LONGITUDE as LONGITUDE,
                EQUIPMENT.STATUS_CODE as STATUS_CODE
        from "$dbSchema".CSEQ_EQUIPMENT EQUIPMENT
            left join "$dbSchema".CSEQ_STRUCTURE STRUCTURE on STRUCTURE.ID = EQUIPMENT.STRUCTURE_ID
            
        where   LATITUDE is not null and
                LONGITUDE is not null
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTypeId(): string
    {
        return $this->typeId;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): string
    {
        return $this->label ?? '';
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function getCoordinates(): Coordinates
    {
        return new Coordinates($this->getLatitude(), $this->getLongitude());
    }

    public function getStatusCode(): string
    {
        return $this->statusCode;
    }

    /**
     * @return Collection<DbCarlEquipementDbCarlEquipement>
     */
    public function getDbCarlEquipementCoordinateDbCarlEquipementAncestors(): Collection
    {
        return $this->dbCarlEquipementCoordinateDbCarlEquipementAncestors;
    }

    public function toEquipementCoordinates(): EquipementCoordinates
    {
        return new EquipementCoordinates(
            id: $this->getId(),
            label: trim($this->code.' '.($this->label ?? '')),
            coordinates: $this->getCoordinates(),
        );
    }
}
