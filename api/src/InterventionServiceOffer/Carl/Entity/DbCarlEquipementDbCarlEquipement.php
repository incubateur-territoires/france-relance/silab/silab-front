<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlEquipementDbCarlEquipementRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlEquipementDbCarlEquipementRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlEquipementDbCarlEquipement implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id] // Note, il faut faire très attention aux id, doctrine utilise les ID pour dédupliquer les résultats, ce qui peut mener à des surprises si les id ne sont pas uniques !
        #[ORM\Column()]
        private string $path,
        #[ORM\Id]
        #[ORM\ManyToOne(inversedBy: 'dbCarlEquipementDbCarlEquipementAncestors')]
        #[JoinColumn()]
        private DbCarlEquipement $dbCarlEquipementDescendant,
        #[ORM\Id]
        #[ORM\ManyToOne(inversedBy: 'dbCarlEquipementDbCarlEquipementDescendants')]
        #[JoinColumn()]
        private DbCarlEquipement $dbCarlEquipementAncestor,
        #[ORM\ManyToOne(inversedBy: 'dbCarlEquipementCoordinateDbCarlEquipementAncestors')]
        #[JoinColumn()]
        private DbCarlEquipementCoordinate $dbCarlEquipementCoordinateDescendant,
        #[ORM\ManyToOne(inversedBy: 'dbCarlEquipementCoordinateDbCarlEquipementDescendants')]
        #[JoinColumn()]
        private DbCarlEquipement $dbCarlEquipementCoordinateAncestor,
        #[ORM\Column(length: 255)]
        private string $dbCarlEquipementDescendantStatusCode,
        #[ORM\Column(length: 60)]
        private ?string $dbCarlEquipementAncestorLabel,
        #[ORM\Column(length: 33)]
        private string $dbCarlEquipementAncestorStructureId,
        #[ORM\Column(length: 33, nullable: true)]
        private ?string $dbCarlEquipementAncestorParentId,
        #[ORM\Column()]
        private bool $directEqpt,
        #[ORM\Column()]
        private bool $isRoot
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_equipement_db_carl_equipement"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $activeStatusCodes = implode(', ', array_map(function ($activeStatusCode) {
            return "''$activeStatusCode''";
        }, DbCarlEquipement::SILAB_EQUIPEMENT_STATUS_CODE_TO_COMPATIBLE_CARL_EQUIPEMENT_STATUS_CODE_MAPPING['active']));
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
        CREATE FOREIGN TABLE $dbCarlTableName (
            path                                        text            OPTIONS (key 'true')    NOT NULL    ,
            db_carl_equipement_descendant_id            varchar(33)                             NOT NULL    ,
            db_carl_equipement_coordinate_descendant_id varchar(33)                             NOT NULL    ,
            db_carl_equipement_descendant_status_code   varchar(255)                            NOT NULL    ,
            db_carl_equipement_ancestor_id              varchar(33)     OPTIONS (key 'true')    NOT NULL    ,
            db_carl_equipement_coordinate_ancestor_id   varchar(33)                             NOT NULL    ,
            db_carl_equipement_ancestor_label           varchar(60)                                         ,
            db_carl_equipement_ancestor_structure_id    varchar(33)                             NOT NULL    ,
            db_carl_equipement_ancestor_parent_id       varchar(33)                                         ,
            direct_eqpt                                 boolean                                 NOT NULL    ,
            is_root                                     boolean                                 NOT NULL
        ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with
                EQUIPEMENT_ANCESTOR as (
                    select
                        EQUIPMENT.ID as ID,
                        CONNECT_BY_ROOT EQUIPMENT.STRUCTURE_ID as ANCESTOR_STRUCTURE_ID,
                        CONNECT_BY_ROOT EQUIPMENT.DESCRIPTION as ANCESTOR_DESCRIPTION,
                        CONNECT_BY_ROOT EQUIPMENT.STATUS_CODE as ANCESTOR_STATUS_CODE,
                        CONNECT_BY_ROOT EQUIPMENT.ID as ANCESTOR_ID,
                        CONNECT_BY_ROOT LINKEQPT.PARENT_ID as ANCESTOR_PARENT_ID,
                        SYS_CONNECT_BY_PATH(LINKEQPT.PARENT_ID, ''|'') "PATH",
                        LEVEL
                    from "$dbSchema".CSEQ_LINKEQUIPMENT LINKEQPT
                        left join "$dbSchema".CSEQ_EQUIPMENT EQUIPMENT on EQUIPMENT.ID = LINKEQPT.CHILD_ID
                        left join "$dbSchema".CSEQ_STRUCTURE STRUCTURE on STRUCTURE.ID = EQUIPMENT.STRUCTURE_ID
                    where LEVEL != 1
                    connect by prior LINKEQPT.CHILD_ID = LINKEQPT.PARENT_ID and SYSDATE < LINKEQPT.LINKEND and SYSDATE > LINKEQPT.LINKBEGIN and EQUIPMENT.status_code in ($activeStatusCodes)
                )

            select unique
                EQUIPEMENT_ANCESTOR.PATH as "PATH",
                EQUIPMENT.ID as DB_CARL_EQUIPEMENT_ID,
                EQUIPMENT.ID as DB_CARL_EQUIPEMENT_COORDINATE_ID,
                EQUIPMENT.STATUS_CODE as DB_CARL_STATUS_CODE,
                EQUIPEMENT_ANCESTOR.ANCESTOR_ID as DB_CARL_EQUIPEMENT_ANCESTOR_ID,
                EQUIPEMENT_ANCESTOR.ANCESTOR_ID as DB_CARL_EQUIPEMENT_COORDINATE_ANCESTOR_ID,
                EQUIPEMENT_ANCESTOR.ANCESTOR_DESCRIPTION as DB_CARL_ANCESTOR_LABEL,
                EQUIPEMENT_ANCESTOR.ANCESTOR_STRUCTURE_ID as DB_CARL_ANCESTOR_STRUCTURE_ID,
                EQUIPEMENT_ANCESTOR.ANCESTOR_PARENT_ID as DB_CARL_ANCESTOR_PARENT_ID,
                case when EQUIPEMENT_ANCESTOR."LEVEL" = 2 THEN 1 ELSE 0 END "IS_DIRECT_PARENT",
                case when EQUIPEMENT_ANCESTOR.ANCESTOR_PARENT_ID is null THEN 1 ELSE 0 END "IS_ROOT"
            from "$dbSchema".CSEQ_EQUIPMENT EQUIPMENT
                inner join EQUIPEMENT_ANCESTOR on EQUIPEMENT_ANCESTOR.ID = EQUIPMENT.ID
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getDbCarlEquipementDescendant(): DbCarlEquipement
    {
        return $this->dbCarlEquipementDescendant;
    }

    public function getDbCarlEquipementDescendantCoordinate(): DbCarlEquipementCoordinate
    {
        return $this->dbCarlEquipementCoordinateDescendant;
    }

    public function getDbCarlEquipementDescendantStatusCode(): ?string
    {
        return $this->dbCarlEquipementDescendantStatusCode;
    }

    public function getDbCarlEquipementAncestor(): DbCarlEquipement
    {
        return $this->dbCarlEquipementAncestor;
    }

    public function getDbCarlEquipementAncestorLabel(): ?string
    {
        return $this->dbCarlEquipementAncestorLabel;
    }

    public function getDbCarlEquipementAncestorStructureId(): string
    {
        return $this->dbCarlEquipementAncestorStructureId;
    }

    public function getDbCarlEquipementAncestorParentId(): ?string
    {
        return $this->dbCarlEquipementAncestorParentId;
    }

    public function getIsRoot(): bool
    {
        return $this->isRoot;
    }

    public function getDirectEqpt(): bool
    {
        return $this->directEqpt;
    }
}
