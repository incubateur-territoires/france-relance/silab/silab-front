<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlEquipementTypeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entité légère pour récupérer le type d'un équipement.
 */
#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlEquipementTypeRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlEquipementType implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 33)]
        private string $typeId,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_equipement_type"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
        CREATE FOREIGN TABLE $dbCarlTableName (
            id          varchar(33)           OPTIONS (key 'true')  NOT NULL,
            type_id          varchar(33) NOT NULL
        ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
        select
                EQUIPMENT.ID,
                LOWER(STRUCTURE.ENTITY_ID) as TYPE_ID
        from "$dbSchema".CSEQ_EQUIPMENT EQUIPMENT
        left join "$dbSchema".CSEQ_STRUCTURE STRUCTURE on STRUCTURE.ID = EQUIPMENT.STRUCTURE_ID
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTypeId(): string
    {
        return $this->typeId;
    }
}
