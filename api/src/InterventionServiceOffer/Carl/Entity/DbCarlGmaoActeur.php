<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlGmaoActeurRepository;
use App\Shared\Gmao\Entity\GmaoActeur;
use App\Shared\Gmao\Entity\GmaoActeurStatus;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlGmaoActeurRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlGmaoActeur implements DbCarlEntitySqlBuilder
{
    public const CARL_ACTOR_STATUS_CODE_TO_GMAOACTEUR_STATUS_CODE_MAPPING = [
        'ACTIVE' => 'active',
        'INACTIVE' => 'inactive',
        'CANCEL' => 'inactive',
    ];
    public const SILAB_GMAOACTEUR_STATUS_CODE_TO_COMPATIBLE_CARL_ACTOR_STATUS_CODE_MAPPING = [
        'active' => ['ACTIVE'],
        'inactive' => ['INACTIVE', 'CANCEL'],
    ];

    public const SILAB_GMAOACTEUR_STATUS_CODE_TO_COMPATIBLE_CARL_TECHNICIAN_STATUS_CODE_MAPPING = [
        'active' => ['ACTIVE'],
        'inactive' => ['INACTIVE', 'CANCEL'],
    ];

    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 255)]
        private ?string $code,
        #[ORM\Column(length: 255)]
        private ?string $nomComplet,
        #[ORM\Column(length: 255)]
        private string $statusCode,
        #[ORM\Column(length: 255)]
        private ?string $statusLabel,
        #[ORM\Column(length: 255)]
        private ?string $statusCreatedBy,
        #[ORM\Column()]
        private \DateTime $statusCreatedAt,
        /** @var array<string> */
        #[ORM\Column(type: 'json')]
        private array $emails,
        /** @var array<string> */
        #[ORM\Column(type: 'json')]
        private array $telephones,
        #[ORM\Column(length: 33, nullable: true)]
        private ?string $technicianId,
        #[ORM\Column(length: 255)]
        private string $technicianStatusCode,
        #[ORM\Column(nullable: true)]
        private ?string $technicianTeamId,
        #[ORM\OneToMany(targetEntity: DbCarlIntervention::class, mappedBy: 'createdBy')]
        private Collection $dbCarlInterventions,
        #[ORM\OneToMany(targetEntity: DbCarlRessource::class, mappedBy: 'dbCarlGmaoActeur')]
        private Collection $dbCarlRessources,
        #[ORM\OneToMany(targetEntity: DbCarlOperationDbCarlGmaoActeur::class, mappedBy: 'dbCarlGmaoActeur')]
        private Collection $dbCarlOperationDbCarlGmaoActeur,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_gmao_acteur"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id                      varchar(33)     OPTIONS (key 'true')    NOT NULL,
            code                    varchar(20)                            NOT NULL,
            nom_complet             varchar(255),
            status_code             varchar(255)                            NOT NULL,
            status_label            varchar(255),
            status_created_by       varchar(255),
            status_created_at       timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            emails                  json                                    NOT NULL,
            telephones              json                                    NOT NULL,
            technician_id           varchar(33),
            technician_status_code  varchar(255),
            technician_team_id  varchar(33)
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with ACTOR_EMAILS as (
                select
                    PHONE.ACTOR_ID,
                    JSON_ARRAYAGG(PHONE.PHONENUM returning clob) as EMAILS
                from
                    "$dbSchema".CSSY_PHONE PHONE
                where
                    PHONE.TYPE = ''EMAIL''
                group by
                    PHONE.ACTOR_ID
            ),
            ACTOR_PHONES as (
                select
                    PHONE.ACTOR_ID,
                    JSON_ARRAYAGG(PHONE.PHONENUM returning clob) as PHONENUMBERS
                from
                    "$dbSchema".CSSY_PHONE PHONE
                where
                    PHONE.TYPE NOT IN (''EMAIL'', ''FAX'')
                group by
                    PHONE.ACTOR_ID
            ),
            ACTOR_STEP as (
                select STEP.CODE as CODE, STEP.DESCRIPTION as DESCRIPTION from "$dbSchema".CSSY_STEP STEP
                inner join "$dbSchema".CSSY_STATUSFLOW STATUSFLOW on STATUSFLOW.ID = STEP.STATUSFLOW_ID
                inner join "$dbSchema".CSSY_OBJECTINFO OBJECTINFO on OBJECTINFO.STATUSFLOW = STATUSFLOW.CODE and OBJECTINFO.CODE = ''ACTOR''
            )
            select
                ACTOR.ID as DB_CARL_GMAOACTEUR_ID,
                ACTOR.CODE as CODE,
                ACTOR.FULLNAME as DB_CARL_GMAOACTEUR_FULLNAME,
                ACTOR.STATUS_CODE as DB_CARL_GMAOACTEUR_STATUS_CODE,
                ACTOR_STEP.DESCRIPTION as STATUS_LABEL,
                STATUS_CREATEDBY_ACTOR.FULLNAME as STATUS_CREATED_BY,
                FROM_TZ(ACTOR.STATUS_CHANGEDDATE, ''GMT'') as STATUS_CREATED_AT,
                coalesce(ACTOR_EMAILS.EMAILS, to_clob(''[]'')) as DB_CARL_GMAOACTEUR_EMAILS,
                coalesce(ACTOR_PHONES.PHONENUMBERS, to_clob(''[]'')) as DB_CARL_GMAOACTEUR_PHONENUMBERS,
                TECHNICIAN.ID as TECHNICIAN_ID,
                TECHNICIAN.STATUS_CODE as TECHNICIAN_STATUS_CODE,
                TECHNICIAN.TEAM_ID as TECHNICIAN_TEAM_ID
            from
                "$dbSchema".CSSY_ACTOR ACTOR
                left join ACTOR_EMAILS on ACTOR_EMAILS.ACTOR_ID = ACTOR.ID
                left join ACTOR_PHONES on ACTOR_PHONES.ACTOR_ID = ACTOR.ID
                left join "$dbSchema".CSRE_TECHNICIAN TECHNICIAN on TECHNICIAN.ACTOR_ID = ACTOR.ID
                left join "$dbSchema".CSSY_ACTOR STATUS_CREATEDBY_ACTOR on STATUS_CREATEDBY_ACTOR.ID = ACTOR.STATUS_CHANGEDBY_ID
                left join ACTOR_STEP on ACTOR_STEP.CODE = ACTOR.STATUS_CODE
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getNomComplet(): ?string
    {
        return $this->nomComplet;
    }

    public function getStatusCode(): string
    {
        return $this->statusCode;
    }

    public function getStatusLabel(): ?string
    {
        return $this->statusLabel;
    }

    public function getStatusCreatedBy(): ?string
    {
        return $this->statusCreatedBy;
    }

    public function getStatusCreatedAt(): \DateTime
    {
        return $this->statusCreatedAt;
    }

    /**
     * @return array<string>
     */
    public function getEmails(): array
    {
        return array_unique($this->emails);
    }

    /**
     * @return array<string>
     */
    public function getTelephones(): array
    {
        return array_unique($this->telephones);
    }

    public function getTechnicianId(): ?string
    {
        return $this->technicianId;
    }

    public function getTechnicianStatusCode(): string
    {
        return $this->technicianStatusCode;
    }

    public function getTechnicianTeamId(): ?string
    {
        return $this->technicianTeamId;
    }

    public function getDbCarlRessources(): Collection
    {
        return $this->dbCarlRessources;
    }

    public function getDbCarlOperationDbCarlGmaoActeur(): Collection
    {
        return $this->dbCarlOperationDbCarlGmaoActeur;
    }

    /**
     * Undocumented function.
     *
     * @param array<string> $include
     */
    public function toGmaoActeur(array $include = []): GmaoActeur
    {
        return new GmaoActeur(
            id: $this->getId(),
            nomComplet: $this->getNomComplet() ?? '',
            emails: $this->getEmails(),
            telephones: $this->getTelephones(),
            currentStatus: new GmaoActeurStatus(
                code: self::CARL_ACTOR_STATUS_CODE_TO_GMAOACTEUR_STATUS_CODE_MAPPING[$this->getStatusCode()],
                label: $this->getStatusLabel() ?? '',
                createdBy: $this->getStatusCreatedBy() ?? '',
                createdAt: $this->getStatusCreatedAt()
            ),
            operationsHabilitees: in_array('operationsHabilitees', $include)
                ? $this->getDbCarlOperationDbCarlGmaoActeur()->map(
                    function (DbCarlOperationDbCarlGmaoActeur $dbCarlOperationDbCarlGmaoActeur) {
                        return $dbCarlOperationDbCarlGmaoActeur->getDbCarlOperation()->toOperation();
                    }
                )->toArray()
                : null
        );
    }
}
