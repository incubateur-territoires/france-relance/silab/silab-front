<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Intervention\Entity\ActionType\ActionType;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\Entity\Status\InterventionStatus;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlInterventionRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlInterventionRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlIntervention implements DbCarlEntitySqlBuilder
{
    public const INTERVENTION_STATUS_WORKFLOW_VERS_CLOSED = [// amelioration : récupérer dynamiquement
        'REQUEST' => 'VALIDATE',
        'VALIDATE' => 'AWAITINGREAL',
        'AWAITINGREAL' => 'CLOSED',
        'INPROGRESS' => 'CLOSED',
    ];

    public const INTERVENTION_STATUS_WORKFLOW_VERS_CANCEL = [// amelioration : récupérer dynamiquement
        'REQUEST' => 'CANCEL',
        'VALIDATE' => 'AWAITINGREAL',
        'AWAITINGREAL' => 'CANCEL',
        // 'INPROGRESS' => 'CANCEL', //ne fonctionne pas toujours malgré ce qui est sur le workflow
    ];

    public const INTERVENTION_STATUS_WORKFLOW_VERS_AWAITINGRECOMP = [// amelioration : récupérer dynamiquement
        'REQUEST' => 'VALIDATE',
        'VALIDATE' => 'AWAITINGREAL',
        'AWAITINGREAL' => 'INPROGRESS',
        'INPROGRESS' => 'AWAITINGRECOMP',
    ];

    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 60)]
        private ?string $title,
        #[ORM\Column(length: 255)]
        private string $workPriority,
        #[ORM\Column(length: 255)]
        private string $statusCode,
        #[ORM\Column(length: 255)]
        private ?string $statusLabel,
        #[ORM\Column(length: 255)]
        private ?string $statusCreatedBy,
        #[ORM\Column()]
        private \DateTime $statusCreatedAt,
        #[ORM\Column()]
        private \DateTime $createdAt,
        #[ORM\Column()]
        private \DateTime $updatedAt,
        #[ORM\ManyToOne()]
        #[ORM\JoinColumn()]
        private ?DbCarlGmaoActeur $createdBy,
        #[ORM\Column(type: 'datetime_immutable')]
        private \DateTimeImmutable $beginDate,
        #[ORM\Column(type: 'datetime_immutable')]
        private \DateTimeImmutable $endDate,
        #[ORM\Column(length: 20)]
        private string $code,
        #[ORM\Column(type: 'text')]
        private ?string $description,
        #[ORM\Column(length: 33)]
        private string $actionTypeId,
        #[ORM\Column(length: 255)]
        private ?string $actionTypeLabel,
        #[ORM\Column(length: 1540)]
        private ?string $address,
        #[ORM\Column(precision: 9, scale: 6)]
        private ?float $latitude,
        #[ORM\Column(precision: 9, scale: 6)]
        private ?float $longitude,
        #[ORM\Column(length: 33)]
        private string $supervisorId,
        #[ORM\Column(length: 33)]
        private string $costcenterId,
        #[ORM\Column(length: 33)]
        private ?string $preventiveTriggerId,
        #[ORM\ManyToOne()]
        #[ORM\JoinColumn(nullable: true)]
        private ?DbCarlEquipement $directDbCarlEquipement,
        #[ORM\OneToMany(targetEntity: DbCarlInterventionDbCarlEquipement::class, mappedBy: 'dbCarlIntervention')]
        private Collection $dbCarlEquipements,
        #[ORM\OneToMany(targetEntity: DbCarlDocument::class, mappedBy: 'dbCarlIntervention')]
        private Collection $dbCarlDocuments,
        #[ORM\OneToMany(targetEntity: DbCarlInterventionStatus::class, mappedBy: 'dbCarlIntervention')]
        #[OrderBy(['createdAt' => 'DESC'])]
        private Collection $dbCarlInterventionStatuses,
        #[ORM\OneToMany(targetEntity: DbCarlDemandeIntervention::class, mappedBy: 'dbCarlIntervention')]
        private Collection $dbCarlDemandeInterventions,
        #[ORM\OneToMany(targetEntity: DbCarlRessource::class, mappedBy: 'dbCarlIntervention')]
        private Collection $dbCarlRessources,
        #[ORM\Column(length: 255)]
        private ?string $materielConsomme,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_intervention"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        $sqlColonneChampCarlPourStockageMaterielConsomme = is_null($carlClient->getChampCarlPourStockageMaterielConsomme()) ? 'null' : 'INTERVENTION.'.$carlClient->getChampCarlPourStockageMaterielConsomme();

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id                      varchar(33)     OPTIONS (key 'true') NOT NULL,
            title                   varchar(60),
            work_priority           varchar(255),
            status_code             varchar(255),
            status_label            varchar(60),
            status_created_by       varchar(255),
            status_created_at       timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            created_at              timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            updated_at              timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            created_by_id           varchar(33) NOT NULL,
            begin_date              timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            end_date                timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            code                    varchar(20),
            description             text,
            action_type_id          varchar(33),
            action_type_label       varchar(255),
            address                 varchar(1540),
            latitude                numeric(9,6),
            longitude               numeric(9,6),
            supervisor_id          varchar(33),
            costcenter_id          varchar(33),
            preventive_trigger_id          varchar(33),
            direct_db_carl_equipement_id   varchar(33),
            materiel_consomme   varchar(255)
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with INTERVENTION_DIRECT_EQUIPEMENT as (
                select
                    INTERVENTION_EQUIPMENT.WO_ID as INTERVENTION_ID,
                    coalesce(MAX(INTERVENTION_MATERIAL.ID),MAX(INTERVENTION_BOX.ID)) as EQUIPEMENT_ID
                from "$dbSchema".CSWO_WOEQPT INTERVENTION_EQUIPMENT
                    left join "$dbSchema".CSEQ_MATERIAL INTERVENTION_MATERIAL on INTERVENTION_MATERIAL.ID = INTERVENTION_EQUIPMENT.EQPT_ID
                    left join "$dbSchema".CSEQ_BOX INTERVENTION_BOX on INTERVENTION_BOX.ID = INTERVENTION_EQUIPMENT.EQPT_ID
                where INTERVENTION_EQUIPMENT.DIRECTEQPT = 1
                group by INTERVENTION_EQUIPMENT.WO_ID
            ),
            INTERVENTION_ADDRESS as (
                select
                    ((CASE when WO_ADDRESS."NUMBER" = -1 THEN '''' ELSE TO_CHAR(WO_ADDRESS."NUMBER") END) || '' '' || WO_ADDRESS.MENTION || '' '' || WO_ADDRESS.ADDRESS1 || '' ''|| WO_ADDRESS.ADDRESS2 ||'' ''|| WO_ADDRESS.ADDRESS3 || '' '' || WO_ADDRESS.ZIPCODE || '' '' || WO_ADDRESS.CITY) as ADDRESS,
                    WO_ADDRESS.WO_ID as INTERVENTION_ID
                from "$dbSchema".CSWO_WOADDRESS WO_ADDRESS
                where WO_ADDRESS.MAINADDRESS = 1
            ),
            INTERVENTION_STEP as (
                select STEP.CODE as CODE, STEP.DESCRIPTION as DESCRIPTION from "$dbSchema".CSSY_STEP STEP
                inner join "$dbSchema".CSSY_STATUSFLOW STATUSFLOW on STATUSFLOW.ID = STEP.STATUSFLOW_ID
                inner join "$dbSchema".CSSY_OBJECTINFO OBJECTINFO on OBJECTINFO.STATUSFLOW = STATUSFLOW.CODE and OBJECTINFO.CODE = ''WO''
            )
            select
                INTERVENTION.ID as ID,
                INTERVENTION.DESCRIPTION as TITLE,
                INTERVENTION.WORKPRIORITY as WORK_PRIORITY,
                INTERVENTION.STATUS_CODE as STATUS_CODE,
                INTERVENTION_STEP.DESCRIPTION as STATUS_LABEL,
                STATUS_CREATEDBY_ACTOR.FULLNAME as STATUS_CREATED_BY,
                FROM_TZ(INTERVENTION.STATUS_CHANGEDDATE, ''GMT'') as STATUS_CREATED_AT,
                FROM_TZ(INTERVENTION.CREATEDATE, ''GMT'') as CREATED_AT,
                FROM_TZ(INTERVENTION.MODIFYDATE, ''GMT'') as UPDATED_AT,
                INTERVENTION.CREATEDBY_ID as CREATED_BY_ID,
                FROM_TZ(INTERVENTION.WOBEGIN, ''GMT'') as BEGIN_DATE,
                FROM_TZ(INTERVENTION.WOEND, ''GMT'') as END_DATE,
                INTERVENTION.CODE as CODE,
                INTERVENTION_DESCRIPTION.RAWDESCRIPTION as DESCRIPTION,
                INTERVENTION_ACTIONTYPE.ID as ACTION_TYPE_ID,
                INTERVENTION_ACTIONTYPE.DESCRIPTION as ACTION_TYPE_LABEL,
                INTERVENTION_ADDRESS.ADDRESS as ADDRESS,
                INTERVENTION.LATITUDE as LATITUDE,
                INTERVENTION.LONGITUDE as LONGITUDE,
                INTERVENTION.SUPERVISOR_ID as SUPERVISOR_ID,
                INTERVENTION.COSTCENTER_ID as COSTCENTER_ID,
                INTERVENTION.PREVENTIVETRIGGER_ID as PREVENTIVE_TRIGGER_ID,
                INTERVENTION_DIRECT_EQUIPEMENT.EQUIPEMENT_ID as DB_CARL_EQUIPEMENT_ID,
                $sqlColonneChampCarlPourStockageMaterielConsomme as MATERIEL_CONSOMME
            from "$dbSchema".CSWO_WO INTERVENTION
            left join "$dbSchema".CSSY_DESCRIPTION INTERVENTION_DESCRIPTION on INTERVENTION_DESCRIPTION.ID = INTERVENTION."COMMENT"
            left join "$dbSchema".CSWO_ACTIONTYPE INTERVENTION_ACTIONTYPE on INTERVENTION_ACTIONTYPE.ID = INTERVENTION.ACTIONTYPE_ID
            left join INTERVENTION_ADDRESS on INTERVENTION_ADDRESS.INTERVENTION_ID = INTERVENTION.ID
            left join INTERVENTION_DIRECT_EQUIPEMENT on INTERVENTION_DIRECT_EQUIPEMENT.INTERVENTION_ID = INTERVENTION.ID
            left join INTERVENTION_STEP on INTERVENTION_STEP.CODE = INTERVENTION.STATUS_CODE
            left join "$dbSchema".CSSY_ACTOR STATUS_CREATEDBY_ACTOR ON STATUS_CREATEDBY_ACTOR.ID = INTERVENTION.STATUS_CHANGEDBY_ID
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getDbCarlInterventionStatuses(): Collection
    {
        return $this->dbCarlInterventionStatuses;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getCreatedBy(): DbCarlGmaoActeur
    {
        return $this->createdBy;
    }

    public function getBeginDate(): \DateTimeImmutable
    {
        return $this->beginDate;
    }

    public function getEndDate(): \DateTimeImmutable
    {
        return $this->endDate;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    public function getActionTypeId(): string
    {
        return $this->actionTypeId;
    }

    public function getActionTypeLabel(): string
    {
        return $this->actionTypeLabel ?? '';
    }

    public function getAddress(): ?string
    {
        if (is_null($this->address)) {
            return null;
        }

        return str_replace( // de part la construction de l'address des doubles espaces peuvent apparaitre
            '  ',
            ' ',
            $this->address
        );
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function getPreventiveTriggerId(): ?string
    {
        return $this->preventiveTriggerId;
    }

    public function getWorkPriority(): string
    {
        return $this->workPriority;
    }

    public function getStatusCode(): string
    {
        return $this->statusCode;
    }

    public function getStatusLabel(): ?string
    {
        return $this->statusLabel;
    }

    public function getStatusCreatedBy(): ?string
    {
        return $this->statusCreatedBy;
    }

    public function getStatusCreatedAt(): \DateTime
    {
        return $this->statusCreatedAt;
    }

    public function getDirectDbCarlEquipement(): ?DbCarlEquipement
    {
        return $this->directDbCarlEquipement;
    }

    public function getDbCarlEquipements(): Collection
    {
        return $this->dbCarlEquipements;
    }

    public function getDbCarlDocuments(): Collection
    {
        return $this->dbCarlDocuments;
    }

    public function getDbCarlDemandeInterventions(): Collection
    {
        return $this->dbCarlDemandeInterventions;
    }

    public function getDbCarlRessources(): Collection
    {
        return $this->dbCarlRessources;
    }

    public function getMaterielConsomme(): ?string
    {
        return $this->materielConsomme;
    }

    public function toIntervention(InterventionServiceOffer $serviceOffer, CarlConfigurationIntervention $gmaoCarl, bool $includeDemandes = false): Intervention
    {
        $priorityMapping = $gmaoCarl->getCarlClient()->getPriorityMapping()[$this->getWorkPriority()];
        $imagesAvant = [];
        $imagesAprès = [];
        foreach ($this->getDbCarlDocuments()->toArray() as $dbCarlDocument) {
            switch ($dbCarlDocument->getTypeId()) {
                case $gmaoCarl->getPhotoApresInterventionDoctypeId():
                    $imagesAprès[] = $dbCarlDocument->toImage();
                    break;
                case $gmaoCarl->getPhotoAvantInterventionDoctypeId():
                    $imagesAvant[] = $dbCarlDocument->toImage();
                    break;
                default:
                    break;
            }
        }

        $demandeIds = $includeDemandes ? $this->getDbCarlDemandeInterventions()->map(
            function (DbCarlDemandeIntervention $dbCarlDemandeIntervention) {
                return $dbCarlDemandeIntervention->getId();
            })->toArray() : null;

        return new Intervention(
            priority: $priorityMapping,
            title: $this->getTitle() ?? '',
            description: $this->getDescription(),
            actionType: new ActionType(
                id: $this->getActionTypeId(),
                label: $this->getActionTypeLabel(),
            ),
            latitude: $this->getLatitude(),
            longitude: $this->getLongitude(),
            relatedEquipement: $this->getDirectDbCarlEquipement()?->toEquipement(serviceOffer: $serviceOffer, gmaoCarl: $gmaoCarl),
            id: $this->getId(),
            createdAt: $this->getCreatedAt(),
            address: $this->getAddress(),
            createdBy: $this->getCreatedBy()->toGmaoActeur(),
            dateDeDebut: $this->getBeginDate(),
            dateDeFin: $this->getEndDate(),
            code: $this->getCode(),
            imagesAvant: $imagesAvant,
            imagesApres: $imagesAprès,
            currentStatus: new InterventionStatus(
                code: CarlClient::getSilabStatusCodeFromCarlStatusCode($this->getStatusCode(), CarlClient::SILAB_TO_CARL_INTERVENTION_STATUS_MAPPING),
                label: $this->getStatusLabel() ?? 'étape inconnue',
                createdBy: $this->getStatusCreatedBy() ?? 'acteur sans nom',
                createdAt: $this->getStatusCreatedAt()
            ),
            gmaoObjectViewUrl: null,
            demandeInterventionIds: $demandeIds,// Probleme : accéder aux demande possède un cout proportionnel au nombre total de résultats de la pagination, je ne sais pas comment régler ça,
            documentsJoints: $this->getDbCarlDocuments()->map(
                function (DbCarlDocument $dbCarlDocument) {
                    return $dbCarlDocument->toDocument();
                }
            )->toArray(),
            intervenantsAffectes: $this->getDbCarlRessources()->map(
                function (DbCarlRessource $dbCarlRessource) {
                    return $dbCarlRessource->getDbCarlGmaoActeur()->toGmaoActeur();
                }
            )->toArray(),
            materielConsomme: $this->getMaterielConsomme()
        );
    }
}
