<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\Intervention\Entity\Intervention\InterventionCoordinates;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlInterventionCoordinatesRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlInterventionCoordinatesRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlInterventionCoordinates implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 60)]
        private ?string $title,
        #[ORM\Column(length: 255)]
        private string $workPriority,
        #[ORM\Column(length: 255)]
        private string $statusCode,
        #[ORM\Column()]
        private \DateTime $createdAt,
        #[ORM\Column()]
        private \DateTime $updatedAt,
        #[ORM\Column(type: 'datetime_immutable')]
        private \DateTimeImmutable $beginDate,
        #[ORM\Column(type: 'datetime_immutable')]
        private \DateTimeImmutable $endDate,
        #[ORM\Column(length: 20)]
        private string $code,
        #[ORM\Column(length: 33)]
        private string $actionTypeId,
        #[ORM\Column(precision: 9, scale: 6)]
        private float $latitude,
        #[ORM\Column(precision: 9, scale: 6)]
        private float $longitude,
        #[ORM\Column(length: 33)]
        private string $supervisorId,
        #[ORM\Column(length: 33)]
        private string $costcenterId,
        #[ORM\OneToMany(targetEntity: DbCarlInterventionDbCarlEquipement::class, mappedBy: 'dbCarlInterventionCoordinates')]
        private Collection $dbCarlEquipements,
        #[ORM\OneToMany(targetEntity: DbCarlRessource::class, mappedBy: 'dbCarlIntervention')]
        private Collection $dbCarlRessources,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_intervention_coordinates"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id                      varchar(33)     OPTIONS (key 'true') NOT NULL,
            title                   varchar(60),
            work_priority           varchar(255),
            status_code             varchar(255),
            created_at              timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            updated_at              timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            begin_date              timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            end_date                timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            code                    varchar(20),
            action_type_id          varchar(33),
            latitude                numeric(9,6) NOT NULL,
            longitude               numeric(9,6) NOT NULL,
            supervisor_id          varchar(33),
            costcenter_id          varchar(33)
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with INTERVENTION_DIRECT_EQUIPEMENT as (
                select
                    INTERVENTION_EQUIPMENT.WO_ID as INTERVENTION_ID,
                    coalesce(MAX(INTERVENTION_MATERIAL.ID),MAX(INTERVENTION_BOX.ID)) as EQUIPEMENT_ID
                from "$dbSchema".CSWO_WOEQPT INTERVENTION_EQUIPMENT
                    left join "$dbSchema".CSEQ_MATERIAL INTERVENTION_MATERIAL on INTERVENTION_MATERIAL.ID = INTERVENTION_EQUIPMENT.EQPT_ID
                    left join "$dbSchema".CSEQ_BOX INTERVENTION_BOX on INTERVENTION_BOX.ID = INTERVENTION_EQUIPMENT.EQPT_ID
                where INTERVENTION_EQUIPMENT.DIRECTEQPT = 1
                group by INTERVENTION_EQUIPMENT.WO_ID
            )
            select
                INTERVENTION.ID as ID,
                INTERVENTION.DESCRIPTION as TITLE,
                INTERVENTION.WORKPRIORITY as WORK_PRIORITY,
                INTERVENTION.STATUS_CODE as STATUS_CODE,
                FROM_TZ(INTERVENTION.CREATEDATE, ''GMT'') as CREATED_AT,
                FROM_TZ(INTERVENTION.MODIFYDATE, ''GMT'') as UPDATED_AT,
                FROM_TZ(INTERVENTION.WOBEGIN, ''GMT'') as BEGIN_DATE,
                FROM_TZ(INTERVENTION.WOEND, ''GMT'') as END_DATE,
                INTERVENTION.CODE as CODE,
                INTERVENTION.ACTIONTYPE_ID as ACTION_TYPE_ID,
                coalesce(INTERVENTION.LATITUDE, DIRECT_EQUIPEMENT.LATITUDE) as LATITUDE,
                coalesce(INTERVENTION.LONGITUDE, DIRECT_EQUIPEMENT.LONGITUDE) as LONGITUDE,
                INTERVENTION.SUPERVISOR_ID as SUPERVISOR_ID,
                INTERVENTION.COSTCENTER_ID as COSTCENTER_ID
            from "$dbSchema".CSWO_WO INTERVENTION
                left join INTERVENTION_DIRECT_EQUIPEMENT on INTERVENTION_DIRECT_EQUIPEMENT.INTERVENTION_ID = INTERVENTION.ID
                left join "$dbSchema".CSEQ_EQUIPMENT DIRECT_EQUIPEMENT on DIRECT_EQUIPEMENT.ID = INTERVENTION_DIRECT_EQUIPEMENT.EQUIPEMENT_ID
            where
                coalesce(INTERVENTION.LATITUDE, DIRECT_EQUIPEMENT.LATITUDE) IS NOT NULL
                and
                coalesce(INTERVENTION.LONGITUDE, DIRECT_EQUIPEMENT.LONGITUDE) IS NOT NULL
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getActionTypeId(): string
    {
        return $this->actionTypeId;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function getWorkPriority(): string
    {
        return $this->workPriority;
    }

    public function getStatusCode(): string
    {
        return $this->statusCode;
    }

    public function getBeginDate(): \DateTimeImmutable
    {
        return $this->beginDate;
    }

    public function getEndDate(): \DateTimeImmutable
    {
        return $this->endDate;
    }

    public function toInterventionCoordinates(): InterventionCoordinates
    {
        return new InterventionCoordinates(
            id: $this->id,
            label: trim($this->code.' '.($this->title ?? '')),
            latitude: $this->latitude,
            longitude: $this->longitude
        );
    }
}
