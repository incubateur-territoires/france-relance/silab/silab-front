<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlInterventionDbCarlEquipementRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlInterventionDbCarlEquipementRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlInterventionDbCarlEquipement implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\ManyToOne()]
        #[JoinColumn()]
        private DbCarlIntervention $dbCarlIntervention,
        #[ORM\Id]
        #[ORM\ManyToOne()]
        #[JoinColumn()]
        private DbCarlInterventionCoordinates $dbCarlInterventionCoordinates,
        #[ORM\Id]
        #[ORM\Column()]
        private bool $directEqpt,
        #[ORM\Id]
        #[ORM\ManyToOne()]
        #[JoinColumn()]
        private DbCarlEquipement $dbCarlEquipement,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_intervention_db_carl_equipement"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            db_carl_intervention_id     varchar(33)     OPTIONS (key 'true')  NOT NULL,
            db_carl_intervention_coordinates_id     varchar(33)     OPTIONS (key 'true')  NOT NULL,
            db_carl_equipement_id       varchar(33)     OPTIONS (key 'true')  NOT NULL,
            direct_eqpt                 boolean         OPTIONS (key 'true')  NOT NULL
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            select
                INTERVENTION_EQUIPMENT.WO_ID as DB_CARL_INTERVENTION_ID,
                INTERVENTION_EQUIPMENT.WO_ID as DB_CARL_INTERVENTION_COORDINATES_ID,
                INTERVENTION_EQUIPMENT.EQPT_ID as DB_CARL_EQUIPEMENT_ID,
                INTERVENTION_EQUIPMENT.DIRECTEQPT as "DIRECT_EQPT"
            from "$dbSchema".CSWO_WOEQPT INTERVENTION_EQUIPMENT
       )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getDbCarlIntervention(): DbCarlIntervention
    {
        return $this->dbCarlIntervention;
    }

    public function getDirectEqpt(): bool
    {
        return $this->directEqpt;
    }

    public function getDbCarlEquipement(): DbCarlEquipement
    {
        return $this->dbCarlEquipement;
    }
}
