<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\Intervention\Entity\Status\InterventionStatusHistorised;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlInterventionStatusRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlInterventionStatusRepository::class,
)]
#[ORM\Table(schema: 'carl')]
class DbCarlInterventionStatus implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 20)]
        private string $code,
        #[ORM\Column(length: 60)]
        private ?string $label,
        #[ORM\Column(nullable: true, length: 255)]
        private string $createdBy,
        #[ORM\Column()]
        private \DateTimeImmutable $createdAt,
        #[ORM\Column(nullable: true, length: 250)]
        private string $comment,
        #[ORM\ManyToOne(inversedBy: 'dbCarlInterventionStatuses')]
        #[JoinColumn()]
        private DbCarlIntervention $dbCarlIntervention
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_intervention_status"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id        varchar(33)           OPTIONS (key 'true')  NOT NULL,
            code      varchar(20),
            label  varchar(60),
            created_at  timestamptz(3),--attention, carl stocke en GMT et pas sur le fuseau de la DB
            "comment"  varchar(250),
            created_by  varchar(255),
            db_carl_intervention_id varchar(33)
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with ACTOR_UNIQUE_EMAIL as (
                select PHONE.ACTOR_ID, min(PHONE.PHONENUM) as EMAIL from "$dbSchema".CSSY_PHONE PHONE where PHONE.TYPE=''EMAIL'' group by PHONE.ACTOR_ID
            ),
            INTERVENTION_STEP as (
                select STEP.CODE as CODE, STEP.DESCRIPTION as DESCRIPTION from "$dbSchema".CSSY_STEP STEP
                inner join "$dbSchema".CSSY_STATUSFLOW STATUSFLOW on STATUSFLOW.ID = STEP.STATUSFLOW_ID
                inner join "$dbSchema".CSSY_OBJECTINFO OBJECTINFO on OBJECTINFO.STATUSFLOW = STATUSFLOW.CODE and OBJECTINFO.CODE = ''WO''
            )
            select
                WO_STATUS.ID as STATUS_ID,
                WO_STATUS.STATUS_CODE as CODE,
                INTERVENTION_STEP.DESCRIPTION as LABEL,
                FROM_TZ(WO_STATUS.STATUS_CHANGEDDATE, ''GMT'') as CREATED_AT,
                WO_STATUS."COMMENT" as COMMENTAIRE,
                ACTOR_UNIQUE_EMAIL.EMAIL as CREATEDBY,
                WO.ID as INTERVENTION_ID
            from
                "$dbSchema".CSWO_WOSTATUS WO_STATUS
                left join "$dbSchema".CSWO_WO WO on WO_STATUS.ORIGIN_ID = WO.ID
                left join INTERVENTION_STEP on INTERVENTION_STEP.CODE = WO_STATUS.STATUS_CODE
                left join ACTOR_UNIQUE_EMAIL on WO_STATUS.STATUS_CHANGEDBY_ID = ACTOR_UNIQUE_EMAIL.ACTOR_ID
       )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function getCreatedBy(): string
    {
        return $this->createdBy ?? '';
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getComment(): string
    {
        return $this->comment ?? '';
    }

    public function getInterventionCalculated(): DbCarlIntervention
    {
        return $this->dbCarlIntervention;
    }

    public function toInterventionStatusHistorised(): InterventionStatusHistorised
    {
        return new InterventionStatusHistorised(
            code: CarlClient::getSilabStatusCodeFromCarlStatusCode(
                carlStatusCode: $this->getCode(),
                silabToCarlStatusMapping: CarlClient::SILAB_TO_CARL_INTERVENTION_STATUS_MAPPING
            ),
            label: $this->getLabel() ?? 'étape inconnue',
            createdBy: $this->getCreatedBy(),
            createdAt: $this->getCreatedAt(),
            comment: $this->getComment()
        );
    }

    /**
     * @param array<DbCarlInterventionStatus> $dbCarlInterventionStatuses
     *
     * @return array<InterventionStatusHistorised>
     */
    public static function toInterventionStatusHistorisedArray(array $dbCarlInterventionStatuses): array
    {
        return array_map(
            function ($dbCarlInterventionStatus) {
                return $dbCarlInterventionStatus->toInterventionStatusHistorised();
            }, $dbCarlInterventionStatuses
        );
    }
}
