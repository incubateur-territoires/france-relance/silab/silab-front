<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\Equipement\Entity\Mesure\Mesure;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlMesureRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlMesureRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlMesure implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 33)] // attention c'est "théoriquement" nullable dans la base carl
        private string $compteurId,
        #[ORM\Column(precision: 15, scale: 4)] // attention c'est "théoriquement" nullable dans la base carl
        private float $valeur,
        #[ORM\Column()] // attention c'est "théoriquement" nullable dans la base carl
        private ?\DateTimeImmutable $createdAt,
        #[ORM\Column(nullable: true, length: 255)]
        private ?string $createdBy,// Note: on pourrait faire le lien avec les acteurs, mais pour l'instant pas besoin,
        // , on fait comme avant, à faire quand le moment venu sera venu
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_odsint_db_carl_mesure"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id        varchar(33)           OPTIONS (key 'true')  NOT NULL,
            compteur_id        varchar(33),
            valeur numeric(15,4),
            created_at timestamptz(3),
            created_by varchar(255)
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
        with MEASURE_ACTOR_UNIQUE_EMAIL as (
            select PHONE.ACTOR_ID, min(PHONE.PHONENUM) as EMAIL from "$dbSchema".CSSY_PHONE PHONE where PHONE.TYPE=''EMAIL'' group by PHONE.ACTOR_ID
        )
        select
            MEASUREREADING.ID as ID,
            MEASUREREADING.MEASUREPOINT_ID as COMPTEUR_ID,
            MEASUREREADING.MEASURE as VALEUR,
            MEASUREREADING.DATEMEASURE as CREATED_AT,
            MEASUREREADING.CREATEDBY_ID as CREATED_BY
        from
            "$dbSchema".CSEQ_MEASUREREADING MEASUREREADING
            left join MEASURE_ACTOR_UNIQUE_EMAIL on MEASURE_ACTOR_UNIQUE_EMAIL.ACTOR_ID = MEASUREREADING.CREATEDBY_ID
    )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCompteurId(): string
    {
        return $this->compteurId;
    }

    public function getValeur(): float
    {
        return $this->valeur;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function toMesure(): Mesure
    {
        return new Mesure(
            valeur: $this->getValeur(),
            createdAt: $this->getCreatedAt(),
            createdBy: $this->getCreatedBy() ?? ''
        );
    }

    /**
     * @param array<DbCarlMesure> $dbCarlMesures
     *
     * @return array<Mesure>
     */
    public static function toMesureArray(array $dbCarlMesures): array
    {
        return array_map(
            function ($dbCarlMesure) {
                return $dbCarlMesure->toMesure();
            },
            $dbCarlMesures
        );
    }
}
