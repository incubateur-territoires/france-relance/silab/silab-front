<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\Intervention\Entity\Activite\Operation;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlCompteurRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlCompteurRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlOperation implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 60)]
        private ?string $label
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_operation"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
    CREATE FOREIGN TABLE $dbCarlTableName (
                id              varchar(33)           OPTIONS (key 'true')  NOT NULL,
                label           varchar(60)
              
           ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            select
                PROCESS.ID,
                PROCESS.DESCRIPTION as LABEL
            from "$dbSchema".CSWO_PROCESS PROCESS
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function toOperation(): Operation
    {
        return new Operation(
            id: $this->getId(),
            label: $this->getLabel() ?? ''
        );
    }
}
