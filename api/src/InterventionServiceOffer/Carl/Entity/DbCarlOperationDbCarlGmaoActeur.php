<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlOperationDbCarlGmaoActeurRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlOperationDbCarlGmaoActeurRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlOperationDbCarlGmaoActeur implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\ManyToOne()]
        #[JoinColumn()]
        private DbCarlOperation $dbCarlOperation,
        #[ORM\Id]
        #[ORM\ManyToOne()]
        #[JoinColumn()]
        private DbCarlGmaoActeur $dbCarlGmaoActeur,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_operation_db_carl_gmao_acteur"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
    CREATE FOREIGN TABLE $dbCarlTableName (
                db_carl_operation_id              varchar(33)           OPTIONS (key 'true')  NOT NULL,
                db_carl_gmao_acteur_id              varchar(33)           OPTIONS (key 'true')  NOT NULL
           ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
                select
                    "PROCESS".ID as PROCESS_ID,
                    "USER".ACTOR_ID as ACTOR_ID
                from "$dbSchema".CSSY_USERS "USER"
                    left join "$dbSchema".CSSY_TECHACCESSGROUP TECHACCESSGROUP on TECHACCESSGROUP.ID = "USER".GROUPTECH_ID
                    left join "$dbSchema".CSSY_TECHGROUPENTITY TECHGROUPENTITY on TECHGROUPENTITY.TECHACCESSGROUP_ID = TECHACCESSGROUP.ID and TECHGROUPENTITY.ENTITY_ID = ''PROCESS''
                    left join "$dbSchema".CSSY_TECHGROUPRIGHT TECHGROUPRIGHT on TECHGROUPRIGHT.TECHGROUPENTITY_ID = TECHGROUPENTITY.ID and TECHGROUPRIGHT.RIGHT_ID = ''READ''
                    left join "$dbSchema".CSSY_ACL ACL on ACL.ID = TECHGROUPRIGHT.ACL_ID
                    inner join "$dbSchema".CSWO_PROCESS "PROCESS" on "PROCESS".ID = ACL.VALUE or TECHGROUPENTITY.ALLACCESS = 1 or "USER".GROUPTECH_ID is null
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getDbCarlOperation(): DbCarlOperation
    {
        return $this->dbCarlOperation;
    }

    public function getDbCarlGmaoActeur(): DbCarlGmaoActeur
    {
        return $this->dbCarlGmaoActeur;
    }
}
