<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlCompteurRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlCompteurRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlReleveDeCompteur implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(nullable: true, length: 33)]
        private ?string $idMesure,
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $idCompteur,// Note: le lien n'est pas fait avec DbCarlCompteur car on en à pas l'intéret pour le moment
        #[ORM\Column(nullable: true, precision: 15, scale: 4)]
        private ?float $mesure,
        #[ORM\Column(nullable: true)]
        private ?\DateTimeImmutable $dateMesure
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_releve_de_compteur"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id_mesure           varchar(33)             OPTIONS (key 'true'),
            id_compteur         varchar(33)             OPTIONS (key 'true'),
            mesure              numeric(15,4),
            date_mesure         timestamptz(3)
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
        select
            MEASURE_READING.ID as ID_MESURE,
            MEASURE_POINT.ID as ID_COMPTEUR,
            MEASURE_READING.MEASURE as MESURE,
            MEASURE_READING.DATEMEASURE as DATE_MESURE

            from "$dbSchema".CSEQ_MEASUREPOINT MEASURE_POINT

            left join "$dbSchema".CSEQ_MEASUREREADING MEASURE_READING
                on MEASURE_READING.MEASUREPOINT_ID = MEASURE_POINT.ID
    )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getIdMesure(): ?string
    {
        return $this->idMesure;
    }

    public function getIdCompteur(): string
    {
        return $this->idCompteur;
    }

    public function getMesure(): ?float
    {
        return $this->mesure;
    }

    public function getDateMesure(): ?\DateTimeImmutable
    {
        return $this->dateMesure;
    }
}
