<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlRessourceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlRessourceRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlRessource implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\ManyToOne()]
        private DbCarlIntervention $dbCarlIntervention,
        #[ORM\Id]
        #[ORM\ManyToOne()]
        private DbCarlGmaoActeur $dbCarlGmaoActeur
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_ressource"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            db_carl_intervention_id     varchar(33)     NOT NULL,
            db_carl_gmao_acteur_id      varchar(33)     NOT NULL
        ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with VALID_INTERVENANT as (
                select
                    *
                from
                    "$dbSchema".CSRE_TECHNICIAN INTERVENANT
                where
                    INTERVENANT.STATUS_CODE = ''ACTIVE''
            )
            select
                RESSOURCE.WO_ID as INTERVENTION_ID,
                VALID_INTERVENANT.ACTOR_ID as GMAO_ACTEUR_ID,
                VALID_INTERVENANT.TEAM_ID as TEAM_ID
            from "$dbSchema".CSWO_WORESOURCES RESSOURCE
                inner join VALID_INTERVENANT on (VALID_INTERVENANT.ID = RESSOURCE.TECHNICIAN_ID or (RESSOURCE.TECHNICIAN_ID is null and VALID_INTERVENANT.TEAM_ID = RESSOURCE.TEAM_ID))
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getDbCarlIntervention(): DbCarlIntervention
    {
        return $this->dbCarlIntervention;
    }

    public function getDbCarlGmaoActeur(): DbCarlGmaoActeur
    {
        return $this->dbCarlGmaoActeur;
    }
}
