<?php

namespace App\InterventionServiceOffer\Carl\Entity;

use App\InterventionServiceOffer\SearchEntity\Entity\SearchEntity\SearchEntity;
use App\InterventionServiceOffer\SearchEntity\Entity\SearchEntity\SearchEntityType;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlSearchEntityRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlSearchEntityRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlSearchEntity implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(type: 'string', enumType: SearchEntityType::class)]
        private SearchEntityType $type,
        #[ORM\Id]
        #[ORM\Column(length: 33)]
        private string $id,
        #[ORM\Column(length: 255)]
        private ?string $code,
        #[ORM\Column(length: 255)]
        private ?string $label
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_intervention_db_carl_search_entity"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            type        varchar(33)     OPTIONS (key 'true')    NOT NULL,
            id          varchar(33)     OPTIONS (key 'true')    NOT NULL,
            code        varchar(255),
            label       varchar(255)
        ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with EQUIPEMENT_SEARCH_ENTITY as (
                select
                    ''équipements'' as TYPE,
                    EQUIPMENT.ID as ID,
                    EQUIPMENT.CODE as CODE,
                    EQUIPMENT.DESCRIPTION as LABEL
                from
                    "$dbSchema".CSEQ_EQUIPMENT EQUIPMENT
            ),
            INTERVENTION_SEARCH_ENTITY as (
                select
                    ''interventions'' as TYPE,
                    WO.ID as ID,
                    WO.CODE as CODE,
                    WO.DESCRIPTION as LABEL
                from
                    "$dbSchema".CSWO_WO WO
            ),
            DEMANDE_SEARCH_ENTITY as (
                select
                    ''demandes'' as TYPE,
                    MR.ID as ID,
                    MR.CODE as CODE,
                    MR.DESCRIPTION as LABEL
                from
                    "$dbSchema".CSWO_MR MR
            ),
            ALL_SEARCH_ENTITY as (
                select * from EQUIPEMENT_SEARCH_ENTITY
                union
                select * from INTERVENTION_SEARCH_ENTITY
                union
                select * from DEMANDE_SEARCH_ENTITY
            )
            select
                SEARCH_ENTITY.TYPE,
                SEARCH_ENTITY.ID,
                SEARCH_ENTITY.CODE,
                SEARCH_ENTITY.LABEL
            from ALL_SEARCH_ENTITY SEARCH_ENTITY
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getType(): SearchEntityType
    {
        return $this->type;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function toSearchEntity(): SearchEntity
    {
        return new SearchEntity(
            type: $this->getType(),
            id: $this->getId(),
            code: $this->getCode(),
            label: $this->getLabel()
        );
    }

    /**
     * @param array<DbCarlSearchEntity> $dbCarlSearchEntities
     *
     * @return array<SearchEntity>
     */
    public static function toSearchEntityArray(array $dbCarlSearchEntities): array
    {
        return array_map(
            function ($dbCarlSearchEntity) {
                return $dbCarlSearchEntity->toSearchEntity();
            },
            $dbCarlSearchEntities
        );
    }
}
