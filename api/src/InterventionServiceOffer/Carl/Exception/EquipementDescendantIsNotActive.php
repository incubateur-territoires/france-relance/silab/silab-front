<?php

namespace App\InterventionServiceOffer\Carl\Exception;

use App\Shared\Exception\RuntimeException;

class EquipementDescendantIsNotActive extends RuntimeException
{
    public function __construct()
    {
        parent::__construct(message: "L'équipement descendant n'est pas actif");
    }
}
