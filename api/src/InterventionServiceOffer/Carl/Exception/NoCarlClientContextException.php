<?php

namespace App\InterventionServiceOffer\Carl\Exception;

use App\Shared\Exception\RuntimeException;

class NoCarlClientContextException extends RuntimeException
{
    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct(message: 'Aucun contexte de client Carl', previous: $previous);
    }
}
