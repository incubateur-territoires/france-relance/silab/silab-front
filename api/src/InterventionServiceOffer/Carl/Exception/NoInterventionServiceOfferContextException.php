<?php

namespace App\InterventionServiceOffer\Carl\Exception;

class NoInterventionServiceOfferContextException extends \RuntimeException
{
    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct(message: "Aucun contexte d'offre de service intervention", previous: $previous);
    }
}
