<?php

namespace App\InterventionServiceOffer\Carl\Exception;

use App\Shared\Exception\BadRequestException;

class OperationProcessIdIsNullException extends BadRequestException
{
    public function __construct(string $idOperation)
    {
        parent::__construct(
            message: 'L\'Opération '.$idOperation.' est mal configuré sur Carl, elle n\'a pas de process rataché. Merci de vous rapprocher d\'un administrateur.');
    }
}
