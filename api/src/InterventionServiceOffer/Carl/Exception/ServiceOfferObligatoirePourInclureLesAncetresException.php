<?php

namespace App\InterventionServiceOffer\Carl\Exception;

class ServiceOfferObligatoirePourInclureLesAncetresException extends \RuntimeException
{
    public function __construct(string $codeEquipement, ?\Throwable $previous = null)
    {
        parent::__construct(message: "Impossible d'inclure la lignée d'ancètres de l'équipement $codeEquipement car aucune offre de service n'est fournie", previous: $previous);
    }
}
