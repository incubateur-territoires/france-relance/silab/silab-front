<?php

namespace App\InterventionServiceOffer\Carl\Exception;

use App\Shared\Exception\BadRequestException;

class TechnicianNotFoundException extends BadRequestException
{
    public function __construct(string $actorName, ?\Throwable $previous = null)
    {
        parent::__construct(message: "L'acteur $actorName n'est pas liée à un technien sur carl, veuillez vous rapprocher d'un administrateur", previous: $previous);
    }
}
