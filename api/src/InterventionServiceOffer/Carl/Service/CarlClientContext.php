<?php

namespace App\InterventionServiceOffer\Carl\Service;

use App\InterventionServiceOffer\Carl\Exception\NoCarlClientContextException;
use App\InterventionServiceOffer\Gmao\Service\CarlConfigurationInterventionContext;
use App\LogisticServiceOffer\Gmao\Service\CarlConfigurationLogistiqueContext;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Exception\RuntimeException;

class CarlClientContext
{
    public function __construct(
        private CarlConfigurationInterventionContext $carlConfigurationInterventionContext,
        private CarlConfigurationLogistiqueContext $carlConfigurationLogistiqueContext,
    ) {
    }

    public function getCarlClient(): CarlClient
    {
        $carlClient = null;
        try {
            $carlClient = $this->carlConfigurationInterventionContext->getCarlConfiguration()->getCarlClient();
            if (is_null($carlClient)) {
                throw new RuntimeException('Pas de client carl attributé');
            }

            return $carlClient;
        } catch (\Throwable $e) {
        }

        try {
            $carlClient = $this->carlConfigurationLogistiqueContext->getCarlConfiguration()->getCarlClient();
            if (is_null($carlClient)) {
                throw new RuntimeException('Pas de client carl attributé');
            }

            return $carlClient;
        } catch (\Throwable $e) {
        }

        if (is_null($carlClient)) {
            throw new NoCarlClientContextException($e);
        }

        return $carlClient;
    }
}
