<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Repository\ConfigurationTableauDeMesuresRepository;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Component\Serializer\Annotation\Groups;

#[GetCollection(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/configuration-tableau-de-mesures',
    uriVariables: [
        'serviceOfferId' => new Link(
            fromClass: InterventionServiceOffer::class,
            toProperty: 'interventionServiceOffer'
        ),
    ],
    normalizationContext: ['groups' => [ConfigurationTableauDeMesures::GROUP_AFFICHER_DETAILS]],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_CONFIGURATIONS_TABLEAU_DE_MESURES')"
)]
#[Get(
    uriTemplate: '/configuration-tableau-de-mesures/{id}',
    security: 'false' // Cette route sert uniquement pour des problématiques de gestion d'IRI mal mal faites, on la bloque simplement
)]
#[ORM\Entity(repositoryClass: ConfigurationTableauDeMesuresRepository::class)]
#[ORM\Table('odsint_configuration_tableau_de_mesures')]
#[Groups([
    ConfigurationTableauDeMesures::GROUP_AFFICHER_DETAILS,
])]
class ConfigurationTableauDeMesures
{
    public const GROUP_AFFICHER_DETAILS = 'Afficher les détails du tableau de configuration';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne()]
    #[JoinColumn(nullable: false)]
    private ?InterventionServiceOffer $interventionServiceOffer = null;

    #[ORM\Column(length: 255)]
    private ?string $titre = null;

    /**
     * @var Collection<int, Ligne>
     */
    #[ORM\OneToMany(targetEntity: Ligne::class, mappedBy: 'configurationTableauDeMesures', orphanRemoval: true)]
    #[OrderBy(['ordre' => 'ASC'])]
    private ?Collection $lignes = null;

    /**
     * @var Collection<int, SousCellule>
     */
    #[ORM\OneToMany(targetEntity: SousCellule::class, mappedBy: 'configurationTableauDeMesures', orphanRemoval: true)]
    #[OrderBy(['ordre' => 'ASC'])]
    private ?Collection $sousCellules = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInterventionServiceOffer(): ?InterventionServiceOffer
    {
        return $this->interventionServiceOffer;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    /**
     * @return ?Collection<int, Ligne>
     */
    public function getLignes(): ?Collection
    {
        return $this->lignes;
    }

    /**
     * @return ?Collection<int, SousCellule>
     */
    public function getSousCellules(): ?Collection
    {
        return $this->sousCellules;
    }

    /**
     * @return array<string>
     */
    public function getTousLesIdCompteursDesSousCellules(): array
    {
        return $this->getSousCellules()->reduce(
            function (array $ids_compteurs, SousCellule $sousCellule) {
                return array_merge(
                    $ids_compteurs,
                    $sousCellule->getEmplacementsDeCompteurs()->map(function (EmplacementDeCompteur $emplacementDeCompteur) {
                        return $emplacementDeCompteur->getIdCompteur();
                    })->toArray()
                );
            },
            []
        );
    }
}
