<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity;

use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Repository\EmplacementDeCompteurRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: EmplacementDeCompteurRepository::class)]
#[ORM\Table('odsint_emplacement_de_compteur')]
#[UniqueConstraint(name: 'emplacement_dans_le_tableau', columns: ['ligne_id', 'sous_cellule_id'])]
class EmplacementDeCompteur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([ConfigurationTableauDeMesures::GROUP_AFFICHER_DETAILS])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'emplacementsDeCompteurs')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Ligne $ligne = null;

    #[ORM\ManyToOne(inversedBy: 'emplacementsDeCompteurs')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([ConfigurationTableauDeMesures::GROUP_AFFICHER_DETAILS])]
    private ?SousCellule $sousCellule = null;

    #[ORM\Column(length: 255)]
    #[Groups([ConfigurationTableauDeMesures::GROUP_AFFICHER_DETAILS])]
    private ?string $idCompteur = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLigne(): ?Ligne
    {
        return $this->ligne;
    }

    public function getSousCellule(): ?SousCellule
    {
        return $this->sousCellule;
    }

    public function getIdCompteur(): ?string
    {
        return $this->idCompteur;
    }
}
