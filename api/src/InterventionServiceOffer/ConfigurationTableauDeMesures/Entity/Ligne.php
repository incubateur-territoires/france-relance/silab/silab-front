<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity;

use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Repository\LigneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: LigneRepository::class)]
#[ORM\Table('odsint_ligne')]
#[UniqueConstraint(name: 'ordre_dans_le_tableau', columns: ['ordre', 'configuration_tableau_de_mesures_id'])]
#[Groups([ConfigurationTableauDeMesures::GROUP_AFFICHER_DETAILS])]
class Ligne
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $titre = null;

    #[ORM\Column]
    private ?int $ordre = null;

    #[ORM\ManyToOne(inversedBy: 'lignes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ConfigurationTableauDeMesures $configurationTableauDeMesures = null;

    /**
     * @var Collection<int, EmplacementDeCompteur>
     */
    #[ORM\OneToMany(targetEntity: EmplacementDeCompteur::class, mappedBy: 'ligne', orphanRemoval: true)]
    private Collection $emplacementsDeCompteurs;

    public function __construct()
    {
        $this->emplacementsDeCompteurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function getConfigurationTableauDeMesures(): ?ConfigurationTableauDeMesures
    {
        return $this->configurationTableauDeMesures;
    }

    /**
     * @return Collection<int, EmplacementDeCompteur>
     */
    public function getEmplacementsDeCompteurs(): Collection
    {
        return $this->emplacementsDeCompteurs;
    }
}
