<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity;

class LigneDeMesures
{
    public function __construct(
        private string $titre,
        /** @var array<string> */
        private array $idCompteurs,
        /** @var array<array<float|null>> */
        private array $mesures
    ) {
    }

    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @return array<string>
     */
    public function getIdCompteurs(): array
    {
        return $this->idCompteurs;
    }

    /**
     * @return array<array<float|null>>
     */
    public function getMesures(): array
    {
        return $this->mesures;
    }

    /** @param array<array<float|null>> $mesures */
    public function setMesures(array $mesures): self
    {
        $this->mesures = $mesures;

        return $this;
    }
}

class TableauDeMesures
{
    public function __construct(
        private string $titre,
        /** @var array<string> */
        private array $sousCellules,
        /** @var array<\DateTimeImmutable> */
        private array $dates,
        /** @var array<LigneDeMesures> */
        private array $lignes,
    ) {
    }

    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @return array<string>
     */
    public function getSousCellules(): array
    {
        return $this->sousCellules;
    }

    /**
     * @return array<\DateTimeImmutable>
     */
    public function getDates(): array
    {
        return $this->dates;
    }

    /**
     * @return array<LigneDeMesures>
     */
    public function getLignes(): array
    {
        return $this->lignes;
    }
}
