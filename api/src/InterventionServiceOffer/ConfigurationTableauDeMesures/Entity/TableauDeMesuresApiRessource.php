<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity;

use ApiPlatform\Metadata\Get;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\State\GetTableauDeMesuresProvider;

#[Get(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/configuration-tableau-de-mesures/{configurationTableauxDeMesuresId}/tableau-de-mesures',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
        'configurationTableauxDeMesuresId' => 'configurationTableauxDeMesuresId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme (peut-être ^^) en "id"
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_COMPTEUR')",
    output: TableauDeMesures::class,
    provider: GetTableauDeMesuresProvider::class,
)]
class TableauDeMesuresApiRessource
{
}
