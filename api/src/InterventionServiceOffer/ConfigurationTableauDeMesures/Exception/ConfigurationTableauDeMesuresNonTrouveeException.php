<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ConfigurationTableauDeMesuresNonTrouveeException extends NotFoundHttpException
{
    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct(message: "Vous n'avez paramétré de tableaux de relèves sur cette offre de service.", previous: $previous);
    }
}
