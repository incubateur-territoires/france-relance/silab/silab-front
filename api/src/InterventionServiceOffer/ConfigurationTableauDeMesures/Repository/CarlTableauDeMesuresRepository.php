<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\Repository;

use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\ConfigurationTableauDeMesures;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\EmplacementDeCompteur;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\Ligne;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\LigneDeMesures;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\SousCellule;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\TableauDeMesures;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlReleveDeCompteurRepository;

class CarlTableauDeMesuresRepository implements TableauDeMesureRepositoryInterface
{
    public function __construct(private DbCarlReleveDeCompteurRepository $dbCarlReleveDeCompteurRepository)
    {
    }

    public function getPeriode(ConfigurationTableauDeMesures $configurationTableauDeMesures, \DateTimeInterface $dateDeDebut, int $nombreDeColonnes, int $intervallesDeTempsEntreLesColonnesEnSecondes): TableauDeMesures
    {
        $intervallesDeMesures = $this->dbCarlReleveDeCompteurRepository->getPeriode(
            idCompteurs: $configurationTableauDeMesures->getTousLesIdCompteursDesSousCellules(),
            dateDeDebut: $dateDeDebut,
            nombreDeColonnes: $nombreDeColonnes,
            intervallesDeTempsEntreLesColonnesEnSecondes: $intervallesDeTempsEntreLesColonnesEnSecondes
        );

        /** @var array<LigneDeMesures> */
        $lignesDeMesures = $configurationTableauDeMesures->getLignes()->map(
            function (Ligne $ligne) {
                $idCompteurs = $ligne->getEmplacementsDeCompteurs()->map(function (EmplacementDeCompteur $emplacementDeCompteur) {return $emplacementDeCompteur->getIdCompteur(); })->toArray();

                return new LigneDeMesures(
                    titre: $ligne->getTitre(),
                    idCompteurs: $idCompteurs,
                    mesures: []// vide et rempli plus loin pour l'instant pour limiter les imbrications
                );
            }
        )->toArray();

        foreach ($lignesDeMesures as $ligneDeMesure) {
            $mesures = array_map(
                function (array $intervalleDeMesures) use ($ligneDeMesure) {
                    return array_map(
                        function (string $idCompteur) use ($intervalleDeMesures) {
                            return $intervalleDeMesures[$idCompteur];
                        },
                        $ligneDeMesure->getIdCompteurs()
                    );
                },
                array_values($intervallesDeMesures),
            );
            $ligneDeMesure->setMesures($mesures);
        }

        $datesDeDebutDesIntervalles = array_map(
            function ($dateDeDebutDIntervalleString) {
                return new \DateTimeImmutable($dateDeDebutDIntervalleString);
            },
            array_keys($intervallesDeMesures)
        );

        return new TableauDeMesures(
            titre: $configurationTableauDeMesures->getTitre(),
            sousCellules: $configurationTableauDeMesures->getSousCellules()->map(function (SousCellule $sousCellule) {return $sousCellule->getTitre(); })->toArray(),
            dates: $datesDeDebutDesIntervalles,
            lignes: $lignesDeMesures
        );
    }
}
