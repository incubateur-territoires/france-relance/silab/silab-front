<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\Repository;

use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\EmplacementDeCompteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EmplacementDeCompteur>
 */
class EmplacementDeCompteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmplacementDeCompteur::class);
    }
}
