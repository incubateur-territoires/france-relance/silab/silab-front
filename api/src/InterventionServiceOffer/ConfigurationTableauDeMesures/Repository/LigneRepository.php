<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\Repository;

use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\Ligne;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Ligne>
 */
class LigneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ligne::class);
    }
}
