<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\Repository;

use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\ConfigurationTableauDeMesures;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\TableauDeMesures;

interface TableauDeMesureRepositoryInterface
{
    public function getPeriode(ConfigurationTableauDeMesures $configurationTableauDeMesures, \DateTimeInterface $dateDeDebut, int $nombreDeColonnes, int $intervallesDeTempsEntreLesColonnesEnSecondes): TableauDeMesures;
}
