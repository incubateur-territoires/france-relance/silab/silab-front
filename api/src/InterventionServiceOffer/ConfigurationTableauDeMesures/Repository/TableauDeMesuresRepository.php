<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\Repository;

use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\ConfigurationTableauDeMesures;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\TableauDeMesures;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;

class TableauDeMesuresRepository implements TableauDeMesureRepositoryInterface
{
    private TableauDeMesureRepositoryInterface $specificTableauDeMesureRepository;

    public function __construct(
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private CarlTableauDeMesuresRepository $carlTableauDeMesureRepository
    ) {
        try {
            $this->specificTableauDeMesureRepository = match (
                $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class
            ) {
                CarlConfigurationIntervention::class => $this->carlTableauDeMesureRepository
            };
        } catch (\UnhandledMatchError $e) {
            throw new \RuntimeException('Aucun repository correspondant à la configuration de type '.$this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class);
        }
    }

    public function getPeriode(ConfigurationTableauDeMesures $configurationTableauDeMesures, \DateTimeInterface $dateDeDebut, int $nombreDeColonnes, int $intervallesDeTempsEntreLesColonnesEnSecondes): TableauDeMesures
    {
        return $this->specificTableauDeMesureRepository->getPeriode($configurationTableauDeMesures, $dateDeDebut, $nombreDeColonnes, $intervallesDeTempsEntreLesColonnesEnSecondes);
    }
}
