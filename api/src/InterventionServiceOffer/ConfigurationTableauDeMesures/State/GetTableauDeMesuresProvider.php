<?php

namespace App\InterventionServiceOffer\ConfigurationTableauDeMesures\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Entity\TableauDeMesures;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Exception\ConfigurationTableauDeMesuresNonTrouveeException;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Repository\ConfigurationTableauDeMesuresRepository;
use App\InterventionServiceOffer\ConfigurationTableauDeMesures\Repository\TableauDeMesuresRepository;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;

final class GetTableauDeMesuresProvider implements ProviderInterface
{
    use UnexpectedCriteriaCheckingTrait;

    public function __construct(
        private ConfigurationTableauDeMesuresRepository $configurationTableauDeMesuresRepository,
        private TableauDeMesuresRepository $tableauDeMesuresRepository,
        private InterventionServiceOfferContext $interventionServiceOfferContext,
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): TableauDeMesures
    {
        assert($operation instanceof Get);

        $criteria = $context['filters'] ?? [];

        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'dateDeDebut',
                'nombreDeColonnes',
                'intervallesDeTempsEntreLesColonnesEnSecondes',
            ]
        );

        $configurationTableauxDeMesuresId = (int) $uriVariables['configurationTableauxDeMesuresId'];
        $interventionServiceOfferId = $this->interventionServiceOfferContext->getServiceOfferId();

        $configurationTableauDeMesures = $this->configurationTableauDeMesuresRepository->findOneBy(['id' => $configurationTableauxDeMesuresId, 'interventionServiceOffer' => $interventionServiceOfferId]);

        if (is_null($configurationTableauDeMesures)) {
            throw new ConfigurationTableauDeMesuresNonTrouveeException();
        }

        return $this->tableauDeMesuresRepository->getPeriode(
            configurationTableauDeMesures: $configurationTableauDeMesures,
            dateDeDebut: new \DateTimeImmutable($criteria['dateDeDebut']),
            nombreDeColonnes: (int) $criteria['nombreDeColonnes'],
            intervallesDeTempsEntreLesColonnesEnSecondes: (int) $criteria['intervallesDeTempsEntreLesColonnesEnSecondes']
        );
    }
}
