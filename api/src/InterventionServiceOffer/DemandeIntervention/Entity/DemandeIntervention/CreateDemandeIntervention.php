<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;

use App\Shared\ApiPlatform\Entity\IdentifiableRelation;
use App\Shared\Location\Entity\Coordinates;

class CreateDemandeIntervention extends DemandeInterventionAttributes
{
    public function __construct(
        string $title,
        ?string $description,
        ?Coordinates $coordonnees,
        private ?IdentifiableRelation $relatedEquipement,
        ?\DateTimeImmutable $dateDeDebutSouhaitee,
        ?\DateTimeImmutable $dateDeFinSouhaitee,
    ) {
        parent::__construct(
            title: $title,
            description: $description,
            coordonnees: $coordonnees,
            dateDeDebutSouhaitee: $dateDeDebutSouhaitee,
            dateDeFinSouhaitee: $dateDeFinSouhaitee
        );
    }

    public function getRelatedEquipement(): ?IdentifiableRelation
    {
        return $this->relatedEquipement;
    }
}
