<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;

use App\InterventionServiceOffer\DemandeIntervention\Entity\Status\DemandeInterventionStatus;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\Shared\Document\Entity\DocumentInterface;
use App\Shared\Document\Entity\ImageInterface;
use App\Shared\Gmao\Entity\GmaoActeur;
use App\Shared\Location\Entity\Coordinates;

class DemandeIntervention extends DemandeInterventionAttributes
{
    public function __construct(
        string $title,
        ?string $description,
        ?Coordinates $coordonnees,
        ?\DateTimeImmutable $dateDeDebutSouhaitee,
        ?\DateTimeImmutable $dateDeFinSouhaitee,
        private ?Equipement $relatedEquipement,
        private string $id,
        private string $code,
        private DemandeInterventionStatus $currentStatus,
        /** @var array<ImageInterface> */
        private array $images,
        private GmaoActeur $createdBy,
        private \DateTime $createdAt,
        private \DateTime $updatedAt,
        private ?string $address,
        /** @var array<DocumentInterface> */
        private array $documentsJoints,
        private ?string $gmaoObjectViewUrl,
        private ?string $raisonSiNonSupprimable
    ) {
        parent::__construct(
            title: $title,
            description: $description,
            coordonnees: $coordonnees,
            dateDeDebutSouhaitee: $dateDeDebutSouhaitee,
            dateDeFinSouhaitee: $dateDeFinSouhaitee
        );
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getCurrentStatus(): DemandeInterventionStatus
    {
        return $this->currentStatus;
    }

    /** @return array<ImageInterface> */
    public function getImages(): array
    {
        return $this->images;
    }

    public function getCreatedBy(): ?GmaoActeur
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    /** @return array<DocumentInterface> */
    public function getDocumentsJoints(): array
    {
        return $this->documentsJoints;
    }

    public function getGmaoObjectViewUrl(): ?string
    {
        return $this->gmaoObjectViewUrl;
    }

    public function setGmaoObjectViewUrl(?string $gmaoObjectViewUrl): self
    {
        $this->gmaoObjectViewUrl = $gmaoObjectViewUrl;

        return $this;
    }

    public function getRelatedEquipement(): ?Equipement
    {
        return $this->relatedEquipement;
    }

    public function setRelatedEquipement(?Equipement $relatedEquipement): self
    {
        $this->relatedEquipement = $relatedEquipement;

        return $this;
    }

    public function getRaisonSiNonSupprimable(): ?string
    {
        return $this->raisonSiNonSupprimable;
    }
}
