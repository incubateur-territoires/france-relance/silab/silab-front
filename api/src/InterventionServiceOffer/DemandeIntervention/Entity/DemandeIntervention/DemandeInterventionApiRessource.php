<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\InterventionServiceOffer\DemandeIntervention\State\CreateDemandeInterventionProcessor;
use App\InterventionServiceOffer\DemandeIntervention\State\GetCollectionDemandeInterventionCoordinatesProvider;
use App\InterventionServiceOffer\DemandeIntervention\State\GetCollectionDemandeInterventionProvider;
use App\InterventionServiceOffer\DemandeIntervention\State\GetDemandeInterventionProvider;
use App\InterventionServiceOffer\DemandeIntervention\State\UpdateDemandeInterventionProcessor;

#[Post(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/demandes-interventions',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_DEMANDER_INTERVENTIONS')",
    input: CreateDemandeIntervention::class,
    processor: CreateDemandeInterventionProcessor::class,
    read: false,
)]
#[Put(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/demandes-interventions/{id}',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_MODIFIER_DEMANDES_INTERVENTIONS')",
    input: CreateDemandeIntervention::class,
    processor: UpdateDemandeInterventionProcessor::class,
    read: false,
)]
#[Get(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/demandes-interventions/{id}',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS')",
    output: DemandeIntervention::class,
    provider: GetDemandeInterventionProvider::class,
)]
#[GetCollection(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/demandes-interventions',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS')",
    output: DemandeIntervention::class,
    provider: GetCollectionDemandeInterventionProvider::class,
)]
#[GetCollection(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/demandes-interventions-coordinates',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_INTERVENTIONS')",
    output: DemandeInterventionCoordinates::class,
    provider: GetCollectionDemandeInterventionCoordinatesProvider::class,
)]
class DemandeInterventionApiRessource
{
}
