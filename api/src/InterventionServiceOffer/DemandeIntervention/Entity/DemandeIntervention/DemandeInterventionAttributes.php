<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;

use App\Shared\Location\Entity\Coordinates;

class DemandeInterventionAttributes
{
    public function __construct(
        private string $title,
        private ?string $description,
        private ?Coordinates $coordonnees,
        private ?\DateTimeImmutable $dateDeDebutSouhaitee,
        private ?\DateTimeImmutable $dateDeFinSouhaitee,
    ) {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getCoordonnees(): ?Coordinates
    {
        return $this->coordonnees;
    }

    public function getDateDeDebutSouhaitee(): ?\DateTimeImmutable
    {
        return $this->dateDeDebutSouhaitee;
    }

    public function getDateDeFinSouhaitee(): ?\DateTimeImmutable
    {
        return $this->dateDeFinSouhaitee;
    }
}
