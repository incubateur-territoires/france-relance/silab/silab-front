<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity\HistorisedStatus;

use App\InterventionServiceOffer\DemandeIntervention\Entity\Status\DemandeInterventionStatus;
use App\Shared\ObjectStatus\Entity\ObjectHistorisedStatusInterface;

class DemandeInterventionHistorisedStatus extends DemandeInterventionStatus implements ObjectHistorisedStatusInterface
{
    public function __construct(
        string $code,
        string $label,
        string $createdBy,
        \DateTimeInterface $createdAt,
        private ?string $comment
    ) {
        parent::__construct($code, $label, $createdBy, $createdAt);
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }
}
