<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity\HistorisedStatus;

use ApiPlatform\Metadata\GetCollection;
use App\InterventionServiceOffer\DemandeIntervention\State\GetDemandeInterventionStatusHistoryProvider;

#[GetCollection(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/demandes-interventions/{demandeInterventionId}/statusHistory',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS')",
    output: DemandeInterventionHistorisedStatus::class,
    provider: GetDemandeInterventionStatusHistoryProvider::class,
)]
class DemandeInterventionHistorisedStatusApiRessource
{
}
