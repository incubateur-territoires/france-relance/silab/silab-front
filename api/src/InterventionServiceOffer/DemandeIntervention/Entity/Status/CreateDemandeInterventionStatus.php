<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity\Status;

class CreateDemandeInterventionStatus
{
    public function __construct(
        private string $code,
    ) {
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
