<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity\Status;

use App\Shared\ObjectStatus\Entity\ObjectStatusInterface;

class DemandeInterventionStatus extends CreateDemandeInterventionStatus implements ObjectStatusInterface
{
    public function __construct(
        string $code,
        private string $label,
        private string $createdBy,
        private \DateTimeInterface $createdAt
    ) {
        parent::__construct($code);
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}
