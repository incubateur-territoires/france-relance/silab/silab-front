<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity\Status;

use ApiPlatform\Metadata\Post;
use App\InterventionServiceOffer\DemandeIntervention\State\AddStatusToDemandeInterventionProcessor;

#[Post(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/demandes-interventions/{demandeInterventionId}/statuses',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_SOUMETTRE_LA_DEMANDE_APRES_ATTENTE_INFORMATION')",
    read: false,
    input: CreateDemandeInterventionStatus::class,
    processor: AddStatusToDemandeInterventionProcessor::class,
)]
class DemandeInterventionStatusApiRessource
{
}
