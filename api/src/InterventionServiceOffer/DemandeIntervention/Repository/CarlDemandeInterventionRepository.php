<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Repository;

use ApiPlatform\State\Pagination\PaginatorInterface;
use App\InterventionServiceOffer\Carl\Entity\DbCarlDemandeIntervention;
use App\InterventionServiceOffer\Carl\Entity\DbCarlDemandeInterventionCoordinates;
use App\InterventionServiceOffer\Carl\Service\CarlClientContext;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\CreateDemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Entity\HistorisedStatus\DemandeInterventionHistorisedStatus;
use App\InterventionServiceOffer\DemandeIntervention\Entity\Status\CreateDemandeInterventionStatus;
use App\InterventionServiceOffer\DemandeIntervention\Entity\Status\DemandeInterventionStatus;
use App\InterventionServiceOffer\Gmao\Service\CarlConfigurationInterventionContext;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\ApiPlatform\Pagination\TraversablePaginator;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlCustomerRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlDemandeInterventionCoordinatesRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlDemandeInterventionRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlEquipementRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlEquipementTypeRepository;
use App\Shared\Carl\Service\CurrentCarlUserContext;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Carl\Trait\UnexpectedOrderByValueCheckingTrait;
use App\Shared\Exception\RuntimeException;
use App\Shared\JsonApi\JsonApiResource;
use App\Shared\ReverseGeocoding\Service\ReverseGeocoding;
use App\Shared\User\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlDemandeInterventionRepository implements DemandeInterventionRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;
    use UnexpectedOrderByValueCheckingTrait;

    public function __construct(
        private DbCarlDemandeInterventionRepository $dbCarlDemandeInterventionRepository,
        private DbCarlDemandeInterventionCoordinatesRepository $dbCarlDemandeInterventionCoordinatesRepository,
        private DbCarlEquipementRepository $dbCarlEquipementRepository,
        private DbCarlEquipementTypeRepository $dbCarlEquipementTypeRepository,
        private DbCarlCustomerRepository $dbCarlCustomerRepository,
        private CurrentCarlUserContext $currentCarlUserContext,
        private CarlConfigurationInterventionContext $carlConfigurationInterventionContext,
        private CarlClientContext $carlClientContext,
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private ReverseGeocoding $reverseGeocoding,
    ) {
    }

    public function delete(string $id): void
    {
        $this->carlClientContext->getCarlClient()->deleteObject('mr', $id);
    }

    public function find($id): DemandeIntervention
    {
        $sampleDemandeInterventionArray = $this->findBy(['id' => [$id]]);

        if (count($sampleDemandeInterventionArray) > 1) {
            throw new RuntimeException("Plus d'une intervention trouvée pour l'id : $id");
        }

        if (0 === count($sampleDemandeInterventionArray)) {
            throw new RuntimeException("Aucune demande d'intervention trouvée pour l'id : $id");
        }

        return iterator_to_array($sampleDemandeInterventionArray)[0];
    }

    public function findAll(): PaginatorInterface
    {
        throw new RuntimeException('methode non implémentée');
    }

    /**
     * @template EntityType of object
     *
     * @param ServiceEntityRepository<EntityType> $entityRepository
     * @param array<int,string>|null              $orderBy
     * @param array<string, mixed>                $criteria
     */
    public function createQueryBy(
        ServiceEntityRepository $entityRepository,
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): Query {
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
                'statuses',
                'relatedEquipement.id',
                'equipementRacine.id',
            ]
        );

        $demandeInterventionLikeQueryBuilder = $entityRepository->createQueryBuilder('dbCarlDemandeInterventionLike');

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $demandeInterventionLikeQueryBuilder
                ->innerJoin('dbCarlDemandeInterventionLike.dbCarlEquipements', 'dbCarlDemandeInterventionDbCarlEquipement');

        $equipementsRacinesIdsAuFormatSiilab = $propertyAccessor->getValue($criteria, '[equipementRacine.id]');
        if (!empty($equipementsRacinesIdsAuFormatSiilab)) {
            // on doit convertir les id d'équipements récupérés
            $dbCarlEquipementsRacinesIds = array_map(
                function (string $equipementRacineId) {
                    return ['id' => $equipementRacineId];
                },
                $equipementsRacinesIdsAuFormatSiilab
            );

            // amelioration, ignorer les doublons intelligement... ou pas
            $demandeInterventionLikeQueryBuilder
                ->andWhere('dbCarlDemandeInterventionDbCarlEquipement.dbCarlEquipement IN(:equipementsRacinesIds)')
                ->setParameter('equipementsRacinesIds', $dbCarlEquipementsRacinesIds);
        }

        $relatedEquipementIds = $propertyAccessor->getValue($criteria, '[relatedEquipement.id]');
        if (!empty($relatedEquipementIds)) {
            // on doit convertir les id d'équipements récupérés
            $dbCarlEquipementIds = array_map(
                function (string $relatedEquipementId) {
                    return ['id' => $relatedEquipementId];
                },
                $relatedEquipementIds
            );

            // amelioration, ignorer les doublons intelligement... ou pas
            $demandeInterventionLikeQueryBuilder
                ->andWhere('dbCarlDemandeInterventionDbCarlEquipement.dbCarlEquipement IN(:relatedEquipementIds)')
                ->andWhere('dbCarlDemandeInterventionDbCarlEquipement.directEqpt = true')
                ->setParameter('relatedEquipementIds', $dbCarlEquipementIds);
        }

        $id = $propertyAccessor->getValue($criteria, '[id]');
        if (!empty($id)) {
            $demandeInterventionLikeQueryBuilder
                ->andWhere('dbCarlDemandeInterventionLike.id IN(:dbCarlDemandeInterventionLike_id)')
                ->setParameter('dbCarlDemandeInterventionLike_id', $id);
        }

        $statuses = $propertyAccessor->getValue($criteria, '[statuses]');
        if (!empty($statuses)) {
            $demandeInterventionLikeQueryBuilder
                ->andWhere('dbCarlDemandeInterventionLike.statusCode IN(:dbCarlDemandeInterventionLike_statusCode)')
                ->setParameter('dbCarlDemandeInterventionLike_statusCode', $this->getCarlStatusCodesFromSilabStatusCodes($statuses));
        }

        if (null !== $orderBy) {
            $this->throwIfUnexpectedOrderByValueCheckingTrait(
                $orderBy,
                []
            );
        }
        $demandeInterventionLikeQueryBuilder->addOrderBy(
            new OrderBy('dbCarlDemandeInterventionLike.updatedAt', 'DESC')
        );

        $demandeInterventionLikeQueryBuilder->getEntityManager()->clear(); // Si un appel a déja était fait sur cette objet permet de le détacher afin de récupérerl'état actuel en db.

        return $demandeInterventionLikeQueryBuilder
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery();
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): PaginatorInterface
    {
        $query = $this->createQueryBy(
            entityRepository: $this->dbCarlDemandeInterventionRepository,
            criteria: $criteria,
            orderBy: $orderBy,
            limit: $limit,
            offset: $offset
        );

        $dbCarlDemandeInterventionsPaginator = new DoctrinePaginator(
            $query
        );
        $limit ??= PHP_INT_MAX;
        $carlGmao = $this->carlConfigurationInterventionContext->getCarlConfiguration();

        $page = intdiv($offset ?? 0, $limit) + 1;

        return new TraversablePaginator(
            new \ArrayIterator(array_map(
                function ($dbCarlDemandeIntervention) use ($carlGmao) {
                    assert($dbCarlDemandeIntervention instanceof DbCarlDemandeIntervention);

                    return $dbCarlDemandeIntervention->toDemandeIntervention(
                        serviceOffer: $this->interventionServiceOfferContext->getServiceOffer(),
                        gmaoCarl: $carlGmao
                    );
                },
                iterator_to_array($dbCarlDemandeInterventionsPaginator->getIterator())
            )),
            $page,
            $limit,
            $dbCarlDemandeInterventionsPaginator->count()
        );
    }

    public function findCoordinatesBy(array $criteria): array
    {
        return array_map(
            function ($dbCarlDemandeIntervention) {
                assert($dbCarlDemandeIntervention instanceof DbCarlDemandeInterventionCoordinates);

                return $dbCarlDemandeIntervention->toDemandeInterventionCoordinates();
            },
            $this->createQueryBy(
                entityRepository: $this->dbCarlDemandeInterventionCoordinatesRepository,
                criteria: $criteria
            )->getResult()
        );
    }

    public function findOneBy(array $criteria): DemandeIntervention
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function getClassName(): string
    {
        return DemandeIntervention::class;
    }

    public function update(string $id, CreateDemandeIntervention $demandeIntervention, string $userEmail): DemandeIntervention
    {
        $originalDemandeIntervention = $this->find($id);
        $attributes = [
            'description' => $demandeIntervention->getTitle(),
            'latitude' => $demandeIntervention->getCoordonnees()?->getLat(),
            'longitude' => $demandeIntervention->getCoordonnees()?->getLng(),
            $this->carlClientContext->getCarlClient()->getChampCarlPourStockageDateDebutSouhaiteeDI() => $demandeIntervention->getDateDeDebutSouhaitee()?->format(\DateTimeInterface::ATOM),
            'expEnd' => $demandeIntervention->getDateDeFinSouhaitee()?->format(\DateTimeInterface::ATOM),
        ];

        $relationships = [];

        if (!empty($demandeIntervention->getDescription())) {
            if (!empty($originalDemandeIntervention->getDescription())) {
                $this->carlClientContext->getCarlClient()->patchCarlObject(
                    'description',
                    $this->carlClientContext->getCarlClient()->getCarlObject('mr', $id, ['longDesc'])
                        ->getRelationship('longDesc')
                        ->getId(),
                    ['description' => $demandeIntervention->getDescription()]
                );
            } else {
                $this->carlClientContext->getCarlClient()->addDescriptionToObjectRelationshipsArray($relationships, $demandeIntervention->getDescription(), 'longDesc');
            }
        } elseif (!empty($originalDemandeIntervention->getDescription())) {
            $this->carlClientContext->getCarlClient()->deleteObjectDescription(objectId: $id, entity: 'mr', descriptionRelashionshipName: 'longDesc');
        }

        $isNewEquipementDifferentFromActual = $demandeIntervention->getRelatedEquipement()->getId() !== $originalDemandeIntervention->getRelatedEquipement()->getId();
        if ($isNewEquipementDifferentFromActual) {
            if (!is_null($originalDemandeIntervention->getRelatedEquipement())) {
                // supression des rattachements d'équipement existants
                $attachedEquipementsData = $this->carlClientContext->getCarlClient()->getCarlObject('mr', $id, includes: ['MREqpt'])
                    ->getRelationshipCollection('MREqpt');
                foreach ($attachedEquipementsData as $attachedEquipementData) {
                    $this->carlClientContext->getCarlClient()->deleteObject(
                        'mreqpt',
                        $attachedEquipementData->getId()
                    );
                }
            }

            if (!is_null($demandeIntervention->getRelatedEquipement())) {
                // Création du nouvel attachement
                $this->attachDemandeInterventionToEquipement($id, $demandeIntervention->getRelatedEquipement()->getId());

                // Mise a jour de l'organisme en charge
                $relatedEquipementIdAndType = $this->dbCarlEquipementTypeRepository->find($demandeIntervention->getRelatedEquipement()->getId());

                $relatedEquipementApiResource = $this->carlClientContext->getCarlClient()->getCarlObject(
                    entity: $relatedEquipementIdAndType->getTypeId(),
                    id: $relatedEquipementIdAndType->getId(),
                    includes: ['site'],
                    fields: [
                        'site' => ['id'],
                    ]
                );

                $relationships['site']['data'] = is_null($relatedEquipementApiResource->getRelationship('site'))
                    ? null
                    : [
                        'id' => $relatedEquipementApiResource->getRelationship('site')->getId(),
                        'type' => 'site',
                    ];
            }
        }

        $this->carlClientContext->getCarlClient()->patchCarlObject('mr', $id, $attributes, $relationships);

        $this->execScriptSpécifiquePoitiersPostPersist(
            $id
        );

        return $this->find($id);
    }

    public function save(CreateDemandeIntervention $demandeIntervention): DemandeIntervention
    {
        $attributesPreset = $this->carlConfigurationInterventionContext->getCarlConfiguration()
            ->getCarlAttributesPresetForDemandeInterventionCreation();
        $relationShipsPreset = $this->carlConfigurationInterventionContext->getCarlConfiguration()
            ->getApiFormatedCarlRelationshipsPresetForDemandeInterventionCreation();
        $relatedEquipementApiResource = null;
        if (!is_null($demandeIntervention->getRelatedEquipement())) {
            $relatedEquipementIdAndType =
                 $this->dbCarlEquipementTypeRepository->find(
                     $demandeIntervention->getRelatedEquipement()->getId()
                 );
            $relatedEquipementApiResource = $this->carlClientContext->getCarlClient()->getCarlObject(
                entity: $relatedEquipementIdAndType->getTypeId(),
                id: $relatedEquipementIdAndType->getId(),
                includes: ['site'],
                fields: [
                    'site' => ['id'],
                ]
            );
        }

        $demandeInterventionArray = [
            'data' => [
                'type' => 'mr',
                'attributes' => array_merge(
                    [
                        'description' => $demandeIntervention->getTitle(),
                    ],
                    $attributesPreset
                ),
                'relationships' => array_merge(
                    [
                        'createdBy' => [
                            'data' => [
                                'id' => $this->currentCarlUserContext->getCurrentGmaoActeur()->getId(),
                                'type' => 'actor',
                            ],
                        ],
                        'site' => [
                            'data' => !is_null($relatedEquipementApiResource?->getRelationship('site'))
                                ?
                                [
                                    'id' => $relatedEquipementApiResource->getRelationship('site')->getId(),
                                    'type' => 'site',
                                ]

                                : null,
                        ],
                    ],
                    $relationShipsPreset
                ),
            ],
        ];

        $demandeInterventionArray['data']['attributes'][$this->carlClientContext->getCarlClient()->getChampCarlPourStockageDateDebutSouhaiteeDI()] = $demandeIntervention->getDateDeDebutSouhaitee()?->format(\DateTimeInterface::ATOM);
        $demandeInterventionArray['data']['attributes']['expEnd'] = $demandeIntervention->getDateDeFinSouhaitee()?->format(\DateTimeInterface::ATOM);

        if (!is_null($demandeIntervention->getCoordonnees())) {
            $demandeInterventionArray['data']['attributes']['latitude'] =
                $demandeIntervention->getCoordonnees()->getLat();
            $demandeInterventionArray['data']['attributes']['longitude'] =
                $demandeIntervention->getCoordonnees()->getLng();
        }

        if (!is_null($demandeIntervention->getDescription())) {
            $this->carlClientContext->getCarlClient()->addDescriptionToObjectRelationshipsArray($demandeInterventionArray['data']['relationships'], $demandeIntervention->getDescription(), 'longDesc');
        }

        $createdDemandeInterventionApiResource = $this->carlClientContext->getCarlClient()->postCarlObject('mr', json_encode($demandeInterventionArray));

        if (!is_null($demandeIntervention->getRelatedEquipement())) {
            $this->attachDemandeInterventionToEquipement($createdDemandeInterventionApiResource->getId(), $demandeIntervention->getRelatedEquipement()->getId());
        }

        $this->execScriptSpécifiquePoitiersPostPersist(
            $createdDemandeInterventionApiResource->getId()
        );

        return $this->find($createdDemandeInterventionApiResource->getId());
    }

    private function execScriptSpécifiquePoitiersPostPersist(
        string $persistedDemandeInterventionId
    ): void {
        $this->updateCommune(
            createdDemandeInterventionId: $persistedDemandeInterventionId,
            carlClient: $this->carlClientContext->getCarlClient(),
            dbCarlEquipementRepository: $this->dbCarlEquipementRepository,
            dbCarlCustomerRepository: $this->dbCarlCustomerRepository,
            reverseGeocoding: $this->reverseGeocoding,
        );
    }

    /**
     * Ajoute la commune a la demande d'intervention renseigné.
     *
     * Context :
     *      Chez Grand Poitiers la commune correspond au champ suivant :
     *          - Equipement -> xtraXxt09
     *          - Mr -> poiXtraTxt14
     *
     * Régles de définition de la commune :
     *      - Si équipement lié prendre sa commune,
     *      - Sinon si Géolocalisation,
     *          prendre la commune identifié depuis l'api de géocoding,
     *      - Sinon null
     */
    private function updateCommune(
        string $createdDemandeInterventionId,
        CarlClient $carlClient,
        DbCarlEquipementRepository $dbCarlEquipementRepository,
        DbCarlCustomerRepository $dbCarlCustomerRepository,
        ReverseGeocoding $reverseGeocoding
    ): void {
        $demandeIntervention = $this->find($createdDemandeInterventionId);
        $hasEquipment = !is_null($relatedEquipment = $demandeIntervention->getRelatedEquipement());
        $codeCommune = null;
        if ($hasEquipment) {
            $codeCommune = $dbCarlEquipementRepository->find(
                $relatedEquipment->getId()
            )
                ?->getXtratxt09();
        } elseif (!is_null($demandeIntervention->getCoordonnees())) {
            $nomCommune = $reverseGeocoding->getCommuneFromCoordinates(
                $demandeIntervention->getCoordonnees()->getLat(),
                $demandeIntervention->getCoordonnees()->getlng()
            );
            $codeCommune = $dbCarlCustomerRepository->findOneBy(['label' => $nomCommune])?->getCode();
        } else {
            return;
        }

        $carlClient->patchCarlObject(
            entity: 'mr',
            objectId: $createdDemandeInterventionId,
            attributes: ['poiXtraTxt14' => $codeCommune]
        );
    }

    private function attachDemandeInterventionToEquipement(string $demandeInterventionId, string $equipmentId): void
    {
        // 1. suppression des liaisons existantes
        // 1.1 récupération des eqpt liés
        $carlMrEqpts = $this->carlClientContext->getCarlClient()->getCarlObjectsRaw(
            entity: 'mreqpt',
            filters: ['MR.id' => [$demandeInterventionId]],
        );
        // 1.2 suppression des eqpt liés
        foreach ($carlMrEqpts['data'] as $carlMrEqpt) {
            $this->carlClientContext->getCarlClient()->deleteObject(
                entity: 'mreqpt',
                id: $carlMrEqpt['id']
            );
        }
        $hasMaterialDirectEqpt = false;
        $hasBoxDirectEqpt = false;
        $carlEqptIdAndType = $this->dbCarlEquipementTypeRepository->find($equipmentId);
        $equipementJsonApiResource = $this->carlClientContext->getCarlClient()->getCarlObject(
            entity: $carlEqptIdAndType->getTypeId(),
            id: $carlEqptIdAndType->getId(),
        );
        $this->linkModelToMrIfExists(// la liaison avec le modèle se fait directement depuis les attributs
            equipementJsonApiResource: $equipementJsonApiResource,
            mrId: $demandeInterventionId
        );
        $this->linkEqptAndAncestorsToMrRecursive(
            equipement: $equipementJsonApiResource,
            mrId: $demandeInterventionId,
            mrHasMaterialDirectEqpt: $hasMaterialDirectEqpt,
            mrHasBoxDirectEqpt: $hasBoxDirectEqpt
        );
    }

    private function linkModelToMrIfExists(JsonApiResource $equipementJsonApiResource, string $mrId): void
    {
        // seuls les materials ont des modèles (item)
        if ('material' !== $equipementJsonApiResource->getType()) {
            return;
        }

        $equipementJsonApiResource = $this->carlClientContext->getCarlClient()->getCarlObject(
            entity: $equipementJsonApiResource->getType(),
            id: $equipementJsonApiResource->getId(),
            includes: ['item']
        );

        $item = $equipementJsonApiResource->getRelationship('item');

        if (is_null($item)) {
            return;
        }

        $this->linkEqptToMr(equipement: $item, mrId: $mrId, directEqpt: true);
    }

    /**
     * Amelioration, la partie "liaison avec les ancetres" est à revoir (ordre de prio de liaison si plusieurs parents éligibles),
     * et potentiellement à basculer sur un script spécif.
     */
    private function linkEqptAndAncestorsToMrRecursive(
        JsonApiResource $equipement,
        string $mrId,
        bool &$mrHasMaterialDirectEqpt,
        bool &$mrHasBoxDirectEqpt
    ): void {
        $isDirectEqpt = false;

        if (!$mrHasMaterialDirectEqpt && 'material' === $equipement->getType()) {
            $isDirectEqpt = true;
            $mrHasMaterialDirectEqpt = true;
        }

        if (!$mrHasBoxDirectEqpt && $this->carlClientContext->getCarlClient()->isBox($equipement->getType())) {
            $isDirectEqpt = true;
            $mrHasBoxDirectEqpt = true;
        }

        $this->linkEqptToMr(
            equipement: $equipement,
            mrId: $mrId,
            directEqpt: $isDirectEqpt
        );

        $parentsLinkequipments = $this->carlClientContext->getCarlClient()->getCarlObject(
            entity: $equipement->getType(),
            id: $equipement->getId(),
            includes: ['parents', 'parents.parent']
        )->getRelationshipCollection('parents');

        // 3. ajout des équipements parents
        foreach ($parentsLinkequipments as $parentLinkequipment) {
            if (
                !is_null($parentLinkequipment->getRelationship('parent'))
                && $this->eqptLinkIsValid($parentLinkequipment)
            ) {
                // Il faut vérifier que le lien est toujorus d'actualité
                $this->linkEqptAndAncestorsToMrRecursive(
                    equipement: $parentLinkequipment->getRelationship('parent'),
                    mrId: $mrId,
                    mrHasMaterialDirectEqpt: $mrHasMaterialDirectEqpt,
                    mrHasBoxDirectEqpt: $mrHasBoxDirectEqpt);
            }
        }
    }

    private function eqptLinkIsValid(JsonApiResource $linkEqptJsonApiResource): bool
    {
        $linkHasStartedInThePast = (new \DateTimeImmutable($linkEqptJsonApiResource->getAttribute('linkBegin'))) < (new \DateTimeImmutable());
        $linkWillEndInTheFuture = (new \DateTimeImmutable($linkEqptJsonApiResource->getAttribute('linkEnd'))) > (new \DateTimeImmutable());

        return $linkHasStartedInThePast && $linkWillEndInTheFuture;
    }

    private function linkEqptToMr(JsonApiResource $equipement, string $mrId, bool $directEqpt = false): JsonApiResource
    {
        $carlMrEqptArray = [
            'data' => [
                'type' => 'mreqpt',
                'attributes' => [
                    'directEqpt' => $directEqpt,
                ],
                'relationships' => [
                    'MR' => [
                        'data' => [
                            'id' => $mrId,
                            'type' => 'mr',
                        ],
                    ],
                    'eqpt' => [
                        'data' => [
                            'id' => $equipement->getId(),
                            'type' => $equipement->getType(),
                        ],
                    ],
                ],
            ],
        ];

        return $this->carlClientContext->getCarlClient()->postCarlObject('mreqpt', json_encode($carlMrEqptArray));
    }

    public function transistionToStatus(string $id, CreateDemandeInterventionStatus $status): DemandeInterventionStatus
    {
        $this->carlClientContext->getCarlClient()->transitionToStatus(
            entityType: 'mr',
            objectId: $id,
            targetCarlStatusCode: $this->getCarlStatusCodeFromSilabStatusCode($status->getCode()),
            statusCodeWorkflow: CarlClient::DEMANDEINTERVENTION_STATUS_WORKFLOW,
            targetStatusComment: 'Via Siilab par '.$this->currentCarlUserContext->getCurrentGmaoActeur()->getNomComplet()
        );

        return $this->find($id)->getCurrentStatus();
    }

    /**
     * @param array<string> $silabStatuses
     *
     * @return array<string>
     */
    private function getCarlStatusCodesFromSilabStatusCodes(array $silabStatuses): array
    {
        return CarlClient::getCarlStatusCodesFromSilabStatusCodes($silabStatuses, CarlClient::SILAB_TO_CARL_DEMANDEINTERVENTION_STATUS_MAPPING);
    }

    private function getCarlStatusCodeFromSilabStatusCode(string $silabStatusCode): string
    {
        return CarlClient::getCarlStatusCodeFromSilabStatusCode($silabStatusCode, CarlClient::CARL_TO_SILAB_DEMANDEINTERVENTION_STATUS_MAPPING);
    }

    public function getStatusHistory(string $id): array
    {
        $filters = [
            'origin.id' => [$id],
        ];
        $includes = [
            'statusChangedBy',
        ];
        $fields = [
            'actor' => ['id'],
        ];
        $unsortedHistorisedStatusesResponse = $this->carlClientContext->getCarlClient()->getCarlObjectsRaw(
            entity: 'mrhistostatus',
            filters: $filters,
            includes: $includes,
            fields: $fields,
        );

        /** @var array<DemandeInterventionHistorisedStatus> $statusHistory */
        $statusHistory = [];
        foreach ($unsortedHistorisedStatusesResponse['data'] as $unsortedHistorisedStatusesResponseData) {
            $historisedStatusApiResource = new JsonApiResource($unsortedHistorisedStatusesResponseData, $unsortedHistorisedStatusesResponse['included']);

            $statusHistory[] = new DemandeInterventionHistorisedStatus(
                code: $historisedStatusApiResource->getAttribute('statusCode'),
                label: $this->carlClientContext->getCarlClient()->getStatusDescription(
                    $historisedStatusApiResource->getAttribute('statusCode'),
                    'GPAT_MRSTATUS' // AMELIORATION Récupérer duynamiquement le workflow depuis objectinfo
                ),
                createdBy: $this->carlClientContext->getCarlClient()->getUserEmailFromIdSafe($historisedStatusApiResource->getRelationship('statusChangedBy')->getId()),
                createdAt: new \DateTime($historisedStatusApiResource->getAttribute('statusChangedDate')),
                comment: $historisedStatusApiResource->getAttribute('comment')
            );
        }

        // Note: on fait le tri ici car en rest ça ne marche pas actuellement (sur cette entitée en tt cas)
        usort(
            $statusHistory,
            function (DemandeInterventionHistorisedStatus $a, DemandeInterventionHistorisedStatus $b) {
                return ((int) $b->getCreatedAt()->format('Uu')) - ((int) $a->getCreatedAt()->format('Uu'));
            }
        );

        return $statusHistory;
    }

    public function addImageBase64(string $base64, DemandeIntervention $demandeIntervention, User $user, ?array $metadatas = null): string
    {
        $attachementId = $this->carlClientContext->getCarlClient()->persistFile(
            base64: $base64,
            name: $demandeIntervention->getTitle(),
            user: $user,
            metadatas: $metadatas
        );

        $carlUserId = $this->carlClientContext->getCarlClient()
            ->getUserIdFromEmail($user->getEmail());

        $carlLinkedUrlDocument = $this->carlClientContext->getCarlClient()->addDocumentToObject(
            attachementId: $attachementId,
            objectId: $demandeIntervention->getId(),
            objectJavaClass: 'com.carl.xnet.works.backend.bean.AbstractMRBean',
            userId: $carlUserId,
            docTypeId: $this->carlConfigurationInterventionContext->getCarlConfiguration()->getPhotoAvantInterventionDoctypeId(),
            documentTitle: 'photo préalable'
        );

        return $carlLinkedUrlDocument->getDocumentlinkId();
    }

    /**
     * @param string $imageId Attention l'imageId doit bien correspondre a l'id du documentLink de carl
     */
    public function deleteImage(string $imageId, DemandeIntervention $demandeIntervention): void
    {
        // Note: supprimer un documentlink supprime également le document associé
        $this->carlClientContext->getCarlClient()->deleteObject('documentlink', $imageId);
    }

    public function getImageBlob(string $id): string
    {
        return $this->carlClientContext->getCarlClient()->getDocumentBlob($id);
    }
}
