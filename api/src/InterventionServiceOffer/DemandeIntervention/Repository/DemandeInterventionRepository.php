<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Repository;

use ApiPlatform\State\Pagination\PaginatorInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\CreateDemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Entity\Status\CreateDemandeInterventionStatus;
use App\InterventionServiceOffer\DemandeIntervention\Entity\Status\DemandeInterventionStatus;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Exception\RuntimeException;
use App\Shared\User\Entity\User;

class DemandeInterventionRepository implements DemandeInterventionRepositoryInterface
{
    private DemandeInterventionRepositoryInterface $specificDemandeInterventionRepository;

    public function __construct(
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        CarlDemandeInterventionRepository $carlDemandeInterventionRepository
    ) {
        try {
            $this->specificDemandeInterventionRepository = match ($interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class) {
                CarlConfigurationIntervention::class => $carlDemandeInterventionRepository
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class);
        }
    }

    public function findCoordinatesBy(array $criteria): array
    {
        return $this->specificDemandeInterventionRepository->findCoordinatesBy($criteria);
    }

    public function delete(string $id): void
    {
        $this->specificDemandeInterventionRepository->delete($id);
    }

    public function find($id): DemandeIntervention
    {
        $demandeIntervention = $this->specificDemandeInterventionRepository->find($id);
        if (!is_null($demandeIntervention->getRelatedEquipement())) {
            $demandeIntervention->getRelatedEquipement()->setGmaoObjectViewUrl(
                $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()->getEquipementViewUrl($demandeIntervention->getRelatedEquipement())
            );
        }

        return $demandeIntervention->setGmaoObjectViewUrl(
            $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()->getDemandeInterventionViewUrl($demandeIntervention)
        );
    }

    public function findAll(): PaginatorInterface
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): PaginatorInterface
    {
        $demandeInterventions = $this->specificDemandeInterventionRepository->findBy($criteria, $orderBy, $limit, $offset);
        foreach ($demandeInterventions as $demandeIntervention) {
            if (!is_null($demandeIntervention->getRelatedEquipement())) {
                $demandeIntervention->getRelatedEquipement()->setGmaoObjectViewUrl(
                    $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()->getEquipementViewUrl($demandeIntervention->getRelatedEquipement())
                );
            }
            $demandeIntervention
                    ->setGmaoObjectViewUrl(
                        $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()
                            ->getDemandeInterventionViewUrl($demandeIntervention)
                    );
        }

        return $demandeInterventions;
    }

    public function findOneBy(array $criteria): DemandeIntervention
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function getClassName()
    {
        return DemandeIntervention::class;
    }

    public function update(string $id, CreateDemandeIntervention $demandeIntervention, string $userEmail): DemandeIntervention
    {
        return $this->specificDemandeInterventionRepository->update($id, $demandeIntervention, $userEmail);
    }

    public function save(CreateDemandeIntervention $demandeIntervention): DemandeIntervention
    {
        return $this->specificDemandeInterventionRepository->save($demandeIntervention);
    }

    public function transistionToStatus(string $id, CreateDemandeInterventionStatus $status): DemandeInterventionStatus
    {
        return $this->specificDemandeInterventionRepository->transistionToStatus($id, $status);
    }

    public function getStatusHistory(string $id): array
    {
        return $this->specificDemandeInterventionRepository->getStatusHistory($id);
    }

    public function addImageBase64(string $base64, DemandeIntervention $demandeIntervention, User $user, ?array $metadatas = null): string
    {
        $metadatas ??= [
            'serviceOffer:title' => $this->interventionServiceOfferContext->getServiceOffer()->getTitle(),
            'interventionServiceOffer:object:title' => $demandeIntervention->getTitle(),
            'interventionServiceOffer:object:description' => $demandeIntervention->getDescription(),
            'interventionServiceOffer:demandeIntervention:title' => $demandeIntervention->getTitle(),
            'interventionServiceOffer:demandeIntervention:code' => $demandeIntervention->getCode(),
            'interventionServiceOffer:demandeIntervention:description' => $demandeIntervention->getDescription(),
        ];

        return $this->specificDemandeInterventionRepository->addImageBase64(
            base64: $base64,
            demandeIntervention: $demandeIntervention,
            user: $user,
            metadatas: $metadatas
        );
    }

    public function deleteImage(string $imageId, DemandeIntervention $demandeIntervention): void
    {
        // Note: on ne supprime jamais les images de la GED...
        $this->specificDemandeInterventionRepository->deleteImage(imageId: $imageId, demandeIntervention: $demandeIntervention);
    }

    public function getImageBlob(string $id): string
    {
        return $this->specificDemandeInterventionRepository->getImageBlob(id: $id);
    }
}
