<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Repository;

use ApiPlatform\State\Pagination\PaginatorInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\CreateDemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeInterventionCoordinates;
use App\InterventionServiceOffer\DemandeIntervention\Entity\HistorisedStatus\DemandeInterventionHistorisedStatus;
use App\InterventionServiceOffer\DemandeIntervention\Entity\Status\CreateDemandeInterventionStatus;
use App\InterventionServiceOffer\DemandeIntervention\Entity\Status\DemandeInterventionStatus;
use App\Shared\Doctrine\Persistence\PaginableObjectRepositoryInterface;
use App\Shared\User\Entity\User;

/**
 * @extends PaginableObjectRepositoryInterface<DemandeIntervention>
 */
interface DemandeInterventionRepositoryInterface extends PaginableObjectRepositoryInterface
{
    public function find(mixed $id): DemandeIntervention;

    /**
     * @param array<string,mixed>    $criteria
     *                                         'relatedEquipement.id' : retourne toutes les demandes d'interventions rattachées à l'équipement ou un de ses enfant
     * @param array<int,string>|null $orderBy
     *
     * @return PaginatorInterface<DemandeIntervention>
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): PaginatorInterface;

    /**
     * @return PaginatorInterface<DemandeIntervention>
     */
    public function findAll(): PaginatorInterface;

    public function findOneBy(array $criteria): DemandeIntervention;

    /**
     * @param array<string,mixed> $criteria
     *
     * @return array<DemandeInterventionCoordinates>
     */
    public function findCoordinatesBy(array $criteria): array;

    public function update(string $id, CreateDemandeIntervention $demandeIntervention, string $userEmail): DemandeIntervention;

    public function delete(string $id): void;

    public function save(CreateDemandeIntervention $demandeIntervention): DemandeIntervention;

    public function transistionToStatus(string $id, CreateDemandeInterventionStatus $status): DemandeInterventionStatus;

    /**
     * @return array<DemandeInterventionHistorisedStatus>
     */
    public function getStatusHistory(string $id): array;

    /**
     * @param array<string,string|null>|null $metadatas amelioration: à l'avenir, implementer des metadataserializer ça pourrait être pas mal
     *
     * @return string l'id de l'image
     */
    public function addImageBase64(string $base64, DemandeIntervention $demandeIntervention, User $user, ?array $metadatas): string;

    public function deleteImage(string $imageId, DemandeIntervention $demandeIntervention): void;

    public function getImageBlob(string $id): string;
}
