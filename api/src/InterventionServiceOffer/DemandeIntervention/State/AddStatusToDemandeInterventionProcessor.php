<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\Status\CreateDemandeInterventionStatus;
use App\InterventionServiceOffer\DemandeIntervention\Entity\Status\DemandeInterventionStatus;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use Psr\Log\LoggerInterface;

final class AddStatusToDemandeInterventionProcessor implements ProcessorInterface
{
    public function __construct(
        private DemandeInterventionRepository $demandeInterventionRepository,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param CreateDemandeInterventionStatus $data
     * @param array<mixed>                    $uriVariables
     * @param array<mixed>                    $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): DemandeInterventionStatus
    {
        $this->logger->debug('Changement de statut d\'une demande d\'intervention', ['data' => json_encode($data), 'context' => $context, 'uriVariables' => $uriVariables]);

        assert($operation instanceof Post);
        assert($data instanceof CreateDemandeInterventionStatus);

        return $this->demandeInterventionRepository->transistionToStatus(
            id: $uriVariables['demandeInterventionId'],
            status: $data
        );
    }
}
