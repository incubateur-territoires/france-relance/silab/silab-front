<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\CreateDemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use Psr\Log\LoggerInterface;

final class CreateDemandeInterventionProcessor implements ProcessorInterface
{
    public function __construct(
        private DemandeInterventionRepository $demandeInterventionRepository,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): DemandeIntervention
    {
        $this->logger->debug(
            'Création d\'une demande d\'intervention',
            ['data' => json_encode($data), 'context' => $context, 'uriVariables' => $uriVariables]
        );

        assert($operation instanceof Post);
        assert($data instanceof CreateDemandeIntervention);

        return $this->demandeInterventionRepository->save($data);
    }
}
