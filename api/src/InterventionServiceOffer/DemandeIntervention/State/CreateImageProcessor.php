<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\Shared\Document\Entity\ImageApiResource;
use App\Shared\Helpers;
use App\Shared\User\Service\CurrentUserContextInterface;
use Psr\Log\LoggerInterface;

final class CreateImageProcessor implements ProcessorInterface
{
    public function __construct(
        private DemandeInterventionRepository $demandeInterventionRepository,
        private CurrentUserContextInterface $currentUserContext,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param ImageApiResource $data
     * @param array<mixed>     $uriVariables
     * @param array<mixed>     $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): ImageApiResource {
        $this->logger->debug('Création d\'une image', ['context' => $context, 'uriVariables' => $uriVariables]);

        assert($operation instanceof Post, 'Opération '.$operation->getDescription().' non supportée');
        assert($data instanceof ImageApiResource);

        Helpers::validateImageMimeType($data->getBase64());

        $imageId = $this->demandeInterventionRepository->addImageBase64(
            $data->getBase64(),
            $this->demandeInterventionRepository->find($uriVariables['demandeInterventionId']),
            $this->currentUserContext->getCurrentUser()
        );

        return new ImageApiResource(
            id: $imageId,
            url: ''// L'url va être généré par l'ImageUrlAttributeNormalizer
        );
    }
}
