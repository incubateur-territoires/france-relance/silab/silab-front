<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\Shared\Exception\InsufficientRolesException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DeleteDemandeInterventionController extends AbstractController
{
    public function __construct(
        private DemandeInterventionRepository $demandeInterventionRepository,
        private Security $security,
    ) {
    }

    // QUESTION_TECHNIQUE: Comment faire avec api-platform, si on utilise une operation delete on a une erreur similaire a https://github.com/api-platform/core/issues/5827
    #[Route('/api/intervention-service-offers/{serviceOfferId}/demandes-interventions/{id}',
        name: 'delete_demande_intervention', methods: ['DELETE'])]
    public function deleteDemandeIntervention(string $id): Response
    {
        // Securité :
        if ($this->security->isGranted('SERVICEOFFER_ROLE_INTERVENTION_SUPPRIMER_DEMANDES_INTERVENTIONS')) {
            $this->demandeInterventionRepository->delete($id);

            return new Response(status: 204);
        } else {
            throw new InsufficientRolesException("Vous n'avez pas les droits suffisants pour supprimer cette demande d'intervention");
        }
    }
}
