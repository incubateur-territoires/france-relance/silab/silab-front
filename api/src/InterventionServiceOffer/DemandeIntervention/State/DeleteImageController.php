<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\Shared\Exception\InsufficientRolesException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

// QUESTION_TECHNIQUE: Comment faire avec api-platform, si on utilise une operation delete on a une erreur similaire a https://github.com/api-platform/core/issues/5827
#[Route(
    '/api/intervention-service-offers/{serviceOfferId}/demandes-interventions/{demandeInterventionId}/images/{id}',
    name: 'interventionServiceOffer_delete_demande_intervention_image',
    methods: ['DELETE']
)]
class DeleteImageController extends AbstractController
{
    public function __invoke(
        string $demandeInterventionId,
        string $id,
        DemandeInterventionRepository $demandeInterventionRepository,
        Security $security
    ): Response {
        // Securité :
        if (!$security->isGranted(
            'SERVICEOFFER_ROLE_INTERVENTION_SUPPRIMER_IMAGE_A_DEMANDE_INTERVENTION'
        )) {
            throw new InsufficientRolesException("Vous n'avez pas les droits suffisants pour supprimer une image pré-intervention");
        }

        $demandeInterventionRepository->deleteImage(
            imageId: $id,
            demandeIntervention: $demandeInterventionRepository->find($demandeInterventionId)
        );

        return new Response(status: 204);
    }
}
