<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeInterventionCoordinates;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;

final class GetCollectionDemandeInterventionCoordinatesProvider implements ProviderInterface
{
    public function __construct(
        private DemandeInterventionRepository $demandeInterventionRepository,
        private InterventionServiceOfferContext $interventionServiceOfferContext
    ) {
    }

    /**
     * Fournit au endpoint getAllIntervention les datas provenants du connecteur Carl.
     *
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<DemandeInterventionCoordinates>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $criteria = $context['filters'] ?? [];

        foreach ($criteria as $key => $value) {
            if (is_string($value)) {
                $criteria[$key] = [$value];
            }
        }

        $lOffreDeServicePorteSurDesEquipements = !empty($this->interventionServiceOfferContext->getServiceOffer()->getAvailableEquipementsIds());
        $onAFiltréSurDesEquipements = array_key_exists('relatedEquipement.id', $criteria) | array_key_exists('equipementRacine.id', $criteria);

        if ($lOffreDeServicePorteSurDesEquipements) {
            // on restreint les équipements à ceux définits dans l'ods
            // ça posait question sur la gestion des favories
            // if ($onAFiltréSurDesEquipements) {
            //     $criteria['relatedEquipement.id'] = array_intersect($criteria['relatedEquipement.id'], $this->interventionServiceOfferContext->getServiceOffer()->getAvailableEquipementsIds());
            // }
            // par défault on restreint aux equipements de l'ODS
            if (!$onAFiltréSurDesEquipements) {
                $criteria['equipementRacine.id'] = $this->interventionServiceOfferContext->getServiceOffer()->getAvailableEquipementsIds(); // amélioration: attention aux enfants, on devrait ptet prendre les enfants aussi !
            }
        }

        return $this->demandeInterventionRepository->findCoordinatesBy(criteria: $criteria);
    }
}
