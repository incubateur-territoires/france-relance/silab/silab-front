<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\Pagination\Pagination;
use ApiPlatform\State\Pagination\PaginatorInterface;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;

final class GetCollectionDemandeInterventionProvider implements ProviderInterface
{
    public function __construct(
        private DemandeInterventionRepository $demandeInterventionRepository,
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private readonly Pagination $pagination
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return PaginatorInterface<DemandeIntervention>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): PaginatorInterface
    {
        assert($operation instanceof CollectionOperationInterface);

        $criteria = $context['filters'] ?? [];

        // on veut toujours des tableaux
        foreach ($criteria as $key => $value) {
            if (is_string($value)) {
                $criteria[$key] = [$value];
            }
        }

        $lOffreDeServicePorteSurDesEquipements = !empty(
            $this->interventionServiceOfferContext->getServiceOffer()->getAvailableEquipementsIds()
        );
        $onAFiltréSurDesEquipements = array_key_exists('relatedEquipement.id', $criteria) | array_key_exists('equipementRacine.id', $criteria);
        if ($lOffreDeServicePorteSurDesEquipements) {
            // on restreint les équipements à ceux définits dans l'ods
            // ça posait question sur la gestion des favories
            // if ($onAFiltréSurDesEquipements) {
            //     $criteria['relatedEquipement.id'] = array_intersect($criteria['relatedEquipement.id'], $this->interventionServiceOfferContext->getServiceOffer()->getAvailableEquipementsIds());
            // }
            // par défault on restreint aux equipements de l'ODS
            if (!$onAFiltréSurDesEquipements) {
                $criteria['equipementRacine.id'] =
                $this->interventionServiceOfferContext->getServiceOffer()->getAvailableEquipementsIds();
                // amélioration: attention aux enfants, on devrait ptet prendre les enfants aussi !
            }
        }

        // from : https://api-platform.com/docs/guides/custom-pagination/
        [, $offset , $limit] = $this->pagination->getPagination($operation, $context);
        unset($criteria['page']);

        return $this->demandeInterventionRepository->findBy(criteria: $criteria, offset: $offset, limit: $limit);
    }
}
