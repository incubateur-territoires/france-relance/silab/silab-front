<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;

final class GetDemandeInterventionProvider implements ProviderInterface
{
    public function __construct(
        private DemandeInterventionRepository $demandeInterventionRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): DemandeIntervention
    {
        assert($operation instanceof Get);

        return $this->demandeInterventionRepository->find($uriVariables['id']);
    }
}
