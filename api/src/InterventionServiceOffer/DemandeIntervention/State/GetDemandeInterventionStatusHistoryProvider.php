<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\HistorisedStatus\DemandeInterventionHistorisedStatus;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;

final class GetDemandeInterventionStatusHistoryProvider implements ProviderInterface
{
    public function __construct(
        private DemandeInterventionRepository $demandeInterventionRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<DemandeInterventionHistorisedStatus>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        // amelioration: retourner un 404 si pas de demande ?
        return $this->demandeInterventionRepository->getStatusHistory($uriVariables['demandeInterventionId']);
    }
}
