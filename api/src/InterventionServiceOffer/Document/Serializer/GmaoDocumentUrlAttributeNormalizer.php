<?php

namespace App\InterventionServiceOffer\Document\Serializer;

use App\InterventionServiceOffer\Gmao\Service\GmaoConfigurationInterventionContext;
use App\Shared\Document\Entity\Document;
use App\Shared\Document\Entity\DocumentInterface;
use CoopTilleuls\UrlSignerBundle\UrlSigner\UrlSignerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * see : https://api-platform.com/docs/core/serialization/
 * see : https://symfony.com/doc/current/serializer/custom_normalizer.html#creating-a-new-normalizer.
 *
 * Ce normalizer génère une url de document temporaire permattant de sécuriser les documnets et de faire office de proxy pour
 * les documnents qui nécéssite une 'désécurisation' (url de documnents non accessibles par les clients web sans authentification, eg. derrière une GED sécurisée).
 * L'url pointe vers un controller silab dédié à la restitution de documnents.
 */
class GmaoDocumentUrlAttributeNormalizer implements NormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'DOCUMENT_URL_ATTRIBUTE_NORMALIZER_ALREADY_CALLED';

    public function __construct(
        private UrlSignerInterface $urlSigner,
        private UrlGeneratorInterface $urlGenerator,
        private GmaoConfigurationInterventionContext $gmaoConfigurationInterventionContext
    ) {
    }

    public function getSupportedTypes(?string $format): array
    {
        return [
            // les classes implémentant DocumentInterface (on ne peut pas mettre simplement l'interface, on doit préciser des classes finales)
            Document::class => false, // false est important, le support dépend du contexte (de l'ODS)
        ];
    }

    /**
     * @param array<mixed> $context
     *
     * @return array<mixed>|string|int|float|bool|\ArrayObject|null
     */
    public function normalize(mixed $object, ?string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null
    {
        $context[self::ALREADY_CALLED] = true;
        $normalizedObject = $this->normalizer->normalize($object, $format, $context);

        $internalDocumentUrl = $this->urlGenerator->generate(
            'gmaoConfigurationIntervention_document',
            [
                'gmaoConfigurationInterventionId' => $this->gmaoConfigurationInterventionContext->getGmaoConfigurationId(),
                'id' => $normalizedObject['id'],
            ],
            RouterInterface::ABSOLUTE_URL
        );

        $signedInternalDocumentUrl = $this->urlSigner->sign(
            $internalDocumentUrl,
            (new \DateTime('now'))->add(new \DateInterval('PT10M'))
        );

        $normalizedObject['url'] = $signedInternalDocumentUrl;

        return $normalizedObject;
    }

    /**
     * @param array<mixed> $context
     */
    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool
    {
        // Make sure we're not called twice
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        if (!array_key_exists('request_uri', $context)) {
            return false;
        }

        if (
            !str_starts_with($context['request_uri'], '/api/intervention-service-offers')
            && !str_starts_with($context['request_uri'], '/api/gmao-configuration-interventions')
        ) {
            return false;
        }

        return $data instanceof DocumentInterface;
    }
}
