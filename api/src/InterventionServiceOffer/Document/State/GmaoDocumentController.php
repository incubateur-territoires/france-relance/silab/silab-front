<?php

namespace App\InterventionServiceOffer\Document\State;

use App\InterventionServiceOffer\Gmao\Service\GmaoConfigurationInterventionContext;
use App\Shared\Document\Trait\DocumentResponderTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// cf. https://stackoverflow.com/a/64045615
#[Route(
    '/gmao-configuration-interventions/{gmaoConfigurationInterventionId}/documents/{id}',
    name: 'gmaoConfigurationIntervention_document',
    defaults: ['_signed' => true],
    methods: ['GET']
)]
class GmaoDocumentController extends AbstractController
{
    use DocumentResponderTrait;

    public function __invoke(
        string $id,
        GmaoConfigurationInterventionContext $gmaoConfigurationInterventionContext,
    ): Response {
        $documentBlob = $gmaoConfigurationInterventionContext->getGmaoConfiguration()->getDocumentBlob($id);

        return $this->generateResponseFromBlob(
            documentBlob: $documentBlob
        );
    }
}
