<?php

namespace App\InterventionServiceOffer\Document\State;

use App\InterventionServiceOffer\Gmao\Service\GmaoConfigurationInterventionContext;
use App\Shared\Document\Trait\SizeableImageResponderTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Annotation\Route;

// cf. https://stackoverflow.com/a/64045615
#[Route(
    '/gmao-configuration-interventions/{gmaoConfigurationInterventionId}/images/{id}',
    name: 'gmaoConfigurationIntervention_image',
    defaults: ['_signed' => true],
    methods: ['GET']
)]
class GmaoImageController extends AbstractController
{
    use SizeableImageResponderTrait;

    public function __invoke(
        string $id,
        GmaoConfigurationInterventionContext $gmaoConfigurationInterventionContext,
        #[MapQueryParameter] ?int $maxSize = null,
    ): Response {
        // étape 1 vérifier la signature de l'url, cf. https://les-tilleuls.coop/blog/url-signer-bundle-create-validate-signed-url-symfony-en
        // -> `defaults: ['_signed' => true]` dans les paramètres de la route

        // étape 2 récupérerer l'image sur la ged ou en direct
        // ici on ne verifie pas que la hoto apprtiennent bien a l'équipement
        $imageBlob = $gmaoConfigurationInterventionContext->getGmaoConfiguration()->getDocumentBlob($id);

        return $this->generateResponseFromImageBlob(
            imageBlob: $imageBlob,
            maxSize: $maxSize
        );
    }
}
