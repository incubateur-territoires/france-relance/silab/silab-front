<?php

namespace App\InterventionServiceOffer\Equipement\Entity\Compteur;

use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Equipement\Entity\Mesure\Mesure;

class Compteur
{
    public function __construct(
        private string $id,
        private string $code,
        private string $label,
        private string $unite,
        private Equipement $equipement,
        private CompteurType $type,
        private ?Mesure $derniereMesure,
        private ?float $valeurMinimum,
        private ?float $valeurMaximum,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getUnite(): string
    {
        return $this->unite;
    }

    public function setEquipement(Equipement $equipement): self
    {
        $this->equipement = $equipement;

        return $this;
    }

    public function getEquipement(): Equipement
    {
        return $this->equipement;
    }

    public function getType(): CompteurType
    {
        return $this->type;
    }

    public function getDerniereMesure(): ?Mesure
    {
        return $this->derniereMesure;
    }

    public function getValeurMinimum(): ?float
    {
        return $this->valeurMinimum;
    }

    public function getValeurMaximum(): ?float
    {
        return $this->valeurMaximum;
    }
}
