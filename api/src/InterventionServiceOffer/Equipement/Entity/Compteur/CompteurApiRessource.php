<?php

namespace App\InterventionServiceOffer\Equipement\Entity\Compteur;

use ApiPlatform\Metadata\GetCollection;
use App\InterventionServiceOffer\Equipement\State\GetCollectionCompteurByEquipementProvider;

#[GetCollection(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/equipements/{equipementId}/compteurs',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_COMPTEUR')",
    output: Compteur::class,
    provider: GetCollectionCompteurByEquipementProvider::class
)]
class CompteurApiRessource
{
}
