<?php

namespace App\InterventionServiceOffer\Equipement\Entity\Compteur;

enum CompteurType: string
{
    case Flottant = 'float';
    case Binaire = 'boolean';
    case Entier = 'integer';
}
