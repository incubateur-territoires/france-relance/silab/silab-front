<?php

namespace App\InterventionServiceOffer\Equipement\Entity\Equipement;

class AncetreEquipement
{
    public function __construct(
        private string $id,
        private string $label
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }
}
