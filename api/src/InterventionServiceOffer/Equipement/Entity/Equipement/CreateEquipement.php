<?php

namespace App\InterventionServiceOffer\Equipement\Entity\Equipement;

class CreateEquipement
{
    public function __construct(
        private string $comment,
    ) {
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }
}
