<?php

namespace App\InterventionServiceOffer\Equipement\Entity\Equipement;

use App\InterventionServiceOffer\Equipement\Entity\Status\EquipementStatus;
use App\Shared\Document\Entity\DocumentInterface;
use App\Shared\Document\Entity\ImageInterface;
use App\Shared\Location\Entity\Coordinates;

class Equipement extends CreateEquipement
{
    public function __construct(
        string $comment,
        private string $id,
        private string $label,
        private string $code,
        private ?Coordinates $coordinates,
        private EquipementStatus $currentStatus,
        private ?string $gmaoObjectViewUrl,
        /** @var array<ImageInterface> */
        private array $imagesIllustration,
        /** @var array<DocumentInterface> */
        private array $documentsJoints,
        /** @var array<string,array<AncetreEquipement>>|null */
        private ?array $ancetres,
        /** @var bool */
        private bool $interventionsAutorisees
    ) {
        parent::__construct($comment);
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }

    public function getCurrentStatus(): EquipementStatus
    {
        return $this->currentStatus;
    }

    public function setGmaoObjectViewUrl(?string $gmaoObjectViewUrl): self
    {
        $this->gmaoObjectViewUrl = $gmaoObjectViewUrl;

        return $this;
    }

    public function getGmaoObjectViewUrl(): ?string
    {
        return $this->gmaoObjectViewUrl;
    }

    /** @return array<DocumentInterface> */
    public function getDocumentsJoints(): array
    {
        return $this->documentsJoints;
    }

    /** @return array<ImageInterface> */
    public function getImagesIllustration(): array
    {
        return $this->imagesIllustration;
    }

    /** @return array<string,array<AncetreEquipement>>|null */
    public function getAncetres(): ?array
    {
        return $this->ancetres;
    }

    public function getInterventionsAutorisees(): bool
    {
        return $this->interventionsAutorisees;
    }
}
