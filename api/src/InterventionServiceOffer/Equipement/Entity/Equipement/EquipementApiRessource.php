<?php

namespace App\InterventionServiceOffer\Equipement\Entity\Equipement;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use App\InterventionServiceOffer\Equipement\State\GetCollectionEquipementByGmaoConfigurationInterventionProvider;
use App\InterventionServiceOffer\Equipement\State\GetCollectionEquipementProvider;
use App\InterventionServiceOffer\Equipement\State\GetCollectionEquipementsCoordinatesProvider;
use App\InterventionServiceOffer\Equipement\State\GetEquipementProvider;
use App\InterventionServiceOffer\Equipement\State\UpdateEquipementCommentProcessor;

#[Get(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/equipements/{id}',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_EQUIPEMENT')",
    output: Equipement::class,
    provider: GetEquipementProvider::class
)]
#[Post(
    read: false,
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/equipements/{id}/comment',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_MODIFIER_COMMENTAIRE_EQUIPEMENT')",
    input: CreateEquipement::class,
    processor: UpdateEquipementCommentProcessor::class,
)]
#[GetCollection(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/equipements',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_EQUIPEMENT')",
    output: Equipement::class,
    provider: GetCollectionEquipementProvider::class
)]
#[GetCollection(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/equipements-coordinates',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_EQUIPEMENT')",
    output: EquipementCoordinates::class,
    provider: GetCollectionEquipementsCoordinatesProvider::class,
)]
#[GetCollection(
    uriTemplate: 'gmao-configuration-interventions/{gmaoConfigurationInterventionId}/equipements',
    uriVariables: [
        'gmaoConfigurationInterventionId' => 'gmaoConfigurationInterventionId',
    ],
    security: "is_granted('ROLE_INTERVENTION_CONSULTER_EQUIPEMENT')",
    output: Equipement::class,
    provider: GetCollectionEquipementByGmaoConfigurationInterventionProvider::class,
)]
class EquipementApiRessource
{
}
