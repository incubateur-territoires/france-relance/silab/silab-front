<?php

namespace App\InterventionServiceOffer\Equipement\Entity\Equipement;

use App\Shared\Location\Entity\Coordinates;

class EquipementCoordinates
{
    public function __construct(
        private string $id,
        private string $label,
        private Coordinates $coordinates,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }
}
