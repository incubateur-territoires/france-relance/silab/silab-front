<?php

namespace App\InterventionServiceOffer\Equipement\Entity\Mesure;

class CreateMesure
{
    public function __construct(
        protected readonly float $valeur,
    ) {
    }

    public function getValeur(): float
    {
        return $this->valeur;
    }
}
