<?php

namespace App\InterventionServiceOffer\Equipement\Entity\Mesure;

class Mesure extends CreateMesure
{
    public function __construct(
        float $valeur,
        private readonly \DateTimeInterface $createdAt,
        private readonly string $createdBy,
    ) {
        parent::__construct($valeur);
    }

    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}
