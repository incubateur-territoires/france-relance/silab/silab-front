<?php

namespace App\InterventionServiceOffer\Equipement\Entity\Mesure;

use ApiPlatform\Metadata\Post;
use App\InterventionServiceOffer\Equipement\State\CreateMesureProcessor;

#[Post(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/compteurs/{compteurId}/mesures',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
        'compteurId' => 'compteurId',
    ],
    input: CreateMesure::class,
    output: Mesure::class,
    processor: CreateMesureProcessor::class,
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_RELEVER_COMPTEUR')",
)]
class MesureApiResource
{
}
