<?php

namespace App\InterventionServiceOffer\Equipement\Exception;

use App\Shared\Exception\RuntimeException;

class MalformedEquipementJsonException extends RuntimeException
{
}
