<?php

namespace App\InterventionServiceOffer\Equipement\Repository;

use App\InterventionServiceOffer\Carl\Entity\DbCarlCompteur;
use App\InterventionServiceOffer\Carl\Entity\DbCarlCompteurDbCarlEquipement;
use App\InterventionServiceOffer\Carl\Entity\DbCarlMesure;
use App\InterventionServiceOffer\Equipement\Entity\Compteur\Compteur;
use App\InterventionServiceOffer\Equipement\Entity\Mesure\CreateMesure;
use App\InterventionServiceOffer\Equipement\Entity\Mesure\Mesure;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Exception\EmailMatchingFailureExceptionInterface;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlCompteurDbCarlEquipementRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlCompteurRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlMesureRepository;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException as SymfonyBadRequestException;

class CarlCompteurRepository implements CompteurRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;

    private CarlConfigurationIntervention $carlConfigurationIntervention;
    private CarlClient $carlClient;

    public function __construct(
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private DbCarlCompteurDbCarlEquipementRepository $dbCarlCompteurDbCarlEquipementRepository,
        private DbCarlCompteurRepository $dbCarlCompteurRepository,
        private DbCarlMesureRepository $dbCarlMesureRepository
    ) {
        $carlConfigurationIntervention = $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration();
        if (is_null($carlConfigurationIntervention)) {
            throw new RuntimeException('Veuillez attacher une configuration GMAO à cette offre de service');
        }

        assert($carlConfigurationIntervention instanceof CarlConfigurationIntervention);
        $this->carlConfigurationIntervention = $carlConfigurationIntervention;
        $this->carlClient = $this->carlConfigurationIntervention->getCarlClient();
        if (is_null($this->carlClient)) {
            throw new RuntimeException('Veuillez configurer un client Carl pour cette offre de service');
        }
    }

    public function find($id): Compteur
    {
        $sampleCompteurArray = $this->findBy(['id' => [$id]]);

        if (count($sampleCompteurArray) > 1) {
            throw new RuntimeException("Plus d'un compteur trouvée pour l'id : $id");
        }

        if (0 === count($sampleCompteurArray)) {
            throw new RuntimeException("Aucune compteur trouvée pour l'id : $id");
        }

        return array_pop($sampleCompteurArray);
    }

    /**
     * @return array<Compteur>
     */
    public function findAll(): array
    {
        return $this->findBy([]);
    }

    /**
     * @return array<Compteur>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }
        if (null !== $orderBy) {
            throw new RuntimeException('Le paramètre orderBy n\'est pas encore implémenté');
        }
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
            ]
        );

        return array_values(
            DbCarlCompteur::toCompteurArray(
                $this->dbCarlCompteurRepository->findBy($criteria),
                $this->interventionServiceOfferContext->getServiceOffer(),
                $this->carlConfigurationIntervention
            )
        );
    }

    /**
     * @return array<Compteur>
     */
    public function findByEquipements(array $equipementsIds, bool $includeChilren, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }
        if (null !== $orderBy) {
            throw new RuntimeException('Le paramètre orderBy n\'est pas encore implémenté');
        }

        // Mappage des critères Silab vers dbCarl

        $criteria['directEqpt'] = !$includeChilren;
        $criteria['dbCarlEquipement'] = $equipementsIds;

        $dbCarlCompteurs = array_reduce(
            $this->dbCarlCompteurDbCarlEquipementRepository->findBy($criteria),
            function ($dbCarlCompteurs, $dbCarlCompteurDbCarlEquipement) {
                assert($dbCarlCompteurDbCarlEquipement instanceof DbCarlCompteurDbCarlEquipement);
                $currentDbCarlCompteur = $dbCarlCompteurDbCarlEquipement->getDbCarlCompteur();
                $dbCarlCompteurs[$currentDbCarlCompteur->getId()] = $currentDbCarlCompteur;

                return $dbCarlCompteurs;
            },
            []
        );

        return array_values(
            DbCarlCompteur::toCompteurArray(
                $dbCarlCompteurs,
                $this->interventionServiceOfferContext->getServiceOffer(),
                $this->carlConfigurationIntervention
            )
        );
    }

    public function findOneBy(array $criteria): Compteur
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function newMesure(CreateMesure $createMesure, string $compteurId, string $createdBy): Mesure
    {
        try {
            $carlMeasureReadingArray = [
                'data' => [
                    'attributes' => [
                        'origin' => 1, // relève manuelle
                        'measure' => $createMesure->getValeur(),
                        'dateMeasure' => (new \DateTime())->format(\DateTimeInterface::ATOM),
                    ],
                    'relationships' => [
                        'measurePoint' => [
                            'data' => [
                                'id' => $compteurId,
                                'type' => 'measurepoint',
                            ],
                        ],
                        'createdBy' => [
                            'data' => [
                                'id' => $this->carlClient->getUserIdFromEmail($createdBy),
                                'type' => 'actor',
                            ],
                        ],
                    ],
                ],
            ];
        } catch (EmailMatchingFailureExceptionInterface $e) {
            throw new SymfonyBadRequestException($e->getMessage());
        }

        $createdMesureJsonApiResource = $this->carlClient->postCarlObject(
            entity: 'measurereading',
            postedResource: json_encode($carlMeasureReadingArray),
            includes: ['createdBy']
        );

        return new Mesure(
            valeur: $createdMesureJsonApiResource->getAttribute('measure'),
            createdAt: \DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, $createdMesureJsonApiResource->getAttribute('dateMeasure')),
            createdBy: $this->carlClient->getUserEmailFromId($createdMesureJsonApiResource->getRelationship('createdBy')->getId())
        );
    }

    public function findHistoriqueDesMesures(string $compteurId): array
    {
        return DbCarlMesure::toMesureArray(
            $this->dbCarlMesureRepository->findBy(
                ['compteurId' => $compteurId],
                ['createdAt' => 'DESC']
            )
        );
    }

    public function getClassName(): string
    {
        return Compteur::class;
    }
}
