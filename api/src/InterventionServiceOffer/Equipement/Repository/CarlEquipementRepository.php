<?php

namespace App\InterventionServiceOffer\Equipement\Repository;

use ApiPlatform\State\Pagination\PaginatorInterface;
use App\InterventionServiceOffer\Carl\Entity\DbCarlEquipement;
use App\InterventionServiceOffer\Carl\Entity\DbCarlEquipementCoordinate;
use App\InterventionServiceOffer\Carl\Service\CarlClientContext;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Gmao\Service\CarlConfigurationInterventionContext;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\ApiPlatform\Pagination\TraversablePaginator;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlEquipementCoordinateRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlEquipementRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlEquipementTypeRepository;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Carl\Trait\UnexpectedOrderByValueCheckingTrait;
use App\Shared\Exception\RuntimeException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlEquipementRepository implements EquipementRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;
    use UnexpectedOrderByValueCheckingTrait;

    public function __construct(
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private CarlConfigurationInterventionContext $carlConfigurationInterventionContext,
        private DbCarlEquipementRepository $dbCarlEquipementRepository,
        private DbCarlEquipementTypeRepository $dbCarlEquipementTypeRepository,
        private DbCarlEquipementCoordinateRepository $dbCarlEquipementCoordinateRepository,
        private CarlClientContext $carlClientContext
    ) {
    }

    public function find($id): Equipement
    {
        $sampleEquipementArray = iterator_to_array($this->findBy(['id' => [$id]])->getIterator());

        if (count($sampleEquipementArray) > 1) {
            throw new RuntimeException("Plus d'un équipement trouvé pour l'id : $id");
        }

        if (0 === count($sampleEquipementArray)) {
            throw new RuntimeException("Aucun équipement trouvé pour l'id : $id");
        }

        return $sampleEquipementArray[0];
    }

    /**
     * @return PaginatorInterface<Equipement>
     */
    public function findAll(): PaginatorInterface
    {
        throw new RuntimeException('methode non implémenté');
    }

    /**
     * @return TraversablePaginator<Equipement>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): TraversablePaginator
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $includeAncetresFilter = $propertyAccessor->getValue($criteria, '[includeAncetres]');
        $includeAncetres = false;
        if ('true' === $includeAncetresFilter) {
            $includeAncetres = true;
        }
        unset($criteria['includeAncetres']);

        $query = $this->createQueryBy(
            entityRepository: $this->dbCarlEquipementRepository,
            criteria: $criteria,
            orderBy: $orderBy,
            limit: $limit,
            offset: $offset
        );

        $dbCarlEquipementsPaginator = new DoctrinePaginator(
            $query
        );
        $limit ??= PHP_INT_MAX;

        $page = intdiv($offset ?? 0, $limit) + 1;

        $gmaoCarl = $this->carlConfigurationInterventionContext->getCarlConfiguration();

        return new TraversablePaginator(
            new \ArrayIterator(array_map(
                function ($dbCarlEquipement) use ($gmaoCarl, $includeAncetres) {
                    assert($dbCarlEquipement instanceof DbCarlEquipement);

                    return $dbCarlEquipement->toEquipement(
                        gmaoCarl: $gmaoCarl,
                        includeAncetres: $includeAncetres,
                        serviceOffer: $this->interventionServiceOfferContext->getServiceOfferSafe()
                    );
                },
                iterator_to_array($dbCarlEquipementsPaginator->getIterator())
            )),
            $page,
            $limit,
            $dbCarlEquipementsPaginator->count()
        );
    }

    public function findCoordinatesBy(array $criteria): array
    {
        return array_map(
            function ($dbCarlEquipementCoordinates) {
                assert($dbCarlEquipementCoordinates instanceof DbCarlEquipementCoordinate);

                return $dbCarlEquipementCoordinates->toEquipementCoordinates();
            },
            $this->createQueryBy(
                entityRepository: $this->dbCarlEquipementCoordinateRepository,
                isCoordinate: true,
                criteria: $criteria,
                orderBy: null
            )->getResult()
        );
    }

    /**
     * @template EntityType of object
     *
     * @param ServiceEntityRepository<EntityType> $entityRepository
     * @param array<int,string>|null              $orderBy
     * @param array<string, mixed>                $criteria
     */
    public function createQueryBy(
        ServiceEntityRepository $entityRepository,
        ?bool $isCoordinate = false,
        ?array $criteria = [],
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null,
    ): Query {
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
                'labelOuCode',
                'currentStatus.code',
                'equipementRacine.id',
                'parentDirectUniquement',
                'pinnedIds',
            ]
        );
        $equipementQueryBuilder = $entityRepository->createQueryBuilder('dbCarlEquipementLike');

        // Mappage des critères Silab vers dbCarl
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $equipementRacineIds = $propertyAccessor->getValue($criteria, '[equipementRacine.id]');
        if (!empty($equipementRacineIds)) {
            $equipementQueryBuilder->join('dbCarlEquipementLike.dbCarlEquipement'.($isCoordinate ? 'Coordinate' : '').'DbCarlEquipementAncestors', 'dbCarlEquipementDbCarlEquipement')
            ->andWhere('dbCarlEquipementDbCarlEquipement.dbCarlEquipement'.($isCoordinate ? 'Coordinate' : '').'Ancestor IN (:ancestorIds)')
            ->setParameter('ancestorIds', $equipementRacineIds);

            $parentDirectUniquementFilter = $propertyAccessor->getValue($criteria, '[parentDirectUniquement]');
            if ('true' === $parentDirectUniquementFilter) {
                $equipementQueryBuilder
                    ->andWhere('dbCarlEquipementDbCarlEquipement.directEqpt = :parentDirectUniquementFilter')
                    ->setParameter('parentDirectUniquementFilter', true);
            }
        }

        $equipementIds = $propertyAccessor->getValue($criteria, '[id]');
        if (!empty($equipementIds)) {
            $equipementQueryBuilder->andWhere('dbCarlEquipementLike.id IN (:ids) ')->setParameter('ids', $equipementIds);
        }

        $labelOuCode = $propertyAccessor->getValue($criteria, '[labelOuCode]');
        if (!empty($labelOuCode)) {
            $labelOuCode = str_replace('%', '\\%', strtolower($labelOuCode));
            $labelOuCode = str_replace('*', '%', $labelOuCode);
            $equipementQueryBuilder
            ->andWhere('LOWER(dbCarlEquipementLike.label) LIKE :label OR LOWER(dbCarlEquipementLike.code) LIKE :code')
            ->setParameter('label', $labelOuCode)
            ->setParameter('code', $labelOuCode);
        }

        $silabStatusCodes = $propertyAccessor->getValue($criteria, '[currentStatus.code]');
        if (!empty($silabStatusCodes)) {
            $carlStatusCodes = self::getCompatibleCarlEquipementStatusCodesFromSilabStatusCodes($silabStatusCodes);
            $equipementQueryBuilder->andWhere('dbCarlEquipementLike.statusCode IN (:statusCodes) ')->setParameter('statusCodes', $carlStatusCodes);
        }

        $pinnedIds = $propertyAccessor->getValue($criteria, '[pinnedIds]');
        if (!empty($pinnedIds)) {
            $equipementQueryBuilder->addSelect('COALESCE(ARRAY_POSITION(:pinnedIds,dbCarlEquipementLike.id),'.(count($pinnedIds) + 1).') AS HIDDEN pinned_order')
            ->setParameter('pinnedIds', $pinnedIds)
            ->addOrderBy('pinned_order', 'ASC');
        }

        if (null !== $orderBy) {
            $this->throwIfUnexpectedOrderByValueCheckingTrait(
                $orderBy,
                [
                    'label',
                    '-label',
                ]
            );

            foreach ($orderBy as $orderByAttribute) {
                $equipementQueryBuilder->addOrderBy(
                    match ($orderByAttribute) {
                        'label' => new OrderBy('dbCarlEquipementLike.label', 'ASC'),
                        '-label' => new OrderBy('dbCarlEquipementLike.label', 'DESC'),
                        default => new OrderBy('dbCarlEquipementLike.label', 'ASC')
                    }
                );
            }
        }

        $equipementQueryBuilder->getEntityManager()->clear(); // Si un appel a déjà été fait sur cet objet permet de le détacher afin de récupérer l'état actuel en db.

        $equipementQueryBuilder->setMaxResults($limit);

        return $equipementQueryBuilder
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery();
    }

    public function findOneBy(array $criteria): Equipement
    {
        $équipements = $this->findBy(criteria: $criteria, limit: 1);
        $équipementArray = iterator_to_array($équipements->getIterator());
        if (0 === count($équipementArray)) {
            throw new RuntimeException('Aucun équipement trouvé avec les critères donnés');
        }

        // Assurez-vous que le premier élément est de type Equipement
        $equipement = $équipementArray[0];
        assert($equipement instanceof Equipement);

        return $equipement;
    }

    public function getClassName(): string
    {
        return Equipement::class;
    }

    public function update(string $id, Equipement $entity): Equipement
    {
        $carlEquipementIdAndType = $this->dbCarlEquipementTypeRepository->find($id);
        $equipement = $entity;
        $originalEquipement = $this->find($id);

        $carlCommentRelationshipNameForThisEntityType = $this->carlClientContext->getCarlClient()->getEquipmentCommentFieldFromType($carlEquipementIdAndType->getTypeId());

        if (!empty($equipement->getComment())) {
            if (!empty($originalEquipement->getComment())) {
                $this->carlClientContext->getCarlClient()->patchCarlObject(
                    'description',
                    $this->carlClientContext->getCarlClient()->getCarlObject($carlEquipementIdAndType->getTypeId(), $carlEquipementIdAndType->getId(), [$carlCommentRelationshipNameForThisEntityType])
                        ->getRelationship($carlCommentRelationshipNameForThisEntityType)
                        ->getId(),
                    ['description' => $equipement->getComment()]
                );
            } else {
                $relationships = [];
                $this->carlClientContext->getCarlClient()->addDescriptionToObjectRelationshipsArray(
                    objectRelationShipsArray: $relationships,
                    description: $equipement->getComment(),
                    descriptionRelashionshipName: $carlCommentRelationshipNameForThisEntityType
                );
                $this->carlClientContext->getCarlClient()->patchCarlObject(
                    entity: $carlEquipementIdAndType->getTypeId(),
                    objectId: $carlEquipementIdAndType->getId(),
                    relationships: $relationships
                );
            }
        } elseif (!empty($originalEquipement->getComment())) {
            $this->carlClientContext->getCarlClient()->deleteObjectDescription(
                objectId: $carlEquipementIdAndType->getId(),
                entity: $carlEquipementIdAndType->getTypeId(),
                descriptionRelashionshipName: $carlCommentRelationshipNameForThisEntityType
            );
        }

        return $this->find($id);
    }

    public function getImageBlob(string $id): string
    {
        return $this->carlClientContext->getCarlClient()->getDocumentBlob($id);
    }

    /**
     * @param array<string> $silabStatusCodes
     *
     * @return array<string>
     */
    private static function getCompatibleCarlEquipementStatusCodesFromSilabStatusCodes(array $silabStatusCodes): array
    {
        return array_reduce(
            $silabStatusCodes,
            function ($carlStatusCodesAccumulator, $silabStatusCode) {
                $compatibleCarlStatusCodes = DbCarlEquipement::SILAB_EQUIPEMENT_STATUS_CODE_TO_COMPATIBLE_CARL_EQUIPEMENT_STATUS_CODE_MAPPING[$silabStatusCode];

                return array_unique(array_merge($carlStatusCodesAccumulator, $compatibleCarlStatusCodes));
            },
            []
        );
    }
}
