<?php

namespace App\InterventionServiceOffer\Equipement\Repository;

use App\InterventionServiceOffer\Equipement\Entity\Compteur\Compteur;
use App\InterventionServiceOffer\Equipement\Entity\Mesure\CreateMesure;
use App\InterventionServiceOffer\Equipement\Entity\Mesure\Mesure;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Exception\RuntimeException;

class CompteurRepository implements CompteurRepositoryInterface
{
    private CompteurRepositoryInterface $specificCompteurRepository;

    public function __construct(
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private CarlCompteurRepository $carlCompteurRepository// AMELIORATION: ne pas importer tous les repo, ça va péter le jour ou on met autre chose que du carl, cf. CarlClientContext
    ) {
        try {
            $this->specificCompteurRepository = match (
                $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class
            ) {
                CarlConfigurationIntervention::class => $this->carlCompteurRepository
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class);
        }
    }

    public function find($id): Compteur
    {
        return $this->specificCompteurRepository->find($id);
    }

    public function findAll(): array
    {
        return $this->specificCompteurRepository->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->specificCompteurRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findByEquipements(array $equipementsIds, bool $includeChilren, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->specificCompteurRepository->findByEquipements($equipementsIds, $includeChilren, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): Compteur
    {
        return $this->specificCompteurRepository->findOneBy($criteria);
    }

    public function getClassName()
    {
        return Compteur::class;
    }

    public function newMesure(CreateMesure $createMesure, string $compteurId, string $createdBy): Mesure
    {
        return $this->specificCompteurRepository->newMesure(
            createMesure: $createMesure,
            compteurId: $compteurId,
            createdBy: $createdBy
        );
    }

    public function findHistoriqueDesMesures(string $compteurId): array
    {
        return $this->specificCompteurRepository->findHistoriqueDesMesures($compteurId);
    }
}
