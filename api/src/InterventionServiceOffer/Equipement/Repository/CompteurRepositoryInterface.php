<?php

namespace App\InterventionServiceOffer\Equipement\Repository;

use App\InterventionServiceOffer\Equipement\Entity\Compteur\Compteur;
use App\InterventionServiceOffer\Equipement\Entity\Mesure\CreateMesure;
use App\InterventionServiceOffer\Equipement\Entity\Mesure\Mesure;
use Doctrine\Persistence\ObjectRepository;

interface CompteurRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): Compteur;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<Compteur>
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array;

    /**
     * @param array<string>          $equipementsIds
     * @param array<int,string>|null $orderBy
     *
     * @return array<Compteur>
     */
    public function findByEquipements(
        array $equipementsIds,
        bool $includeChilren,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array;

    /**
     * @return array<Compteur>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): Compteur;

    public function newMesure(CreateMesure $createMesure, string $compteurId, string $createdBy): Mesure;

    /**
     * @return array<Mesure> toutes les mesures effectuées depuis la création du compteur
     *                       triées par ordre de dates de relève décroissantes
     */
    public function findHistoriqueDesMesures(string $compteurId): array;
}
