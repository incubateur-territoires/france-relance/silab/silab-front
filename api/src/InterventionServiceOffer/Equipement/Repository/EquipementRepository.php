<?php

namespace App\InterventionServiceOffer\Equipement\Repository;

use ApiPlatform\State\Pagination\PaginatorInterface;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Gmao\Service\GmaoConfigurationInterventionContext;
use App\Shared\Exception\RuntimeException;

class EquipementRepository implements EquipementRepositoryInterface
{
    private EquipementRepositoryInterface $specificEquipementRepository;

    public function __construct(
        private GmaoConfigurationInterventionContext $gmaoConfigurationInterventionContext,
        CarlEquipementRepository $carlEquipementRepository
    ) {
        try {
            $this->specificEquipementRepository = match ($gmaoConfigurationInterventionContext->getGmaoConfiguration()::class) {
                CarlConfigurationIntervention::class => $carlEquipementRepository,
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfigurationInterventionContext->getGmaoConfiguration()::class);
        }
    }

    public function find($id): Equipement
    {
        $equipement = $this->specificEquipementRepository->find($id);

        return $equipement
            ->setGmaoObjectViewUrl(
                $this->gmaoConfigurationInterventionContext->getGmaoConfiguration()->getEquipementViewUrl($equipement)
            );
    }

    public function findAll(): PaginatorInterface
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): PaginatorInterface
    {
        $equipements = $this->specificEquipementRepository->findBy($criteria, $orderBy, $limit, $offset);
        foreach ($equipements as $equipement) {
            $equipement
                    ->setGmaoObjectViewUrl(
                        $this->gmaoConfigurationInterventionContext->getGmaoConfiguration()->getEquipementViewUrl($equipement)
                    );
        }

        return $equipements;
    }

    public function findCoordinatesBy(array $criteria): array
    {
        return $this->specificEquipementRepository->findCoordinatesBy($criteria);
    }

    public function findOneBy(array $criteria): Equipement
    {
        $equipement = $this->specificEquipementRepository->findOneBy($criteria);

        return $equipement
            ->setGmaoObjectViewUrl(
                $this->gmaoConfigurationInterventionContext->getGmaoConfiguration()->getEquipementViewUrl($equipement)
            );
    }

    public function update(string $id, Equipement $entity): Equipement
    {
        return $this->specificEquipementRepository->update($id, $entity);
    }

    public function getImageBlob(string $id): string
    {
        return $this->specificEquipementRepository->getImageBlob(id: $id);
    }

    public function getClassName()
    {
        return Equipement::class;
    }
}
