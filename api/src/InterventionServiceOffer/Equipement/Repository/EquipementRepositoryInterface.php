<?php

namespace App\InterventionServiceOffer\Equipement\Repository;

use ApiPlatform\State\Pagination\PaginatorInterface;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\EquipementCoordinates;
use App\Shared\Doctrine\Persistence\PaginableObjectRepositoryInterface;

/**
 * @extends PaginableObjectRepositoryInterface<Equipement>
 */
interface EquipementRepositoryInterface extends PaginableObjectRepositoryInterface
{
    public function find(mixed $id): Equipement;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return PaginatorInterface<Equipement>
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): PaginatorInterface;

    /**
     * @param array<string,mixed> $criteria
     *
     * @return array<EquipementCoordinates>
     */
    public function findCoordinatesBy(
        array $criteria,
    ): array;

    /**
     * @return PaginatorInterface<Equipement>
     */
    public function findAll(): PaginatorInterface;

    public function findOneBy(array $criteria): Equipement;

    public function update(string $id, Equipement $entity): Equipement;

    public function getImageBlob(string $id): string;
}
