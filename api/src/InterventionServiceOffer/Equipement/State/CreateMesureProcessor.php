<?php

namespace App\InterventionServiceOffer\Equipement\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\Equipement\Entity\Mesure\CreateMesure;
use App\InterventionServiceOffer\Equipement\Entity\Mesure\Mesure;
use App\InterventionServiceOffer\Equipement\Repository\CompteurRepository;
use App\Shared\User\Service\CurrentUserContextInterface;
use Psr\Log\LoggerInterface;

final class CreateMesureProcessor implements ProcessorInterface
{
    public function __construct(
        private CompteurRepository $compteurRepository,
        private CurrentUserContextInterface $currentUserContext,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param Mesure       $data
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): Mesure {
        $this->logger->debug('Création d\'une nouvelle mesure', ['data' => json_encode($data), 'context' => $context, 'uriVariables' => $uriVariables]);

        assert($data instanceof CreateMesure);

        return $this->compteurRepository->newMesure(
            createMesure: $data,
            compteurId: $uriVariables['compteurId'],
            createdBy: $this->currentUserContext->getCurrentUser()->getEmail());
    }
}
