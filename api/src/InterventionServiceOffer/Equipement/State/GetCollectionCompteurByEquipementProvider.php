<?php

namespace App\InterventionServiceOffer\Equipement\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Equipement\Entity\Compteur\Compteur;
use App\InterventionServiceOffer\Equipement\Repository\CompteurRepository;
use Symfony\Component\PropertyAccess\PropertyAccess;

final class GetCollectionCompteurByEquipementProvider implements ProviderInterface
{
    public function __construct(
        private CompteurRepository $compteurRepository,
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<Compteur>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $criteria = $context['filters'] ?? [];
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $includeChildren = ('true' === $propertyAccessor->getValue($criteria, '[includeChildren]'));

        return $this->compteurRepository->findByEquipements([$uriVariables['equipementId']], $includeChildren);
    }
}
