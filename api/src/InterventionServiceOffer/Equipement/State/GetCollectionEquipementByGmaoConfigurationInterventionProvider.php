<?php

namespace App\InterventionServiceOffer\Equipement\State;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\Pagination\PaginatorInterface;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Equipement\Repository\EquipementRepository;
use Symfony\Component\PropertyAccess\PropertyAccess;

final class GetCollectionEquipementByGmaoConfigurationInterventionProvider implements ProviderInterface
{
    public function __construct(
        private EquipementRepository $equipementRepository,
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return PaginatorInterface<Equipement>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): PaginatorInterface
    {
        assert($operation instanceof GetCollection);
        $criteria = $context['filters'] ?? [];
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $limit = null;
        if ($propertyAccessor->getValue($criteria, '[limit]')) {
            $limit = $criteria['limit'];
            unset($criteria['limit']);
        }

        // on s'assure d'avoir les status sous forme de tableau
        if (isset($criteria['currentStatus.code']) && is_string($criteria['currentStatus.code'])) {
            $criteria['currentStatus.code'] = [$criteria['currentStatus.code']];
        }

        return $this->equipementRepository->findBy(criteria: $criteria, limit: $limit);
    }
}
