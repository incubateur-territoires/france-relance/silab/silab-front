<?php

namespace App\InterventionServiceOffer\Equipement\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\Pagination\Pagination;
use ApiPlatform\State\Pagination\PaginatorInterface;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Equipement\Repository\EquipementRepository;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;

final class GetCollectionEquipementProvider implements ProviderInterface
{
    public function __construct(
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private EquipementRepository $equipementRepository,
        private readonly Pagination $pagination,
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return PaginatorInterface<Equipement>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): PaginatorInterface
    {
        assert($operation instanceof CollectionOperationInterface);

        $criteria = $context['filters'] ?? [];

        // on s'assure d'avoir les status sous forme de tableau
        if (isset($criteria['currentStatus.code']) && is_string($criteria['currentStatus.code'])) {
            $criteria['currentStatus.code'] = [$criteria['currentStatus.code']];
        }

        // on s'assure d'avoir les id d'ancetre sous forme de tableau
        if (isset($criteria['equipementRacine.id']) && is_string($criteria['equipementRacine.id'])) {
            $criteria['equipementRacine.id'] = [$criteria['equipementRacine.id']];
        }

        // si aucun filtrage par identifiant ou parent, on retourne les équipement définits dans l'offre de service
        // AMELIORATION : ça fait un peu bidouille quand même...
        $aucunFiltrageSurDesIds = empty($criteria['id']);
        $aucunFiltrageSurUnEquipementRacine = empty($criteria['equipementRacine.id']);
        if ($aucunFiltrageSurDesIds && $aucunFiltrageSurUnEquipementRacine) {
            $criteria['id'] = $this->interventionServiceOfferContext->getServiceOffer()->getAvailableEquipementsIds();
        }

        // from : https://api-platform.com/docs/guides/custom-pagination/
        [, $offset , $limit] = $this->pagination->getPagination($operation, $context);
        unset($criteria['page']);
        if (isset($criteria['orderBy'])) {
            $orderBy = $criteria['orderBy'];
            unset($criteria['orderBy']);
        } else {
            $orderBy = ['label'];
        }

        return $this->equipementRepository->findBy($criteria, $orderBy, $limit, $offset);
    }
}
