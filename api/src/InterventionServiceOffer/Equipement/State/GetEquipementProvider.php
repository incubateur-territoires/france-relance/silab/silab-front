<?php

namespace App\InterventionServiceOffer\Equipement\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Equipement\Repository\EquipementRepository;

final class GetEquipementProvider implements ProviderInterface
{
    public function __construct(
        private EquipementRepository $equipementRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): Equipement
    {
        assert($operation instanceof Get);

        $criteria = $context['filters'] ?? [];
        $criteria['id'] = [$uriVariables['id']];

        return $this->equipementRepository->findOneBy($criteria);
    }
}
