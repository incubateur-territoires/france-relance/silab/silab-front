<?php

namespace App\InterventionServiceOffer\Equipement\State;

use App\InterventionServiceOffer\Equipement\Repository\CompteurRepository;
use OpenSpout\Common\Entity\Cell;
use OpenSpout\Common\Entity\Row;
use OpenSpout\Common\Entity\Style\Style;
use OpenSpout\Writer\XLSX\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(
    '/api/intervention-service-offers/{serviceOfferId}/compteurs/{compteurId}/historique-des-releves',
    name: 'interventionServiceOffer_historiqueDesReleves',
    methods: ['GET'],
)]
#[IsGranted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_COMPTEUR')]
class GetHistoriqueDesRelevesController extends AbstractController
{
    public function __invoke(
        CompteurRepository $compteurRepository,
        string $compteurId,
        #[MapQueryParameter] ?string $timezone = 'Europe/Paris'// vive la france 🔵⚪🔴 !
    ): Response {
        $compteur = $compteurRepository->find($compteurId);
        $mesuresRelevées = $compteurRepository->findHistoriqueDesMesures($compteurId);
        $timezoneClient = new \DateTimeZone($timezone);

        $writerOptions = new Options();
        $nomDeLaColoneDeValeurs = 'Valeur de '.$compteur->getLabel();
        $writerOptions->setColumnWidth(32, 1); // dates
        $nomDeLaColoneDeLUnité = 'Unité';
        $writerOptions->setColumnWidth(strlen($nomDeLaColoneDeValeurs), 2); // valeurs
        $writerOptions->setColumnWidth(max(strlen($compteur->getUnite()), strlen($nomDeLaColoneDeLUnité)), 3); // unité
        $writer = new \OpenSpout\Writer\XLSX\Writer($writerOptions);

        ob_start();
        $writer->openToFile('php://output');

        $rows = [
            new Row([
                Cell::fromValue('Date'),
                Cell::fromValue($nomDeLaColoneDeValeurs),
                Cell::fromValue($nomDeLaColoneDeLUnité),
            ]),
        ];

        foreach ($mesuresRelevées as $mesure) {
            $createdAtAvecLaTimezoneDuClient = \DateTime::createFromInterface($mesure->getCreatedAt())->setTimezone($timezoneClient);
            $createdAtCell = Cell::fromValue($createdAtAvecLaTimezoneDuClient);
            $createdAtStyle = new Style();
            $createdAtStyle->setFormat(
                'dd/mm/yyyy hh:mm "'
                .$createdAtAvecLaTimezoneDuClient->getTimezone()->getName()
                .'"'
            );
            $createdAtCell->setStyle($createdAtStyle);
            $rows[] = new Row(
                [
                    $createdAtCell,
                    Cell::fromValue($mesure->getValeur()),
                    Cell::fromValue($compteur->getUnite()),
                ]
            );
        }

        $writer->addRows($rows);
        $writer->close();
        $blob = ob_get_clean();

        $response = new Response();
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-length', (string) strlen($blob));
        $response->headers->set('Content-Disposition', 'attachment; filename='.urlencode('historique_releves_'.$compteur->getCode().'_'.(new \DateTime(timezone: $timezoneClient))->format('d-m-Y')).'.xlsx');
        $response->setContent($blob);

        return $response;
    }
}
