<?php

namespace App\InterventionServiceOffer\Equipement\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\CreateEquipement;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Equipement\Repository\EquipementRepository;

final class UpdateEquipementCommentProcessor implements ProcessorInterface
{
    public function __construct(
        private EquipementRepository $equipementRepository
    ) {
    }

    /**
     * @param CreateEquipement $data
     * @param array<mixed>     $uriVariables
     * @param array<mixed>     $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): Equipement {
        assert($operation instanceof Post, 'Opération non supportée');

        assert($data instanceof CreateEquipement);

        $equipement = $this->equipementRepository->find($uriVariables['id']);
        $equipement->setComment($data->getComment());
        $this->equipementRepository->update($uriVariables['id'], $equipement);

        return $equipement;
    }
}
