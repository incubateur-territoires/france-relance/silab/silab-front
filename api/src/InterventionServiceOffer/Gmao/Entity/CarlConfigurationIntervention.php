<?php

namespace App\InterventionServiceOffer\Gmao\Entity;

use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeIntervention;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Gmao\Repository\CarlConfigurationInterventionRepository;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\ValueObject\CarlObject;
use App\Shared\Document\Entity\DocumentBlob;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Serializer;

#[Post(
    uriTemplate: 'carl-configurations-intervention',
    security: "is_granted('ROLE_CREER_GMAO_CONFIGURATION_INTERVENTION')",
    denormalizationContext: ['groups' => [
        GmaoConfigurationIntervention::GROUP_CREER_GMAO_CONFIGURATION_INTERVENTION,
        CarlConfigurationIntervention::GROUP_CREER_CARL_CONFIGURATION_INTERVENTION,
    ]],
    normalizationContext: ['groups' => [
        GmaoConfigurationIntervention::GROUP_AFFICHER_GMAO_CONFIGURATION_INTERVENTION,
        CarlConfigurationIntervention::GROUP_AFFICHER_CARL_CONFIGURATION_INTERVENTION,
    ]]
)]
#[Put(
    uriTemplate: 'carl-configurations-intervention/{id}',
    security: "is_granted('ROLE_MODIFIER_GMAO_CONFIGURATION_INTERVENTION')",
    denormalizationContext: ['groups' => [
        GmaoConfigurationIntervention::GROUP_CREER_GMAO_CONFIGURATION_INTERVENTION,
        CarlConfigurationIntervention::GROUP_CREER_CARL_CONFIGURATION_INTERVENTION,
    ]],
    normalizationContext: ['groups' => [
        GmaoConfigurationIntervention::GROUP_AFFICHER_GMAO_CONFIGURATION_INTERVENTION,
        CarlConfigurationIntervention::GROUP_AFFICHER_CARL_CONFIGURATION_INTERVENTION,
    ]]
)]
#[GetCollection(
    uriTemplate: 'carl-configurations-intervention',
    security: "is_granted('ROLE_CONSULTER_GMAO_CONFIGURATION_INTERVENTION')",
    normalizationContext: ['groups' => [
        GmaoConfigurationIntervention::GROUP_AFFICHER_GMAO_CONFIGURATION_INTERVENTION,
        CarlConfigurationIntervention::GROUP_AFFICHER_CARL_CONFIGURATION_INTERVENTION,
    ]],
    order: ['title' => 'ASC']
)]
#[Get(
    uriTemplate: 'carl-configurations-intervention/{id}',
    security: "is_granted('ROLE_CONSULTER_GMAO_CONFIGURATION_INTERVENTION')",
    normalizationContext: ['groups' => [
        GmaoConfigurationIntervention::GROUP_AFFICHER_GMAO_CONFIGURATION_INTERVENTION,
        CarlConfigurationIntervention::GROUP_AFFICHER_CARL_CONFIGURATION_INTERVENTION,
    ]]
)]
#[Delete(
    uriTemplate: 'carl-configurations-intervention/{id}',
    security: "is_granted('ROLE_SUPPRIMER_GMAO_CONFIGURATION_INTERVENTION')",
)]
#[Groups([
    CarlConfigurationIntervention::GROUP_AFFICHER_CARL_CONFIGURATION_INTERVENTION,
    CarlConfigurationIntervention::GROUP_CREER_CARL_CONFIGURATION_INTERVENTION,
])]
#[ORM\Entity(repositoryClass: CarlConfigurationInterventionRepository::class)]
class CarlConfigurationIntervention extends GmaoConfigurationIntervention
{
    public const GROUP_AFFICHER_CARL_CONFIGURATION_INTERVENTION =
        "Afficher la configuration carl d'une offre de service intervention";
    public const GROUP_CREER_CARL_CONFIGURATION_INTERVENTION =
        "Créer la configuration carl d'une offre de service intervention";

    /** @var array<int,string> */
    #[ORM\Column(options: ['default' => '[]'])]
    private array $interventionCostCenterIdsFilter = [];

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $costCenterIdForInterventionCreation = null;

    #[ORM\Column(options: ['default' => true])]
    private bool $useEquipmentCostCenterForInterventionCreation;

    /** @var array<int,string> */
    #[ORM\Column(nullable: true)]
    private array $directions = [];

    /** @var array<string,string> */
    #[ORM\Column(type: 'json', options: ['default' => '{}'])]
    #[Context(
        normalizationContext: [Serializer::EMPTY_ARRAY_AS_OBJECT => true]
    )] // ne fonctionne pas
    private array $actionTypesMap = [];

    #[ORM\Column(type: 'json', options: ['default' => '{}'])]
    #[Context(
        normalizationContext: [Serializer::EMPTY_ARRAY_AS_OBJECT => true]
    )] // QUESTION_TECHNIQUE: ne fonctionne pas, pk ?
    private mixed $carlAttributesPresetForInterventionCreation = [];

    /** @var array<string,CarlObject> */
    #[ORM\Column(type: 'json', options: ['default' => '{}'])]
    #[Context(normalizationContext: [Serializer::EMPTY_ARRAY_AS_OBJECT => true])] // ne fonctionne pas
    private array $carlRelationshipsPresetForInterventionCreation = [];

    #[ORM\Column(type: 'json', options: ['default' => '{}'])]
    #[Context(
        normalizationContext: [Serializer::EMPTY_ARRAY_AS_OBJECT => true]
    )] // ne fonctionne pas
    private mixed $carlAttributesPresetForDemandeInterventionCreation = [];

    /** @var array<string,CarlObject> */
    #[ORM\Column(type: 'json', options: ['default' => '{}'])]
    #[Context(normalizationContext: [Serializer::EMPTY_ARRAY_AS_OBJECT => true])] // ne fonctionne pas
    private array $carlRelationshipsPresetForDemandeInterventionCreation = [];

    #[ORM\Column(options: ['default' => 'IMG'])]
    private ?string $photoAvantInterventionDoctypeId = null;

    #[ORM\Column(nullable: true)]
    private ?string $photoApresInterventionDoctypeId = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?CarlClient $carlClient = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $woViewUrlSubPath = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $mrViewUrlSubPath = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $boxViewUrlSubPath = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $materialViewUrlSubPath = null;

    /**
     * La fonction de recherche d'enfants sera limitée aux types définis dans cette liste.
     *
     * @var array<int,string>
     */
    #[ORM\Column(nullable: true, options: ['default' => '["building", "zone"]'])]
    private array $includeEquipementsChildrenWithType = [];

    public function getDocumentBlob(string $documentId): DocumentBlob
    {
        return $this->carlClient->getDocumentBlob($documentId);
    }

    /** @return array<string> */
    public function getInterventionCostCenterIdsFilter(): array
    {
        return array_values($this->interventionCostCenterIdsFilter);
    }

    /**
     * @param array<string> $interventionCostCenterIdsFilter
     */
    public function setInterventionCostCenterIdsFilter(array $interventionCostCenterIdsFilter): self
    {
        $this->interventionCostCenterIdsFilter = array_values($interventionCostCenterIdsFilter);

        return $this;
    }

    public function getCostCenterIdForInterventionCreation(): ?string
    {
        return $this->costCenterIdForInterventionCreation;
    }

    public function setCostCenterIdForInterventionCreation(string $costCenterForInterventionCreation): self
    {
        $this->costCenterIdForInterventionCreation = $costCenterForInterventionCreation;

        return $this;
    }

    public function getUseEquipmentCostCenterForInterventionCreation(): bool
    {
        return $this->useEquipmentCostCenterForInterventionCreation;
    }

    public function setUseEquipmentCostCenterForInterventionCreation(
        bool $useEquipmentCostCenterForInterventionCreation
    ): self {
        $this->useEquipmentCostCenterForInterventionCreation = $useEquipmentCostCenterForInterventionCreation;

        return $this;
    }

    /** @return array<string>|null */
    public function getDirections(): ?array
    {
        return array_values($this->directions);
    }

    /**
     * @param array<string>|null $directions
     */
    public function setDirections(?array $directions): self
    {
        $this->directions = array_values($directions);

        return $this;
    }

    /**
     * @return array<string,string>
     */
    public function getActionTypesMap(): array
    {
        return $this->actionTypesMap;
    }

    /**
     * @param array<string,string> $actionTypesMap
     */
    public function setActionTypesMap(array $actionTypesMap): self
    {
        $this->actionTypesMap = $actionTypesMap;

        return $this;
    }

    /**
     * @return array<string,string>
     */
    // #[Context([Serializer::EMPTY_ARRAY_AS_OBJECT => true])]
    public function getCarlAttributesPresetForInterventionCreation(): array
    {
        return $this->carlAttributesPresetForInterventionCreation;
    }

    /**
     * @param array<string,string> $carlAttributesPresetForInterventionCreation
     */
    public function setCarlAttributesPresetForInterventionCreation(
        array $carlAttributesPresetForInterventionCreation
    ): self {
        $this->carlAttributesPresetForInterventionCreation = $carlAttributesPresetForInterventionCreation;

        return $this;
    }

    /**
     * @return array<string,array<string,array<string,string>>>
     */
    public function getApiFormatedCarlRelationshipsPresetForInterventionCreation(): array
    {
        return array_map(
            function ($carlObject) {
                return $carlObject->toApiRelationship();
            },
            $this->getCarlRelationshipsPresetForInterventionCreation()
        );
    }

    /**
     * @return array<string,CarlObject>
     */
    public function getCarlRelationshipsPresetForInterventionCreation(): array
    {
        // la désérialisation json ne va pas construire les objects, on doit le faire
        return array_map(
            function ($rawRelationshipPresetArray) {
                return new CarlObject($rawRelationshipPresetArray['id'], $rawRelationshipPresetArray['type']);
            },
            $this->carlRelationshipsPresetForInterventionCreation
        );
    }

    /**
     * @param array<string,CarlObject> $carlRelationshipsPresetForInterventionCreation
     */
    public function setCarlRelationshipsPresetForInterventionCreation(
        array $carlRelationshipsPresetForInterventionCreation
    ): self {
        $this->carlRelationshipsPresetForInterventionCreation =
            $carlRelationshipsPresetForInterventionCreation;

        return $this;
    }

    /**
     * @return array<string,string>
     */
    // #[Context([Serializer::EMPTY_ARRAY_AS_OBJECT => true])]
    public function getCarlAttributesPresetForDemandeInterventionCreation(): array
    {
        return $this->carlAttributesPresetForDemandeInterventionCreation;
    }

    /**
     * @param array<string,string> $carlAttributesPresetForDemandeInterventionCreation
     */
    public function setCarlAttributesPresetForDemandeInterventionCreation(
        array $carlAttributesPresetForDemandeInterventionCreation
    ): self {
        $this->carlAttributesPresetForDemandeInterventionCreation = $carlAttributesPresetForDemandeInterventionCreation;

        return $this;
    }

    /**
     * @return array<string,array<string,array<string,string>>>
     */
    public function getApiFormatedCarlRelationshipsPresetForDemandeInterventionCreation(): array
    {
        return array_map(
            function ($carlObject) {
                return $carlObject->toApiRelationship();
            },
            $this->getCarlRelationshipsPresetForDemandeInterventionCreation()
        );
    }

    /**
     * @return array<string,CarlObject>
     */
    public function getCarlRelationshipsPresetForDemandeInterventionCreation(): array
    {
        // la désérialisation json ne va pas construire les objects, on doit le faire
        return array_map(
            function ($rawRelationshipPresetArray) {
                return new CarlObject($rawRelationshipPresetArray['id'], $rawRelationshipPresetArray['type']);
            },
            $this->carlRelationshipsPresetForDemandeInterventionCreation
        );
    }

    /**
     * @param array<string,CarlObject> $carlRelationshipsPresetForDemandeInterventionCreation
     */
    public function setCarlRelationshipsPresetForDemandeInterventionCreation(
        array $carlRelationshipsPresetForDemandeInterventionCreation
    ): self {
        $this->carlRelationshipsPresetForDemandeInterventionCreation =
            $carlRelationshipsPresetForDemandeInterventionCreation;

        return $this;
    }

    public function getPhotoAvantInterventionDoctypeId(): string
    {
        return $this->photoAvantInterventionDoctypeId;
    }

    public function setPhotoAvantInterventionDoctypeId(string $photoAvantInterventionDoctypeId): self
    {
        $this->photoAvantInterventionDoctypeId = $photoAvantInterventionDoctypeId;

        return $this;
    }

    public function getPhotoApresInterventionDoctypeId(): ?string
    {
        return $this->photoApresInterventionDoctypeId;
    }

    public function setPhotoApresInterventionDoctypeId(string $photoApresInterventionDoctypeId): self
    {
        $this->photoApresInterventionDoctypeId = $photoApresInterventionDoctypeId;

        return $this;
    }

    public function getCarlClient(): ?CarlClient
    {
        return $this->carlClient;
    }

    public function setCarlClient(?CarlClient $carlClient): self
    {
        $this->carlClient = $carlClient;

        return $this;
    }

    public function getWoViewUrlSubPath(): ?string
    {
        return $this->woViewUrlSubPath;
    }

    public function setWoViewUrlSubPath(?string $woViewUrlSubPath): static
    {
        $this->woViewUrlSubPath = $woViewUrlSubPath;

        return $this;
    }

    public function getMrViewUrlSubPath(): ?string
    {
        return $this->mrViewUrlSubPath;
    }

    public function setMrViewUrlSubPath(?string $mrViewUrlSubPath): static
    {
        $this->mrViewUrlSubPath = $mrViewUrlSubPath;

        return $this;
    }

    public function getBoxViewUrlSubPath(): ?string
    {
        return $this->boxViewUrlSubPath;
    }

    public function setBoxViewUrlSubPath(?string $boxViewUrlSubPath): static
    {
        $this->boxViewUrlSubPath = $boxViewUrlSubPath;

        return $this;
    }

    public function getMaterialViewUrlSubPath(): ?string
    {
        return $this->materialViewUrlSubPath;
    }

    public function setMaterialViewUrlSubPath(?string $materialViewUrlSubPath): static
    {
        $this->materialViewUrlSubPath = $materialViewUrlSubPath;

        return $this;
    }

    public function getEnvironnement(): ?string
    {
        return $this->getCarlClient()?->getTitle();
    }

    public function getInterventionViewUrl(Intervention $intervention): ?string
    {
        $woViewUrlSubPath = $this->getWoViewUrlSubPath() ?? $this->carlClient->getWoViewUrlSubPath();

        if (
            is_null($this->carlClient->getBaseUrl())
            || is_null($woViewUrlSubPath)
            || is_null($intervention->getId())
        ) {
            return null;
        }

        return $this->carlClient->getBaseUrl()
            .$woViewUrlSubPath
            .'&BEAN_ID='
            .$intervention->getId();
    }

    public function getDemandeInterventionViewUrl(DemandeIntervention $demandeIntervention): ?string
    {
        $mrViewUrlSubPath = $this->getMrViewUrlSubPath() ?? $this->carlClient->getMrViewUrlSubPath();

        if (
            is_null($this->carlClient->getBaseUrl())
            || is_null($mrViewUrlSubPath)
            || is_null($demandeIntervention->getId())
        ) {
            return null;
        }

        return $this->carlClient->getBaseUrl()
            .$mrViewUrlSubPath
            .'&BEAN_ID='
            .$demandeIntervention->getId();
    }

    public function getEquipementViewUrl(Equipement $equipement): ?string
    {
        if (
            is_null($this->carlClient->getBaseUrl())
            || is_null($equipement->getId())
        ) {
            return null;
        }

        $viewUrlSubPath = null;

        // if ('material' === 'todo: récupérer le vrai type') {
        //     $viewUrlSubPath = $this->getMaterialViewUrlSubPath() ?? $this->carlClient->getMaterialViewUrlSubPath();
        // } else {
        $viewUrlSubPath = $this->getBoxViewUrlSubPath() ?? $this->carlClient->getBoxViewUrlSubPath();
        // }

        if (is_null($viewUrlSubPath)) {
            return null;
        }

        return $this->carlClient->getBaseUrl()
            .$viewUrlSubPath
            .'&BEAN_ID='
            .$equipement->getId();
    }

    /** @return array<string>|null */
    public function getIncludeEquipementsChildrenWithType(): ?array
    {
        return array_values($this->includeEquipementsChildrenWithType);
    }

    /**
     * @param array<string>|null $includedChildrenTypes
     */
    public function setIncludeEquipementsChildrenWithType(?array $includedChildrenTypes): self
    {
        $this->includeEquipementsChildrenWithType = array_values($includedChildrenTypes);

        return $this;
    }
}
