<?php

namespace App\InterventionServiceOffer\Gmao\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention\DemandeIntervention;
use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Gmao\Repository\GmaoConfigurationInterventionRepository;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\Shared\Document\Entity\DocumentBlob;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Serializer;

#[Get(
    security: "is_granted('ROLE_CONSULTER_GMAO_CONFIGURATION_INTERVENTION')"
)]
#[GetCollection(
    security: "is_granted('ROLE_CONSULTER_GMAO_CONFIGURATION_INTERVENTION')",
    order: ['title' => 'ASC']
)]
#[ORM\Entity(repositoryClass: GmaoConfigurationInterventionRepository::class)]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap([
    'carl' => CarlConfigurationIntervention::class,
])]
abstract class GmaoConfigurationIntervention
{
    public const GROUP_AFFICHER_GMAO_CONFIGURATION_INTERVENTION = "Afficher la configuration GMAO d'une offre de service intervention";
    public const GROUP_CREER_GMAO_CONFIGURATION_INTERVENTION = "Créer la configuration GMAO d'une offre de service intervention";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_LISTE_ODS, GmaoConfigurationIntervention::GROUP_AFFICHER_GMAO_CONFIGURATION_INTERVENTION])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_LISTE_ODS, GmaoConfigurationIntervention::GROUP_AFFICHER_GMAO_CONFIGURATION_INTERVENTION, GmaoConfigurationIntervention::GROUP_CREER_GMAO_CONFIGURATION_INTERVENTION])]
    protected ?string $title = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_LISTE_ODS, GmaoConfigurationIntervention::GROUP_AFFICHER_GMAO_CONFIGURATION_INTERVENTION, GmaoConfigurationIntervention::GROUP_CREER_GMAO_CONFIGURATION_INTERVENTION])]
    protected ?string $notes = null;

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(string $notes): static
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @return array<string,string> un tableau associatif du type "id de la nature"=>"libellé de la nature"
     */
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_LISTE_ODS])]
    #[Context(normalizationContext: [Serializer::EMPTY_ARRAY_AS_OBJECT => true])] // ne fonctionne pas
    abstract public function getActionTypesMap(): array;

    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_LISTE_ODS])]
    abstract public function getEnvironnement(): ?string;

    /**
     * Si la Gmao employé possède une interface pour visualiser les interventions, il faut surcharger cette méthode dans sont client spécifique.
     */
    abstract public function getInterventionViewUrl(Intervention $intervention): ?string;

    /**
     * Lien (externe) vers la page de la demande sur la gmao.
     * Si la Gmao employé possède une interface pour visualiser les demandes d'interventions, il faut surcharger cette méthode dans sont client spécifique.
     */
    abstract public function getDemandeInterventionViewUrl(DemandeIntervention $demandeIntervention): ?string;

    /**
     * Si la Gmao employé possède une interface pour visualiser les équipements, il faut surcharger cette méthode dans sont client spécifique.
     */
    abstract public function getEquipementViewUrl(Equipement $equipement): ?string;

    abstract public function getDocumentBlob(string $documentId): DocumentBlob;
}
