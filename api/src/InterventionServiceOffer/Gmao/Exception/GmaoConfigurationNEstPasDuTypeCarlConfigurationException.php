<?php

namespace App\InterventionServiceOffer\Gmao\Exception;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\Shared\Exception\RuntimeException;

class GmaoConfigurationNEstPasDuTypeCarlConfigurationException extends RuntimeException
{
    public function __construct(string $typeDeGmaoConfiguration, ?\Throwable $previous = null)
    {
        parent::__construct(message: "La configuration GMAO intervention n'est pas du type attendu ".CarlConfigurationIntervention::class.". Type reçu : $typeDeGmaoConfiguration", previous: $previous);
    }
}
