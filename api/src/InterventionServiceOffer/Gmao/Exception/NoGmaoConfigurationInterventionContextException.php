<?php

namespace App\InterventionServiceOffer\Gmao\Exception;

use App\Shared\Exception\RuntimeException;

class NoGmaoConfigurationInterventionContextException extends RuntimeException
{
    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct(message: 'Aucun contexte de configuration GMAO intervention', previous: $previous);
    }
}
