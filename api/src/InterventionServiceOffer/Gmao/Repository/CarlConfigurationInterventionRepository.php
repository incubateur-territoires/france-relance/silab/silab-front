<?php

namespace App\InterventionServiceOffer\Gmao\Repository;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<\App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention>
 *
 * @method CarlConfigurationIntervention|null find($id, $lockMode = null, $lockVersion = null)
 * @method CarlConfigurationIntervention|null findOneBy(array $criteria, array $orderBy = null)
 * @method CarlConfigurationIntervention[]    findAll()
 * @method CarlConfigurationIntervention[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarlConfigurationInterventionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CarlConfigurationIntervention::class);
    }

    public function save(CarlConfigurationIntervention $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CarlConfigurationIntervention $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    //    /**
    //     * @return CarlConfigurationIntervention[] Returns an array of CarlConfigurationIntervention objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?CarlConfigurationIntervention
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
