<?php

namespace App\InterventionServiceOffer\Gmao\Repository;

use App\InterventionServiceOffer\Gmao\Entity\GmaoConfigurationIntervention;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<GmaoConfigurationIntervention>
 *
 * @method GmaoConfigurationIntervention|null find($id, $lockMode = null, $lockVersion = null)
 * @method GmaoConfigurationIntervention|null findOneBy(array $criteria, array $orderBy = null)
 * @method GmaoConfigurationIntervention[]    findAll()
 * @method GmaoConfigurationIntervention[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GmaoConfigurationInterventionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GmaoConfigurationIntervention::class);
    }

    public function save(GmaoConfigurationIntervention $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(GmaoConfigurationIntervention $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    //    /**
    //     * @return GmaoConfigurationIntervention[] Returns an array of GmaoConfigurationIntervention objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('g')
    //            ->andWhere('g.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('g.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?GmaoConfigurationIntervention
    //    {
    //        return $this->createQueryBuilder('g')
    //            ->andWhere('g.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
