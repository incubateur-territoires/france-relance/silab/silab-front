<?php

namespace App\InterventionServiceOffer\Gmao\Service;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Gmao\Exception\GmaoConfigurationNEstPasDuTypeCarlConfigurationException;

class CarlConfigurationInterventionContext
{
    public function __construct(
        private GmaoConfigurationInterventionContext $gmaoConfigurationInterventionContext
    ) {
    }

    public function getCarlConfigurationId(): int
    {
        return $this->getCarlConfiguration()->getId();
    }

    public function getCarlConfiguration(): CarlConfigurationIntervention
    {
        $gmaoConfiguration = $this->gmaoConfigurationInterventionContext->getGmaoConfiguration();

        if (!$gmaoConfiguration instanceof CarlConfigurationIntervention) {
            throw new GmaoConfigurationNEstPasDuTypeCarlConfigurationException(get_class($gmaoConfiguration));
        }

        return $gmaoConfiguration;
    }
}
