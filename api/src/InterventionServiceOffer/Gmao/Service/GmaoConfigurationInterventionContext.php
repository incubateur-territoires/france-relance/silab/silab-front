<?php

namespace App\InterventionServiceOffer\Gmao\Service;

use App\InterventionServiceOffer\Carl\Exception\NoInterventionServiceOfferContextException;
use App\InterventionServiceOffer\Gmao\Entity\GmaoConfigurationIntervention;
use App\InterventionServiceOffer\Gmao\Exception\NoGmaoConfigurationInterventionContextException;
use App\InterventionServiceOffer\Gmao\Repository\GmaoConfigurationInterventionRepository;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use Symfony\Component\HttpFoundation\RequestStack;

class GmaoConfigurationInterventionContext
{
    private ?GmaoConfigurationIntervention $gmaoConfiguration = null;
    private bool $hasCachedGmaoConfiguration = false;

    public function __construct(
        private GmaoConfigurationInterventionRepository $gmaoConfigurationInterventionRepository,
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private RequestStack $requestStack
    ) {
    }

    public function getGmaoConfigurationId(): int
    {
        if ($this->hasCachedGmaoConfiguration && !is_null($this->gmaoConfiguration)) {
            return $this->gmaoConfiguration->getId();
        }

        $gmaoConfigurationInterventionId = $this->requestStack->getCurrentRequest()?->get('gmaoConfigurationInterventionId');

        // Si on ne nous à pas fourni directement un id de gmaoConfigurationIntervention,
        // on essaie de le déduire de l'offre de service (c'est le cas la plupart du temps)
        if (is_null($gmaoConfigurationInterventionId)) {
            try {
                $gmaoConfigurationInterventionId = $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()->getId();
            } catch (\Throwable $e) {
                throw new NoGmaoConfigurationInterventionContextException($e);
            }
        }

        return $gmaoConfigurationInterventionId;
    }

    public function getGmaoConfiguration(): GmaoConfigurationIntervention
    {
        if ($this->hasCachedGmaoConfiguration) {
            return $this->gmaoConfiguration;
        }

        $this->gmaoConfiguration = $this->gmaoConfigurationInterventionRepository->find(
            $this->getGmaoConfigurationId()
        );

        if (is_null($this->gmaoConfiguration)) {
            throw new NoInterventionServiceOfferContextException();
        }

        $this->hasCachedGmaoConfiguration = true;

        return $this->gmaoConfiguration;
    }
}
