<?php

namespace App\InterventionServiceOffer\GmaoActeur\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\InterventionServiceOffer\GmaoActeur\State\GetCollectionEquipierProvider;
use App\InterventionServiceOffer\GmaoActeur\State\GetCollectionGmaoActeurProvider;
use App\InterventionServiceOffer\GmaoActeur\State\GetMyselfProvider;
use App\Shared\Gmao\Entity\GmaoActeur;

#[GetCollection(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/myself/equipiers',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE')",
    output: GmaoActeur::class,
    provider: GetCollectionEquipierProvider::class,
)]
#[GetCollection(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/gmao-acteurs',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_SERVICEOFFER_CONSULTER_GMAO_ACTEUR')",
    output: GmaoActeur::class,
    provider: GetCollectionGmaoActeurProvider::class,
    filters: ['nomComplet', 'limit', 'statusCode']
)]
#[Get(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/myself',
    uriVariables: ['serviceOfferId' => 'serviceOfferId'],
    security: "is_granted('SERVICEOFFER_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE')",
    output: GmaoActeur::class,
    provider: GetMyselfProvider::class,
    filters: ['include']
)]
class GmaoActeurApiRessource
{
}
