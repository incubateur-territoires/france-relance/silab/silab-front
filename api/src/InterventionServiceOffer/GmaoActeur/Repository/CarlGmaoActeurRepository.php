<?php

namespace App\InterventionServiceOffer\GmaoActeur\Repository;

use App\InterventionServiceOffer\Carl\Entity\DbCarlGmaoActeur;
use App\InterventionServiceOffer\Carl\Service\CarlClientContext;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlGmaoActeurRepository;
use App\Shared\Carl\Service\CurrentCarlUserContext;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Exception\RuntimeException;
use App\Shared\Gmao\Entity\GmaoActeur;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlGmaoActeurRepository implements GmaoActeurRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;

    public function __construct(
        private DbCarlGmaoActeurRepository $dbCarlGmaoActeurRepository,
        private CurrentCarlUserContext $currentCarlUserContext,
        protected CarlClientContext $carlClientContext,
    ) {
    }

    public function getTeammates(): array
    {
        $currentDbCarlGmaoActeur = $this->currentCarlUserContext->getCurrentDbCarlGmaoActeur();

        if (is_null($currentDbCarlGmaoActeur->getTechnicianTeamId())) {
            return [];
        }

        return array_map(
            function (DbCarlGmaoActeur $dbCarlGmaoActeur) {
                return $dbCarlGmaoActeur->toGmaoActeur();
            },
            $this->dbCarlGmaoActeurRepository->matching(
                (new Criteria())
                // Note: dans un contexte d'équipiers (et donc d'équipe, donc technician, on retourne uniquement les intervenants actifs, d'où le test sur technicianStatusCode)
                 ->where(Criteria::expr()->in('technicianStatusCode', self::getCompatibleCarlTechnicianStatusCodesFromSilabStatusCodes(['active'])))
                 ->andWhere(Criteria::expr()->eq('technicianTeamId', $currentDbCarlGmaoActeur->getTechnicianTeamId()))
                 ->andWhere(Criteria::expr()->neq('id', $currentDbCarlGmaoActeur->getId()))
            )->toArray()
        );
    }

    /**
     * @param array<string> $criteria
     */
    public function getMyself(array $criteria = []): GmaoActeur
    {
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'include',
            ]
        );
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $include = $propertyAccessor->getValue($criteria, '[include]') ?? [];
        $this->throwIfUnexpectedValue(
            $include,
            [
                'operationsHabilitees',
            ]
        );

        return $this->currentCarlUserContext->getCurrentGmaoActeur($include);
    }

    public function find($id): GmaoActeur
    {
        $sampleGmaoActeurArray = $this->findBy(['id' => [$id]]);

        if (count($sampleGmaoActeurArray) > 1) {
            throw new RuntimeException("Plus d'un acteur trouvé pour l'id : $id");
        }

        if (0 === count($sampleGmaoActeurArray)) {
            throw new RuntimeException("Aucune acteur trouvé pour l'id : $id");
        }

        return array_pop($sampleGmaoActeurArray);
    }

    /**
     * @return array<GmaoActeur>
     */
    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    /**
     * @return array<GmaoActeur>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }
        if (null !== $orderBy) {
            throw new RuntimeException('Le paramètre orderBy n\'est pas encore implémenté');
        }

        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
                'nomComplet',
                'currentStatus.code',
            ]
        );

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $queryBuilder = $this->dbCarlGmaoActeurRepository->createQueryBuilder('acteur');

        $id = $propertyAccessor->getValue($criteria, '[id]');
        if (!empty($id)) {
            $queryBuilder->andWhere('acteur.id = :id ')->setParameter('id', $id);
        }

        $nomComplet = $propertyAccessor->getValue($criteria, '[nomComplet]');
        if (!empty($nomComplet)) {
            $nomComplet = str_replace('%', '\\%', strtolower($nomComplet));
            $nomComplet = str_replace('*', '%', $nomComplet);
            $queryBuilder->andWhere('LOWER(acteur.nomComplet) LIKE :nomComplet')->setParameter('nomComplet', $nomComplet);
        }

        $silabStatusCodes = $propertyAccessor->getValue($criteria, '[currentStatus.code]');
        if (!empty($silabStatusCodes)) {
            $carlStatusCodes = self::getCompatibleCarlActorStatusCodesFromSilabStatusCodes($silabStatusCodes);
            $queryBuilder->andWhere('acteur.statusCode IN (:statusCodes) ')->setParameter('statusCodes', $carlStatusCodes);
        }

        $queryBuilder->setMaxResults($limit);
        $fetchedDbCarlGmaoActeurs = $queryBuilder->getQuery()->getResult();

        return array_map(
            function (DbCarlGmaoActeur $gmaoActeur) {
                return $gmaoActeur->toGmaoActeur();
            },
            $fetchedDbCarlGmaoActeurs);
    }

    public function findOneBy(array $criteria): GmaoActeur
    {
        $acteurs = $this->findBy($criteria);
        if (empty($acteurs)) {
            throw new RuntimeException('Aucun GmaoActeur trouvé avec les critères donnés');
        }

        return $acteurs[0];
    }

    public function getClassName(): string
    {
        return GmaoActeur::class;
    }

    /**
     * @param array<string> $silabStatusCodes
     *
     * @return array<string>
     */
    private static function getCompatibleCarlActorStatusCodesFromSilabStatusCodes(array $silabStatusCodes): array
    {
        return array_reduce(
            $silabStatusCodes,
            function ($carlStatusCodesAccumulator, $silabStatusCode) {
                $compatibleCarlStatusCodes = DbCarlGmaoActeur::SILAB_GMAOACTEUR_STATUS_CODE_TO_COMPATIBLE_CARL_ACTOR_STATUS_CODE_MAPPING[$silabStatusCode];

                return array_unique(array_merge($carlStatusCodesAccumulator, $compatibleCarlStatusCodes));
            },
            []
        );
    }

    /**
     * @param array<string> $silabStatusCodes
     *
     * @return array<string>
     */
    private static function getCompatibleCarlTechnicianStatusCodesFromSilabStatusCodes(array $silabStatusCodes): array
    {
        return array_reduce(
            $silabStatusCodes,
            function ($carlStatusCodesAccumulator, $silabStatusCode) {
                $compatibleCarlStatusCodes = DbCarlGmaoActeur::SILAB_GMAOACTEUR_STATUS_CODE_TO_COMPATIBLE_CARL_TECHNICIAN_STATUS_CODE_MAPPING[$silabStatusCode];

                return array_unique(array_merge($carlStatusCodesAccumulator, $compatibleCarlStatusCodes));
            },
            []
        );
    }
}
