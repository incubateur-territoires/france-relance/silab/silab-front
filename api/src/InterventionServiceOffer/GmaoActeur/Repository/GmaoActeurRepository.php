<?php

namespace App\InterventionServiceOffer\GmaoActeur\Repository;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Exception\RuntimeException;
use App\Shared\Gmao\Entity\GmaoActeur;

class GmaoActeurRepository implements GmaoActeurRepositoryInterface
{
    private GmaoActeurRepositoryInterface $specificGmaoActeurRepository;

    public function __construct(
        InterventionServiceOfferContext $interventionServiceOfferContext,
        CarlGmaoActeurRepository $carlGmaoActeurRepository
    ) {
        try {
            $this->specificGmaoActeurRepository = match ($interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class) {
                CarlConfigurationIntervention::class => $carlGmaoActeurRepository
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class);
        }
    }

    public function getTeammates(): array
    {
        return $this->specificGmaoActeurRepository->getTeammates();
    }

    /**
     * @param array<string> $criteria
     */
    public function getMyself(array $criteria = []): GmaoActeur
    {
        return $this->specificGmaoActeurRepository->getMyself($criteria);
    }

    public function find($id): GmaoActeur
    {
        return $this->specificGmaoActeurRepository->find($id);
    }

    public function findAll(): array
    {
        return $this->specificGmaoActeurRepository->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->specificGmaoActeurRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): GmaoActeur
    {
        return $this->specificGmaoActeurRepository->findOneBy($criteria);
    }

    public function getClassName()
    {
        return GmaoActeur::class;
    }
}
