<?php

namespace App\InterventionServiceOffer\GmaoActeur\Repository;

use App\Shared\Gmao\Entity\GmaoActeur;
use Doctrine\Persistence\ObjectRepository;

interface GmaoActeurRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): GmaoActeur;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<GmaoActeur>
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array;

    /**
     * @return array<GmaoActeur>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): GmaoActeur;

    /**
     * Undocumented function.
     *
     * @param array<string,mixed> $criteria
     */
    public function getMyself(array $criteria = []): GmaoActeur;

    /**
     * @return array<GmaoActeur> Note : retourne uniquement les équipiers avec le statut "active"
     */
    public function getTeammates(): array;
}
