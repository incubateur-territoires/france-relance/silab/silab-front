<?php

namespace App\InterventionServiceOffer\GmaoActeur\State;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\GmaoActeur\Repository\GmaoActeurRepository;
use App\Shared\Gmao\Entity\GmaoActeur;

final class GetCollectionEquipierProvider implements ProviderInterface
{
    public function __construct(
        private GmaoActeurRepository $gmaoActeurRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<GmaoActeur>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof GetCollection);

        return $this->gmaoActeurRepository->getTeammates();
    }
}
