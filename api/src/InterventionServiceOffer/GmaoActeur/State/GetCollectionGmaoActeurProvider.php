<?php

namespace App\InterventionServiceOffer\GmaoActeur\State;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\GmaoActeur\Repository\GmaoActeurRepository;
use App\Shared\Gmao\Entity\GmaoActeur;
use Symfony\Component\PropertyAccess\PropertyAccess;

final class GetCollectionGmaoActeurProvider implements ProviderInterface
{
    public function __construct(
        private GmaoActeurRepository $gmaoActeurRepository,
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<GmaoActeur>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof GetCollection);
        $criteria = $context['filters'] ?? [];
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $limit = null;
        if ($propertyAccessor->getValue($criteria, '[limit]')) {
            $limit = $criteria['limit'];
            unset($criteria['limit']);
        }

        // on s'assure d'avoir les status sous forme de tableau
        if (isset($criteria['currentStatus.code']) && is_string($criteria['currentStatus.code'])) {
            $criteria['currentStatus.code'] = [$criteria['currentStatus.code']];
        }

        return $this->gmaoActeurRepository->findBy(criteria: $criteria, limit: $limit);
    }
}
