<?php

namespace App\InterventionServiceOffer\GmaoActeur\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\GmaoActeur\Repository\GmaoActeurRepository;
use App\Shared\Gmao\Entity\GmaoActeur;

final class GetMyselfProvider implements ProviderInterface
{
    public function __construct(
        private GmaoActeurRepository $gmaoActeurRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): GmaoActeur
    {
        assert($operation instanceof Get);

        $criteria = $context['filters'] ?? [];

        foreach ($criteria as $key => $value) {
            if (is_string($value)) {
                $criteria[$key] = [$value];
            }
        }

        return $this->gmaoActeurRepository->getMyself($criteria);
    }
}
