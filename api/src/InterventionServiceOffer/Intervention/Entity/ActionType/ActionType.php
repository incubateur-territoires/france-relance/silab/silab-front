<?php

namespace App\InterventionServiceOffer\Intervention\Entity\ActionType;

class ActionType
{
    public function __construct(
        public string $id,
        public string $label)
    {
    }
}
