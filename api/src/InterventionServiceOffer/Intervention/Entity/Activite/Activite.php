<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Activite;

use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\Shared\Gmao\Entity\GmaoActeur;

abstract class Activite
{
    public function __construct(
        private string $id,
        private ?GmaoActeur $createdBy,
        private ?string $description,
        private ?Operation $operation,
        private ?Equipement $equipementCible,
        private bool $isPlanifie,
        private bool $isObligatoire,
        private ?\DateTimeImmutable $dateDeDebut,
        private ?GmaoActeur $intervenant,
        private ?int $duree,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCreatedBy(): ?GmaoActeur
    {
        return $this->createdBy;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getOperation(): ?Operation
    {
        return $this->operation;
    }

    public function getEquipementCible(): ?Equipement
    {
        return $this->equipementCible;
    }

    public function getIsPlanifie(): bool
    {
        return $this->isPlanifie;
    }

    public function getIsObligatoire(): bool
    {
        return $this->isObligatoire;
    }

    public function getIntervenant(): ?GmaoActeur
    {
        return $this->intervenant;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function getDateDeDebut(): ?\DateTimeImmutable
    {
        return $this->dateDeDebut;
    }

    public function setDateDeDebut(?\DateTimeImmutable $dateDeDebut): static
    {
        $this->dateDeDebut = $dateDeDebut;

        return $this;
    }

    public function setIntervenant(?GmaoActeur $intervenant): static
    {
        $this->intervenant = $intervenant;

        return $this;
    }

    // Cette méthode est utilisé ici et elle est envoyé par default par api platform parcequ'elle est préfixée par "is.
    // En revanche on se fiche de la récupérer car s'est une info redondante (elle est recalculée sur le front)
    // Amelioration: désactiver ça (conf globale api-platform pour garder que les get ?).
    public function isDone(): bool
    {
        return !is_null($this->getDateDeDebut());
    }
}
