<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Activite;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\InterventionServiceOffer\Intervention\State\CreateActiviteActionProcessor;
use App\InterventionServiceOffer\Intervention\State\GetCollectionActiviteActionProvider;
use App\InterventionServiceOffer\Intervention\State\UpdateActiviteActionProcessor;

#[GetCollection(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions/{interventionId}/activites-action',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_ACTIVITE')",
    output: ActiviteAction::class,
    provider: GetCollectionActiviteActionProvider::class,
)]
#[Put(
    read: false,
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/activites-action/{id}',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_MODIFIER_ACTIVITE')",
    output: ActiviteAction::class,
    input: UpdateActiviteAction::class,
    processor: UpdateActiviteActionProcessor::class,
)]
#[Post(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions/{interventionId}/activites-action',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'interventionId' => 'interventionId',
    ],
    input: CreateActiviteAction::class,
    output: ActiviteAction::class,
    processor: CreateActiviteActionProcessor::class,

    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_DECLARER_ACTIVITE')",
)]
#[Get()]
class ActiviteActionApiRessource
{
}
