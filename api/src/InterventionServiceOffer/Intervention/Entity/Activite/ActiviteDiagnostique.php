<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Activite;

use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\Shared\Gmao\Entity\GmaoActeur;

class ActiviteDiagnostique extends Activite
{
    public function __construct(
        string $id,
        GmaoActeur $createdBy,
        string $description,
        Operation $operation,
        ?Equipement $equipementCible,
        bool $isPlanifie,
        bool $isObligatoire,
        ?\DateTimeImmutable $dateDeDebut,
        ?GmaoActeur $intervenant,
        ?int $duree,
        private ?Evaluation $evaluation,
        private ?string $interventionIdGeneree,
        private ?string $demandeInterventionIdGeneree,
    ) {
        parent::__construct(
            id: $id,
            createdBy: $createdBy,
            description: $description,
            operation: $operation,
            equipementCible: $equipementCible,
            isPlanifie: $isPlanifie,
            isObligatoire: $isObligatoire,
            dateDeDebut: $dateDeDebut,
            intervenant: $intervenant,
            duree: $duree
        );
    }

    public function getEvaluation(): ?Evaluation
    {
        return $this->evaluation;
    }

    public function getInterventionIdGeneree(): ?string
    {
        return $this->interventionIdGeneree;
    }

    public function getDemandeInterventionIdGeneree(): ?string
    {
        return $this->demandeInterventionIdGeneree;
    }
}
