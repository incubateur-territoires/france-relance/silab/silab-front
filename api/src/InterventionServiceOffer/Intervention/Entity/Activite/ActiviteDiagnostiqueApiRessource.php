<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Activite;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Put;
use App\InterventionServiceOffer\Intervention\State\GetCollectionActiviteDiagnostiqueProvider;
use App\InterventionServiceOffer\Intervention\State\UpdateActiviteDiagnostiqueProcessor;

#[GetCollection(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions/{interventionId}/activites-diagnostique',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_ACTIVITE')",
    output: ActiviteDiagnostique::class,
    provider: GetCollectionActiviteDiagnostiqueProvider::class,
)]
#[Put(
    read: false,
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/activites-diagnostique/{id}',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_MODIFIER_ACTIVITE')",
    output: ActiviteDiagnostique::class,
    input: UpdateActiviteDiagnostique::class,
    processor: UpdateActiviteDiagnostiqueProcessor::class,
)]
#[Get()]
class ActiviteDiagnostiqueApiRessource
{
}
