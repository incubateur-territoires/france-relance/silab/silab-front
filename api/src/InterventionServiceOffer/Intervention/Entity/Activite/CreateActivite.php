<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Activite;

class CreateActivite
{
    /**
     * @param int $duree en minute
     */
    public function __construct(
        private ?string $intervenantId,
        private ?int $duree,
        private ?\DateTimeImmutable $dateDeDebut,
        private ?string $operationId,
    ) {
    }

    public function getIntervenantId(): ?string
    {
        return $this->intervenantId;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function getDateDeDebut(): ?\DateTimeImmutable
    {
        return $this->dateDeDebut;
    }

    public function setDateDeDebut(?\DateTimeImmutable $dateDeDebut): static
    {
        $this->dateDeDebut = $dateDeDebut;

        return $this;
    }

    public function getOperationId(): ?string
    {
        return $this->operationId;
    }
}
