<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Activite;

enum Evaluation: string
{
    case Conforme = 'conforme';
    case Reserve = 'reserve';
    case NonConforme = 'non conforme';
}
