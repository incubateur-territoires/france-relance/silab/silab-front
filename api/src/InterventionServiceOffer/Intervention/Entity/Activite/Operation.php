<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Activite;

class Operation
{
    public function __construct(
        public string $id,
        public string $label
    ) {
    }
}
