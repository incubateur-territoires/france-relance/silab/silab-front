<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Activite;

abstract class UpdateActivite
{
    public function __construct(
        private ?\DateTimeImmutable $dateDeDebut,
    ) {
    }

    public function setDateDeDebut(?\DateTimeImmutable $dateDeDebut): self
    {
        $this->dateDeDebut = $dateDeDebut;

        return $this;
    }

    public function getDateDeDebut(): ?\DateTimeImmutable
    {
        return $this->dateDeDebut;
    }
}
