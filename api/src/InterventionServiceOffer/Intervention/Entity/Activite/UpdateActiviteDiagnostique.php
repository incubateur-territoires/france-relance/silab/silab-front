<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Activite;

class UpdateActiviteDiagnostique extends UpdateActivite
{
    public function __construct(
        ?\DateTimeImmutable $dateDeDebut,
        private ?Evaluation $evaluation,
        private ?string $interventionIdGeneree,
        private ?string $demandeInterventionIdGeneree
    ) {
        parent::__construct(dateDeDebut: $dateDeDebut);
    }

    public function setEvaluation(?Evaluation $evaluation): self
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    public function getEvaluation(): ?Evaluation
    {
        return $this->evaluation;
    }

    public function getInterventionIdGeneree(): ?string
    {
        return $this->interventionIdGeneree;
    }

    public function getDemandeInterventionIdGeneree(): ?string
    {
        return $this->demandeInterventionIdGeneree;
    }
}
