<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Intervention;

use App\InterventionServiceOffer\Intervention\Entity\ActionType\ActionType;

trait AttributsInterventionCommunsTrait
{
    use AttributsInterventionNecessairesAuSoldageTrait {
        __construct as __attributsNecessairesAuSoldageConstruct;
    }

    private InterventionPriority $priority;

    public function __construct(
        ?string $materielConsomme,
        \DateTimeImmutable $dateDeDebut,
        \DateTimeImmutable $dateDeFin,
        string $priority,
        private string $title,
        private ?string $description,
        private ?ActionType $actionType,
        private ?float $latitude,
        private ?float $longitude,
    ) {
        $this->__attributsNecessairesAuSoldageConstruct(
            materielConsomme: $materielConsomme,
            dateDeDebut: $dateDeDebut,
            dateDeFin: $dateDeFin
        );
        $this->priority = InterventionPriority::from($priority);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setPriority(string $priority): self
    {
        $this->priority = InterventionPriority::from($priority);

        return $this;
    }

    public function getPriority(): string
    {
        return $this->priority->value;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getActionType(): ?ActionType
    {
        return $this->actionType;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }
}
