<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Intervention;

trait AttributsInterventionNecessairesAuSoldageTrait
{
    public function __construct(
        private ?string $materielConsomme,
        private \DateTimeImmutable $dateDeDebut,
        private \DateTimeImmutable $dateDeFin,
    ) {
    }

    public function getMaterielConsomme(): ?string
    {
        return $this->materielConsomme;
    }

    public function setMaterielConsomme(?string $materielConsomme): static
    {
        $this->materielConsomme = $materielConsomme;

        return $this;
    }

    public function getDateDeDebut(): \DateTimeImmutable
    {
        return $this->dateDeDebut;
    }

    public function setDateDeDebut(\DateTimeImmutable $dateDeDebut): static
    {
        $this->dateDeDebut = $dateDeDebut;

        return $this;
    }

    public function getDateDeFin(): \DateTimeImmutable
    {
        return $this->dateDeFin;
    }

    public function setDateDeFin(\DateTimeImmutable $dateDeFin): static
    {
        $this->dateDeFin = $dateDeFin;

        return $this;
    }
}
