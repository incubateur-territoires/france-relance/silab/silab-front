<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Intervention;

use App\InterventionServiceOffer\Intervention\Entity\ActionType\ActionType;
use App\Shared\ApiPlatform\Entity\IdentifiableRelation;

class CreateIntervention
{
    use AttributsInterventionCommunsTrait {
        __construct as __attributsCommunsConstruct;
    }

    public function __construct(
        string $priority,
        string $title,
        ?string $description,
        ?ActionType $actionType,
        ?float $latitude,
        ?float $longitude,
        ?string $materielConsomme,
        \DateTimeImmutable $dateDeDebut,
        \DateTimeImmutable $dateDeFin,
        private ?IdentifiableRelation $relatedEquipement,
    ) {
        $this->__attributsCommunsConstruct(
            priority: $priority,
            title: $title,
            description: $description,
            actionType: $actionType,
            latitude: $latitude,
            longitude: $longitude,
            materielConsomme: $materielConsomme,
            dateDeDebut: $dateDeDebut,
            dateDeFin: $dateDeFin
        );
    }

    public function getRelatedEquipement(): ?IdentifiableRelation
    {
        return $this->relatedEquipement;
    }
}
