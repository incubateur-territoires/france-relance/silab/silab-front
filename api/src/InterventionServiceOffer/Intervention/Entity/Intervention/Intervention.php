<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Intervention;

use App\InterventionServiceOffer\Equipement\Entity\Equipement\Equipement;
use App\InterventionServiceOffer\Intervention\Entity\ActionType\ActionType;
use App\InterventionServiceOffer\Intervention\Entity\Status\InterventionStatus;
use App\Shared\Document\Entity\DocumentInterface;
use App\Shared\Document\Entity\ImageInterface;
use App\Shared\Gmao\Entity\GmaoActeur;

class Intervention
{
    use AttributsInterventionCommunsTrait {
        __construct as __attributsCommunsConstruct;
    }

    public const IMAGE_CATEGORY_IMAGE_AVANT = 'images-avant';
    public const IMAGE_CATEGORY_IMAGE_APRES = 'images-apres';

    public function __construct(
        string $priority,
        string $title,
        ?string $description,
        ?ActionType $actionType,
        ?float $latitude,
        ?float $longitude,
        ?string $materielConsomme,
        \DateTimeImmutable $dateDeDebut,
        \DateTimeImmutable $dateDeFin,
        private ?Equipement $relatedEquipement,
        private string $id,
        private \DateTime $createdAt,
        private ?string $address,
        private GmaoActeur $createdBy,
        private string $code,
        /** @var array<ImageInterface> */
        private array $imagesAvant,
        /** @var array<ImageInterface> */
        private array $imagesApres,
        private InterventionStatus $currentStatus,
        private ?string $gmaoObjectViewUrl,
        /** @var array<string> */
        private ?array $demandeInterventionIds,
        /** @var array<DocumentInterface> */
        private array $documentsJoints,
        /** @var array<GmaoActeur> */
        private array $intervenantsAffectes,
    ) {
        $this->__attributsCommunsConstruct(
            priority: $priority,
            title: $title,
            description: $description,
            actionType: $actionType,
            latitude: $latitude,
            longitude: $longitude,
            materielConsomme: $materielConsomme,
            dateDeDebut: $dateDeDebut,
            dateDeFin: $dateDeFin
        );
    }

    public function getRelatedEquipement(): ?Equipement
    {
        return $this->relatedEquipement;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): ?GmaoActeur
    {
        return $this->createdBy;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    /** @return array<ImageInterface> */
    public function getImagesAvant(): array
    {
        return $this->imagesAvant;
    }

    /** @return array<ImageInterface> */
    public function getImagesApres(): array
    {
        return $this->imagesApres;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setGmaoObjectViewUrl(?string $gmaoObjectViewUrl): self
    {
        $this->gmaoObjectViewUrl = $gmaoObjectViewUrl;

        return $this;
    }

    public function getGmaoObjectViewUrl(): ?string
    {
        return $this->gmaoObjectViewUrl;
    }

    /**
     * @return array<string>|null
     */
    public function getDemandeInterventionIds(): ?array
    {
        return $this->demandeInterventionIds;
    }

    /** @return array<DocumentInterface> */
    public function getDocumentsJoints(): array
    {
        return $this->documentsJoints;
    }

    /** @return array<GmaoActeur> */
    public function getIntervenantsAffectes(): array
    {
        return $this->intervenantsAffectes;
    }

    public function getCurrentStatus(): InterventionStatus
    {
        return $this->currentStatus;
    }

    public function hasCoordinates(): bool
    {
        return !is_null($this->getLatitude()) && !is_null($this->getLongitude());
    }
}
