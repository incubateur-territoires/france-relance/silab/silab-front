<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Intervention;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\InterventionServiceOffer\Intervention\State\CreateInterventionProcessor;
use App\InterventionServiceOffer\Intervention\State\GetCollectionInterventionCoordinatesProvider;
use App\InterventionServiceOffer\Intervention\State\GetCollectionInterventionProvider;
use App\InterventionServiceOffer\Intervention\State\GetInterventionProvider;
use App\InterventionServiceOffer\Intervention\State\UpdateAttributsInterventionLiesAuSoldageProcessor;
use App\InterventionServiceOffer\Intervention\State\UpdateInterventionProcessor;

#[Post(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CREER_INTERVENTIONS')",
    input: CreateIntervention::class,
    processor: CreateInterventionProcessor::class,
    read: false,
)]
#[Put(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions/{id}',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_MODIFIER_INTERVENTIONS')",
    input: CreateIntervention::class,
    processor: UpdateInterventionProcessor::class,
    read: false,
)]
#[Put(
    read: false,
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions/{id}/attributs-lies-au-soldage',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_SOLDER_INTERVENTIONS')",
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
        'id' => 'id',
    ],
    input: UpdateInterventionPourSoldage::class,
    processor: UpdateAttributsInterventionLiesAuSoldageProcessor::class,
)]
#[Get(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions/{id}',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_INTERVENTIONS')",
    output: Intervention::class,
    provider: GetInterventionProvider::class,
)]
#[GetCollection(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_INTERVENTIONS')",
    output: Intervention::class,
    provider: GetCollectionInterventionProvider::class,
)]
#[GetCollection(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions-coordinates',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_INTERVENTIONS')",
    output: InterventionCoordinates::class,
    provider: GetCollectionInterventionCoordinatesProvider::class,
)]
class InterventionApiRessource
{
}
