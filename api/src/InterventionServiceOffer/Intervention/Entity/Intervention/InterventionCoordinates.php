<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Intervention;

class InterventionCoordinates
{
    public function __construct(
        private string $id,
        private string $label,
        private float $latitude,
        private float $longitude,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }
}
