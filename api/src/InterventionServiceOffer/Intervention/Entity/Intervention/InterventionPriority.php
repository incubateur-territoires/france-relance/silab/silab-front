<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Intervention;

enum InterventionPriority: string
{
    case Urgent = 'urgent';
    case Important = 'important';
    case Normal = 'normal';
    case Low = 'bas';
}
