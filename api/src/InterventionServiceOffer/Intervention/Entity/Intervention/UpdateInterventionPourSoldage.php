<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Intervention;

class UpdateInterventionPourSoldage
{
    use AttributsInterventionNecessairesAuSoldageTrait {
        __construct as __attributsNecessairesAuSoldageConstruct;
    }

    public function __construct(
        ?string $materielConsomme,
        \DateTimeImmutable $dateDeDebut,
        \DateTimeImmutable $dateDeFin,
    ) {
        $this->__attributsNecessairesAuSoldageConstruct(
            materielConsomme: $materielConsomme,
            dateDeDebut: $dateDeDebut,
            dateDeFin: $dateDeFin
        );
    }
}
