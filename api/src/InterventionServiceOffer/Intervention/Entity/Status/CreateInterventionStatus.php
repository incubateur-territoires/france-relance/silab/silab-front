<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Status;

class CreateInterventionStatus
{
    public function __construct(
        private string $code
    ) {
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
