<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Status;

use ApiPlatform\Metadata\GetCollection;
use App\InterventionServiceOffer\Intervention\State\GetInterventionStatusHistoryProvider;

#[GetCollection(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/interventions/{interventionId}/statusHistory',
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_INTERVENTIONS')",
    output: InterventionStatusHistorised::class,
    provider: GetInterventionStatusHistoryProvider::class,
)]
class InterventionHistorisedStatusApiRessource
{
}
