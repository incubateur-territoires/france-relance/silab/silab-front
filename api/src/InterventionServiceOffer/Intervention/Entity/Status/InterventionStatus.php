<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Status;

use App\Shared\ObjectStatus\Entity\ObjectStatusInterface;

class InterventionStatus extends CreateInterventionStatus implements ObjectStatusInterface
{
    public function __construct(
        string $code,
        private ?string $label = null,
        private ?string $createdBy = null,
        private ?\DateTimeInterface $createdAt = null
    ) {
        parent::__construct($code);
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}
