<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Status;

use ApiPlatform\Metadata\Post;
use App\InterventionServiceOffer\Intervention\State\AddStatusToInterventionProcessor;

#[Post(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/interventions/{interventionId}/statuses',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'interventionId' => 'interventionId',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_SOLDER_INTERVENTIONS')",
    input: CreateInterventionStatus::class,
    processor: AddStatusToInterventionProcessor::class,
    read: false,
)]
class InterventionStatusApiRessource
{
}
