<?php

namespace App\InterventionServiceOffer\Intervention\Entity\Status;

use App\Shared\ObjectStatus\Entity\ObjectHistorisedStatusInterface;

class InterventionStatusHistorised extends InterventionStatus implements ObjectHistorisedStatusInterface
{
    public function __construct(
        string $code,
        ?string $label = null,
        ?string $createdBy = null,
        ?\DateTimeInterface $createdAt = null,
        private ?string $comment = null
    ) {
        parent::__construct(
            code: $code,
            label: $label,
            createdBy: $createdBy,
            createdAt: $createdAt
        );
    }

    public function getComment(): string
    {
        return $this->comment ?? '';
    }
}
