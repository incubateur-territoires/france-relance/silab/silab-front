<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\InterventionServiceOffer\Intervention\Exception;

use App\Shared\Exception\RuntimeException;

class ActiviteDatesInvalidException extends RuntimeException
{
    public function __construct(string $message, ?\Throwable $previous = null)
    {
        parent::__construct(message: $message, previous: $previous);
    }
}
