<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\InterventionServiceOffer\Intervention\Exception;

use Symfony\Component\HttpFoundation\Exception\BadRequestException as SymfonyBadRequestException;

class EmptyActionTypeException extends SymfonyBadRequestException
{
    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct(message: "La nature d'intervention est obligatoire", previous: $previous);
    }
}
