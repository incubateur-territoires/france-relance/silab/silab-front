<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\InterventionServiceOffer\Intervention\Exception;

use App\InterventionServiceOffer\Intervention\Entity\ActionType\ActionType;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use Symfony\Component\HttpFoundation\Exception\BadRequestException as SymfonyBadRequestException;

class ForbiddenActionTypeException extends SymfonyBadRequestException
{
    public function __construct(InterventionServiceOffer $interventionServiceOffer, ActionType $actionTypeWithoutLabel, ?\Throwable $previous = null)
    {
        parent::__construct(message: "La nature d'intervention avec l'id ".$actionTypeWithoutLabel->id.' ne pas être assignée à une intervention Carl. Les valeurs accéptées sont les suivantes : '.implode(', ', array_keys($interventionServiceOffer->getActionTypesMap())), previous: $previous);
    }
}
