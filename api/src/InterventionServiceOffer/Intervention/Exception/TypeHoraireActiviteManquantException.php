<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\InterventionServiceOffer\Intervention\Exception;

use App\InterventionServiceOffer\Carl\Entity\DbCarlGmaoActeur;
use App\Shared\Exception\BadRequestException;

class TypeHoraireActiviteManquantException extends BadRequestException
{
    public function __construct(DbCarlGmaoActeur $intervenantConcerné, ?\Throwable $previous = null)
    {
        parent::__construct(message: "Le type d'horaire est manquant. Vérifiez que l'intervenant ".$intervenantConcerné->getNomComplet()." possède un type d'horaire par défaut dans Carl.", previous: $previous);
    }
}
