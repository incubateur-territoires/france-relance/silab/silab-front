<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\InterventionServiceOffer\Intervention\Exception;

use Symfony\Component\HttpFoundation\Exception\BadRequestException as SymfonyBadRequestException;

class UnknownStatusException extends SymfonyBadRequestException
{
    public function __construct(string $targetStatus, ?\Throwable $previous = null)
    {
        parent::__construct(message: "Les transitions vers le statut $targetStatus ne sont pas gérées.", previous: $previous);
    }
}
