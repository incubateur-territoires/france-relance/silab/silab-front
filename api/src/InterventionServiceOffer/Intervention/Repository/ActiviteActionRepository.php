<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteAction;
use App\InterventionServiceOffer\Intervention\Entity\Activite\CreateActiviteAction;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Exception\RuntimeException;

class ActiviteActionRepository implements ActiviteActionRepositoryInterface
{
    private ActiviteActionRepositoryInterface $specificActivitéActionRepository;

    public function __construct(
        CarlActiviteActionRepository $carlActiviteActionRepository,
        InterventionServiceOfferContext $interventionServiceOfferContext
    ) {
        try {
            $this->specificActivitéActionRepository = match ($interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class) {
                CarlConfigurationIntervention::class => $carlActiviteActionRepository,
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class);
        }
    }

    public function delete(string $id): void
    {
        $this->specificActivitéActionRepository->delete($id);
    }

    public function update(string $id, ActiviteAction $activitéAction): ActiviteAction
    {
        return $this->specificActivitéActionRepository->update($id, $activitéAction);
    }

    public function find(mixed $id): ActiviteAction
    {
        return $this->specificActivitéActionRepository->find($id);
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->specificActivitéActionRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findAll(): array
    {
        return $this->findBy([]);
    }

    public function findOneBy(array $criteria): ActiviteAction
    {
        $sampleInterventionArray = $this->findBy($criteria);

        if (0 === count($sampleInterventionArray)) {
            throw new RuntimeException('Aucune opération avec les critères donnés');
        }

        return array_values($sampleInterventionArray)[0];
    }

    public function getClassName()
    {
        return ActiviteAction::class;
    }

    public function save(CreateActiviteAction $createActivitéAction, string $interventionId): ActiviteAction
    {
        return $this->specificActivitéActionRepository->save(
            createActivitéAction: $createActivitéAction,
            interventionId: $interventionId
        );
    }
}
