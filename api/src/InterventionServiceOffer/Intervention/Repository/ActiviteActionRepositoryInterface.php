<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteAction;
use App\InterventionServiceOffer\Intervention\Entity\Activite\CreateActiviteAction;
use Doctrine\Persistence\ObjectRepository;

interface ActiviteActionRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): ActiviteAction;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<ActiviteAction>
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array;

    /**
     * @return array<ActiviteAction>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): ActiviteAction;

    public function update(string $id, ActiviteAction $activitéAction): ActiviteAction;

    public function save(CreateActiviteAction $createActivitéAction, string $interventionId): ActiviteAction;

    public function delete(string $id): void;
}
