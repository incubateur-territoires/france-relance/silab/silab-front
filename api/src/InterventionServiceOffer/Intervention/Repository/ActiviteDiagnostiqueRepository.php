<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteDiagnostique;
use App\InterventionServiceOffer\Intervention\Entity\Activite\UpdateActiviteDiagnostique;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Exception\RuntimeException;

class ActiviteDiagnostiqueRepository implements ActiviteDiagnostiqueRepositoryInterface
{
    private ActiviteDiagnostiqueRepositoryInterface $specificOperationRepository;

    public function __construct(
        CarlActiviteDiagnostiqueRepository $carlActiviteDiagnostiqueRepository,
        InterventionServiceOfferContext $interventionServiceOfferContext
    ) {
        try {
            $this->specificOperationRepository = match ($interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class) {
                CarlConfigurationIntervention::class => $carlActiviteDiagnostiqueRepository,
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class);
        }
    }

    public function update(string $id, UpdateActiviteDiagnostique $updateOperation): ActiviteDiagnostique
    {
        return $this->specificOperationRepository->update($id, $updateOperation);
    }

    public function find(mixed $id): ActiviteDiagnostique
    {
        return $this->specificOperationRepository->find($id);
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->specificOperationRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findAll(): array
    {
        return $this->findBy([]);
    }

    public function findOneBy(array $criteria): ActiviteDiagnostique
    {
        $sampleInterventionArray = $this->findBy($criteria);

        if (0 === count($sampleInterventionArray)) {
            throw new RuntimeException('Aucune opération avec les critères donnés');
        }

        return array_values($sampleInterventionArray)[0];
    }

    public function getClassName()
    {
        return ActiviteDiagnostique::class;
    }
}
