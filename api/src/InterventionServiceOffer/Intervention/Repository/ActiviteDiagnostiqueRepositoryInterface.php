<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteDiagnostique;
use App\InterventionServiceOffer\Intervention\Entity\Activite\UpdateActiviteDiagnostique;
use Doctrine\Persistence\ObjectRepository;

interface ActiviteDiagnostiqueRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): ActiviteDiagnostique;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<ActiviteDiagnostique>
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array;

    /**
     * @return array<ActiviteDiagnostique>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): ActiviteDiagnostique;

    public function update(string $id, UpdateActiviteDiagnostique $updateOperation): ActiviteDiagnostique;
}
