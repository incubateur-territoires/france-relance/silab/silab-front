<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use App\InterventionServiceOffer\Carl\Entity\DbCarlActiviteAction;
use App\InterventionServiceOffer\Carl\Exception\TechnicianNotFoundException;
use App\InterventionServiceOffer\Carl\Service\CarlClientContext;
use App\InterventionServiceOffer\Gmao\Service\CarlConfigurationInterventionContext;
use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteAction;
use App\InterventionServiceOffer\Intervention\Entity\Activite\CreateActiviteAction;
use App\InterventionServiceOffer\Intervention\Exception\TypeHoraireActiviteManquantException;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Carl\Exception\CarlActorNotFoundException;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlActiviteActionRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlGmaoActeurRepository;
use App\Shared\Carl\Service\CurrentCarlUserContext;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Carl\Trait\UnexpectedOrderByValueCheckingTrait;
use App\Shared\Exception\BadRequestException;
use App\Shared\Exception\RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccess;

enum NatureActivité
{
    case Inconnue;
    case Occupation;
    case Woprocess;
}

class CarlActiviteActionRepository implements ActiviteActionRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;
    use UnexpectedOrderByValueCheckingTrait;

    public function __construct(
        private CarlInterventionRepository $carlInterventionRepository,
        private DbCarlActiviteActionRepository $dbCarlActiviteActionRepository,
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private DbCarlGmaoActeurRepository $dbCarlGmaoActeurRepository,
        private CarlConfigurationInterventionContext $carlConfigurationInterventionContext,
        private CarlClientContext $carlClientContext,
        private CurrentCarlUserContext $currentCarlUserContext
    ) {
    }

    public function delete(string $id): void
    {
        $activitéCible = $this->find($id);

        // on ne sait pas encore si on a affaire à un woprocess ou un occupation :
        // on va essayer de récupérer l'un ou l'autre pour savoir à quoi on à affaire
        $typeActivité = NatureActivité::Inconnue;
        try {
            $this->carlClientContext->getCarlClient()->getCarlObjectRaw('occupation', $id);
            $typeActivité = NatureActivité::Occupation;
        } catch (\Throwable $e) {
            $typeActivité = NatureActivité::Woprocess;
        }

        switch ($typeActivité) {
            case NatureActivité::Woprocess:
                // Une opération réalisée ne peut être supprimée, il faut la "dé-réaliser" au préalable
                // Carl nous interdit de mettre la date de réalisation à null si un intervenant est associé.
                if ($activitéCible->isDone()) {
                    $this->update($id, $activitéCible->setDateDeDebut(null)->setIntervenant(null));
                }

                $this->carlClientContext->getCarlClient()->deleteObject('woprocess', $id);
                break;
            case NatureActivité::Occupation:
                $this->carlClientContext->getCarlClient()->deleteObject('occupation', $id);
                break;
        }
    }

    public function update(string $id, ActiviteAction $activitéAction): ActiviteAction
    {
        if (!is_null($activitéAction->getDateDeDebut())) {
            $datesDeLOperation = [$activitéAction->getDateDeDebut()];
            if (!is_null($activitéAction->getDuree())) {
                $datesDeLOperation[] = (new \DateTimeImmutable())->setTimestamp($activitéAction->getDateDeDebut()->getTimestamp() + $activitéAction->getDuree() * 60);
            }
            $this->carlInterventionRepository->enforceInterventionDatesContainDates(
                $this->carlClientContext->getCarlClient()->getCarlObject(
                    entity: 'woprocess',
                    id: $id,
                    includes: [
                        'WO',
                    ]
                )
                    ->getRelationship('WO')->getId(),
                $datesDeLOperation
            );
        }

        if (!is_null($activitéAction->getIntervenant())) {
            $technician = ['type' => 'technician',
                'id' => $this->dbCarlGmaoActeurRepository->find($activitéAction->getIntervenant()->getId())->getTechnicianId(),
            ];
        } else {
            $technician = ['data' => null];
        }

        $woProcessJsonApiResource = $this->carlClientContext->getCarlClient()->patchCarlObject(
            entity: 'woprocess',
            objectId: $activitéAction->getId(),
            attributes: [
                'realisedDate' => $activitéAction->getDateDeDebut()?->format(\DateTimeInterface::ATOM),
            ],
            relationships: [
                'technician' => $technician,
            ],
            includes: [
                'material',
                'box',
            ]
        );

        return $this->find($woProcessJsonApiResource->getId());
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findOneBy(array $criteria): ActiviteAction
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function find(mixed $id): ActiviteAction
    {
        $sampleActivitéActionArray = $this->findBy(['id' => [$id]]);

        if (count($sampleActivitéActionArray) > 1) {
            throw new RuntimeException("Plus d'une activité action prévue trouvée pour l'id : $id");
        }

        if (0 === count($sampleActivitéActionArray)) {
            throw new RuntimeException("Aucune activité action trouvée pour l'id : $id");
        }

        return array_pop($sampleActivitéActionArray);
    }

    public function getClassName()
    {
        return ActiviteAction::class;
    }

    /**
     * @param array<string, mixed> $criteria
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array {
        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }
        if (null !== $orderBy) {
            throw new RuntimeException('Le paramètre orderBy n\'est pas encore implémenté');
        }
        $orderBy = ['ordering' => 'ASC'];

        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
                'intervention.id',
                'isDone',
            ]
        );

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $id = $propertyAccessor->getValue($criteria, '[id]');
        $interventionId = $propertyAccessor->getValue($criteria, '[intervention.id]');
        $isDoneFromAccessor = $propertyAccessor->getValue($criteria, '[isDone]');

        $filters = [];

        if (!empty($id)) {
            $filters['id'] = $id;
        }

        if (!empty($interventionId)) {
            $filters['interventionId'] = $interventionId;
        }

        if (!empty($isDoneFromAccessor)) {
            switch ($isDoneFromAccessor[0]) {
                case 'true':
                    $filters['isDone'] = true;
                    break;
                case 'false':
                    $filters['isDone'] = false;
                    break;
                default:
                    throw new BadRequestException('Le paramètre isDone accepte uniquement un booléen');
            }
        }

        return DbCarlActiviteAction::toActiviteActionArray(
            dbCarlActiviteActions: $this->dbCarlActiviteActionRepository->findBy($filters, $orderBy),
            serviceOffer: $this->interventionServiceOfferContext->getServiceOffer(),
            gmaoCarl: $this->carlConfigurationInterventionContext->getCarlConfiguration()
        );
    }

    public function save(CreateActiviteAction $createActivitéAction, string $interventionId): ActiviteAction
    {
        /** @var NatureActivité */
        $natureDeLActivité = NatureActivité::Inconnue;
        if (!is_null($createActivitéAction->getIntervenantId()) && !is_null($createActivitéAction->getDuree()) && !is_null($createActivitéAction->getDateDeDebut()) && is_null($createActivitéAction->getOperationId())) {
            $natureDeLActivité = NatureActivité::Occupation;
        }
        if (!is_null($createActivitéAction->getIntervenantId()) && !is_null($createActivitéAction->getOperationId()) && !is_null($createActivitéAction->getDateDeDebut())) {
            $natureDeLActivité = NatureActivité::Woprocess;
        }

        if (NatureActivité::Inconnue === $natureDeLActivité) {
            throw new BadRequestException("Activite non pris en charge par le connecteur Carl. L'activité doit possèder au minmium les attributs suivant soit un intervenvent, une durée et une date de début soit un interventant, une date de début et un process");
        }

        $acteur = $this->dbCarlGmaoActeurRepository->find(
            $createActivitéAction->getIntervenantId()
        );

        if (is_null($acteur)) {
            throw new CarlActorNotFoundException($createActivitéAction->getIntervenantId());
        }
        $technicianId = $acteur->getTechnicianId();
        if (is_null($technicianId)) {
            throw new TechnicianNotFoundException($acteur->getNomComplet());
        }

        $champsCommunsOccupationEtWoProcessArray = [
            'data' => [
                'attributes' => [
                    'UOwner' => $this->currentCarlUserContext->getCurrentDbCarlGmaoActeur()->getCode(),
                ],
                'relationships' => [
                    'WO' => [
                        'data' => [
                            'id' => $interventionId,
                            'type' => 'wo',
                        ],
                    ],
                    'technician' => [
                        'data' => [
                            'id' => $technicianId,
                            'type' => 'technician',
                        ],
                    ],
                ],
            ],
        ];

        if (!is_null($createActivitéAction->getDuree())) {
            $champsCommunsOccupationEtWoProcessArray['data']['attributes']['duration'] = $createActivitéAction->getDuree() / 60;
        }

        $occupationArray = $champsCommunsOccupationEtWoProcessArray;
        $woProcessArray = $champsCommunsOccupationEtWoProcessArray;

        $dateDeDébut = $createActivitéAction->getDateDeDebut()->format(\DateTimeInterface::ATOM);

        $datesDeLOperation = [$createActivitéAction->getDateDeDebut()];

        if (!is_null($createActivitéAction->getDuree())) {
            $datesDeLOperation[] = (new \DateTimeImmutable())->setTimestamp($createActivitéAction->getDateDeDebut()->getTimestamp() + $createActivitéAction->getDuree() * 60);
        }

        $this->carlInterventionRepository->enforceInterventionDatesContainDates($interventionId, $datesDeLOperation);

        try {
            switch ($natureDeLActivité) {
                case NatureActivité::Occupation:
                    $occupationArray['data']['attributes']['occupationDate'] = $dateDeDébut;

                    $createdActivitéActionJsonApiResource = $this->carlClientContext->getCarlClient()->postCarlObject(
                        entity: 'occupation',
                        postedResource: json_encode($occupationArray)
                    );
                    break;
                case NatureActivité::Woprocess:
                    $woProcessArray['data']['attributes']['realisedDate'] = $dateDeDébut;
                    $woProcessArray['data']['relationships']['process']['data'] = [
                        'id' => $createActivitéAction->getOperationId(),
                        'type' => 'process',
                    ];

                    $createdActivitéActionJsonApiResource = $this->carlClientContext->getCarlClient()->postCarlObject(
                        entity: 'woprocess',
                        postedResource: json_encode($woProcessArray)
                    );
                    break;
                default:
                    throw new BadRequestException("Type d'Activite non pris en charge");
            }
        } catch (\Throwable $e) {
            if (str_contains($e->getMessage(), 'Le type d\'horaire de l\'activité est obligatoire')) {
                throw new TypeHoraireActiviteManquantException($acteur);
            }
            throw $e;
        }

        return $this->find($createdActivitéActionJsonApiResource->getId());
    }
}
