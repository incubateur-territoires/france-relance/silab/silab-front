<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use App\InterventionServiceOffer\Carl\Entity\DbCarlActiviteDiagnostique;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteDiagnostique;
use App\InterventionServiceOffer\Intervention\Entity\Activite\UpdateActiviteDiagnostique;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlActiviteDiagnostiqueRepository;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Carl\Trait\UnexpectedOrderByValueCheckingTrait;
use App\Shared\Exception\BadRequestException;
use App\Shared\Exception\RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlActiviteDiagnostiqueRepository implements ActiviteDiagnostiqueRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;
    use UnexpectedOrderByValueCheckingTrait;

    private CarlConfigurationIntervention $carlConfigurationIntervention;
    private CarlClient $carlClient;

    public function __construct(
        private CarlInterventionRepository $carlInterventionRepository,
        private DbCarlActiviteDiagnostiqueRepository $dbCarlActiviteDiagnostiqueRepository,
        private InterventionServiceOfferContext $interventionServiceOfferContext,
    ) {
        $carlConfigurationIntervention = $interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration();
        if (is_null($carlConfigurationIntervention)) {
            throw new RuntimeException('Veuillez attacher une configuration GMAO à cette offre de service');
        }

        assert($carlConfigurationIntervention instanceof CarlConfigurationIntervention);
        $this->carlConfigurationIntervention = $carlConfigurationIntervention;
        $this->carlClient = $this->carlConfigurationIntervention->getCarlClient();
        if (is_null($this->carlClient)) {
            throw new RuntimeException('Veuillez configurer un client Carl pour cette offre de service');
        }
    }

    public function update(string $id, UpdateActiviteDiagnostique $updateOperation): ActiviteDiagnostique
    {
        if (!is_null($updateOperation->getDateDeDebut())) {
            $datesDeLOperation = [$updateOperation->getDateDeDebut()];

            $this->carlInterventionRepository->enforceInterventionDatesContainDates(
                $this->carlClient->getCarlObject(
                    entity: 'wodiagprocess',
                    id: $id,
                    includes: [
                        'WO',
                    ]
                )
                    ->getRelationship('WO')->getId(),
                $datesDeLOperation
            );
        }

        $woDiagProcessJsonApiResource = $this->carlClient->patchCarlObject(
            entity: 'wodiagprocess',
            objectId: $id,
            attributes: [
                'realisedDate' => $updateOperation->getDateDeDebut()?->format(\DateTimeInterface::ATOM),
            ],
            relationships: [
                'evaluation' => [
                    'data' => !is_null($updateOperation->getDateDeDebut())
                        ? [
                            'id' => $this->carlClient->getCarlEvaluationId($updateOperation->getEvaluation()),
                            'type' => 'evaluation',
                        ]
                        : null,
                ],
                'createdWO' => [
                    'data' => !is_null($updateOperation->getInterventionIdGeneree())
                        ? [
                            'id' => $updateOperation->getInterventionIdGeneree(),
                            'type' => 'wo',
                        ]
                        : null,
                ],
                'createdMR' => [
                    'data' => !is_null($updateOperation->getDemandeInterventionIdGeneree())
                        ? [
                            'id' => $updateOperation->getDemandeInterventionIdGeneree(),
                            'type' => 'mr',
                        ]
                        : null,
                ],
            ],
            includes: [
                'material',
                'box',
                'evaluation',
                'createdWO',
                'createdMR',
            ]
        );

        return $this->find($woDiagProcessJsonApiResource->getId());
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findOneBy(array $criteria): ActiviteDiagnostique
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function find(mixed $id): ActiviteDiagnostique
    {
        $sampleOpérationPrévueArray = $this->findBy(['id' => [$id]]);

        if (count($sampleOpérationPrévueArray) > 1) {
            throw new RuntimeException("Plus d'une opération prévue trouvée pour l'id : $id");
        }

        if (0 === count($sampleOpérationPrévueArray)) {
            throw new RuntimeException("Aucune intervention trouvée pour l'id : $id");
        }

        return array_pop($sampleOpérationPrévueArray);
    }

    public function getClassName()
    {
        return ActiviteDiagnostique::class;
    }

    /**
     * @param array<string, mixed> $criteria
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array {
        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }
        if (null !== $orderBy) {
            throw new RuntimeException('Le paramètre orderBy n\'est pas encore implémenté');
        }
        $orderBy = ['ordering' => 'ASC'];

        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
                'intervention.id',
                'isDone',
            ]
        );
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $id = $propertyAccessor->getValue($criteria, '[id]');
        $interventionId = $propertyAccessor->getValue($criteria, '[intervention.id]');
        $isDoneFromAccessor = $propertyAccessor->getValue($criteria, '[isDone]');

        $filters = [];

        if (!empty($id)) {
            $filters['id'] = $id;
        }

        if (!empty($interventionId)) {
            $filters['interventionId'] = $interventionId;
        }

        if (!empty($isDoneFromAccessor)) {
            switch ($isDoneFromAccessor[0]) {
                case 'true':
                    $filters['isDone'] = true;
                    break;
                case 'false':
                    $filters['isDone'] = false;
                    break;
                default:
                    throw new BadRequestException('Le paramètre isDone accepte uniquement un booléen');
            }
        }

        return DbCarlActiviteDiagnostique::toActiviteDiagnostiqueArray(
            dbCarlActiviteDiagnostiques: $this->dbCarlActiviteDiagnostiqueRepository->findBy($filters, $orderBy),
            serviceOffer: $this->interventionServiceOfferContext->getServiceOffer(),
            gmaoCarl: $this->carlConfigurationIntervention
        );
    }
}
