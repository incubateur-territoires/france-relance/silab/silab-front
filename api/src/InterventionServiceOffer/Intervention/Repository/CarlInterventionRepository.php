<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use ApiPlatform\State\Pagination\PaginatorInterface;
use App\InterventionServiceOffer\Carl\Entity\DbCarlIntervention;
use App\InterventionServiceOffer\Carl\Entity\DbCarlInterventionCoordinates;
use App\InterventionServiceOffer\Carl\Entity\DbCarlInterventionStatus;
use App\InterventionServiceOffer\Carl\Service\CarlClientContext;
use App\InterventionServiceOffer\Gmao\Service\CarlConfigurationInterventionContext;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\CreateIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\Entity\Status\InterventionStatus;
use App\InterventionServiceOffer\Intervention\Entity\Status\InterventionStatusHistorised;
use App\InterventionServiceOffer\Intervention\Exception\UnknownStatusException;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\ApiPlatform\Pagination\TraversablePaginator;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlCustomerRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlEquipementRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlEquipementTypeRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlInterventionCoordinatesRepository;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlInterventionRepository;
use App\Shared\Carl\Service\CurrentCarlUserContext;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Carl\Trait\UnexpectedOrderByValueCheckingTrait;
use App\Shared\Exception\BadCarlConfigurationInterventionException;
use App\Shared\Exception\BadConfigurationException;
use App\Shared\Exception\RuntimeException;
use App\Shared\JsonApi\JsonApiResource;
use App\Shared\ReverseGeocoding\Service\ReverseGeocoding;
use App\Shared\User\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlInterventionRepository implements InterventionRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;
    use UnexpectedOrderByValueCheckingTrait;

    public function __construct(
        private DbCarlInterventionRepository $dbCarlInterventionRepository,
        private DbCarlInterventionCoordinatesRepository $dbCarlInterventionCoordinatesRepository,
        private DbCarlEquipementRepository $dbCarlEquipementRepository,
        private DbCarlCustomerRepository $dbCarlCustomerRepository,
        private DbCarlEquipementTypeRepository $dbCarlEquipementTypeRepository,
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private CarlConfigurationInterventionContext $carlConfigurationInterventionContext,
        private CarlClientContext $carlClientContext,
        private CurrentCarlUserContext $currentCarlUserContext,
        private ReverseGeocoding $reverseGeocoding
    ) {
    }

    public function transistionToStatus(string $id, InterventionStatus $status): InterventionStatus
    {
        $dbCarlIntervention = $this->dbCarlInterventionRepository->find($id);
        switch ($status->getCode()) {
            case 'closed':
                // s'il y a du matériel consommé, on fait une transition différente // amelioration: specif: ici on est spécifique GP je pense (le statut AWAITINGRECOMP n'est peut-être pas standard)
                if (!empty($dbCarlIntervention->getMaterielConsomme())) {
                    $interventionStatusWorkflow = DbCarlIntervention::INTERVENTION_STATUS_WORKFLOW_VERS_AWAITINGRECOMP;
                    $targetStatusComment = substr($dbCarlIntervention->getMaterielConsomme()."\nVia Siilab par ".$this->currentCarlUserContext->getCurrentGmaoActeur()->getNomComplet(), 0, CarlClient::STATUS_COMMENT_MAX_LENGTH);
                    $targetCarlStatusCode = 'AWAITINGRECOMP';
                    break;
                }
                $interventionStatusWorkflow = DbCarlIntervention::INTERVENTION_STATUS_WORKFLOW_VERS_CLOSED;
                $targetStatusComment = substr('Via Siilab par '.$this->currentCarlUserContext->getCurrentGmaoActeur()->getNomComplet(), 0, CarlClient::STATUS_COMMENT_MAX_LENGTH);
                $targetCarlStatusCode = 'CLOSED';
                break;
            default:
                throw new UnknownStatusException($status->getCode());
        }

        $this->carlClientContext->getCarlClient()->transitionToStatus(
            entityType: 'wo',
            objectId: $id,
            targetCarlStatusCode: $targetCarlStatusCode,
            statusCodeWorkflow: $interventionStatusWorkflow,
            targetStatusComment: $targetStatusComment
        );

        if ('closed' === $status->getCode()) {
            /**
             * Si l'intervention doit être soldée et est issue d'un plan préventif, il peut y avoir eu du retard pour traiter l'intervention
             * ce qui fait que le plan préventif carl à potentiellement généré d'autres interventions identifques
             * on préfère annuler ces autres interventions issues du même plan préventif sur cette équipement car :
             * - Il n'y a pas d'intérêt à laisser les autres interventions ouvertes, il faut attendre le prochain déclenchement du plan préventif
             * - Carl n'est pas en mesure de le faire.
             */
            $dbCarlIntervention = $this->dbCarlInterventionRepository->find($id);
            assert($dbCarlIntervention instanceof DbCarlIntervention);
            $preventiveTriggerId = $dbCarlIntervention->getPreventiveTriggerId();
            if (!is_null($preventiveTriggerId)) {
                $criteria = [
                    'preventiveTriggerId' => $preventiveTriggerId,
                    'statusCode' => ['VALIDATE', 'AWAITINGREAL'],
                ];

                $dbCarlIntreventionsOuvertesAvecLeMemePlanPréventifSurLeMêmeEquipement = $this->dbCarlInterventionRepository->findBy($criteria);

                foreach ($dbCarlIntreventionsOuvertesAvecLeMemePlanPréventifSurLeMêmeEquipement as $dbCarlInterventionAAnnuler) {
                    // amelioration : on devrait récupérer les workflow dynamiquement pour faire ça

                    $this->carlClientContext->getCarlClient()->transitionToStatus(
                        entityType: 'wo',
                        objectId: $dbCarlInterventionAAnnuler->getId(),
                        targetCarlStatusCode: 'CANCEL',
                        statusCodeWorkflow: DbCarlIntervention::INTERVENTION_STATUS_WORKFLOW_VERS_CANCEL,
                        targetStatusComment: "Annulation automatique via Siilab suite au soldage de l'OT ".$dbCarlIntervention->getCode().".\nPar ".$this->currentCarlUserContext->getCurrentGmaoActeur()->getNomComplet()
                    );
                }
            }
        }

        return $this->find($id)->getCurrentStatus();
    }

    public function findAll(): PaginatorInterface
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findOneBy(array $criteria): Intervention
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function update(string $id, CreateIntervention $entity, string $userEmail): Intervention
    {
        $intervention = $entity;
        $originalIntervention = $this->find($id);
        $attributes = [];

        $attributes['workPriority'] = array_search($intervention->getPriority(), $this->carlClientContext->getCarlClient()->getPriorityMapping());
        $attributes['latitude'] = $intervention->getLatitude();
        $attributes['longitude'] = $intervention->getLongitude();
        $attributes['description'] = $intervention->getTitle();
        $attributes['WOBegin'] = $intervention->getDateDeDebut()->format(\DateTimeInterface::ATOM);
        $attributes['WOEnd'] = $intervention->getDateDeFin()->format(\DateTimeInterface::ATOM);

        if (!is_null($this->carlClientContext->getCarlClient()->getChampCarlPourStockageMaterielConsomme())) {
            $attributes[$this->carlClientContext->getCarlClient()->getChampCarlPourStockageMaterielConsomme()] = $intervention->getMaterielConsomme();
        }

        $relationships = [];

        $actionType = $intervention->getActionType();
        $hasActionType = !is_null($actionType);

        $relationships['actionType']['data'] = $hasActionType
            ? [
                'id' => $actionType->id,
                'type' => 'actiontype',
            ]
            : null;

        if (!empty($intervention->getDescription())) {
            if (!empty($originalIntervention->getDescription())) {
                $this->carlClientContext->getCarlClient()->patchCarlObject(
                    'description',
                    $this->carlClientContext->getCarlClient()->getCarlObject('wo', $id, ['longDesc'])
                        ->getRelationship('longDesc')
                        ->getId(),
                    ['description' => $intervention->getDescription()]
                );
            } else {
                $this->carlClientContext->getCarlClient()->addDescriptionToObjectRelationshipsArray($relationships, $intervention->getDescription(), 'longDesc');
            }
        } elseif (!empty($originalIntervention->getDescription())) {
            $this->carlClientContext->getCarlClient()->deleteObjectDescription(objectId: $id, entity: 'wo', descriptionRelashionshipName: 'longDesc');
        }

        $this->carlClientContext->getCarlClient()->patchCarlObject('wo', $id, $attributes, $relationships);

        $this->execScriptSpécifiquePoitiersPostPersist(
            $id
        );

        return $this->find($id);
    }

    public function find(mixed $id): Intervention
    {
        $sampleInterventionArray = $this->findBy(
            criteria: ['id' => [$id], 'includeDemandes' => true]
        );

        if (count($sampleInterventionArray) > 1) {
            throw new RuntimeException("Plus d'une intervention trouvée pour l'id : $id");
        }

        if (0 === count($sampleInterventionArray)) {
            throw new RuntimeException("Aucune intervention trouvée pour l'id : $id");
        }

        return iterator_to_array($sampleInterventionArray)[0];
    }

    public function save(CreateIntervention $entity, User $currentUser): Intervention
    {
        $attributesPreset = $this->carlConfigurationInterventionContext->getCarlConfiguration()
            ->getCarlAttributesPresetForInterventionCreation();
        $relationshipsPreset = $this->carlConfigurationInterventionContext->getCarlConfiguration()
            ->getApiFormatedCarlRelationshipsPresetForInterventionCreation();

        $intervention = $entity;
        $userId = $this->carlClientContext->getCarlClient()
            ->getUserIdFromEmail($currentUser->getEmail());

        $materielConsomméAttributes = [];
        if (!is_null($this->carlClientContext->getCarlClient()->getChampCarlPourStockageMaterielConsomme())) {
            $materielConsomméAttributes[$this->carlClientContext->getCarlClient()->getChampCarlPourStockageMaterielConsomme()] = $intervention->getMaterielConsomme();
        }

        $interventionArray = [
            'data' => [
                'type' => 'wo',
                'attributes' => array_merge(
                    [
                        'description' => $intervention->getTitle(),
                        'statusCode' => 'VALIDATE',
                        'workPriority' => array_search($intervention->getPriority(), $this->carlClientContext->getCarlClient()->getPriorityMapping()),
                        'WOBegin' => $intervention->getDateDeDebut()->format(\DateTimeInterface::ATOM),
                        'WOEnd' => $intervention->getDateDeFin()->format(\DateTimeInterface::ATOM),
                    ],
                    $materielConsomméAttributes,
                    $attributesPreset
                ),
                'relationships' => array_merge(
                    [
                        'createdBy' => [
                            'data' => [
                                'id' => $userId,
                                'type' => 'actor',
                            ],
                        ],
                    ],
                    $relationshipsPreset
                ),
            ],
        ];

        if (!is_null($intervention->getLatitude()) && !is_null($intervention->getLongitude())) {
            $interventionArray['data']['attributes']['latitude'] = $intervention->getLatitude();
            $interventionArray['data']['attributes']['longitude'] = $intervention->getLongitude();
        }

        $costCenterId = $this->getCostCenterIdForThisNewIntervention($intervention);

        $interventionArray['data']['relationships']['costCenter']['data'] =
            [
                'id' => $costCenterId,
                'type' => 'costcenter',
            ];

        $directions = $this->carlConfigurationInterventionContext->getCarlConfiguration()->getDirections();
        if (count($directions) > 0) {
            $interventionArray['data']['relationships']['supervisor']['data'] =
                [
                    'id' => array_pop($directions),
                    'type' => 'actor',
                ];
        }
        // S'il n'y a pas d'action type de fournit et que l'on est sur un equipement on lui associe l'action Type 'CORREFCTIF : Maintenancecorretive'.
        $actionTypeId = $intervention->getActionType()?->id
            ?? ($intervention->getRelatedEquipement()
                ? '5'
                : null);

        if (is_null($actionTypeId)) {
            throw new BadConfigurationException($this->carlConfigurationInterventionContext->getCarlConfiguration()->getTitle(), 'la nature est obligatoire pour la création d\'une intervention');
        }

        $interventionArray['data']['relationships']['actionType'] = [
            'data' => [
                'id' => $actionTypeId,
                'type' => 'actiontype',
            ],
        ];

        if (!is_null($intervention->getDescription())) {
            $this->carlClientContext->getCarlClient()->addDescriptionToObjectRelationshipsArray(
                $interventionArray['data']['relationships'],
                $intervention->getDescription(),
                'longDesc'
            );
        }

        $createdInterventionApiResource = $this->carlClientContext->getCarlClient()->postCarlObject('wo', json_encode($interventionArray));

        if (!is_null($relatedEquipment = $intervention->getRelatedEquipement())) {
            $this->attachInterventionToEquipement(
                $createdInterventionApiResource->getId(),
                $relatedEquipment->getId()
            );
        }

        $this->execScriptSpécifiquePoitiersPostPersist(
            $createdInterventionApiResource->getId()
        );

        return $this->find($createdInterventionApiResource->getId());
    }

    private function execScriptSpécifiquePoitiersPostPersist(
        string $createdInterventionId
    ): void {
        $this->updateCommune(
            $createdInterventionId,
            $this->carlClientContext->getCarlClient(),
            $this->dbCarlEquipementRepository,
            $this->dbCarlCustomerRepository,
            $this->reverseGeocoding
        );
    }

    /**
     * Ajoute la commune a l'intervention renseigné.
     *
     * Context :
     *      Chez Grand Poitiers la commune correspond au champ suivant :
     *          - Equipement -> xtraXxt09
     *          - Wo -> poiXtraTxt14
     *
     * Régles de définition de la commune :
     *      - Si équipement lié prendre sa commune,
     *      - Sinon si Géolocalisation,
     *          prendre la commune identifié depuis l'api de géocoding,
     *      - Sinon null
     */
    private function updateCommune(
        string $createdInterventionId,
        CarlClient $carlClient,
        DbCarlEquipementRepository $dbCarlEquipementRepository,
        DbCarlCustomerRepository $dbCarlCustomerRepository,
        ReverseGeocoding $reverseGeocoding
    ): void {
        $intervention = $this->find($createdInterventionId);
        $hasEquipment = !is_null($relatedEquipment = $intervention->getRelatedEquipement());
        $codeCommune = null;
        if ($hasEquipment) {
            $codeCommune = $dbCarlEquipementRepository->find(
                $relatedEquipment->getId()
            )
                ?->getXtratxt09();
        } elseif ($intervention->hasCoordinates()) {
            $nomCommune = $reverseGeocoding->getCommuneFromCoordinates(
                $intervention->getLatitude(),
                $intervention->getLongitude()
            );
            $codeCommune = $dbCarlCustomerRepository->findOneBy(['label' => $nomCommune])?->getCode();
        }

        if (is_null($codeCommune)) {
            return;
        }

        $carlClient->patchCarlObject(
            entity: 'wo',
            objectId: $createdInterventionId,
            attributes: ['poiXtraTxt14' => $codeCommune]
        );
    }

    public function getCostCenterIdForThisNewIntervention(CreateIntervention $intervention): string
    {
        $costCenterId = $this->carlConfigurationInterventionContext->getCarlConfiguration()->getCostCenterIdForInterventionCreation();

        $utiliserCostcenterDeLequipementEnPriorité = $this->carlConfigurationInterventionContext->getCarlConfiguration()->getUseEquipmentCostCenterForInterventionCreation();
        $LInterventionPorteSurUnEquipement = !is_null($intervention->getRelatedEquipement());
        if ($utiliserCostcenterDeLequipementEnPriorité && $LInterventionPorteSurUnEquipement) {
            $carlEqptIdAndType = $this->dbCarlEquipementTypeRepository->find($intervention->getRelatedEquipement()->getId());

            $relatedEquipementJsonApiRessource = $this->carlClientContext->getCarlClient()->getCarlObject(
                entity: $carlEqptIdAndType->getTypeId(),
                id: $carlEqptIdAndType->getId(),
                includes: ['costCenter'],
                fields: [
                    $carlEqptIdAndType->getTypeId() => ['description'],
                    'costcenter' => ['id'],
                ]
            );

            $costCenterId = $relatedEquipementJsonApiRessource->getRelationship('costCenter')?->getId() ?? $costCenterId;

            if (is_null($costCenterId)) {
                throw new BadCarlConfigurationInterventionException($this->carlConfigurationInterventionContext->getCarlConfiguration(), 'L\'équipement rattaché à l\'intervention ne possède aucun centre de coût et celui par défault costCenterForInterventionCreation n\'est pas définit.');
            }
        }
        if (is_null($costCenterId)) {
            throw new BadCarlConfigurationInterventionException($this->carlConfigurationInterventionContext->getCarlConfiguration(), 'Le centre de coût par défault costCenterForInterventionCreation n\'est pas définit, et l\'offre de service n\'est pas configurée pour récupérer les centres de coût depuis les équipements.');
        }

        return $costCenterId;
    }

    private function attachInterventionToEquipement(string $interventionId, string $equipmentId): void
    {
        // 1. suppression des liaisons existantes
        // 1.1 récupération des eqpt liés
        $carlWoEqpts = $this->carlClientContext->getCarlClient()->getCarlObjectsRaw(
            entity: 'woeqpt',
            filters: ['WO.id' => [$interventionId]],
        );
        // 1.2 suppression des eqpt liés
        foreach ($carlWoEqpts['data'] as $carlWoEqpt) {
            $this->carlClientContext->getCarlClient()->deleteObject(
                entity: 'woeqpt',
                id: $carlWoEqpt['id']
            );
        }
        $hasMaterialDirectEqpt = false;
        $hasBoxDirectEqpt = false;
        $carlEqptIdAndType = $this->dbCarlEquipementTypeRepository->find($equipmentId);
        $equipementJsonApiResource = $this->carlClientContext->getCarlClient()->getCarlObject(
            entity: $carlEqptIdAndType->getTypeId(),
            id: $carlEqptIdAndType->getId(),
        );
        $this->linkModelToWoIfExists( // la liaison avec le modèle se fait directement depuis les attributs
            equipementJsonApiResource: $equipementJsonApiResource,
            woId: $interventionId
        );

        $this->linkEqptAndAncestorsToWoRecursive(
            equipement: $equipementJsonApiResource,
            woId: $interventionId,
            woHasMaterialDirectEqpt: $hasMaterialDirectEqpt,
            woHasBoxDirectEqpt: $hasBoxDirectEqpt
        );
    }

    private function linkModelToWoIfExists(JsonApiResource $equipementJsonApiResource, string $woId): void
    {
        // seuls les materials ont des modèles (item)
        if ('material' !== $equipementJsonApiResource->getType()) {
            return;
        }

        $equipementJsonApiResource = $this->carlClientContext->getCarlClient()->getCarlObject(
            entity: $equipementJsonApiResource->getType(),
            id: $equipementJsonApiResource->getId(),
            includes: ['item']
        );

        $item = $equipementJsonApiResource->getRelationship('item');

        if (is_null($item)) {
            return;
        }

        $this->linkEqptToWo(equipement: $item, woId: $woId, directEqpt: true);
    }

    /**
     * Amelioration, la partie "liaison avec les ancetres" est à revoir (ordre de prio de liaison si plusieurs parents éligibles),
     * et potentiellement à basculer sur un script spécif.
     */
    private function linkEqptAndAncestorsToWoRecursive(
        JsonApiResource $equipement,
        string $woId,
        bool &$woHasMaterialDirectEqpt,
        bool &$woHasBoxDirectEqpt
    ): void {
        $isDirectEqpt = false;

        if (!$woHasMaterialDirectEqpt && 'material' === $equipement->getType()) {
            $isDirectEqpt = true;
            $woHasMaterialDirectEqpt = true;
        }

        if (!$woHasBoxDirectEqpt && $this->carlClientContext->getCarlClient()->isBox($equipement->getType())) {
            $isDirectEqpt = true;
            $woHasBoxDirectEqpt = true;
        }
        $this->linkEqptToWo(
            equipement: $equipement,
            woId: $woId,
            directEqpt: $isDirectEqpt
        );

        $parentsLinkequipments = $this->carlClientContext->getCarlClient()->getCarlObject(
            entity: $equipement->getType(),
            id: $equipement->getId(),
            includes: ['parents', 'parents.parent']
        )->getRelationshipCollection('parents');

        foreach ($parentsLinkequipments as $parentLinkequipment) {
            if (
                !is_null($parentLinkequipment->getRelationship('parent'))
                && $this->eqptLinkIsValid($parentLinkequipment)
            ) {
                $this->linkEqptAndAncestorsToWoRecursive(
                    equipement: $parentLinkequipment->getRelationship('parent'),
                    woId: $woId,
                    woHasMaterialDirectEqpt: $woHasMaterialDirectEqpt,
                    woHasBoxDirectEqpt: $woHasBoxDirectEqpt
                );
            }
        }
    }

    private function eqptLinkIsValid(JsonApiResource $linkEqptJsonApiResource): bool
    {
        $linkHasStartedInThePast = (new \DateTimeImmutable($linkEqptJsonApiResource->getAttribute('linkBegin'))) < (new \DateTimeImmutable());
        $linkWillEndInTheFuture = (new \DateTimeImmutable($linkEqptJsonApiResource->getAttribute('linkEnd'))) > (new \DateTimeImmutable());

        return $linkHasStartedInThePast && $linkWillEndInTheFuture;
    }

    private function linkEqptToWo(JsonApiResource $equipement, string $woId, bool $directEqpt = false): JsonApiResource
    {
        $carlWoEqptArray = [
            'data' => [
                'type' => 'woeqpt',
                'attributes' => [
                    'directEqpt' => $directEqpt,
                ],
                'relationships' => [
                    'WO' => [
                        'data' => [
                            'id' => $woId,
                            'type' => 'wo',
                        ],
                    ],
                    'eqpt' => [
                        'data' => [
                            'id' => $equipement->getId(),
                            'type' => $equipement->getType(),
                        ],
                    ],
                ],
            ],
        ];

        return $this->carlClientContext->getCarlClient()->postCarlObject('woeqpt', json_encode($carlWoEqptArray));
    }

    public function getClassName()
    {
        return Intervention::class;
    }

    /**
     * @param array<string, mixed> $criteria :
     *                                       [interventionActionTypes] = Nature sur Carl. Seules les interventions
     *                                       qui ont une de ces natures seront retournée.
     *                                       Si vide, toutes les natures seront incluses.
     *                                       [directions]              = supervisor sur Carl. Seules les interventions
     *                                       dont la direction du demandeur correspond
     *                                       seront retournées.
     *                                       Si vide, toutes les directions seront incluses.
     *                                       [costCenters]             Seules les interventions
     *                                       dont le centre de cout correspond à une des valeur
     *                                       seront retournées.
     *                                       Si vide, tous les centres de coûts seront inclus.
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): PaginatorInterface {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $includeDemandesFilter = $propertyAccessor->getValue($criteria, '[includeDemandes]');
        $includeDemandes = false;
        if (true === $includeDemandesFilter) {
            $includeDemandes = true;
        }
        unset($criteria['includeDemandes']);

        $query = $this->createQueryBy(
            entityRepository: $this->dbCarlInterventionRepository,
            criteria: $criteria,
            orderBy: $orderBy,
            limit: $limit,
            offset: $offset
        );

        $dbCarlInterventionsPaginator = new DoctrinePaginator(
            $query
        );
        $limit ??= PHP_INT_MAX;
        $carlGmao = $this->carlConfigurationInterventionContext->getCarlConfiguration();

        $page = intdiv($offset ?? 0, $limit) + 1;

        return new TraversablePaginator(
            new \ArrayIterator(array_map(
                function ($dbCarlIntervention) use ($carlGmao, $includeDemandes) {
                    assert($dbCarlIntervention instanceof DbCarlIntervention);

                    return $dbCarlIntervention->toIntervention(
                        serviceOffer: $this->interventionServiceOfferContext->getServiceOffer(),
                        gmaoCarl: $carlGmao,
                        includeDemandes: $includeDemandes
                    );
                },
                iterator_to_array($dbCarlInterventionsPaginator->getIterator())
            )),
            $page,
            $limit,
            $dbCarlInterventionsPaginator->count()
        );
    }

    public function findCoordinatesBy(array $criteria): array
    {
        return array_map(
            function ($dbCarlInterventionCoordinates) {
                assert($dbCarlInterventionCoordinates instanceof DbCarlInterventionCoordinates);

                return $dbCarlInterventionCoordinates->toInterventionCoordinates();
            },
            $this->createQueryBy(
                entityRepository: $this->dbCarlInterventionCoordinatesRepository,
                criteria: $criteria,
                orderBy: null
            )->getResult()
        );
    }

    /**
     * @template EntityType of object
     *
     * @param ServiceEntityRepository<EntityType> $entityRepository
     * @param array<int,string>|null              $orderBy
     * @param array<string, mixed>                $criteria
     */
    public function createQueryBy(
        ServiceEntityRepository $entityRepository,
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): Query {
        $criteria['directions'] = $this->carlConfigurationInterventionContext->getCarlConfiguration()->getDirections();
        $criteria['costCenters'] = $this->carlConfigurationInterventionContext->getCarlConfiguration()->getInterventionCostCenterIdsFilter();

        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
                'statuses',
                'interventionActionTypes',
                'directions',
                'costCenters',
                'equipementRacine.id',
                'relatedEquipement.id',
                'intervenantsAffectes.id',
                'priority',
                'dateDeDebut',
                'dateDeFin',
            ]
        );

        $interventionQueryBuilder = $entityRepository->createQueryBuilder('dbCarlInterventionLike');

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $interventionQueryBuilder
                ->innerJoin('dbCarlInterventionLike.dbCarlEquipements', 'dbCarlInterventionDbCarlEquipement');

        $equipementsRacinesIdsAuFormatSiilab = $propertyAccessor->getValue($criteria, '[equipementRacine.id]');
        if (!empty($equipementsRacinesIdsAuFormatSiilab)) {
            // on doit convertir les id d'équipements récupérés
            $dbCarlEquipementsRacinesIds = array_map(
                function (string $equipementRacineId) {
                    return ['id' => $equipementRacineId];
                },
                $equipementsRacinesIdsAuFormatSiilab
            );

            // amelioration, ignorer les doublons intelligement... ou pas
            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionDbCarlEquipement.dbCarlEquipement IN(:equipementsRacinesIds)')
                ->setParameter('equipementsRacinesIds', $dbCarlEquipementsRacinesIds);
        }

        $relatedEquipementIds = $propertyAccessor->getValue($criteria, '[relatedEquipement.id]');
        if (!empty($relatedEquipementIds)) {
            // on doit convertir les id d'équipements récupérés
            $dbCarlEquipementIds = array_map(
                function (string $relatedEquipementId) {
                    return ['id' => $relatedEquipementId];
                },
                $relatedEquipementIds
            );

            // amelioration, ignorer les doublons intelligement... ou pas
            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionDbCarlEquipement.dbCarlEquipement IN(:relatedEquipementIds)')
                ->andWhere('dbCarlInterventionDbCarlEquipement.directEqpt = true')
                ->setParameter('relatedEquipementIds', $dbCarlEquipementIds);
        }

        $id = $propertyAccessor->getValue($criteria, '[id]');
        if (!empty($id)) {
            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionLike.id IN(:dbCarlInterventionLike_id)')
                ->setParameter('dbCarlInterventionLike_id', $id);
        }

        $statuses = $propertyAccessor->getValue($criteria, '[statuses]');
        if (!empty($statuses)) {
            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionLike.statusCode IN(:dbCarlInterventionLike_statusCode)')
                ->setParameter('dbCarlInterventionLike_statusCode', $this->getCarlStatusCodesFromSilabStatusCodes($statuses));
        }

        $interventionActionTypes = $propertyAccessor->getValue($criteria, '[interventionActionTypes]');
        if (!empty($interventionActionTypes)) {
            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionLike.actionTypeId IN(:dbCarlInterventionLike_actionTypeId)')
                ->setParameter('dbCarlInterventionLike_actionTypeId', $interventionActionTypes);
        }

        $directions = $propertyAccessor->getValue($criteria, '[directions]');
        if (!empty($directions)) {
            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionLike.supervisorId IN(:dbCarlInterventionLike_supervisorId)')
                ->setParameter('dbCarlInterventionLike_supervisorId', $directions);
        }

        $costCenters = $propertyAccessor->getValue($criteria, '[costCenters]');
        if (!empty($costCenters)) {
            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionLike.costcenterId IN(:dbCarlInterventionLike_costcenterId)')
                ->setParameter('dbCarlInterventionLike_costcenterId', $costCenters);
        }

        $intervenantsAffectesIds = $propertyAccessor->getValue($criteria, '[intervenantsAffectes.id]');
        if (!empty($intervenantsAffectesIds)) {
            $interventionQueryBuilder
                ->innerJoin('dbCarlInterventionLike.dbCarlRessources', 'dbCarlRessource')
                ->andWhere('dbCarlRessource.dbCarlGmaoActeur IN(:intervenantsAffectesIds)')
                ->setParameter('intervenantsAffectesIds', $intervenantsAffectesIds);
        }

        $siilabPriorities = $propertyAccessor->getValue($criteria, '[priority]');
        if (!empty($siilabPriorities)) {
            $carlPriorityMapping = $this->carlClientContext->getCarlClient()->getPriorityMapping();

            // conversion des priorités siilab vers les code workPriority carl correspondants
            $carlPriorities = array_reduce(
                $siilabPriorities,
                function ($carlPriorities, $siilabPriority) use ($carlPriorityMapping) {
                    return array_unique([...$carlPriorities, ...array_keys($carlPriorityMapping, $siilabPriority)]);
                },
                []
            );

            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionLike.workPriority IN(:priority)')
                ->setParameter('priority', $carlPriorities);
        }
        $dateDeDebut = $propertyAccessor->getValue($criteria, '[dateDeDebut]');
        if (isset($dateDeDebut['before'])) {
            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionLike.beginDate <= :dateDeDebutAvant')
                ->setParameter('dateDeDebutAvant', new \DateTimeImmutable($dateDeDebut['before']), Types::DATETIME_IMMUTABLE);
        }
        if (isset($dateDeDebut['after'])) {
            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionLike.beginDate >= :dateDeDebutApres')
                ->setParameter('dateDeDebutApres', new \DateTimeImmutable($dateDeDebut['after']), Types::DATETIME_IMMUTABLE);
        }

        $dateDeFin = $propertyAccessor->getValue($criteria, '[dateDeFin]');
        if (isset($dateDeFin['before'])) {
            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionLike.endDate <= :dateDeFinAvant')
                ->setParameter('dateDeFinAvant', new \DateTimeImmutable($dateDeFin['before']), Types::DATETIME_IMMUTABLE);
        }
        if (isset($dateDeFin['after'])) {
            $interventionQueryBuilder
                ->andWhere('dbCarlInterventionLike.endDate >= :dateDeFinApres')
                ->setParameter('dateDeFinApres', new \DateTimeImmutable($dateDeFin['after']), Types::DATETIME_IMMUTABLE);
        }

        if (null !== $orderBy) {
            $this->throwIfUnexpectedOrderByValueCheckingTrait(
                $orderBy,
                [
                    'createdAt',
                    '-createdAt',
                    'updatedAt',
                    '-updatedAt',
                    'priority',
                    '-priority',
                ]
            );

            foreach ($orderBy as $orderByAttribute) {
                $interventionQueryBuilder->addOrderBy(
                    match ($orderByAttribute) {
                        'createdAt' => new OrderBy('dbCarlInterventionLike.createdAt', 'ASC'),
                        '-createdAt' => new OrderBy('dbCarlInterventionLike.createdAt', 'DESC'),
                        'updatedAt' => new OrderBy('dbCarlInterventionLike.updatedAt', 'ASC'),
                        '-updatedAt' => new OrderBy('dbCarlInterventionLike.updatedAt', 'DESC'),
                        'priority' => new OrderBy('dbCarlInterventionLike.workPriority', 'ASC'),
                        '-priority' => new OrderBy('dbCarlInterventionLike.workPriority', 'DESC'),
                        default => new OrderBy('dbCarlInterventionLike.createdAt', 'DESC')
                    }
                );
            }
        }

        $interventionQueryBuilder->getEntityManager()->clear(); // Si un appel a déjà été fait sur cet objet permet de le détacher afin de récupérer l'état actuel en db.

        return $interventionQueryBuilder
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery();
    }

    /**
     * @return array<InterventionStatusHistorised>
     */
    public function getStatusHistory(
        string $interventionId
    ): array {
        $dbCarlInterventionStatuses = $this->dbCarlInterventionRepository->find($interventionId)->getDbCarlInterventionStatuses();

        return DbCarlInterventionStatus::toInterventionStatusHistorisedArray($dbCarlInterventionStatuses->toArray());
    }

    public function addImageBase64(string $imageType, string $base64, Intervention $intervention, User $user, ?array $metadatas = null): string
    {
        $attachementId = $this->carlClientContext->getCarlClient()->persistFile(
            base64: $base64,
            name: $intervention->getTitle(),
            user: $user,
            metadatas: $metadatas
        );

        $carlUserId = $this->carlClientContext->getCarlClient()
            ->getUserIdFromEmail($user->getEmail());

        $photoDoctypeId = match ($imageType) {
            Intervention::IMAGE_CATEGORY_IMAGE_AVANT => $this->carlConfigurationInterventionContext->getCarlConfiguration()->getPhotoAvantInterventionDoctypeId(),
            Intervention::IMAGE_CATEGORY_IMAGE_APRES => $this->carlConfigurationInterventionContext->getCarlConfiguration()->getPhotoApresInterventionDoctypeId(),
            default => null
        };
        assert(!is_null($photoDoctypeId));

        $photoTitle = match ($imageType) {
            Intervention::IMAGE_CATEGORY_IMAGE_AVANT => 'photo préalable',
            Intervention::IMAGE_CATEGORY_IMAGE_APRES => 'photo à postériori',
            default => null
        };
        assert(!is_null($photoTitle));

        $carlLinkedUrlDocument = $this->carlClientContext->getCarlClient()->addDocumentToObject(
            attachementId: $attachementId,
            objectId: $intervention->getId(),
            objectJavaClass: 'com.carl.xnet.works.backend.bean.WOBean',
            userId: $carlUserId,
            docTypeId: $photoDoctypeId,
            documentTitle: $photoTitle
        );

        return $carlLinkedUrlDocument->getDocumentlinkId();
    }

    /**
     * @param string $imageId Attention l'imageId doit bien correspondre a l'id du documentLink de carl
     */
    public function deleteImage(string $imageType, string $imageId, Intervention $intervention): void
    {
        // Note: supprimer un documentlink supprime également le document associé
        $this->carlClientContext->getCarlClient()->deleteObject('documentlink', $imageId);
    }

    /**
     * S'assure que la dates de début et de fin de l'intervention contiennent les dates fournis dans le tableau en
     * modifiant l'intervention au besoin.
     *
     * @param array<\DateTimeInterface> $dates
     */
    public function enforceInterventionDatesContainDates(string $interventionId, array $dates): void
    {
        if (empty($dates)) {
            return;
        }

        sort($dates);

        $minimumDateTime = $dates[0];
        $maximumDateTime = end($dates);

        $fieldsOnlyWoBeginAndEndDate = [
            'reserve' => ['WO'],
            'wo' => ['WOEnd', 'WOBegin'],
        ];

        $reserveApiResource = $this->carlClientContext->getCarlClient()->getCarlObject(
            'wo',
            $interventionId,
            [],
            $fieldsOnlyWoBeginAndEndDate
        );

        $attributesToPatch = [];

        $woEndDateTime = new \DateTime($reserveApiResource->getAttribute('WOEnd'));
        if ($woEndDateTime < $maximumDateTime) {
            $attributesToPatch['WOEnd'] = $maximumDateTime->format(\DateTimeInterface::ATOM);
        }

        $woBeginDateTime = new \DateTime($reserveApiResource->getAttribute('WOBegin'));
        if ($woBeginDateTime > $minimumDateTime) {
            $attributesToPatch['WOBegin'] = $minimumDateTime->format(\DateTimeInterface::ATOM);
        }

        if (!empty($attributesToPatch)) {
            $this->carlClientContext->getCarlClient()->patchCarlObject(
                'wo',
                $interventionId,
                $attributesToPatch
            );
        }
    }

    /**
     * @param array<string> $silabStatuses
     *
     * @return array<string>
     */
    private function getCarlStatusCodesFromSilabStatusCodes(array $silabStatuses): array
    {
        return CarlClient::getCarlStatusCodesFromSilabStatusCodes($silabStatuses, CarlClient::SILAB_TO_CARL_INTERVENTION_STATUS_MAPPING);
    }

    public function getImageBlob(string $id): string
    {
        return $this->carlClientContext->getCarlClient()->getDocumentBlob($id);
    }
}
