<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use ApiPlatform\State\Pagination\PaginatorInterface;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\CreateIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\Entity\Status\InterventionStatus;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Exception\RuntimeException;
use App\Shared\User\Entity\User;

class InterventionRepository implements InterventionRepositoryInterface
{
    private InterventionRepositoryInterface $specificInterventionRepository;

    public function __construct(
        CarlInterventionRepository $carlInterventionRepository,// AMELIORATION: ne pas importer tous les repo, ça va péter le jour ou on met autre chose que du carl, cf. CarlClientContext
        // je pense que cette piste est intéressante: https://symfony.com/doc/current/service_container/factories.html (cette classe finalement ressemble pas mal à un InterventionRepositoryFactory)
        private InterventionServiceOfferContext $interventionServiceOfferContext
    ) {
        try {
            $this->specificInterventionRepository = match ($this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class) {
                CarlConfigurationIntervention::class => $carlInterventionRepository,
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class);
        }
    }

    public function transistionToStatus(string $id, InterventionStatus $status): InterventionStatus
    {
        return $this->specificInterventionRepository->transistionToStatus($id, $status);
    }

    public function update(string $id, CreateIntervention $entity, string $userEmail): Intervention
    {
        return $this->specificInterventionRepository->update($id, $entity, $userEmail);
    }

    public function find(mixed $id): Intervention
    {
        $intervention = $this->specificInterventionRepository->find($id);

        if (!is_null($intervention->getRelatedEquipement())) {
            $intervention->getRelatedEquipement()->setGmaoObjectViewUrl(
                $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()->getEquipementViewUrl($intervention->getRelatedEquipement())
            );
        }

        return $intervention->setGmaoObjectViewUrl(
            $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()->getInterventionViewUrl($intervention)
        );
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): PaginatorInterface
    {
        $criteria['interventionActionTypes'] ??= $this->getAllowedActionTypesIds();
        $orderBy ??= ['createdAt'];

        if ($this->areActionTypesOutsideServiceOfferScope($criteria['interventionActionTypes'])) {
            throw new RuntimeException("Vous avez demandé à voir des natures d'interventions qui sortent du cadre de cette offre de service : ".implode(', ', array_diff($criteria['interventionActionTypes'], $this->getAllowedActionTypesIds())));
        }

        $interventions = $this->specificInterventionRepository->findBy($criteria, $orderBy, $limit, $offset);
        foreach ($interventions as $intervention) {
            if (!is_null($intervention->getRelatedEquipement())) {
                $intervention->getRelatedEquipement()->setGmaoObjectViewUrl(
                    $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()->getEquipementViewUrl($intervention->getRelatedEquipement())
                );
            }

            $intervention->setGmaoObjectViewUrl(
                $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()->getInterventionViewUrl($intervention)
            );
        }

        return $interventions;
    }

    public function findCoordinatesBy(array $criteria): array
    {
        $criteria['interventionActionTypes'] ??= $this->getAllowedActionTypesIds();

        if ($this->areActionTypesOutsideServiceOfferScope($criteria['interventionActionTypes'])) {
            throw new RuntimeException("Vous avez demandé à voir des natures d'interventions qui sortent du cadre de cette offre de service : ".implode(', ', array_diff($criteria['interventionActionTypes'], $this->getAllowedActionTypesIds())));
        }

        return $this->specificInterventionRepository->findCoordinatesBy($criteria);
    }

    public function findAll(): PaginatorInterface
    {
        return $this->findBy([]);
    }

    public function findOneBy(array $criteria): Intervention
    {
        $interventions = $this->findBy($criteria);

        if (0 === count($interventions)) {
            throw new RuntimeException('Aucune intervention trouvée avec les critères donnés');
        }

        return iterator_to_array($interventions)[0];
    }

    public function save(CreateIntervention $entity, User $currentUser): Intervention
    {
        $hasCoordinates = !is_null($entity->getLatitude()) && !is_null($entity->getLongitude());

        if (!$hasCoordinates && is_null($entity->getRelatedEquipement())) {
            throw new RuntimeException("L'intervention que vous essayez de créer ne possède pas de coordonnées ou d'équipement de rattachement.");
        }

        return $this->find($this->specificInterventionRepository->save($entity, $currentUser)->getId());
    }

    public function getClassName()
    {
        return Intervention::class;
    }

    /**
     * @param array<string> $actionTypesIds
     */
    private function areActionTypesOutsideServiceOfferScope(array $actionTypesIds): bool
    {
        $requestedActionTypesThatAreOutsideOfServiceOfferScope =
            array_diff(
                $actionTypesIds,
                $this->getAllowedActionTypesIds()
            );

        return !empty(
            $requestedActionTypesThatAreOutsideOfServiceOfferScope
        );
    }

    public function isActionTypeOutsideServiceOfferScope(string $actionTypeId): bool
    {
        return $this->areActionTypesOutsideServiceOfferScope([$actionTypeId]);
    }

    /**
     * @return array<string>
     */
    public function getAllowedActionTypesIds(): array
    {
        return array_keys($this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()->getActionTypesMap());
    }

    public function addImageBase64(string $imageType, string $base64, Intervention $intervention, User $user, ?array $metadatas = null): string
    {
        $metadatas ??= [
            'serviceOffer:title' => $this->interventionServiceOfferContext->getServiceOffer()->getTitle(),
            'interventionServiceOffer:object:title' => $intervention->getTitle(),
            'interventionServiceOffer:object:description' => $intervention->getDescription(),
            'interventionServiceOffer:intervention:title' => $intervention->getTitle(),
            'interventionServiceOffer:intervention:code' => $intervention->getCode(),
            'interventionServiceOffer:intervention:description' => $intervention->getDescription(),
        ];

        return $this->specificInterventionRepository->addImageBase64(
            imageType: $imageType,
            base64: $base64,
            intervention: $intervention,
            user: $user,
            metadatas: $metadatas
        );
    }

    public function deleteImage(string $imageType, string $imageId, Intervention $intervention): void
    {
        // Note: on ne supprime jamais les images de la GED...
        $this->specificInterventionRepository->deleteImage(imageType: $imageType, imageId: $imageId, intervention: $intervention);
    }

    public function getImageBlob(string $id): string
    {
        return $this->specificInterventionRepository->getImageBlob(id: $id);
    }

    public function getStatusHistory(string $id): array
    {
        return $this->specificInterventionRepository->getStatusHistory($id);
    }
}
