<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use ApiPlatform\State\Pagination\PaginatorInterface;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\CreateIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\InterventionCoordinates;
use App\InterventionServiceOffer\Intervention\Entity\Status\InterventionStatus;
use App\InterventionServiceOffer\Intervention\Entity\Status\InterventionStatusHistorised;
use App\Shared\Doctrine\Persistence\PaginableObjectRepositoryInterface;
use App\Shared\User\Entity\User;

/**
 * @extends PaginableObjectRepositoryInterface<Intervention>
 */
interface InterventionRepositoryInterface extends PaginableObjectRepositoryInterface
{
    /**
     * @param mixed $id On attend un InterventionId
     *                  QUESTION_TECHNIQUE: les repos doctrine prefère renvoyer du nullable plutot que throw une exception, est-ce une pratique à suivre ?
     */
    public function find(mixed $id): Intervention;

    /**
     * @param array<string,mixed>    $criteria
     *                                         'relatedEquipement.id' : retourne toutes les interventions rattachées a l'équipement ou un de ses enfant
     * @param array<int,string>|null $orderBy
     *
     * @return PaginatorInterface<Intervention>
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): PaginatorInterface;

    /**
     * @param array<string,mixed> $criteria
     *                                      'relatedEquipement.id' : retourne toutes les interventions rattachées a l'équipement ou un de ses enfant
     *
     * @return array<InterventionCoordinates>
     */
    public function findCoordinatesBy(array $criteria): array;

    /**
     * @return PaginatorInterface<Intervention>
     */
    public function findAll(): PaginatorInterface;

    public function findOneBy(array $criteria): Intervention;

    public function save(CreateIntervention $entity, User $currentUser): Intervention;

    public function update(string $id, CreateIntervention $entity, string $userEmail): Intervention;

    public function transistionToStatus(string $id, InterventionStatus $status): InterventionStatus;

    /**
     * @param array<string,string|null>|null $metadatas amelioration: à l'avenir, implementer des metadataserializer ça pourrait être pas mal
     *
     * @return string l'id de l'image
     */
    public function addImageBase64(string $imageType, string $base64, Intervention $intervention, User $user, ?array $metadatas): string;

    public function deleteImage(string $imageType, string $imageId, Intervention $intervention): void;

    public function getImageBlob(string $id): string;

    /**
     * @return array<InterventionStatusHistorised>
     */
    public function getStatusHistory(string $id): array;
}
