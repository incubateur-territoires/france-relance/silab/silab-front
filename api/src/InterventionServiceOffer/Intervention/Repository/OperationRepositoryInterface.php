<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use App\InterventionServiceOffer\Intervention\Entity\Activite\Operation;
use Doctrine\Persistence\ObjectRepository;

interface OperationRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): Operation;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<Operation>
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array;

    /**
     * @return array<Operation>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): Operation;
}
