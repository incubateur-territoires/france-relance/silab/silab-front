<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\Intervention\Entity\Status\CreateInterventionStatus;
use App\InterventionServiceOffer\Intervention\Entity\Status\InterventionStatus;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\Shared\Exception\InsufficientRolesException;
use App\Shared\Exception\RuntimeException;
use App\Shared\User\Service\CurrentUserContextInterface;
use Psr\Log\LoggerInterface;

class AddStatusToInterventionProcessor implements ProcessorInterface
{
    public function __construct(
        private InterventionRepository $interventionRepository,
        private CurrentUserContextInterface $currentUserContext,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): InterventionStatus {
        $this->logger->debug('Changement de statut d\'une intervention',
            ['data' => json_encode($data), 'context' => $context, 'uriVariables' => $uriVariables]
        );

        assert($operation instanceof Post);
        assert($data instanceof CreateInterventionStatus);

        try {
            $this->interventionRepository->find($uriVariables['interventionId']);
        } catch (RuntimeException) {
            throw new InsufficientRolesException("Vous n'avez pas les droits suffisants pour changer le statut de cette intervention");
        }

        return $this->interventionRepository->transistionToStatus(
            $uriVariables['interventionId'],
            new InterventionStatus(code: $data->getCode(), createdBy: $this->currentUserContext->getCurrentUser()->getEmail())
        );
    }
}
