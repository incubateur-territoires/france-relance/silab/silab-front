<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteAction;
use App\InterventionServiceOffer\Intervention\Entity\Activite\CreateActiviteAction;
use App\InterventionServiceOffer\Intervention\Repository\ActiviteActionRepository;
use App\Shared\Exception\RuntimeException;
use Psr\Log\LoggerInterface;

final class CreateActiviteActionProcessor implements ProcessorInterface
{
    public function __construct(
        private LoggerInterface $logger,
        private ActiviteActionRepository $activiteActionRepository
    ) {
    }

    /**
     * @param CreateActiviteAction $data
     * @param array<mixed>         $uriVariables
     * @param array<mixed>         $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): ActiviteAction {
        $this->logger->debug('Création d\'une activité', ['data' => json_encode($data), 'context' => $context, 'uriVariables' => $uriVariables]);

        if (!($operation instanceof Post)) {
            throw new RuntimeException('Opération non supportée');
        }

        assert($data instanceof CreateActiviteAction);

        return $this->activiteActionRepository->save($data, $uriVariables['interventionId']);
    }
}
