<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\CreateIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\Exception\ForbiddenActionTypeException;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\InterventionServiceOffer\InterventionServiceOffer\Repository\InterventionServiceOfferRepository;
use App\Shared\Exception\RuntimeException;
use App\Shared\User\Service\CurrentUserContextInterface;
use Psr\Log\LoggerInterface;

final class CreateInterventionProcessor implements ProcessorInterface
{
    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository,
        private InterventionRepository $interventionRepository,
        private CurrentUserContextInterface $currentUserContext,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param CreateIntervention $data
     * @param array<mixed>       $uriVariables
     * @param array<mixed>       $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): Intervention {
        $this->logger->debug('Création d\'une intervention', ['data' => json_encode($data), 'context' => $context, 'uriVariables' => $uriVariables]);

        if (!($operation instanceof Post)) {
            throw new RuntimeException('Opération non supportée');
        }

        assert($data instanceof CreateIntervention);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $interventionActionType = $data->getActionType();

        // si on à donné une nature, on s'assure qu'elle soit dans la liste autorisée
        if (!is_null($interventionActionType)) {
            if ($this->interventionRepository->isActionTypeOutsideServiceOfferScope(actionTypeId: $interventionActionType->id)) {
                throw new ForbiddenActionTypeException(interventionServiceOffer: $interventionServiceOffer, actionTypeWithoutLabel: $data->getActionType());
            }
        }

        return $this->interventionRepository->save($data, $this->currentUserContext->getCurrentUser());
    }
}
