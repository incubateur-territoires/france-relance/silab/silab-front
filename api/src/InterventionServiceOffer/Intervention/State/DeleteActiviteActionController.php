<?php

namespace App\InterventionServiceOffer\Intervention\State;

use App\InterventionServiceOffer\GmaoActeur\Repository\GmaoActeurRepository;
use App\InterventionServiceOffer\Intervention\Repository\ActiviteActionRepository;
use App\Shared\Exception\InsufficientRolesException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

// QUESTION_TECHNIQUE: Comment faire avec api-platform, si on utilise une operation delete on a une erreur similaire a https://github.com/api-platform/core/issues/5827
#[Route('/api/intervention-service-offers/{serviceOfferId}/activites-action/{id}',
    name: 'delete_activite_action', methods: ['DELETE'])]
class DeleteActiviteActionController extends AbstractController
{
    public function __invoke(
        ActiviteActionRepository $activiteActionRepository,
        Security $security,
        GmaoActeurRepository $gmaoActeurRepository,
        string $id,
    ): Response {
        // Securité :
        if ($security->isGranted('SERVICEOFFER_ROLE_INTERVENTION_SUPPRIMER_ACTIVITE'
        )) {
            throw new InsufficientRolesException("Vous n'avez pas les droits suffisants pour supprimer cette demande d'intervention");
        }

        $curentGmaoActeur = $gmaoActeurRepository->getMyself();
        $activitéCible = $activiteActionRepository->find($id);

        if ($activitéCible->getIsPlanifie()) {
            throw new InsufficientRolesException('Vous ne pouvez pas supprimer une activité issue d\'une planification');
        }
        if (
            $activitéCible->getCreatedBy()?->getId() === $curentGmaoActeur->getId()
            || $activitéCible->getIntervenant()?->getId() === $curentGmaoActeur->getId()
        ) {
            $activiteActionRepository->delete($id);

            return new Response(status: 204);
        } else {
            throw new InsufficientRolesException("Vous ne pouvez pas supprimer une activité dont vous êtes ni le créateur ni l'intervenant");
        }
    }
}
