<?php

namespace App\InterventionServiceOffer\Intervention\State;

use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\Shared\Exception\InsufficientRolesException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

// QUESTION_TECHNIQUE: Comment faire avec api-platform, si on utilise une operation delete on a une erreur similaire a https://github.com/api-platform/core/issues/5827
#[Route(
    '/api/intervention-service-offers/{serviceOfferId}/interventions/{interventionId}/{imageType}/{id}',
    name: 'interventionServiceOffer_delete_intervention_image',
    requirements: ['imageType' => Intervention::IMAGE_CATEGORY_IMAGE_AVANT.'|'.Intervention::IMAGE_CATEGORY_IMAGE_APRES],
    methods: ['DELETE']
)]
class DeleteImageController extends AbstractController
{
    public function __invoke(
        string $interventionId,
        string $imageType,
        string $id,
        InterventionRepository $interventionRepository,
        Security $security
    ): Response {
        // Securité :
        switch ($imageType) {
            case Intervention::IMAGE_CATEGORY_IMAGE_AVANT:
                if (!$security->isGranted(
                    'SERVICEOFFER_ROLE_INTERVENTION_SUPPRIMER_IMAGE_AVANT_INTERVENTION'
                )) {
                    throw new InsufficientRolesException("Vous n'avez pas les droits suffisants pour supprimer une image d'avant l'intervention");
                }
                break;
            case Intervention::IMAGE_CATEGORY_IMAGE_APRES:
                if (!$security->isGranted(
                    'SERVICEOFFER_ROLE_INTERVENTION_SUPPRIMER_IMAGE_APRES_INTERVENTION'
                )) {
                    throw new InsufficientRolesException("Vous n'avez pas les droits suffisants pour supprimer une image d'après l'intervention");
                }
                break;
            default:
                throw $this->createNotFoundException('Type d\'image inconnu.');
        }

        $interventionRepository->deleteImage(
            imageType: $imageType,
            imageId: $id,
            intervention: $interventionRepository->find($interventionId)
        );

        return new Response(status: 204);
    }
}
