<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation as ApiPlatformOperation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteAction;
use App\InterventionServiceOffer\Intervention\Repository\ActiviteActionRepository;

final class GetCollectionActiviteActionProvider implements ProviderInterface
{
    public function __construct(
        private ActiviteActionRepository $activiteActionRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<ActiviteAction>
     */
    public function provide(ApiPlatformOperation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $criteria = $context['filters'] ?? [];

        $criteria['intervention.id'] = [$uriVariables['interventionId']];

        foreach ($criteria as $key => $value) {
            if (is_string($value)) {
                $criteria[$key] = [$value];
            }
        }

        return $this->activiteActionRepository->findBy($criteria);
    }
}
