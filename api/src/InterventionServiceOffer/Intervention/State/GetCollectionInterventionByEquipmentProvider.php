<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\Pagination\PaginatorInterface;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;

final class GetCollectionInterventionByEquipmentProvider implements ProviderInterface
{
    public function __construct(
        private InterventionRepository $interventionRepository,
    ) {
    }

    /**
     * Fournit au endpoint getAllIntervention les datas provenants du connecteur Carl.
     *
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return PaginatorInterface<Intervention>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): PaginatorInterface
    {
        assert($operation instanceof CollectionOperationInterface);

        $criteria = $context['filters'] ?? [];

        foreach ($criteria as $key => $value) {
            if (is_string($value)) {
                $criteria[$key] = [$value];
            }
        }

        if (isset($criteria['orderBy'])) {
            $orderBy = $criteria['orderBy'];
            unset($criteria['orderBy']);
        } else {
            $orderBy = null;
        }

        $criteria['relatedEquipement.id'] = [$uriVariables['equipementId']];

        return $this->interventionRepository->findBy($criteria, $orderBy);
    }
}
