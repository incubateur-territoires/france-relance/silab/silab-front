<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\InterventionCoordinates;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\InterventionServiceOffer\InterventionServiceOffer\Repository\InterventionServiceOfferRepository;

final class GetCollectionInterventionCoordinatesProvider implements ProviderInterface
{
    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository,
        private InterventionRepository $interventionRepository,
    ) {
    }

    /**
     * Fournit au endpoint getAllIntervention les datas provenants du connecteur Carl.
     *
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<InterventionCoordinates>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);

        $criteria = $context['filters'] ?? [];

        foreach ($criteria as $key => $value) {
            if (is_string($value)) {
                $criteria[$key] = [$value];
            }
        }

        $lOffreDeServicePorteSurDesEquipements = !empty($interventionServiceOffer->getAvailableEquipementsIds());
        $onAFiltréSurDesEquipements = array_key_exists('relatedEquipement.id', $criteria) | array_key_exists('equipementRacine.id', $criteria);

        if ($lOffreDeServicePorteSurDesEquipements) {
            // on restreint les équipements à ceux définits dans l'ods
            // ça posait question sur la gestion des favories
            // if ($onAFiltréSurDesEquipements) {
            //     $criteria['relatedEquipement.id'] = array_intersect($criteria['relatedEquipement.id'], $interventionServiceOffer->getAvailableEquipementsIds());
            // }
            // par défault on restreint aux equipements de l'ODS
            if (!$onAFiltréSurDesEquipements) {
                $criteria['equipementRacine.id'] = $interventionServiceOffer->getAvailableEquipementsIds(); // amélioration: attention aux enfants, on devrait ptet prendre les enfants aussi !
            }
        }

        return $this->interventionRepository->findCoordinatesBy($criteria);
    }
}
