<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\Pagination\Pagination;
use ApiPlatform\State\Pagination\PaginatorInterface;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;

final class GetCollectionInterventionProvider implements ProviderInterface
{
    public function __construct(
        private InterventionRepository $interventionRepository,
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private readonly Pagination $pagination,
    ) {
    }

    /**
     * Fournit au endpoint getAllIntervention les datas provenants du connecteur Carl.
     *
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return PaginatorInterface<Intervention>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): PaginatorInterface
    {
        assert($operation instanceof CollectionOperationInterface);

        $criteria = $context['filters'] ?? [];

        foreach ($criteria as $key => $value) {
            if (is_string($value)) {
                $criteria[$key] = [$value];
            }
        }

        if (isset($criteria['orderBy'])) {
            $orderBy = $criteria['orderBy'];
            unset($criteria['orderBy']);
        } else {
            $orderBy = null;
        }

        $lOffreDeServicePorteSurDesEquipements = !empty(
            $this->interventionServiceOfferContext->getServiceOffer()->getAvailableEquipementsIds()
        );
        $onAFiltréSurDesEquipements = array_key_exists('relatedEquipement.id', $criteria) | array_key_exists('equipementRacine.id', $criteria);

        if ($lOffreDeServicePorteSurDesEquipements) {
            // on restreint les équipements à ceux définits dans l'ods
            // ça posait question sur la gestion des favories
            // if ($onAFiltréSurDesEquipements) {
            //     $criteria['relatedEquipement.id'] = array_intersect($criteria['relatedEquipement.id'], $interventionServiceOffer->getAvailableEquipementsIds());
            // }
            // par défault on restreint aux equipements de l'ODS
            if (!$onAFiltréSurDesEquipements) {
                $criteria['equipementRacine.id'] =
                $this->interventionServiceOfferContext->getServiceOffer()->getAvailableEquipementsIds(); // amélioration: attention aux enfants, on devrait ptet prendre les enfants aussi !
            }
        }

        // from : https://api-platform.com/docs/guides/custom-pagination/
        [, $offset , $limit] = $this->pagination->getPagination($operation, $context);
        unset($criteria['page']);

        return $this->interventionRepository->findBy($criteria, $orderBy, $limit, $offset);
    }
}
