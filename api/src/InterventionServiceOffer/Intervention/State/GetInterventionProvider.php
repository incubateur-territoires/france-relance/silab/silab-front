<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;

final class GetInterventionProvider implements ProviderInterface
{
    public function __construct(
        private InterventionRepository $interventionRepository,
    ) {
    }

    /**
     * Fournit au endpoint getAllIntervention les datas provenants du connecteur.
     *
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<Intervention>|Intervention
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array|Intervention
    {
        assert($operation instanceof Get);

        return $this->interventionRepository->find($uriVariables['id']);
    }
}
