<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Intervention\Entity\Status\InterventionStatusHistorised;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;

final class GetInterventionStatusHistoryProvider implements ProviderInterface
{
    public function __construct(
        private InterventionRepository $interventionRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<InterventionStatusHistorised>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        // amelioration: retourner un 404 si pas d'intervention ?
        return $this->interventionRepository->getStatusHistory($uriVariables['interventionId']);
    }
}
