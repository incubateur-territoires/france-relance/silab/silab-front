<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Operation as ApiPlatformOperation;
use ApiPlatform\Metadata\Put;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteAction;
use App\InterventionServiceOffer\Intervention\Entity\Activite\UpdateActiviteAction;
use App\InterventionServiceOffer\Intervention\Repository\ActiviteActionRepository;

final class UpdateActiviteActionProcessor implements ProcessorInterface
{
    public function __construct(
        private ActiviteActionRepository $activiteActionRepository
    ) {
    }

    /**
     * @param UpdateActiviteAction $data
     * @param array<mixed>         $uriVariables
     * @param array<mixed>         $context
     */
    public function process(
        mixed $data,
        ApiPlatformOperation $operation,
        array $uriVariables = [],
        array $context = []
    ): ActiviteAction {
        assert($operation instanceof Put, 'Opération non supportée');

        assert($data instanceof UpdateActiviteAction);

        $activité = $this->activiteActionRepository->find($uriVariables['id']);

        // Note: à ce jour, nous n'authorisons que la modification de la date de réalisation
        $activité->setDateDeDebut($data->getDateDeDebut());

        return
            $this->activiteActionRepository->update(
                $uriVariables['id'],
                $activité
            );
    }
}
