<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Operation as ApiPlatformOperation;
use ApiPlatform\Metadata\Put;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\Intervention\Entity\Activite\ActiviteDiagnostique;
use App\InterventionServiceOffer\Intervention\Entity\Activite\UpdateActiviteDiagnostique;
use App\InterventionServiceOffer\Intervention\Repository\ActiviteDiagnostiqueRepository;

final class UpdateActiviteDiagnostiqueProcessor implements ProcessorInterface
{
    public function __construct(
        private ActiviteDiagnostiqueRepository $activiteDiagnostiqueRepository
    ) {
    }

    /**
     * @param UpdateActiviteDiagnostique $data
     * @param array<mixed>               $uriVariables
     * @param array<mixed>               $context
     */
    public function process(
        mixed $data,
        ApiPlatformOperation $operation,
        array $uriVariables = [],
        array $context = []
    ): ActiviteDiagnostique {
        assert($operation instanceof Put, 'Opération non supportée');

        assert($data instanceof UpdateActiviteDiagnostique);

        return
            $this->activiteDiagnostiqueRepository->update(
                $uriVariables['id'],
                $data
            );
    }
}
