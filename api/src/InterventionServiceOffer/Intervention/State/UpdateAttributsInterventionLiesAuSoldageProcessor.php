<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Put;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\CreateIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\UpdateInterventionPourSoldage;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\Shared\ApiPlatform\Entity\IdentifiableRelation;
use App\Shared\User\Service\CurrentUserContextInterface;

final class UpdateAttributsInterventionLiesAuSoldageProcessor implements ProcessorInterface
{
    public function __construct(
        private InterventionRepository $interventionRepository,
        private CurrentUserContextInterface $currentUserContext,
    ) {
    }

    /**
     * @param UpdateInterventionPourSoldage $data
     * @param array<mixed>                  $uriVariables
     * @param array<mixed>                  $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): Intervention {
        assert($operation instanceof Put, 'Opération non supportée');

        assert($data instanceof UpdateInterventionPourSoldage);

        $intervention = $this->interventionRepository->find($uriVariables['id']);
        $interventionMiseAJour = new CreateIntervention(
            materielConsomme: $data->getMaterielConsomme(),
            dateDeDebut: $data->getDateDeDebut(),
            dateDeFin: $data->getDateDeFin(),
            priority: $intervention->getPriority(),
            title: $intervention->getTitle(),
            description: $intervention->getDescription(),
            actionType: $intervention->getActionType(),
            latitude: $intervention->getLatitude(),
            longitude: $intervention->getLongitude(),
            relatedEquipement: new IdentifiableRelation(id: $intervention->getRelatedEquipement()->getId()),
        );

        return $this->interventionRepository->update($uriVariables['id'], $interventionMiseAJour, $this->currentUserContext->getCurrentUser()->getEmail());
    }
}
