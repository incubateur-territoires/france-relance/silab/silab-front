<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Put;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\CreateIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\Shared\User\Service\CurrentUserContextInterface;

final class UpdateInterventionProcessor implements ProcessorInterface
{
    public function __construct(
        private InterventionRepository $interventionRepository,
        private CurrentUserContextInterface $currentUserContext,
    ) {
    }

    /**
     * @param CreateIntervention $data
     * @param array<mixed>       $uriVariables
     * @param array<mixed>       $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): Intervention {
        assert($operation instanceof Put, 'Opération non supportée');

        assert($data instanceof CreateIntervention);

        return $this->interventionRepository->update($uriVariables['id'], $data, $this->currentUserContext->getCurrentUser()->getEmail());
    }
}
