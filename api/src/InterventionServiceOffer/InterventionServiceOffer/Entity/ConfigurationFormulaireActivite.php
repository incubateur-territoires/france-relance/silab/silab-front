<?php

namespace App\InterventionServiceOffer\InterventionServiceOffer\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[
    Groups([
        InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS,
        InterventionServiceOffer::GROUP_CREER_ODS,
        InterventionServiceOffer::GROUP_AFFICHER_LISTE_ODS,
    ])
]
#[ORM\Embeddable]
class ConfigurationFormulaireActivite
{
    public const DEFAULT_VALUE_HEURE_PAR_DEFAUT_EN_MINNUTE = 8 * 60;

    public function __construct(
        #[ORM\Column(type: 'string', enumType: DateDeDébut::class, options: ['default' => 'automatique'])]
        private DateDeDébut $dateDeDebut = DateDeDébut::Automatique,
        #[ORM\Column(options: ['default' => self::DEFAULT_VALUE_HEURE_PAR_DEFAUT_EN_MINNUTE])]
        private int $heureParDefautEnMinute = self::DEFAULT_VALUE_HEURE_PAR_DEFAUT_EN_MINNUTE
    ) {
    }

    public function getDateDeDebut(): DateDeDébut
    {
        return $this->dateDeDebut;
    }

    public function setDateDeDebut(DateDeDébut $dateDeDebut): self
    {
        $this->dateDeDebut = $dateDeDebut;

        return $this;
    }

    public function getHeureParDefautEnMinute(): int
    {
        return $this->heureParDefautEnMinute;
    }

    public function setHeureParDefautEnMinute(int $heureParDefautEnMinute): self
    {
        $this->heureParDefautEnMinute = $heureParDefautEnMinute;

        return $this;
    }
}

enum DateDeDébut: string
{
    case Jour = 'jour';
    case Heure = 'heure';
    case Automatique = 'automatique'; // date de début  =  now - durée
}
