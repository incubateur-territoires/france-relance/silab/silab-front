<?php

namespace App\InterventionServiceOffer\InterventionServiceOffer\Entity;

use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\InterventionServiceOffer\Gmao\Entity\GmaoConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Repository\InterventionServiceOfferRepository;
use App\ServiceOffer\Role\ValueObject\Role;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use App\Shared\Exception\RuntimeException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[Get(
    uriTemplate: 'intervention-service-offers/{id}',
    security: "is_granted('SERVICEOFFER_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE')",
    normalizationContext: ['groups' => [ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS]],
)]
#[GetCollection(
    security: "is_granted('ROLE_CONSULTER_OFFRE_DE_SERVICE')",
    normalizationContext: ['groups' => [ServiceOffer::GROUP_AFFICHER_LISTE_ODS, InterventionServiceOffer::GROUP_AFFICHER_LISTE_ODS]],
    order: ['title' => 'ASC']
)]
#[Get()]
#[Post(
    security: "is_granted('ROLE_CREER_OFFRE_DE_SERVICE')",
    denormalizationContext: ['groups' => [ServiceOffer::GROUP_CREER_ODS, InterventionServiceOffer::GROUP_CREER_ODS]],
    normalizationContext: ['groups' => [ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS]]
)]
#[Put(
    uriTemplate: 'intervention-service-offers/{id}',
    security: "is_granted('ROLE_MODIFIER_OFFRE_DE_SERVICE')",
    denormalizationContext: ['groups' => [ServiceOffer::GROUP_CREER_ODS, InterventionServiceOffer::GROUP_CREER_ODS]],
    normalizationContext: ['groups' => [ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS]]
)]
#[Delete(
    uriTemplate: 'intervention-service-offers/{id}',
    security: "is_granted('ROLE_SUPPRIMER_OFFRE_DE_SERVICE')",
)]
#[Groups([
    InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS,
    InterventionServiceOffer::GROUP_CREER_ODS,
    InterventionServiceOffer::GROUP_AFFICHER_LISTE_ODS])
]
#[ORM\Entity(repositoryClass: InterventionServiceOfferRepository::class)]
class InterventionServiceOffer extends ServiceOffer
{
    public const GROUP_AFFICHER_DETAILS_ODS = "Afficher les détails d'un offre de service intervention";
    public const GROUP_AFFICHER_LISTE_ODS = 'Afficher la liste des offres de service intervention';
    public const GROUP_CREER_ODS = 'Créer une offre de service intervention';

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?GmaoConfigurationIntervention $gmaoConfiguration;

    /** @var array<string> $availableEquipementsIds */
    #[ORM\Column(type: 'json', options: ['default' => '[]'])]
    private array $availableEquipementsIds = [];

    #[ORM\Column(options: ['default' => false])]
    private bool $gestionRecompletementActif = false;

    #[ORM\Embedded(class: ConfigurationFormulaireActivite::class)]
    private ConfigurationFormulaireActivite $configurationFormulaireActivite;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $modeleDeDescriptionPourDemandeIntervention = null;

    #[ORM\Column(options: ['default' => false])]
    private bool $gestionDatesSouhaiteesPourDemandeInterventionActif = false;

    #[ORM\Column(options: ['default' => false])]
    private bool $gestionDatesPourInterventionActif = false;

    public function __construct()
    {
        $this->configurationFormulaireActivite = new ConfigurationFormulaireActivite();
    }

    public function getTemplate(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return array<Role>
     */
    public function getAvailableRoles(): array
    {
        $availableRoles = [
            new Role(value: 'SERVICEOFFER_'.$this->getId().'_ROLE_INTERVENTION_SUPERVISEUR', libelle: 'Superviseur', description: "Peut réaliser l'ensemble des tâches des déclarants, intervenants et demandeurs"),
            new Role(value: 'SERVICEOFFER_'.$this->getId().'_ROLE_INTERVENTION_DECLARANT', libelle: 'Déclarant', description: 'Peut consulter et créer directement des interventions'),
            new Role(value: 'SERVICEOFFER_'.$this->getId().'_ROLE_INTERVENTION_INTERVENANT', libelle: 'Intervenant', description: 'Peut consulter et solder les interventions'),
            new Role(value: 'SERVICEOFFER_'.$this->getId().'_ROLE_INTERVENTION_DEMANDEUR', libelle: 'Demandeur', description: "Peut faire des demandes d'intervention"),
            new Role(value: 'SERVICEOFFER_'.$this->getId().'_ROLE_INTERVENTION_OBSERVATEUR', libelle: 'Observateur', description: 'Peut consulter les interventions et les demandes'),
        ];

        return array_merge(parent::getAvailableRoles(), $availableRoles);
    }

    public function getGmaoConfiguration(): ?GmaoConfigurationIntervention
    {
        return $this->gmaoConfiguration;
    }

    public function setGmaoConfiguration(?GmaoConfigurationIntervention $gmaoConfiguration): self
    {
        $this->gmaoConfiguration = $gmaoConfiguration;

        return $this;
    }

    public function getConfigurationFormulaireActivite(): ConfigurationFormulaireActivite
    {
        return $this->configurationFormulaireActivite;
    }

    public function setConfigurationFormulaireActivite(ConfigurationFormulaireActivite $configurationFormulaireActivite): self
    {
        $this->configurationFormulaireActivite = $configurationFormulaireActivite;

        return $this;
    }

    /**
     * @return array<string,string> un tableau associatif du type "libellé de la nature"=>"id de la nature"
     */
    public function getActionTypesMap(): array
    {
        if (is_null($this->gmaoConfiguration)) {
            throw new RuntimeException('Veuillez renseigner une configuration GMAO.');
        }

        return $this->gmaoConfiguration->getActionTypesMap();
    }

    /**
     * @return array<string>
     */
    public function getAvailableEquipementsIds(): array
    {
        return $this->availableEquipementsIds;
    }

    /**
     * @param array<string> $equipements
     */
    public function setAvailableEquipementsIds(array $equipements): self
    {
        $this->availableEquipementsIds = array_values($equipements);

        return $this;
    }

    public function getGestionRecompletementActif(): bool
    {
        return $this->gestionRecompletementActif;
    }

    public function setGestionRecompletementActif(bool $gestionRecompletementActif): static
    {
        $this->gestionRecompletementActif = $gestionRecompletementActif;

        return $this;
    }

    public function getModeleDeDescriptionPourDemandeIntervention(): ?string
    {
        return $this->modeleDeDescriptionPourDemandeIntervention;
    }

    public function setModeleDeDescriptionPourDemandeIntervention(?string $modeleDeDescriptionPourDemandeIntervention): self
    {
        $this->modeleDeDescriptionPourDemandeIntervention = $modeleDeDescriptionPourDemandeIntervention;

        return $this;
    }

    public function getGestionDatesSouhaiteesPourDemandeInterventionActif(): bool
    {
        return $this->gestionDatesSouhaiteesPourDemandeInterventionActif;
    }

    public function setGestionDatesSouhaiteesPourDemandeInterventionActif(bool $gestionDatesSouhaiteesPourDemandeInterventionActif): static
    {
        $this->gestionDatesSouhaiteesPourDemandeInterventionActif = $gestionDatesSouhaiteesPourDemandeInterventionActif;

        return $this;
    }

    public function getGestionDatesPourInterventionActif(): bool
    {
        return $this->gestionDatesPourInterventionActif;
    }

    public function setGestionDatesPourInterventionActif(bool $gestionDatePourInterventionActif): static
    {
        $this->gestionDatesPourInterventionActif = $gestionDatePourInterventionActif;

        return $this;
    }
}
