<?php

namespace App\InterventionServiceOffer\InterventionServiceOffer\Repository;

use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<\App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer>
 *
 * @method InterventionServiceOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method InterventionServiceOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method InterventionServiceOffer[]    findAll()
 * @method InterventionServiceOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterventionServiceOfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InterventionServiceOffer::class);
    }

    public function save(InterventionServiceOffer $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(InterventionServiceOffer $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    //    /**
    //     * @return Intervention[] Returns an array of Intervention objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('i.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Intervention
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
