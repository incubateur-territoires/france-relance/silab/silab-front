<?php

namespace App\InterventionServiceOffer\InterventionServiceOffer\Service;

use App\InterventionServiceOffer\Carl\Exception\NoInterventionServiceOfferContextException;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\InterventionServiceOffer\InterventionServiceOffer\Repository\InterventionServiceOfferRepository;
use App\ServiceOffer\ServiceOffer\Exception\NoServiceOfferContextException;
use App\ServiceOffer\ServiceOffer\Service\ServiceOfferContextInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class InterventionServiceOfferContext implements ServiceOfferContextInterface
{
    private ?InterventionServiceOffer $serviceOffer = null;
    private bool $hasCachedServiceOffer = false;

    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository,
        private RequestStack $requestStack,
    ) {
    }

    public function getServiceOfferId(): int
    {
        if ($this->hasCachedServiceOffer && !is_null($this->serviceOffer)) {
            return $this->serviceOffer->getId();
        }

        $serviceOfferId = $this->requestStack->getCurrentRequest()?->get('serviceOfferId');

        if (is_null($serviceOfferId)) {
            throw new NoServiceOfferContextException();
        }

        return $serviceOfferId;
    }

    public function getServiceOffer(): InterventionServiceOffer
    {
        if ($this->hasCachedServiceOffer) {
            return $this->serviceOffer;
        }

        $this->serviceOffer = $this->interventionServiceOfferRepository->find(
            $this->getServiceOfferId()
        );

        if (is_null($this->serviceOffer)) {
            throw new NoInterventionServiceOfferContextException();
        }

        $this->hasCachedServiceOffer = true;

        return $this->serviceOffer;
    }

    public function getServiceOfferSafe(): ?InterventionServiceOffer
    {
        try {
            return $this->getServiceOffer();
        } catch (NoInterventionServiceOfferContextException|NoServiceOfferContextException $e) {
            // ne rien faire
        }

        return null;
    }
}
