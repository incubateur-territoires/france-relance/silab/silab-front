<?php

namespace App\InterventionServiceOffer\SearchEntity\Entity\SearchEntity;

class SearchEntity
{
    public function __construct(
        private SearchEntityType $type,
        private string $id,
        private ?string $code,
        private ?string $label
    ) {
    }

    public function getType(): SearchEntityType
    {
        return $this->type;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }
}
