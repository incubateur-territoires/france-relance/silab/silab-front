<?php

namespace App\InterventionServiceOffer\SearchEntity\Entity\SearchEntity;

use ApiPlatform\Metadata\GetCollection;
use App\InterventionServiceOffer\SearchEntity\State\GetCollectionSearchEntityProvider;

#[GetCollection(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/search-entity/search',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS') or is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_INTERVENTIONS') or is_granted('SERVICEOFFER_ROLE_INTERVENTION_CONSULTER_EQUIPEMENT')",
    output: SearchEntity::class,
    provider: GetCollectionSearchEntityProvider::class,
)]
class SearchEntityApiResource
{
}
