<?php

namespace App\InterventionServiceOffer\SearchEntity\Entity\SearchEntity;

enum SearchEntityType: string
{
    case DemandeIntervention = 'demandes';
    case Intervention = 'interventions';
    case Equipement = 'équipements';
}
