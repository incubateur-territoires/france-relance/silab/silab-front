<?php

namespace App\InterventionServiceOffer\SearchEntity\Repository;

use App\InterventionServiceOffer\Carl\Entity\DbCarlSearchEntity;
use App\InterventionServiceOffer\SearchEntity\Entity\SearchEntity\SearchEntity;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlSearchEntityRepository;
use App\Shared\Exception\RuntimeException;

class CarlSearchEntityRepository implements SearchEntityRepositoryInterface
{
    public function __construct(
        private DbCarlSearchEntityRepository $dbCarlSearchEntityRepository
    ) {
    }

    /**
     * @return array<SearchEntity>
     */
    public function search(string $searchQuery): array
    {
        return DbCarlSearchEntity::toSearchEntityArray($this->dbCarlSearchEntityRepository->search($searchQuery));
    }

    public function find($id)
    {
        throw new RuntimeException('Méthode non implémentée, repostory dédiée à la recherche');
    }

    public function findAll()
    {
        throw new RuntimeException('Méthode non implémentée, repostory dédiée à la recherche');
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null)
    {
        throw new RuntimeException('Méthode non implémentée, repostory dédiée à la recherche');
    }

    public function findOneBy(array $criteria)
    {
        throw new RuntimeException('Méthode non implémentée, repostory dédiée à la recherche');
    }

    public function getClassName()
    {
        throw new RuntimeException('Méthode non implémentée, repostory dédiée à la recherche');
    }
}
