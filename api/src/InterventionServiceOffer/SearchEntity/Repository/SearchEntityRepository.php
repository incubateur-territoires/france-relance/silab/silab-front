<?php

namespace App\InterventionServiceOffer\SearchEntity\Repository;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Exception\RuntimeException;

class SearchEntityRepository implements SearchEntityRepositoryInterface
{
    private SearchEntityRepositoryInterface $specificSearchEntityRepository;

    public function __construct(
        InterventionServiceOfferContext $interventionServiceOfferContext,
        CarlSearchEntityRepository $carlSearchEntityRepository
    ) {
        try {
            $this->specificSearchEntityRepository = match ($interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class) {
                CarlConfigurationIntervention::class => $carlSearchEntityRepository
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration()::class);
        }
    }

    public function search(string $searchQuery): array
    {
        return $this->specificSearchEntityRepository->search($searchQuery);
    }

    public function find($id)
    {
        throw new RuntimeException('Méthode non implémentée, repostory dédiée à la recherche');
    }

    public function findAll()
    {
        throw new RuntimeException('Méthode non implémentée, repostory dédiée à la recherche');
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null)
    {
        throw new RuntimeException('Méthode non implémentée, repostory dédiée à la recherche');
    }

    public function findOneBy(array $criteria)
    {
        throw new RuntimeException('Méthode non implémentée, repostory dédiée à la recherche');
    }

    public function getClassName()
    {
        throw new RuntimeException('Méthode non implémentée, repostory dédiée à la recherche');
    }
}
