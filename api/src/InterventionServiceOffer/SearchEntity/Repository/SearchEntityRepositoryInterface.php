<?php

namespace App\InterventionServiceOffer\SearchEntity\Repository;

use App\InterventionServiceOffer\SearchEntity\Entity\SearchEntity\SearchEntity;
use Doctrine\Persistence\ObjectRepository;

interface SearchEntityRepositoryInterface extends ObjectRepository
{
    /**
     * @return array<SearchEntity>
     */
    public function search(
        string $searchQuery,
    ): array;
}
