<?php

namespace App\InterventionServiceOffer\SearchEntity\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\SearchEntity\Entity\SearchEntity\SearchEntity;
use App\InterventionServiceOffer\SearchEntity\Repository\SearchEntityRepository;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;

final class GetCollectionSearchEntityProvider implements ProviderInterface
{
    use UnexpectedCriteriaCheckingTrait;

    public function __construct(
        private SearchEntityRepository $searchEntityRepository,
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<SearchEntity>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        $criteria = $context['filters'] ?? [];
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'q',
            ]
        );

        $searchQuery = $criteria['q'];

        return $this->searchEntityRepository->search($searchQuery);
    }
}
