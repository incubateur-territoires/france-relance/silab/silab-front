<?php

namespace App\LogisticServiceOffer\Article\Entity;

use ApiPlatform\Metadata\GetCollection;
use App\LogisticServiceOffer\Article\State\GetCollectionArticleProvider;
use Symfony\Component\Serializer\Annotation\Groups;

#[GetCollection(
    uriTemplate: '/logistic-service-offers/{serviceOfferId}/articles',
    uriVariables: ['serviceOfferId' => 'serviceOfferId'],
    security: "is_granted('SERVICEOFFER_ROLE_LOGISTIQUE_CONSULTER_ARTICLES')",
    provider: GetCollectionArticleProvider::class,
    normalizationContext: ['groups' => [self::GROUP_AFFICHER_ARTICLE]],
)]
class ArticleApiResource
{
    public const GROUP_AFFICHER_ARTICLE = 'Afficher informations d\'un article';

    public function __construct(
        #[Groups([self::GROUP_AFFICHER_ARTICLE])]
        private string $id,
        #[Groups([self::GROUP_AFFICHER_ARTICLE])]
        private ?string $code = null,
        #[Groups([self::GROUP_AFFICHER_ARTICLE])]
        private ?string $titre = null,
        /** @var array<Lot> */
        #[Groups([self::GROUP_AFFICHER_ARTICLE])]
        private ?array $lots = null,
        #[Groups([self::GROUP_AFFICHER_ARTICLE])]
        private ?string $unité = null
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    /**
     * @return array<Lot>
     */
    public function getLots(): ?array
    {
        return $this->lots;
    }

    public function getUnité(): ?string
    {
        return $this->unité;
    }
}
