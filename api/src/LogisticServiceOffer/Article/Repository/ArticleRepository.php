<?php

namespace App\LogisticServiceOffer\Article\Repository;

use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\LogisticServiceOffer\Gmao\Entity\GmaoConfigurationLogistique;
use App\Shared\Exception\RuntimeException;

class ArticleRepository implements ArticleRepositoryInterface
{
    private ArticleRepositoryInterface $specificArticleRepository;

    public function __construct(
        protected GmaoConfigurationLogistique $gmaoConfigurationLogistique
    ) {
        try {
            $this->specificArticleRepository = match ($gmaoConfigurationLogistique::class) {
                CarlConfigurationLogistique::class => new CarlArticleRepository(
                    $gmaoConfigurationLogistique
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfigurationLogistique::class);
        }
    }

    public function find($id): Article
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->specificArticleRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): Article
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function getClassName()
    {
        return Article::class;
    }
}
