<?php

namespace App\LogisticServiceOffer\Article\Repository;

use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Article\Entity\Lot;
use App\LogisticServiceOffer\Emplacement\Entity\Emplacement;
use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Exception\RuntimeException;
use App\Shared\Helpers;
use App\Shared\JsonApi\JsonApiResource;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlArticleRepository implements ArticleRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;

    public function __construct(
        private CarlConfigurationLogistique $carlConfigurationLogistique
    ) {
    }

    public function find($id): Article
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }
        if (null !== $orderBy) {
            throw new RuntimeException('Le paramètre orderBy n\'est pas encore implémenté');
        }
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'code',
                'warehouse.id',
            ]
        );

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $filters = [];

        $code = $propertyAccessor->getValue($criteria, '[code]');
        if (!empty($code)) {
            $filters['code'] = $code;
        }

        $allowedWarehouseIds = $propertyAccessor->getValue($criteria, '[warehouse.id]');

        $includes = [
            'stockBatches',
            'stockBatches.warehouse',
            'stockBatches.location',
            'stockBatches.location.location',
            'stockBatches.location.location.warehouse', // C'est uinpeu contre-intuitif car on utilise pas cette relationship par ce biais là mais si on ne l'ajoute pas ici aussi, le itemLocations.location.warehouse n'est pas inclus !
            'itemLocations',
            'itemLocations.location',
            'itemLocations.location.warehouse',
            'itemWarehouses',
            'itemWarehouses.warehouse',
        ];

        $fields = [
            'item' => [
                'code',
                'description',
                'unit',
                'stockBatches',
                'itemLocations',
                'itemWarehouses',
            ],
            'batch' => [
                'quantity',
                'warehouse',
                'location',
            ],
            'itemwarehouse' => [
                'warehouse',
            ],
            'warehouse' => [
                'id',
            ],
            'itemlocation' => [
                'location',
            ],
            'location' => [
                'code',
                'warehouse',
            ],
        ];

        $itemsApiResponse =
            $this->carlConfigurationLogistique->getCarlClient()->getCarlObjectsRaw(
                entity: 'item',
                filters: $filters,
                includes: $includes,
                fields: $fields
            );

        $articles = [];
        foreach ($itemsApiResponse['data'] as $itemApiResponseData) {
            $articleApiResource = new JsonApiResource($itemApiResponseData, $itemsApiResponse['included']);

            $indexedLots = [];
            $this->addWarehousesToIndexedLots($articleApiResource->getRelationshipCollection('itemWarehouses'), $indexedLots, $allowedWarehouseIds);
            $this->addLocationsToIndexedLots($articleApiResource->getRelationshipCollection('itemLocations'), $indexedLots, $allowedWarehouseIds);
            $this->addQuantitiesToIndexedLots($articleApiResource->getRelationshipCollection('stockBatches'), $indexedLots, $allowedWarehouseIds);

            $lots = $this->mergeIndexedLots($indexedLots);

            $articles[] = new Article(
                id: $articleApiResource->getId(),
                code: $articleApiResource->getAttribute('code'),
                titre: $articleApiResource->getAttribute('description') ?? '',
                unité: $this->carlConfigurationLogistique->getCarlClient()->getUnitDescriptionFromCode($articleApiResource->getAttribute('unit')),
                lots: $lots
            );
        }

        return $articles;
    }

    /**
     * @param array<JsonApiResource>          $itemWarehousesJsonApiResource
     * @param array<string,array<string,Lot>> $indexedLots                   lots indéxés par [warehouseId][locationId]
     * @param array<string>                   $allowedWarehouseIds
     *
     * @return void
     */
    private function addWarehousesToIndexedLots(array $itemWarehousesJsonApiResource, array &$indexedLots, $allowedWarehouseIds)
    {
        foreach ($itemWarehousesJsonApiResource as $itemWarehouseJsonApiResource) {
            $currentWarehouseId = $itemWarehouseJsonApiResource->getRelationship('warehouse')->getId();
            if (!$this->isWarehouseIdAllowedInFilters($currentWarehouseId, $allowedWarehouseIds)) {
                continue;
            }
            $indexedLots[$itemWarehouseJsonApiResource->getRelationship('warehouse')->getId()] = [];
        }
    }

    /**
     * @param array<JsonApiResource>          $itemLocationsJsonApiResource
     * @param array<string,array<string,Lot>> $indexedLots                  lots indéxés par [warehouseId][locationId]
     * @param array<string>                   $allowedWarehouseIds
     *
     * @return void
     */
    private function addLocationsToIndexedLots(array $itemLocationsJsonApiResource, array &$indexedLots, $allowedWarehouseIds)
    {
        foreach ($itemLocationsJsonApiResource as $itemLocationJsonApiResource) {
            $currentLocationJsonApiResource = $itemLocationJsonApiResource->getRelationship('location');
            $currentWarehouseId = $currentLocationJsonApiResource->getRelationship('warehouse')->getId();
            if (!$this->isWarehouseIdAllowedInFilters($currentWarehouseId, $allowedWarehouseIds)) {
                continue;
            }

            $currentLocationId = $currentLocationJsonApiResource->getId();

            Helpers::initArrayValueIfAbsent($indexedLots, $currentWarehouseId, []);
            Helpers::initArrayValueIfAbsent(
                $indexedLots[$currentWarehouseId],
                $currentLocationId,
                new Lot(
                    quantité: 0,
                    magasinId: $currentWarehouseId,
                    emplacement: new Emplacement(
                        id: $currentLocationId,
                        code: $currentLocationJsonApiResource->getAttribute('code')
                    )
                )
            );
        }
    }

    /**
     * @param array<JsonApiResource>          $stockBatchesJsonApiResource
     * @param array<string,array<string,Lot>> $indexedLots                 lots indéxés par [warehouseId][locationId]
     * @param array<string>                   $allowedWarehouseIds
     *
     * @return void
     */
    private function addQuantitiesToIndexedLots(array $stockBatchesJsonApiResource, array &$indexedLots, array $allowedWarehouseIds)
    {
        foreach ($stockBatchesJsonApiResource as $batchJsonApiResource) {
            $currentWarehouseId = $batchJsonApiResource->getRelationship('warehouse')->getId();
            if (!$this->isWarehouseIdAllowedInFilters($currentWarehouseId, $allowedWarehouseIds)) {
                continue;
            }

            $currentLocationApiResource = $batchJsonApiResource->getRelationship('location')?->getRelationship('location');
            $currentLotIndex = $currentLocationApiResource?->getId() ?? 0; // pour les batch sans location, on indexe sur la clef 0

            Helpers::initArrayValueIfAbsent($indexedLots, $currentWarehouseId, []);
            Helpers::initArrayValueIfAbsent(
                $indexedLots[$currentWarehouseId],
                $currentLotIndex,
                new Lot(
                    quantité: 0,
                    magasinId: $currentWarehouseId,
                    emplacement: is_null($currentLocationApiResource) ? null : new Emplacement(
                        id: $currentLocationApiResource->getId(),
                        code: $currentLocationApiResource->getAttribute('code')
                    )
                )
            );

            $indexedLots[$currentWarehouseId][$currentLotIndex]->addQuantité(
                $batchJsonApiResource->getAttribute('quantity')
            );
        }
    }

    /**
     * créé un tableau plat contenant tous les lots.
     * Note : si la clef d'un magasin est pésente mais avec rien dedans,
     * il faut ajouter un lot sans emplacement car on peut stocker des articles dedans (comportelment carl).
     *
     * @param array<string,array<string,Lot>> $indexedLots lots indéxés par [warehouseId][locationId]
     *
     * @return array<Lot>
     */
    private function mergeIndexedLots(array $indexedLots): array
    {
        $lots = [];

        foreach ($indexedLots as $magasinId => $magasin) {
            if (empty($magasin)) { // si la clef d'un magasin est pésente mais avec rien dedans, il faut ajouter un lot sans emplacement car on peut stocker des articles dedans (comportelment carl).
                $lots[] = new Lot(
                    quantité: 0,
                    magasinId: $magasinId
                );
            }
            foreach ($magasin as $lot) {
                $lots[] = $lot;
            }
        }

        return $lots;
    }

    /**
     * @param array<string> $warehouseIdFilters
     *
     * @return bool
     */
    private function isWarehouseIdAllowedInFilters(string $warehouseId, array $warehouseIdFilters)
    {
        return !empty($warehouseIdFilters) && in_array($warehouseId, $warehouseIdFilters);
    }

    public function findOneBy(array $criteria): Article
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function getClassName()
    {
        return Article::class;
    }
}
