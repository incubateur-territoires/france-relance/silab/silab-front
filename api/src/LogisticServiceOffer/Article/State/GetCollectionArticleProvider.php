<?php

namespace App\LogisticServiceOffer\Article\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\LogisticServiceOffer\Article\Entity\ArticleApiResource;
use App\LogisticServiceOffer\Article\Repository\ArticleRepository;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;

final class GetCollectionArticleProvider implements ProviderInterface
{
    public function __construct(private LogisticServiceOfferRepository $logisticServiceOfferRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<ArticleApiResource>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $logisticServiceOffer = $this->logisticServiceOfferRepository->find($uriVariables['serviceOfferId']);

        $criteria = $context['filters'] ?? [];

        foreach ($criteria as $key => $value) {
            if (is_string($value)) {
                $criteria[$key] = [$value];
            }
        }

        $criteria['warehouse.id'] = [$logisticServiceOffer->getWarehouseId()];

        $articleRepository = new ArticleRepository($logisticServiceOffer->getGmaoConfiguration());

        return array_map(
            function ($article) {
                return $article->toApiRessource();
            },
            $articleRepository->findBy($criteria)
        );
    }
}
