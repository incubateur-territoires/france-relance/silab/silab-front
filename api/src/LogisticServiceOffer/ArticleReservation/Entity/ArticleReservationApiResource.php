<?php

namespace App\LogisticServiceOffer\ArticleReservation\Entity;

use ApiPlatform\Metadata\GetCollection;
use App\LogisticServiceOffer\ArticleReservation\State\GetCollectionArticleReservationProvider;

// voir src/LogisticServiceOffer/ArticleReservation/reservation_article.md
#[GetCollection(
    uriTemplate: '/logistic-service-offers/{serviceOfferId}/interventions/{interventionId}/article-reservations',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'interventionId' => 'interventionId',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_LOGISTIQUE_CONSULTER_RESERVATIONS')",
    output: ArticleReservation::class,
    provider: GetCollectionArticleReservationProvider::class,
    order: ['dateAttendueDeReception' => 'DESC']
)]
#[GetCollection(
    uriTemplate: '/logistic-service-offers/{serviceOfferId}/article-reservations',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_LOGISTIQUE_CONSULTER_RESERVATIONS')",
    output: ArticleReservation::class,
    provider: GetCollectionArticleReservationProvider::class,
    order: ['dateAttendueDeReception' => 'DESC']
)]
class ArticleReservationApiResource
{
}
