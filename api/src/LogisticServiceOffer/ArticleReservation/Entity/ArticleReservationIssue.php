<?php

namespace App\LogisticServiceOffer\ArticleReservation\Entity;

use ApiPlatform\Metadata\Post;
use App\LogisticServiceOffer\ArticleReservation\State\CreateArticleReservationIssueProcessor;

#[Post(
    read: false,
    uriTemplate: 'logistic-service-offers/{serviceOfferId}/article-reservations/{articleReservationId}/article-reservation-issues',
    uriVariables: [
        'articleReservationId' => 'articleReservationId',
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_LOGISTIQUE_SORTIR_ARTICLES')",
    processor: CreateArticleReservationIssueProcessor::class,
)]
class ArticleReservationIssue
{
    public function __construct(
        private string $articleId,
        private float $quantity,
        private string $warehouseClerkEmail,
        private ?string $interventionId,
        private ?string $warehouseId,
        private ?string $storageLocationId
    ) {
    }

    public function getArticleId(): string
    {
        return $this->articleId;
    }

    public function getQuantity(): float
    {
        return $this->quantity;
    }

    public function getWarehouseClerkEmail(): string
    {
        return $this->warehouseClerkEmail;
    }

    public function getInterventionId(): ?string
    {
        return $this->interventionId;
    }

    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }

    public function getStorageLocationId(): ?string
    {
        return $this->storageLocationId;
    }
}
