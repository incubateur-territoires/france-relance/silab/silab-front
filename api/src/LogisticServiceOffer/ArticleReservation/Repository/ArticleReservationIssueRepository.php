<?php

namespace App\LogisticServiceOffer\ArticleReservation\Repository;

use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservationIssue;
use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\LogisticServiceOffer\Gmao\Entity\GmaoConfigurationLogistique;
use App\Shared\Exception\RuntimeException;

final class ArticleReservationIssueRepository implements ArticleReservationIssueRepositoryInterface
{
    private ArticleReservationIssueRepositoryInterface $specificArticleReservationIssueRepository;

    public function __construct(
        protected GmaoConfigurationLogistique $gmaoConfigurationLogistique
    ) {
        try {
            $this->specificArticleReservationIssueRepository = match ($gmaoConfigurationLogistique::class) {
                CarlConfigurationLogistique::class => new CarlArticleReservationIssueRepository(
                    $gmaoConfigurationLogistique
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfigurationLogistique::class);
        }
    }

    public function save(ArticleReservationIssue $entity): void
    {
        $this->specificArticleReservationIssueRepository->save($entity);
    }

    public function remove(ArticleReservationIssue $entity): void
    {
        throw new RuntimeException("Cette méthode n'est pas encore implémentée");
    }
}
