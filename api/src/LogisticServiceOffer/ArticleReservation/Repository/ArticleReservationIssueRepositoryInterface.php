<?php

namespace App\LogisticServiceOffer\ArticleReservation\Repository;

use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservationIssue;

interface ArticleReservationIssueRepositoryInterface
{
    public function save(ArticleReservationIssue $entity): void;

    public function remove(ArticleReservationIssue $entity): void;
}
