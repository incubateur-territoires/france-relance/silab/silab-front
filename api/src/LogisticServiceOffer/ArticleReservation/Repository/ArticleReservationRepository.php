<?php

namespace App\LogisticServiceOffer\ArticleReservation\Repository;

use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\LogisticServiceOffer\Gmao\Service\GmaoConfigurationLogistiqueContext;
use App\Shared\Exception\RuntimeException;

final class ArticleReservationRepository implements ArticleReservationRepositoryInterface
{
    private ArticleReservationRepositoryInterface $specificArticleReservationRepository;

    public function __construct(
        CarlArticleReservationRepository $carlArticleReservationRepository,
        GmaoConfigurationLogistiqueContext $gmaoConfigurationLogistiqueContext
    ) {
        try {
            $this->specificArticleReservationRepository = match ($gmaoConfigurationLogistiqueContext->getGmaoConfiguration()::class) {
                CarlConfigurationLogistique::class => $carlArticleReservationRepository,
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfigurationLogistiqueContext->getGmaoConfiguration()::class);
        }
    }

    public function find(mixed $id): ?ArticleReservation
    {
        return $this->specificArticleReservationRepository->find($id);
    }

    /**
     * @return array<ArticleReservation>
     */
    public function findAll(): array
    {
        return $this->specificArticleReservationRepository->findAll();
    }

    public function findOneBy(array $criteria): ArticleReservation
    {
        return $this->specificArticleReservationRepository->findOneBy($criteria);
    }

    public function getClassName()
    {
        return self::class;
    }

    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array {
        return $this->specificArticleReservationRepository->findBy(
            $criteria,
            $orderBy,
            $limit,
            $offset
        );
    }
}
