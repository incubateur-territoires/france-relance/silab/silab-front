<?php

namespace App\LogisticServiceOffer\ArticleReservation\Repository;

use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\LogisticServiceOffer\ArticleReservation\Entity\CarlArticleReservationId;
use App\LogisticServiceOffer\CarlV6\Repository\DbCarlArticleReservationRepository;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Exception\RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccess;

final class CarlArticleReservationRepository implements ArticleReservationRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;

    public function __construct(
        private DbCarlArticleReservationRepository $dbCarlArticleReservationRepository,
    ) {
    }

    /**
     * @param mixed $id un id au format généré par la méthode CarlArticleReservationId->toString()
     */
    public function find(mixed $id): ?ArticleReservation
    {
        $criterias = [
            'id' => $id,
        ];

        return $this->findOneBy($criterias);
    }

    /**
     * @return array<ArticleReservation>
     */
    public function findAll(): array
    {
        return $this->findBy([]);
    }

    public function findOneBy(array $criteria): ArticleReservation
    {
        $articleReservations = $this->findBy($criteria);

        if (0 === count($articleReservations)) {
            throw new RuntimeException('Aucune réservation trouvée avec les critères donnés');
        }

        return array_values($articleReservations)[0];
    }

    public function getClassName()
    {
        return self::class;
    }

    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
                'warehouseCode',
                'warehouseId',
                'intervention.id',
                'articleId',
                'storageLocationId',
            ]
        );

        $articleReservationQueryBuilder = $this->dbCarlArticleReservationRepository->createQueryBuilder('dbCarlArticleReservation')
            ->innerJoin('dbCarlArticleReservation.dbCarlIntervention', 'dbCarlIntervention');

        $id = $propertyAccessor->getValue($criteria, '[id]');
        if (!empty($id)) {
            $articleReservationQueryBuilder
                ->andWhere('dbCarlArticleReservation.id IN(:dbCarlArticleReservation_id)')
                ->setParameter('dbCarlArticleReservation_id', $id);
        }

        $warehouseCode = $propertyAccessor->getValue($criteria, '[warehouseCode]');
        if (!empty($warehouseCode)) {
            $articleReservationQueryBuilder
                ->andWhere('dbCarlArticleReservation.warehouseCode IN(:warehouseCode)')
                ->setParameter('warehouseCode', $warehouseCode);
        }

        $warehouseId = $propertyAccessor->getValue($criteria, '[warehouseId]');
        if (!empty($warehouseId)) {
            $articleReservationQueryBuilder
                ->andWhere('dbCarlArticleReservation.warehouseId IN(:warehouseId)')
                ->setParameter('warehouseId', $warehouseId);
        }

        $interventionId = $propertyAccessor->getValue($criteria, '[intervention.id]');
        if (!empty($interventionId)) {
            $articleReservationQueryBuilder
                ->andWhere('dbCarlIntervention.id IN(:interventionId)')
                ->setParameter('interventionId', $interventionId);
        }

        $articleId = $propertyAccessor->getValue($criteria, '[articleId]');
        if (!empty($articleId)) {
            $articleReservationQueryBuilder
                ->andWhere('dbCarlArticleReservation.articleId IN(:articleId)')
                ->setParameter('articleId', $articleId);
        }

        $storageLocationId = $propertyAccessor->getValue($criteria, '[storageLocationId]');
        if (!empty($storageLocationId)) {
            $articleReservationQueryBuilder
                ->andWhere('dbCarlArticleReservation.storageLocationId IN(:storageLocationId)')
                ->setParameter('storageLocationId', $storageLocationId);
        }

        foreach ($orderBy as $field => $direction) {
            $articleReservationQueryBuilder->addOrderBy("dbCarlArticleReservation.$field", $direction);
        }

        $dbCarlArticleReservations = $articleReservationQueryBuilder
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        return array_map(
            function ($dbCarlArticleReservation) {
                return $dbCarlArticleReservation->toArticleReservation();
            },
            $dbCarlArticleReservations
        );
    }
}
