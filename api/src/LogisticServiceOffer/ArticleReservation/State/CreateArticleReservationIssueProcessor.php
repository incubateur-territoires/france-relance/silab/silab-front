<?php

namespace App\LogisticServiceOffer\ArticleReservation\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservationIssue;
use App\LogisticServiceOffer\ArticleReservation\Repository\ArticleReservationIssueRepository;
use App\LogisticServiceOffer\ArticleReservation\Repository\ArticleReservationRepository;
use App\LogisticServiceOffer\LogisticServiceOffer\Service\LogisticServiceOfferContext;
use App\Shared\Exception\InsufficientRolesException;
use App\Shared\Exception\RuntimeException;
use Psr\Log\LoggerInterface;

class CreateArticleReservationIssueProcessor implements ProcessorInterface
{
    public function __construct(
        private LogisticServiceOfferContext $logisticServiceOfferContext,
        private LoggerInterface $logger,
        private ArticleReservationRepository $articleReservationRepository
    ) {
    }

    /**
     * @param ArticleReservationIssue $data
     * @param array<mixed>            $uriVariables
     * @param array<mixed>            $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): ArticleReservationIssue {
        $this->logger->debug('Création d\'une sortie de stock', ['data' => json_encode($data), 'context' => $context, 'uriVariables' => $uriVariables]);
        assert($operation instanceof Post);
        assert($data instanceof ArticleReservationIssue);

        $logisticServiceOffer = $this->logisticServiceOfferContext->getServiceOffer();

        // On verifie que le warehouseId de la sortie de stock soit l'attribut d'une des offres de service de la liste autorisé.
        $laSortieNeConcernepasCeMagasin = $logisticServiceOffer->getWarehouseId() !== $data->getWarehouseId();

        if ($laSortieNeConcernepasCeMagasin) {
            throw new InsufficientRolesException("Vous n'avez pas les droits suffisants pour réaliser une sortie dans ce magasin.");
        }

        // Verification des quantités
        $articleReservationId = $uriVariables['articleReservationId'];
        $articleReservation = $this->articleReservationRepository->find($articleReservationId);
        $quantityToIssue = $data->getQuantity();
        if (0.0 === $quantityToIssue) {
            throw new RuntimeException('Quantité à sortir incorrecte. Merci de fournir une quantité non nulle.');
        }
        if ($quantityToIssue < 0.0) {
            throw new RuntimeException("Quantité à sortir incorrecte. La quantité d'articles à sortir doit être positive.");
        }
        if (is_null($articleReservation)) {
            throw new RuntimeException('Aucune réservation trouvée sur ces critères.Vous ne pouvez pas retirer d\'articles sans réservation préalable.');
        }
        if ($quantityToIssue > $articleReservation->getQuantity()) {
            throw new RuntimeException("Quantité à sortir incorrecte. $quantityToIssue article(s) à sortir alors que les réservations concernent seulement ".$articleReservation->getQuantity().' articles');
        }
        if ($quantityToIssue > $articleReservation->getAvailableQuantity()) {
            throw new RuntimeException("Quantité à sortir incorrecte. $quantityToIssue article(s) à sortir alors que le stock en possède seulement ".$articleReservation->getAvailableQuantity());
        }

        $articleReservationIssueRepository = new ArticleReservationIssueRepository($logisticServiceOffer->getGmaoConfiguration());
        $articleReservationIssueRepository->save($data);

        return $data;
    }
}
