<?php

namespace App\LogisticServiceOffer\ArticleReservation\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\LogisticServiceOffer\ArticleReservation\Repository\ArticleReservationRepository;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;

final class GetCollectionArticleReservationProvider implements ProviderInterface
{
    public function __construct(private LogisticServiceOfferRepository $logisticServiceOfferRepository, private ArticleReservationRepository $articleReservationRepository)
    {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<ArticleReservation>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $criteria = $context['filters'] ?? [];

        $logisticServiceOffer = $this->logisticServiceOfferRepository->find($uriVariables['serviceOfferId']);

        $criteria['warehouseId'] = $logisticServiceOffer->getWarehouseId();

        if (isset($uriVariables['interventionId'])) {
            $criteria['intervention.id'] = $uriVariables['interventionId'];
        }
        $order = $operation->getOrder();

        return $this->articleReservationRepository->findBy($criteria, $order);
    }
}
