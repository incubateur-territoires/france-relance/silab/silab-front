# Réservation d'articles

## Stockage des articles

Avant d'expliquer le mécaniques de réservation, il peut être important de comprendre comment carl représente le stockage des articles.

Voici les différentes entités utilisées pour représenter le stockage des articles dans les mégasins sur Carl :

- **Magasin** (CSST_WAREHOUSE et CSPU_SUPPLIER)  
  Un entrepot gérant le stockage d'articles.
- **Emplacement** (CSST_LOCATION)  
  Un emplacement localisé dédié au stockage d'articles et rattaché à un magasin, ex: 1er étage de l'a deuxième étagère du rang 3 du magasin général.  
  Ces emplacements ont généralement un code attribué, et idéalement un code-barre (ou autre datamatrix, qrcode...) portant ce code (ex: *R3_E2_3*).
- **Emplacement dédié à un article** (CSST_ITEMLOCATION)  
  Endroit prévu pour le stockage d'un article spécifié. ex: les *GANTS_NYLON_3M_T3* doivent êtres stockés à l'emplacement *R3_E2_3*.
- **Article** (CSST_ITEM extends CSEQ_EQUIPMENT)  
  Consommables, actifs, matériels divers, équipements, matériaux.  
  Ils ont normalement un code (ex: *GANTS_NYLON_3M_T3*) et porte leur unité de stockage (ex: paire, kilogramme, unité, etc...).
- **Lot** (CSST_BATCH)
  Un lot indique combien d'articles sont stockés sur un emplacement dédié pour un magasin. ex: il y a actuellement 18 *GANTS_NYLON_3M_T3* sur sont emplacement dédié *R3_E2_3*.  
  Note importante : un lot associe des quantités à des emplacements dédiés (CSST_ITEMLOCATION), de fait il est impossible de stocker un article sur un emplacement qui n'est pas prévu pour. En revanche, un article peut être stocké sans emplacement dans un magasin, dans ce cas le lot précise simplement l'article et le magasin.  
  Note: il est impossible de créer des lots avec 0 quantité via l'api ou l'application, dans ce cas il n'y a simplement pas de lot.
- **Magasin pour un article** (CSST_ITEMWAREHOUSE)
  Permet d'indiquer qu'un article est géré par un magasin, cela permet notamment de décrire des règle de gestion (niveaux de réappro, etc...).  
  Nous ne détaillerons pas plus cet aspect là ici.
  
```mermaid
classDiagram
    Magasin o-- Emplacement : plusieurs
    Emplacement_dédié o-- Emplacement : un
    Emplacement_dédié o-- Article : un
    Lot o-- Emplacement_dédié : un
    Lot o-- Magasin : un
    Lot o-- Article : un
    class Magasin{
    }
    class Article{
        unité
    }
    class Emplacement{
    }
    class Emplacement_dédié{
    }
    class Lot{
        quantité
    }
```

## Réservation

Voici les différentes entités utilisées pour représenter la réservation d'articles sur Carl (en plus des précédentes) :

- **Réservation**
  Une réservation indique un besoin de fourniture d'article, il concerne un quantité d'un article precis et indique le magasin souhaité.  
- **Intervention** (CSWO_WO)
  Une reservations d'article est nécéssairement portée par une intervention, ex: Pour repeindre le passage piéton, j'ai besoin de 1 litre de *PEINTURE_AXION_ANTIDERAP_BLANC*  
  Note: il est possible de créer des interventions dans le seul but de fournir du matériel, ex: j'ai besoin d'équiper mon nouveau technicien d'une paire de *GANTS_NYLON_3M_T3*. Ces interventions sont appelées des *demandes de fournitures*

```mermaid
classDiagram
    Réservation o-- Intervention : un
    Réservation o-- Article_de_magasin : un
    Réservation o-- Article : un
    Article_de_magasin o-- Article : un
    Article_de_magasin o-- Magasin : un
    class Réservation{
        quantité
    }
    class Article_de_magasin{
    }
    class Article{
        unité
    }
    class Magasin{
    }
```

## Sortie de stock

Voici les différentes entités utilisées pour représenter une sortie de stock sur Carl (en plus des précédentes) :

- **Sortie de stock** (CSST_STOCKMOVEMENT)
  Une sortie de stock indique que l'on a retiré une certaine quantité d'un article du stock d'un magasin à la faveur d'une intervention.
  Note: effectuer une sortie de stock sur carl va automatiquement décrémenter les réservations portant sur le même combo intervention/article/magasin.
  
```mermaid
classDiagram
    Sortie o-- Intervention : un
    Sortie o-- Article : un
    Sortie o-- Magasin : un
    class Sortie{
        quantité
    }
    class Article{
        unité
    }
    class Magasin{
    }
```
