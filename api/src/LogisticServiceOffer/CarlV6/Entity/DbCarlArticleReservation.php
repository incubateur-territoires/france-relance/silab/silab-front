<?php

namespace App\LogisticServiceOffer\CarlV6\Entity;

use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\LogisticServiceOffer\CarlV6\Repository\DbCarlArticleReservationRepository;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlArticleReservationRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlArticleReservation implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 102)] // CSST_RESERVE.ID + CSST_ITEM.ID + CSWO_WO.ID + CSST_WAREHOUSE.ID + 3
        private string $id,
        #[ORM\Column(length: 33)] // CSST_ITEM.ID
        private string $articleId,
        #[ORM\Column(length: 20)] // CSEQ_EQUIPMENT.CODE
        private string $articleCode,
        #[ORM\Column(length: 60)] // CSEQ_EQUIPMENT.DESCRIPTION
        private string $articleDescription,
        #[ORM\Column(precision: 15, scale: 4)] // CSST_RESERVE.QUANTITY
        private float $quantity,
        #[ORM\Column(precision: 15, scale: 4)] // CSST_BATCH.QUANTITY
        private float $availableQuantity,
        #[ORM\Column(length: 255)] // CSST_ITEM.UNIT
        private string $unit,
        #[ORM\Column(length: 33)] // CSST_WAREHOUSE.ID
        private string $warehouseId,
        #[ORM\Column(length: 40)] // CSPU_SUPPLIER.CODE
        private ?string $warehouseCode = null,
        #[ORM\Column(length: 33)] // CSST_LOCATION.ID
        private ?string $storageLocationId = null,
        #[ORM\Column(length: 20)] // CSST_LOCATION.CODE
        private ?string $storageLocationCode = null,
        #[ORM\Column(length: 255)] // CSSY_ACTOR.FULLNAME
        private ?string $creatorName = null,
        #[ORM\Column(length: 255)] // CSSY_ACTOR.FULLNAME
        private ?string $creatorDirectionName = null,
        #[ORM\Column(type: 'datetime_immutable')] // CSST_RESERVE.RESERVEDATE
        private ?\DateTimeInterface $dateAttendueDeReception = null,
        #[ORM\ManyToOne(inversedBy: 'dbCarlReservations')]
        #[JoinColumn()]
        private ?DbCarlIntervention $dbCarlIntervention = null,
        #[ORM\Column(length: 20)] // CSWO_WO.CODE
        private ?string $interventionCode = null,
        #[ORM\Column(length: 60)] // CSWO_WO.DESCRIPTION
        private ?string $interventionTitle = null,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_logistique_db_carl_article_reservation"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id                          varchar(102)     OPTIONS (key 'true') NOT NULL,
            article_id                  varchar(33),
            article_code                varchar(20),
            article_description         varchar(60),
            quantity                    numeric(15,4),
            available_quantity          numeric(15,4),
            unit                        varchar(255),
            warehouse_id                varchar(33),
            warehouse_code              varchar(40),
            storage_location_id         varchar(33),
            storage_location_code       varchar(20),
            creator_name                varchar(255),
            creator_direction_name      varchar(255),
            date_attendue_de_reception  timestamptz(3),
            db_carl_intervention_id     varchar(33),
            intervention_code           varchar(20),
            intervention_title          varchar(60)
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            -- dans la réalité, s''il y a plusieurs emplacements possibles pour un article dans un magasin,
            -- pour l''intant on choisit arbitrairement le premier qui viens (d''où le `where rownum = 1`), mais ça reste à améliorer
            with FIRST_LOCALISATION_ARTICLE as (
                select *
                from (
                    select
                        ITEMLOCATION.ITEM_ID as ITEM_ID,
                        LOCALISATION.ID as LOCALISATION_ID,
                        LOCALISATION.CODE as LOCALISATION_CODE,
                        LOCALISATION.WAREHOUSE_ID as WAREHOUSE_ID,
                        ARTICLE_BATCH.QUANTITY as STORED_QUANTITY,
                        row_number() over (partition by ARTICLE_BATCH.ID order by ARTICLE_BATCH.QUANTITY desc) as ROWNUMBER
                    from "$dbSchema".CSST_ITEMLOCATION ITEMLOCATION
                    inner join "$dbSchema".CSST_LOCATION LOCALISATION
                        on LOCALISATION.ID = ITEMLOCATION.LOCATION_ID
                    inner join "$dbSchema".CSST_BATCH ARTICLE_BATCH
                        on ARTICLE_BATCH.ITEM_ID = ITEMLOCATION.ITEM_ID
                        and ARTICLE_BATCH.LOCATION_ID = ITEMLOCATION.ID
                ) temp
                where ROWNUMBER = 1
            )
            select
                ARTICLE_RESERVATION.ITEM_ID || ''|'' || INTERVENTION.ID || ''|'' || ITEMWAREHOUSE.WAREHOUSE_ID || ''|'' || FIRST_LOCALISATION_ARTICLE.LOCALISATION_ID as ID,
                ARTICLE_RESERVATION.ITEM_ID as ARTICLE_ID,
                any_value(ARTICLE_EQUIPEMENT.CODE) as ARTICLE_CODE,
                any_value(ARTICLE_EQUIPEMENT.DESCRIPTION) as ARTICLE_DESCRIPTION,
                sum(ARTICLE_RESERVATION.QUANTITY) as QUANTITY,
                any_value(coalesce(FIRST_LOCALISATION_ARTICLE.STORED_QUANTITY,0)) as AVAILABLE_QUANTITY,
                any_value(ITEM_UNIT.DESCRIPTION) as UNIT,
                ITEMWAREHOUSE.WAREHOUSE_ID as WAREHOUSE_ID,
                any_value(WAREHOUSE_SUPPLIER.CODE) as WAREHOUSE_CODE,
                FIRST_LOCALISATION_ARTICLE.LOCALISATION_ID as STORAGE_LOCATION_ID,
                any_value(FIRST_LOCALISATION_ARTICLE.LOCALISATION_CODE) as STORAGE_LOCATION_CODE,
                any_value(RESERVATION_ACTOR.FULLNAME) as CREATOR_NAME,
                any_value(RESERVATION_ACTOR_SUPERVISOR.FULLNAME) as CREATOR_DIRECTION_NAME,
                min(FROM_TZ(ARTICLE_RESERVATION.RESERVEDATE, ''GMT'')) as EXPECTED_RECEIPT_DATE,
                INTERVENTION.ID as INTERVENTION_ID,
                any_value(INTERVENTION.CODE) as INTERVENTION_CODE,
                any_value(INTERVENTION.DESCRIPTION) as INTERVENTION_TITLE
            from "$dbSchema".CSST_RESERVE ARTICLE_RESERVATION
                inner join "$dbSchema".CSST_ITEM ITEM on ITEM.ID = ARTICLE_RESERVATION.ITEM_ID
                inner join "$dbSchema".CSEQ_EQUIPMENT ARTICLE_EQUIPEMENT on ARTICLE_EQUIPEMENT.ID = ARTICLE_RESERVATION.ITEM_ID
                inner join "$dbSchema".CSWO_WO INTERVENTION on INTERVENTION.ID = ARTICLE_RESERVATION.WO_ID
                inner join "$dbSchema".CSST_ITEMWAREHOUSE ITEMWAREHOUSE on ITEMWAREHOUSE.ID = ARTICLE_RESERVATION.ITEMWAREHOUSE_ID
                inner join "$dbSchema".CSST_WAREHOUSE WAREHOUSE on WAREHOUSE.ID = ITEMWAREHOUSE.WAREHOUSE_ID
                inner join "$dbSchema".CSPU_SUPPLIER WAREHOUSE_SUPPLIER on WAREHOUSE_SUPPLIER.ID = WAREHOUSE.ID
                left join FIRST_LOCALISATION_ARTICLE
                    on FIRST_LOCALISATION_ARTICLE.ITEM_ID = ARTICLE_RESERVATION.ITEM_ID
                    and FIRST_LOCALISATION_ARTICLE.WAREHOUSE_ID = ITEMWAREHOUSE.WAREHOUSE_ID
                inner join "$dbSchema".CSSY_ACTOR RESERVATION_ACTOR on RESERVATION_ACTOR.ID = ARTICLE_RESERVATION.ACTOR_ID
                left join "$dbSchema".CSSY_ACTOR RESERVATION_ACTOR_SUPERVISOR on RESERVATION_ACTOR_SUPERVISOR.ID = RESERVATION_ACTOR.SUPERVISOR_ID
                left join "$dbSchema".CSSY_UNIT ITEM_UNIT on ITEM_UNIT.code = ITEM.UNIT
            group by
                ARTICLE_RESERVATION.ITEM_ID, INTERVENTION.ID, ITEMWAREHOUSE.WAREHOUSE_ID, FIRST_LOCALISATION_ARTICLE.LOCALISATION_ID
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getArticleId(): string
    {
        return $this->articleId;
    }

    public function getArticleCode(): string
    {
        return $this->articleCode;
    }

    public function getArticleDescription(): string
    {
        return $this->articleDescription;
    }

    public function getQuantity(): float
    {
        return $this->quantity;
    }

    public function getAvailableQuantity(): float
    {
        return $this->availableQuantity;
    }

    public function getUnit(): string
    {
        return $this->unit;
    }

    public function getWarehouseId(): string
    {
        return $this->warehouseId;
    }

    public function getWarehouseCode(): ?string
    {
        return $this->warehouseCode;
    }

    public function getStorageLocationId(): ?string
    {
        return $this->storageLocationId;
    }

    public function getStorageLocationCode(): ?string
    {
        return $this->storageLocationCode;
    }

    public function getCreatorName(): ?string
    {
        return $this->creatorName;
    }

    public function getCreatorDirectionName(): ?string
    {
        return $this->creatorDirectionName;
    }

    public function getDateAttendueDeReception(): ?\DateTimeInterface
    {
        return $this->dateAttendueDeReception;
    }

    public function getDbCarlIntervention(): ?DbCarlIntervention
    {
        return $this->dbCarlIntervention;
    }

    public function getInterventionCode(): ?string
    {
        return $this->interventionCode;
    }

    public function getInterventionTitle(): ?string
    {
        return $this->interventionTitle;
    }

    public function toArticleReservation(): ArticleReservation
    {
        return new ArticleReservation(
            id: $this->getId(),
            articleId: $this->getArticleId(),
            articleCode: $this->getArticleCode(),
            articleDescription: $this->getArticleDescription(),
            quantity: $this->getQuantity(),
            availableQuantity: $this->getAvailableQuantity(),
            unit: $this->getUnit(),
            warehouseId: $this->getWarehouseId(),
            warehouseCode: $this->getWarehouseCode(),
            storageLocationId: $this->getStorageLocationId(),
            storageLocationCode: $this->getStorageLocationCode(),
            creatorName: $this->getCreatorName(),
            creatorDirectionName: $this->getCreatorDirectionName(),
            dateAttendueDeReception: $this->getDateAttendueDeReception(),
            interventionId: $this->getDbCarlIntervention()->getId(),
            interventionCode: $this->getInterventionCode(),
            interventionTitle: $this->getInterventionTitle()
        );
    }

    /**
     * @param array<DbCarlArticleReservation> $dbCarlArticleReservations
     *
     * @return array<ArticleReservation>
     */
    public static function toArticleReservationArray(
        array $dbCarlArticleReservations,
    ): array {
        return array_map(
            function (DbCarlArticleReservation $dbCarlArticleReservation) {
                return $dbCarlArticleReservation->toArticleReservation();
            },
            $dbCarlArticleReservations
        );
    }
}
