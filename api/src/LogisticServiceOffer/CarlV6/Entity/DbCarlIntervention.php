<?php

namespace App\LogisticServiceOffer\CarlV6\Entity;

use App\LogisticServiceOffer\CarlV6\Repository\DbCarlInterventionRepository;
use App\LogisticServiceOffer\Intervention\Entity\Intervention;
use App\LogisticServiceOffer\MouvementDeSortie\Entity\MouvementDeSortieApiResource;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlInterventionRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlIntervention implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(length: 33)] // CSWO_WO.ID
        #[Groups([MouvementDeSortieApiResource::GROUP_AFFICHER_LISTE_MOUVEMENTS_DE_SORTIE])]
        private string $id,
        #[ORM\Column(length: 20)] // CSWO_WO.CODE
        private ?string $code,
        #[ORM\Column(length: 60)] // CSWO_WO.DESCRIPTION
        private ?string $titre,
        #[ORM\OneToMany(targetEntity: DbCarlArticleReservation::class, mappedBy: 'dbCarlIntervention')]
        #[OrderBy(['dateAttendueDeReception' => 'DESC'])]
        private Collection $dbCarlArticleReservations,
        #[ORM\OneToMany(targetEntity: DbCarlInterventionWarehouseReservation::class, mappedBy: 'dbCarlIntervention')]
        #[OrderBy(['dateAttendueDeReceptionLaPlusUrgente' => 'DESC'])]
        private Collection $dbCarlInterventionWarehouseReservations,
        #[OrderBy(['createdAt' => 'DESC'])]
        #[ORM\OneToMany(targetEntity: DbCarlInterventionStatus::class, mappedBy: 'dbCarlIntervention')]
        private Collection $dbCarlInterventionStatuses,
        #[ORM\Column(length: 255)] // CSSY_ACTOR.FULLANME
        private ?string $createdBy,
        #[ORM\Column(length: 60)] // CSFI_COSTCENTER.DESCRIPTION
        private ?string $centreDeCouts,
        #[ORM\Column(type: 'datetime_immutable')] // CSST_RESERVE.RESERVEDATE
        private ?\DateTimeInterface $dateAttendueDeReceptionLaPlusUrgente,
        /** @var array<string> */
        #[ORM\Column(type: 'json')]
        private array $equipes,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_logistique_db_carl_intervention"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            id                                              varchar(33)     OPTIONS (key 'true') NOT NULL,
            code                                            varchar(20),
            titre                                           varchar(60),
            created_by                                      varchar(255),
            centre_de_couts                                 varchar(60),
            date_attendue_de_reception_la_plus_urgente      timestamptz(3),
            equipes                                         json                                 NOT NULL
            
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with ARTICLE_RESERVATION_LA_PLUS_URGENTE as (
                select
                    RESERVE.WO_ID as INTERVENTION_ID,
                    MIN(RESERVE.RESERVEDATE) as RESERVEDATE
                from
                    "$dbSchema".CSST_RESERVE RESERVE
                group by
                    RESERVE.WO_ID
            ), EQUIPES_MAIN_D_OEUVRE as (
                select
                    DISTINCT_INTERVENTION_TEAMS.INTERVENTION_ID as INTERVENTION_ID,
                    JSON_ARRAYAGG(TEAM.DESCRIPTION returning clob) as EQUIPES
                from
                (
                    select distinct
                        RESSOURCE.WO_ID as INTERVENTION_ID,
                        RESSOURCE.TEAM_ID as TEAM_ID
                    from "$dbSchema".CSWO_WORESOURCES RESSOURCE
                ) DISTINCT_INTERVENTION_TEAMS
                left join "$dbSchema".CSWO_TEAM TEAM on TEAM.ID = DISTINCT_INTERVENTION_TEAMS.TEAM_ID
                group by
                    DISTINCT_INTERVENTION_TEAMS.INTERVENTION_ID
            )
            select
                INTERVENTION.ID as ID,
                INTERVENTION.CODE as CODE,
                INTERVENTION.DESCRIPTION as TITRE,
                CREATEDBY.FULLNAME as CREATED_BY,
                COSTCENTER.description as CENTRE_DE_COUTS,
                FROM_TZ(ARTICLE_RESERVATION_LA_PLUS_URGENTE.RESERVEDATE, ''GMT'') as DATE_ATTENDUE_DE_RECEPTION_LA_PLUS_URGENTE,
                coalesce(EQUIPES_MAIN_D_OEUVRE.EQUIPES,TO_CLOB(json_array())) as EQUIPES
            from "$dbSchema".CSWO_WO INTERVENTION
                left join "$dbSchema".CSSY_ACTOR CREATEDBY on CREATEDBY.ID = INTERVENTION.CREATEDBY_ID
                left join "$dbSchema".CSFI_COSTCENTER COSTCENTER on COSTCENTER.ID = INTERVENTION.COSTCENTER_ID
                left join ARTICLE_RESERVATION_LA_PLUS_URGENTE on ARTICLE_RESERVATION_LA_PLUS_URGENTE.INTERVENTION_ID = INTERVENTION.ID
                left join EQUIPES_MAIN_D_OEUVRE on EQUIPES_MAIN_D_OEUVRE.INTERVENTION_ID = INTERVENTION.ID
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function getDbCarlArticleReservations(): Collection
    {
        return $this->dbCarlArticleReservations;
    }

    public function getDbCarlInterventionStatuses(): Collection
    {
        return $this->dbCarlInterventionStatuses;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function getCentreDeCouts(): ?string
    {
        return $this->centreDeCouts;
    }

    /**
     * @return array<string>
     */
    public function getEquipes(): array
    {
        return $this->equipes;
    }

    public function getDateAttendueDeReceptionLaPlusUrgente(): ?\DateTimeInterface
    {
        return $this->dateAttendueDeReceptionLaPlusUrgente;
    }

    public function getDbCarlInterventionWarehouseReservation(): Collection
    {
        return $this->dbCarlInterventionWarehouseReservations;
    }

    public function toIntervention(bool $includeReservations, bool $includeHistoriqueDesEtats): Intervention
    {
        return new Intervention(
            id: $this->getId(),
            code: $this->getCode(),
            titre: $this->getTitre(),
            reservations: $includeReservations ? DbCarlArticleReservation::toArticleReservationArray($this->getDbCarlArticleReservations()->toArray()) : null,
            historiqueDesEtats: $includeHistoriqueDesEtats ? DbCarlInterventionStatus::toEtatArray($this->getDbCarlInterventionStatuses()->toArray()) : null,
            createdBy: $this->getCreatedBy(),
            centreDeCouts: $this->getCentreDeCouts(),
            dateAttendueDeReceptionLaPlusUrgente: $this->getDateAttendueDeReceptionLaPlusUrgente(),
            equipes: $this->getEquipes()
        );
    }
}
