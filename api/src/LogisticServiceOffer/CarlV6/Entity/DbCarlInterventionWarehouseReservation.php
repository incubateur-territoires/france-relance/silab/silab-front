<?php

namespace App\LogisticServiceOffer\CarlV6\Entity;

use App\LogisticServiceOffer\CarlV6\Repository\DbCarlInterventionWarehouseReservationRepository;
use App\LogisticServiceOffer\Intervention\Entity\Intervention;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * Cette classe sert à répondre à la question : "une intervention a t'elle des reservations concernant un magasin spécifique ?".
 */
#[ORM\Entity(
    readOnly: true,
    repositoryClass: DbCarlInterventionWarehouseReservationRepository::class
)]
#[ORM\Table(schema: 'carl')]
class DbCarlInterventionWarehouseReservation implements DbCarlEntitySqlBuilder
{
    public function __construct(
        #[ORM\Id]
        #[ORM\ManyToOne(inversedBy: 'dbCarlInterventionWarehouseReservations')]
        #[JoinColumn()]
        private DbCarlIntervention $dbCarlIntervention,
        #[ORM\Id]
        #[ORM\Column(length: 33)] // CSST_WAREHOUSE.ID
        private ?string $warehouseId,
        #[ORM\Column()] // count
        private int $nombreDeReservations,
        #[ORM\Column(type: 'datetime_immutable')] // CSST_RESERVE.RESERVEDATE
        private ?\DateTimeInterface $dateAttendueDeReceptionLaPlusUrgente,
    ) {
    }

    public static function getTableName(int $carlClientId): string
    {
        return '"carl"."carlclient_'.$carlClientId.'_logistique_db_carl_intervention_warehouse_reservation"';
    }

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string
    {
        $dbCarlTableName = self::getTableName($carlClient->getId());

        return <<<FOREIGN_TABLE_CREATION_SQL
CREATE FOREIGN TABLE $dbCarlTableName (
            db_carl_intervention_id                         varchar(33)     OPTIONS (key 'true') NOT NULL,
            warehouse_id                                    varchar(33)     OPTIONS (key 'true'),
            nombre_de_reservations                          integer         NOT NULL,
            date_attendue_de_reception_la_plus_urgente      timestamptz(3)
       ) SERVER "$serverName" OPTIONS (readonly 'true', table '(
            with NOMBRE_DE_RESERVATIONS as (
                select
                    RESERVE.WO_ID as INTERVENTION_ID,
                    ITEMWAREHOUSE.WAREHOUSE_ID as WAREHOUSE_ID,
                    count(*) as NOMBRE_DE_RESERVATIONS,
                    min(RESERVE.RESERVEDATE) as DATE_ATTENDUE_DE_RECEPTION_LA_PLUS_URGENTE
                from
                    "$dbSchema".CSST_RESERVE RESERVE
                left join
                    "$dbSchema".CSST_ITEMWAREHOUSE ITEMWAREHOUSE on ITEMWAREHOUSE.ID = RESERVE.ITEMWAREHOUSE_ID
                group by
                    RESERVE.WO_ID,
                    ITEMWAREHOUSE.WAREHOUSE_ID
            )
            select
                INTERVENTION.ID as INTERVENTION_ID,
                NOMBRE_DE_RESERVATIONS.WAREHOUSE_ID as WAREHOUSE_ID,
                coalesce(NOMBRE_DE_RESERVATIONS.NOMBRE_DE_RESERVATIONS,0) as NOMBRE_DE_RESERVATIONS,
                from_tz(NOMBRE_DE_RESERVATIONS.DATE_ATTENDUE_DE_RECEPTION_LA_PLUS_URGENTE, ''GMT'') as DATE_ATTENDUE_DE_RECEPTION_LA_PLUS_URGENTE
            from
            "$dbSchema".CSWO_WO INTERVENTION
            left join NOMBRE_DE_RESERVATIONS
                on NOMBRE_DE_RESERVATIONS.INTERVENTION_ID = INTERVENTION.ID
        )')
FOREIGN_TABLE_CREATION_SQL;
    }

    public function getDbCarlIntervention(): DbCarlIntervention
    {
        return $this->dbCarlIntervention;
    }

    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }

    public function getNombreDeReservations(): int
    {
        return $this->nombreDeReservations;
    }

    public function getDateAttendueDeReceptionLaPlusUrgente(): ?\DateTimeInterface
    {
        return $this->dateAttendueDeReceptionLaPlusUrgente;
    }
}
