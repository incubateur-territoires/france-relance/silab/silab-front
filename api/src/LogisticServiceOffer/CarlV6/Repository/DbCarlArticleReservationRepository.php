<?php

namespace App\LogisticServiceOffer\CarlV6\Repository;

use App\LogisticServiceOffer\CarlV6\Entity\DbCarlArticleReservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlArticleReservation>
 *
 * @method DbCarlArticleReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlArticleReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlArticleReservation[]    findAll()
 * @method DbCarlArticleReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlArticleReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlArticleReservation::class);
    }
}
