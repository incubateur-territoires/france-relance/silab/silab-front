<?php

namespace App\LogisticServiceOffer\CarlV6\Repository;

use App\LogisticServiceOffer\CarlV6\Entity\DbCarlIntervention;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlIntervention>
 *
 * @method DbCarlIntervention|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlIntervention|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlIntervention[]    findAll()
 * @method DbCarlIntervention[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlInterventionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlIntervention::class);
    }
}
