<?php

namespace App\LogisticServiceOffer\CarlV6\Repository;

use App\LogisticServiceOffer\CarlV6\Entity\DbCarlInterventionWarehouseReservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlInterventionWarehouseReservation>
 *
 * @method DbCarlInterventionWarehouseReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlInterventionWarehouseReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlInterventionWarehouseReservation[]    findAll()
 * @method DbCarlInterventionWarehouseReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlInterventionWarehouseReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlInterventionWarehouseReservation::class);
    }
}
