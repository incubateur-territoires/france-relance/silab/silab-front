<?php

namespace App\LogisticServiceOffer\Gmao\Entity;

use App\LogisticServiceOffer\Gmao\Repository\CarlConfigurationLogistiqueRepository;
use App\Shared\Carl\Entity\CarlClient;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CarlConfigurationLogistiqueRepository::class)]
#[ORM\Table('logisticserviceoffer_carl_gmao_configuration')]
class CarlConfigurationLogistique extends GmaoConfigurationLogistique
{
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?CarlClient $carlClient = null;

    public function getCarlClient(): ?CarlClient
    {
        return $this->carlClient;
    }

    public function setCarlClient(?CarlClient $carlClient): self
    {
        $this->carlClient = $carlClient;

        return $this;
    }

    public function getEnvironnement(): ?string
    {
        return $this->getCarlClient()->getTitle();
    }
}
