<?php

namespace App\LogisticServiceOffer\Gmao\Entity;

use App\LogisticServiceOffer\Gmao\Repository\GmaoConfigurationLogistiqueRepository;
use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use App\LogisticServiceOffer\Mouvement\Entity\TypeDeMouvement;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: GmaoConfigurationLogistiqueRepository::class)]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap([
    'carl' => CarlConfigurationLogistique::class,
])]
#[ORM\Table('logisticserviceoffer_gmao_configuration')]
abstract class GmaoConfigurationLogistique
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[ORM\JoinColumn(nullable: false)]
    protected ?string $title = null;

    /** @var array<TypeDeMouvement> */
    #[Groups([LogisticServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
    private array $typeDInventaires = [];

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return array<TypeDeMouvement>
     */
    public function getTypeDInventaires(): array
    {
        return $this->typeDInventaires;
    }

    /**
     * @param array<TypeDeMouvement> $typeDInventaires
     */
    public function setTypeDInventaires(array $typeDInventaires): self
    {
        $this->typeDInventaires = $typeDInventaires;

        return $this;
    }

    abstract public function getEnvironnement(): ?string;
}
