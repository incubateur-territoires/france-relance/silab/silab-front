<?php

namespace App\LogisticServiceOffer\Gmao\Exception;

use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\Shared\Exception\RuntimeException;

class GmaoConfigurationNEstPasDuTypeCarlConfigurationException extends RuntimeException
{
    public function __construct(string $typeDeGmaoConfiguration, ?\Throwable $previous = null)
    {
        parent::__construct(message: "La configuration GMAO intervention n'est pas du type attendu ".CarlConfigurationLogistique::class.". Type reçu : $typeDeGmaoConfiguration", previous: $previous);
    }
}
