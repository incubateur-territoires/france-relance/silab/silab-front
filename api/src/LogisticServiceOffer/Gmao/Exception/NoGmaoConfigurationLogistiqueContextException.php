<?php

namespace App\LogisticServiceOffer\Gmao\Exception;

use App\Shared\Exception\RuntimeException;

class NoGmaoConfigurationLogistiqueContextException extends RuntimeException
{
    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct(message: 'Aucun contexte de configuration GMAO logistique', previous: $previous);
    }
}
