<?php

namespace App\LogisticServiceOffer\Gmao\Repository;

use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CarlConfigurationLogistique>
 *
 * @method CarlConfigurationLogistique|null find($id, $lockMode = null, $lockVersion = null)
 * @method CarlConfigurationLogistique|null findOneBy(array $criteria, array $orderBy = null)
 * @method CarlConfigurationLogistique[]    findAll()
 * @method CarlConfigurationLogistique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarlConfigurationLogistiqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CarlConfigurationLogistique::class);
    }

    public function save(CarlConfigurationLogistique $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CarlConfigurationLogistique $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
