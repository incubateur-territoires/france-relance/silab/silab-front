<?php

namespace App\LogisticServiceOffer\Gmao\Repository;

use App\LogisticServiceOffer\Gmao\Entity\GmaoConfigurationLogistique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<GmaoConfigurationLogistique>
 *
 * @method GmaoConfigurationLogistique|null find($id, $lockMode = null, $lockVersion = null)
 * @method GmaoConfigurationLogistique|null findOneBy(array $criteria, array $orderBy = null)
 * @method GmaoConfigurationLogistique[]    findAll()
 * @method GmaoConfigurationLogistique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GmaoConfigurationLogistiqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GmaoConfigurationLogistique::class);
    }

    public function save(GmaoConfigurationLogistique $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(GmaoConfigurationLogistique $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
