<?php

namespace App\LogisticServiceOffer\Gmao\Service;

use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\LogisticServiceOffer\Gmao\Exception\GmaoConfigurationNEstPasDuTypeCarlConfigurationException;

class CarlConfigurationLogistiqueContext
{
    public function __construct(
        private GmaoConfigurationLogistiqueContext $gmaoConfigurationLogistiqueContext
    ) {
    }

    public function getCarlConfigurationId(): int
    {
        return $this->getCarlConfiguration()->getId();
    }

    public function getCarlConfiguration(): CarlConfigurationLogistique
    {
        $gmaoConfiguration = $this->gmaoConfigurationLogistiqueContext->getGmaoConfiguration();

        if (!$gmaoConfiguration instanceof CarlConfigurationLogistique) {
            throw new GmaoConfigurationNEstPasDuTypeCarlConfigurationException(get_class($gmaoConfiguration));
        }

        return $gmaoConfiguration;
    }
}
