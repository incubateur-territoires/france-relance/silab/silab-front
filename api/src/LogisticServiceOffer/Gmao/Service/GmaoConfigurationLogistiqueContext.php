<?php

namespace App\LogisticServiceOffer\Gmao\Service;

use App\LogisticServiceOffer\Gmao\Entity\GmaoConfigurationLogistique;
use App\LogisticServiceOffer\Gmao\Exception\NoGmaoConfigurationLogistiqueContextException;
use App\LogisticServiceOffer\Gmao\Repository\GmaoConfigurationLogistiqueRepository;
use App\LogisticServiceOffer\LogisticServiceOffer\Exception\NoLogisticServiceOfferContextException;
use App\LogisticServiceOffer\LogisticServiceOffer\Service\LogisticServiceOfferContext;
use Symfony\Component\HttpFoundation\RequestStack;

class GmaoConfigurationLogistiqueContext
{
    private ?GmaoConfigurationLogistique $gmaoConfiguration = null;
    private bool $hasCachedGmaoConfiguration = false;

    public function __construct(
        private GmaoConfigurationLogistiqueRepository $gmaoConfigurationLogistiqueRepository,
        private LogisticServiceOfferContext $logistiqueServiceOfferContext,
        private RequestStack $requestStack
    ) {
    }

    public function getGmaoConfigurationId(): int
    {
        if ($this->hasCachedGmaoConfiguration && !is_null($this->gmaoConfiguration)) {
            return $this->gmaoConfiguration->getId();
        }

        $gmaoConfigurationLogistiqueId = $this->requestStack->getCurrentRequest()?->get('gmaoConfigurationLogistiqueId');

        // Si on ne nous à pas fourni directement un id de gmaoConfigurationLogistique,
        // on essaie de le déduire de l'offre de service (c'est le cas la plupart du temps)
        if (is_null($gmaoConfigurationLogistiqueId)) {
            try {
                $gmaoConfigurationLogistiqueId = $this->logistiqueServiceOfferContext->getServiceOffer()->getGmaoConfiguration()->getId();
            } catch (\Throwable $e) {
                throw new NoGmaoConfigurationLogistiqueContextException($e);
            }
        }

        return $gmaoConfigurationLogistiqueId;
    }

    public function getGmaoConfiguration(): GmaoConfigurationLogistique
    {
        if ($this->hasCachedGmaoConfiguration) {
            return $this->gmaoConfiguration;
        }

        $this->gmaoConfiguration = $this->gmaoConfigurationLogistiqueRepository->find(
            $this->getGmaoConfigurationId()
        );

        if (is_null($this->gmaoConfiguration)) {
            throw new NoLogisticServiceOfferContextException();
        }

        $this->hasCachedGmaoConfiguration = true;

        return $this->gmaoConfiguration;
    }
}
