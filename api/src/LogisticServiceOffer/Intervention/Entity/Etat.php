<?php

namespace App\LogisticServiceOffer\Intervention\Entity;

class Etat
{
    public function __construct(
        private string $code,
        private string $label,
        private ?string $createdBy,
        private ?\DateTimeInterface $createdAt
    ) {
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }
}
