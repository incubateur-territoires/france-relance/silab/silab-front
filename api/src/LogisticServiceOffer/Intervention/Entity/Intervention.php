<?php

namespace App\LogisticServiceOffer\Intervention\Entity;

use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\LogisticServiceOffer\MouvementDeSortie\Entity\MouvementDeSortieApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

class Intervention
{
    public function __construct(
        #[Groups([MouvementDeSortieApiResource::GROUP_AFFICHER_LISTE_MOUVEMENTS_DE_SORTIE])]
        private string $id,
        private ?string $code,
        private ?string $titre,
        /** @var ?array<ArticleReservation> */
        private ?array $reservations,
        /** @var ?array<Etat> */
        private ?array $historiqueDesEtats,
        private ?string $createdBy,
        private ?string $centreDeCouts,
        private ?\DateTimeInterface $dateAttendueDeReceptionLaPlusUrgente,
        /** @var array<string> */
        private array $equipes,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    /** @return array<ArticleReservation>*/
    public function getReservations(): ?array
    {
        return $this->reservations;
    }

    /** @return array<Etat>*/
    public function getHistoriqueDesEtats(): ?array
    {
        return $this->historiqueDesEtats;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function getCentreDeCouts(): ?string
    {
        return $this->centreDeCouts;
    }

    public function getDateAttendueDeReceptionLaPlusUrgente(): ?\DateTimeInterface
    {
        return $this->dateAttendueDeReceptionLaPlusUrgente;
    }

    /**
     * @return array<string>
     */
    public function getEquipes(): array
    {
        return $this->equipes;
    }
}
