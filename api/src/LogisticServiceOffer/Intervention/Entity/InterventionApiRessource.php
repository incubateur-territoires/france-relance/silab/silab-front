<?php

namespace App\LogisticServiceOffer\Intervention\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\LogisticServiceOffer\Intervention\State\GetCollectionDemandesDeFournituresProvider;
use App\LogisticServiceOffer\Intervention\State\GetInterventionProvider;

#[GetCollection(
    uriTemplate: '/logistic-service-offers/{serviceOfferId}/demandes-de-fournitures',
    uriVariables: ['serviceOfferId' => 'serviceOfferId'],
    security: "is_granted('SERVICEOFFER_ROLE_LOGISTIQUE_CONSULTER_RESERVATIONS')",
    output: Intervention::class,
    provider: GetCollectionDemandesDeFournituresProvider::class,
    order: ['dateAttendueDeReceptionLaPlusUrgente' => 'ASC']
)]
#[Get()]
#[Get(
    uriTemplate: '/logistic-service-offers/{serviceOfferId}/interventions/{id}',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_LOGISTIQUE_CONSULTER_INTERVENTIONS')",
    output: Intervention::class,
    provider: GetInterventionProvider::class,
)]
class InterventionApiRessource
{
}
