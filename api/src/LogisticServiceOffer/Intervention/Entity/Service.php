<?php

namespace App\LogisticServiceOffer\Intervention\Entity;

class Service
{
    public function __construct(
        private string $id,
        private string $libele
    ) {
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setLibele(string $libele): self
    {
        $this->libele = $libele;

        return $this;
    }

    public function getLibele(): string
    {
        return $this->libele;
    }
}
