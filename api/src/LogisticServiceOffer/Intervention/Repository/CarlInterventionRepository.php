<?php

namespace App\LogisticServiceOffer\Intervention\Repository;

use App\LogisticServiceOffer\CarlV6\Repository\DbCarlInterventionRepository;
use App\LogisticServiceOffer\Intervention\Entity\Intervention;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Carl\Trait\UnexpectedOrderByValueCheckingTrait;
use App\Shared\Exception\RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlInterventionRepository implements InterventionRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;
    use UnexpectedOrderByValueCheckingTrait;

    public const NB_MIN_D_INTERVENTION_RETROURNE = 20;

    public function __construct(
        private DbCarlInterventionRepository $dbCarlInterventionRepository
    ) {
    }

    public function update(string $id, Intervention $entity, string $userEmail): Intervention
    {
        throw new RuntimeException('Méthode non implémenté');
    }

    public function find(mixed $id): Intervention
    {
        $sampleInterventionArray = $this->findBy(['id' => [$id]]);

        if (count($sampleInterventionArray) > 1) {
            throw new RuntimeException("Plus d'une intervention trouvée pour l'id : $id");
        }

        if (0 === count($sampleInterventionArray)) {
            throw new RuntimeException("Aucune intervention trouvée pour l'id : $id");
        }

        return array_pop($sampleInterventionArray);
    }

    public function getClassName()
    {
        return Intervention::class;
    }

    public function findAll(): array
    {
        throw new RuntimeException("Cette méthode n'est pas implémentée");
    }

    public function findOneBy(array $criteria): Intervention
    {
        throw new RuntimeException("Cette méthode n'est pas implémentée");
    }

    /**
     * @param array<string, mixed> $criteria
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }

        $includeReservationsFilter = $propertyAccessor->getValue($criteria, '[includeReservations]');
        $includeReservations = false;
        if (true === $includeReservationsFilter) {
            $includeReservations = true;
        }
        unset($criteria['includeReservations']);

        $includeHistoriqueDesEtatsFilter = $propertyAccessor->getValue($criteria, '[includeHistoriqueDesEtats]');
        $includeHistoriqueDesEtats = false;
        if (true === $includeHistoriqueDesEtatsFilter) {
            $includeHistoriqueDesEtats = true;
        }
        unset($criteria['includeHistoriqueDesEtats']);

        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
            ]
        );

        $dbCarlInterventions = $this->dbCarlInterventionRepository->findBy($criteria, $orderBy);

        return array_map(
            function ($dbCarlIntervention) use ($includeReservations, $includeHistoriqueDesEtats) {
                return $dbCarlIntervention->toIntervention($includeReservations, $includeHistoriqueDesEtats);
            },
            $dbCarlInterventions
        );
    }

    // on considère les demandes de fournitures des interventions qui comportent nécessairement des réservations
    public function findDemandesDeFournituresBy(
        string $warehouseId,
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $includeReservationsFilter = $propertyAccessor->getValue($criteria, '[includeReservations]');
        $includeReservations = false;
        if (true === $includeReservationsFilter) {
            $includeReservations = true;
        }
        unset($criteria['includeReservations']);

        $includeHistoriqueDesEtatsFilter = $propertyAccessor->getValue($criteria, '[includeHistoriqueDesEtats]');
        $includeHistoriqueDesEtats = false;
        if (true === $includeHistoriqueDesEtatsFilter) {
            $includeHistoriqueDesEtats = true;
        }
        unset($criteria['includeHistoriqueDesEtats']);

        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'likeCode',
                'likeTitre',
                'likeCreatedBy',
            ]
        );

        $interventionQueryBuilder = $this->dbCarlInterventionRepository->createQueryBuilder('dbCarlIntervention')
            ->innerJoin('dbCarlIntervention.dbCarlInterventionWarehouseReservations', 'dbCarlInterventionWarehouseReservation')
            ->andWhere('dbCarlInterventionWarehouseReservation.warehouseId = :reservations_warehouseId')
            ->andWhere('dbCarlInterventionWarehouseReservation.nombreDeReservations > 0')
            ->setParameter('reservations_warehouseId', $warehouseId);

        $likeCode = $propertyAccessor->getValue($criteria, '[likeCode]');
        if (!empty($likeCode)) {
            $sanitizedlikeCode = strtolower('%'.str_replace('*', '%', $likeCode).'%');
            $interventionQueryBuilder
                ->andWhere('LOWER(dbCarlIntervention.code) LIKE :likeCode')
                ->setParameter('likeCode', $sanitizedlikeCode);
        }

        $likeTitre = $propertyAccessor->getValue($criteria, '[likeTitre]');
        if (!empty($likeTitre)) {
            $sanitizedlikeTitre = strtolower('%'.str_replace('*', '%', $likeTitre).'%');
            $interventionQueryBuilder
                ->andWhere('LOWER(dbCarlIntervention.titre) LIKE :likeTitre')
                ->setParameter('likeTitre', $sanitizedlikeTitre);
        }

        $likeCreatedBy = $propertyAccessor->getValue($criteria, '[likeCreatedBy]');
        if (!empty($likeCreatedBy)) {
            $sanitizedlikeCreatedBy = strtolower('%'.str_replace('*', '%', $likeCreatedBy).'%');
            $interventionQueryBuilder
                ->andWhere('LOWER(dbCarlIntervention.createdBy) LIKE :likeCreatedBy')
                ->setParameter('likeCreatedBy', $sanitizedlikeCreatedBy);
        }

        if (array_key_exists('dateAttendueDeReceptionLaPlusUrgente', $orderBy)) {
            $direction = $orderBy['dateAttendueDeReceptionLaPlusUrgente'];
            $interventionQueryBuilder->addOrderBy('dbCarlInterventionWarehouseReservation.dateAttendueDeReceptionLaPlusUrgente', $direction);
            unset($orderBy['dateAttendueDeReceptionLaPlusUrgente']);
        }
        foreach ($orderBy as $field => $direction) {
            $interventionQueryBuilder->addOrderBy("dbCarlIntervention.$field", $direction);
        }

        $dbCarlInterventions = $interventionQueryBuilder
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        return array_map(
            function ($dbCarlIntervention) use ($includeReservations, $includeHistoriqueDesEtats) {
                return $dbCarlIntervention->toIntervention($includeReservations, $includeHistoriqueDesEtats);
            },
            $dbCarlInterventions
        );
    }
}
