<?php

namespace App\LogisticServiceOffer\Intervention\Repository;

use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\LogisticServiceOffer\Gmao\Service\GmaoConfigurationLogistiqueContext;
use App\LogisticServiceOffer\Intervention\Entity\Intervention;
use App\Shared\Exception\RuntimeException;

class InterventionRepository implements InterventionRepositoryInterface
{
    private InterventionRepositoryInterface $specificInterventionRepository;

    public function __construct(
        CarlInterventionRepository $carlInterventionRepository,
        GmaoConfigurationLogistiqueContext $gmaoConfigurationLogistiqueContext
    ) {
        try {
            $this->specificInterventionRepository = match ($gmaoConfigurationLogistiqueContext->getGmaoConfiguration()::class) {
                CarlConfigurationLogistique::class => $carlInterventionRepository,
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfigurationLogistiqueContext->getGmaoConfiguration()::class);
        }
    }

    public function find($id): Intervention
    {
        return $this->specificInterventionRepository->find($id);
    }

    public function findAll(): array
    {
        return $this->specificInterventionRepository->findAll();
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->specificInterventionRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findDemandesDeFournituresBy(string $warehouseId, array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->specificInterventionRepository->findDemandesDeFournituresBy($warehouseId, $criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): Intervention
    {
        return $this->specificInterventionRepository->findOneBy($criteria);
    }

    public function getClassName()
    {
        return Intervention::class;
    }
}
