<?php

namespace App\LogisticServiceOffer\Intervention\Repository;

use App\LogisticServiceOffer\Intervention\Entity\Intervention;
use Doctrine\Persistence\ObjectRepository;

interface InterventionRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): Intervention;

    /**
     * @param array<string,mixed>       $criteria
     * @param array<string,string>|null $orderBy
     *
     * @return array<Intervention>
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array;

    /**
     * @param array<string,mixed>       $criteria
     * @param array<string,string>|null $orderBy
     *
     * @return array<Intervention>
     */
    public function findDemandesDeFournituresBy(
        string $warehouseId,
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array;

    /**
     * @return array<Intervention>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): Intervention;
}
