<?php

namespace App\LogisticServiceOffer\Intervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\LogisticServiceOffer\Intervention\Entity\Intervention;
use App\LogisticServiceOffer\Intervention\Repository\InterventionRepository;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;

final class GetCollectionDemandesDeFournituresProvider implements ProviderInterface
{
    public function __construct(private LogisticServiceOfferRepository $logisticServiceOfferRepository,
        private InterventionRepository $interventionRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<Intervention>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $logisticServiceOffer = $this->logisticServiceOfferRepository->find($uriVariables['serviceOfferId']);

        $criteria = $context['filters'] ?? [];

        $order = $operation->getOrder();

        return $this->interventionRepository->findDemandesDeFournituresBy($logisticServiceOffer->getWarehouseId(), $criteria, $order, 50); // Amelioration: limite custom, pagination
    }
}
