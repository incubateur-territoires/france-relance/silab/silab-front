<?php

namespace App\LogisticServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\LogisticServiceOffer\Intervention\Entity\Intervention;
use App\LogisticServiceOffer\Intervention\Repository\InterventionRepository;

final class GetInterventionProvider implements ProviderInterface
{
    public function __construct(
        private InterventionRepository $interventionRepository,
    ) {
    }

    /**
     * Fournit au endpoint getAllIntervention les datas provenants du connecteur Carl.
     *
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): Intervention
    {
        assert($operation instanceof Get);

        return $this->interventionRepository->find($uriVariables['id']);
    }
}
