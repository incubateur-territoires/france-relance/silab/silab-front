<?php

namespace App\LogisticServiceOffer\LogisticServiceOffer\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Put;
use App\LogisticServiceOffer\Gmao\Entity\GmaoConfigurationLogistique;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;
use App\LogisticServiceOffer\LogisticServiceOffer\State\GetLogisticServiceOfferDataProvider;
use App\LogisticServiceOffer\LogisticServiceOffer\State\UpdateLogisticServiceOfferProcessor;
use App\ServiceOffer\Role\ValueObject\Role;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[Get(
    uriTemplate: '/logistic-service-offers/{id}',
    security: "is_granted('SERVICEOFFER_ROLE_LOGISTIQUE_CONSULTER_TYPE_INVENTAIRE')",
    provider: GetLogisticServiceOfferDataProvider::class,
    normalizationContext: ['groups' => [LogisticServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS]]
)]
#[Put(
    uriTemplate: '/logistic-service-offers/{id}',
    security: "is_granted('SERVICEOFFER_ROLE_LOGISTIQUE_MODIFIER_TYPE_INVENTAIRE')",
    processor: UpdateLogisticServiceOfferProcessor::class, // sert uniquement à modifier  l'inventaireID
    // AMELIORATION: ne pas utiliser de processor mais un voter pour vérifier que l'objet fournit ne possède uniquement des champ que l'utilisateur à le droit de modifier.
    // AMELIORATION: passer en PATCH pour respect rfc
    // Actuellement rappel un PUT modifie les uniquement les champs fournits https://api-platform.com/docs/core/operations/
    normalizationContext: ['groups' => [LogisticServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS]]
)]
#[ORM\Entity(repositoryClass: LogisticServiceOfferRepository::class)]
#[Groups([LogisticServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
class LogisticServiceOffer extends ServiceOffer
{
    public const GROUP_AFFICHER_DETAILS_ODS = 'Afficher le détail des offres der service logistique';

    #[ORM\Column(length: 255)]
    private ?string $warehouseId = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?GmaoConfigurationLogistique $gmaoConfigurationLogistique;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $typeDInventaireId;

    /**
     * @return array<Role>
     */
    public function getAvailableRoles(): array
    {
        $availableRoles = [
            new Role(value: 'SERVICEOFFER_'.$this->getId().'_ROLE_LOGISTIQUE_SUPERVISEUR', libelle: 'Superviseur', description: "Peut réaliser l'ensemble des tâches des magasiniers et inventoristes"),
            new Role(value: 'SERVICEOFFER_'.$this->getId().'_ROLE_LOGISTIQUE_MAGASINIER', libelle: 'Magasinier', description: 'Peut réaliser des sorties de stock'),
            new Role(value: 'SERVICEOFFER_'.$this->getId().'_ROLE_LOGISTIQUE_INVENTORISTE', libelle: 'Inventoriste', description: 'Peut réaliser des inventaires'),
        ];

        return array_merge(parent::getAvailableRoles(), $availableRoles);
    }

    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }

    public function setWarehouseId(string $warehouseId): self
    {
        $this->warehouseId = $warehouseId;

        return $this;
    }

    public function getTemplate(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    public function getGmaoConfiguration(): ?GmaoConfigurationLogistique
    {
        return $this->gmaoConfigurationLogistique;
    }

    public function setGmaoConfiguration(?GmaoConfigurationLogistique $gmaoConfigurationLogistique): self
    {
        $this->gmaoConfigurationLogistique = $gmaoConfigurationLogistique;

        return $this;
    }

    public function getTypeDInventaireId(): ?string
    {
        return $this->typeDInventaireId;
    }

    public function setTypeDInventaireId(?string $typeDInventaireId): self
    {
        $this->typeDInventaireId = $typeDInventaireId;

        return $this;
    }
}
