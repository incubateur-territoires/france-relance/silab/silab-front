<?php

namespace App\LogisticServiceOffer\LogisticServiceOffer\Exception;

class NoLogisticServiceOfferContextException extends \RuntimeException
{
    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct(message: "Aucun contexte d'offre de service logistique", previous: $previous);
    }
}
