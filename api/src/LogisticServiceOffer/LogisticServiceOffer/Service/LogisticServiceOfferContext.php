<?php

namespace App\LogisticServiceOffer\LogisticServiceOffer\Service;

use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use App\LogisticServiceOffer\LogisticServiceOffer\Exception\NoLogisticServiceOfferContextException;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;
use App\ServiceOffer\ServiceOffer\Exception\NoServiceOfferContextException;
use App\ServiceOffer\ServiceOffer\Service\ServiceOfferContextInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class LogisticServiceOfferContext implements ServiceOfferContextInterface
{
    private ?LogisticServiceOffer $serviceOffer = null;
    private bool $hasCachedServiceOffer = false;

    public function __construct(
        private LogisticServiceOfferRepository $logisticServiceOfferRepository,
        private RequestStack $requestStack,
    ) {
    }

    public function getServiceOfferId(): int
    {
        if ($this->hasCachedServiceOffer && !is_null($this->serviceOffer)) {
            return $this->serviceOffer->getId();
        }

        $serviceOfferId = $this->requestStack->getCurrentRequest()?->get('serviceOfferId');

        if (is_null($serviceOfferId)) {
            throw new NoServiceOfferContextException();
        }

        return $serviceOfferId;
    }

    public function getServiceOffer(): LogisticServiceOffer
    {
        if ($this->hasCachedServiceOffer) {
            return $this->serviceOffer;
        }

        $this->serviceOffer = $this->logisticServiceOfferRepository->find(
            $this->getServiceOfferId()
        );

        if (is_null($this->serviceOffer)) {
            throw new NoLogisticServiceOfferContextException();
        }

        $this->hasCachedServiceOffer = true;

        return $this->serviceOffer;
    }

    public function getServiceOfferSafe(): ?LogisticServiceOffer
    {
        try {
            return $this->getServiceOffer();
        } catch (NoLogisticServiceOfferContextException|NoServiceOfferContextException $e) {
            // ne rien faire
        }

        return null;
    }
}
