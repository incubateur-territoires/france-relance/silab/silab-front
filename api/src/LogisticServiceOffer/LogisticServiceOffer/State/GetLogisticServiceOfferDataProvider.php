<?php

namespace App\LogisticServiceOffer\LogisticServiceOffer\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;
use App\LogisticServiceOffer\Mouvement\Repository\MouvementRepository;

class GetLogisticServiceOfferDataProvider implements ProviderInterface
{
    public function __construct(
        private LogisticServiceOfferRepository $logisticServiceOfferRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): LogisticServiceOffer
    {
        assert($operation instanceof Get);

        $logisticServiceOffer = $this->logisticServiceOfferRepository->find($uriVariables['id']);

        $mouvementRepository = new MouvementRepository($logisticServiceOffer->getGmaoConfiguration());

        $logisticServiceOffer->getGmaoConfiguration()->setTypeDInventaires(
            $mouvementRepository->getTypeDeMouvementsInventaire()
        );

        return $logisticServiceOffer;
    }
}
