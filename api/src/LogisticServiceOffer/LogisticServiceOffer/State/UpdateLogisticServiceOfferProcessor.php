<?php

namespace App\LogisticServiceOffer\LogisticServiceOffer\State;

use ApiPlatform\Metadata\Operation as ApiPlatformOperation;
use ApiPlatform\Metadata\Put;
use ApiPlatform\State\ProcessorInterface;
use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;

final class UpdateLogisticServiceOfferProcessor implements ProcessorInterface
{
    public function __construct(private LogisticServiceOfferRepository $logisticServiceOfferRepository)
    {
    }

    /**
     * @param LogisticServiceOffer $data
     * @param array<mixed>         $uriVariables
     * @param array<mixed>         $context
     */
    public function process(
        mixed $data,
        ApiPlatformOperation $operation,
        array $uriVariables = [],
        array $context = []
    ): LogisticServiceOffer {
        assert($operation instanceof Put, 'Opération non supportée');
        assert($data instanceof LogisticServiceOffer);
        $updatedEntity = $this->logisticServiceOfferRepository->find($uriVariables['id'])?->setTypeDInventaireId($data->getTypeDInventaireId());

        $this->logisticServiceOfferRepository->save($updatedEntity, true);

        return $this->logisticServiceOfferRepository->find($uriVariables['id']);
    }
}
