<?php

namespace App\LogisticServiceOffer\Mouvement\Entity;

use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Emplacement\Entity\Emplacement;

class CreateMouvement
{
    public function __construct(
        protected Article $article,
        protected string $magasinId,
        protected TypeDeMouvement $typeDeMouvement,
        protected ?float $quantite,
        protected ?string $createdBy,
        protected ?Emplacement $emplacement,
    ) {
    }

    public static function fromApiResourceInMagasin(MouvementApiResource $mouvementApiResource, string $magasinId): CreateMouvement
    {
        return new self(
            quantite: $mouvementApiResource->getQuantité(),
            article: $mouvementApiResource->getArticle(),
            emplacement: $mouvementApiResource->getEmplacement(),
            magasinId: $magasinId,
            typeDeMouvement: $mouvementApiResource->getTypeDeMouvement(),
            createdBy: $mouvementApiResource->getCreatedBy()
        );
    }

    public function setQuantite(?float $quantité): self
    {
        $this->quantite = $quantité;

        return $this;
    }

    public function getQuantite(): ?float
    {
        return $this->quantite;
    }

    public function setArticle(Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getArticle(): Article
    {
        return $this->article;
    }

    public function setEmplacement(Emplacement $emplacement): self
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    public function getEmplacement(): ?Emplacement
    {
        return $this->emplacement;
    }

    public function setMagasinId(string $magasinId): self
    {
        $this->magasinId = $magasinId;

        return $this;
    }

    public function getMagasinId(): string
    {
        return $this->magasinId;
    }

    public function setTypeDeMouvement(TypeDeMouvement $typeDeMouvement): self
    {
        $this->typeDeMouvement = $typeDeMouvement;

        return $this;
    }

    public function getTypeDeMouvement(): ?TypeDeMouvement
    {
        return $this->typeDeMouvement;
    }

    public function setCreatedBy(?string $userEmail): self
    {
        $this->createdBy = $userEmail;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }
}
