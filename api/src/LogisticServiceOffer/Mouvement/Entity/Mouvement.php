<?php

namespace App\LogisticServiceOffer\Mouvement\Entity;

use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Emplacement\Entity\Emplacement;

class Mouvement extends CreateMouvement implements MouvementInterface
{
    public function __construct(
        private string $id,
        Article $article,
        string $magasinId,
        TypeDeMouvement $typeDeMouvement,
        private \DateTime $createdAt,
        ?float $quantite,
        ?string $createdBy,
        ?Emplacement $emplacement,
    ) {
        $this->id = $id;
        $this->createdAt = $createdAt;

        parent::__construct(
            quantite: $quantite,
            article: $article,
            emplacement: $emplacement,
            magasinId: $magasinId,
            typeDeMouvement: $typeDeMouvement,
            createdBy: $createdBy
        );
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function toApiRessource(): MouvementApiResource
    {
        return new MouvementApiResource(
            id: $this->getId(),
            quantité: $this->getQuantite(),
            article: $this->getArticle(),
            emplacement: $this->getEmplacement(),
            typeDeMouvement: $this->getTypeDeMouvement(),
            createdBy: $this->getCreatedBy(),
            createdAt: $this->getCreatedAt()
        );
    }
}
