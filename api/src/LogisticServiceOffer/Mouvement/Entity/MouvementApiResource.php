<?php

namespace App\LogisticServiceOffer\Mouvement\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Emplacement\Entity\Emplacement;
use App\LogisticServiceOffer\Mouvement\State\CreateMouvementProcessor;
use App\LogisticServiceOffer\Mouvement\State\GetCollectionMouvementProvider;
use Symfony\Component\Serializer\Annotation\Groups;

#[Post(
    read: false,
    uriTemplate: '/logistic-service-offers/{serviceOfferId}/mouvements',
    uriVariables: ['serviceOfferId' => 'serviceOfferId'],
    security: "is_granted('SERVICEOFFER_ROLE_LOGISTIQUE_INVENTORIER_ARTICLES')",
    processor: CreateMouvementProcessor::class,
    normalizationContext: ['groups' => [self::GROUP_AFFICHER_MOUVEMENT]],
    denormalizationContext: ['groups' => [self::GROUP_CREER_MOUVEMENT]],
)]
#[GetCollection(
    uriTemplate: '/logistic-service-offers/{serviceOfferId}/mouvements',
    uriVariables: ['serviceOfferId' => 'serviceOfferId'],
    security: "is_granted('SERVICEOFFER_ROLE_LOGISTIQUE_CONSULTER_ARTICLES')",
    provider: GetCollectionMouvementProvider::class,
    normalizationContext: ['groups' => [self::GROUP_AFFICHER_MOUVEMENT]],
)]
class MouvementApiResource
{
    public const GROUP_CREER_MOUVEMENT = 'Créer un mouvement';
    // Pour des questions de perfomance nous retournons uniquement les ids des entités imbriquées.
    public const GROUP_AFFICHER_MOUVEMENT = 'Afficher informations d\' un mouvement';

    public function __construct(
        #[Groups([self::GROUP_CREER_MOUVEMENT, self::GROUP_AFFICHER_MOUVEMENT])]
        private Article $article,
        #[Groups([self::GROUP_CREER_MOUVEMENT, self::GROUP_AFFICHER_MOUVEMENT])]
        private ?float $quantité = null,
        #[Groups([self::GROUP_CREER_MOUVEMENT, self::GROUP_AFFICHER_MOUVEMENT])]
        private ?Emplacement $emplacement = null,
        #[Groups([self::GROUP_CREER_MOUVEMENT, self::GROUP_AFFICHER_MOUVEMENT])]
        private ?TypeDeMouvement $typeDeMouvement = null,
        #[Groups([self::GROUP_AFFICHER_MOUVEMENT])]
        private ?string $id = null,
        #[Groups([self::GROUP_AFFICHER_MOUVEMENT])]
        private ?string $createdBy = null,
        #[Groups([self::GROUP_AFFICHER_MOUVEMENT])]
        private ?\DateTime $createdAt = null,
    ) {
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getQuantité(): ?float
    {
        return $this->quantité;
    }

    public function getArticle(): Article
    {
        return $this->article;
    }

    public function getEmplacement(): ?Emplacement
    {
        return $this->emplacement;
    }

    public function getTypeDeMouvement(): ?TypeDeMouvement
    {
        return $this->typeDeMouvement;
    }

    public function setCreatedBy(?string $createdby): self
    {
        $this->createdBy = $createdby;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }
}
