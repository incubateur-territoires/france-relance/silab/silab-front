<?php

namespace App\LogisticServiceOffer\Mouvement\Entity;

use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Emplacement\Entity\Emplacement;

interface MouvementInterface
{
    public function getId(): string;

    public function getArticle(): Article;

    public function getMagasinId(): string;

    public function getCreatedAt(): \DateTimeInterface;

    public function getQuantite(): ?float;

    public function getEmplacement(): ?Emplacement;

    public function getCreatedBy(): ?string;
}
