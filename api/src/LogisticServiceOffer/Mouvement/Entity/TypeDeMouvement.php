<?php

namespace App\LogisticServiceOffer\Mouvement\Entity;

use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use Symfony\Component\Serializer\Annotation\Groups;

#[Groups([MouvementApiResource::GROUP_CREER_MOUVEMENT, MouvementApiResource::GROUP_AFFICHER_MOUVEMENT, LogisticServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
class TypeDeMouvement
{
    public function __construct(
        private string $id,
        private ?string $libele = null
    ) {
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setLibele(string $libele): self
    {
        $this->libele = $libele;

        return $this;
    }

    public function getLibele(): ?string
    {
        return $this->libele;
    }
}
