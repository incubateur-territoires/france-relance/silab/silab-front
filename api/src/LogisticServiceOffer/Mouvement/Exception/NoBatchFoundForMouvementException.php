<?php

namespace App\LogisticServiceOffer\Mouvement\Exception;

use App\Shared\Exception\RuntimeException;

class NoBatchFoundForMouvementException extends RuntimeException
{
}
