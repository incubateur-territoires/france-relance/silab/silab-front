<?php

namespace App\LogisticServiceOffer\Mouvement\Repository;

use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Emplacement\Entity\Emplacement;
use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\LogisticServiceOffer\Mouvement\Entity\CreateMouvement;
use App\LogisticServiceOffer\Mouvement\Entity\Mouvement;
use App\LogisticServiceOffer\Mouvement\Entity\TypeDeMouvement;
use App\LogisticServiceOffer\Mouvement\Exception\NoBatchFoundForMouvementException;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Exception\RuntimeException;
use App\Shared\JsonApi\JsonApiResource;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlMouvementRepository implements MouvementRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;

    public function __construct(
        private CarlConfigurationLogistique $carlConfigurationLogistique
    ) {
    }
    /** @var array<TypeDeMouvement> */
    private array $inventoryMovementType = [];

    /**
     * @return array<TypeDeMouvement>
     */
    public function getTypeDeMouvementsInventaire(): array
    {
        return $this->getInventoryMovementTypesCached();
    }

    /**
     * @return array<TypeDeMouvement>
     */
    private function getInventoryMovementTypesCached(): array
    {
        if (empty($this->inventoryMovementType)) {
            $inventoryMovementTypesJsonApiRessources = $this->carlConfigurationLogistique->getCarlClient()->getCarlObjects(
                entity: 'movementtype',
                filters: ['category' => ['INVENTORY']],
                fields: ['movementtype' => ['description', 'category', 'defaultMovement']]);
            foreach ($inventoryMovementTypesJsonApiRessources as $movementTypeJsonApiRessources) {
                $this->inventoryMovementType[] = new TypeDeMouvement($movementTypeJsonApiRessources->getId(), $movementTypeJsonApiRessources->getAttribute('description') ?? '');
            }
        }

        return $this->inventoryMovementType;
    }

    /**
     * @return array<string>
     */
    private function getInventoryMovementTypesIdsCached(): array
    {
        $inventoryMovementTypes = $this->getInventoryMovementTypesCached();

        return array_map(function (TypeDeMouvement $inventoryMovementType) {
            return $inventoryMovementType->getId();
        }, $inventoryMovementTypes);
    }

    public function find($id): Mouvement
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }
        if (null !== $orderBy) {
            throw new RuntimeException('Le paramètre orderBy n\'est pas encore implémenté');
        }
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'article.id',
                'article.code',
                'warehouse.id',
            ]
        );

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $filters = [];

        $itemId = $propertyAccessor->getValue($criteria, '[article.id]');
        if (!empty($itemId)) {
            $filters['item.id'] = $itemId;
        }

        $itemCode = $propertyAccessor->getValue($criteria, '[article.code]');
        if (!empty($itemCode)) {
            $filters['item.code'] = $itemCode;
        }

        $warehouseId = $propertyAccessor->getValue($criteria, '[warehouse.id]');
        if (!empty($warehouseId)) {
            $filters['warehouse.id'] = $warehouseId;
        }

        $includes = [
            'item',
            'location',
            'movementType',
            'createdBy',
            'warehouse',
        ];

        $fields = [
            'stockmovement' => [
                'movementQuantity',
                'movementDate',
                'createdBy',
                'item',
                'location',
                'movementType',
                'warehouse',
            ],
            'item' => [
                'code',
                'description',
                'unit',
            ],
            'location' => [
                'code',
            ],
            'movementtype' => [
                'description',
            ],
            'actor' => [
                'fullName',
            ],
            'warehouse' => [
                'id',
            ],
        ];

        $movementCategoriesCode = $this->getMovementCategoriesCode();
        $mouvementApartirDuQuelLHistoriquePeutContenirDesTrous = null;
        $mouvements = [];
        foreach ($movementCategoriesCode as $movementCategory) {
            $filters['movementType.category'] = [$movementCategory];
            $movementsApiResource =
                $this->carlConfigurationLogistique->getCarlClient()->getCarlObjects(
                    entity: 'stockmovement',
                    filters: $filters,
                    includes: $includes,
                    fields: $fields,
                    sort: ['-movementDate'], // Si ce tri est modifié l'algo suivant est obselète
                );

            foreach ($movementsApiResource as $movementApiResource) {
                $mouvements[] = new Mouvement(
                    id: $movementApiResource->getId(),
                    quantite: $movementApiResource->getAttribute('movementQuantity'),
                    article: new Article(
                        id: $movementApiResource->getRelationship('item')->getId(),
                    ),
                    magasinId: $movementApiResource->getRelationship('warehouse')->getId(),
                    typeDeMouvement: new TypeDeMouvement(
                        id: $movementApiResource->getRelationship('movementType')->getId(),
                        libele: $movementApiResource->getRelationship('movementType')->getAttribute('description') ?? '',
                    ),
                    emplacement: $movementApiResource->getRelationship('location')
                        ? new Emplacement(
                            id: $movementApiResource->getRelationship('location')->getId(),
                            code: $movementApiResource->getRelationship('location')->getAttribute('code'),
                        )
                        : null,
                    createdBy: $movementApiResource->getRelationship('createdBy')->getAttribute('fullName'),
                    createdAt: new \DateTime($movementApiResource->getAttribute('movementDate')),
                );
            }

            $mouvementLePlusAncienRetournéPourCetteCatégorie = end($mouvements);
            $limitDElementRetournéParCarlAtteinte = count($movementsApiResource) === $this->carlConfigurationLogistique->getCarlClient()->getPageLimit();
            // Afin d'assurer un historique des mouvements sans trou :
            //      dans le cas où le nombre d'élément retourné est limité, on stock la dernière valeur.
            //      si une valeur est déjà stocké on garde la plus récente.
            if (
                $limitDElementRetournéParCarlAtteinte
                && (
                    null === $mouvementApartirDuQuelLHistoriquePeutContenirDesTrous
                    || $mouvementApartirDuQuelLHistoriquePeutContenirDesTrous->getCreatedAt() < $mouvementLePlusAncienRetournéPourCetteCatégorie->getCreatedAt()
                )
            ) {
                $mouvementApartirDuQuelLHistoriquePeutContenirDesTrous = $mouvementLePlusAncienRetournéPourCetteCatégorie;
            }
        }

        usort(
            $mouvements,
            function (Mouvement $mouvementA, Mouvement $mouvementB) {
                return $mouvementA->getCreatedAt()->getTimestamp() - $mouvementB->getCreatedAt()->getTimestamp();
            }
        );

        if (null !== $mouvementApartirDuQuelLHistoriquePeutContenirDesTrous) {
            array_splice(
                $mouvements,
                0,
                array_search($mouvementApartirDuQuelLHistoriquePeutContenirDesTrous, $mouvements)
            );
        }

        return $mouvements;
    }

    /**
     * Récupère la liste de valeur.
     *
     * @return array<string>
     */
    private function getMovementCategoriesCode(): array
    {
        $movementCategoriesApiRessource = $this->carlConfigurationLogistique->getCarlClient()->getCarlObjects(
            entity: 'valuelistitem',
            filters: ['valueList.id' => ['MVTCATEGORY']],
            fields: ['valuelistitem' => ['code']],
        );
        $movementCategories = [];
        foreach ($movementCategoriesApiRessource as $movementCategoryApiRessource) {
            $movementCategories[] = $movementCategoryApiRessource->getAttribute('code');
        }

        return $movementCategories;
    }

    public function findOneBy(array $criteria): Mouvement
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function getClassName()
    {
        return Mouvement::class;
    }

    public function save(CreateMouvement $createMouvement): Mouvement
    {
        // A la différence d'une 'issue' poster un  'inventory' ne met pas à jour les données de prix, de stock et la date du dernier inventaire.
        // Il faut donc le faire via d'autre requêtes, soit mettre à jour le batch existant, soit un créer un.
        try {
            $batchJsonApiResource = $this->findBatchFromMouvement($createMouvement);
            $mouvement = $this->postInventory(
                $createMouvement,
                $batchJsonApiResource->getAttribute('unitPriceHT')
            );
            // on à bien trouvé un batch correspondant, il suffit alors de le mettre à jour
            $this->updateBatchQuantityAccordingToStockMovement($mouvement, $batchJsonApiResource);

            return $mouvement;
        } catch (NoBatchFoundForMouvementException $e) {
            // si aucun batch n'a été trouvé, cela veut dire qu'il y à un stock de zéro et il faudra en créer un
            $itemPriceForWarehouse = $this->getArticlePriceForWarehouse(
                articleId: $createMouvement->getArticle()->getId(),
                warehouseId: $createMouvement->getMagasinId()
            );
            if (0. !== $createMouvement->getQuantite()) {
                // IMPORTANT: sur carl créer un batch génère AUTOMATIQUENT un mouvement d'inventaire (avec le type par default de l'application carl, cf commentaires de la méthode createBatchForStockMovement)
                // on n'a donc pas besoin de créer le mouvement
                $this->createBatchForStockMovement(mouvement: $createMouvement, unitprice: $itemPriceForWarehouse);
                // Le probleme est que le mouvement créé ne comporte pas le nom du magasinier, comme solution on choisit donc de créé un nouveau mouvement de stock "vide"
                // pour savoir qui à fait cette relève... (on à essayé de patcher le mouvement mais carl nous laisse pas passer :( )
                $autoGeneratedMovement = $this->getAutoGeneratedStockMovement(createMouvement: $createMouvement);
                $this->postInventory(
                    $createMouvement->setQuantite(0),
                    0.
                );

                return $autoGeneratedMovement;
            } else {
                // dans le cas ou on relève une Qté de zero, on doit faire le mouvement
                $mouvement = $this->postInventory(
                    $createMouvement,
                    $itemPriceForWarehouse
                );

                return $mouvement;
            }
        }
    }

    private function updateBatchQuantityAccordingToStockMovement(Mouvement $mouvement, JsonApiResource $batchJsonApiResource): void
    {
        $this->carlConfigurationLogistique->getCarlClient()->patchCarlObject(
            'batch',
            $batchJsonApiResource->getId(),
            attributes: [
                'quantity' => $batchJsonApiResource->getAttribute('quantity') + $mouvement->getQuantite(),
                'lastInventory' => date_format($mouvement->getCreatedAt(), 'c'),
            ]
        );
    }

    private function getArticlePriceForWarehouse(string $articleId, string $warehouseId): float
    {
        return $this->carlConfigurationLogistique->getCarlClient()->getOneCarlObject(
            entity: 'itemwarehouse',
            filters: [
                'item.id' => [$articleId],
                'warehouse.id' => [$warehouseId],
            ],
            fields: [
                'itemwarehouse' => ['PMP'],
            ]
        )->getAttribute('PMP');
    }

    private function getDefaultMovementTypeId(string $mouvementcategory): string
    {
        return $this->carlConfigurationLogistique->getCarlClient()->getOneCarlObject(
            entity: 'movementtype',
            filters: [
                'category' => [$mouvementcategory],
                'defaultMovement' => ['true'],
            ],
            fields: ['movementtype' => ['description', 'category', 'defaultMovement']]
        )->getId();
    }

    private function setInventoryDefaultMovementType(string $movementTypeId): void
    {
        foreach ($this->getInventoryMovementTypesIdsCached() as $inventoryMovementTypeId) {
            $this->carlConfigurationLogistique->getCarlClient()->patchCarlObject(
                entity: 'movementtype',
                objectId: $inventoryMovementTypeId,
                attributes: [
                    'defaultMovement' => 'false',
                ]
            );
        }
        $this->carlConfigurationLogistique->getCarlClient()->patchCarlObject(
            entity: 'movementtype',
            objectId: $movementTypeId,
            attributes: [
                'defaultMovement' => 'true',
            ]
        );
    }

    private function createBatchForStockMovement(CreateMouvement $mouvement, float $unitprice): void
    {
        // IMPORTANT: sur carl créer un batch génère AUTOMATIQUENT un mouvement d'inventaire avec le type par default de l'application carl
        // nous devons donc nous assurer que l'inventiare par défaut corresponde au notre.
        // Pour ce faire, on modifie (si nécéssaire) temporairement le type par défaut de l'inventaire carl et on le rétabli après la manoeuvre
        $inventoryDefaultMouvementTypeId = $this->getDefaultMovementTypeId('INVENTORY');
        try {
            if ($mouvement->getTypeDeMouvement()->getId() !== $inventoryDefaultMouvementTypeId) {
                $this->setInventoryDefaultMovementType($mouvement->getTypeDeMouvement()->getId());
            }

            // Note intéressante, carl nous empêche d'inventorier deux fois le même article avec l'erreur suivante : "Impossible d'inventorier deux fois le même lot de l'article ********"
            $batchArray = [
                'data' => [
                    'attributes' => [
                        'quantity' => $mouvement->getQuantite(),
                        'unitPriceHT' => $unitprice,
                    ],
                    'relationships' => [
                        'item' => [
                            'data' => [
                                'id' => $mouvement->getArticle()->getId(),
                                'type' => 'item',
                            ],
                        ],
                        'warehouse' => [
                            'data' => [
                                'id' => $mouvement->getMagasinId(),
                                'type' => 'warehouse',
                            ],
                        ],
                    ],
                ],
            ];

            $ilYAUnEmplacement = !is_null($mouvement->getEmplacement());
            if ($ilYAUnEmplacement) {
                $batchArray['data']['relationships']['location'] = [
                    'data' => [
                        'id' => $this->getItemLocationId(
                            $mouvement->getArticle()->getId(),
                            $mouvement->getEmplacement()->getId()
                        ),
                        'type' => 'itemlocation',
                    ],
                ];
            }

            $this->carlConfigurationLogistique->getCarlClient()->postCarlObject(
                entity: 'batch',
                postedResource: json_encode($batchArray)
            );
        } finally {
            if ($mouvement->getTypeDeMouvement()->getId() !== $inventoryDefaultMouvementTypeId) {
                $this->setInventoryDefaultMovementType($inventoryDefaultMouvementTypeId);
            }
        }
    }

    /**
     * Cette méthode est utilisée pour récupérer les information d'un mouvement de stock généré automatiquement par carl lors de la création d'un batch.
     * N'ayant pas l'id de ce mouvement généré, on part du postulat qu'il n'y a pas eu d'autre mouvement "identique" entre temps et on récupère le mouvement le plus récent
     * qui possède les caractéristiques souahitées...
     */
    private function getAutoGeneratedStockMovement(CreateMouvement $createMouvement): Mouvement
    {
        $filters = [
            'item.id' => [$createMouvement->getArticle()->getId()],
            'movementQuantity' => [$createMouvement->getQuantite()],
            'warehouse.id' => [$createMouvement->getMagasinId()],
        ];

        $ilYAUnEmplacement = !is_null($createMouvement->getEmplacement());
        if ($ilYAUnEmplacement) {
            $filters['location.id'] = [$createMouvement->getEmplacement()->getId()];
        }
        $stockmovementJsonApiResource = $this->carlConfigurationLogistique->getCarlClient()->getCarlObjects(
            entity: 'stockmovement',
            filters: $filters,
            includes: [
                'warehouse',
                'movementType',
                'createdBy',
            ],
            fields: [
                'warehouse' => ['id'],
                'actor' => ['id'],
            ],
            sort: [
                '-movementDate',
            ],
            pageLimit: 1// Note du 29/11/2024 : cette limite est très importante sinon carl ne renvoie pas bien la relation movementType, voir : https://gitlab.com/incubateur-territoires/france-relance/silab/silab-front/-/issues/1211#note_2233551546
        );

        if (0 === count($stockmovementJsonApiResource)) {
            throw new RuntimeException('Le mouvement de stock auto-généré n\'a pas été trouvé');
        }

        $lastStockmovementJsonApiResource = $stockmovementJsonApiResource[0];

        return new Mouvement(
            id: $lastStockmovementJsonApiResource->getId(),
            quantite: $lastStockmovementJsonApiResource->getAttribute('movementQuantity'),
            article: new Article(id: $createMouvement->getArticle()->getId()),
            magasinId: $lastStockmovementJsonApiResource->getRelationship('warehouse')->getId(),
            typeDeMouvement: new TypeDeMouvement(
                id: $lastStockmovementJsonApiResource->getRelationship('movementType')->getId(),
                libele: $lastStockmovementJsonApiResource->getRelationship('movementType')->getAttribute('description') ?? ''
            ),
            createdBy: $this->carlConfigurationLogistique->getCarlClient()->getUserEmailFromIdSafe($lastStockmovementJsonApiResource->getRelationship('createdBy')->getId()),
            createdAt: new \DateTime($lastStockmovementJsonApiResource->getAttribute('movementDate')),
            emplacement: $createMouvement->getEmplacement()
        );
    }

    private function getItemLocationId(string $itemId, string $locationId): string
    {
        return $this->carlConfigurationLogistique->getCarlClient()->getOneCarlObject(
            entity: 'itemlocation',
            filters: [
                'item.id' => [$itemId],
                'location.id' => [$locationId],
            ],
            fields: [
                'itemlocation' => ['id'],
            ]
        )->getId();
    }

    private function findBatchFromMouvement(CreateMouvement $createMouvement): JsonApiResource
    {
        $batchJsonApiResources = $this->carlConfigurationLogistique->getCarlClient()->getCarlObjects(
            entity: 'batch',
            filters: [
                'item.id' => [$createMouvement->getArticle()->getId()],
                'warehouse.id' => [$createMouvement->getMagasinId()],
            ],
            includes: [
                'item',
                'warehouse',
                'location',
                'location.location',
            ],
            fields: [
                'batch' => [
                    'id',
                    'unitPriceHT',
                    'item',
                    'warehouse',
                    'location',
                    'quantity',
                ],
                'item' => [
                    'id',
                ],
                'warehouse' => [
                    'id',
                ],
                'itemlocation' => [
                    'location',
                ],
                'location' => [
                    'id',
                ],
            ]);

        $matchingBatches = [];
        foreach ($batchJsonApiResources as $batchJsonApiResource) {
            if (
                $createMouvement->getEmplacement()?->getId()
                ===
                $batchJsonApiResource->getRelationship('location')?->getRelationship('location')->getId()
            ) {
                $matchingBatches[] = $batchJsonApiResource;
            }
        }

        if (count($matchingBatches) > 1) {
            throw new RuntimeException('Plusieurs lots trouvés l\'article '.$createMouvement->getArticle()->getCode().' dans le magasinId '.$createMouvement->getMagasinId().' et l\'empalcement '.$createMouvement->getEmplacement()?->getCode().'. (! problème d\'intégrité de la BDD carl !)');
        }

        if (empty($matchingBatches)) {
            throw new NoBatchFoundForMouvementException("Aucun lot de stockage trouvé à l'emplacement ".$createMouvement->getEmplacement()?->getCode()." pour le mouvement de stock concernant l'article ".$createMouvement->getArticle()->getCode().'.');
        }

        return $matchingBatches[0];
    }

    private function postInventory(CreateMouvement $createMouvement, float $articlePrice): Mouvement
    {
        $warehouseId = $createMouvement->getMagasinId();
        assert(!empty($warehouseId));

        $userEmail = $createMouvement->getCreatedBy();
        assert(!empty($userEmail));

        $inventoryArray = [
            'data' => [
                'attributes' => [
                    'movementQuantity' => $createMouvement->getQuantite(),
                    'stockCost' => $createMouvement->getQuantite() * $articlePrice,
                ],
                'relationships' => [
                    'item' => [
                        'data' => [
                            'id' => $createMouvement->getArticle()->getId(),
                            'type' => 'item',
                        ],
                    ],
                    'movementType' => [
                        'data' => [
                            'id' => $createMouvement->getTypeDeMouvement()->getId(),
                            'type' => 'movementtype',
                        ],
                    ],
                    'createdBy' => [
                        'data' => [
                            'id' => $this->carlConfigurationLogistique->getCarlClient()->getUserIdFromEmail($userEmail),
                            'type' => 'actor',
                        ],
                    ],
                    'actor' => [
                        'data' => [
                            'id' => $this->carlConfigurationLogistique->getCarlClient()->getUserIdFromEmail($userEmail),
                            'type' => 'actor',
                        ],
                    ],
                    'warehouse' => [
                        'data' => [
                            'id' => $warehouseId,
                            'type' => 'warehouse',
                        ],
                    ],
                ],
            ],
        ];
        if ($createMouvement->getEmplacement()) {
            $inventoryArray['data']['relationships']['location'] =
                [
                    'data' => [
                        'id' => $createMouvement->getEmplacement()->getId(),
                        'type' => 'location',
                    ],
                ];
        }
        $inventoryJsonApiResource = $this->carlConfigurationLogistique->getCarlClient()->postCarlObject('inventory', json_encode($inventoryArray));

        return new Mouvement(
            id: $inventoryJsonApiResource->getId(),
            quantite: $inventoryJsonApiResource->getAttribute('movementQuantity'),
            article: new Article(id: $createMouvement->getArticle()->getId()),
            emplacement: $createMouvement->getEmplacement() ? new Emplacement(id: $createMouvement->getEmplacement()->getId()) : null,
            typeDeMouvement: new TypeDeMouvement(
                id: $createMouvement->getTypeDeMouvement()->getId()
            ),
            magasinId: $warehouseId,
            createdBy: $createMouvement->getCreatedBy(),
            createdAt: new \DateTime($inventoryJsonApiResource->getAttribute('movementDate'))
        );
    }
}
