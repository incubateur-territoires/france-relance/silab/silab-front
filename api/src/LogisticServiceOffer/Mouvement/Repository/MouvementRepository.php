<?php

namespace App\LogisticServiceOffer\Mouvement\Repository;

use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\LogisticServiceOffer\Gmao\Entity\GmaoConfigurationLogistique;
use App\LogisticServiceOffer\Mouvement\Entity\CreateMouvement;
use App\LogisticServiceOffer\Mouvement\Entity\Mouvement;
use App\LogisticServiceOffer\Mouvement\Entity\TypeDeMouvement;
use App\Shared\Exception\RuntimeException;

class MouvementRepository implements MouvementRepositoryInterface
{
    private MouvementRepositoryInterface $specificMouvementRepository;

    public function __construct(
        protected GmaoConfigurationLogistique $gmaoConfigurationLogistique
    ) {
        try {
            $this->specificMouvementRepository = match ($gmaoConfigurationLogistique::class) {
                CarlConfigurationLogistique::class => new CarlMouvementRepository(
                    $gmaoConfigurationLogistique
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfigurationLogistique::class);
        }
    }

    public function find($id): Mouvement
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->specificMouvementRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): Mouvement
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function getClassName()
    {
        return Mouvement::class;
    }

    public function save(CreateMouvement $createMouvement): Mouvement
    {
        return $this->specificMouvementRepository->save($createMouvement);
    }

    /**
     * @return array<TypeDeMouvement>
     */
    public function getTypeDeMouvementsInventaire(): array
    {
        return $this->specificMouvementRepository->getTypeDeMouvementsInventaire();
    }
}
