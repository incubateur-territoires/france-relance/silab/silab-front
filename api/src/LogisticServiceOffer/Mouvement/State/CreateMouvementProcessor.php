<?php

namespace App\LogisticServiceOffer\Mouvement\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;
use App\LogisticServiceOffer\Mouvement\Entity\CreateMouvement;
use App\LogisticServiceOffer\Mouvement\Entity\MouvementApiResource;
use App\LogisticServiceOffer\Mouvement\Repository\MouvementRepository;
use App\Shared\User\Service\CurrentUserContextInterface;
use Psr\Log\LoggerInterface;

final class CreateMouvementProcessor implements ProcessorInterface
{
    public function __construct(
        private LogisticServiceOfferRepository $logisticServiceOfferRepository,
        private CurrentUserContextInterface $currentUserContext,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): MouvementApiResource
    {
        $this->logger->debug('Création d\'un mouvement de stock', ['data' => json_encode($data), 'context' => $context, 'uriVariables' => $uriVariables]);
        assert($operation instanceof Post);
        assert($data instanceof MouvementApiResource);

        $logistiqueServiceOffer = $this->logisticServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $mouvementRepository = new MouvementRepository($logistiqueServiceOffer->getGmaoConfiguration());

        $data->setCreatedBy($this->currentUserContext->getCurrentUser()->getEmail());

        $createMouvement = CreateMouvement::fromApiResourceInMagasin($data, $logistiqueServiceOffer->getWarehouseId());

        return $mouvementRepository->save($createMouvement)->toApiRessource();
    }
}
