<?php

namespace App\LogisticServiceOffer\Mouvement\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;
use App\LogisticServiceOffer\Mouvement\Entity\MouvementApiResource;
use App\LogisticServiceOffer\Mouvement\Repository\MouvementRepository;

final class GetCollectionMouvementProvider implements ProviderInterface
{
    public function __construct(private LogisticServiceOfferRepository $logisticServiceOfferRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<MouvementApiResource>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $logisticServiceOffer = $this->logisticServiceOfferRepository->find($uriVariables['serviceOfferId']);

        $criteria = $context['filters'] ?? [];

        foreach ($criteria as $key => $value) {
            if (is_string($value)) {
                $criteria[$key] = [$value];
            }
        }

        $criteria['warehouse.id'] = [$logisticServiceOffer->getWarehouseId()];

        $mouvementRepository = new MouvementRepository($logisticServiceOffer->getGmaoConfiguration());

        return array_map(
            function ($mouvement) {
                return $mouvement->toApiRessource();
            },
            $mouvementRepository->findBy($criteria)
        );
    }
}
