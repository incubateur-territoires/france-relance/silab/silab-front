<?php

namespace App\LogisticServiceOffer\MouvementDeSortie\Entity;

use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Emplacement\Entity\Emplacement;
use App\LogisticServiceOffer\Mouvement\Entity\MouvementInterface;

class MouvementDeSortie implements MouvementInterface
{
    public function __construct(
        private string $id,
        private Article $article,
        private string $magasinId,
        private \DateTime $createdAt,
        private string $interventionId,
        private float $quantite,
        private ?string $createdBy,
        private ?Emplacement $emplacement,
    ) {
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setArticle(Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getArticle(): Article
    {
        return $this->article;
    }

    public function setIntervention(string $intervention): self
    {
        $this->interventionId = $intervention;

        return $this;
    }

    public function getInterventionId(): string
    {
        return $this->interventionId;
    }

    public function setMagasinId(string $magasinId): self
    {
        $this->magasinId = $magasinId;

        return $this;
    }

    public function getMagasinId(): string
    {
        return $this->magasinId;
    }

    public function setQuantite(float $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getQuantite(): float
    {
        return $this->quantite;
    }

    public function setEmplacement(?Emplacement $emplacement): self
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    public function getEmplacement(): ?Emplacement
    {
        return $this->emplacement;
    }

    public function setCreatedBy(string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function toApiRessource(): MouvementDeSortieApiResource
    {
        return new MouvementDeSortieApiResource(
            id: $this->getId(),
            interventionId: $this->getInterventionId(),
            quantite: $this->getQuantite(),
            article: $this->getArticle(),
            emplacement: $this->getEmplacement(),
            createdBy: $this->getCreatedBy(),
            createdAt: $this->getCreatedAt()
        );
    }
}
