<?php

namespace App\LogisticServiceOffer\MouvementDeSortie\Entity;

use ApiPlatform\Metadata\GetCollection;
use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Emplacement\Entity\Emplacement;
use App\LogisticServiceOffer\MouvementDeSortie\State\GetCollectionMouvementDeSortieProvider;
use Symfony\Component\Serializer\Annotation\Groups;

#[GetCollection(
    uriTemplate: '/logistic-service-offers/{serviceOfferId}/interventions/{interventionId}/sorties-de-stock',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'interventionId' => 'interventionId',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_LOGISTIQUE_CONSULTER_SORTIES_DE_STOCK')",
    provider: GetCollectionMouvementDeSortieProvider::class,
    normalizationContext: ['groups' => [self::GROUP_AFFICHER_LISTE_MOUVEMENTS_DE_SORTIE]],
)]
class MouvementDeSortieApiResource
{
    public const GROUP_AFFICHER_LISTE_MOUVEMENTS_DE_SORTIE = 'Afficher informations d\'une liste de mouvement de sortie d\'une intervention';

    public function __construct(
        #[Groups([self::GROUP_AFFICHER_LISTE_MOUVEMENTS_DE_SORTIE])]
        private string $id,
        #[Groups([self::GROUP_AFFICHER_LISTE_MOUVEMENTS_DE_SORTIE])]
        private Article $article,
        #[Groups([self::GROUP_AFFICHER_LISTE_MOUVEMENTS_DE_SORTIE])]
        private string $interventionId,
        #[Groups([self::GROUP_AFFICHER_LISTE_MOUVEMENTS_DE_SORTIE])]
        private \DateTime $createdAt,
        #[Groups([self::GROUP_AFFICHER_LISTE_MOUVEMENTS_DE_SORTIE])]
        private float $quantite,
        #[Groups([self::GROUP_AFFICHER_LISTE_MOUVEMENTS_DE_SORTIE])]
        private ?string $createdBy = null,
        #[Groups([self::GROUP_AFFICHER_LISTE_MOUVEMENTS_DE_SORTIE])]
        private ?Emplacement $emplacement = null,
    ) {
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setArticle(Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getArticle(): Article
    {
        return $this->article;
    }

    public function setIntervention(string $intervention): self
    {
        $this->interventionId = $intervention;

        return $this;
    }

    public function getIntervention(): string
    {
        return $this->interventionId;
    }

    public function setQuantite(float $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getQuantite(): float
    {
        return $this->quantite;
    }

    public function setEmplacement(?Emplacement $emplacement): self
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    public function getEmplacement(): ?Emplacement
    {
        return $this->emplacement;
    }

    public function setCreatedBy(string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }
}
