<?php

namespace App\LogisticServiceOffer\MouvementDeSortie\Repository;

use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Emplacement\Entity\Emplacement;
use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\LogisticServiceOffer\MouvementDeSortie\Entity\MouvementDeSortie;
use App\Shared\Carl\Trait\UnexpectedCriteriaCheckingTrait;
use App\Shared\Exception\RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlMouvementDeSortieRepository implements MouvementDeSortieRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;

    public function __construct(
        private CarlConfigurationLogistique $carlConfigurationLogistique
    ) {
    }

    public function findOneBy(array $criteria): MouvementDeSortie
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function find($id): MouvementDeSortie
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }
        if (null !== $orderBy) {
            throw new RuntimeException('Le paramètre orderBy n\'est pas encore implémenté');
        }
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'intervention.id',
                'warehouse.id',
            ]
        );

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $filters = [];

        $interventionId = $propertyAccessor->getValue($criteria, '[intervention.id]');
        if (!empty($interventionId)) {
            $filters['WO.id'] = $interventionId;
        }

        $warehouseId = $propertyAccessor->getValue($criteria, '[warehouse.id]');
        if (!empty($warehouseId)) {
            $filters['warehouse.id'] = $warehouseId;
        }

        $includes = [
            'item',
            'WO',
            'createdBy',
            'warehouse',
            'location',
        ];

        $fields = [
            'issue' => [
                'movementQuantity',
                'movementDate',
                'createdBy',
                'item',
                'location',
                'warehouse',
            ],
            'item' => [
                'code',
                'description',
                'unit',
            ],
            'location' => [
                'code',
            ],
            'actor' => [
                'fullName',
            ],
        ];
        $movementsJsonApiResource =
            $this->carlConfigurationLogistique->getCarlClient()->getCarlObjects(
                entity: 'issue',
                filters: $filters,
                includes: $includes,
                fields: $fields,
                sort: ['-movementDate'],
            );
        $mouvementsDeSortie = [];
        foreach ($movementsJsonApiResource as $movementApiResource) {
            $mouvementsDeSortie[] = new MouvementDeSortie(
                id: $movementApiResource->getId(),
                quantite: abs($movementApiResource->getAttribute('movementQuantity')),
                article: new Article(
                    id: $movementApiResource->getRelationship('item')->getId(),
                    code: $movementApiResource->getRelationship('item')->getAttribute('code'),
                    titre: $movementApiResource->getRelationship('item')->getAttribute('description') ?? '',
                    unité: $this->carlConfigurationLogistique->getCarlClient()->getUnitDescriptionFromCode(
                        $movementApiResource->getRelationship('item')->getAttribute('unit')
                    )
                ),
                interventionId: $movementApiResource->getRelationship('WO')->getId(),
                magasinId: $movementApiResource->getRelationship('warehouse')->getId(),
                emplacement: $movementApiResource->getRelationship('location')
                    ? new Emplacement(
                        id: $movementApiResource->getRelationship('location')->getId(),
                        code: $movementApiResource->getRelationship('location')->getAttribute('code'),
                    )
                    : null,
                createdBy: $movementApiResource->getRelationship('createdBy')->getAttribute('fullName'),
                createdAt: new \DateTime($movementApiResource->getAttribute('movementDate')),
            );
        }

        return $mouvementsDeSortie;
    }

    public function getClassName()
    {
        return MouvementDeSortie::class;
    }
}
