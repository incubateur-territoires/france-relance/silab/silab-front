<?php

namespace App\LogisticServiceOffer\MouvementDeSortie\Repository;

use App\LogisticServiceOffer\Gmao\Entity\CarlConfigurationLogistique;
use App\LogisticServiceOffer\Gmao\Entity\GmaoConfigurationLogistique;
use App\LogisticServiceOffer\MouvementDeSortie\Entity\MouvementDeSortie;
use App\Shared\Exception\RuntimeException;

class MouvementDeSortieRepository implements MouvementDeSortieRepositoryInterface
{
    private MouvementDeSortieRepositoryInterface $specificMouvementDeSortieRepository;

    public function __construct(
        protected GmaoConfigurationLogistique $gmaoConfigurationLogistique
    ) {
        try {
            $this->specificMouvementDeSortieRepository = match ($gmaoConfigurationLogistique::class) {
                CarlConfigurationLogistique::class => new CarlMouvementDeSortieRepository(
                    $gmaoConfigurationLogistique
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfigurationLogistique::class);
        }
    }

    public function find($id): MouvementDeSortie
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->specificMouvementDeSortieRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): MouvementDeSortie
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function getClassName()
    {
        return MouvementDeSortie::class;
    }
}
