<?php

namespace App\LogisticServiceOffer\MouvementDeSortie\Repository;

use App\LogisticServiceOffer\MouvementDeSortie\Entity\MouvementDeSortie;
use Doctrine\Persistence\ObjectRepository;

interface MouvementDeSortieRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): MouvementDeSortie;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<MouvementDeSortie>
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array;

    /**
     * @return array<MouvementDeSortie>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): MouvementDeSortie;
}
