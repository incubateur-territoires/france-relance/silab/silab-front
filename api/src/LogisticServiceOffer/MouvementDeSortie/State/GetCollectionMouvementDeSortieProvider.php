<?php

namespace App\LogisticServiceOffer\MouvementDeSortie\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;
use App\LogisticServiceOffer\MouvementDeSortie\Entity\MouvementDeSortieApiResource;
use App\LogisticServiceOffer\MouvementDeSortie\Repository\MouvementDeSortieRepository;

final class GetCollectionMouvementDeSortieProvider implements ProviderInterface
{
    public function __construct(private LogisticServiceOfferRepository $logisticServiceOfferRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<MouvementDeSortieApiResource>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $logisticServiceOffer = $this->logisticServiceOfferRepository->find($uriVariables['serviceOfferId']);

        $criteria['warehouse.id'] = [$logisticServiceOffer->getWarehouseId()];
        $criteria['intervention.id'] = [$uriVariables['interventionId']];

        $mouvementDeSortieRepository = new MouvementDeSortieRepository($logisticServiceOffer->getGmaoConfiguration());

        return array_map(
            function ($mouvementDeSortie) {
                return $mouvementDeSortie->toApiRessource();
            },
            $mouvementDeSortieRepository->findBy($criteria)
        );
    }
}
