<?php

namespace App\ServiceOffer\EventListener;

use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use App\ServiceOffer\ServiceOffer\Repository\ServiceOfferRepository;
use App\Shared\User\Repository\UserRepository;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::preRemove, method: 'removeUserRoles', entity: ServiceOffer::class)]
class RoleCleaner
{
    protected UserRepository $userRepository;
    protected ServiceOfferRepository $serviceOfferRepository;

    public function __construct(UserRepository $userRepository, ServiceOfferRepository $serviceOfferRepository)
    {
        $this->userRepository = $userRepository;
        $this->serviceOfferRepository = $serviceOfferRepository;
    }

    public function removeUserRoles(ServiceOffer $serviceOffer, PreRemoveEventArgs $args): void
    {
        $users = $this->userRepository->findAll();
        foreach ($users as $user) {
            $user->removeServiceOfferRoles($serviceOffer);
            $args->getObjectManager()->persist($user);
        }
    }
}
