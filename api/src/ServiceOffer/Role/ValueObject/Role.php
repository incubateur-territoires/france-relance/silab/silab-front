<?php

namespace App\ServiceOffer\Role\ValueObject;

use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use Symfony\Component\Serializer\Annotation\Groups;

class Role
{
    public function __construct(
        private string $value,
        private string $libelle,
        private string $description
    ) {
    }

    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS])]
    public function getValue(): string
    {
        return $this->value;
    }

    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS])]
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS])]
    public function getDescription(): string
    {
        return $this->description;
    }
}
