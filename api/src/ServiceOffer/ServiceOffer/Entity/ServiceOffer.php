<?php

namespace App\ServiceOffer\ServiceOffer\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\ElusServiceOffer\ElusServiceOffer\Entity\ElusServiceOffer;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use App\ServiceOffer\Role\ValueObject\Role;
use App\ServiceOffer\ServiceOffer\Repository\ServiceOfferRepository;
use App\ServiceOffer\ServiceOffer\State\GetCollectionServiceOfferProvider;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[GetCollection(
    provider: GetCollectionServiceOfferProvider::class,
    security: "is_granted('IS_AUTHENTICATED_FULLY')",
    normalizationContext: ['groups' => [ServiceOffer::GROUP_LISTER_ODS]],
    order: ['title' => 'ASC']
)]
#[Get(
    security: "is_granted('SERVICEOFFER_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE')",
    normalizationContext: ['groups' => [ServiceOffer::GROUP_AFFICHER_DETAILS_ODS]]
)]
#[ORM\Entity(repositoryClass: ServiceOfferRepository::class)]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'template', type: 'string')]
#[ORM\DiscriminatorMap([
    'intervention' => InterventionServiceOffer::class,
    'logistic' => LogisticServiceOffer::class,
    'elus' => ElusServiceOffer::class,
])]
abstract class ServiceOffer
{
    public const GROUP_AFFICHER_DETAILS_ODS = 'Afficher les détails d\'une offre de service';
    public const GROUP_AFFICHER_LISTE_ODS = 'Afficher la liste des offres de service';
    public const GROUP_LISTER_ODS = 'Lister les offres de service';
    public const GROUP_CREER_ODS = 'Créer une offre de service';

    public const ROLE_REGEX = '/^SERVICEOFFER_(?P<serviceOfferId>[0-9]+)_ROLE_(?P<serviceOfferType>[^_]+)_.*/';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS])]
    protected ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS, ServiceOffer::GROUP_CREER_ODS])]
    protected ?string $link = null;

    #[ORM\Column(length: 255)]
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS, ServiceOffer::GROUP_CREER_ODS])]
    protected ?string $title = null;

    #[ORM\Column(length: 500, nullable: true)]
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS, ServiceOffer::GROUP_CREER_ODS])]
    protected ?string $description = null;

    #[ORM\Column(length: 255)]
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS, ServiceOffer::GROUP_CREER_ODS])]
    protected ?string $image = null;

    #[ORM\Column(length: 255, options: ['default' => 'PROD'])]
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS, ServiceOffer::GROUP_CREER_ODS])]
    protected ?string $environnement = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS, ServiceOffer::GROUP_CREER_ODS])]
    protected ?string $notes = null;

    /**
     * @return array<Role>
     */
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS])]
    public function getAvailableRoles(): array
    {
        return [
            new Role(value: 'SERVICEOFFER_'.$this->getId().'_ROLE_SERVICEOFFER_ADMIN', libelle: 'Administrateur', description: 'Peut administrer les agents ayant accès à cette offre de service ainsi que leurs rôles associés'),
        ];
    }

    public static function getRoleRegexpByServiceOfferId(int $serviceOfferId): string
    {
        return '/^SERVICEOFFER_'.$serviceOfferId.'_ROLE_(?P<serviceOfferType>[^_]+)_.*/';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): static
    {
        $this->link = $link;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): static
    {
        $this->image = $image;

        return $this;
    }

    public function getEnvironnement(): ?string
    {
        return $this->environnement;
    }

    public function setEnvironnement(string $environnement): static
    {
        $this->environnement = $environnement;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(string $notes): static
    {
        $this->notes = $notes;

        return $this;
    }

    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_AFFICHER_LISTE_ODS])]
    abstract public function getTemplate(): string;
}
