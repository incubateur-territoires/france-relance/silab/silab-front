<?php

namespace App\ServiceOffer\ServiceOffer\Exception;

use App\Shared\Exception\RuntimeException;

class NoServiceOfferContextException extends RuntimeException
{
    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct(message: "Aucun contexte d'offre de service", previous: $previous);
    }
}
