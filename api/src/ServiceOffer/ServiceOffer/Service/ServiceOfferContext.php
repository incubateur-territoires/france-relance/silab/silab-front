<?php

namespace App\ServiceOffer\ServiceOffer\Service;

use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use App\ServiceOffer\ServiceOffer\Exception\NoServiceOfferContextException;
use App\ServiceOffer\ServiceOffer\Repository\ServiceOfferRepository;
use Symfony\Component\HttpFoundation\RequestStack;

class ServiceOfferContext implements ServiceOfferContextInterface
{
    private ?ServiceOffer $serviceOffer = null;
    private bool $hasCachedServiceOffer = false;

    public function __construct(
        private ServiceOfferRepository $serviceOfferRepository,
        private RequestStack $requestStack,
    ) {
    }

    public function getServiceOfferId(): int
    {
        if ($this->hasCachedServiceOffer && !is_null($this->serviceOffer)) {
            return $this->serviceOffer->getId();
        }

        $serviceOfferId = $this->requestStack->getCurrentRequest()?->get('serviceOfferId');

        if (
            is_null($serviceOfferId)
            && in_array(
                $this->requestStack->getCurrentRequest()?->get('_api_resource_class'),
                [
                    ServiceOffer::class,
                    InterventionServiceOffer::class,
                    LogisticServiceOffer::class,
                ]
            )
        ) {
            $serviceOfferId = $this->requestStack->getCurrentRequest()?->get('id');
        }

        if (is_null($serviceOfferId)) {
            throw new NoServiceOfferContextException();
        }

        return $serviceOfferId;
    }

    public function getServiceOffer(): ServiceOffer
    {
        if ($this->hasCachedServiceOffer) {
            return $this->serviceOffer;
        }

        $this->serviceOffer = $this->serviceOfferRepository->find(
            $this->getServiceOfferId()
        );

        if (is_null($this->serviceOffer)) {
            throw new NoServiceOfferContextException();
        }

        $this->hasCachedServiceOffer = true;

        return $this->serviceOffer;
    }
}
