<?php

namespace App\ServiceOffer\ServiceOffer\Service;

use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;

interface ServiceOfferContextInterface
{
    public function getServiceOfferId(): ?int;

    public function getServiceOffer(): ?ServiceOffer;
}
