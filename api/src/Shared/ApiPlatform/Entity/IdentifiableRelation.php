<?php

namespace App\Shared\ApiPlatform\Entity;

class IdentifiableRelation
{
    public function __construct(
        private string $id,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }
}
