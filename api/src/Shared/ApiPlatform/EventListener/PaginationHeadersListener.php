<?php

namespace App\Shared\ApiPlatform\EventListener;

use ApiPlatform\State\Pagination\PaginatorInterface;
use ApiPlatform\State\Pagination\PartialPaginatorInterface;
use ApiPlatform\Util\IriHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\WebLink\GenericLinkProvider;
use Symfony\Component\WebLink\Link;

/**
 * inspiré fortement de https://github.com/api-platform/core/issues/1548#issuecomment-977851638
 * mais avec des noms de headers plutot inspirés de https://developer.anvyl.com/docs/pagination qui ont l'ai plus standards.
 */
class PaginationHeadersListener implements EventSubscriberInterface
{
    private string $paginationParameterName = 'page';

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => ['onKernelResponse'],
        ];
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        if (HttpKernelInterface::MAIN_REQUEST !== $event->getRequestType()) {
            return;
        }

        $request = $event->getRequest();
        $response = $event->getResponse();
        $data = $request->attributes->get('data');

        if (!$data instanceof PartialPaginatorInterface) {
            return;
        }

        $currentPage = (int) $data->getCurrentPage();

        $response->headers->set('X-Page', (string) $currentPage);
        $response->headers->set('X-Per-Page', (string) (int) $data->getItemsPerPage());

        if (!$data instanceof PaginatorInterface) {
            return;
        }

        $lastPage = (int) $data->getLastPage();

        $response->headers->set('X-Total-Pages', (string) $lastPage);
        $response->headers->set('X-Total-Count', (string) (int) $data->getTotalItems());

        $linkProvider = $request->attributes->get('_links', new GenericLinkProvider());

        foreach ($this->collectLinks($request, $currentPage, $lastPage) as $rel => $url) {
            $linkProvider = $linkProvider->withLink(new Link($rel, $url));
        }

        $request->attributes->set('_links', $linkProvider);
    }

    /**
     * @return array<string>
     */
    private function collectLinks(Request $request, int $currentPage, int $lastPage): array
    {
        $links = [];
        $parsed = IriHelper::parseIri($request->getUri(), $this->paginationParameterName);

        $links['first'] = IriHelper::createIri($parsed['parts'], $parsed['parameters'], $this->paginationParameterName, 1);
        $links['last'] = IriHelper::createIri($parsed['parts'], $parsed['parameters'], $this->paginationParameterName, $lastPage);

        if (1 !== $currentPage) {
            $links['prev'] = IriHelper::createIri($parsed['parts'], $parsed['parameters'], $this->paginationParameterName, $currentPage - 1);
        }

        if ($currentPage !== $lastPage) {
            $links['next'] = IriHelper::createIri($parsed['parts'], $parsed['parameters'], $this->paginationParameterName, $currentPage + 1);
        }

        return $links;
    }
}
