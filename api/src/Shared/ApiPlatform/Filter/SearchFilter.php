<?php

// depuis https://api-platform.com/docs/core/filters/#creating-custom-filters
// et https://stackoverflow.com/a/52496870/2474460

namespace App\Shared\ApiPlatform\Filter;

use ApiPlatform\Metadata\FilterInterface;

/**
 * Class SearchFilter.
 */
class SearchFilter implements FilterInterface
{
    /**
     * @var string Exact matching
     */
    public const STRATEGY_EXACT = 'exact';

    /**
     * @var array<string,string>
     */
    protected array $properties;

    /**
     * @param array<string>|null $properties
     *
     * @return void
     */
    public function __construct(?array $properties = null)
    {
        $this->properties = $properties;
    }

    /**
     * @return array<string,array<mixed>>
     */
    public function getDescription(string $resourceClass): array
    {
        /** @var array<string,array<mixed>> */
        $description = [];

        /** @var array<string,string> */
        $properties = $this->properties;

        foreach ($properties as $property => $strategy) {
            $description[$property] = [
                'property' => $property,
                'type' => 'array',
                'required' => false,
                'strategy' => $strategy,
                'is_collection' => false,
            ];
        }

        return $description;
    }
}
