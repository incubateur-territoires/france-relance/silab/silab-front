<?php

declare(strict_types=1);

namespace App\Shared\ApiPlatform\Pagination;

use ApiPlatform\State\Pagination\PaginatorInterface;

/**
 * version de ApiPlatform\State\Pagination\TraversablePaginator mais avec du typage en plus.
 *
 * @template T of object
 *
 * @implements PaginatorInterface<T>
 * @implements \IteratorAggregate<T>
 */
final class TraversablePaginator implements \IteratorAggregate, PaginatorInterface
{
    /**
     * @param \Traversable<T> $traversable
     */
    public function __construct(private readonly \Traversable $traversable, private readonly float $currentPage, private readonly float $itemsPerPage, private readonly float $totalItems)
    {
    }

    public function getCurrentPage(): float
    {
        return $this->currentPage;
    }

    public function getLastPage(): float
    {
        if (0. >= $this->itemsPerPage) {
            return 1.;
        }

        return max(ceil($this->totalItems / $this->itemsPerPage) ?: 1., 1.);
    }

    public function getItemsPerPage(): float
    {
        return $this->itemsPerPage;
    }

    public function getTotalItems(): float
    {
        return $this->totalItems;
    }

    public function count(): int
    {
        if ($this->getCurrentPage() < $this->getLastPage()) {
            return (int) ceil($this->itemsPerPage);
        }

        if (0. >= $this->itemsPerPage) {
            return (int) ceil($this->totalItems);
        }

        return $this->totalItems % $this->itemsPerPage;
    }

    /**
     * @return \Traversable<T>
     */
    public function getIterator(): \Traversable
    {
        return $this->traversable;
    }
}
