<?php

namespace App\Shared\Carl\Command;

use App\InterventionServiceOffer\Carl\Entity\DbCarlActiviteAction;
use App\InterventionServiceOffer\Carl\Entity\DbCarlActiviteDiagnostique;
use App\InterventionServiceOffer\Carl\Entity\DbCarlCompteur;
use App\InterventionServiceOffer\Carl\Entity\DbCarlCompteurDbCarlEquipement;
use App\InterventionServiceOffer\Carl\Entity\DbCarlCustomer;
use App\InterventionServiceOffer\Carl\Entity\DbCarlDemandeIntervention;
use App\InterventionServiceOffer\Carl\Entity\DbCarlDemandeInterventionCoordinates;
use App\InterventionServiceOffer\Carl\Entity\DbCarlDemandeInterventionDbCarlEquipement;
use App\InterventionServiceOffer\Carl\Entity\DbCarlDocument;
use App\InterventionServiceOffer\Carl\Entity\DbCarlEquipement;
use App\InterventionServiceOffer\Carl\Entity\DbCarlEquipementCoordinate;
use App\InterventionServiceOffer\Carl\Entity\DbCarlEquipementDbCarlEquipement;
use App\InterventionServiceOffer\Carl\Entity\DbCarlEquipementType;
use App\InterventionServiceOffer\Carl\Entity\DbCarlGmaoActeur;
use App\InterventionServiceOffer\Carl\Entity\DbCarlIntervention as InterventionDbCarlIntervention;
use App\InterventionServiceOffer\Carl\Entity\DbCarlInterventionCoordinates;
use App\InterventionServiceOffer\Carl\Entity\DbCarlInterventionDbCarlEquipement;
use App\InterventionServiceOffer\Carl\Entity\DbCarlInterventionStatus as InterventionDbCarlInterventionStatus;
use App\InterventionServiceOffer\Carl\Entity\DbCarlMesure;
use App\InterventionServiceOffer\Carl\Entity\DbCarlOperation;
use App\InterventionServiceOffer\Carl\Entity\DbCarlOperationDbCarlGmaoActeur;
use App\InterventionServiceOffer\Carl\Entity\DbCarlReleveDeCompteur;
use App\InterventionServiceOffer\Carl\Entity\DbCarlRessource;
use App\InterventionServiceOffer\Carl\Entity\DbCarlSearchEntity;
use App\LogisticServiceOffer\CarlV6\Entity\DbCarlArticleReservation;
use App\LogisticServiceOffer\CarlV6\Entity\DbCarlIntervention as LogistiqueDbCarlIntervention;
use App\LogisticServiceOffer\CarlV6\Entity\DbCarlInterventionStatus as LogistiqueDbCarlInterventionStatus;
use App\LogisticServiceOffer\CarlV6\Entity\DbCarlInterventionWarehouseReservation;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Repository\CarlClientRepository;
use App\Shared\Helper\Exception\EmptyStringException;
use App\Shared\Helper\Exception\ForbiddenDoubleQuoteCharacterException;
use App\Shared\Helpers;
use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

#[AsCommand(name: 'app:carlclient:buildforeigntables')]
class BuildForeignTablesCommand extends Command
{
    public function __construct(
        private CarlClientRepository $carlClientRepository,
        private Connection $connection,
        private ContainerBagInterface $params,
    ) {
        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * @var array<class-string<DbCarlEntitySqlBuilder>>
     */
    public const DB_CARL_ENTITIES_CLASSES = [// évolution: détecter les classes avec des attributs ?
        // interventionServiceOffer
        InterventionDbCarlIntervention::class,
        InterventionDbCarlInterventionStatus::class,
        DbCarlInterventionCoordinates::class,
        DbCarlInterventionDbCarlEquipement::class,
        DbCarlDemandeIntervention::class,
        DbCarlDemandeInterventionCoordinates::class,
        DbCarlDemandeInterventionDbCarlEquipement::class,
        DbCarlCompteur::class,
        DbCarlCompteurDbCarlEquipement::class,
        DbCarlActiviteAction::class,
        DbCarlActiviteDiagnostique::class,
        DbCarlOperation::class,
        DbCarlOperationDbCarlGmaoActeur::class,
        DbCarlEquipement::class,
        DbCarlEquipementType::class,
        DbCarlEquipementCoordinate::class,
        DbCarlEquipementDbCarlEquipement::class,
        DbCarlGmaoActeur::class,
        DbCarlDocument::class,
        DbCarlCustomer::class,
        DbCarlSearchEntity::class,
        DbCarlRessource::class,
        DbCarlReleveDeCompteur::class,
        DbCarlMesure::class,
        // logisticServiceOffer
        DbCarlArticleReservation::class,
        LogistiqueDbCarlIntervention::class,
        LogistiqueDbCarlInterventionStatus::class,
        DbCarlInterventionWarehouseReservation::class,
    ];

    protected function configure(): void
    {
        $this
            ->setDescription('Met en place les tables étrangères faisant la liaison avec les bases de données Carl');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->buildForeignTables(
            function ($message) use ($output) {
                $output->writeln($message);
            }
        );

        return Command::SUCCESS;
    }

    public function buildForeignTables(callable $outputFunction): void
    {
        // on s'assure que la miration nécessaire à bien étée passée
        $mandatoryMigration = $this->connection->executeQuery("select * from doctrine_migration_versions where version='DoctrineMigrations\Version20240417130317'")->fetchOne();
        if (false === $mandatoryMigration) {
            return;
        }

        $carlClients = $this->carlClientRepository->findAll();

        foreach ($carlClients as $carlClient) {
            try {
                /**
                 * @var array<string> $sqlRequestQueue
                 */
                $sqlRequestQueue = [];

                $carlClientId = $carlClient->getId();
                $doctrineDbUser = $this->params->get('app.postgres_user');
                $dbServer = $carlClient->getDbServer();
                $dbUser = $carlClient->getDbUser();
                $dbPassword = $carlClient->getDbPassword();
                $dbSchema = $carlClient->getDbShema();

                // on s'assure que la configuration de ce client carl
                // ai des chance de fonctionner
                Helpers::throwIfempty([
                    $doctrineDbUser,
                    $dbServer,
                    $dbUser,
                    $dbPassword,
                    $dbSchema,
                ]);
                Helpers::throwIfDoubleQuotes([
                    $doctrineDbUser,
                    $dbServer,
                    $dbUser,
                    $dbPassword,
                    $dbSchema,
                ]);

                $serverName = 'carl_'.$carlClientId.'_db_oracle';

                // on drop le serveur et tous les objets liés
                $sqlRequestQueue[] = 'DROP SERVER IF EXISTS '.$serverName.' CASCADE';
                $sqlRequestQueue[] = 'CREATE SERVER '.$serverName.' FOREIGN DATA WRAPPER oracle_fdw OPTIONS (dbserver \''.$dbServer.'\', isolation_level \'read_only\')';
                $sqlRequestQueue[] = 'CREATE USER MAPPING FOR "'.$doctrineDbUser.'" SERVER '.$serverName.' OPTIONS (user \''.$dbUser.'\', password \''.$dbPassword.'\')';

                // foreign tables des interventionServiceOffers
                $entitiesSqlRequests = array_reduce(
                    self::DB_CARL_ENTITIES_CLASSES,
                    function ($sqlRequestQueue, $dbCarlEntity) use ($carlClient, $dbSchema, $serverName) {
                        $sqlRequestQueue[] = $dbCarlEntity::buildTableSql(
                            carlClient: $carlClient,
                            dbSchema: $dbSchema,
                            serverName: $serverName
                        );

                        return $sqlRequestQueue;
                    },
                    []
                );
                $sqlRequestQueue = array_merge(
                    $sqlRequestQueue,
                    $entitiesSqlRequests
                );

                while (!empty($sqlRequestQueue)) {
                    $statement = $this->connection->prepare(array_shift($sqlRequestQueue));
                    $statement->executeStatement();
                }
                $outputFunction('Client Carl '.$carlClient->getTitle().' : '.count(self::DB_CARL_ENTITIES_CLASSES).' tables étrangères créées avec succès.');
            } catch (ForbiddenDoubleQuoteCharacterException|EmptyStringException $e) {
                // la configuration de ce client n'est pas bonne
                // et ne sera simplement pas traitée, on passe au suivant
                $outputFunction('Client Carl '.$carlClient->getTitle().' ignoré : '.$e->getMessage());
            }
        }
    }
}
