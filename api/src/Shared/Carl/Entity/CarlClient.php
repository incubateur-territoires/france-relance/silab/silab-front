<?php

namespace App\Shared\Carl\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Intervention\Entity\Activite\Evaluation;
use App\Shared\Carl\Exception\AttachementTypeUnsupported;
use App\Shared\Carl\Exception\MultipleCarlObjectsFoundWithGivenFilters;
use App\Shared\Carl\Exception\MultipleEmailsFoundForUser;
use App\Shared\Carl\Exception\MultipleUsersFoundForEmailException;
use App\Shared\Carl\Exception\NoAvailableGedException;
use App\Shared\Carl\Exception\NoCarlObjectsFoundWithGivenFilters;
use App\Shared\Carl\Exception\NoEmailFoundForUser;
use App\Shared\Carl\Exception\NoUserFoundForEmailException;
use App\Shared\Carl\Exception\UnknownEquipementTypeException;
use App\Shared\Carl\Repository\CarlClientRepository;
use App\Shared\Carl\ValueObject\CarlAccessToken;
use App\Shared\Carl\ValueObject\CarlDocument;
use App\Shared\Carl\ValueObject\CarlLinkedUrlDocument;
use App\Shared\Carl\ValueObject\CarlStoredDocument;
use App\Shared\Document\Entity\DocumentBlob;
use App\Shared\Document\Entity\Image;
use App\Shared\Document\Entity\ImageInterface;
use App\Shared\Exception\RuntimeException;
use App\Shared\Ged\Entity\GedClient;
use App\Shared\Ged\Entity\GedUserInterface;
use App\Shared\Ged\Exception\UnsupportedUrlException;
use App\Shared\Helpers;
use App\Shared\JsonApi\JsonApiResource;
use App\Shared\JsonApi\JsonApiResourceCollection;
use App\Shared\User\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

#[Get(
    security: "is_granted('ROLE_CONSULTER_CARL_CLIENTS')",
    normalizationContext: ['groups' => [CarlClient::GROUP_AFFICHER_CARL_CLIENT]]
)]
#[GetCollection(
    security: "is_granted('ROLE_CONSULTER_CARL_CLIENTS')",
    normalizationContext: ['groups' => [CarlClient::GROUP_AFFICHER_CARL_CLIENT]]
)]
// QUESTION_TECHNIQUE : comment injecter le logger ici ?
#[ORM\Entity(repositoryClass: CarlClientRepository::class)]
class CarlClient implements GedUserInterface
{
    public const CARL_WO_MR_IMAGE_FIELD_AVANT = 'xtraTxt07';
    public const CARL_WO_MR_IMAGE_FIELD_APRES = 'xtraTxt09';
    private const CARL_API_ORIGIN = 'silab';
    private const CARL_API_ENTITY_ENDPOINT = 'entities/v1';
    private const CARL_API_UI_ENDPOINT = 'ui/v1';
    private const CARL_API_AUTH_ENDPOINT = 'auth/v1';
    private const CARL_API_SUB_PATH = 'api/';
    private const CARL_ERROR_500_MAX_TRIES = 3;
    public const STATUS_COMMENT_MAX_LENGTH = 250;

    /**
     * Les données considérées comme stable vont être généralement des données du référentiel
     * (équipement, acteurs, configuration).
     */
    public const APCU_TTL_DATA_STABLE = 60 * 60 * 12;
    /**
     * Les données liées à l'activité par exemple (interventions, mesures) sont considérée comme plus
     * sujetes aux changement.
     */
    public const APCU_TTL_DATA_TEMPS_REEL = 1;

    public const GROUP_AFFICHER_CARL_CLIENT = 'Afficher le carl client';

    /**
     * @var array<string,string>
     *
     * On ne peut pas utiliser d'enum en clé de tableau, utilise donc sa valeur, https://wiki.php.net/rfc/object_keys_in_arrays
     */
    public const SILAB_EVALUATION_TO_CARL_EVALUATION_ID = [
        'conforme' => 'CONFORME',
        'reserve' => 'RESERVE',
        'non conforme' => 'NON-CONFORME',
    ];

    /**
     * @var array<string,Evaluation>
     */
    public const CARL_EVALUATION_ID_TO_SILAB_EVALUATION = [
        'CONFORME' => Evaluation::Conforme,
        'OBSERVATION' => Evaluation::Reserve,
        'RESERVE' => Evaluation::Reserve,
        '15c717a9df2-3184a2' => Evaluation::Reserve, // correspond à 'Non-présenté' sur carl GP //CONFIGURATION: A rendre configurable
        'NON-CONFORME' => Evaluation::NonConforme,
    ];

    public const SILAB_TO_CARL_DEMANDEINTERVENTION_STATUS_MAPPING = [
        'awaiting_validation' => ['REQUEST', 'PREPARE', 'APPROBATION', 'ARBITRAGE', 'PREPA_BUDG'],
        'awaiting_information' => ['WAITINFO'],
        'accepted' => ['AWAITINGREAL', 'INPROGRESS'],
        'closed' => ['FINISHED', 'CLOSED', 'DENIED'],
    ];

    public const CARL_TO_SILAB_DEMANDEINTERVENTION_STATUS_MAPPING = [
        'REQUEST' => ['awaiting_validation'],
    ];

    public const SILAB_TO_CARL_INTERVENTION_STATUS_MAPPING = [
        'awaiting_validation' => [
            'REQUEST',
            'AWAITINGVALID',
            'AWAITINGDOCS',
            'AWAITINGESTIMATE',
            'AWAITINGINFO',
            'VALID_N+1',
            'REFUSED',
        ],
        'awaiting_realisation' => ['AWAITINGREAL', 'INPROGRESS', 'VALIDATE', 'AWAITINGITEM'],
        'closed' => ['FINISHED', 'CLOSED', 'ARCHIVED', 'CANCEL', 'AWAITINGRECOMP'],
    ];

    public const CARL_TO_SILAB_INTERVENTION_STATUS_MAPPING = [
        'CLOSED' => ['closed'],
        'AWAITINGRECOMP' => ['awaiting_replenishment'],
    ];

    public const DEMANDEINTERVENTION_STATUS_WORKFLOW = [
        'WAITINFO' => 'REQUEST',
    ];

    /**
     * On veut traiter les material et box de la même manière mais ils ne stockent pas la description longue dans le
     * même champ, cette methode permmet de faire la correspondance.
     */
    public function getEquipmentCommentFieldFromType(string $equipmentType): string
    {
        if (in_array($equipmentType, $this->getObjectAndDescendantTypes('MATERIAL'))) {
            return 'longDesc';
        }

        if (
            in_array($equipmentType, $this->getObjectAndDescendantTypes('BOX'))
        ) {
            return 'comment';
        }

        throw new UnknownEquipementTypeException($equipmentType);
    }

    /**
     * @var array<string,array<string,string>> ['statusflow'=>['statusCode'=>'statusDescription']]
     */
    private $statusesDescriptionByCodeByStatusFlow = [];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([CarlConfigurationIntervention::GROUP_AFFICHER_CARL_CONFIGURATION_INTERVENTION, CarlClient::GROUP_AFFICHER_CARL_CLIENT])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([CarlConfigurationIntervention::GROUP_AFFICHER_CARL_CONFIGURATION_INTERVENTION, CarlClient::GROUP_AFFICHER_CARL_CLIENT])]
    protected ?string $title = null;

    #[ORM\Column(length: 255, options: ['default' => 'à changer'])]
    private ?string $login = null;

    #[ORM\Column(length: 255, options: ['default' => 'à changer'])]
    private ?string $password = null;

    #[ORM\Column(length: 255, options: ['default' => 'à changer'])]
    private ?string $baseUrl = null;

    #[ORM\Column(options: ['default' => 100])]
    private ?int $pageLimit = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $dbServer = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $dbShema = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $dbUser = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $dbPassword = null;

    /** @var array<string,string> $priorityMapping la liste de valeur doit être ordonnée par ordre décroissant. */
    #[ORM\Column(options: ['default' => '[]'])]
    private array $priorityMapping = [];

    #[ORM\ManyToOne]
    private ?GedClient $gedDocumentWriter = null;

    /** @var array<string,string> */
    #[ORM\Column(options: ['default' => '[]'])]
    private array $photoIllustrationEquipementDoctypeIds = [];

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $champCarlPourStockageMaterielConsomme = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $champCarlPourStockageDateDebutSouhaiteeDI = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups([CarlClient::GROUP_AFFICHER_CARL_CLIENT])]
    private ?string $woViewUrlSubPath = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups([CarlClient::GROUP_AFFICHER_CARL_CLIENT])]
    private ?string $mrViewUrlSubPath = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups([CarlClient::GROUP_AFFICHER_CARL_CLIENT])]
    private ?string $boxViewUrlSubPath = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups([CarlClient::GROUP_AFFICHER_CARL_CLIENT])]
    private ?string $materialViewUrlSubPath = null;

    /**
     * Undocumented function.
     *
     * @param Collection<int,GedClient> $gedDocumentReaders
     */
    public function __construct(
        #[ORM\ManyToMany(targetEntity: GedClient::class)]
        private Collection $gedDocumentReaders = new ArrayCollection()
    ) {
    }

    /**
     * @return array<int,GedClient>
     */
    public function getGedDocumentReaders(): array
    {
        return $this->gedDocumentReaders->getValues();
    }

    /**
     * @param array<GedClient> $gedClients
     */
    public function setGedDocumentReaders(array $gedClients): self
    {
        $this->gedDocumentReaders = new ArrayCollection($gedClients); // new Collection ?

        return $this;
    }

    public function getGedDocumentWriter(): ?GedClient
    {
        return $this->gedDocumentWriter;
    }

    public function setGedDocumentWriter(?GedClient $gedClient): self
    {
        $this->gedDocumentWriter = $gedClient;

        return $this;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getBaseUrl(): ?string
    {
        return $this->baseUrl;
    }

    public function setBaseUrl(string $baseUrl): self
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    public function getApiUrl(): ?string
    {
        if (is_null($this->getBaseUrl())) {
            return null;
        }

        return $this->getBaseUrl().self::CARL_API_SUB_PATH;
    }

    public function getPageLimit(): ?int
    {
        return $this->pageLimit;
    }

    public function setPageLimit(int $pageLimit): self
    {
        $this->pageLimit = $pageLimit;

        return $this;
    }

    public function getDbServer(): ?string
    {
        return $this->dbServer;
    }

    public function setDbServer(?string $dbServer): self
    {
        $this->dbServer = $dbServer;

        return $this;
    }

    public function getDbShema(): ?string
    {
        return $this->dbShema;
    }

    public function setDbShema(?string $dbShema): self
    {
        $this->dbShema = $dbShema;

        return $this;
    }

    public function getDbUser(): ?string
    {
        return $this->dbUser;
    }

    public function setDbUser(?string $dbUser): self
    {
        $this->dbUser = $dbUser;

        return $this;
    }

    public function getDbPassword(): ?string
    {
        return $this->dbPassword;
    }

    public function setDbPassword(?string $dbPassword): self
    {
        $this->dbPassword = $dbPassword;

        return $this;
    }

    public function getChampCarlPourStockageMaterielConsomme(): ?string
    {
        return $this->champCarlPourStockageMaterielConsomme;
    }

    public function setChampCarlPourStockageMaterielConsomme(?string $champCarlPourStockageMaterielConsomme): static
    {
        $this->champCarlPourStockageMaterielConsomme = $champCarlPourStockageMaterielConsomme;

        return $this;
    }

    public function getChampCarlPourStockageDateDebutSouhaiteeDI(): ?string
    {
        return $this->champCarlPourStockageDateDebutSouhaiteeDI;
    }

    public function setChampCarlPourStockageDateDebutSouhaiteeDI(?string $champCarlPourStockageDateDebutSouhaiteeDI): static
    {
        $this->champCarlPourStockageDateDebutSouhaiteeDI = $champCarlPourStockageDateDebutSouhaiteeDI;

        return $this;
    }

    public function getWoViewUrlSubPath(): ?string
    {
        return $this->woViewUrlSubPath;
    }

    public function setWoViewUrlSubPath(?string $woViewUrlSubPath): static
    {
        $this->woViewUrlSubPath = $woViewUrlSubPath;

        return $this;
    }

    public function getMrViewUrlSubPath(): ?string
    {
        return $this->mrViewUrlSubPath;
    }

    public function setMrViewUrlSubPath(?string $mrViewUrlSubPath): static
    {
        $this->mrViewUrlSubPath = $mrViewUrlSubPath;

        return $this;
    }

    public function getBoxViewUrlSubPath(): ?string
    {
        return $this->boxViewUrlSubPath;
    }

    public function setBoxViewUrlSubPath(?string $boxViewUrlSubPath): static
    {
        $this->boxViewUrlSubPath = $boxViewUrlSubPath;

        return $this;
    }

    public function getMaterialViewUrlSubPath(): ?string
    {
        return $this->materialViewUrlSubPath;
    }

    public function setMaterialViewUrlSubPath(?string $materialViewUrlSubPath): static
    {
        $this->materialViewUrlSubPath = $materialViewUrlSubPath;

        return $this;
    }

    // Note: créer un client http à chaque fois peut empêcher de faire des requêtes en parrallèle
    private function getCarlHttpClient(): HttpClientInterface
    {
        return HttpClient::createForBaseUri(
            $this->getApiUrl(),
        );
    }

    /**
     * Retourne le token d'accès si éxistant ou en génère un nouveau si besoin.
     */
    private function getToken(bool $forceRefresh = false): string
    {
        $apcuAccessTokenKey = $this->getCacheKey('accessToken');

        if (false === $forceRefresh) {
            $accessToken = apcu_fetch($apcuAccessTokenKey);

            if (false !== $accessToken) {
                assert($accessToken instanceof CarlAccessToken);
                if ($accessToken->isNotExpired()) {
                    return $accessToken->getXcsAccessToken();
                }
            }
        }

        $accessToken = $this->generateToken();

        apcu_store(
            $apcuAccessTokenKey,
            $accessToken,
            $accessToken->getTimeToLive()
        );

        return $accessToken->getXcsAccessToken();
    }

    /**
     * Génére un token grâce aux identifiants Carl.
     *
     * @return CarlAccessToken un nouveau token fraichement généré. Note : générer un token ouvre un session sur carl
     */
    private function generateToken(): CarlAccessToken
    {
        $response = $this->getCarlHttpClient()->request(
            'POST',
            self::CARL_API_AUTH_ENDPOINT.'/authenticate',
            [
                'body' => [
                    'login' => $this->getLogin(),
                    'password' => $this->getPassword(),
                    'origin' => self::CARL_API_ORIGIN,
                ],
            ]
        );

        $body = $response->toArray();

        if (!array_key_exists('X-CS-Access-Token', $body)) {
            $message = 'Token non trouvé dans la réponse carl';
            throw new RuntimeException($message);
        }

        return CarlAccessToken::fromNow(
            xcsAccessToken: $body['X-CS-Access-Token'],
            validityTimeInSeconds: intval(floor(
                $body['expires_in'] / 1000
            ))// la date d'expiration carl est en millisecondes
        );
    }

    /**
     * appel simple à l'API carl.
     *
     * @param string|null  $body    inséré dans le body de la requête
     * @param array<mixed> $options option du client http
     *                              voir: https://symfony.com/doc/current/http_client.html#making-requests
     *
     * @return array<mixed> le retour de carl
     */
    public function call(
        string $method,
        string $absoluteUrl,
        mixed $body = null,
        array $options = [],
    ): array {
        $options = array_replace_recursive(
            [
                'headers' => [
                    'Accept' => 'application/vnd.api+json',
                ],
                'query' => [
                    'page[limit]' => $this->getPageLimit(),
                ],
                'body' => $body,
            ],
            $options
        );
        $responseArray = []; // Note: si on ne met pas ça, l'analyse statique pense que : Variable $responseArray might not be defined.

        $responseArray = $this->callRaw(
            method: $method,
            absoluteUrl: $absoluteUrl,
            options: $options
        )->toArray();

        return $responseArray;
    }

    /**
     * appel simple à l'API carl.
     *
     * @param array<mixed> $options option du client http
     *                              voir: https://symfony.com/doc/current/http_client.html#making-requests
     *
     * @return ResponseInterface le retour brut de carl
     */
    public function callRaw(
        string $method,
        string $absoluteUrl,
        array $options = [],
    ): ResponseInterface {
        $options = array_replace_recursive(
            [
                'headers' => [
                    'X-CS-Access-Token' => $this->getToken(),
                ],
            ],
            $options
        );
        // inspiré par https://stackoverflow.com/questions/25002164/php-try-catch-and-retry
        $nbTries = 0;
        while (true) {
            if (self::CARL_ERROR_500_MAX_TRIES == $nbTries) {
                // si on est sur la dernière tentative on regénère le token
                // car si ça se trouve la session du token est fermée
                $options['headers']['X-CS-Access-Token'] = $this->getToken(forceRefresh: true);
            }
            try {
                $response = $this->getCarlHttpClient()->request(
                    $method,
                    $absoluteUrl,
                    $options
                );

                $response->getHeaders(); // Note: il est Nécessaire de déclencher les erreurs ici, c'est pourquoi on appelle getHeaders

                // cf. https://symfony.com/doc/current/http_client.html#handling-exceptions
                return $response;
            } catch (HttpExceptionInterface $clientException) { // voir https://symfony.com/doc/current/http_client.html#handling-exceptions
                // on essaye de récupérer les jolies erreurs carl en json si c'est possible
                ++$nbTries;

                if ($nbTries > self::CARL_ERROR_500_MAX_TRIES) {
                    try {
                        $errorMessages = array_reduce(
                            $clientException->getResponse()->toArray(false)['errors'],
                            function ($carlErrorMessages, $carlError) {
                                $carlErrorMessages[] =
                                    isset($carlError['title'])
                                        ? $carlError['title']
                                        : 'aucun titre'
                                    .' : '.($carlError['detail'] ?? '');

                                return $carlErrorMessages;
                            },
                            []
                        );
                        $carlErrorMessage = implode(', ', $errorMessages);
                    } catch (\Exception $e) {
                        $carlErrorMessage = $clientException->getResponse()->getContent(false);
                    }
                    throw new RuntimeException("Erreurs Carl rencontrées : $carlErrorMessage");
                }
                continue;
            }
        }
    }

    /**
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     * @param string                      $entity   le code de l'entité requêtée, ex: wo
     *
     * @return array<mixed>
     */
    public function getCarlObjectRaw(
        string $entity,
        string $id,
        array $includes = [],
        array $fields = [],
        array $sort = []
    ): array {
        $options = [
            'query' => $this->buildJsonApiQuery([], $includes, $fields, $sort),
        ];

        $absoluteUrl = $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.'/'.urlencode($entity).'/'.urlencode($id);

        return $this->call('GET', $absoluteUrl, null, $options);
    }

    /**
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     * @param string                      $entity   le code de l'entité requêtée, ex: wo
     */
    public function getCarlObject(
        string $entity,
        string $id,
        array $includes = [],
        array $fields = [],
        array $sort = []
    ): JsonApiResource {
        $objectApiResponse = $this->getCarlObjectRaw(
            entity: $entity,
            id: $id,
            includes: $includes,
            fields: $fields,
            sort: $sort
        );
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        return new JsonApiResource(
            $objectApiResponse['data'],
            $propertyAccessor->getValue($objectApiResponse, '[included]')
        );
    }

    /**
     * La méthode getCarlObjects est plus aisée à manipuler mais consomme potentiellement plus de RAM.
     * Vous pouvez utiliser cette méthode pour des besoin d'optimisation (impact réèl à mesurer).
     *
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $filters  voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-filtering
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     * @param string                      $entity   le code de l'entité requêtée, ex: wo
     *
     * @return array<mixed>
     */
    public function getCarlObjectsRaw(
        string $entity,
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array $sort = [],
        ?int $pageLimit = null
    ): array {
        $absoluteUrl = $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.'/'.urlencode($entity);

        return $this->getCarlObjectsByAbsoluteUrlRaw($absoluteUrl, $filters, $includes, $fields, $sort, $pageLimit);
    }

    /**
     * Cette méthode tranforme pour nous les élément en JsonApiResource.
     *
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $filters  voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-filtering
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     * @param string                      $entity   le code de l'entité requêtée, ex: wo
     */
    public function getCarlObjects(
        string $entity,
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array $sort = [],
        ?int $pageLimit = null
    ): JsonApiResourceCollection {
        $objectsApiResponse = $this->getCarlObjectsRaw($entity, $filters, $includes, $fields, $sort, $pageLimit);
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $objectApiResources = new JsonApiResourceCollection($propertyAccessor->getValue($objectsApiResponse['links'], '[next]'));
        foreach ($objectsApiResponse['data'] as $objectApiResponseData) {
            $objectApiResources[] = new JsonApiResource(
                $objectApiResponseData,
                $propertyAccessor->getValue($objectsApiResponse, '[included]')
            );
        }

        return $objectApiResources;
    }

    /**
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $filters  voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-filtering
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     * @param string                      $entity   le code de l'entité requêtée, ex: wo
     *
     * @throws MultipleCarlObjectsFoundWithGivenFilters
     * @throws NoCarlObjectsFoundWithGivenFilters
     */
    public function getOneCarlObject(
        string $entity,
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array $sort = []
    ): JsonApiResource {
        $absoluteUrl = $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.'/'.urlencode($entity);

        $carlObjects = $this->getCarlObjectsByAbsoluteUrlRaw($absoluteUrl, $filters, $includes, $fields, $sort);

        if (sizeof($carlObjects['data']) > 1) {
            throw new MultipleCarlObjectsFoundWithGivenFilters("Plusieurs objets $entity trouvés pour les filtres donnés");
        }

        if (sizeof($carlObjects['data']) < 1) {
            throw new NoCarlObjectsFoundWithGivenFilters("Aucun objet $entity trouvé pour les filtres donnés");
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        return new JsonApiResource(
            $carlObjects['data'][0],
            $propertyAccessor->getValue($carlObjects, '[included]')
        );
    }

    /**
     * Limitation : La donnée retournée ne contien pas la section relative à la pagination 'links' (self, first, next ...).
     *
     * @param array<string>               $orderOfValues par exemple ['urgent','important','normal']
     * @param array<string,array<string>> $filters       voir doc json-api :
     *                                                   https://jsonapi.org/format/#fetching-filtering
     * @param array<string>               $includes      Attention, une relation doit être inclue
     *                                                   dans les displayedMembers
     *                                                   (si displayedMembers n'est pas vide)
     * @param array<string,array<string>> $fields        champs à inclure pour chaque type voir doc json-api :
     *                                                   https://jsonapi.org/format/#fetching-includes, ex:
     *                                                   ['type1'
     *                                                   =>
     *                                                   ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>|null          $secondarySort
     *
     * @return array<mixed>
     */
    private function getCarlObjectsByAbsoluteUrlWithProvidedOrderOfValues(
        string $absoluteUrl,
        string $sortAttribut,
        array $orderOfValues,
        array $filters = [],
        array $includes = [],
        array $fields = [],
        ?array $secondarySort = ['createDate'],
        ?int $pageLimit = null
    ): array {
        $missingObjects = $this->getPageLimit();

        $allObjects = [
            'data' => [],
            'included' => [],
        ];

        if (!empty($filters[$sortAttribut])) {
            $orderOfValues = array_intersect($orderOfValues, $filters[$sortAttribut]);
        }

        foreach ($orderOfValues as $targetValue) {
            $filters[$sortAttribut] = [$targetValue];
            $options = [
                'query' => $this->buildJsonApiQuery($filters, $includes, $fields, $secondarySort, $pageLimit),
            ];
            $options['query']['page[limit]'] = $missingObjects;

            $newObjects = $this->call('GET', $absoluteUrl, null, $options);

            $newObjectsCount = count($newObjects['data']);

            if (0 === $newObjectsCount) {
                continue;
            }

            $newIncludedRelationships = [];

            foreach ($newObjects['included'] as $relationship) {
                if (in_array($relationship, $allObjects['included'])) {
                    continue;
                }
                $newIncludedRelationships[] = $relationship;
            }

            $allObjects = array_merge_recursive($allObjects, ['data' => $newObjects['data'], 'included' => $newIncludedRelationships]);

            $missingObjects -= $newObjectsCount;

            if (0 === $missingObjects) {
                break;
            }
        }

        return $allObjects;
    }

    /**
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $filters  voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-filtering
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     *
     * @return array<mixed>
     */
    public function getCarlObjectsByAbsoluteUrlRaw(
        string $absoluteUrl,
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array $sort = [],
        ?int $pageLimit = null
    ): array {
        // $sortedAttributesThatNeedsOrderOfValuesProvision contient tout les atributs qui pour un tri necessitent de fournir l'ordre des valeurs souhaités. Par exemple pour 'workPriority' on peut vouloir ['faible','normale','importante'].
        $sortedAttributesThatNeedsOrderOfValuesProvision = [
            'workPriority',
            '-workPriority',
        ];

        $sortHasAttributesThatNeedsOrderOfValuesProvision = !empty(array_intersect($sort, $sortedAttributesThatNeedsOrderOfValuesProvision));

        // Ce bloc de code géré le cas ou l'on tri avec un ordre de valeur custom.
        if (!empty($sortHasAttributesThatNeedsOrderOfValuesProvision)) {
            $sortAttribut = array_intersect($sort, $sortedAttributesThatNeedsOrderOfValuesProvision);

            // On verifie qu'il n'y ait pas plus de deux tri, et que le premier soit celui qui necessite la fourniture d'une liste d'ordre de valeur.
            assert(2 <= count($sort));
            assert(1 === count($sortAttribut));
            assert($sortAttribut[0] === $sort[0]);

            $sortAttribut = array_shift($sort);

            $orderOfValues =
                match ($sortAttribut) {
                    '-workPriority' => array_keys($this->getPriorityMapping()),
                    'workPriority' => array_keys(array_reverse($this->getPriorityMapping())),
                    default => []
                };
            assert(!empty($orderOfValues));

            return $this->getCarlObjectsByAbsoluteUrlWithProvidedOrderOfValues(
                $absoluteUrl,
                trim($sortAttribut, '-'),
                $orderOfValues,
                $filters,
                $includes,
                $fields,
                $sort,
                $pageLimit
            );
        }

        $options = [
            'query' => $this->buildJsonApiQuery($filters, $includes, $fields, $sort, $pageLimit),
        ];

        return $this->call('GET', $absoluteUrl, null, $options);
    }

    /**
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $filters  voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-filtering
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     */
    public function getCarlObjectsByAbsoluteUrl(
        string $absoluteUrl,
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array $sort = []
    ): JsonApiResourceCollection {
        $objectsApiResponse = $this->getCarlObjectsByAbsoluteUrlRaw($absoluteUrl, $filters, $includes, $fields, $sort);
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $objectApiResources = new JsonApiResourceCollection($propertyAccessor->getValue($objectsApiResponse['links'], '[next]'));
        foreach ($objectsApiResponse['data'] as $objectApiResponseData) {
            $objectApiResources[] = new JsonApiResource(
                $objectApiResponseData,
                $propertyAccessor->getValue($objectsApiResponse, '[included]')
            );
        }

        return $objectApiResources;
    }

    /**
     * @param array<string,array<string>> $filters  voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-filtering
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     * @param array<string>               $sort
     *
     * @return array<mixed> le tableau query pour le httpclient
     */
    private function buildJsonApiQuery(
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array $sort = [],
        ?int $pageLimit = null
    ): array {
        $query = [];

        foreach ($filters as $attributeKey => $allowedAttributeValues) {
            if (is_array($allowedAttributeValues) && array_key_exists('LIKE', $allowedAttributeValues)) {
                $query["filter[$attributeKey][LIKE]"] = $allowedAttributeValues['LIKE'];
                continue;
            }
            $query["filter[$attributeKey]"] = join(',', $allowedAttributeValues);
        }

        if (!empty($includes)) {
            $query['include'] = join(',', $includes);
        }

        foreach ($fields as $includedEntityType => $includedAttributeValues) {
            $query["fields[$includedEntityType]"] = join(',', $includedAttributeValues);
        }

        if (!empty($sort)) {
            $query['sort'] = join(',', $sort);
        }

        if (!is_null($pageLimit)) {
            $query['page[limit]'] = strval($pageLimit);
        }

        return $query;
    }

    /**
     * @param string        $entity         le code de l'entité requêtée, ex: wo
     * @param string        $postedResource le JSON de l'entity postée
     * @param array<string> $includes
     */
    public function postCarlObject(
        string $entity,
        string $postedResource,
        array $includes = []
    ): JsonApiResource {
        $absoluteUrl = $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.'/'.urlencode($entity);
        $options = [
            'headers' => ['Content-Type' => 'application/vnd.api+json'],
            'query' => $this->buildJsonApiQuery(includes: $includes),
        ];

        $resourceResponseArray = $this->call('POST', $absoluteUrl, $postedResource, $options);

        return new JsonApiResource($resourceResponseArray['data'], $resourceResponseArray['included'] ?? null);
    }

    /**
     * Amélioration à faire : pouvoir mettre à jour les relationships.
     *
     * @param array<string,string|number|bool|null> $attributes
     * @param array<string,mixed>                   $relationships
     * @param array<string>                         $includes
     */
    public function patchCarlObject(string $entity, string $objectId, array $attributes = [], array $relationships = [], array $includes = []): JsonApiResource
    {
        $objectData = ['data' => new \stdClass()];
        if (!empty($attributes)) {
            $objectData['data']->attributes = $attributes;
        }

        if (!empty($relationships)) {
            $objectData['data']->relationships = $relationships;
        }

        $options = [
            'headers' => ['Content-Type' => 'application/vnd.api+json'],
            'query' => $this->buildJsonApiQuery(includes: $includes),
        ];

        $entityAbsoluteUrl = $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.'/'.urlencode($entity).'/'.$objectId;

        $resourceResponseArray = $this->call(
            'PATCH',
            $entityAbsoluteUrl,
            json_encode($objectData),
            $options
        );

        return new JsonApiResource($resourceResponseArray['data'], $resourceResponseArray['included'] ?? null);
    }

    public function getUserIdFromEmail(string $userEmail): string
    {
        $filters = [
            'type' => ['EMAIL'],
            'phoneNum' => [$userEmail],
        ];

        $includes = [
            'actor',
        ];

        $fields = [
            'actor' => ['id'],
            'phone' => ['actor'],
        ];

        $usersFound = $this->getCarlObjectsRaw(
            'phone',
            $filters,
            $includes,
            $fields,
        );

        if (!array_key_exists('included', $usersFound) || sizeof($usersFound['included']) < 1) {
            throw new NoUserFoundForEmailException($userEmail);
        }
        if (sizeof($usersFound['included']) > 1) {
            throw new MultipleUsersFoundForEmailException($userEmail);
        }

        $includedActor = new JsonApiResource(
            $usersFound['included'][0]
        );

        return $includedActor->getId();
    }

    public function getUserEmailFromId(string $userId): string
    {
        $userEmailCacheKey = $this->getCacheKey("userEmail/$userId");
        $filters = [
            'type' => ['EMAIL'],
            'actor.id' => [$userId],
        ];

        $fields = [
            'phone' => ['phoneNum'],
        ];

        $cachedEmail = apcu_fetch($userEmailCacheKey);
        if (false !== $cachedEmail) {
            return $cachedEmail;
        }

        try {
            $emailResource = $this->getOneCarlObject(
                'phone',
                $filters,
                [],
                $fields,
            );
        } catch (NoCarlObjectsFoundWithGivenFilters $e) {
            throw new NoEmailFoundForUser($userId, $e);
        } catch (MultipleCarlObjectsFoundWithGivenFilters $e) {
            throw new MultipleEmailsFoundForUser($userId, $e);
        }

        apcu_store(
            $userEmailCacheKey,
            $emailResource->getAttribute('phoneNum'),
            self::APCU_TTL_DATA_STABLE
        );

        return $emailResource->getAttribute('phoneNum');
    }

    public function getUserEmailFromIdSafe(string $userId): string
    {
        try {
            return $this->getUserEmailFromId($userId);
        } catch (NoEmailFoundForUser $e) {
            return "Aucun email renseigné pour l'acteur carl avec l'id $userId";
        } catch (MultipleEmailsFoundForUser $e) {
            return "Plusieurs emails renseignés pour l'acteur carl avec l'id $userId";
        }
    }

    /** @return array<string,string>  les valeurs retournées sont ordonnées par ordre décroissant*/
    public function getPriorityMapping(): array
    {
        return $this->priorityMapping;
    }

    /** @param array<string,string> $priorityMapping */
    public function setPriorityMapping(array $priorityMapping): self
    {
        $this->priorityMapping = $priorityMapping;

        return $this;
    }

    /** @return array<string,string>  les valeurs retournées sont ordonnées par ordre décroissant*/
    public function getPhotoIllustrationEquipementDoctypeIds(): array
    {
        return $this->photoIllustrationEquipementDoctypeIds;
    }

    /** @param array<string,string> $photoIllustrationEquipementDoctypeId */
    public function setPhotoIllustrationEquipementDoctypeIds(array $photoIllustrationEquipementDoctypeId): self
    {
        $this->photoIllustrationEquipementDoctypeIds = $photoIllustrationEquipementDoctypeId;

        return $this;
    }

    public function deleteObjectByAbsoluteUrl(string $absoluteUrl, ?string $bodyJson = null): void
    {
        $this->callRaw(
            method: 'DELETE',
            absoluteUrl: $absoluteUrl,
            options: ['body' => $bodyJson],
        );
    }

    public function deleteObject(string $entity, string $id): void
    {
        $this->deleteObjectByAbsoluteUrl(
            $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.
                '/'.urlencode($entity).
                '/'.$id
        );
    }

    public function addDocumentLinkToObject(string $documentUrl, string $objectId, string $objectJavaClass, string $userId, string $docTypeId, string $documentTitle): CarlDocument
    {
        $attachementId = $this->createUrlAttachement($documentUrl, $userId);

        return $this->addDocumentToObject(
            attachementId: $attachementId,
            objectId: $objectId,
            objectJavaClass: $objectJavaClass,
            userId: $userId,
            docTypeId: $docTypeId,
            documentTitle: $documentTitle
        );
    }

    public function addDocumentToObject(string $attachementId, string $objectId, string $objectJavaClass, string $userId, string $docTypeId, string $documentTitle): CarlDocument
    {
        $documentId = $this->createDocument(
            $attachementId,
            $docTypeId,
            $documentTitle
        );
        $documentLinkId = $this->linkDocumentToObject($documentId, $objectId, $objectJavaClass, $userId);

        return new CarlDocument(
            documentLinkId: $documentLinkId,
            documentId: $documentId
        );
    }

    /**
     * @return string id de l'url d'attachement
     */
    private function createUrlAttachement(string $attachmentUrl, string $userId): string
    {
        $dataImageUrl = $this->extractDataFromUrl($attachmentUrl);
        $urlToEncode = [
            'data' => [
                'type' => 'url',
                'attributes' => [
                    'path' => $dataImageUrl['path'],
                    'fileName' => $dataImageUrl['name'],
                ],
                'relationships' => [
                    'publishedBy' => [
                        'data' => [
                            'id' => $userId,
                            'type' => 'actor',
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->postCarlObject('url', json_encode($urlToEncode));

        return $response->getId();
    }

    /**
     * @return array<string,string> un tableau représentant les données du l'url. Il est constitué des clés 'path' et 'name'.
     */
    private function extractDataFromUrl(string $url): array
    {
        $separatorPosition = strrpos($url, '/');

        return [
            'path' => substr($url, 0, $separatorPosition + 1),
            'name' => substr($url, $separatorPosition + 1),
        ];
    }

    /**
     * @return string id du document
     */
    private function createDocument(string $attachementId, string $docTypeId, string $title): string
    {
        $documentToEncode = [
            'data' => [
                'type' => 'document',
                'attributes' => [
                    'description' => $title,
                    'library' => false,
                ],
                'relationships' => [
                    'type' => [
                        'data' => [
                            'id' => $docTypeId,
                            'type' => 'doctype',
                        ],
                    ],
                    'attachment' => [
                        'data' => [
                            'id' => $attachementId,
                            'type' => 'url',
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->postCarlObject('document', json_encode($documentToEncode));

        return $response->getId();
    }

    /**
     * @return string l'id du documentLink
     */
    private function linkDocumentToObject(string $documentId, string $objectId, string $objectJavaClass, string $userId): string
    {
        $linkToEncode = [
            'data' => [
                'type' => 'documentlink',
                'attributes' => [
                    'entityClass' => $objectJavaClass,
                    'entityId' => $objectId,
                ],
                'relationships' => [
                    'document' => [
                        'data' => [
                            'type' => 'document',
                            'id' => $documentId,
                        ],
                    ],
                    'attachedBy' => [
                        'data' => [
                            'id' => $userId,
                            'type' => 'actor',
                        ],
                    ],
                ],
            ],
        ];

        return $this->postCarlObject('documentlink', json_encode($linkToEncode))->getId();
    }

    /**
     * @return array<CarlLinkedUrlDocument>
     */
    public function getObjectUrlDocuments(string $objectId, string $doctypeId): array // performances: ici on appelle à chaque inter/di récupérée
    {
        $linkedDocumentsJsonApiResourceCollection = $this->getLinkedDocuments(
            $objectId,
            [
                'document.type.id' => [$doctypeId],
            ]
        );

        return array_map(
            function ($documentLinkJsonApiResource) {
                $documentPath = $documentLinkJsonApiResource->getRelationship('document')->getRelationship('attachment')->getAttribute('path');
                $documentFileName = $documentLinkJsonApiResource->getRelationship('document')->getRelationship('attachment')->getAttribute('fileName');

                return new CarlLinkedUrlDocument(
                    $documentLinkJsonApiResource->getId(),
                    $documentLinkJsonApiResource->getRelationship('document')->getId(),
                    $documentPath.$documentFileName
                );
            },
            $linkedDocumentsJsonApiResourceCollection->getArrayCopy(),
        );
    }

    /**
     * @param array<string> $doctypeIds
     *
     * @return array<CarlDocument>
     */
    public function getObjectDocuments(string $objectId, array $doctypeIds): array // performances: ici on appelle à chaque inter/di récupérée
    {
        $linkedDocumentsJsonApiResourceCollection = $this->getLinkedDocuments($objectId, ['document.type.id' => $doctypeIds]);

        return array_map(
            function ($documentLinkJsonApiResource) {
                return new CarlDocument(
                    $documentLinkJsonApiResource->getId(),
                    $documentLinkJsonApiResource->getRelationship('document')->getId(),
                );
            },
            $linkedDocumentsJsonApiResourceCollection->getArrayCopy(),
        );
    }

    /**
     * Cette fonction assure simplement la compatibilité de type entre document et image sans aucune autre sorte de vérification.
     *
     * @param array<string> $doctypeIds
     *
     * @return array<ImageInterface>
     */
    public function getObjectAttachedImages(string $objectId, array $doctypeIds): array
    {
        return array_map(
            function ($attachedDocument) {
                return new Image(
                    id: $attachedDocument->getDocumentlinkId(),
                    url: 'url privée'
                );
            },
            $this->getObjectDocuments(
                objectId: $objectId,
                doctypeIds: $doctypeIds
            )
        );
    }

    /**
     * @return ?string Retourne le premier url trouvé corresspondant aux critères, ou null
     */
    public function getObjectDocumentUrl(string $objectId, string $doctypeId): ?string
    {
        $objectUrlDocuments = $this->getObjectUrlDocuments($objectId, $doctypeId);

        if (empty($objectUrlDocuments)) {
            return null;
        }

        return $objectUrlDocuments[0]->getAttachmentUrl();
    }

    /**
     * @param array<string,array<string>> $filters voir doc json-api :
     *                                             https://jsonapi.org/format/#fetching-filtering
     */
    private function getLinkedDocuments(string $entityId, array $filters = []): JsonApiResourceCollection
    {
        $filters['entityId'] = [$entityId];

        $includes = ['document', 'document.attachment'];

        $fields = [
            'documentlink' => [
                'document',
            ],
            'document' => [
                'attachment',
            ],
            'url' => [
                'path',
                'fileName',
            ],
        ];

        $sort = ['attachDt'];

        return $this->getCarlObjects(
            entity: 'documentlink',
            filters: $filters,
            includes: $includes,
            fields: $fields,
            sort: $sort
        );
    }

    private function createDescriptionObject(string $description): JsonApiResource
    {
        $descriptionJson = json_encode([
            'data' => [
                'type' => 'description',
                'attributes' => [
                    'description' => $description,
                ],
            ],
        ]);

        return $this->postCarlObject('description', $descriptionJson);
    }

    /**
     * @param array<mixed> $objectRelationShipsArray
     */
    public function addDescriptionToObjectRelationshipsArray(array &$objectRelationShipsArray, string $description, string $descriptionRelashionshipName): void
    {
        $objectRelationShipsArray[$descriptionRelashionshipName] = [
            'data' => [
                'id' => $this->createDescriptionObject($description)->getId(),
                'type' => 'description',
            ],
        ];
    }

    /**
     * Supprime la relation ainsi que la description.
     *
     * @param string $descriptionRelashionshipName eg. longDesc, comment
     */
    public function deleteObjectDescription(string $objectId, string $entity, string $descriptionRelashionshipName): void
    {
        $objectRessource = $this->getCarlObject($entity, $objectId, [$descriptionRelashionshipName]);
        $relationshipLink = $objectRessource->getRelationshipSelfLink($descriptionRelashionshipName);
        $descriptionId = $objectRessource->getRelationship($descriptionRelashionshipName)->getId();

        $this->deleteObjectByAbsoluteUrl($relationshipLink, json_encode(['data' => null]));
        $this->deleteObject('description', $descriptionId);
    }

    /**
     * @param array<string,string> $statusCodeWorkflow
     * @param ?string              $targetStatusComment Le commentaire à appliquer sur le dernier changement de statut
     */
    public function transitionToStatus(string $entityType, string $objectId, string $targetCarlStatusCode, array $statusCodeWorkflow, ?string $targetStatusComment = ''): void
    {
        do {
            $object = $this->getCarlObject(
                $entityType,
                $objectId
            );

            $objectStatusCode = $object->getAttribute('statusCode');
            if (!array_key_exists($objectStatusCode, $statusCodeWorkflow)) {
                throw new RuntimeException('Etat '.$objectStatusCode.' non pris en charge. Liste des status pris en charge : '.implode(', ', array_keys($statusCodeWorkflow)).'.');
            }

            $nextReachableStatusCode = $statusCodeWorkflow[$objectStatusCode];

            if ($nextReachableStatusCode === $targetCarlStatusCode) {
                $statusComment = $targetStatusComment;
            } else {
                $statusComment = "Statut appliqué automatiquement via Siilab pour transition vers statut $targetCarlStatusCode.";
            }

            $newStatusCode = $this->doStatusTransition(
                $object,
                $nextReachableStatusCode,
                $statusComment
            );
        } while ($targetCarlStatusCode !== $newStatusCode);
    }

    /**
     * @return string le nouveau status
     */
    private function doStatusTransition(JsonApiResource $objectApiResource, string $nextStatus, ?string $comment = ''): string
    {
        //  Générer l'url
        $workflowTransitionUrl = $objectApiResource->getLink('workflow-transitions');
        //  Générer le body
        $filters = [
            'nextStepCode' => [$nextStatus],
            'id' => ['LIKE' => ':com.carl.xnet.system.status.TransitionParameters'],
        ];

        $bodyArray =
            $this->getCarlObjectsByAbsoluteUrlRaw(
                $workflowTransitionUrl,
                $filters
            )
        ;

        $bodyArray['data'] = [$bodyArray['data'][0]]; // on s'assure qu'il n'y ait qu'une seule transition

        $bodyArray['data'][0]['attributes']['transitionParameters'] = [
            'comment' => $comment,
        ];

        $body = json_encode($bodyArray);
        //  Faire le post
        $options = ['headers' => ['Content-Type' => 'application/vnd.api+json']];
        $response = $this->call('POST', $workflowTransitionUrl, $body, $options);

        return $response['data']['attributes']['nextStepCode'];
    }

    private function cacheStatusesDescription(string $statusFlowCode): void
    {
        $statusesDescriptionByCode = [];
        $stepsJsonApiRessources = $this->getCarlObjects(
            entity: 'simplestep',
            filters: ['statusFlow.code' => [$statusFlowCode]],
            fields: ['simplestep' => ['code', 'description', 'statusFlow']]
        );
        foreach ($stepsJsonApiRessources as $stepJsonApiRessources) {
            $statusesDescriptionByCode[$stepJsonApiRessources->getAttribute('code')] =
                $stepJsonApiRessources->getAttribute('description') ?? '';
        }

        $this->statusesDescriptionByCodeByStatusFlow[$statusFlowCode] = $statusesDescriptionByCode;
    }

    /**
     * @return array<string,string>
     */
    private function getStatusesDescription(string $statusFlowCode): array
    {
        if (!array_key_exists($statusFlowCode, $this->statusesDescriptionByCodeByStatusFlow)) {
            $this->cacheStatusesDescription($statusFlowCode);
        }

        return $this->statusesDescriptionByCodeByStatusFlow[$statusFlowCode];
    }

    public function getStatusDescription(string $statusCode, string $statusFlowCode): string
    {
        return $this->getStatusesDescription($statusFlowCode)[$statusCode];
    }

    public function getUnitDescriptionFromCode(string $unitCode): string
    {
        $unitDescriptionCacheKey = $this->getCacheKey("unit/$unitCode");

        $cachedUnitDescription = apcu_fetch($unitDescriptionCacheKey);

        if (false !== $cachedUnitDescription) {
            return $cachedUnitDescription;
        }

        $unitJsonApiRessource = $this->getOneCarlObject(
            entity: 'unit',
            filters: ['code' => [$unitCode]],
            fields: ['unit' => ['description']]
        );

        apcu_store(
            $unitDescriptionCacheKey,
            $unitJsonApiRessource->getAttribute('description') ?? '',
            self::APCU_TTL_DATA_STABLE
        );

        return $unitJsonApiRessource->getAttribute('description') ?? '';
    }

    /**
     * @return array<string,string>
     */
    #[Groups([CarlClient::GROUP_AFFICHER_CARL_CLIENT, CarlConfigurationIntervention::GROUP_AFFICHER_CARL_CONFIGURATION_INTERVENTION])]
    #[Context([Serializer::EMPTY_ARRAY_AS_OBJECT => true])]
    public function getAvailableActionTypes(): array
    {
        $availableActionTypesJsonApiRessources = $this->getCarlObjects(
            entity: 'actiontype',
            fields: ['actiontype' => ['description']],
            sort: ['description']
        );

        $availableActionTypes = [];

        foreach ($availableActionTypesJsonApiRessources as $availableActionTypesJsonApiRessource) {
            $availableActionTypes[$availableActionTypesJsonApiRessource->getId()] = $availableActionTypesJsonApiRessource->getAttribute('description') ?? '';
        }
        $nextAvaibleActionTypesPageLink = $availableActionTypesJsonApiRessources->getNextPageUrl();

        while (!is_null($nextAvaibleActionTypesPageLink)) {
            $availableActionTypesJsonApiRessources = $this->getCarlObjectsByAbsoluteUrl($nextAvaibleActionTypesPageLink);

            foreach ($availableActionTypesJsonApiRessources as $availableActionTypesJsonApiRessource) {
                $availableActionTypes[$availableActionTypesJsonApiRessource->getId()] = $availableActionTypesJsonApiRessource->getAttribute('description') ?? '';
            }

            $nextAvaibleActionTypesPageLink = $availableActionTypesJsonApiRessources->getNextPageUrl();
        }

        return $availableActionTypes;
    }

    /**
     * Transforme un status générique Silab vers le status Carl correspondant.
     * Typiquement utilisé pour faire des transitions de status.
     *
     * @param array<string,array<string>> $carlToSilabStatusMapping
     */
    public static function getCarlStatusCodeFromSilabStatusCode(string $silabStatusCode, array $carlToSilabStatusMapping): string
    {
        $carlStatusCode = null;

        foreach ($carlToSilabStatusMapping as $currentCarlStatusCode => $silabStatusCodes) {
            if (in_array($silabStatusCode, $silabStatusCodes)) {
                if (!is_null($carlStatusCode)) {
                    throw new RuntimeException("Plusieurs status Carl sont possible pour le status carl $silabStatusCode, veuillez vérifier le mapping.");
                }
                $carlStatusCode = $currentCarlStatusCode;
            }
        }

        if (is_null($carlStatusCode)) {
            throw new RuntimeException("Aucun status Carl trouvé pour le status silab $silabStatusCode, veuillez vérifier le mapping.");
        }

        return $carlStatusCode;
    }

    /**
     * Transforme un status générique Silab vers le status Carl correspondant.
     * Typiquement utilisé pour récupérer des status de puis carl.
     *
     * @param array<string,array<string>> $silabToCarlStatusMapping
     */
    public static function getSilabStatusCodeFromCarlStatusCode(string $carlStatusCode, array $silabToCarlStatusMapping): string
    {
        $silabStatusCode = null;

        foreach ($silabToCarlStatusMapping as $currentSilabStatusCode => $carlStatusCodes) {
            if (in_array($carlStatusCode, $carlStatusCodes)) {
                if (!is_null($silabStatusCode)) {
                    throw new RuntimeException("Plusieurs status de Silab sont possible pour le status carl $carlStatusCode, veuillez vérifier le mapping.");
                }
                $silabStatusCode = $currentSilabStatusCode;
            }
        }

        if (is_null($silabStatusCode)) {
            throw new RuntimeException("Aucun status Silab trouvé pour le status carl $carlStatusCode, veuillez vérifier le mapping.");
        }

        return $silabStatusCode;
    }

    /**
     * Récupère les status carl correspondants à des status silab.
     * Typiquement utilisé pour du findby.
     *
     * @param array<string>               $silabStatuses
     * @param array<string,array<string>> $silabToCarlStatusMapping
     *
     * @return array<string>
     */
    public static function getCarlStatusCodesFromSilabStatusCodes(array $silabStatuses, array $silabToCarlStatusMapping): array
    {
        return array_unique(array_reduce(
            $silabStatuses,
            function ($carlStatuses, $silabStatus) use ($silabToCarlStatusMapping) {
                if (!array_key_exists($silabStatus, $silabToCarlStatusMapping)) {
                    throw new RuntimeException("Le status $silabStatus n'a pas de statut carl correspondant, cf. CarlClient::SILAB_TO_CARL_DEMANDEINTERVENTION_STATUS_MAPPING.");
                }

                return array_merge($carlStatuses, $silabToCarlStatusMapping[$silabStatus]);
            },
            []
        ));
    }

    /**
     * permet de savoir si une entité est un point de structure (box sur carl).
     */
    public function isBox(string $entity): bool
    {
        return in_array(
            $entity,
            [
                'box',
                'building',
                'buildingset',
                'zone',
                'floor',
                'space',
                'room',
            ]
        );
    }

    public function getCarlDocumentFromDocumentLinkId(string $documentLinkId): CarlDocument
    {
        $includes = ['document', 'document.attachment'];

        $fields = [
            'documentlink' => [
                'document',
            ],
            'document' => [
                'attachment',
            ],
            'url' => [
                'path',
                'fileName',
                'attachmentType',
            ],
            'storeddoc' => [
                'attachmentType',
            ],
        ];

        $sort = ['attachDt'];

        $documentLinkJsonApiResource = $this->getCarlObject(
            entity: 'documentlink',
            id: $documentLinkId,
            includes: $includes,
            fields: $fields,
            sort: $sort
        );

        switch ($documentLinkJsonApiResource->getRelationship('document')->getRelationship('attachment')->getType()) {
            case 'storeddoc':
                return new CarlStoredDocument(
                    $documentLinkJsonApiResource->getId(),
                    $documentLinkJsonApiResource->getRelationship('document')->getId()
                );
            case 'url':
                $documentPath = $documentLinkJsonApiResource->getRelationship('document')->getRelationship('attachment')->getAttribute('path');
                $documentFileName = $documentLinkJsonApiResource->getRelationship('document')->getRelationship('attachment')->getAttribute('fileName');

                return new CarlLinkedUrlDocument(
                    $documentLinkJsonApiResource->getId(),
                    $documentLinkJsonApiResource->getRelationship('document')->getId(),
                    $documentPath.$documentFileName
                );
            default:
                throw new AttachementTypeUnsupported($documentLinkJsonApiResource->getRelationship('document')->getRelationship('attachment')->getType());
        }
    }

    private function downloadStoredFileBlobFromDocumentId(string $documentId): DocumentBlob
    {
        $response = $this->callRaw(
            method: 'GET',
            absoluteUrl: $this->getApiUrl().self::CARL_API_UI_ENDPOINT.'/documents/download/'.urlencode($documentId)
        );

        $filename = Helpers::getFileNameFromResponse($response);

        return new DocumentBlob(
            blob: $response->getContent(),
            fileName: $filename
        );
    }

    public function getDocumentBlob(string $documentLinkId): DocumentBlob
    {
        $carlDocument = $this->getCarlDocumentFromDocumentLinkId(documentLinkId: $documentLinkId);

        switch (get_class($carlDocument)) {
            case CarlStoredDocument::class :
                return $this->downloadStoredFileBlobFromDocumentId($carlDocument->getDocumentId());
            case CarlLinkedUrlDocument::class :
                assert($carlDocument instanceof CarlLinkedUrlDocument);
                foreach ($this->getGedDocumentReaders() as $gedClient) {
                    try {
                        return $gedClient->getFile($carlDocument->getAttachmentUrl());
                    } catch (UnsupportedUrlException $e) {
                        continue;
                    }
                }
                $response = HttpClient::create()->request('GET', $carlDocument->getAttachmentUrl());

                // si aucun des client GED n'a réussi à lire le fichier c'est peut-être simplement un lien publique
                return new DocumentBlob(
                    blob: $response->getContent(),
                    fileName: Helpers::getFileNameFromResponse($response)
                ); // Note amelioration:  ça peut être des lien de type autre que des fichiers, ex: page web avec mire de connexion
            default:
                throw new AttachementTypeUnsupported(get_class($carlDocument));
        }
    }

    /**
     * @param array<string,string>|null $metadatas amelioration: à l'avenir, implementer des metadataserializer ça pourrait être pas mal
     *
     * @return string L'id de l'attachement
     */
    public function persistFile(string $base64, string $name, User $user, ?array $metadatas): string
    {
        if (empty($this->getGedDocumentWriter())) {
            throw new NoAvailableGedException($this);
        }

        return $this->createUrlAttachement(
            attachmentUrl: $this->getGedDocumentWriter()->persistFile(
                $base64,
                $name,
                $metadatas
            ),
            userId: $this->getUserIdFromEmail($user->getEmail())
        );
    }

    public function getCarlEvaluationId(Evaluation $silabEvaluation): string
    {
        return $this::SILAB_EVALUATION_TO_CARL_EVALUATION_ID[
            $silabEvaluation->value
        ]
        ?? throw new RuntimeException('Aucune corrrespondance carl pour '.$silabEvaluation->value);
    }

    public function getSilabEvaluation(string $carlEvaluationId): ?Evaluation
    {
        return $this::CARL_EVALUATION_ID_TO_SILAB_EVALUATION[
                        $carlEvaluationId
                    ]
            ?? throw new RuntimeException("Aucune corrrespondance Silab pour l'evalution id $carlEvaluationId");
    }

    public function getCacheKey(string $key): string
    {
        return 'CarlClient/'.$this->id.'/'.$key;
    }

    /**
     * @return array<string>
     */
    public function getObjectAndDescendantTypes(string $objectId): array
    {
        $descendantCacheKey = $this->getCacheKey('objectInfo/'.$objectId.'/objectAndDescendantTypes');
        $descendantsTypes = apcu_fetch($descendantCacheKey);

        if (false !== $descendantsTypes) {
            return $descendantsTypes;
        }

        $objectInfoJsonApiResource = $this->getCarlObject(
            entity: 'objectinfo',
            id: $objectId,
            fields: ['objectinfo' => ['typeName']]
        );

        $objectAndDescendantTypes = [
            ...$this->getObjectDescendantTypesBySuperType($objectInfoJsonApiResource->getAttribute('typeName')),
            strtolower($objectInfoJsonApiResource->getId()),
        ];
        apcu_store(
            $descendantCacheKey,
            $objectAndDescendantTypes,
            self::APCU_TTL_DATA_STABLE
        );

        return $objectAndDescendantTypes;
    }

    /**
     * @return array<string>
     */
    private function getObjectDescendantTypesBySuperType(string $objectSuperType): array
    {
        $objectInfoJsonApiResources = $this->getCarlObjects(
            entity: 'objectinfo',
            filters: ['superTypeName' => [$objectSuperType]],
            fields: ['objectinfo' => ['typeName']]
        );

        if (0 === count($objectInfoJsonApiResources)) {
            return [];
        }

        return array_reduce(
            $objectInfoJsonApiResources->getArrayCopy(),
            function ($objectAndDescendantsIds, $objectInfoJsonApiResource) {
                try {
                    $this->getCarlObjects(
                        entity: strtolower($objectInfoJsonApiResource->getId()),
                        filters: ['id' => [uniqid()]]
                    );
                    $currentIdInAnArray = [strtolower($objectInfoJsonApiResource->getId())];
                } catch (\Exception $e) {
                    $currentIdInAnArray = [];
                }

                return [
                    ...$objectAndDescendantsIds,
                    ...$currentIdInAnArray,
                    ...$this->getObjectDescendantTypesBySuperType($objectInfoJsonApiResource->getAttribute('typeName')),
                ];
            },
            []
        );
    }

    /**
     * @return array<string>
     */
    public function getAllowedEqptTypes(): array
    {
        return [...$this->getObjectAndDescendantTypes('BOX'), ...$this->getObjectAndDescendantTypes('MATERIAL')];
    }
}
