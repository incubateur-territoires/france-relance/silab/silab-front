<?php

namespace App\Shared\Carl\Entity;

use ApiPlatform\Metadata\GetCollection;
use App\Shared\Carl\State\GetCollectionCostCentersProvider;
use App\Shared\Carl\ValueObject\ConfigurationCarlObject;

#[GetCollection(
    uriTemplate: '/carl-clients/{carlClientId}/cost-centers',
    output: ConfigurationCarlObject::class,
    security: "is_granted('ROLE_CONSULTER_CARL_COSTCENTER')",
    provider: GetCollectionCostCentersProvider::class,
)]
class CostCenter
{
    public static function getEntity(): string
    {
        return 'costcenter';
    }
}
