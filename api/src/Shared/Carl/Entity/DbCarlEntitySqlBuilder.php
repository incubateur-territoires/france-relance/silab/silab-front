<?php

namespace App\Shared\Carl\Entity;

interface DbCarlEntitySqlBuilder
{
    public static function getTableName(int $carlClientId): string;

    public static function buildTableSql(CarlClient $carlClient, string $dbSchema, string $serverName): string;
}
