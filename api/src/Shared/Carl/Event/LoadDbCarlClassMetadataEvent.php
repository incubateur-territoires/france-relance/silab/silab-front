<?php

namespace App\Shared\Carl\Event;

use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Contracts\EventDispatcher\Event;

class LoadDbCarlClassMetadataEvent extends Event
{
    private ClassMetadata $classMetadata;

    public function __construct(ClassMetadata $classMetadata)
    {
        $this->classMetadata = $classMetadata;
    }

    public function getClassMetadata(): ClassMetadata
    {
        return $this->classMetadata;
    }
}
