<?php

namespace App\Shared\Carl\EventListener;

use App\InterventionServiceOffer\Carl\Exception\NoCarlClientContextException;
use App\InterventionServiceOffer\Carl\Service\CarlClientContext;
use App\Shared\Carl\Command\BuildForeignTablesCommand;
use App\Shared\Carl\Entity\DbCarlEntitySqlBuilder;
use App\Shared\Carl\Event\LoadDbCarlClassMetadataEvent;
use App\Shared\Exception\RuntimeException;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener(event: LoadDbCarlClassMetadataEvent::class, method: 'configureDbCarlForeignTableNames')]
class ForeignTableNamesMetadataConfigurator
{
    public function __construct(private CarlClientContext $carlClientContext)
    {
    }

    /**
     * Important : votre classe doit être également correctement référencée dans `src/Shared/Carl/EventListener/LoadDbCarlClassMetadataEventDispatcher.php`
     * sous peine de ne pas être prise en compte.
     */
    public function configureDbCarlForeignTableNames(LoadDbCarlClassMetadataEvent $event): void
    {
        try {
            $carlClientId = $this->carlClientContext->getCarlClient()->getId();

            $classMetadata = $event->getClassMetadata();

            if (!in_array($classMetadata->getName(), BuildForeignTablesCommand::DB_CARL_ENTITIES_CLASSES)) {
                throw new RuntimeException('La classe '.$classMetadata->getName()." n'est référencé dans BuildForeignTablesCommand");
            }

            if (!is_subclass_of($classMetadata->getName(), DbCarlEntitySqlBuilder::class)) {
                throw new \LogicException($classMetadata->getName().' n\'implémenta pas DbCarlEntitySqlBuilder');
            }

            $classMetadata->setPrimaryTable(['name' => $classMetadata->getName()::getTableName($carlClientId)]);
        } catch (NoCarlClientContextException $e) {
            // ne rien faire, cette méthode n'est utile que dans un contexte d'une requête vers une offre de service intervention
            // Si on ne fait pas ça ça pose notamment problème lors des cache:clear de prod
        }
    }
}
