<?php

namespace App\Shared\Carl\EventListener;

use App\Shared\Carl\Command\BuildForeignTablesCommand;
use App\Shared\Carl\Entity\CarlClient;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\Migrations\Events as MigrationsEvents;
use Doctrine\ORM\Events as ORMEvents;
use Psr\Log\LoggerInterface;

#[AsEntityListener(event: ORMEvents::postRemove, method: 'buildForeignTables', entity: CarlClient::class)]
#[AsEntityListener(event: ORMEvents::postPersist, method: 'buildForeignTables', entity: CarlClient::class)]
#[AsEntityListener(event: ORMEvents::postUpdate, method: 'buildForeignTables', entity: CarlClient::class)]
#[AsDoctrineListener(event: MigrationsEvents::onMigrationsMigrated)]
class ForeignTablesBuilder
{
    public function __construct(
        private BuildForeignTablesCommand $buildForeignTablesCommand,
        private LoggerInterface $logger
    ) {
    }

    public function onMigrationsMigrated(): void
    {
        $this->buildForeignTables();
    }

    public function buildForeignTables(): void
    {
        $this->buildForeignTablesCommand->buildForeignTables(
            function ($message) {
                $this->logger->info($message);
            }
        );
    }
}
