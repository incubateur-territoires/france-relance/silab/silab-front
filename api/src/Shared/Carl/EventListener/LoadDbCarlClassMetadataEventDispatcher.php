<?php

namespace App\Shared\Carl\EventListener;

use App\Shared\Carl\Command\BuildForeignTablesCommand;
use App\Shared\Carl\Event\LoadDbCarlClassMetadataEvent;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events as ORMEvents;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

#[AsDoctrineListener(event: ORMEvents::loadClassMetadata)]
class LoadDbCarlClassMetadataEventDispatcher
{
    public function __construct(private EventDispatcherInterface $eventDispatcher)
    {
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $loadClassMetadataEventArgs): void
    {
        $classMetadata = $loadClassMetadataEventArgs->getClassMetadata();

        if ($classMetadata instanceof ClassMetadata) {
            $this->dispatchEventOnDbCarlClassMetadata($classMetadata);
        }
    }

    public function dispatchEventOnDbCarlClassMetadata(ClassMetadata $classMetadata): void
    {
        if (
            in_array(
                $classMetadata->getName(),
                BuildForeignTablesCommand::DB_CARL_ENTITIES_CLASSES
            )
        ) {
            $this->eventDispatcher->dispatch(new LoadDbCarlClassMetadataEvent($classMetadata));
        }
    }
}
