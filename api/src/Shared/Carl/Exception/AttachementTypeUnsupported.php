<?php

namespace App\Shared\Carl\Exception;

use App\Shared\Exception\RuntimeException;

class AttachementTypeUnsupported extends RuntimeException
{
    public function __construct(string $attachementType, ?\Throwable $previous = null)
    {
        parent::__construct(message: "Ce type de pièces jointes n'est pas géré par le connecteur carl : $attachementType.", previous: $previous);
    }
}
