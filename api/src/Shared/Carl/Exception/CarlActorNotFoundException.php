<?php

namespace App\Shared\Carl\Exception;

use App\Shared\Exception\BadRequestException;

class CarlActorNotFoundException extends BadRequestException
{
    public function __construct(string $actorId, ?\Throwable $previous = null)
    {
        parent::__construct(message: "Aucun acteur trouvé avec l'id : ".$actorId, previous: $previous);
    }
}
