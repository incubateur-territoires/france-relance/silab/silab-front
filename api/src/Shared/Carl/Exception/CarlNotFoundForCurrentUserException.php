<?php

namespace App\Shared\Carl\Exception;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Exception\RuntimeException;
use App\Shared\User\Entity\User;

class CarlNotFoundForCurrentUserException extends RuntimeException
{
    public function __construct(User $currentUser, CarlClient $carlClient, ?\Throwable $previous = null)
    {
        parent::__construct(message: "Impossible de retrouver l'utilisateur actuel ".$currentUser->getEmail().' dans carl (client carl '.$carlClient->getTitle().'.', previous: $previous);
    }
}
