<?php

namespace App\Shared\Carl\Exception;

interface EmailMatchingFailureExceptionInterface extends \Throwable
{
    public function __construct(string $userEmail, ?\Throwable $previous = null);
}
