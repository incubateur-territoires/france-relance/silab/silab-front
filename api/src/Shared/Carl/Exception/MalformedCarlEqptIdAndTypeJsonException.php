<?php

namespace App\Shared\Carl\Exception;

use App\Shared\Exception\RuntimeException;

class MalformedCarlEqptIdAndTypeJsonException extends RuntimeException
{
}
