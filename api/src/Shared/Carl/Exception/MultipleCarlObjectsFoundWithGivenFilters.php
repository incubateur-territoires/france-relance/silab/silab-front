<?php

namespace App\Shared\Carl\Exception;

use App\Shared\Exception\RuntimeException;

class MultipleCarlObjectsFoundWithGivenFilters extends RuntimeException
{
}
