<?php

namespace App\Shared\Carl\Exception;

use App\Shared\Exception\RuntimeException;

class MultipleEmailsFoundForUser extends RuntimeException
{
    public function __construct(string $userId, ?\Throwable $previous = null)
    {
        parent::__construct(message: "Plusieurs emails trouvés pour l'acteur carl avec l'id $userId.", previous: $previous);
    }
}
