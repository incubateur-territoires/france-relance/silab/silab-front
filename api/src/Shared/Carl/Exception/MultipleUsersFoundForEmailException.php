<?php

namespace App\Shared\Carl\Exception;

use App\Shared\Exception\RuntimeException;

class MultipleUsersFoundForEmailException extends RuntimeException implements EmailMatchingFailureExceptionInterface
{
    public function __construct(string $userEmail, ?\Throwable $previous = null)
    {
        parent::__construct(message: "Plusieurs utilisateurs carl trouvés pour l'email $userEmail.", previous: $previous);
    }
}
