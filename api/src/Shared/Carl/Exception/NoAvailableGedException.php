<?php

namespace App\Shared\Carl\Exception;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Exception\BadRequestException;

class NoAvailableGedException extends BadRequestException
{
    public function __construct(CarlClient $carlClient, ?\Throwable $previous = null)
    {
        parent::__construct(message: "Aucune GED configurée sur le client carl $carlClient", previous: $previous);
    }
}
