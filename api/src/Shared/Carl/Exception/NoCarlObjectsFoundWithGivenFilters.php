<?php

namespace App\Shared\Carl\Exception;

use App\Shared\Exception\RuntimeException;

class NoCarlObjectsFoundWithGivenFilters extends RuntimeException
{
}
