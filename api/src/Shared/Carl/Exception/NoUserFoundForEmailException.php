<?php

namespace App\Shared\Carl\Exception;

use App\Shared\Exception\RuntimeException;

class NoUserFoundForEmailException extends RuntimeException implements EmailMatchingFailureExceptionInterface
{
    public function __construct(string $userEmail, ?\Throwable $previous = null)
    {
        parent::__construct(message: "Aucun utilisateur carl trouvé pour l'email $userEmail.", previous: $previous);
    }
}
