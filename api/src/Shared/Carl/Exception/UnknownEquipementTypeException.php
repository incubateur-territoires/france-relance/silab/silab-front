<?php

namespace App\Shared\Carl\Exception;

use App\Shared\Exception\RuntimeException;

class UnknownEquipementTypeException extends RuntimeException
{
    public function __construct(string $equipmentType, ?\Throwable $previous = null)
    {
        parent::__construct(message: "Le typ d'équipement $equipmentType est inconnu.", previous: $previous);
    }
}
