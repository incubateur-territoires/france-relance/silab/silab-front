<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\Shared\Carl\Exception;

class WarehouseIdIsMissingException extends \RuntimeException
{
}
