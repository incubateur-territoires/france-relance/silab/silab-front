<?php

namespace App\Shared\Carl\Repository;

use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\ValueObject\ConfigurationCarlObject;

class ConfigurationCarlObjectRepository
{
    public function __construct(private CarlClient $carlClient, private string $entity)
    {
    }

    /**
     * @param array<string> $sort
     *
     * @return array<ConfigurationCarlObject>
     */
    public function findAll(?array $sort = []): array
    {
        $objectsJsonApiRessources = $this->carlClient->getCarlObjects(
            entity: $this->entity,
            fields: [$this->entity => ['description', 'code']],
            sort: $sort
        );

        $objects = [];
        while (true) {
            foreach ($objectsJsonApiRessources as $objectJsonApiRessource) {
                $objects[] = new ConfigurationCarlObject(
                    id: $objectJsonApiRessource->getId(),
                    type : $objectJsonApiRessource->getType(),
                    code: $objectJsonApiRessource->getAttribute('code'),
                    label: $objectJsonApiRessource->getAttribute('description')
                );
            }
            $nextPageLink = $objectsJsonApiRessources->getNextPageUrl();
            if (is_null($nextPageLink)) {
                return $objects;
            }
            $objectsJsonApiRessources = $this->carlClient->getCarlObjectsByAbsoluteUrl($nextPageLink);
        }
    }
}
