<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlActiviteAction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlActiviteAction>
 *
 * @method DbCarlActiviteAction|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlActiviteAction|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlActiviteAction[]    findAll()
 * @method DbCarlActiviteAction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlActiviteActionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlActiviteAction::class);
    }
}
