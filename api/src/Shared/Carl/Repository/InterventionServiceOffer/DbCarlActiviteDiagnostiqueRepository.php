<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlActiviteDiagnostique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlActiviteDiagnostique>
 *
 * @method DbCarlActiviteDiagnostique|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlActiviteDiagnostique|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlActiviteDiagnostique[]    findAll()
 * @method DbCarlActiviteDiagnostique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlActiviteDiagnostiqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlActiviteDiagnostique::class);
    }
}
