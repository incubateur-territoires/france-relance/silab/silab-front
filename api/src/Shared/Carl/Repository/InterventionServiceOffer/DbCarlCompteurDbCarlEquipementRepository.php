<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlCompteurDbCarlEquipement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlCompteurDbCarlEquipement>
 *
 * @method DbCarlCompteurDbCarlEquipement|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlCompteurDbCarlEquipement|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlCompteurDbCarlEquipement[]    findAll()
 * @method DbCarlCompteurDbCarlEquipement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlCompteurDbCarlEquipementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlCompteurDbCarlEquipement::class);
    }
}
