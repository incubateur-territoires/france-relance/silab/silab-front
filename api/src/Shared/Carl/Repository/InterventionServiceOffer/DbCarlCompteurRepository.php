<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlCompteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlCompteur>
 *
 * @method DbCarlCompteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlCompteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlCompteur[]    findAll()
 * @method DbCarlCompteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlCompteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlCompteur::class);
    }
}
