<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlCustomer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlCustomer>
 *
 * @method DbCarlCustomer|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlCustomer|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlCustomer[]    findAll()
 * @method DbCarlCustomer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlCustomerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlCustomer::class);
    }
}
