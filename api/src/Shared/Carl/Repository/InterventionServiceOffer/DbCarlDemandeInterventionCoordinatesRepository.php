<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlDemandeInterventionCoordinates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlDemandeInterventionCoordinates>
 *
 * @method DbCarlDemandeInterventionCoordinates|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlDemandeInterventionCoordinates|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlDemandeInterventionCoordinates[]    findAll()
 * @method DbCarlDemandeInterventionCoordinates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlDemandeInterventionCoordinatesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlDemandeInterventionCoordinates::class);
    }
}
