<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlDemandeInterventionDbCarlEquipement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlDemandeInterventionDbCarlEquipement>
 *
 * @method DbCarlDemandeInterventionDbCarlEquipement|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlDemandeInterventionDbCarlEquipement|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlDemandeInterventionDbCarlEquipement[]    findAll()
 * @method DbCarlDemandeInterventionDbCarlEquipement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlDemandeInterventionDbCarlEquipementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlDemandeInterventionDbCarlEquipement::class);
    }
}
