<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlDemandeIntervention;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlDemandeIntervention>
 *
 * @method DbCarlDemandeIntervention|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlDemandeIntervention|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlDemandeIntervention[]    findAll()
 * @method DbCarlDemandeIntervention[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlDemandeInterventionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlDemandeIntervention::class);
    }
}
