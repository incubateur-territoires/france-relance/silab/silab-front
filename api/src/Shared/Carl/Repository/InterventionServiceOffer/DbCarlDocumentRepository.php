<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlDocument;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlDocument>
 *
 * @method DbCarlDocument|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlDocument|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlDocument[]    findAll()
 * @method DbCarlDocument[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlDocumentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlDocument::class);
    }
}
