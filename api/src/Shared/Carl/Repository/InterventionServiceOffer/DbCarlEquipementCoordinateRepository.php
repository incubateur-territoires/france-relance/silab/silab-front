<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlEquipementCoordinate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlEquipementCoordinate>
 *
 * @method DbCarlEquipementCoordinate|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlEquipementCoordinate|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlEquipementCoordinate[]    findAll()
 * @method DbCarlEquipementCoordinate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlEquipementCoordinateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlEquipementCoordinate::class);
    }
}
