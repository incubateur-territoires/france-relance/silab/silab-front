<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlEquipementDbCarlEquipement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlEquipementDbCarlEquipement>
 *
 * @method DbCarlEquipementDbCarlEquipement|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlEquipementDbCarlEquipement|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlEquipementDbCarlEquipement[]    findAll()
 * @method DbCarlEquipementDbCarlEquipement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlEquipementDbCarlEquipementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlEquipementDbCarlEquipement::class);
    }
}
