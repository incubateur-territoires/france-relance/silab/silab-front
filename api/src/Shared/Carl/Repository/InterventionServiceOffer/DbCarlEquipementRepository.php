<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlEquipement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlEquipement>
 *
 * @method DbCarlEquipement|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlEquipement|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlEquipement[]    findAll()
 * @method DbCarlEquipement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlEquipementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlEquipement::class);
    }
}
