<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlEquipementType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlEquipementType>
 *
 * @method DbCarlEquipementType|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlEquipementType|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlEquipementType[]    findAll()
 * @method DbCarlEquipementType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlEquipementTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlEquipementType::class);
    }
}
