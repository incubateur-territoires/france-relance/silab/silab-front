<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlGmaoActeur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlGmaoActeur>
 *
 * @method DbCarlGmaoActeur|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlGmaoActeur|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlGmaoActeur[]    findAll()
 * @method DbCarlGmaoActeur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlGmaoActeurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlGmaoActeur::class);
    }
}
