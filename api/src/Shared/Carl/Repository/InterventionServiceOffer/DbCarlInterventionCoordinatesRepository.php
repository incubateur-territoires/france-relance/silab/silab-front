<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlInterventionCoordinates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlInterventionCoordinates>
 *
 * @method DbCarlInterventionCoordinates|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlInterventionCoordinates|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlInterventionCoordinates[]    findAll()
 * @method DbCarlInterventionCoordinates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlInterventionCoordinatesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlInterventionCoordinates::class);
    }
}
