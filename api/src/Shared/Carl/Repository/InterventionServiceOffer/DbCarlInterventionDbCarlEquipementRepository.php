<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlInterventionDbCarlEquipement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlInterventionDbCarlEquipement>
 *
 * @method DbCarlInterventionDbCarlEquipement|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlInterventionDbCarlEquipement|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlInterventionDbCarlEquipement[]    findAll()
 * @method DbCarlInterventionDbCarlEquipement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlInterventionDbCarlEquipementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlInterventionDbCarlEquipement::class);
    }
}
