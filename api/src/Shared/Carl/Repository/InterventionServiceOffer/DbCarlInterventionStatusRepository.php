<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlInterventionStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlInterventionStatus>
 *
 * @method DbCarlInterventionStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlInterventionStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlInterventionStatus[]    findAll()
 * @method DbCarlInterventionStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlInterventionStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlInterventionStatus::class);
    }
}
