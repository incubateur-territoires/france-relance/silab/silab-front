<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlMesure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlMesure>
 *
 * @method DbCarlMesure|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlMesure|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlMesure[]    findAll()
 * @method DbCarlMesure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlMesureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlMesure::class);
    }
}
