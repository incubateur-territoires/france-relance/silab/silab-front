<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlOperationDbCarlGmaoActeur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlOperationDbCarlGmaoActeur>
 *
 * @method DbCarlOperationDbCarlGmaoActeur|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlOperationDbCarlGmaoActeur|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlOperationDbCarlGmaoActeur[]    findAll()
 * @method DbCarlOperationDbCarlGmaoActeur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlOperationDbCarlGmaoActeurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlOperationDbCarlGmaoActeur::class);
    }
}
