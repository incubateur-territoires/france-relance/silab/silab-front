<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlOperation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlOperation>
 *
 * @method DbCarlOperation|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlOperation|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlOperation[]    findAll()
 * @method DbCarlOperation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlOperationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlOperation::class);
    }
}
