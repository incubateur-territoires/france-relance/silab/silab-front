<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlReleveDeCompteur;
use App\Shared\Exception\RuntimeException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\ParameterType;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlReleveDeCompteur>
 *
 * @method DbCarlReleveDeCompteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlReleveDeCompteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlReleveDeCompteur[]    findAll()
 * @method DbCarlReleveDeCompteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlReleveDeCompteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlReleveDeCompteur::class);
    }

    /**
     * retourne la dernière mesure de chaque compteur sur des intervalles d'une période donnée.
     *
     * @param array<string> $idCompteurs
     *
     * @return array<array<float>> Le tableau est trié par intervalle ascendant
     */
    public function getPeriode(
        array $idCompteurs,
        \DateTimeInterface $dateDeDebut,
        int $nombreDeColonnes,
        int $intervallesDeTempsEntreLesColonnesEnSecondes
    ): array {
        $tableName = $this->getEntityManager()->getClassMetadata(DbCarlReleveDeCompteur::class)->getTableName();

        // Note: on rentre ces deux prochaine paramètres directement en SQL car visiblement
        // l'ORM n'arrive pas à les binder la ou on en a besoin 🤷‍♂️
        $dateDeDebutIsoString = $dateDeDebut->format(\DateTimeInterface::ATOM);

        /* @phpstan-ignore-next-line On fait cette vérification pour éviter les failles d'injections SQL */
        if ('integer' !== gettype($intervallesDeTempsEntreLesColonnesEnSecondes)) {
            throw new RuntimeException('Type de donnée interdite pour les intervalles de temps de la periode reçu :"'.gettype($intervallesDeTempsEntreLesColonnesEnSecondes).'", attendu: "integer"');
        }
        $dureeIntervalleENSecondesString = "$intervallesDeTempsEntreLesColonnesEnSecondes seconds";

        $sql = "
        WITH COMPTEUR AS (
            SELECT DISTINCT ID_COMPTEUR AS ID
            FROM carl.$tableName
        ), INTERVALLES AS (
            SELECT
                generate_series(1, :nombre_intervalles) AS INDEX_INTERVALLE,
                TIMESTAMPTZ '$dateDeDebutIsoString'  + (generate_series(1, :nombre_intervalles) - 1) * INTERVAL '$dureeIntervalleENSecondesString' AS DEBUT_INTERVALLE,
                TIMESTAMPTZ '$dateDeDebutIsoString' + generate_series(1, :nombre_intervalles) * INTERVAL '$dureeIntervalleENSecondesString' AS FIN_INTERVALLE
        )
        SELECT
            INDEX_INTERVALLE,
            DEBUT_INTERVALLE,
            FIN_INTERVALLE,
            ID_COMPTEUR,
            MESURE,
            DATE_MESURE
        FROM (
            SELECT
                COMPTEUR.ID AS ID_COMPTEUR,
                RELEVE_DE_COMPTEUR.MESURE,
                RELEVE_DE_COMPTEUR.DATE_MESURE,
                INTERVALLES.INDEX_INTERVALLE,
                INTERVALLES.DEBUT_INTERVALLE,
                INTERVALLES.FIN_INTERVALLE,
                ROW_NUMBER() OVER (
                    PARTITION BY COMPTEUR.ID, INTERVALLES.INDEX_INTERVALLE
                    ORDER BY RELEVE_DE_COMPTEUR.DATE_MESURE DESC
                ) AS RN
            FROM INTERVALLES
            LEFT JOIN COMPTEUR ON TRUE
            LEFT JOIN carl.$tableName RELEVE_DE_COMPTEUR
                ON RELEVE_DE_COMPTEUR.DATE_MESURE >= INTERVALLES.DEBUT_INTERVALLE
                AND RELEVE_DE_COMPTEUR.DATE_MESURE < INTERVALLES.FIN_INTERVALLE
                AND RELEVE_DE_COMPTEUR.ID_COMPTEUR = COMPTEUR.ID
            WHERE COMPTEUR.ID IN (:idCompteurs)
        ) subquery
        WHERE RN = 1
        ORDER BY INDEX_INTERVALLE;
        ";

        $mesuresIntervallisées = $this->getEntityManager()->getConnection()->executeQuery(
            $sql,
            [
                'idCompteurs' => $idCompteurs,
                'nombre_intervalles' => $nombreDeColonnes,
            ],
            [
                'idCompteurs' => ArrayParameterType::STRING,
                'nombre_intervalles' => ParameterType::INTEGER,
            ]
        )->fetchAllAssociative();

        // on formate les données selon une structure plus imple à utiliser :
        // index_intervalle->id_compteur->mesure
        // ATTENTION, cette boucle par du principe que les clefs de $mesuresIntervallisées
        //  sont bien triées par ordre ascendant d'intervalle
        $mesuresIndexéesParIntervalleEtCompteur = [];
        foreach ($mesuresIntervallisées as $mesureSurUnIntervale) {
            $debutDeLIntervalle = $mesureSurUnIntervale['debut_intervalle'];
            $idCompteur = $mesureSurUnIntervale['id_compteur'];
            $mesure = $mesureSurUnIntervale['mesure'] ? (float) $mesureSurUnIntervale['mesure'] : null;

            $mesuresIndexéesParIntervalleEtCompteur[$debutDeLIntervalle][$idCompteur] = $mesure;
        }

        return $mesuresIndexéesParIntervalleEtCompteur;
    }
}
