<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlRessource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlRessource>
 *
 * @method DbCarlRessource|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbCarlRessource|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbCarlRessource[]    findAll()
 * @method DbCarlRessource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbCarlRessourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlRessource::class);
    }
}
