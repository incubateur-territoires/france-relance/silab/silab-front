<?php

namespace App\Shared\Carl\Repository\InterventionServiceOffer;

use App\InterventionServiceOffer\Carl\Entity\DbCarlSearchEntity;
use App\InterventionServiceOffer\SearchEntity\Entity\SearchEntity\SearchEntityType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DbCarlSearchEntity>
 */
class DbCarlSearchEntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DbCarlSearchEntity::class);
    }

    /**
     * @return array<DbCarlSearchEntity>
     */
    public function search(string $searchQuery): array
    {
        $tableName = $this->getEntityManager()->getClassMetadata(DbCarlSearchEntity::class)->getTableName();

        $sql = "
            SELECT
                type,
                id,
                code,
                label,
                (
                    SELECT
                        MIN(distance)
                    FROM
                        (
                            VALUES
                                (levenshtein(LOWER(code), LOWER(:searchQuery)),5,1,10),
                                (levenshtein(LOWER(label), LOWER(:searchQuery)),5,1,10)
                        ) AS distances(distance)
                ) AS distance
            FROM carl.$tableName
            WHERE
                LOWER(code) like lower(:searchQueryPlusPermissive)
                or
                LOWER(label) like lower(:searchQueryPlusPermissive)
            ORDER BY distance
            LIMIT 24;
        ";

        $searchQueryPlusPermissive = '%'.str_replace([' ', '*'], '%', $searchQuery).'%';

        $preparedQuery = $this->getEntityManager()->getConnection()->prepare($sql);
        $preparedQuery->bindValue('searchQuery', $searchQuery);
        $preparedQuery->bindValue('searchQueryPlusPermissive', $searchQueryPlusPermissive);
        $result = $preparedQuery->executeQuery()->fetchAllAssociative();

        // Note: on ferait pas mieux d'utiliser https://www.doctrine-project.org/projects/doctrine-orm/en/3.3/reference/native-sql.html#the-nativequery-class ?
        return array_map(function ($row) {
            return new DbCarlSearchEntity(SearchEntityType::from($row['type']), $row['id'], $row['code'], $row['label']);
        }, $result);
    }
}
