<?php

namespace App\Shared\Carl\Service;

use App\InterventionServiceOffer\Carl\Entity\DbCarlGmaoActeur;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\InterventionServiceOffer\Service\InterventionServiceOfferContext;
use App\Shared\Carl\Exception\CarlNotFoundForCurrentUserException;
use App\Shared\Carl\Repository\InterventionServiceOffer\DbCarlGmaoActeurRepository;
use App\Shared\Gmao\Entity\GmaoActeur;
use App\Shared\User\Service\CurrentUserContextInterface;

class CurrentCarlUserContext // Amelioration: mettre `implements CurrentGmaoActeurContextInterface` quand on saura un peu mieu faire l'autowiring
{
    private ?DbCarlGmaoActeur $currentDbCarlGmaoActeur = null;

    public function __construct(
        private CurrentUserContextInterface $currentUserContextInterface,
        private InterventionServiceOfferContext $interventionServiceOfferContext,
        private DbCarlGmaoActeurRepository $dbCarlGmaoActeurRepository
    ) {
    }

    public function getCurrentDbCarlGmaoActeur(): DbCarlGmaoActeur
    {
        if (null === $this->currentDbCarlGmaoActeur) {
            $gmaoConfiguration = $this->interventionServiceOfferContext->getServiceOffer()->getGmaoConfiguration();
            assert($gmaoConfiguration instanceof CarlConfigurationIntervention);
            $carlClient = $gmaoConfiguration->getCarlClient();

            try {
                $this->currentDbCarlGmaoActeur = $this->dbCarlGmaoActeurRepository->find(
                    $carlClient->getUserIdFromEmail(
                        $this->currentUserContextInterface->getCurrentUser()->getEmail()
                    )
                );
            } catch (\Throwable $e) {
                throw new CarlNotFoundForCurrentUserException($this->currentUserContextInterface->getCurrentUser(), $carlClient, $e);
            }

            if (is_null($this->currentDbCarlGmaoActeur)) {
                throw new CarlNotFoundForCurrentUserException($this->currentUserContextInterface->getCurrentUser(), $carlClient);
            }
        }

        return $this->currentDbCarlGmaoActeur;
    }

    /**
     * @param array<string> $include
     */
    public function getCurrentGmaoActeur(array $include = []): GmaoActeur
    {
        return $this->getCurrentDbCarlGmaoActeur()->toGmaoActeur($include);
    }
}
