<?php

namespace App\Shared\Carl\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Shared\Carl\Entity\CostCenter;
use App\Shared\Carl\Repository\CarlClientRepository;
use App\Shared\Carl\Repository\ConfigurationCarlObjectRepository;
use App\Shared\Carl\ValueObject\ConfigurationCarlObject;

final class GetCollectionCostCentersProvider implements ProviderInterface
{
    public function __construct(
        private CarlClientRepository $carlClientRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<ConfigurationCarlObject>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $carlClient = $this->carlClientRepository->find($uriVariables['carlClientId']);
        $costCenterRepository = new ConfigurationCarlObjectRepository(
            carlClient: $carlClient,
            entity: CostCenter::getEntity()
        );

        return $costCenterRepository->findAll(sort: ['description']);
    }
}
