<?php

namespace App\Shared\Carl\Trait;

trait UnexpectedCriteriaCheckingTrait
{
    /**
     * Cette fonction à autoriser uniquement un ensemble de critères attendus
     * renvoit une erreur quand le tableau $actualCriteria contient des clés qui ne font pas partie des valeurs $expectedCriteriaKeys.
     *
     * @param array<string> $actualCriteria
     * @param array<string> $expectedCriteriaKeys
     *
     * @throws \UnexpectedValueException si Le tableau de critère fourni possède une clef non autorisé dans les $expectedCriteriaKeys
     */
    protected function throwIfUnexpectedCriteria(array $actualCriteria, array $expectedCriteriaKeys): void
    {
        $unexpectedCriterias = array_diff(array_keys($actualCriteria), $expectedCriteriaKeys);

        if (!empty($unexpectedCriterias)) {
            throw new \UnexpectedValueException('Le(s) critère(s) '.implode(', ', $unexpectedCriterias).' ne sont pas supportés. Voici la liste des critères acceptés : '.implode(', ', $expectedCriteriaKeys).'.');
        }
    }

    /**
     * Cette fonction à autoriser uniquement un ensemble de valeurs dans un tableua, utile par exemple pour vérifier des paramètre 'include'.
     *
     * @param array<string> $actualValues
     * @param array<string> $expectedValues
     *
     * @throws \UnexpectedValueException si Le tableau de actualValues fourni possède une valeur non autorisée dans les $expectedValues
     */
    protected function throwIfUnexpectedValue(array $actualValues, array $expectedValues): void
    {
        $unexpectedCriterias = array_diff($actualValues, $expectedValues);

        if (!empty($unexpectedCriterias)) {
            throw new \UnexpectedValueException('Le(s) valeurs(s) '.implode(', ', $unexpectedCriterias).' ne sont pas supportés. Voici la liste des valeurs acceptées : '.implode(', ', $expectedValues).'.');
        }
    }
}
