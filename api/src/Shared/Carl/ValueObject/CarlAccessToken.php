<?php

namespace App\Shared\Carl\ValueObject;

class CarlAccessToken
{
    private const EXPIRY_TOLERANCE_SECONDS = 60;

    public static function fromNow(string $xcsAccessToken, int $validityTimeInSeconds): self
    {
        return new CarlAccessToken(
            xcsAccessToken: $xcsAccessToken,
            expiryTimestampSeconds: time() + $validityTimeInSeconds// la date d'expiration carl est en millisecondes
        );
    }

    public function __construct(private string $xcsAccessToken, private int $expiryTimestampSeconds)
    {
    }

    public function getXcsAccessToken(): string
    {
        return $this->xcsAccessToken;
    }

    public function isNotExpired(): bool
    {
        return ($this->expiryTimestampSeconds - self::EXPIRY_TOLERANCE_SECONDS) > time();
    }

    /**
     * @return int le nombre de secondes avant expiration du token
     */
    public function getTimeToLive(): int
    {
        return $this->expiryTimestampSeconds - time();
    }
}
