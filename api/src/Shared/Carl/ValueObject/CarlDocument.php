<?php

namespace App\Shared\Carl\ValueObject;

class CarlDocument
{
    public function __construct(
        private string $documentLinkId,
        private string $documentId,
    ) {
    }

    public function getDocumentlinkId(): string
    {
        return $this->documentLinkId;
    }

    public function getDocumentId(): string
    {
        return $this->documentId;
    }
}
