<?php

namespace App\Shared\Carl\ValueObject;

class CarlLinkedUrlDocument extends CarlDocument
{
    public function __construct(
        string $documentLinkId,
        string $documentId,
        private string $documentUrl
    ) {
        parent::__construct(
            documentLinkId: $documentLinkId, documentId: $documentId);
    }

    // liens de l'image
    public function getAttachmentUrl(): string
    {
        return $this->documentUrl;
    }
}
