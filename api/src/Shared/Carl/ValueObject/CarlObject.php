<?php

namespace App\Shared\Carl\ValueObject;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\Shared\Carl\Exception\MalformedCarlEqptIdAndTypeJsonException;
use Symfony\Component\Serializer\Annotation\Groups;

class CarlObject
{
    public function __construct(
        #[Groups([
            CarlConfigurationIntervention::GROUP_AFFICHER_CARL_CONFIGURATION_INTERVENTION,
            CarlConfigurationIntervention::GROUP_CREER_CARL_CONFIGURATION_INTERVENTION,
        ])]
        private string $id,
        #[Groups([
            CarlConfigurationIntervention::GROUP_AFFICHER_CARL_CONFIGURATION_INTERVENTION,
            CarlConfigurationIntervention::GROUP_CREER_CARL_CONFIGURATION_INTERVENTION,
        ])]
        private string $type
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getSilabId(): string
    {
        return json_encode(['id' => $this->getId(), 'type' => $this->getType()]);
    }

    public static function getSilabIdFromIdAndType(
        string $id,
        string $type): string
    {
        $tmpCarlObject = new CarlObject($id, $type);

        return $tmpCarlObject->getSilabId();
    }

    public static function fromJson(string $carlObjectIdAndTypeJson): self
    {
        $carlObjectIdAndTypeArray = json_decode($carlObjectIdAndTypeJson, true);

        if (
            is_null($carlObjectIdAndTypeArray)
        ) {
            throw new MalformedCarlEqptIdAndTypeJsonException('Le json envoyé n\'est pas décodable. Attendu : {"id":"id de l\'objet", "type":"type de l\'objet, exemple: buildingset"}. Chaîne reçue : '.$carlObjectIdAndTypeJson);
        }

        return self::fromArray($carlObjectIdAndTypeArray);
    }

    /**
     * @param array<string,string> $carlObjectIdAndTypeArray
     */
    public static function fromArray(array $carlObjectIdAndTypeArray): self
    {
        if (
            !array_key_exists('id', $carlObjectIdAndTypeArray)
            || !array_key_exists('type', $carlObjectIdAndTypeArray)
        ) {
            throw new MalformedCarlEqptIdAndTypeJsonException('Le tableau envoyé n\'est pas convertible en instance de CarlObject.');
        }

        return new CarlEqptIdAndType($carlObjectIdAndTypeArray['id'], $carlObjectIdAndTypeArray['type']);
    }

    /**
     * @return array<string,array<string,string>>
     */
    public function toApiRelationship(): array
    {
        return [
            'data' => [
                'id' => $this->getId(),
                'type' => $this->getType(),
            ],
        ];
    }

    public function __toString()
    {
        return $this->getType().':'.$this->getId();
    }
}
