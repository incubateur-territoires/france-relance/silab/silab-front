<?php

namespace App\Shared\Carl\ValueObject;

class ConfigurationCarlObject
{
    public function __construct(
        private string $id,
        private string $label,
        private string $type,
        private string $code
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
