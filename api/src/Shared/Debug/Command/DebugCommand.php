<?php

namespace App\Shared\Debug\Command;

use App\Shared\Carl\Repository\CarlClientRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:debug',
    description: 'Permet de lancer du code directement depuis une commande pour faciliter le debuggage, mais de toutes façon ya jammais de bug donc vous n\'avez rien à faire ici, circulez',
    hidden: false,
)]
class DebugCommand extends Command
{
    public function __construct(
        private CarlClientRepository $carlClientRepository,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $carlClient = $this->carlClientRepository->findAll()[0];

        $startTime = microtime(as_float: true);
        $test = $carlClient->getObjectAndDescendantTypes('MATERIAL');
        $deltaTime = microtime(as_float: true) - $startTime;
        echo "$deltaTime s\n";

        print_r($test);

        return Command::SUCCESS;
    }
}
