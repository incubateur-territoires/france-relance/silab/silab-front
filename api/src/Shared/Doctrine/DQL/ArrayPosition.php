<?php

namespace App\Shared\Doctrine\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\TokenType;

class ArrayPosition extends FunctionNode
{
    public Node $array;

    public Node $element;

    public function getSql(SqlWalker $sqlWalker): string
    {
        return sprintf(
            'ARRAY_POSITION(ARRAY[%s], %s)',
            $this->array->dispatch($sqlWalker),
            $this->element->dispatch($sqlWalker)
        );
    }

    public function parse(Parser $parser): void
    {
        $parser->match(TokenType::T_IDENTIFIER); // array_position
        $parser->match(TokenType::T_OPEN_PARENTHESIS); // (
        $this->array = $parser->ArithmeticPrimary();
        $parser->match(TokenType::T_COMMA); // ,
        $this->element = $parser->ArithmeticPrimary();
        $parser->match(TokenType::T_CLOSE_PARENTHESIS); // )
    }
}
