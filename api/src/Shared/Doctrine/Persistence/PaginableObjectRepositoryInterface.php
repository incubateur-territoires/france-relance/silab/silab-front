<?php

declare(strict_types=1);

namespace App\Shared\Doctrine\Persistence;

use ApiPlatform\State\Pagination\PaginatorInterface;
use UnexpectedValueException;

/**
 * version de Doctrine\Persistence\ObjectRepository mais avec pagination.
 *
 * @template T of object
 */
interface PaginableObjectRepositoryInterface
{
    /**
     * Finds an object by its primary key / identifier.
     *
     * @param mixed $id the identifier
     *
     * @return T|null the object
     */
    public function find($id);

    /**
     * Finds all objects in the repository.
     *
     * @return PaginatorInterface<T> the objects
     */
    public function findAll();

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @psalm-param array<string, 'asc'|'desc'|'ASC'|'DESC'>|null $orderBy
     *
     * @return PaginatorInterface<T> the objects
     *
     * @throws \UnexpectedValueException
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    );

    /**
     * Finds a single object by a set of criteria.
     *
     * @param array<string, mixed> $criteria the criteria
     *
     * @return T|null the object
     */
    public function findOneBy(array $criteria);

    /**
     * Returns the class name of the object managed by the repository.
     *
     * @psalm-return class-string<T>
     */
    public function getClassName();
}
