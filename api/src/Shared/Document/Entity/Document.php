<?php

namespace App\Shared\Document\Entity;

class Document implements DocumentInterface
{
    public function __construct(private string $id, private string $url, private string $name)
    {
    }

    public function setName(string $name): DocumentInterface
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}
