<?php

namespace App\Shared\Document\Entity;

class DocumentBlob
{
    public function __construct(private string $blob, private string $fileName)
    {
    }

    public function __toString(): string
    {
        return $this->blob;
    }

    public function getBlob(): string
    {
        return $this->blob;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }
}
