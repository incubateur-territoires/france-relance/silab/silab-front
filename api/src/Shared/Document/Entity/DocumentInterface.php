<?php

namespace App\Shared\Document\Entity;

interface DocumentInterface
{
    public function getId(): string;

    public function setName(string $url): self;

    public function getName(): string;

    public function setUrl(string $url): self;

    public function getUrl(): string;
}
