<?php

namespace App\Shared\Document\Entity;

class Image implements ImageInterface
{
    public function __construct(protected string $id, protected string $url)
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}
