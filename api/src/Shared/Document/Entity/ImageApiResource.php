<?php

namespace App\Shared\Document\Entity;

use ApiPlatform\Metadata\Post;
use App\InterventionServiceOffer\DemandeIntervention\State\CreateImageProcessor as CreateImageDemandeInterventionProcessor;
use App\InterventionServiceOffer\Intervention\Entity\Intervention\Intervention;
use App\InterventionServiceOffer\Intervention\State\CreateImageProcessor as CreateImageInterventionProcessor;
use Symfony\Component\Serializer\Annotation\Groups;

#[Post(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/interventions/{interventionId}/'.Intervention::IMAGE_CATEGORY_IMAGE_AVANT,
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
        'interventionId' => 'interventionId',
    ],
    defaults: ['imageType' => Intervention::IMAGE_CATEGORY_IMAGE_AVANT],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_AJOUTER_IMAGE_AVANT_INTERVENTION')",
    denormalizationContext: ['groups' => [self::GROUP_AJOUTER_IMAGE]],
    normalizationContext: ['groups' => [self::GROUP_AFFICHER_IMAGE]],
    read: false,
    processor: CreateImageInterventionProcessor::class
)]
#[Post(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/interventions/{interventionId}/'.Intervention::IMAGE_CATEGORY_IMAGE_APRES,
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
        'interventionId' => 'interventionId',
    ],
    defaults: ['imageType' => Intervention::IMAGE_CATEGORY_IMAGE_APRES],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_AJOUTER_IMAGE_APRES_INTERVENTION')",
    denormalizationContext: ['groups' => [self::GROUP_AJOUTER_IMAGE]],
    normalizationContext: ['groups' => [self::GROUP_AFFICHER_IMAGE]],
    read: false,
    processor: CreateImageInterventionProcessor::class
)]
#[Post(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/demandes-interventions/{demandeInterventionId}/images',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId', // IMPORTANT: si on ne précise pas cette variable ici, api-platform la renomme en "id"
        'demandeInterventionId' => 'demandeInterventionId',
    ],
    security: "is_granted('SERVICEOFFER_ROLE_INTERVENTION_AJOUTER_IMAGE_A_DEMANDE_INTERVENTION')",
    denormalizationContext: ['groups' => [self::GROUP_AJOUTER_IMAGE]],
    normalizationContext: ['groups' => [self::GROUP_AFFICHER_IMAGE]],
    read: false,
    processor: CreateImageDemandeInterventionProcessor::class
)]
class ImageApiResource implements ImageInterface
{
    public const GROUP_AJOUTER_IMAGE = 'Ajouter une image à une intervention';
    public const GROUP_AFFICHER_IMAGE = 'Afficher une image d\'une intervention';

    public function __construct(
        #[Groups([self::GROUP_AFFICHER_IMAGE])]
        private ?string $id = null,
        #[Groups([self::GROUP_AJOUTER_IMAGE])]
        private ?string $base64 = null,
        #[Groups([self::GROUP_AFFICHER_IMAGE])]
        private ?string $url = null,
    ) {
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setBase64(string $base64): self
    {
        $this->base64 = $base64;

        return $this;
    }

    public function getBase64(): string
    {
        return $this->base64;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}
