<?php

namespace App\Shared\Document\Entity;

interface ImageInterface
{
    public function getId(): string;

    public function setUrl(string $url): self;

    public function getUrl(): string;
}
