<?php

namespace App\Shared\Document\Entity;

class Metadata
{
    public function __construct(private string $id, private string $titre, private string $description, private string $serviceOffer)
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getTitre(): string
    {
        return $this->titre;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setServiceOffer(string $serviceOffer): self
    {
        $this->serviceOffer = $serviceOffer;

        return $this;
    }

    public function getServiceOffer(): string
    {
        return $this->serviceOffer;
    }
}
