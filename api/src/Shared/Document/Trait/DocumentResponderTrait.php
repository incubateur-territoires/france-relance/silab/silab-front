<?php

namespace App\Shared\Document\Trait;

use App\Shared\Document\Entity\DocumentBlob;
use Symfony\Component\HttpFoundation\Response;

trait DocumentResponderTrait
{
    private function generateResponseFromBlob(DocumentBlob $documentBlob): Response
    {
        $fileMimeType = finfo_buffer(finfo_open(), $documentBlob->getBlob(), FILEINFO_MIME);
        $response = new Response();
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', $fileMimeType);
        $response->headers->set('Content-length', (string) strlen($documentBlob->getBlob()));
        $response->headers->set('Content-Disposition', 'filename='.$documentBlob->getFileName());
        $response->setContent($documentBlob->getBlob());

        return $response;
    }
}
