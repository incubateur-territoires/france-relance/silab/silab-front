<?php

namespace App\Shared\Document\Trait;

use App\Shared\Document\Entity\DocumentBlob;
use Symfony\Component\HttpFoundation\Response;

trait SizeableImageResponderTrait
{
    use DocumentResponderTrait;

    private function generateResponseFromImageBlob(DocumentBlob $imageBlob, ?int $maxSize = null): Response
    {
        if (!is_null($maxSize)) {
            $imageInImagickFormat = new \Imagick();
            $imageInImagickFormat->readImageBlob($imageBlob->getBlob());

            $imageInImagickFormat->resizeImage(
                columns: $maxSize,
                rows: $maxSize,
                filter: \Imagick::FILTER_TRIANGLE,
                blur: 1,
                bestfit: true
            );

            $imageBlob = new DocumentBlob(
                blob: $imageInImagickFormat->getImageBlob(),
                fileName: $imageBlob->getFileName()
            );
        }

        return $this->generateResponseFromBlob($imageBlob);
    }
}
