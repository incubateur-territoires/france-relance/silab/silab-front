<?php

// from : https://github.com/coopTilleuls/UrlSignerBundle?tab=readme-ov-file#custom-signer

namespace App\Shared\Document\UrlSigner;

use CoopTilleuls\UrlSignerBundle\UrlSigner\AbstractUrlSigner;

class SizeableUrlSigner extends AbstractUrlSigner
{
    public static function getName(): string
    {
        return 'sizeable';
    }

    /**
     * Le but du jeu est de pouvoir signer des url en autorisant l'ajout d'un paramètre max-cover-size "paramétrable".
     * Pour ce faire, on l'exclu simplement de la partie signée.
     * Amelioration: comment faire pour que ce signer soit utilisé uniquement pour les images ?
     */
    protected function createSignature(string $url, string $expiration, string $signatureKey): string
    {
        $exctractMaxSizeFromUrlRegex = '~(\?|&)maxSize=[\d]*~';

        $urlWithoutMaxSizeQueryParameter = preg_replace($exctractMaxSizeFromUrlRegex, '', $url);

        return hash_hmac('sha256', "{$urlWithoutMaxSizeQueryParameter}::{$expiration}", $signatureKey);
    }
}
