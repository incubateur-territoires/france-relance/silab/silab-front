<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\Shared\Exception;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use Symfony\Component\HttpFoundation\Exception\BadRequestException as SymfonyBadRequestException;

class BadCarlConfigurationInterventionException extends SymfonyBadRequestException
{
    public function __construct(CarlConfigurationIntervention $CarlconfigurationIntervention, string $message, ?\Throwable $previous = null)
    {
        parent::__construct(
            message: 'L\'objet CarlConfigurationIntervention '.$CarlconfigurationIntervention->getTitle().' est mal configurée : '.$message.' . Merci de vous rapprocher d\'un administrateur.', previous: $previous);
    }
}
