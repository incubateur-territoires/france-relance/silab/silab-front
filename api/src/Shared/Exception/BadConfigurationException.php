<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\Shared\Exception;

use Symfony\Component\HttpFoundation\Exception\BadRequestException as SymfonyBadRequestException;

class BadConfigurationException extends SymfonyBadRequestException
{
    public function __construct(string $entityName, string $message, ?\Throwable $previous = null)
    {
        parent::__construct(
            message: $entityName.' est mal configurée : '.$message.' . Merci de vous rapprocher d\'un administrateur.', previous: $previous);
    }
}
