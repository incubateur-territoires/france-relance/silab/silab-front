<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\Shared\Exception;

use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use Symfony\Component\HttpFoundation\Exception\BadRequestException as SymfonyBadRequestException;

class BadServiceOfferConfigurationException extends SymfonyBadRequestException
{
    public function __construct(ServiceOffer $serviceOffer, string $message, ?\Throwable $previous = null)
    {
        parent::__construct(
            message: 'L\'offre de service '.$serviceOffer->getTitle().' est mal configurée : '.$message.' . Merci de vous rapprocher d\'un administrateur.', previous: $previous);
    }
}
