<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\Shared\Exception;

use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use Symfony\Component\HttpFoundation\Exception\BadRequestException as SymfonyBadRequestException;

class IncompleteServiceOfferConfigurationException extends SymfonyBadRequestException
{
    public function __construct(ServiceOffer $serviceOffer, string $missingParameter, ?\Throwable $previous = null)
    {
        parent::__construct(message: 'Le paramètre "'.$missingParameter.'" de l\'offre de service '.$serviceOffer->getTitle().' doit être configuré, merci de vous rapprocher d\'un administrateur.', previous: $previous);
    }
}
