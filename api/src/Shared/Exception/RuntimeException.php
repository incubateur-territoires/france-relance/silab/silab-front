<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\Shared\Exception;

class RuntimeException extends \RuntimeException
{
}
