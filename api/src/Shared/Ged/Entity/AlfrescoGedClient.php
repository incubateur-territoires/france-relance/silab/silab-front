<?php

namespace App\Shared\Ged\Entity;

use App\Shared\Document\Entity\DocumentBlob;
use App\Shared\Exception\RuntimeException;
use App\Shared\Ged\Exception\AlfrescoBadUrlFormatException;
use App\Shared\Ged\Exception\AlfrescoWrongInstanceException;
use App\Shared\Ged\Exception\ForbiddenCmisPropertyException;
use App\Shared\Ged\Exception\UnknownMetadataKeyException;
use App\Shared\Ged\Repository\AlfrescoGedClientRepository;
use App\Shared\Helpers;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[ORM\Entity(repositoryClass: AlfrescoGedClientRepository::class)]
class AlfrescoGedClient extends GedClient
{
    //  QUESTION_TECHNIQUE comment autowire httpClientInterface ici?

    #[ORM\Column(length: 255)]
    private ?string $username = null;

    #[ORM\Column(length: 255)]
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    private ?string $instanceUrl = null;

    #[ORM\Column(length: 255)]
    private ?string $dropFolder = null;

    /**
     * @var array<string,CmisMetadataMapping>
     */
    #[ORM\Column(type: 'json', options: ['default' => '{}'])]
    #[Context([Serializer::EMPTY_ARRAY_AS_OBJECT => true])]
    private array $metadadaMappings = [];

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getInstanceUrl(): ?string
    {
        return $this->instanceUrl;
    }

    public function setInstanceUrl(string $instanceUrl): self
    {
        $this->instanceUrl = $instanceUrl;

        return $this;
    }

    public function getDropFolder(): ?string
    {
        return $this->dropFolder;
    }

    public function setDropFolder(string $dropFolder): self
    {
        $this->dropFolder = $dropFolder;

        return $this;
    }

    /**
     * @return array<string,CmisMetadataMapping>
     */
    public function getMetadadaMappings(): array
    {
        // la désérialisation json ne va pas construire les objects, on doit le faire
        return array_map(
            function ($rawMetadadaMapping) {
                return new CmisMetadataMapping(
                    property: $rawMetadadaMapping['property'],
                    secondaryObjectType: $rawMetadadaMapping['secondaryObjectType']
                );
            },
            $this->metadadaMappings
        );
    }

    /**
     * @param array<string,CmisMetadataMapping> $metadadasMapping
     */
    public function setMetadadaMappings(array $metadadasMapping): self
    {
        $this->metadadaMappings = $metadadasMapping;

        return $this;
    }

    private function getRepositoryUrl(): string
    {
        return $this->instanceUrl.'/alfresco/api/-default-/public/cmis/versions/1.1/browser';
    }

    private function getImageVisualizationUrl(string $objectId): string
    {
        return $this->instanceUrl."/share/proxy/alfresco/slingshot/node/content/workspace/SpacesStore/$objectId";
    }

    private function extractObjectIdFromFileUrl(string $url): string
    {
        // Note nous acceptons deux formats d'url, principalement pour des raison de rétro-compatibilité :
        // auparavant nous fonctionnnions avec des url de type $repositoryUrlRegex,
        // dorénavent nous utilisons des url du type $sharedUrlRegex
        $repositoryUrlRegex = '/(?<instanceUrl>.*)\/alfresco\/api\/-default-\/public\/cmis\/versions\/1\.1\/browser\/root\?objectId=(?<objectId>.*)$/U';
        $sharedUrlRegex = '/(?<instanceUrl>.*)\/share\/proxy\/alfresco\/slingshot\/node\/content\/workspace\/SpacesStore\/(?<objectId>.*)$/U';

        $match = null;
        if (preg_match($repositoryUrlRegex, $url, $match)) {
            if ($match['instanceUrl'] !== $this->getInstanceUrl()) {
                throw new AlfrescoWrongInstanceException();
            }

            return $match['objectId'];
        } elseif (preg_match($sharedUrlRegex, $url, $match)) {
            if ($match['instanceUrl'] !== $this->getInstanceUrl()) {
                throw new AlfrescoWrongInstanceException();
            }

            return $match['objectId'];
        }

        throw new AlfrescoBadUrlFormatException("Format d'url Alfresco inconnu ($url), impossible de déduire l'objectId");
    }

    private function getObjectRootUrl(string $objectId): string
    {
        return $this->getRepositoryUrl()."/root?objectId=$objectId";
    }

    private function getObjectRootUrlFromFileUrl(string $url): string
    {
        return $this->getObjectRootUrl($this->extractObjectIdFromFileUrl($url));
    }

    private function getAlfrescoHttpClient(): HttpClientInterface
    {
        return HttpClient::createForBaseUri(
            $this->getInstanceUrl(),
            [
                'auth_basic' => [
                    $this->getUsername(),
                    $this->getPassword(),
                ],
            ]
        );
    }

    /**
     * @param array<string,string|null>|null $metadatas amelioration: à l'avenir, implementer des metadataserializer ça pourrait être pas mal
     *
     * @throws ForbiddenCmisPropertyException
     */
    public function persistFile(string $fileB64, string $name, ?array $metadatas = []): string
    {
        // ATTENTION, à partir du moment ou on ajoute l'aspect P:cm:titled,
        // on doit utiliser cm:description (avec l'aspect P:cm:titled) apour la description au lieu de cmis:description
        // https://stackoverflow.com/questions/17890858/how-to-set-the-title-and-description-in-community-alfresco

        if (is_null($this->getDropFolder())) {
            throw new RuntimeException('Merci de configurer un dossier de dépôt.');
        }

        $fileData = base64_decode($fileB64);

        $fileExtension = explode('/', finfo_buffer(finfo_open(), $fileData, FILEINFO_EXTENSION))[0];
        $uniqueName = $name.Uuid::v4().'.'.$fileExtension;
        // sur alfresco il est interdit d'avoir des espaces en bout de titre, trim est important
        // le nom doit également être compatible avec un nom de fichier
        $sanitizedUniqueName = trim(preg_replace('/[^a-z0-9]+/', '-', strtolower($uniqueName))); // from https://stackoverflow.com/a/42908256/2474460
        $fileMimeType = finfo_buffer(finfo_open(), $fileData, FILEINFO_MIME);

        $formFields = [
            'cmisaction' => 'createDocument',
            'propertyId' => [
                'cmis:objectTypeId',
                'cmis:name',
            ],
            'propertyValue' => [
                'cmis:document',
                $sanitizedUniqueName,
            ],
        ];

        $secondaryObjectTypesIds = [];
        $secondaryObjectProperties = [];

        foreach ($this->getMetadadaMappings() as $metadataKey => $CmisMapping) {
            try {
                if (in_array($CmisMapping->property, ['cmis:objectTypeId', 'cmis:name'])) {
                    throw new ForbiddenCmisPropertyException(cmisPropertyId: $CmisMapping->property, gedClient: $this);
                }

                if (!array_key_exists($metadataKey, $metadatas)) {
                    throw new UnknownMetadataKeyException($metadataKey);
                }

                if (is_null($CmisMapping->secondaryObjectType)) {
                    // c'est une propriété de type d'objet de base, on peut l'ajouter directement
                    $formFields['propertyId'][] = $CmisMapping->property;
                    $formFields['propertyValue'][] = $metadatas[$metadataKey];
                } else {
                    // c'est une propriété de type d'objet secondaire,
                    // on les ajoutera plutard tard après la déclaration des types d'objets secondaires
                    $secondaryObjectTypesIds = array_unique(
                        [
                            ...$secondaryObjectTypesIds,
                            $CmisMapping->secondaryObjectType,
                        ]
                    );
                    $secondaryObjectProperties[$CmisMapping->property] = $metadatas[$metadataKey];
                }
            } catch (UnknownMetadataKeyException $e) {
                // toutes les metadonnées silab ne sont pas disponible à 100% du temps, cela peut dépendre des entitées fournies,
                // il est normal de rencontrer des clefs de metadonnée non fournies
                continue;
            }
        }

        // on ajoute maintenant les déclaration et les valeurs des types d'objets secondaires
        if (!empty($secondaryObjectTypesIds)) {
            $formFields['propertyId'][] = 'cmis:secondaryObjectTypeIds';
            $formFields['propertyValue'][] = $secondaryObjectTypesIds;
        }
        foreach ($secondaryObjectProperties as $secondaryPropertyId => $secondaryPropertyValue) {
            $formFields['propertyId'][] = $secondaryPropertyId;
            $formFields['propertyValue'][] = $secondaryPropertyValue ?? ''; // on ne peut pas mettre de valeur nulle
        }

        $formFields['file'] = new DataPart(
            $fileData,
            $sanitizedUniqueName,
            $fileMimeType
        );

        $formData = new FormDataPart($formFields);

        $options = [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'body' => $formData->bodyToIterable(),
        ];

        $versionedObjectId = $this->getAlfrescoHttpClient()->request('POST', $this->getDropFolder(), $options)
                        ->toArray()['properties']['cmis:objectId']['value'];

        // échappe la version du fichier afin de toujours pointer sur la dernière version, c'est lecomportement par défaut quand auucune version n'est precisée.
        $objectId = substr(
            $versionedObjectId,
            0,
            strrpos($versionedObjectId, ';')
        );

        return $this->getImageVisualizationUrl($objectId);
    }

    public function updateFile(string $fileUrl, string $newFileB64, ?string $filename): void
    {
        $this->checkIn(
            $this->checkOut($this->getObjectRootUrlFromFileUrl($fileUrl)),
            $newFileB64,
            $filename
        );
    }

    private function checkOut(string $fileUrl): string
    {
        $formData = new FormDataPart(['cmisaction' => 'checkOut']);
        $options = [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'body' => $formData->bodyToIterable(),
        ];

        return $this->getAlfrescoHttpClient()->request('POST', $fileUrl, $options)
        ->toArray()['properties']['cmis:objectId']['value'];
    }

    private function checkIn(string $pwcId, string $newFileB64, ?string $filename = ''): void
    {
        $fileData = base64_decode($newFileB64);
        $fileExtension = explode('/', finfo_buffer(finfo_open(), $fileData, FILEINFO_EXTENSION))[0];
        $uniqueName = $filename.Uuid::v4().'.'.$fileExtension;
        // sur alfresco il est interdit d'avoir des espaces en bout de titre, trim est important
        // le nom doit également être compatible avec un nom de fichier
        $sanitizedUniqueName = trim(preg_replace('/[^a-z0-9]+/', '-', strtolower($uniqueName))); // from https://stackoverflow.com/a/42908256/2474460

        $fileMimeType = finfo_buffer(finfo_open(), $fileData, FILEINFO_MIME);

        $formFields = [
            'cmisaction' => 'checkIn',
            'file' => new DataPart(
                $fileData,
                $sanitizedUniqueName,
                $fileMimeType
            ),
        ];

        $formData = new FormDataPart($formFields);

        $options = [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'body' => $formData->bodyToIterable(),
        ];

        $this->getAlfrescoHttpClient()->request(
            'POST',
            $this->getObjectRootUrl($pwcId),
            $options
        );
    }

    public function getFile(string $fileUrl): DocumentBlob
    {
        $response = $this->getAlfrescoHttpClient()->request('GET', $this->getObjectRootUrlFromFileUrl($fileUrl));

        return new DocumentBlob(
            blob: $response->getContent(),
            fileName: Helpers::getFileNameFromResponse($response)
        );
    }
}
