<?php

namespace App\Shared\Ged\Entity;

class CmisMetadataMapping
{
    /**
     * Undocumented function.
     *
     * @param string|null $secondaryObjectType Si non précisé, il s'agit d'une propriété de base, voir section https://docs.oasis-open.org/cmis/CMIS/v1.1/errata01/os/CMIS-v1.1-errata01-os-complete.html#x1-650008, sinon c'est un type secondaire, voir https://docs.oasis-open.org/cmis/CMIS/v1.1/errata01/os/CMIS-v1.1-errata01-os-complete.html#x1-100003
     */
    public function __construct(
        public string $property,
        public ?string $secondaryObjectType = null
    ) {
    }
}
