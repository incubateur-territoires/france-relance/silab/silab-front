<?php

namespace App\Shared\Ged\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\InterventionServiceOffer\InterventionServiceOffer\Entity\InterventionServiceOffer;
use App\Shared\Document\Entity\DocumentBlob;
use App\Shared\Ged\Exception\UnsupportedUrlException;
use App\Shared\Ged\Repository\GedClientRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[Get(
    security: "is_granted('ROLE_CONSULTER_GED_CLIENTS')"
)]
#[GetCollection(
    security: "is_granted('ROLE_CONSULTER_GED_CLIENTS')"
)]
#[ORM\Entity(repositoryClass: GedClientRepository::class)]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap([
    'alfresco' => AlfrescoGedClient::class,
    'mezzoteamV2' => MezzoteamV2GedClient::class,
])]
abstract class GedClient
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_LISTE_ODS])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_LISTE_ODS])]
    private ?string $title = null;

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param array<string,string>|null $metadatas amelioration: à l'avenir, implementer des metadataserializer ça pourrait être pas mal
     *
     * @return string L'url d'accès de l'image stockée
     */
    abstract public function persistFile(string $fileB64, string $name, ?array $metadatas): string;

    abstract public function updateFile(string $fileUrl, string $newFileB64, ?string $filename): void; // note A terme il faudrait retourner l'url de l'image.

    /**
     * @throws UnsupportedUrlException quand l'url fournie ne correspond pas à cette ged
     */
    abstract public function getFile(string $fileUrl): DocumentBlob;
}
