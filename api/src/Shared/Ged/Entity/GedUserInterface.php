<?php

namespace App\Shared\Ged\Entity;

interface GedUserInterface
{
    /**
     * @return array<GedClient>
     */
    public function getGedDocumentReaders(): array;

    /**
     * @param array<GedClient> $gedClients
     */
    public function setGedDocumentReaders(array $gedClients): self;

    public function getGedDocumentWriter(): ?GedClient;

    public function setGedDocumentWriter(?GedClient $gedClient): self;
}
