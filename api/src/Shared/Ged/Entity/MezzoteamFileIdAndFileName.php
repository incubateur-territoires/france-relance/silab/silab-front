<?php

namespace App\Shared\Ged\Entity;

class MezzoteamFileIdAndFileName
{
    public function __construct(private string $id, private string $fileName)
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }
}
