<?php

namespace App\Shared\Ged\Entity;

use App\Shared\Document\Entity\DocumentBlob;
use App\Shared\Exception\BadRequestException;
use App\Shared\Ged\Exception\MezzoteamBadUrlFormatException;
use App\Shared\Ged\Exception\MezzoteamMultipleFilesInDocumentException;
use App\Shared\Ged\Exception\MezzoteamWrongInstanceException;
use App\Shared\Ged\Repository\MezzoteamV2GedClientRepository;
use App\Shared\Ged\ValueObject\MezzoteamV2DocumentUrlInformation;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MezzoteamV2GedClientRepository::class)]
class MezzoteamV2GedClient extends GedClient
{
    //  QUESTION_TECHNIQUE comment autowire httpClientInterface ici?

    private ?\SoapClient $filesSoapClient = null;
    private ?\SoapClient $directorySoapClient = null;

    private ?\SoapHeader $securityHeader = null;

    #[ORM\Column(length: 255)]
    private ?string $username = null;

    #[ORM\Column(length: 255)]
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    private ?string $instanceUrl = null;

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getInstanceUrl(): ?string
    {
        return $this->instanceUrl;
    }

    public function setInstanceUrl(string $instanceUrl): self
    {
        $this->instanceUrl = $instanceUrl;

        return $this;
    }

    private function getFilesServiceWsdlUrl(): string
    {
        return $this->instanceUrl.'/ws/V2/Files.asmx?WSDL';
    }

    private function getFilesSoapClient(): \SoapClient
    {
        $this->filesSoapClient ??= new \SoapClient($this->getFilesServiceWsdlUrl(), ['trace' => 1]);

        return $this->filesSoapClient;
    }

    private function getAuthenticatedFilesSoapClient(): \SoapClient
    {
        $this->getFilesSoapClient()->__setSoapHeaders($this->getSecurityHeader());

        return $this->getFilesSoapClient();
    }

    private function getDirectoryServiceWsdlUrl(): string
    {
        return $this->instanceUrl.'/ws/V2/Directory.asmx?WSDL';
    }

    private function getDirectorySoapClient(): \SoapClient
    {
        $this->directorySoapClient ??= new \SoapClient($this->getDirectoryServiceWsdlUrl(), ['trace' => 1]);

        return $this->directorySoapClient;
    }

    private function getAuthenticatedDirectorySoapClient(): \SoapClient
    {
        $this->getDirectorySoapClient()->__setSoapHeaders($this->getSecurityHeader());

        return $this->getDirectorySoapClient();
    }

    private function extractFileInformationFromUrl(string $url): MezzoteamV2DocumentUrlInformation
    {
        // ex : http://ged**.****.fr/link.aspx?type=document&ws=d7f7ad60-5078-4725-****-1de79625140e&id=476d73ed-221c-****-8fee-*****
        $repositoryUrlRegex = '/(?<instanceUrl>^.*)\/.*$/';

        $match = null;
        if (preg_match($repositoryUrlRegex, $url, $match)) {
            if ($match['instanceUrl'] !== $this->getInstanceUrl()) {
                throw new MezzoteamWrongInstanceException("L'instance Mezzoteam ne correspond pas à l'url fournie: $url");
            }

            $urlParts = parse_url($url);
            $queryVariables = [];
            parse_str($urlParts['query'], $queryVariables);

            $fileId = $queryVariables['id'] ?? null;
            if (!is_string($fileId)) {
                throw new MezzoteamBadUrlFormatException("L'url fournie ($url) ne permet pas de déduire l'id du fichier");
            }
            $workspaceId = $queryVariables['ws'] ?? null;
            if (!is_string($workspaceId)) {
                throw new MezzoteamBadUrlFormatException("L'url fournie ($url) ne permet pas de déduire l'id du workspace");
            }

            return new MezzoteamV2DocumentUrlInformation(
                documentId: $fileId,
                workspaceId: $workspaceId
            );
        }

        throw new MezzoteamWrongInstanceException("L'instance Mezzoteam ne correspond pas à l'url fournie: $url");
    }

    private function getFileblobFromSoapClient(string $fileId): string
    {
        // récupère le Webservice Directory
        $filesWS = $this->getAuthenticatedFilesSoapClient();

        // prépare la requete SOAP en indiquant l'id du fichier
        $downloadRequestParameters = [
            'targetFile' => $fileId,
        ];

        // créé la demande de téléchargement
        $response = $filesWS->DownloadRequestCreate($downloadRequestParameters);

        // c'est bon, le fichier est initié
        // Utils::_($response, "reponse");

        // sauvegarde du requestID
        $requestId = $response->DownloadRequestCreateResult;

        // prépare la requete SOAP en indiquant l'id du fichier
        $requestGetParameters = [
            'requestId' => $requestId,
            'level' => 1,
        ];

        // créé la demande de téléchargement
        $response = $filesWS->RequestGet($requestGetParameters);

        $buffer = null;
        $positionInFile = 0;
        $chunkSize = 1024 * 1024;
        $file = null;
        do {
            // prépare la requete SOAP de téléchargement
            $downloadRequestProcessParameters = [
                'requestId' => $requestId,
                'position' => $positionInFile,
                'count' => $chunkSize,
            ];

            $response = $filesWS->DownloadRequestProcess($downloadRequestProcessParameters);

            $buffer = $response->DownloadRequestProcessResult;
            if (!empty($buffer)) {
                $file .= $buffer;
                $positionInFile += strlen($buffer);
            }
        } while ((null != $buffer) && (strlen($buffer) == $chunkSize));

        return $file;
    }

    private function getFileInfoFromDocumentId(string $documentId): MezzoteamFileIdAndFileName
    {
        // récupère le Webservice Directory
        $filesWS = $this->getAuthenticatedFilesSoapClient();

        // prépare la requete SOAP en indiquant l'id du document
        $fileGetListParameters = [
            'parentFolder' => $documentId,
            'recursive' => false,
        ];

        // on demande tous les fichiers du document
        $file = $filesWS->FileGetList($fileGetListParameters)->FileGetListResult->File;

        // ne pouvant gérer qu'un seul fichier, on part du principe qu'il y a erreur si plusieurs fihiers sont joints
        if (is_array($file)) {
            throw new MezzoteamMultipleFilesInDocumentException(instanceUrl: $this->getInstanceUrl(), documentId: $documentId);
        }

        return new MezzoteamFileIdAndFileName(
            id: $file->Id,
            fileName: $file->Name
        );
    }

    /**
     * @param array<string,string>|null $metadatas amelioration: à l'avenir, implementer des metadataserializer ça pourrait être pas mal
     *
     * @return string L'url d'accès de l'image stockée
     */
    public function persistFile(string $fileB64, string $name, ?array $metadatas): string
    {
        throw new BadRequestException('Le client GED '.$this->getTitle().', avec lequel vous essayez d\'enregistrer un fichier ne permet uniquement de lire des fichiers. Merci de ne pas l\'utiliser en écriture dans la configuration de Silab.');
    }

    public function updateFile(string $fileUrl, string $newFileB64, ?string $filename): void
    {
        throw new BadRequestException('Le client GED '.$this->getTitle().', avec lequel vous essayez d\'enregistrer un fichier ne permet uniquement de lire des fichiers. Merci de ne pas l\'utiliser en écriture dans la configuration de Silab.');
    }

    private function getSecurityHeader(): \SoapHeader
    {
        if (is_null($this->securityHeader)) {
            $loginParameters = [
                'login' => $this->getUsername(),
                'password' => $this->getPassword(),
            ];

            $securityKey = $this->getDirectorySoapClient()->Login($loginParameters)->LoginResult;
            $authentication = ['SecurityKey' => $securityKey];
            $this->securityHeader = new \SoapHeader('http://www.mezzoteam.com/', 'CredentialSoapHeader', $authentication);
        }

        return $this->securityHeader;
    }

    public function getFile(string $fileUrl): DocumentBlob
    {
        $fileInformation = $this->extractFileInformationFromUrl(url: $fileUrl);

        $workspaceChangeParameters =
        [
            'workspaceId' => $fileInformation->workspaceId,
        ];
        $this->getAuthenticatedDirectorySoapClient()->WorkspaceChange($workspaceChangeParameters);

        $fileInfo = $this->getFileInfoFromDocumentId(documentId: $fileInformation->documentId);

        return new DocumentBlob(
            blob: $this->getFileblobFromSoapClient(fileId: $fileInfo->getId()),
            fileName: $fileInfo->getFileName()
        );
    }
}
