<?php

namespace App\Shared\Ged\Exception;

class AlfrescoBadUrlFormatException extends UnsupportedUrlException
{
}
