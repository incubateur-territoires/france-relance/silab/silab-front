<?php

namespace App\Shared\Ged\Exception;

class AlfrescoWrongInstanceException extends UnsupportedUrlException
{
}
