<?php

namespace App\Shared\Ged\Exception;

class BadAlfrescoUrlFormatException extends UnsupportedUrlException
{
}
