<?php

namespace App\Shared\Ged\Exception;

use App\Shared\Exception\BadRequestException;
use App\Shared\Ged\Entity\GedClient;

class ForbiddenCmisPropertyException extends BadRequestException
{
    public function __construct(string $cmisPropertyId, GedClient $gedClient)
    {
        parent::__construct(
            message: "Vous essayez de spécifer la propriété cmis $cmisPropertyId mais celle-ci est interdite par votre connecteur GED \"".$gedClient->getTitle().'", vous devez la retirer des mappings de ce connecteur.');
    }
}
