<?php

namespace App\Shared\Ged\Exception;

class MezzoteamBadUrlFormatException extends UnsupportedUrlException
{
}
