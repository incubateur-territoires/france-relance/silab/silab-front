<?php

namespace App\Shared\Ged\Exception;

use App\Shared\Exception\RuntimeException;

class MezzoteamMultipleFilesInDocumentException extends RuntimeException
{
    public function __construct(string $instanceUrl, string $documentId)
    {
        parent::__construct(
            message: "Le document mezzoteam de l'instance $instanceUrl avec l'id $documentId contient plusieurs fichiers. Ce cas n'est actuellement pas géré par le connecteur GED Mezzoteam.", );
    }
}
