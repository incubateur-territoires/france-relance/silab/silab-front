<?php

namespace App\Shared\Ged\Exception;

class MezzoteamWrongInstanceException extends UnsupportedUrlException
{
}
