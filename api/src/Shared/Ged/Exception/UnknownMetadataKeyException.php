<?php

namespace App\Shared\Ged\Exception;

use App\Shared\Exception\RuntimeException;

class UnknownMetadataKeyException extends RuntimeException
{
    public function __construct(string $metadataKey)
    {
        parent::__construct(
            message: "Vous essayez d'enregistrer le contenu de la metadonnée silab avec la clef $metadataKey mais celle-ci n'existe pas", );
    }
}
