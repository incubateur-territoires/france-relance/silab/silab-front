<?php

namespace App\Shared\Ged\Exception;

use App\Shared\Exception\RuntimeException;

class UnsupportedUrlException extends RuntimeException
{
}
