<?php

namespace App\Shared\Ged\Exception;

class WrongAlfrescoInstanceException extends UnsupportedUrlException
{
}
