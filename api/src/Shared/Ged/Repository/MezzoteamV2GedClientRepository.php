<?php

namespace App\Shared\Ged\Repository;

use App\Shared\Ged\Entity\MezzoteamV2GedClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MezzoteamV2GedClient>
 *
 * @method MezzoteamV2GedClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method MezzoteamV2GedClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method MezzoteamV2GedClient[]    findAll()
 * @method MezzoteamV2GedClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MezzoteamV2GedClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MezzoteamV2GedClient::class);
    }

    public function save(MezzoteamV2GedClient $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(MezzoteamV2GedClient $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    //    /**
    //     * @return MezzoteamV2GedClient[] Returns an array of MezzoteamV2GedClient objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?MezzoteamV2GedClient
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
