<?php

namespace App\Shared\Ged\ValueObject;

class MezzoteamV2DocumentUrlInformation
{
    public function __construct(
        public string $documentId,
        public string $workspaceId
    ) {
    }
}
