<?php

namespace App\Shared\Gmao\Entity;

use App\InterventionServiceOffer\Intervention\Entity\Activite\Operation;

class GmaoActeur
{
    public function __construct(
        private string $id,
        private string $nomComplet,
        /**
         * @var array<string>
         */
        private array $emails,
        /**
         * @var array<string>
         */
        private array $telephones,
        private GmaoActeurStatus $currentStatus,
        /**
         * @var array<Operation>
         */
        private ?array $operationsHabilitees
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getNomComplet(): string
    {
        return $this->nomComplet;
    }

    /**
     * @return array<string>
     */
    public function getEmails(): array
    {
        return $this->emails;
    }

    /**
     * @return array<string>
     */
    public function getTelephones(): array
    {
        return $this->telephones;
    }

    public function getCurrentStatus(): GmaoActeurStatus
    {
        return $this->currentStatus;
    }

    /**
     * @return array<Operation>|null
     */
    public function getOperationsHabilitees(): ?array
    {
        return $this->operationsHabilitees;
    }
}
