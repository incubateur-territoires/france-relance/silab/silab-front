<?php

namespace App\Shared\Helper\Exception;

class EmptyStringException extends \Exception
{
    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct(message: 'La chaine de caractère fournie est vide.', previous: $previous);
    }
}
