<?php

namespace App\Shared\Helper\Exception;

class ForbiddenDoubleQuoteCharacterException extends \Exception
{
    public function __construct(string $veryBadString😈Oulala, ?\Throwable $previous = null)
    {
        parent::__construct(message: "La chaine de caractère $veryBadString😈Oulala contient un ou plusieurs guillemets doubles.", previous: $previous);
    }
}
