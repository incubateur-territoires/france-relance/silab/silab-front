<?php

namespace App\Shared;

use App\Shared\Helper\Exception\EmptyStringException;
use App\Shared\Helper\Exception\ForbiddenDoubleQuoteCharacterException;
use Symfony\Component\Mime\MimeTypes;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Contracts\HttpClient\ResponseInterface;

class Helpers
{
    // from https://stackoverflow.com/a/5147704
    /**
     * @param array<mixed> $array
     * @param array<mixed> $excludeKeys
     *
     * @return array<mixed>
     */
    public static function arrayWithoutKeys(array $array, array $excludeKeys): array
    {
        foreach ($excludeKeys as $key) {
            unset($array[$key]);
        }

        return $array;
    }

    /**
     * @param string|int   $key
     * @param array<mixed> $array
     */
    public static function initArrayValueIfAbsent(array &$array, mixed $key, mixed $value): void
    {
        if (array_key_exists($key, $array)) {
            return;
        }

        $array[$key] = $value;
    }

    /**
     * Vérifie le type MIME d'un fichier.
     *
     * @throws \RuntimeException Si le type MIME n'est pas autorisé
     */
    public static function validateImageMimeType(string $base64): void
    {
        $fileData = base64_decode($base64);
        $fileMimeType = finfo_buffer(finfo_open(), $fileData, FILEINFO_MIME_TYPE);
        $allowedImageMimeTypes = [
            'image/jpeg',
            'image/png',
            'image/gif',
        ];

        if (!in_array($fileMimeType, $allowedImageMimeTypes)) {
            throw new \RuntimeException("Type MIME non pris en charge : $fileMimeType");
        }
    }

    /**
     * @template T
     *
     * @param array<string, T> $array
     *
     * @return array<string, T>
     */
    public static function prefixKeys(array $array, string $prefix): array
    {
        $newArray = [];
        foreach ($array as $key => $value) {
            $newKey = $prefix.$key;
            $newArray[$newKey] = $value;
        }

        return $newArray;
    }

    /**
     * @param string|array<string> $verySupiciousStrings🤨
     *
     * @throws ForbiddenDoubleQuoteCharacterException Si la chaine de caractère
     *                                                ou une des chaines (dans le cas d'un tableau) possède un "
     */
    public static function throwIfDoubleQuotes(string|array $verySupiciousStrings🤨): void
    {
        if (!is_array($verySupiciousStrings🤨)) {
            $verySupiciousStrings🤨 = [$verySupiciousStrings🤨];
        }
        foreach ($verySupiciousStrings🤨 as $verySupiciousString🤨) {
            if (false !== strpos($verySupiciousString🤨, '"')) {
                throw new ForbiddenDoubleQuoteCharacterException($verySupiciousString🤨);
            }
        }
    }

    /**
     * @param string|array<string|null>|null $verySupiciousStrings🤨
     *
     * @throws EmptyStringException Si la chaine de caractère
     *                              ou une des chaines est vide
     */
    public static function throwIfempty(string|array|null $verySupiciousStrings🤨): void
    {
        if (!is_array($verySupiciousStrings🤨)) {
            $verySupiciousStrings🤨 = [$verySupiciousStrings🤨];
        }
        foreach ($verySupiciousStrings🤨 as $verySupiciousString🤨) {
            if (empty($verySupiciousString🤨)) {
                throw new EmptyStringException();
            }
        }
    }

    /**
     * from https://stackoverflow.com/a/73741547/2474460.
     */
    public static function extractFilenameFromContentDispositionHeader(string $contentDisposition): ?string
    {
        $parts = explode(';', $contentDisposition);
        foreach ($parts as $p) {
            if (false !== stripos($p, 'filename')) {
                $kv = parse_ini_string($p);

                return $kv['filename'];
            }
        }

        return null;
    }

    /**
     * Déduit le nom de fichier depuis une réponse d'un httpClient.
     * Retourne en priorité le filename contenu dans le header "content-disposition",
     * sinon utilise le nom de fichier depuis l'url, en l'absence d'extension dans ce dernier,
     * on essaie de la déduire du type mime.
     */
    public static function getFileNameFromResponse(ResponseInterface $response): string
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $filename = self::extractFilenameFromContentDispositionHeader(
            $propertyAccessor->getValue($response->getHeaders(), '[content-disposition][0]') ?? ''
        );

        $content = $response->getContent();

        if (is_null($filename)) {
            $filename = self::getFileNameWithExtensionFromUrlAndBlob(
                $response->getInfo()['original_url'],
                $content
            );
        }

        return $filename;
    }

    public static function getFileNameWithExtensionFromUrlAndBlob(string $url, string &$blob): string
    {
        $path_info = pathinfo(parse_url($url)['path']);

        // si une extension était présente, on retourne directement nomdefichier+ext présent dans le basename
        if (array_key_exists('extension', $path_info)) {
            return $path_info['basename'];
        }

        // sinon, on va devoir déduire l'extension depuis le type mime
        $fileMimeType = finfo_buffer(finfo_open(), $blob, FILEINFO_MIME);
        $mimeTypes = new MimeTypes();
        $guessedExtension = $mimeTypes->getExtensions($fileMimeType)[0] ?? null;

        return $path_info['filename'].is_null($guessedExtension) ? '' : '.'.$guessedExtension;
    }
}
