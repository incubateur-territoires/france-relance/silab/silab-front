<?php

namespace App\Shared\JsonApi;

use App\Shared\Exception\RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccess;

class JsonApiResource
{
    /**
     * @param array<mixed>  $ressourceResponseArray         l'élément de data du $responseArray récupéré par un appel a une api JSON:API
     * @param ?array<mixed> $includedResourcesResponseArray l'élément included du $responseArray récupéré par un appel a une api JSON:API
     *
     * @return void
     */
    public function __construct(private array $ressourceResponseArray, private ?array $includedResourcesResponseArray = null)
    {
    }

    // public static function getResources()

    /**
     * Récupère les données de la relation d'une resource.
     *
     * @param string $relationshipKey la clef de l'entité liée
     */
    public function getRelationship(
        string $relationshipKey,
    ): ?JsonApiResource {
        // si la relation recherchée n'existe pas pour cette resource
        if (!array_key_exists('relationships', $this->ressourceResponseArray)) {
            throw new RuntimeException("La relation $relationshipKey est introuvable dans l'entité {$this->ressourceResponseArray['type']}");
        }

        // si la relation recherchée n'existe pas pour cette resource
        if (!array_key_exists($relationshipKey, $this->ressourceResponseArray['relationships'])) {
            throw new RuntimeException("La relation $relationshipKey est introuvable dans l'entité {$this->ressourceResponseArray['type']}");
        }

        // si la relation recherchée n'est pas inclue
        if (!array_key_exists('data', $this->ressourceResponseArray['relationships'][$relationshipKey])) {
            throw new RuntimeException("La relation $relationshipKey est n'a pas été inclue dans la resource");
        }

        // si la relation recherché est présente mais nulle
        if (is_null($this->ressourceResponseArray['relationships'][$relationshipKey]['data'])) {
            return null;
        }

        foreach ($this->includedResourcesResponseArray as $resourceCandidate) {
            if (
                $resourceCandidate['id'] === $this->ressourceResponseArray['relationships'][$relationshipKey]['data']['id']
                && $resourceCandidate['type'] === $this->ressourceResponseArray['relationships'][$relationshipKey]['data']['type']
            ) {
                return new JsonApiResource($resourceCandidate, [...$this->includedResourcesResponseArray, $this->ressourceResponseArray]);
            }
        }

        throw new RuntimeException();
    }

    /**
     * @return array<JsonApiResource>
     */
    public function getRelationshipCollection(
        string $relationshipKey
    ): array {
        if (!array_key_exists($relationshipKey, $this->ressourceResponseArray['relationships'])) {
            throw new RuntimeException("La relation $relationshipKey est introuvable dans l'entité {$this->ressourceResponseArray['type']}");
        }

        /** @var array<JsonApiResource> $relationships */
        $relationships = [];

        foreach ($this->ressourceResponseArray['relationships'][$relationshipKey]['data'] as $relationship) {
            foreach ($this->includedResourcesResponseArray as $resourceCandidate) {
                if (
                    $resourceCandidate['id'] === $relationship['id']
                    && $resourceCandidate['type'] === $relationship['type']
                ) {
                    $relationships[] = new JsonApiResource($resourceCandidate, $this->includedResourcesResponseArray);
                }
            }
        }

        return $relationships;
    }

    public function getAttribute(string $attributeName): mixed
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->getPropertyAccessor();

        return $propertyAccessor->getValue($this->ressourceResponseArray, "[attributes][$attributeName]");
    }

    public function getId(): string
    {
        return $this->ressourceResponseArray['id'];
    }

    public function getType(): string
    {
        return $this->ressourceResponseArray['type'];
    }

    public function getLink(string $key): string
    {
        $linksData = $this->ressourceResponseArray['links'];

        if (!array_key_exists($key, $linksData)) {
            throw new RuntimeException("La clé $key est introuvable dans l'entité.");
        }

        return $linksData[$key];
    }

    public function getRelationshipRelatedLink(string $relationshipKey): string
    {
        // si la relation recherchée n'existe pas pour cette resource
        if (!array_key_exists('relationships', $this->ressourceResponseArray)) {
            throw new RuntimeException("La relation $relationshipKey est introuvable dans l'entité {$this->ressourceResponseArray['type']}");
        }

        // si la relation recherchée n'existe pas pour cette resource
        if (!array_key_exists($relationshipKey, $this->ressourceResponseArray['relationships'])) {
            throw new RuntimeException("La relation $relationshipKey est introuvable dans l'entité {$this->ressourceResponseArray['type']}");
        }

        // si la relation recherchée n'est pas inclue
        if (!array_key_exists('data', $this->ressourceResponseArray['relationships'][$relationshipKey])) {
            throw new RuntimeException("La relation $relationshipKey est n'a pas été inclue dans la resource");
        }

        return $this->ressourceResponseArray['relationships'][$relationshipKey]['links']['related'];
    }

    public function getRelationshipSelfLink(string $relationshipKey): string
    {
        // si la relation recherchée n'existe pas pour cette resource
        if (!array_key_exists('relationships', $this->ressourceResponseArray)) {
            throw new RuntimeException("La relation $relationshipKey est introuvable dans l'entité {$this->ressourceResponseArray['type']}");
        }

        // si la relation recherchée n'existe pas pour cette resource
        if (!array_key_exists($relationshipKey, $this->ressourceResponseArray['relationships'])) {
            throw new RuntimeException("La relation $relationshipKey est introuvable dans l'entité {$this->ressourceResponseArray['type']}");
        }

        // si la relation recherchée n'est pas inclue
        if (!array_key_exists('data', $this->ressourceResponseArray['relationships'][$relationshipKey])) {
            throw new RuntimeException("La relation $relationshipKey est n'a pas été inclue dans la resource");
        }

        return $this->ressourceResponseArray['relationships'][$relationshipKey]['links']['self'];
    }

    /**
     * @param array<JsonApiResource> $jsonApiResources
     *
     * @return array<JsonApiResource>
     */
    public static function removeDuplicateRessources(array $jsonApiResources): array
    {
        return array_values(
            array_reduce(
                $jsonApiResources,
                function ($filteredJsonApiResources, JsonApiResource $jsonApiResource) {
                    $id = $jsonApiResource->getId();
                    if (!isset($filteredJsonApiResources[$id])) {
                        $filteredJsonApiResources[$id] = $jsonApiResource;
                    }

                    return $filteredJsonApiResources;
                },
                []
            )
        );
    }
}
