<?php

namespace App\Shared\JsonApi;

/** @extends \ArrayObject<int, JsonApiResource> */
class JsonApiResourceCollection extends \ArrayObject
{
    public function __construct(private ?string $nextPageUrl = null)
    {
        parent::__construct();
    }

    public function getNextPageUrl(): ?string
    {
        return $this->nextPageUrl;
    }
}
