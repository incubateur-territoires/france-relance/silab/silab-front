<?php

namespace App\Shared\Location\Entity;

use App\ElusServiceOffer\Intervention\Entity\Intervention as ELusIntervention;
use Symfony\Component\Serializer\Annotation\Groups;

#[Groups([
    ELusIntervention::GROUP_LISTER_INTERVENTIONS,
])]
class Coordinates
{
    public function __construct(
        public float $lat,
        public float $lng
    ) {
    }

    public function getLat(): float
    {
        return $this->lat;
    }

    public function getLng(): float
    {
        return $this->lng;
    }

    public static function fromUnsafeLatLng(?float $latitude, ?float $longitude): ?Coordinates
    {
        $hasCorrectCoordinates = !is_null($latitude) && !is_null($longitude);

        return $hasCorrectCoordinates ? new Coordinates(
            $latitude,
            $longitude
        ) : null;
    }
}
