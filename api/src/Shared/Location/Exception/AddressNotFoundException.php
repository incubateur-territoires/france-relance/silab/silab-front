<?php

namespace App\Shared\Location\Exception;

class AddressNotFoundException extends \Exception
{
}
