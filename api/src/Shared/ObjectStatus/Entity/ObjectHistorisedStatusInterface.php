<?php

namespace App\Shared\ObjectStatus\Entity;

interface ObjectHistorisedStatusInterface extends ObjectStatusInterface
{
    public const GROUP_AFFICHER_HISTORIQUE_STATUTS = 'Affiche un historique de statut';

    public function getComment(): ?string;
}
