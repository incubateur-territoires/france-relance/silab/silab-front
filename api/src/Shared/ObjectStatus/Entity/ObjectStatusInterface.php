<?php

namespace App\Shared\ObjectStatus\Entity;

interface ObjectStatusInterface
{
    public function getCode(): string;

    public function getLabel(): string;

    public function getCreatedBy(): string;

    public function getCreatedAt(): \DateTimeInterface;
}
