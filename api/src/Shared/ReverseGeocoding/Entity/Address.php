<?php

namespace App\Shared\ReverseGeocoding\Entity;

class Address
{
    public function __construct(private string $address)
    {
    }

    public function getAddress(): string
    {
        return $this->address;
    }
}
