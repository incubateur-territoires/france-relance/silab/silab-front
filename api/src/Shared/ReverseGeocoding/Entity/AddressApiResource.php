<?php

namespace App\Shared\ReverseGeocoding\Entity;

use ApiPlatform\Metadata\Get;
use App\Shared\ReverseGeocoding\State\GetReverseProvider;

#[Get(
    uriTemplate: '/geocoding/reverse',
    output: Address::class,
    provider: GetReverseProvider::class,
    security: "is_granted('ROLE_GOCODING_REVERSE_COORDONEES')",
)]
class AddressApiResource
{
}
