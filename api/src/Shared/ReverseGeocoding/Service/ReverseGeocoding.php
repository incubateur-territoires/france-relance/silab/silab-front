<?php

namespace App\Shared\ReverseGeocoding\Service;

use App\Shared\Exception\RuntimeException;
use App\Shared\Location\Exception\AddressNotFoundException;
use Symfony\Component\HttpClient\CachingHttpClient;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpKernel\HttpCache\Store;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ReverseGeocoding
{
    private HttpClientInterface $httpClient;

    public function __construct()
    {
        $store = new Store(__DIR__.'/../../var/cache/storage');

        $this->httpClient = new CachingHttpClient(HttpClient::create(), $store);
    }

    /**
     * Récupère l'adresse à partir des coordonnées en utilisant l'API de géocodage inversé de l'IGN.
     *
     * @param float $latitude  La coordonnée de latitude
     * @param float $longitude La coordonnée de longitude
     *
     * @return string L'adresse correspondant aux coordonnées fournies
     *
     * @throws AddressNotFoundException Si aucune adresse n'est trouvée pour les coordonnées spécifiées
     * @throws RuntimeException         Si une erreur survient lors de la récupération de l'adresse
     */
    public function getAddressFromCoordinates(float $latitude, float $longitude): string
    {
        $addresseCacheKey = "reverseGeocoding/$latitude/$longitude/address";

        $cachedAddressName = apcu_fetch($addresseCacheKey);
        if (false !== $cachedAddressName) {
            return $cachedAddressName;
        }

        $geoCodingApiUrl = $_ENV['GEOPORTAIL_GEOCODING_API_URL'];

        $queryParams = [
            'lat' => $latitude,
            'lon' => $longitude,
            'index' => 'address',
            'limit' => 1,
        ];

        $apiUrlWithParams = $geoCodingApiUrl.'?'.http_build_query($queryParams);

        try {
            $response = $this->httpClient->request('GET', $apiUrlWithParams);
            $responseData = $response->toArray();
        } catch (\Exception $e) {
            return '';
            // Note importante : nous commentons cette exception en hotfix car nous rencontrons des problèmes, visiblement de restriction de nombre d'appels/minute sur l'api https://wxs.ign.fr/essentiels/geoportail/geocodage/rest/0.1/reverse qui bloque notre CI
            // throw new RuntimeException("Une erreur est survenue lors de la récupération de l'adresse", 0, $e);
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $addressName = $propertyAccessor->getValue($responseData, '[features][0][properties][label]');

        if (is_null($addressName)) {
            throw new AddressNotFoundException('Aucune adresse trouvée pour les coordonnées spécifiées');
        }

        apcu_store(
            $addresseCacheKey,
            $addressName,
            60 * 60 * 24 * 7 * 3 // on considere qu'au bout de trois semaine on peut vider le cache pour ne pas garder eternellement les info des OT localisé en xy qui risquerait de faire gonfler le cache inutilement.
        );

        return $addressName;
    }

    /**
     * Récupère le nom de la commune à partir des coordonnées en utilisant l'API de géocodage inversé de l'IGN.
     *
     * @param float $latitude  La coordonnée de latitude
     * @param float $longitude La coordonnée de longitude
     *
     * @return string Le nom de la commune correspondant aux coordonnées fournies
     *
     * @throws AddressNotFoundException Si aucune ville n'est trouvée pour les coordonnées spécifiées
     * @throws RuntimeException         Si une erreur survient lors de la récupération de la ville
     */
    public function getCommuneFromCoordinates(float $latitude, float $longitude): string
    {
        $communeCacheKey = "reverseGeocoding/$latitude/$longitude/commune";

        $cachedCommune = apcu_fetch($communeCacheKey);
        if (false !== $cachedCommune) {
            return $cachedCommune;
        }

        $geoCodingApiUrl = $_ENV['GEOPORTAIL_GEOCODING_API_URL'];

        $queryParams = [
            'lat' => $latitude,
            'lon' => $longitude,
            'index' => 'poi',
            'category' => 'commune',
        ];

        $apiUrlWithParams = $geoCodingApiUrl.'?'.http_build_query($queryParams);

        try {
            $response = $this->httpClient->request('GET', $apiUrlWithParams);
            $responseData = $response->toArray();
        } catch (\Exception $e) {
            return '';
            // Note importante : nous commentons cette exception en hotfix car nous rencontrons des problèmes, visiblement de restriction de nombre d'appels/minute sur l'api https://wxs.ign.fr/essentiels/geoportail/geocodage/rest/0.1/reverse qui bloque notre CI
            // throw new RuntimeException("Une erreur est survenue lors de la récupération de l'adresse", 0, $e);
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $commune = $propertyAccessor->getValue($responseData, '[features][0][properties][toponym]');

        if (is_null($commune)) {
            throw new AddressNotFoundException('Aucune commune trouvée pour les coordonnées spécifiées');
        }

        apcu_store(
            $communeCacheKey,
            $commune,
            60 * 60 * 24 * 7 * 3 // on considere qu'au bout de trois semaine on peut vider le cache pour ne pas garder eternellement les info des OT localisé en xy qui risquerait de faire gonfler le cache inutilement.
        );

        return $commune;
    }
}
