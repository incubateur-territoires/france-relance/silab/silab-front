<?php

namespace App\Shared\ReverseGeocoding\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Shared\Exception\BadRequestException;
use App\Shared\Location\Exception\AddressNotFoundException;
use App\Shared\ReverseGeocoding\Entity\Address;
use App\Shared\ReverseGeocoding\Service\ReverseGeocoding;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * @implements ProviderInterface<Address>
 */
class GetReverseProvider implements ProviderInterface
{
    public function __construct(private ReverseGeocoding $reverseGeocoding)
    {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): Address
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $lat = $propertyAccessor->getValue($context, '[filters][lat]');
        if (is_null($lat)) {
            throw new BadRequestException('La latitude est obligatoire (paramètre "lat").');
        }
        $lng = $propertyAccessor->getValue($context, '[filters][lng]');
        if (is_null($lng)) {
            throw new BadRequestException('La longitude est obligatoire (paramètre "lng").');
        }

        $address = '';
        try {
            $address = $this->reverseGeocoding->getAddressFromCoordinates($lat, $lng);
        } catch (AddressNotFoundException $e) {
        }

        return new Address($address);
    }
}
