<?php

namespace App\Shared\Security\Role;

use App\ServiceOffer\ServiceOffer\Repository\ServiceOfferRepository;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class ScopedRoleHierarchy implements ScopedRoleHierarchyInterface
{
    public const SCOPED_ROLE_DECOMPOSITION_REGEX = '/^SERVICEOFFER_(?<serviceOfferId>[^_]+)_ROLE_(?<serviceOfferType>[^_]+)_(?<profileOrActionOnEntity>.+)$/';

    /** @var array<string, list<string>> */
    private array $map;

    /** @var array<mixed> */
    private array $hierarchy;

    /**
     * @param array<string,list<string>> $unscopedHierarchy
     */
    public function __construct(
        #[Autowire(service: ServiceOfferRepository::class)] ServiceOfferRepository $serviceOfferRepository,
        #[Autowire('%security.role_hierarchy.roles%')] array $unscopedHierarchy
    ) {
        $serviceOfferIds =
            array_map(
                function ($serviceOffer) {return $serviceOffer->getId(); },
                $serviceOfferRepository->findAll()
            );

        $hierarchyWithScopes = [];
        foreach ($serviceOfferIds as $serviceOfferId) {
            assert(!is_null($serviceOfferId));
            $hierarchyWithScopes = array_merge_recursive(
                $hierarchyWithScopes,
                $this->scopeHierarchy($unscopedHierarchy, $serviceOfferId)
            );
        }

        /*
         * Amelioration:
         *
         * Si cette issue est résolue un jour : https://github.com/symfony/symfony/issues/57322
         * On peut reprendre l'ancienne implem, et donc hériter de Symfony\Component\Security\Core\Role\RoleHierarchy
         * et ne pas réimplémenter les fonctions getReachableRoleNames et buildRoleMap
         * (ainsi qu eles propriétés de classes inutiles)
         *
         * ils nous suffirait de faire un parent::__construct($hierarchyWithScopes); et c'est good
         */
        $this->hierarchy = $hierarchyWithScopes;
        $this->buildRoleMap();
    }

    /**
     * @param array<string,list<string>> $unscopedHierarchy
     *
     * @return array<string,list<string>>
     */
    private function scopeHierarchy(array $unscopedHierarchy, int $serviceOfferId): array
    {
        $scopedHierarchy = [];
        foreach ($unscopedHierarchy as $unscopedRootRole => $unscopedSubroles) {
            $scopedHierarchy[$this->scopeRole($unscopedRootRole, $serviceOfferId)] = $this->scopeRoles($unscopedSubroles, $serviceOfferId);
        }

        return $scopedHierarchy;
    }

    /**
     * Insère l'id de l'ods correspondant à la requête actuelle dans le rôle, si celui-ci est prévue pour (scopé).
     */
    private function scopeRole(string $unscopedRole, int $serviceOfferId): string
    {
        return preg_replace(self::SCOPED_ROLE_DECOMPOSITION_REGEX, 'SERVICEOFFER_'.$serviceOfferId.'_ROLE_${2}_${3}', $unscopedRole);
    }

    /**
     * @param array<string> $unscopedRoles
     *
     * @return array<string>
     */
    private function scopeRoles(array $unscopedRoles, int $serviceOfferId): array
    {
        return array_map(function (string $unscopedRole) use ($serviceOfferId) {
            return $this->scopeRole($unscopedRole, $serviceOfferId);
        }, $unscopedRoles);
    }

    public function getReachableRoleNames(array $roles): array
    {
        $reachableRoles = $roles;

        foreach ($roles as $role) {
            if (!isset($this->map[$role])) {
                continue;
            }

            foreach ($this->map[$role] as $r) {
                $reachableRoles[] = $r;
            }
        }

        return array_values(array_unique($reachableRoles));
    }

    protected function buildRoleMap(): void
    {
        $this->map = [];
        foreach ($this->hierarchy as $main => $roles) {
            $this->map[$main] = $roles;
            $visited = [];
            $additionalRoles = $roles;
            while ($role = array_pop($additionalRoles)) {
                if (!isset($this->hierarchy[$role])) {
                    continue;
                }

                $visited[] = $role;

                foreach ($this->hierarchy[$role] as $roleToAdd) {
                    $this->map[$main][] = $roleToAdd;
                }

                foreach (array_diff($this->hierarchy[$role], $visited) as $additionalRole) {
                    $additionalRoles[] = $additionalRole;
                }
            }

            $this->map[$main] = array_unique($this->map[$main]);
        }
    }
}
