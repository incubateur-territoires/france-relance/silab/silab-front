<?php

namespace App\Shared\Security\Voter;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\Voter\RoleVoter;

class ScopedRoleVoter extends RoleVoter
{
    public function __construct(protected RequestStack $requestStack)
    {
        // sources : https://stackoverflow.com/questions/43938782/how-get-request-parameter-in-symfony-voter
        // https://symfony.com/doc/current/service_container/request.html
        parent::__construct('SERVICEOFFER_');
    }
}
