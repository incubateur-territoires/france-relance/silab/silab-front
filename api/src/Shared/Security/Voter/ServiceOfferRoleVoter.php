<?php

namespace App\Shared\Security\Voter;

use App\ServiceOffer\ServiceOffer\Service\ServiceOfferContext;
use App\Shared\Security\Role\ScopedRoleHierarchyInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\RoleVoter;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class ServiceOfferRoleVoter extends RoleVoter
{
    protected bool $requestIsOnUnscopedRoute;
    private string $prefix = 'SERVICEOFFER_';

    public function __construct(
        private ScopedRoleHierarchyInterface $scopedRoleHierarchy,
        private ServiceOfferContext $serviceOfferContext
    ) {
        parent::__construct($this->prefix);
    }

    /**
     * @return array<string>
     */
    protected function extractRoles(TokenInterface $token): array
    {
        return $this->scopedRoleHierarchy->getReachableRoleNames($token->getRoleNames());
    }

    /**
     * @param array<string> $attributes
     */
    public function vote(TokenInterface $token, mixed $subject, array $attributes): int
    {
        $result = VoterInterface::ACCESS_ABSTAIN;
        $roles = $this->extractRoles($token);
        foreach ($attributes as $attribute) {
            if (!\is_string($attribute) || !str_starts_with($attribute, $this->prefix)) {
                continue;
            }

            $result = VoterInterface::ACCESS_DENIED;

            $scopedAttribut = substr_replace(
                $attribute,
                $this->prefix.$this->serviceOfferContext->getServiceOfferId().'_',
                0,
                strlen($this->prefix),
            );

            if (in_array($scopedAttribut, $roles)) {
                return VoterInterface::ACCESS_GRANTED;
            }
        }

        return $result;
    }
}
