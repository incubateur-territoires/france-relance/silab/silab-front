<?php

namespace App\Shared\SiilabConfiguration\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Put;
use App\Shared\SiilabConfiguration\Repository\SiilabConfigurationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[Get(
    uriTemplate: '/siilab-configuration',
)]
#[Put(
    uriTemplate: '/siilab-configuration',
    security: "is_granted('ROLE_MODIFIER_SIILAB_CONFIGURATION')",
)]
#[ORM\Table('siilab_configuration')]
#[ORM\Entity(repositoryClass: SiilabConfigurationRepository::class)]
class SiilabConfiguration
{
    public const ONE_AND_ONLY_CONFIGURATION_ID = 1;
    public const GROUP_AFFICHER_CONFIGURATION_APPLICATION = "Afficher la configuration de l'application";
    public const GROUP_MODIFIER_CONFIGURATION_APPLICATION = "Modifier la configuration de l'application";

    #[ORM\Id]
    #[ORM\Column]
    private int $id = self::ONE_AND_ONLY_CONFIGURATION_ID;

    #[ORM\Column(options: ['default' => 46.584545])]
    #[Groups([
        SiilabConfiguration::GROUP_AFFICHER_CONFIGURATION_APPLICATION,
        SiilabConfiguration::GROUP_MODIFIER_CONFIGURATION_APPLICATION,
    ])]
    private ?float $mapCenterLatitude = null;

    #[ORM\Column(options: ['default' => 0.338348])]
    #[Groups([
        SiilabConfiguration::GROUP_AFFICHER_CONFIGURATION_APPLICATION,
        SiilabConfiguration::GROUP_MODIFIER_CONFIGURATION_APPLICATION,
    ])]
    private ?float $mapCenterLongitude = null;

    public function getId(): float
    {
        return $this->id;
    }

    public function getMapCenterLatitude(): ?float
    {
        return $this->mapCenterLatitude;
    }

    public function setMapCenterLatitude(float $mapCenterLatitude): self
    {
        $this->mapCenterLatitude = $mapCenterLatitude;

        return $this;
    }

    public function getMapCenterLongitude(): ?float
    {
        return $this->mapCenterLongitude;
    }

    public function setMapCenterLongitude(float $mapCenterLongitude): self
    {
        $this->mapCenterLongitude = $mapCenterLongitude;

        return $this;
    }
}
