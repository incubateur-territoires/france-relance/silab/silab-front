<?php

namespace App\Shared\SiilabConfiguration\Repository;

use App\Shared\SiilabConfiguration\Entity\SiilabConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SiilabConfiguration>
 *
 * @method SiilabConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method SiilabConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method SiilabConfiguration[]    findAll()
 * @method SiilabConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiilabConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SiilabConfiguration::class);
    }
}
