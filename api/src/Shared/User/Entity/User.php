<?php

namespace App\Shared\User\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use App\Shared\User\Repository\UserRepository;
use App\Shared\User\State\GetCollectionUserByServiceOfferDataHandler;
use App\Shared\User\State\GetCurrentUserDataHandler;
use App\Shared\User\State\PostUserByServiceOfferProcessor;
use App\Shared\User\State\PutUserByServiceOfferProcessor;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[GetCollection(
    security: "is_granted('ROLE_CONSULTER_UTILISATEURS')",
    normalizationContext: ['groups' => [User::GROUP_LISTER_UTILISATEURS]],
    order: ['email' => 'ASC']
)]
#[Get(
    security: "is_granted('ROLE_CONSULTER_UTILISATEURS')",
    normalizationContext: ['groups' => [User::GROUP_AFFICHER_DETAILS_UTILISATEUR]],
)]
#[Patch(
    security: "is_granted('ROLE_CREER_UTILISATEURS')",
    normalizationContext: ['groups' => [User::GROUP_AFFICHER_DETAILS_UTILISATEUR]],
    denormalizationContext: ['groups' => [User::GROUP_MODIFIER_UTILISATEUR]]
)]
#[Post(
    security: "is_granted('ROLE_CREER_UTILISATEURS')",
    normalizationContext: ['groups' => [User::GROUP_AFFICHER_DETAILS_UTILISATEUR]],
    denormalizationContext: ['groups' => [User::GROUP_CREER_UTILISATEUR]]
)]
// routes scopées par offres de service
#[GetCollection(
    uriTemplate: '/service-offers/{serviceOfferId}/admin/users',
    security: "is_granted('SERVICEOFFER_ROLE_SERVICEOFFER_ADMIN_LIST_USERS')",
    normalizationContext: ['groups' => [User::GROUP_LISTER_UTILISATEURS]],
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    provider: GetCollectionUserByServiceOfferDataHandler::class,
    order: ['email' => 'ASC']
)]
#[Get(
    uriTemplate: '/myself',
    normalizationContext: ['groups' => [User::GROUP_AFFICHER_DETAILS_UTILISATEUR_ACTUEL]],
    provider: GetCurrentUserDataHandler::class,
)]
// Question technique : Comment faire un put dans cette route ?
#[Put(
    uriTemplate: '/service-offers/{serviceOfferId}/admin/users/{id}',
    security: "is_granted('SERVICEOFFER_ROLE_SERVICEOFFER_ADMIN_EDIT_USERS')",
    denormalizationContext: ['groups' => [User::GROUP_MODIFIER_UTILISATEUR]],
    read: false,
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    processor: PutUserByServiceOfferProcessor::class
)]
#[Post(
    uriTemplate: '/service-offers/{serviceOfferId}/admin/users',
    security: "is_granted('SERVICEOFFER_ROLE_SERVICEOFFER_ADMIN_CREATE_USERS')",
    normalizationContext: ['groups' => [User::GROUP_AFFICHER_DETAILS_UTILISATEUR]],
    denormalizationContext: ['groups' => [User::GROUP_CREER_UTILISATEUR]],
    read: false,
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    processor: PostUserByServiceOfferProcessor::class
)]
class User implements UserInterface
{
    public const GROUP_AFFICHER_DETAILS_UTILISATEUR_ACTUEL = 'Afficher les détails de l\'utilisateur actuel';
    public const GROUP_AFFICHER_DETAILS_UTILISATEUR = 'Afficher détails d\'un utilisateur';
    public const GROUP_LISTER_UTILISATEURS = 'Lister les utilisateurs';
    public const GROUP_MODIFIER_UTILISATEUR = 'Modifier un utilisateur';
    public const GROUP_CREER_UTILISATEUR = 'Créer un utilisateur';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([
        User::GROUP_AFFICHER_DETAILS_UTILISATEUR,
        User::GROUP_AFFICHER_DETAILS_UTILISATEUR_ACTUEL,
        User::GROUP_LISTER_UTILISATEURS,
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups([
        User::GROUP_AFFICHER_DETAILS_UTILISATEUR,
        User::GROUP_AFFICHER_DETAILS_UTILISATEUR_ACTUEL,
        User::GROUP_LISTER_UTILISATEURS,
        User::GROUP_MODIFIER_UTILISATEUR,
        User::GROUP_CREER_UTILISATEUR,
    ])]
    private ?string $email = null;

    /**
     * Tous les rôles/droits possédés directement ou indirectement par l'utilisateur.
     * Note : ce tableau est à remplir par le dataHandler car il nécessite un accès aux services.
     *
     * @var array<string>
     */
    #[Groups([User::GROUP_AFFICHER_DETAILS_UTILISATEUR_ACTUEL])]
    private array $reachableRoles = [];

    /**
     * @var array<string>
     */
    #[ORM\Column]
    #[Groups([
        User::GROUP_AFFICHER_DETAILS_UTILISATEUR,
        User::GROUP_AFFICHER_DETAILS_UTILISATEUR_ACTUEL,
        User::GROUP_LISTER_UTILISATEURS,
        User::GROUP_MODIFIER_UTILISATEUR,
        User::GROUP_CREER_UTILISATEUR,
    ])]
    private array $roles = [];

    /**
     * @deprecated
     */
    #[ORM\ManyToMany(targetEntity: ServiceOffer::class)]
    private Collection $serviceOffers;

    public function __construct()
    {
        $this->serviceOffers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_values(array_unique($roles));
    }

    /**
     * @return string[]
     */
    public function getRolesByServiceOfferId(int $serviceOfferId): array
    {
        $filteredRoles = [];
        $serviceOfferRoleRegexp = ServiceOffer::getRoleRegexpByServiceOfferId($serviceOfferId);
        foreach ($this->getRoles() as $role) {
            if (preg_match($serviceOfferRoleRegexp, $role)) {
                $filteredRoles[] = $role;
            }
        }

        return $filteredRoles;
    }

    public function addRole(string $role): self
    {
        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * @param array<string> $roles
     *
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = array_values($roles);

        return $this;
    }

    /**
     * @param string[] $rolesToRemove
     */
    public function removeRoles(array $rolesToRemove): self
    {
        $this->roles = array_diff($this->roles, $rolesToRemove);

        return $this;
    }

    public function removeServiceOfferRoles(ServiceOffer $serviceOffer): self
    {
        $this->removeRoles(
            array_map(
                function ($role) {
                    return $role->getValue();
                },
                $serviceOffer->getAvailableRoles()
            )
        );

        return $this;
    }

    /**
     * @param array<string> $reachableRoles
     */
    public function setReachableRoles(array $reachableRoles): self
    {
        $this->reachableRoles = $reachableRoles;

        return $this;
    }

    /**
     * @return array<string>
     */
    public function getReachableRoles(): array
    {
        return $this->reachableRoles;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, ServiceOffer>
     */
    public function getServiceOffers(): Collection
    {
        return $this->serviceOffers;
    }

    public function addServiceOffer(ServiceOffer $serviceOffer): self
    {
        if (!$this->serviceOffers->contains($serviceOffer)) {
            $this->serviceOffers->add($serviceOffer);
        }

        return $this;
    }

    public function removeServiceOffer(ServiceOffer $serviceOffer): self
    {
        $this->serviceOffers->removeElement($serviceOffer);

        return $this;
    }
}
