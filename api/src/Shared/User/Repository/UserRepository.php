<?php

namespace App\Shared\User\Repository;

use App\Shared\User\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function save(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function isUserRegistered(string $email): ?User
    {
        return $this->findOneByEmail($email);
    }

    public function findOneByEmail(string $email): ?User
    {
        return $this->findOneBy(['email' => $email]);
    }

    /**
     * ATTENTION : Cette methode renvoie une copie des utilisateurs avec les rôles altérés, seuls les rôles scopés sur l'ODS sont renvoyés.
     *
     * @param array<string,string>|null $orderBy
     *
     * @return User[]
     */
    public function findScopedClonedUsersByServiceOffer(int $serviceOfferId, ?array $orderBy = null): array
    {
        $users = $this->findBy([], orderBy: $orderBy);

        $filteredUsers = [];
        foreach ($users as $user) {
            $tmpUser = $this->getUserCloneWithOnlyServiceOfferScopedRoles($user, $serviceOfferId);
            $rolesForThisServiceOffer = $tmpUser->getRoles();
            $rolesForThisServiceOffer = array_diff($rolesForThisServiceOffer, ['ROLE_USER']);
            // Ne renvoie que les utilisateurs qui ont des rôles concernant cette ODS
            if (!empty($rolesForThisServiceOffer)) {
                $filteredUsers[] = $tmpUser;
            }
        }

        return $filteredUsers;
    }

    /**
     * ATTENTION : Cette methode renvoie une copie de l'utilisateur avec les rôles altérés, seuls les rôles scopés sur l'ODS sont renvoyés.
     */
    public function findScopedClonedUserByServiceOffer(int $userId, int $serviceOfferId): User
    {
        $user = $this->find($userId);

        return $this->getUserCloneWithOnlyServiceOfferScopedRoles($user, $serviceOfferId);
    }

    public function getUserCloneWithOnlyServiceOfferScopedRoles(User $user, int $serviceOfferId): User
    {
        // on travaille sur une copie de l'utilisateur car on va altérer temporairement les rôles
        $tmpUser = clone $user;
        $filteredRoles = $tmpUser->getRolesByServiceOfferId($serviceOfferId);
        $tmpUser->setRoles($filteredRoles);

        return $tmpUser;
    }

    public function registerNewUser(string $email): User
    {
        $newlyRegisteredUser = self::createUserFromEmail($email);

        $this->save($newlyRegisteredUser, true);

        return $newlyRegisteredUser;
    }

    private static function createUserFromEmail(string $email): User
    {
        $newlyRegisteredUser = new User();
        $newlyRegisteredUser->setEmail($email);

        return $newlyRegisteredUser;
    }

    public function remove(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
