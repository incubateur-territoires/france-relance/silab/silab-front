<?php

namespace App\Shared\User\Security\Azure;

use App\Shared\User\Repository\UserRepository;
use App\Shared\User\Service\ApcuCachedAzure;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\AccessToken\AccessTokenHandlerInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;

/**
 * @see https://symfony.com/doc/current/security/access_token.html#using-the-access-token-authenticator
 */
class AzureAccessTokenHandler implements AccessTokenHandlerInterface
{
    public function __construct(
        private readonly ApcuCachedAzure $azureAuthenticationProvider,
        private readonly UserRepository $userRepository,
        private LoggerInterface $logger
    ) {
    }

    public function getUserBadgeFrom(#[\SensitiveParameter] string $accessToken): UserBadge
    {
        try {
            $tokenClaims = $this->azureAuthenticationProvider->validateAccessToken($accessToken);
        } catch (\Exception $e) {
            throw new BadCredentialsException($e->getMessage());
        }

        $user = $this->userRepository->isUserRegistered($tokenClaims['upn']);
        if (null === $user) {
            $user = $this->userRepository->registerNewUser($tokenClaims['upn']);
        }

        $this->logger->debug($user->getEmail().' est connecté', ['roles' => $user->getRoles(), 'id' => $user->getId()]);

        return new UserBadge($user->getEmail());
    }
}
