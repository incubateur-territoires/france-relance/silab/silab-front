<?php

namespace App\Shared\User\Service;

use TheNetworg\OAuth2\Client\Provider\Azure;

class ApcuCachedAzure extends Azure
{
    public const APCU_CACHE_KEY_PREFIX = 'azure_provider_cache';
    public const CACHE_DEFAULT_TTL_HOURS = 24;

    /** @var array<string,array<mixed>> */
    private array $jwksStore = [];

    /**
     * @param string $tenant
     * @param string $version
     */
    public function getOpenIdConfiguration($tenant, $version): mixed
    {
        if (!is_array($this->openIdConfiguration)) {
            $this->openIdConfiguration = [];
        }
        if (!array_key_exists($tenant, $this->openIdConfiguration)) {
            $this->openIdConfiguration[$tenant] = [];
        }
        if (!array_key_exists($version, $this->openIdConfiguration[$tenant])) {
            $apcuConfigurationCacheKey = static::APCU_CACHE_KEY_PREFIX.'|'.$tenant.'|'.$version;
            $uneConfigurationExisteEnCache = false;
            $this->openIdConfiguration = apcu_fetch($apcuConfigurationCacheKey, $uneConfigurationExisteEnCache);
            if (!$uneConfigurationExisteEnCache) {
                parent::getOpenIdConfiguration($tenant, $version);
                apcu_store($apcuConfigurationCacheKey, $this->openIdConfiguration, static::CACHE_DEFAULT_TTL_HOURS * 60 * 60);
            }
        }

        return $this->openIdConfiguration[$tenant][$version];
    }

    /**
     * @return array<mixed>
     */
    public function getJwtVerificationKeys(): array
    {
        $openIdConfiguration = $this->getOpenIdConfiguration($this->tenant, $this->defaultEndPointVersion);
        $localJwksStoreKey = $openIdConfiguration['jwks_uri'];

        if (!array_key_exists($localJwksStoreKey, $this->jwksStore)) {
            $apcuJwksCacheKey = static::APCU_CACHE_KEY_PREFIX.'|jwks|'.$openIdConfiguration['jwks_uri'];
            $unJWKSExisteEnCache = false;
            $this->jwksStore[$localJwksStoreKey] = apcu_fetch($apcuJwksCacheKey, $unJWKSExisteEnCache);
            if (!$unJWKSExisteEnCache) {
                $this->jwksStore[$localJwksStoreKey] = parent::getJwtVerificationKeys();
                apcu_store($apcuJwksCacheKey, $this->jwksStore[$localJwksStoreKey], static::CACHE_DEFAULT_TTL_HOURS * 60 * 60);
            }
        }

        return $this->jwksStore[$localJwksStoreKey];
    }
}
