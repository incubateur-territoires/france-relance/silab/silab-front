<?php

namespace App\Shared\User\Service;

use App\Shared\User\Entity\User;

interface CurrentUserContextInterface
{
    public function getCurrentUser(): User;
}
