<?php

namespace App\Shared\User\Service;

use App\Shared\User\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

class SymfonyCurrentUserContext implements CurrentUserContextInterface
{
    public function __construct(
        private Security $security
    ) {
    }

    public function getCurrentUser(): User
    {
        $currentUser = $this->security->getUser();
        assert($currentUser instanceof User);

        return $currentUser;
    }
}
