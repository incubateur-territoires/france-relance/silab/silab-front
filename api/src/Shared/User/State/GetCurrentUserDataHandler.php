<?php

namespace App\Shared\User\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Shared\Security\Role\ScopedRoleHierarchyInterface;
use App\Shared\User\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Core\Authentication\Token\SwitchUserToken;

class GetCurrentUserDataHandler implements ProviderInterface
{
    public function __construct(private Security $security, private ScopedRoleHierarchyInterface $scopedRoleHierarchy)
    {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): User
    {
        assert(!$operation instanceof CollectionOperationInterface);

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $returnOriginalUser = 'true' === $propertyAccessor->getValue($context, '[filters][originalUser]');
        $token = $this->security->getToken();
        $isCurrentRequestUsingImpersonation = $token instanceof SwitchUserToken;

        if ($returnOriginalUser && $isCurrentRequestUsingImpersonation) {
            assert($token instanceof SwitchUserToken);
            $user = $token->getOriginalToken()->getUser(); // cf. https://symfony.com/doc/current/security/impersonating_user.html#finding-the-original-user
            assert($user instanceof User);
        } else {
            $user = $this->security->getUser();

            if (!$user instanceof User) {
                return new User();
            }
        }

        $user->setReachableRoles($this->scopedRoleHierarchy->getReachableRoleNames($user->getRoles()));

        return $user;
    }
}
