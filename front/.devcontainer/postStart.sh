#!/bin/sh
git config --global core.editor "code --wait --reuse-window"
git config --global sequence.editor "code --wait --reuse-window"
git config --global core.autocrlf input
npm install
