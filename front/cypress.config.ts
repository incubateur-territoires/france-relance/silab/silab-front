import { defineConfig } from 'cypress'
import codeCoverageTask from '@cypress/code-coverage/task'
import dotenv from 'dotenv'
import cypressSplit from 'cypress-split'

dotenv.config()
dotenv.config({ path: `.env.local`, override: true })

export default defineConfig({
    fixturesFolder: false,
    videoCompression: true,
    env: {
        azureTestClientSecretValue:
            process.env.VITE_AZURE_TEST_CLIENT_SECRET_VALUE,
        azureTestApiScopes: process.env.VITE_AZURE_TEST_API_SCOPES,
        azureTestUserName: process.env.VITE_AZURE_TEST_USERNAME,
        azureTestPassword: process.env.VITE_AZURE_TEST_PASSWORD,
        azureTestDisplayName: process.env.VITE_AZURE_TEST_DISPLAY_NAME,
        unguardedPage: '/mentions-legales',
        split: process.env.split,
    },
    e2e: {
        setupNodeEvents(on, config) {
            codeCoverageTask(on, config)
            cypressSplit(on, config)
            // IMPORTANT: return the config object

            return config
        },
        baseUrl: 'https://localhost:8444',
        testIsolation: true,
        defaultCommandTimeout: 25000,
        pageLoadTimeout: 90000,
    },
})
