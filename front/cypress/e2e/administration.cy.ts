const silabAdminUserListUrl = '/admin/utilisateurs'
const propreteInterventionsListUrl = '/app/interventions/proprete/map'
const propreteAdminUserListUrl =
    '/app/interventions/proprete/admin/utilisateurs'
const magasinGeneralProvisionRequestsUrl = '/app/stock/stock-et-magasin-general'
const magasinGeneralAdminUserListUrl =
    '/app/stock/stock-et-magasin-general/admin/utilisateurs'

describe("En tant qu'administrateur global, je peux visualiser la liste des utilisateurs", () => {
    it("En tant qu'admininistrateur global Siilab, je peux accéder à la liste de tous les agents ayant accès à Siilab", () => {
        cy.visit('/')

        cy.getBySel('btn-silab-admin').should('be.visible').click()
        cy.getBySel('btn-silab-admin-toggle-menu').click()
        cy.getBySel('btn-silab-admin-menu-item-admin/utilisateurs').click()

        cy.url().should('eq', Cypress.config().baseUrl + silabAdminUserListUrl)
    })

    it('Accède à la liste des utilisateurs enregistrés sur Silab', () => {
        cy.visit(silabAdminUserListUrl)
        cy.getBySel('ligne-liste-utilisateur').should('have.length', 7)
    })
})

describe("En tant qu'administrateur global, je peux modifer les rôles d'un utilisateur", () => {
    it('Modifie les rôles du premier utilisateur de la liste', () => {
        cy.visit('/')

        cy.getBySel('btn-silab-admin').should('be.visible').click()
        cy.getBySel('btn-silab-admin-toggle-menu').click()
        cy.getBySel('btn-silab-admin-menu-item-admin/utilisateurs').click()

        const userEmailToModify = 'agent-proprete-et-stock@mail.fr'

        cy.getBySel('ligne-liste-utilisateur')
            .contains('[data-cy="user-email"]', userEmailToModify)
            .click()
        cy.getBySel('roles').click()
        cy.getBySel('save-user-button').click()

        cy.getBySel('snackbar').contains(
            `Utilisateur "${userEmailToModify}" a été mis à jour`
        )
        cy.getBySel('snackbar-button').should('exist').click()
        cy.getBySel('snackbar').should('not.exist')

        cy.getBySel('ligne-liste-utilisateur')
            .contains('[data-cy="user-email"]', userEmailToModify)
            .closest('[data-cy="ligne-liste-utilisateur"]')
            .within(() => {
                cy.getBySel('roles-utilisateur').should(
                    'include.text',
                    'Administrateur global'
                )
            })
    })
})

describe("En tant qu'administrateur de l'offre de service propreté, je peux modifer les informations d'un utilisateur", () => {
    it('Modifie les informations du formulaire du premier utilisateur de la liste', () => {
        cy.visit('/')

        cy.contains(
            '[data-cy^="component-ServiceOfferComponent"]',
            'Propreté'
        ).click()
        cy.getBySel('btn-interventions-admin-user-list')
            .should('be.visible')
            .click()

        // La fonctionnalité n'est pas tester complétement, car le v-select n'est pour le moment pas sélectionnable
    })
})

describe("En tant qu'administrateur de l'offre de service magasin général, je peux modifer les informations d'un utilisateur", () => {
    it('Modifie les informations du formulaire du premier utilisateur de la liste', () => {
        cy.visit('/')

        cy.contains(
            '[data-cy^="component-ServiceOfferComponent"]',
            'Magasin Général'
        ).click()
        cy.getBySel('btn-stock-admin-user-list').should('be.visible').click()

        // La fonctionnalité n'est pas tester complétement, car le v-select n'est pour le moment pas sélectionnable
    })
})

describe("En tant qu'administrateur de l'offre de service propreté, je peux visualiser la liste de mes agents ainsi que leurs rôles", () => {
    it("En tant qu'admininistrateur de l'offre de service propreté, un bouton sur la liste d'intervention me permet d'accéder à la liste des agents correspondants", () => {
        cy.visit(propreteInterventionsListUrl)
        cy.getBySel('btn-interventions-admin-user-list')
            .should('be.visible')
            .click()

        cy.url().should(
            'eq',
            Cypress.config().baseUrl + propreteAdminUserListUrl
        )
    })

    it("Vérifie que sur la page d'administration propreté que tous les agents propreté soient présents avec uniquement les rôles concernés", () => {
        cy.visit(propreteAdminUserListUrl)

        cy.getBySel('user-email')
            .should('include.text', 'agent.proprete@mail.fr')
            .and('include.text', 'agent-proprete-et-stock@mail.fr')
            .and('include.text', 'lecteur-proprete@mail.fr')
    })
})

describe("En tant qu'administrateur de l'offre de service magasin général, je peux visualiser la liste de mes agents ainsi que leurs rôles", () => {
    it("En tant qu'admininistrateur de l'offre de service magasin général, un bouton sur la liste d'intervention me permet d'accéder à la liste des agents correspondants", () => {
        cy.visit(magasinGeneralProvisionRequestsUrl)
        cy.getBySel('btn-stock-admin-user-list').should('be.visible').click()

        cy.url().should(
            'eq',
            Cypress.config().baseUrl + magasinGeneralAdminUserListUrl
        )
    })

    it("Vérifie que sur la page d'administration stock que tous les magasiniers soient présents avec uniquement les rôles concernés", () => {
        cy.visit(magasinGeneralAdminUserListUrl)

        cy.getBySel('user-email')
            .should('include.text', 'magasinier.stock@mail.fr')
            .and('include.text', 'agent-proprete-et-stock@mail.fr')
            .and('include.text', 'lecteur-stock@mail.fr')
    })
})

describe("En tant qu'administrateur global Silab, je peux ajouter un utilisateur", () => {
    it("Accède au formulaire de création d'utilisateur en cliquant sur le bouton d'ajout d'utilisateur présent sur la liste d'utilisateur", () => {
        cy.visit('/')
        cy.getBySel('btn-silab-admin').should('be.visible').click()
        cy.getBySel('btn-silab-admin-toggle-menu').click()
        cy.getBySel('btn-silab-admin-menu-item-admin/utilisateurs').click()

        cy.getBySel('save-user-button').should('be.visible').click()

        cy.getBySel('save-user-button').should('be.disabled')

        const userToCreateEmail = 'utilisateur-silab@mail.fr'

        cy.getBySel('email').type(userToCreateEmail)
        cy.getBySel('roles').click()

        cy.getBySel('save-user-button').should('be.enabled').click()

        cy.getBySel('snackbar').contains(
            `Utilisateur "${userToCreateEmail}" créé`
        )
        cy.getBySel('snackbar-button').should('exist').click()
        cy.getBySel('snackbar').should('not.exist')

        cy.getBySel('ligne-liste-utilisateur')
            .contains('[data-cy="user-email"]', userToCreateEmail)
            .closest('[data-cy="ligne-liste-utilisateur"]')
            .within(() => {
                cy.getBySel('roles-utilisateur').should(
                    'include.text',
                    'Administrateur global'
                )
            })
    })
})

describe("En tant qu'administrateur de l'offre de service propreté, je peux ajouter un utilisateur", () => {
    it("Accède au formulaire de création d'utilisateur en cliquant sur le bouton d'ajout d'utilisateur présent sur la liste d'utilisateur", () => {
        cy.visit('/')

        cy.contains(
            '[data-cy^="component-ServiceOfferComponent"]',
            'Propreté'
        ).click()
        cy.getBySel('btn-interventions-admin-user-list')
            .should('be.visible')
            .click()
        cy.getBySel('save-user-button').should('be.visible').click()

        cy.getBySel('register-user-button').should('be.disabled')

        cy.getBySel('email').type('utilisateur-proprete@mail.fr')
        cy.getBySel('register-user-button').should('be.disabled')
        cy.getBySel('roles').first().click()

        cy.getBySel('register-user-button').should('not.be.disabled').click()

        cy.getBySel('snackbar').contains(
            'Utilisateur "utilisateur-proprete@mail.fr" créé'
        )
        cy.getBySel('snackbar-button').should('exist').click()
        cy.getBySel('snackbar').should('not.exist')

        cy.getBySel('user-email').contains('utilisateur-proprete@mail.fr')
        cy.getBySel('user-roles').contains('Superviseur')
    })
})

describe("En tant qu'administrateur de l'offre de service propreté, je peux retirer un utilisateur lié à mon offre de service", () => {
    it("Accède à la liste d'utilisateurs et retire le premier utilisateur présent sur la liste", () => {
        cy.visit('/')

        cy.contains(
            '[data-cy^="component-ServiceOfferComponent"]',
            'Propreté'
        ).click()

        cy.getBySel('btn-interventions-admin-user-list')
            .should('be.visible')
            .click()

        cy.getBySel('remove-user-button').first().click()
        cy.getBySel('confirm-remove-user-button').click()

        cy.getBySel('snackbar').contains(
            'L\'utilisateur "agent-proprete-et-stock@mail.fr" a été retiré de l\'offre de service Propreté'
        )
        cy.getBySel('snackbar-button').should('exist').click()
        cy.getBySel('snackbar').should('not.exist')

        cy.getBySel('user-email')
            .contains('agent.proprete-et-stock@mail.fr')
            .should('not.exist')
    })
})

describe("En tant qu'administrateur global, je peux rechercher un utilisateur selon son email", () => {
    it("Accède à la liste d'utilisateurs et recherche un utilisateur", () => {
        cy.visit('/admin/utilisateurs')

        cy.getBySel('button-open-search-user').click()
        // Vérifie que la recherche ne sois pas sensible à la casse
        cy.getBySel('field-search-user').type('Visiteur')

        cy.getBySel('ligne-liste-utilisateur').each(($user) => {
            cy.wrap($user)
                .find('[data-cy=user-email]')
                .should('include.text', 'visiteur')
        })
    })
})

describe("En tant qu'administrateur de l'offre de service Propreté, je peux rechercher un agent selon son email", () => {
    it("Accède à la liste d'utilisateurs et recherche un utilisateur", () => {
        cy.visit(propreteAdminUserListUrl)

        cy.getBySel('button-open-search-user').click()
        // Vérifie que la recherche ne sois pas sensible à la casse
        cy.getBySel('field-search-user').type('Proprete')

        cy.getBySel('ligne-liste-utilisateur').each(($user) => {
            cy.wrap($user)
                .find('[data-cy=user-email]')
                .should('include.text', 'proprete')
        })
    })
})

describe("En tant qu'administrateur global, je peux modifier les coordonnées des cartes", () => {
    it('Accède à la configuration et modifie la latitude et la longitude', () => {
        cy.visit('/admin/general')

        //Il y a un bug avec cypress, il est pas possible de tester avec un nombre décimal
        //Le navigateur de test étant en anglais il faudrait utiliser un . mais quand on le fait le caractère n'apparait pas dans l'input
        cy.getBySel('global-latitude-map')
            .find('input')
            .type('{selectAll}504645640')
        cy.getBySel('global-longitude-map')
            .find('input')
            .type('{selectAll}30498640')
        cy.getBySel('submit').click()

        cy.getBySel('snackbar').contains('Configuration modifiée')
        cy.getBySel('snackbar-button').should('exist').click()
        cy.getBySel('snackbar').should('not.exist')

        cy.getBySel('global-latitude-map')
            .find('input')
            .should('have.value', '504645640')
        cy.getBySel('global-longitude-map')
            .find('input')
            .should('have.value', '30498640')
    })
})
