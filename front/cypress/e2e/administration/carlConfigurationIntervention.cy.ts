const carlConfigurationInterventionUrl =
    '/admin/configuration-carl-intervention'
const createCarlConfigurationInterventionUrl =
    '/admin/configuration-carl-intervention/creer'

describe("En tant qu'administrateur global, je peux créer une configurations Carl, la modifier et la supprimer", () => {
    it('peut accéder au crud des ODS carlConfigurationIntervention', () => {
        cy.visit('/')
        cy.getBySel('btn-silab-admin').click()
        cy.getBySel('btn-silab-admin-toggle-menu').click()
        cy.getBySel(
            'btn-silab-admin-menu-item-admin/configuration-carl-intervention'
        ).click()

        cy.url().should(
            'eq',
            Cypress.config().baseUrl + carlConfigurationInterventionUrl
        )

        cy.getBySel('create-carl-configuration-intervention').click()

        cy.url().should(
            'eq',
            Cypress.config().baseUrl + createCarlConfigurationInterventionUrl
        )
    })
    it('peut créer une configuration carl', () => {
        createCarlConfigurationIntervention()
        cy.getBySel('snackbar').contains('Configuration Carl créé')
        cy.getBySel('carl-configuration-intervention-interventions')
            .contains('Interventions')
            .click()
        assertCarlConfigurationInterventionFields()
        //problème : cy.go('back') ne fonctionne pas sur gitlab (ça marche en local). Sur le screen cypress de gitlab, on reste sur le formulaire
        cy.getBySel('app-name').click()
        cy.getBySel('btn-silab-admin').click()
        cy.getBySel('btn-silab-admin-toggle-menu').click()
        cy.getBySel('btn-silab-admin-menu-item-admin/ods-intervention').click()
        cy.getBySel('create-intervention-service-offer').click()
        cy.getBySel('gmaoConfiguration').click()
        cy.contains('Interventions')
    })
    it('possède des instructions claires sur le fonctionnement du champ direction', () => {
        cy.visit(createCarlConfigurationInterventionUrl)
        cy.getBySel('directions')
            .find('.v-input__details')
            .should(
                'include.text',
                'Seules les OT qui possède une de ces valeur dans le champ cswo_wo.supervisor_id seront récupérées dans la vue carte'
            )
        cy.getBySel('directions').find('.v-field__append-inner i').click()
        cy.get('code').should(
            'include.text',
            '180f2f264b3-35ll3\n587f2f264b3-35f6l'
        )
    })
    it('peut modifier une configuration carl', () => {
        createCarlConfigurationIntervention()
        cy.getBySel('carl-configuration-intervention-interventions').click()
        cy.getBySel('carl-config-notes').type(
            "Connecteur pour les environnements d'intégration"
        )
        cy.getBySel('carl-config-title')
            .type('{selectall}{backspace}')
            .type('Interventions modifiées')
        cy.getBySel('submit').click()
        cy.getBySel('snackbar').contains('Configuration Carl modifiée')
        cy.getBySel(
            'carl-configuration-intervention-interventions-modifiées'
        ).contains('Interventions modifiées')
    })
    it('peut supprimer une configuration carl', () => {
        createCarlConfigurationIntervention()
        cy.getBySel('carl-configuration-intervention-interventions').click()
        cy.getBySel('supprimer-carlconfigurationintervention').click()
        cy.getBySel('confirm-delete-carlconfigurationintervention').click()
        cy.getBySel('snackbar').contains('Configuration Carl supprimée')
        cy.getBySel('carl-configuration-interventions').should('not.exist')
    })
    it('Peut dupliquer une configuration carl', () => {
        createCarlConfigurationIntervention()
        cy.getBySel('snackbar').contains('Configuration Carl créé')
        cy.getBySel('carl-configuration-intervention-interventions')
            .contains('Interventions')
            .click()
        assertCarlConfigurationInterventionFields()
        cy.getBySel('dupliquer-carl-configuration-intervention').click()
        assertCarlConfigurationInterventionFields()
        cy.getBySel('carl-config-title')
            .type('{selectall}{backspace}')
            .type('Copie - Interventions')
        cy.getBySel('submit').click()
        cy.getBySel('snackbar').contains('Configuration Carl créé')
    })
})

const notes = 'Connecteur pour les envrionnements de test'
const title = 'Interventions'
const directions = 'DAEEP_PROPRETE'
const carlClient = 'Carl TEST'
const natures =
    'corbeille{enter}' + '{backspace}'.repeat(9) + 'balayeuse{enter}'
const interventionCostCenterIdsFilter =
    'AMG_EPI{enter}' + '{backspace}'.repeat(7) + 'AMG_ENT{enter}'
const costCenterIdForInterventionCreation = 'AMG_EPI{enter}'
const carlAttributesPresetForInterventionCreation = `{
    "xtraTxt06": "PROPRETE"
}`
const carlAttributesPresetForDemandeInterventionCreation = `{
    "xtraTxt06": "IMMOBILIER",
    "poiXtraTxt14": "POITIERS",
    "xtraTxt01": "DI_GEN"
}`
const photoAvantInterventionDoctypeId = 'GIUiUHGIFDH54FG'
const photoApresInterventionDoctypeId = 'GIUiFDH54FG'
const woViewUrlSubPath = '/wo/dsgrd'
const mrViewUrlSubPath = '/mr/sdfwx'
const boxViewUrlSubPath = '/equipement/box/seg'
const materialViewUrlSubPath = '/equipement/material/sqn'
const includeEquipementsChildrenWithType = 'zone'

function createCarlConfigurationIntervention() {
    cy.visit(createCarlConfigurationInterventionUrl)

    cy.getBySel('carl-config-notes').type(notes)
    cy.getBySel('carl-config-title').type(title)
    cy.getBySel('directions').type(directions)
    cy.getBySel('carlClient').type(`${carlClient}{enter}{enter}`).click()
    cy.getBySel('natures').type(natures).click()
    cy.getBySel('directions').click()
    cy.getBySel('interventionCostCenterIdsFilter')
        .type(interventionCostCenterIdsFilter)
        .click()
    cy.getBySel('directions').click()
    cy.getBySel('costCenterIdForInterventionCreation')
        .type(costCenterIdForInterventionCreation)
        .click()
    cy.getBySel('directions').click()
    cy.getBySel('carlAttributesPresetForInterventionCreation')
        .clear()
        .type(carlAttributesPresetForInterventionCreation)
    cy.getBySel('carlAttributesPresetForDemandeInterventionCreation')
        .clear()
        .type(carlAttributesPresetForDemandeInterventionCreation)
    cy.getBySel('carlRelationshipsPresetForDemandeInterventionCreation').should(
        'be.visible'
    )
    cy.getBySel('photoAvantInterventionDoctypeId')
        .clear()
        .type(photoAvantInterventionDoctypeId)
    cy.getBySel('photoApresInterventionDoctypeId').type(
        photoApresInterventionDoctypeId
    )
    cy.getBySel('woViewUrlSubPath').type(woViewUrlSubPath)
    cy.getBySel('mrViewUrlSubPath').type(mrViewUrlSubPath)
    cy.getBySel('boxViewUrlSubPath').type(boxViewUrlSubPath)
    cy.getBySel('materialViewUrlSubPath').type(materialViewUrlSubPath)
    cy.getBySel('includeEquipementsChildrenWithType').type(
        includeEquipementsChildrenWithType
    )

    cy.getBySel('submit').click()
}

function assertCarlConfigurationInterventionFields() {
    cy.getBySel('carl-config-notes')
        .find('textarea')
        .should('have.value', notes)
    cy.getBySel('carl-config-title').find('input').should('have.value', title)
    cy.getBySel('directions').find('textarea').should('have.value', directions)
    cy.getBySel('carlClient').find('input').should('have.value', carlClient)
    cy.getBySel('carlAttributesPresetForInterventionCreation')
        .find('textarea')
        .should('have.value', '{"xtraTxt06":"PROPRETE"}')
    cy.getBySel('carlAttributesPresetForDemandeInterventionCreation')
        .find('textarea')
        .should(
            'have.value',
            '{"xtraTxt06":"IMMOBILIER","poiXtraTxt14":"POITIERS","xtraTxt01":"DI_GEN"}'
        )
    cy.getBySel('photoAvantInterventionDoctypeId')
        .find('input')
        .should('have.value', photoAvantInterventionDoctypeId)
    cy.getBySel('photoApresInterventionDoctypeId')
        .find('input')
        .should('have.value', photoApresInterventionDoctypeId)
    cy.getBySel('woViewUrlSubPath')
        .find('input')
        .should('have.value', woViewUrlSubPath)
    cy.getBySel('mrViewUrlSubPath')
        .find('input')
        .should('have.value', mrViewUrlSubPath)
    cy.getBySel('boxViewUrlSubPath')
        .find('input')
        .should('have.value', boxViewUrlSubPath)
    cy.getBySel('materialViewUrlSubPath')
        .find('input')
        .should('have.value', materialViewUrlSubPath)
    cy.getBySel('includeEquipementsChildrenWithType')
        .find('textarea')
        .should('have.value', includeEquipementsChildrenWithType)
}
