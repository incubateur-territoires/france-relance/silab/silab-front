const silabAdminMessageListUrl = '/admin/messages'

describe("En tant qu'administrateur global, je peux voir la liste des messages créés, créer un nouveau message et le supprimer", () => {
    it("Un bouton sur la page d'administration me permet d'accéder à la liste de tous les messages d'information silab", () => {
        cy.viewport(1920, 1080)
        cy.visit('/admin/utilisateurs')

        cy.getBySel('btn-silab-admin-menu-item-admin/message')
            .should('be.visible')
            .click()

        cy.url().should(
            'eq',
            Cypress.config().baseUrl + silabAdminMessageListUrl
        )
    })

    it('Peut créer un message', () => {
        cy.visit(silabAdminMessageListUrl)

        cy.getBySel('create-message-button').should('be.visible').click()

        cy.getBySel('message-title')
            .should('be.visible')
            .type('Message de test')
        cy.getBySel('message-body')
            .should('be.visible')
            .type("Contenu d'un message de test assez long mais pas trop")
        cy.getBySel('message-color').should('be.visible')
        cy.getBySel('date-de-debut')
            .should('be.visible')
            .type(new Date().toISOString().split('T')[0])
        cy.getBySel('date-de-fin')
            .should('be.visible')
            .type(
                new Date(Date.now() + 3 * 24 * 60 * 60 * 1000)
                    .toISOString()
                    .split('T')[0]
            )

        cy.getBySel('register-message-button').click()

        cy.getBySel('message').contains('Message de test').should('be.visible')
    })

    it('Peut modifier un message', () => {
        cy.visit(silabAdminMessageListUrl)

        cy.getBySel('create-message-button').should('be.visible').click()

        cy.getBySel('message-title')
            .should('be.visible')
            .type('Message de test')
        cy.getBySel('message-body')
            .should('be.visible')
            .type("Contenu d'un message de test assez long mais pas trop")
        cy.getBySel('message-color').should('be.visible')
        cy.getBySel('date-de-debut')
            .should('be.visible')
            .type(new Date().toISOString().split('T')[0])
        cy.getBySel('date-de-fin')
            .should('be.visible')
            .type(
                new Date(Date.now() + 3 * 24 * 60 * 60 * 1000)
                    .toISOString()
                    .split('T')[0]
            )

        cy.getBySel('register-message-button').click()

        cy.getBySel('message')
            .contains('Message de test')
            .should('be.visible')
            .click()

        cy.getBySel('message-title').should('be.visible').type(' modifié')
        cy.getBySel('message-body').should('be.visible').type(' modifié')
        cy.getBySel('message-color').should('be.visible')
        cy.getBySel('date-de-debut')
            .should('be.visible')
            .type(
                new Date(Date.now() + 1 * 24 * 60 * 60 * 1000)
                    .toISOString()
                    .split('T')[0]
            )
        cy.getBySel('date-de-fin')
            .should('be.visible')
            .type(
                new Date(Date.now() + 2 * 24 * 60 * 60 * 1000)
                    .toISOString()
                    .split('T')[0]
            )

        cy.getBySel('register-message-button').click()

        cy.getBySel('message')
            .contains('Message de test modifié')
            .should('be.visible')
    })

    it('Peut supprimer un message', () => {
        cy.visit(silabAdminMessageListUrl)

        cy.getBySel('create-message-button').click()

        cy.getBySel('message-title').type('Message de test')
        cy.getBySel('message-body').type(
            "Contenu d'un message de test assez long mais pas trop"
        )
        cy.getBySel('date-de-debut').type(
            new Date().toISOString().split('T')[0]
        )
        cy.getBySel('date-de-fin').type(
            new Date(Date.now() + 3 * 24 * 60 * 60 * 1000)
                .toISOString()
                .split('T')[0]
        )
        cy.getBySel('register-message-button').click()

        cy.getBySel('message-delete-btn').click()

        cy.getBySel('message').should('not.exist')
    })

    it('Peut supprimer une date après saisie', () => {
        cy.visit(silabAdminMessageListUrl + '/creer')

        cy.getBySel('date-de-debut').type(
            new Date().toISOString().split('T')[0]
        )
        cy.getBySel('date-de-fin').type(
            new Date(Date.now() + 3 * 24 * 60 * 60 * 1000)
                .toISOString()
                .split('T')[0]
        )
        cy.getBySel('date-de-debut').within(() => {
            cy.get('.v-field__clearable').click('center')
        })
        cy.getBySel('date-de-fin').within(() => {
            cy.get('.v-field__clearable').click('center')
        })

        cy.getBySel('date-de-debut').within(() => {
            cy.get('.v-field__input').should('have.value', '')
        })
        cy.getBySel('date-de-fin').within(() => {
            cy.get('.v-field__input').should('have.value', '')
        })
    })
})

describe("En tant qu'utilisateur, je peux voir un message sur la page d'accueil si on est dans la période de publication", () => {
    it('Peut voir le message dans la période de publication', () => {
        cy.visit(silabAdminMessageListUrl)

        cy.getBySel('create-message-button').click()

        cy.getBySel('message-title').type('Message de test')
        cy.getBySel('message-body').type(
            "Contenu d'un message de test assez long mais pas trop"
        )
        cy.getBySel('date-de-debut').type(
            new Date().toISOString().split('T')[0]
        )
        cy.getBySel('date-de-fin').type(
            new Date(Date.now() + 3 * 24 * 60 * 60 * 1000)
                .toISOString()
                .split('T')[0]
        )
        cy.getBySel('register-message-button').click()

        cy.wait(2000)

        cy.getBySel('app-name').click()

        cy.getBySel('alert-message')
            .contains('Message de test')
            .should('be.visible')
    })
    it('Ne peut pas voir le message or période de publication', () => {
        cy.visit(silabAdminMessageListUrl)

        cy.getBySel('create-message-button').click()

        cy.getBySel('message-title').type('Message de test')
        cy.getBySel('message-body').type(
            "Contenu d'un message de test assez long mais pas trop"
        )
        cy.getBySel('date-de-debut').type(
            new Date(Date.now() + 2 * 24 * 60 * 60 * 1000)
                .toISOString()
                .split('T')[0]
        )
        cy.getBySel('date-de-fin').type(
            new Date(Date.now() + 3 * 24 * 60 * 60 * 1000)
                .toISOString()
                .split('T')[0]
        )
        cy.getBySel('register-message-button').click()

        cy.wait(2000)

        cy.getBySel('app-name').click()

        cy.getBySel('alert-message').should('not.exist')
    })
})
