import { scrollTotheEndOfInfiniteScroll } from '../interventionServiceOffer/helper'

describe("En tant qu'administrateur global, créer une offre de service de type intervention", () => {
    const interventionServiceOfferUrl = '/admin/ods-intervention'

    it('peut accéder au crud des ODS intervention', () => {
        cy.visit('/')
        cy.getBySel('btn-silab-admin').should('be.visible').click()
        cy.getBySel('btn-silab-admin-toggle-menu').should('be.visible').click()
        cy.getBySel('btn-silab-admin-menu-item-admin/ods-intervention')
            .should('be.visible')
            .click()

        cy.url().should(
            'eq',
            Cypress.config().baseUrl + interventionServiceOfferUrl
        )
    })
    it('peut voir la liste des ODS intervention', () => {
        cy.visit(interventionServiceOfferUrl)

        cy.getBySel('listItem-Component-proprete').should('be.visible')
        cy.getBySel('listItem-Component-ecoles-poitiers').should('be.visible')
        cy.getBySel('listItem-Component-step').should('be.visible')
    })
    it('peut créer une offre de service intervention', () => {
        cy.visit(interventionServiceOfferUrl)

        cy.getBySel('create-intervention-service-offer').click()
        cy.getBySel('service-offer-notes').type(
            'Personne de contact pour les Ecoles : Jean Doux'
        )
        cy.getBySel('image').type('https://picsum.photos/200/300')
        cy.getBySel('service-offer-title').type('Ecole')
        cy.getBySel('service-offer-description').type(
            'Offre de service servant aux concierge des écoles'
        )
        cy.getBySel('environnement').click()
        cy.get('.v-select__content .v-list-item').first().click()
        cy.getBySel('gmaoConfiguration').click()
        cy.get('.v-select__content .v-list-item').first().click()
        cy.getBySel('select-equipement').type('Ancienne Comédie')
        cy.get('.v-autocomplete__content .v-list-item').first().click()
        cy.getBySel('service-offer-mode-date-jour').click()
        cy.getBySel('service-offer-heure-par-defaut-en-minutes').type('09:00')
        cy.getBySel('submit').click()

        cy.getBySel('snackbar').contains('Offre de service "Ecole" créée')
        cy.getBySel('listItem-Component-1').contains('Ecole')
        cy.getBySel('listItem-Component-1')
            .getBySel('illustration')
            .should('exist')
        cy.getBySel('listItem-Component-1').click()
        cy.getBySel('service-offer-description')
            .find('textarea')
            .should(
                'have.value',
                'Offre de service servant aux concierge des écoles'
            )
        cy.getBySel('select-equipement').should('contain', 'Ancienne Comédie')
        cy.get('[data-cy="service-offer-mode-date-jour"] input').should(
            'be.checked'
        )
        cy.get(
            '[data-cy="service-offer-heure-par-defaut-en-minutes"] input'
        ).should('have.value', '09:00')
    })
    it('peut modifier une offre de service intervention', () => {
        cy.visit(interventionServiceOfferUrl)

        cy.getBySel('listItem-Component-ecoles-poitiers').click()
        const notes = 'Personne de contact pour les Ecoles : John Doe'
        cy.getBySel('service-offer-notes').type(notes)
        cy.getBySel('image')
            .type('{selectall}{backspace}')
            .type('https://picsum.photos/200/300')
        cy.getBySel('service-offer-title')
            .type('{selectall}{backspace}')
            .type('ODS de Test modifié')
        cy.getBySel('select-equipement')
            .click()
            .within(() => {
                cy.get('.v-field__clearable').within(() => {
                    cy.get('.v-icon--clickable').click()
                })
            })
        cy.getBySel('select-equipement').type('Ecole George moustachequi')
        cy.get('.v-autocomplete__content .v-list-item').first().click()
        cy.getBySel('gestion-recompletement').find('input').click()
        cy.getBySel('submit').click()
        cy.getBySel('snackbar').contains(
            'Offre de service "ODS de Test modifié" modifiée'
        )
        cy.getBySel('listItem-Component-ecoles-poitiers')
            .contains('ODS de Test modifié')
            .click()

        cy.getBySel('service-offer-notes')
            .find('textarea')
            .should('have.value', notes)

        cy.getBySel('image').within(() => {
            cy.get('.v-field__input').should(
                'have.value',
                'https://picsum.photos/200/300'
            )
        })
        cy.getBySel('service-offer-title').within(() => {
            cy.get('.v-field__input').should(
                'have.value',
                'ODS de Test modifié'
            )
        })
        cy.getBySel('select-equipement').should(
            'contain',
            'Ecole George moustachequi'
        )
        cy.getBySel('gestion-recompletement').find('input').should('be.checked')
    })
    it('peut supprimer une offre de service intervention', () => {
        cy.visit(interventionServiceOfferUrl)

        cy.getBySel('listItem-Component-ecoles-poitiers').click()
        // POC: wait for skelleton to be replaced
        cy.get('.v-skeleton-loader').should('exist') // a way to wait for new page to load not necessary after cy.visit()
        cy.get('.v-skeleton-loader').should('not.exist')
        // EndOfPOC
        cy.getBySel('supprimer-ods-intervention').click()
        cy.getBySel('confirm-delete-intervention-service-offer').click()
        cy.getBySel('snackbar').contains(
            'Offre de service "Ecoles de Poitiers" supprimée'
        )

        cy.getBySel('listItem-Component-ecoles-poitiers').should('not.exist')
    })
    it('Peut dupliquer une offre de service intervention', () => {
        const interventionServiceOfferImage =
            'https://loremflickr.com/320/240/schoolbuilding?lock=8'
        const interventionServiceOfferTitle = 'Ecoles de Poitiers'
        const interventionServiceOfferEnvironnement = 'INTEG'
        const interventionServiceOfferAvailableEquipementsLabels = [
            'EE des petits canailloux',
            'Ecole Maternelle F. Pignon',
            'Ecole Maxime Renard',
            'Ecole George moustachequi',
        ]

        function interventionServiceOfferToDuplicate(
            image: string,
            title: string,
            environnement: string,
            availableEquipementsLabels: string[]
        ) {
            cy.getBySel('image').within(() => {
                cy.get('.v-field__input').should('have.value', image)
            })
            cy.getBySel('service-offer-title').within(() => {
                cy.get('.v-field__input').should('have.value', title)
            })
            cy.getBySel('environnement').contains(environnement)
            cy.getBySel('gmaoConfiguration').contains(title)
            cy.getBySel('select-equipement').within(() => {
                cy.get('.v-field__input').within(() => {
                    availableEquipementsLabels.forEach((equipement) => {
                        cy.get('.v-chip').should('contain', equipement)
                    })
                })
            })
        }

        cy.visit(interventionServiceOfferUrl)
        cy.getBySel('listItem-Component-ecoles-poitiers').click()

        interventionServiceOfferToDuplicate(
            interventionServiceOfferImage,
            interventionServiceOfferTitle,
            interventionServiceOfferEnvironnement,
            interventionServiceOfferAvailableEquipementsLabels
        )

        cy.getBySel('dupliquer-ods-intervention').click()

        cy.getBySel('submit').should('be.disabled')
        cy.getBySel('submit').should('not.be.disabled')

        interventionServiceOfferToDuplicate(
            interventionServiceOfferImage,
            interventionServiceOfferTitle,
            interventionServiceOfferEnvironnement,
            interventionServiceOfferAvailableEquipementsLabels
        )
        cy.getBySel('service-offer-title')
            .type('{selectall}{backspace}')
            .type('Copie - Ecoles de Poitiers')

        cy.getBySel('submit').click()
        cy.getBySel('snackbar').contains(
            'Offre de service "Copie - Ecoles de Poitiers" créée'
        )
    })
})
describe("En tant qu'administrateur global, je peux modifier le formulaire de soldage", () => {
    const stepServiceOfferSoldageURl =
        'app/interventions/step/interventions/ot-proprete-6/solder'
    it.skip("peut passer la saisie de la date de début d'une opération en automatique", () => {
        cy.visit(stepServiceOfferSoldageURl)
        cy.getBySel('date-de-début-input-datetime').should('exist')
        cy.getBySel('go-to-ods').click()
        cy.getBySel('app-name').click()
        cy.getBySel('btn-silab-admin').click()
        cy.getBySel('btn-silab-admin-toggle-menu').click()
        cy.getBySel('btn-silab-admin-menu-item-admin/ods-intervention').click()
        cy.getBySel('listItem-Component-step').click()
        cy.getBySel('service-offer-mode-date-automatique').click()
        cy.getBySel('submit').click()
        cy.getBySel('app-name').click()
        cy.waitMainLoader()
        cy.getBySel('component-ServiceOfferComponent-step').click()
        cy.url().should(
            'eq',
            Cypress.config().baseUrl + '/app/interventions/step'
        )
        scrollTotheEndOfInfiniteScroll()
        cy.getBySel('listItem-Component-ot-proprete-6').click()
        cy.getBySel('traiter-intervention-button').click()
        cy.getBySel('date-de-début-input-datetime').should('not.exist')
    })
})

describe("En tant qu'administrateur global, je peux ajouter un modèle de description du formulaire de création d'une DI", () => {
    it("peut ajouter un modèle de description du formulaire de création d'une DI", () => {
        cy.visit('/admin/ods-intervention')
        cy.getBySel('listItem-Component-dpb').click()
        cy.getBySel('modeleDescriptionTextarea').type(
            '{selectall}{backspace}Message de test'
        )
        cy.getBySel('submit').click()
        cy.getBySel('snackbar').contains(
            'Offre de service "DPB - Supervision" modifiée'
        )
        cy.getBySel('go-to-home-button').click()
        cy.getBySel('component-ServiceOfferComponent-dpb').click()
        cy.getBySel('tab-équipements').click()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
        })
        cy.getBySel('create-demande-intervention').click()
        cy.getBySel('description')
            .find('textarea')
            .should('have.value', 'Message de test')
    })
})

describe("En tant qu'administrateur global, je peux activer la gestion de saisie des dates souhaitées sur les formulaires des DIs", () => {
    it("peut activer la gestion de saisie des dates souhaitées sur les formulaires d'une DI", () => {
        //Vérifier que la fonctionnalité n'est pas activee par defaut
        cy.visit('/admin/ods-intervention')
        cy.getBySel('listItem-Component-dpb').click()
        cy.getBySel('gestion-date-souhaitee-demande-intervention')
            .find('input')
            .should('not.be.checked')
        cy.getBySel('go-to-home-button').click()
        cy.getBySel('component-ServiceOfferComponent-dpb').click()
        cy.getBySel('tab-équipements').click()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
        })
        cy.getBySel('create-demande-intervention').click()
        cy.getBySel('date-de-debut').should('not.exist')
        cy.getBySel('date-de-fin').should('not.exist')

        //Vérifier que l'activation fonctionne
        cy.visit('/admin/ods-intervention')
        cy.getBySel('listItem-Component-dpb').click()
        cy.getBySel('gestion-date-souhaitee-demande-intervention').click()
        cy.getBySel('gestion-date-souhaitee-demande-intervention')
            .find('input')
            .should('be.checked')
        cy.getBySel('submit').click()
        cy.getBySel('snackbar').contains(
            'Offre de service "DPB - Supervision" modifiée'
        )
        cy.getBySel('go-to-home-button').click()
        cy.getBySel('component-ServiceOfferComponent-dpb').click()
        cy.getBySel('tab-équipements').click()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
        })
        cy.getBySel('create-demande-intervention').click()
        cy.getBySel('date-de-debut').scrollIntoView().should('be.visible')
        cy.getBySel('date-de-fin').scrollIntoView().should('be.visible')
    })

    it('peut créer une DI avec des dates de début et de fin souhaité', () => {
        const dateDeDebutSouhaitee = new Date().toISOString().split('T')[0]
        const dateDeFinSouhaitee = new Date(
            Date.now() + 3 * 24 * 60 * 60 * 1000
        )
            .toISOString()
            .split('T')[0]

        //Activer la fonctionnalité
        cy.visit('/admin/ods-intervention')
        cy.getBySel('listItem-Component-dpb').click()
        cy.getBySel('gestion-date-souhaitee-demande-intervention').click()
        cy.getBySel('gestion-date-souhaitee-demande-intervention')
            .find('input')
            .should('be.checked')
        cy.getBySel('submit').click()
        cy.getBySel('snackbar').contains(
            'Offre de service "DPB - Supervision" modifiée'
        )

        //Créer une DI avec une date de début et de fin souhaitée
        cy.getBySel('go-to-home-button').click()
        cy.getBySel('component-ServiceOfferComponent-dpb').click()
        cy.getBySel('tab-équipements').click()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
        })
        cy.getBySel('create-demande-intervention').click()
        cy.getBySel('titre-de-la-demande').type('Sanitaires cassés')
        cy.getBySel('select-equipement').click()
        cy.get('.v-autocomplete__content .v-list-item').first().click()
        cy.getBySel('date-de-debut').type(dateDeDebutSouhaitee)
        cy.getBySel('date-de-fin').type(dateDeFinSouhaitee)
        cy.getBySel('submit-demande-intervention-button').click()

        //Vérifier les dates dans le formulaire de  modification
        cy.getBySel('tab-demandes').click()
        cy.getBySel('cliquer-pour-rafraichir-btn').click()
        cy.getBySel('items-list-desktop-items')
            .contains('Sanitaires cassés')
            .click()
        cy.getBySel('edit-demande-intervention-button').click()
        cy.getBySel('date-de-debut')
            .find('input')
            .should('have.value', dateDeDebutSouhaitee)
        cy.getBySel('date-de-fin')
            .find('input')
            .should('have.value', dateDeFinSouhaitee)
    })
})

describe("En tant qu'administrateur global, je peux activer la gestion de saisie des dates sur les formulaires des Interventions", () => {
    it("peut activer la gestion de saisie des dates sur les formulaires d'une Intervention", () => {
        //Vérifier que la fonctionnalité n'est pas activee par defaut
        cy.visit('/admin/ods-intervention')
        cy.getBySel('listItem-Component-step').click()
        cy.getBySel('gestion-date-intervention')
            .find('input')
            .should('not.be.checked')
        cy.getBySel('go-to-home-button').click()
        cy.getBySel('component-ServiceOfferComponent-step').click()
        cy.getBySel('tab-équipements').click()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
        })
        cy.getBySel('create-intervention').click()
        cy.getBySel('date-de-debut').should('not.exist')
        cy.getBySel('date-de-fin').should('not.exist')

        //Vérifier que l'activation fonctionne
        cy.visit('/admin/ods-intervention')
        cy.getBySel('listItem-Component-step').click()
        cy.getBySel('gestion-date-intervention').click()
        cy.getBySel('gestion-date-intervention')
            .find('input')
            .should('be.checked')
        cy.getBySel('submit').click()
        cy.getBySel('snackbar').contains(
            'Offre de service "STEP - Chasseneuil" modifiée'
        )
        cy.getBySel('go-to-home-button').click()
        cy.getBySel('component-ServiceOfferComponent-step').click()
        cy.getBySel('tab-équipements').click()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
        })
        cy.getBySel('create-intervention').click()
        cy.getBySel('date-de-debut').scrollIntoView().should('be.visible')
        cy.getBySel('date-de-fin').scrollIntoView().should('be.visible')
    })

    it('peut créer une intervention avec des dates de début et de fin souhaité', () => {
        //Le format avec l'heure est le suivant : YYYY-MM-DDTHH:MM pour cypress, celui de toISOString() ne correspond pas parfaitement
        const dateDeDebutString = new Date()
            .toISOString()
            .split('T')[0]
            .concat('T09:00')
        const dateDeFinString = new Date(Date.now() + 3 * 24 * 60 * 60 * 1000)
            .toISOString()
            .split('T')[0]
            .concat('T18:00')

        //Activer la fonctionnalité
        cy.visit('/admin/ods-intervention')
        cy.getBySel('listItem-Component-step').click()
        cy.getBySel('gestion-date-intervention').click()
        cy.getBySel('gestion-date-intervention')
            .find('input')
            .should('be.checked')
        cy.getBySel('submit').click()
        cy.getBySel('snackbar').contains(
            'Offre de service "STEP - Chasseneuil" modifiée'
        )

        //Créer une OT avec une date de début et de fin souhaitée
        cy.getBySel('go-to-home-button').click()
        cy.getBySel('component-ServiceOfferComponent-step').click()
        cy.getBySel('tab-équipements').click()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
        })
        cy.getBySel('create-intervention').click()

        cy.getBySel('select-equipement').click()
        cy.get('.v-autocomplete__content .v-list-item').first().click()
        cy.getBySel('switch-auto-title').click()
        cy.getBySel('titre-de-l-intervention').type('Sanitaires cassés')
        cy.getBySel('date-de-debut').type(dateDeDebutString)
        cy.getBySel('date-de-fin').type(dateDeFinString)
        cy.getBySel('submit-intervention-button').click()

        //Vérifier les dates dans le formulaire de modification
        cy.getBySel('tab-interventions').click()
        cy.getBySel('cliquer-pour-rafraichir-btn').click()
        cy.getBySel('items-list-desktop-items')
            .contains('Sanitaires cassés')
            .click()
        cy.getBySel('edit-intervention-button').click()
        cy.getBySel('date-de-debut')
            .find('input')
            .should('have.value', dateDeDebutString)
        cy.getBySel('date-de-fin')
            .find('input')
            .should('have.value', dateDeFinString)
    })
})
