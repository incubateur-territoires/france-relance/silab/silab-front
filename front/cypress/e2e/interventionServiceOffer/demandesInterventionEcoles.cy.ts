import { scrollTotheEndOfInfiniteScroll } from './helper'

const odsEcoleUrl =
    '/app/interventions/ecoles-poitiers/map?tab=interventions&filter-panel=false'

const demandeUrl =
    '/app/interventions/ecoles-poitiers/map?tab=demandes&filter-panel=false&selected-item=demandes-di-ecole-415'
const demandeEnAttenteDInfoSupplementationUrl =
    '/app/interventions/ecoles-poitiers/map?tab=demandes&filter-panel=false&selected-item=demandes-di-ecole-416'
describe("En tant que concierge de l'école Maxime Renard, je peux demander et suivre une intervention", () => {
    it('Peut demander une intervention', () => {
        cy.visit(odsEcoleUrl)
        cy.getBySel('tab-équipements').click()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
        })
        cy.getBySel('create-demande-intervention').click()
        cy.getBySel('upload-image-button').selectFile(
            'src/assets/image-test.jpg',
            { force: true }
        )
        cy.getBySel('titre-de-la-demande').type('Sanitaires cassés')
        cy.getBySel('select-equipement').click()
        cy.get('.v-autocomplete__content .v-list-item').first().click()
        cy.getBySel('description').type(
            'Les sanitaires du bâtiment A sont tous cassés, et franchement... voila.'
        )
        cy.getBySel('submit-demande-intervention-button')
            .should('be.enabled')
            .click()
        cy.getBySel('tab-demandes').click()
        cy.getBySel('cliquer-pour-rafraichir-btn').click()
        cy.getBySel('items-list-desktop-items')
            .contains('Sanitaires cassés')
            .click()
    })
    it("Peut accéder à l'offre de service", () => {
        cy.visit('/')

        cy.contains(
            '[data-cy^="component-ServiceOfferComponent"]',
            'Ecoles de Poitiers'
        )
            .scrollIntoView()
            .click()
        cy.url().should('eq', Cypress.config().baseUrl + odsEcoleUrl)
    })
    it('Peut accéder à son école', () => {
        cy.visit(odsEcoleUrl)
        cy.getBySel('tab-équipements').click()
        scrollTotheEndOfInfiniteScroll()
        cy.getBySel('items-list-desktop-items').contains('Ecole Maxime Renard')
    })

    it('Ne peut pas demander une intervention sans renseigner un titre et un equipement', () => {
        cy.visit(odsEcoleUrl)
        cy.getBySel('tab-équipements').click()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
        })
        cy.getBySel('create-demande-intervention').click()

        cy.getBySel('titre-de-la-demande').type('Sanitaires cassés')
        // ne fonctionne pas car cypress écrit le titre et la localisation en simultanée.le btn est donc enabled pas.
        // cy.getBySel('submit-demande-intervention-button').should('be.disabled')

        cy.getBySel('select-equipement').click()
        cy.get('.v-autocomplete__content .v-list-item').first().click()
        cy.getBySel('submit-demande-intervention-button').should('be.enabled')

        cy.getBySel('titre-de-la-demande').type('{selectall}{backspace}')
        cy.getBySel('submit-demande-intervention-button').should('be.disabled')
    })

    // amélioration: ajuster ce test à l'ajout de l'arborescence
    // it("Peut demander une intervention sans saisir un équipement car il n'y en à qu'un seul possible, et visualiser sa demande fraîchement créée dans la liste des demandes en attente de validation", () => {
    //     cy.visit(demandeInterventionCreationUrl)
    // cy.getBySel('upload-image-button').selectFile(
    //     'src/assets/image-test.jpg',
    //     { force: true }
    // )
    // cy.getBySel('titre-de-la-demande').type('Sanitaires cassés')
    // cy.getBySel('description').type(
    //     'Les sanitairesdu bâtiment A sont tous cassés, et franchement... voila.'
    // )
    // cy.getBySel('submit-demande-intervention-button')
    //     .should('not.be.disabled')
    //     .click()
    // cy.getBySel('demandeIntervention-column-awaiting_validation')
    //     .getByRootSel('listItem-Component')
    //     .should('include.text', 'Sanitaires cassés')
    // })
})

describe("En tant que concierge de l'école Maxime Renard, je peux modifier une demande d'intervention", () => {
    // amélioration: vérifier la localisation
    it("Peut modifier une demande d'intervention", () => {
        cy.visit(odsEcoleUrl)
        cy.getBySel('tab-demandes').click()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
        })
        cy.getBySel('edit-demande-intervention-button').click()

        cy.getBySel('card-modification-demande-intervention').within(() => {
            cy.getBySel('titre-de-la-demande').type(
                '{selectAll}Changer tapisserie'
            )
            cy.getBySel('description').type(
                '{selectAll}La tapisserie est abimeé il faut la changer ou repeindre'
            )
        })
        cy.getBySel('submit-demande-intervention-button').click()
        cy.getBySel('snackbar').should(
            'contain',
            "Demande d'intervention mise à jour"
        )
        cy.getBySel('edit-demande-intervention-button').click()
        cy.getBySel('card-modification-demande-intervention').within(() => {
            cy.getBySel('titre-de-la-demande')
                .find('input')
                .should('contain.value', 'Changer tapisserie')
            cy.getBySel('description')
                .find('textarea')
                .should(
                    'contain.value',
                    'La tapisserie est abimeé il faut la changer ou repeindre'
                )
        })
    })
})

describe("En tant que concierge de l'école Maxime Renard, je peux donner des renseignements complémentaires sur une demande d'intervention en attente d'informations", () => {
    it("Peut répondre a une demande d'information complémentaire puis la retrouver dans la colone 'attente de validation'", () => {
        cy.visit(demandeEnAttenteDInfoSupplementationUrl)

        cy.getBySel('edit-demande-intervention-button').click()

        cy.getBySel('card-modification-demande-intervention').within(() => {
            cy.getBySel('upload-image-button').selectFile(
                'src/assets/image-test.jpg',
                { force: true }
            )
            cy.getBySel('titre-de-la-demande').type(
                '{selectAll}Préau encombrant nécessite une grue pour déplacement'
            )
            // amélioration: ajuster ce test à l'ajout de l'arborescence
            // cy.getBySel('select-equipement').click()
            // cy.get('.v-autocomplete__content .v-list-item').first().click()
            cy.getBySel('description').type(
                '{selectAll}Levier cassé à sa base.'
            )
        })
        cy.getBySel('submit-demande-intervention-button')
            .should('include.text', 'Compléter')
            .click()

        cy.getBySel('snackbar').contains("Complément d'information envoyé")
    })
})

describe("En tant que concierge de l'école Maxime Renard, demander une intervention et en consulter les détails", () => {
    it('Créer une intervention puis la retrouve sur la liste et clique dessus pour en consulter les détails', () => {
        // amélioration: ajuster ce test à l'ajout de l'arborescence
        cy.visit(odsEcoleUrl)
        cy.getBySel('tab-demandes').click()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
        })
        cy.getBySel('edit-demande-intervention-button').click()

        const titreDemandeIntervention = 'Porte cassée'
        const descriptionIntervention =
            "La gâche de la poignée de porte est cassée et le seuil est arraché. Attention c'est un seuil a la suisse"

        cy.getBySel('card-modification-demande-intervention').within(() => {
            cy.getBySel('upload-image-button').selectFile(
                'src/assets/image-test.jpg',
                { force: true }
            )
            cy.getBySel('titre-de-la-demande').type(
                '{selectAll}' + titreDemandeIntervention
            )
            cy.getBySel('description').type(
                '{selectAll}' + descriptionIntervention
            )
        })
        cy.getBySel('submit-demande-intervention-button').click()

        // vérifier les éléments de la page détails
        cy.get('.v-card-title').should('include.text', titreDemandeIntervention)
        cy.getBySel('date-and-creator')
            .invoke('text')
            .should('match', /.+ Le \d\d\/\d\d\/\d\d par .+/)
        cy.getBySel('description').should(
            'include.text',
            descriptionIntervention
        )
    })
    it.skip('Consulte les détails', () => {
        //! les données seed ne ressortent pas sur la ci
        cy.visit(
            '/app/interventions/ecoles-poitiers/equipements/%7B%22id%22:%22ecole-batiment-1%22,%22type%22:%22buildingset%22%7D/demandes-interventions/di-ecole-415'
        )

        cy.getBySel('titre-de-la-demande').should(
            'contain',
            'Trou dans le préau'
        )
        cy.getBySel('code').should('contain', 'DI-00666')
        cy.getBySel('gmaoObjectViewUrl')
            .should('contain', 'Carl INTEG')
            .should('have.attr', 'href')
        cy.getBySel('equipement').should('contain', 'EE des petits canailloux')
        cy.getBySel('address').should(
            'contain',
            '666 rue des mignons 8600 Pasla'
        )
        cy.getBySel('description').should(
            'contain',
            'Au fond un trou diamètre 50 à reboucher avec panneua métallique'
        )
        cy.getBySel('date-and-creator')
            .invoke('text')
            .should('match', /Le \d\d\/\d\d\/\d\d\d\d par Rene Lataupe/)
    })
})

describe("Sur le détail et l'édition d'une demande, je peux accéder à l'historiques des états de l'intervention", () => {
    it('On ne voit par défaut que le dernier état', () => {
        cy.visit(demandeUrl)
        cy.getBySel('object-status').should('have.length', 1)
    })

    it('On peut cliquer sur un bouton pour accéder à tous les états', () => {
        cy.visit(demandeUrl)
        cy.getBySel('object-status-show-more').click()
        cy.getBySel('object-status').its('length').should('be.gte', 1)
    })
})

describe.skip('En tant que concierge, je peux utiliser un systeme de favoris pour filtrer mes equipements', () => {
    it('Peut mettre en favoris une école', () => {
        cy.visit(odsEcoleUrl)
        cy.getBySel('tab-équipements').click()
        cy.getByRootSel('listItem-Component')
            .getBySel('is-not-bookmarked')
            .should('have.length', 4)
            .first()
            .click()
            .getBySel('is-not-bookmarked')
            .should('have.length', 3)
            .getBySel('is-bookmarked')
            .should('have.length', 1)

        cy.getByRootSel('listItem-Component')
            .getBySel('is-bookmarked')
            .first()
            .click()
            .getBySel('is-not-bookmarked')
            .should('have.length', 4)
    })

    it('Peut filtrer par favoris', () => {
        cy.visit(odsEcoleUrl)
        cy.getBySel('tab-équipements').click()

        cy.getByRootSel('listItem-Component').should('have.length', 4)
        cy.getBySel('is-not-bookmarked').first().click()
        cy.getBySel('is-not-bookmarked').last().click()
        cy.getByRootSel('listItem-Component').should('have.length', 4)

        cy.getBySel('switch-favoris-uniquement').click()
        cy.getByRootSel('listItem-Component').should('have.length', 2)

        cy.getBySel('switch-favoris-uniquement').click()
        cy.getByRootSel('listItem-Component').should('have.length', 4)

        cy.getBySel('switch-favoris-uniquement').click()
        cy.getBySel('is-bookmarked').first().click()
        cy.getByRootSel('listItem-Component').should('have.length', 1)
    })
})

describe("En tant que concierge, je peux supprimer une demande d'intervention avec une confirmation necessaire", () => {
    it("Peut supprimer une de demande d'intervention", () => {
        cy.visit(demandeEnAttenteDInfoSupplementationUrl)
        cy.getBySel('supprimer-DI').click()
        cy.getBySel('valider-suppression').click()
        cy.getBySel('snackbar').contains(
            `Demande d'intervention "DI-00667 Préau dans le trou" supprimée`
        )
    })

    it("Peut lors de la demande de confirmation de suprression annuler l'action", () => {
        cy.visit(demandeEnAttenteDInfoSupplementationUrl)
        cy.getBySel('supprimer-DI').click()
        cy.getBySel('annuler-suppression').click()
        cy.getBySel('snackbar').should('not.exist')
    })
})

describe("Sur un équipement, il y a des détails qu'il le concerne", () => {
    it("Visualise les détails d'un équipement", () => {
        const detailsEquipementUrl =
            '/app/interventions/ecoles-poitiers/map?tab=équipements&filter-panel=false&selected-item=équipements-{"id":"ecole-batiment-1","type":"buildingset"}'
        cy.visit(detailsEquipementUrl)
        cy.get('.v-card-title').contains('EE des petits canailloux')
        cy.getBySel('gmaoObjectViewUrl').should(
            'have.attr',
            'href',
            'https://ceci-est-un-lien-gmao-veuillez-circuler/ecole-batiment-1'
        )
        cy.getBySel('equipement-comment')
            .find('textarea')
            .should('have.value', 'Équipement des canailloux')
        cy.getBySel('component-item-map-viewer').scrollIntoView()
    })
})
