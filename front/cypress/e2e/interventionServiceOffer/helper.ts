export function scrollTotheEndOfInfiniteScroll() {
    const scrollAndCheckRecursif = (maxElementInList: number) => {
        cy.getBySel('items-list-desktop-items')
            .then(($item) => {
                cy.wrap($item).scrollTo('bottom', { ensureScrollable: false })
                cy.wait(100)
            })
            .then(() => {
                cy.getByRootSel('listItem-Component').then(($list) => {
                    if ($list.length === maxElementInList) {
                        return
                    }
                    scrollAndCheckRecursif(maxElementInList)
                })
            })
    }

    cy.getByRootSel('listItem-Component').eq(0).should('exist')
    let nbElementInInfinitScroll: number
    cy.getBySel('items-list-desktop')
        .find('.v-tab--selected .v-btn__content')
        .should('not.be.empty')
        .invoke('text')
        .then((text) => {
            nbElementInInfinitScroll = Number(text.split(' ')[0])
        })
        .then(() => {
            scrollAndCheckRecursif(nbElementInInfinitScroll)
        })
}
