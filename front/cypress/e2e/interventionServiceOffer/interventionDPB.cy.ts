import { InterventionPriority } from '@/interventionServiceOffer/entities/intervention/InterventionDto'
import { dateTimeToDateInputValue } from '@/shared/helpers/dates'

const interventionDetails =
    '/app/interventions/dpb/interventions/inter-dpb-avec-seed'
const solderInterventionUrl =
    '/app/interventions/dpb/interventions/inter-dpb-avec-seed/solder'

describe("En tant qu'agent, je peux connaitre les intervenants assigné sur une OT et déclarer les temps d'interventions", () => {
    it('Accède à une intervention et consulte les intervenants affectés', () => {
        cy.visit(interventionDetails)
        cy.scrollTo('bottom', { ensureScrollable: false })
        cy.getBySel('intervenants-affectes').each(
            ($intervenantAffecte, index) => {
                cy.wrap($intervenantAffecte).should(
                    'contain',
                    ['John Doe', 'Jane Doe', 'Jean Doux', 'silab test'][index]
                )
            }
        )
    })

    it("Déclarer les technicien et leur temps d'interventions au soldage d'une OT", () => {
        cy.visit(solderInterventionUrl)
        cy.getByRootSel('activite-action').click({ multiple: true })
        cy.getByRootSel('activite-conforme').click({ multiple: true })
        cy.getByRootSel('intervenant-disponible').click('left', {
            multiple: true,
        })
        cy.getBySel('hours-spend-input').find('input').type('1')
        cy.getBySel('minutes-spend-input').find('input').type('10')
        cy.getBySel('solder-intervention-button').click()
        cy.getBySel('snackbar').should(
            'contain.text',
            'Intervention "Remplacement d\'un détecteur d\'éclairage" terminée'
        )
    })

    it('La liste des intervenants disponibles me contient ainsi que mon équipe', () => {
        cy.visit(solderInterventionUrl)
        cy.getBySel(
            'intervenant-disponible-test.silab@grandpoitiers.fr'
        ).should('exist')
        cy.getBySel('intervenant-disponible-jean-doux-uniq666').should('exist')
        cy.getBySel('intervenant-disponible-john-doe-uniq666').should('exist')
    })

    it('Ajoute un intervenant Carl dans la liste des intervenants à déclarer', () => {
        cy.visit(solderInterventionUrl)
        cy.getBySel('select-intervenant').type('Jean Doux')
        cy.get('.v-autocomplete__content .v-list-item') //
            .first()
            .click()
        cy.getBySel('intervenant-disponible-jean-doux-uniq666').should('exist')
    })
})

describe("En tant qu'agent, je peux consulter l'historique d'une intervention", () => {
    it("Consulte l'historique de l'intervention DPB avec seed", () => {
        cy.visit(interventionDetails)
        cy.getBySel('object-status-history')
            .should('exist')
            .and('contain', 'A faire par John Doe')
    })
})

describe("En tant qu'agent, je peux renseigner les attributs de l'intervention liées aux soldages", () => {
    it("Solde l'intervention DPB avec seed en renseignant les attributs de l'intervention liées aux soldages", () => {
        const materielsConsommes = 'art-16843 x3, 5 interrupteurs lepetit'
        const solderInterventionUrl =
            '/app/interventions/dpb/interventions/inter-dpb-avec-seed/solder'

        //Le format avec l'heure est le suivant : YYYY-MM-DDTHH:MM pour cypress, celui de toISOString() ne correspond pas parfaitement
        const dateDeDebut = new Date()
        dateDeDebut.setHours(9)
        dateDeDebut.setMinutes(0)
        const dateDeDebutString = dateTimeToDateInputValue(dateDeDebut)
        const dateDeFin = new Date(Date.now() + 3 * 24 * 60 * 60 * 1000)
        dateDeFin.setHours(18)
        dateDeFin.setMinutes(0)
        const dateDeFinString = dateTimeToDateInputValue(dateDeFin)

        cy.visit(solderInterventionUrl)
        cy.getByRootSel('activite-action').click({ multiple: true })
        cy.getByRootSel('activite-conforme').click({ multiple: true })
        cy.getBySel('liste-materiels-demande-de-recompletement').type(
            materielsConsommes
        )
        cy.getBySel('date-de-debut').type(dateDeDebutString)
        cy.getBySel('date-de-fin').type(dateDeFinString)
        cy.getBySel('intervenant-disponible-test.silab@grandpoitiers.fr').click(
            50,
            15
        )
        cy.getBySel('solder-intervention-button').click()
        cy.getBySel('current-status').should(
            'have.text',
            'Attente de recomplètement'
        )
        cy.getBySel('object-status-history').should(
            'contain',
            'Attente de recomplètement par test.silab@grandpoitiers.fr'
        )
        cy.getBySel('object-status-comment').should(
            'contain',
            `${materielsConsommes} Via Siilab par test.silab@grandpoitiers.fr`
        )
        //Amélioration : rétablir ce test lors de l'ajout de la vue unifiée.
        //Problème actuel : lors de soldage, l'intervention en mémoire coté front n'est pas mise à jour, la vérification se fait donc avec les anciennes dates et ça marche po
        // cy.getBySel('dates').should(
        //     'contain',
        //     `Du ${formatageDatePourAffichage(dateDeDebut)} au ${formatageDatePourAffichage(dateDeFin)}`
        // )
    })
})

describe("En tant qu'agent, je peux consulter les interventions qui me sont affectés", () => {
    it.skip("Vérifie qu'uniquement les interventions liées à l'utilisateur sélectionné soient affichées et que le nombre total d'interventions récupérées diminue", () => {
        const interventions = '/app/interventions/dpb'
        const silabTest = 'silab test'
        cy.visit(interventions)
        // Vérifie si l'utilisateur connecté n'est pas affecté à la première intervention sans filtre
        let listLenghtOriginal: number
        cy.getBySel('nb-items-quantity')
            .invoke('text')
            .then((quantityText) => {
                listLenghtOriginal = Number(quantityText)
            })
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
            cy.scrollTo('bottom', { ensureScrollable: false })
            cy.getBySel('intervenants-affectes').should(
                'not.contain',
                silabTest
            )
        })
        cy.getBySel('go-to-ods').click()
        // Vérifie si l'utilisateur est affecté à la première intervention filtré uniquement sur lui même
        cy.getBySel('btn-tune').click()
        cy.getBySel('assigne-utilisateur-connecte').click()
        cy.getBySel('is-loading-item-list').should('exist')
        cy.getBySel('is-loading-item-list').should('not.exist')
        cy.getBySel('nb-items-quantity')
            .invoke('text')
            .then((quantityText) => {
                const newLength = Number(quantityText)
                cy.wrap(newLength).should('be.lessThan', listLenghtOriginal)
            })
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
            cy.scrollTo('bottom', { ensureScrollable: false })
            cy.getBySel('intervenants-affectes').should('contain', silabTest)
        })
    })

    it("Vérifie que le l'utilisateur recherché Jean Doux est bien affecté à la première intervention filtré", () => {
        const interventions = '/app/interventions/dpb'
        const intervenantToFilter = 'Jean Doux'
        cy.visit(interventions)
        cy.getBySel('btn-tune').click()
        cy.getBySel('select-intervenant').click().type(intervenantToFilter)
        cy.get('.v-autocomplete__content .v-list-item')
            .first()
            .should('contain', intervenantToFilter)
            .click()
        cy.getBySel('is-loading-item-list').should('exist')
        cy.getBySel('is-loading-item-list').should('not.exist')
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').first().click()
            cy.scrollTo('bottom', { ensureScrollable: false })
            cy.getBySel('intervenants-affectes').should(
                'contain',
                intervenantToFilter
            )
        })
    })
})

describe("En tant qu'agent, je peux sauvegarder le traitement d'une ot sans la solder", () => {
    it('Déclare du temps sur une ot sans la solder et pouvoir retrouver cette déclaration', () => {
        cy.visit(solderInterventionUrl)
        cy.getByRootSel('activite-action-faites').should('not.exist')
        cy.getBySel('hours-spend-input').find('input').type('1')
        cy.getBySel('sauvegarder-intervention').should('be.enabled').click()
        cy.getBySel('snackbar').should(
            'contain.text',
            "Traitement de l'intervention \"Remplacement d'un détecteur d'éclairage\" sauvegardé "
        )
        cy.getByRootSel('activite-action-faites').should('have.lengthOf', 1)
    })
})

describe("En tant qu'agent, je peux lors de la création d'une intervention, enlever la position que j'ai choisi sur la carte", () => {
    const créerInterventionUrl = '/app/interventions/dpb/interventions/creer'

    function selectPositionOnMap() {
        cy.getBySel('component-MapPickerComponent')
            .trigger('mousedown', { button: 0 })
            .trigger('mousemove', { clientX: 200, clientY: 200 })
            .trigger('mouseup', { force: true })
            .click(200, 200)
    }

    it("Choisi une position sur la carte et la retire en cliquant sur l'icône de suppression", () => {
        cy.visit(créerInterventionUrl)
        cy.getBySel('component-MapPickerComponent').scrollIntoView()
        cy.getBySel('reset-location-button').should('not.exist')
        selectPositionOnMap()
        cy.getBySel('reset-location-button').click()
        cy.getBySel('intervention-marker').should('not.exist')
    })

    it('Choisi une position sur la carte et la retire en cliquant sur le marqueur', () => {
        cy.visit(créerInterventionUrl)
        cy.getBySel('component-MapPickerComponent').scrollIntoView()
        cy.getBySel('intervention-marker').should('not.exist')
        selectPositionOnMap()
        cy.getBySel('intervention-marker').click()
        cy.getBySel('intervention-marker').should('not.exist')
    })
})

describe("En tant qu'agent, je peux préciser l'opération lors de la déclaration d'une activité", () => {
    it('Déclare du temps sur une ot avec une', () => {
        let textOpérationSéléctionné: string
        cy.visit(solderInterventionUrl)
        cy.getByRootSel('select-operation').click()
        cy.get('.v-autocomplete__content .v-list-item')
            .first()
            .invoke('text')
            .then((opérationText) => {
                textOpérationSéléctionné = opérationText
            })
        cy.get('.v-autocomplete__content .v-list-item').first().click()
        cy.getBySel('hours-spend-input').find('input').type('1')
        cy.getBySel('sauvegarder-intervention').should('be.enabled').click()
        cy.getByRootSel('activite-action-faites')
            .last()
            .within(() => {
                cy.get('.v-card-subtitle').should(
                    'contain.text',
                    textOpérationSéléctionné
                )
            })
    })
})

describe("En tant qu'agent, j'ai un message qui m'indique si un équipement n'autorise pas les demandes ou les interventions", () => {
    const interventions = '/app/interventions/dpb'

    it("Vérifie qu'il y ai un message qui indique que les interventions ne sont pas autorisées est présent sur l'équipement Ancienne Comédie", () => {
        const equipementDetails =
            '/app/interventions/dpb/equipements/ancienne-comedie'
        cy.visit(equipementDetails)
        cy.getBySel('message-interventions-non-autorisees').should(
            'contain',
            "Carl INTEG n'autorise pas les demandes ou les interventions sur l'équipement"
        )
    })

    it("Vérifie que l'équipement Ancienne Comédie qui n'autorise pas les interventions ne soient pas présent dans la liste d'équipements lors de la création d'intervention", () => {
        cy.visit(interventions)
        cy.getBySel('primary-action-button').click()
        cy.getBySel('select-equipement').click()
        cy.get('.v-autocomplete__content .v-list-item').should(
            'not.contain',
            'Ancienne Comédie'
        )
    })

    it("Vérifie que l'équipement Ancienne Comédie qui n'autorise pas les interventions ne soient pas présent dans la liste d'équipements lors de la création d'une demande", () => {
        cy.visit(interventions)
        cy.getBySel('tab-demandes').click()
        cy.getBySel('primary-action-button').click()
        cy.getBySel('select-equipement').click()
        cy.get('.v-autocomplete__content .v-list-item').should(
            'not.contain',
            'Ancienne Comédie'
        )
    })

    it("Vérifie qu'il y ai un message qui indique qu'il n'est pas possible de créer une intervention lorsque il n'y a pas d'équipements qui en autorise", () => {
        const equipementDetails =
            '/app/interventions/dpb/equipements/ancienne-comedie'
        cy.visit(equipementDetails)
        cy.getBySel('btn-créer-intervention').click()
        cy.getBySel('select-equipement').find('input').should('be.disabled')
        cy.get('.v-input__details').should(
            'contain',
            "Il n'y a pas d'équipement permettant la création d'interventions, veuillez sélectionner la position sur la carte"
        )
    })

    it("Vérifie qu'il y ai un message qui indique qu'il n'est pas possible de créer une demande lorsque il n'y a pas d'équipements qui en autorise", () => {
        const equipementDetails =
            '/app/interventions/dpb/equipements/ancienne-comedie'
        cy.visit(equipementDetails)
        cy.getBySel('btn-créer-demande-intervention').click()
        cy.getBySel('select-equipement').find('input').should('be.disabled')
        cy.get('.v-input__details').should(
            'contain',
            "Il n'y a pas d'équipement permettant la création de demandes"
        )
        cy.getBySel('message-pas-d-equipement-dispo-pour-creation-di').should(
            'contain',
            'Aucun équipement ne permet la création de demande dans ce contexte'
        )
    })

    it('Filtre sur les différentes priorités possibles et constate que la liste se retreint en conséquence', () => {
        const interventionsListUrl = '/app/interventions/dpb'

        function séléctionneUnePrioritéEtVérifieQueLesElementsDeLaListeCorrespondentBienALaPrioritéChoisieVoilaVoilaToutVaBienEtVousPuisLaDéséléctionne(
            priority: InterventionPriority
        ) {
            cy.getBySel(`priority-filter-${priority}`).within(() => {
                cy.get('.v-label--clickable').click()
            })

            if (priority !== InterventionPriority.Normal) {
                cy.getBySel('component-chip').each(($priority) => {
                    cy.wrap($priority).within(() => {
                        cy.get('.v-chip__content').should('contain', priority)
                    })
                })
            } else {
                cy.getBySel('component-chip').should('not.exist')
            }
            cy.getBySel(`priority-filter-${priority}`).within(() => {
                cy.get('.v-label--clickable').click()
            })
        }

        cy.visit(interventionsListUrl)

        cy.getBySel('btn-tune').click()

        séléctionneUnePrioritéEtVérifieQueLesElementsDeLaListeCorrespondentBienALaPrioritéChoisieVoilaVoilaToutVaBienEtVousPuisLaDéséléctionne(
            InterventionPriority.Urgent
        )
        séléctionneUnePrioritéEtVérifieQueLesElementsDeLaListeCorrespondentBienALaPrioritéChoisieVoilaVoilaToutVaBienEtVousPuisLaDéséléctionne(
            InterventionPriority.Important
        )
        séléctionneUnePrioritéEtVérifieQueLesElementsDeLaListeCorrespondentBienALaPrioritéChoisieVoilaVoilaToutVaBienEtVousPuisLaDéséléctionne(
            InterventionPriority.Normal
        )
        séléctionneUnePrioritéEtVérifieQueLesElementsDeLaListeCorrespondentBienALaPrioritéChoisieVoilaVoilaToutVaBienEtVousPuisLaDéséléctionne(
            InterventionPriority.Bas
        )
    })
})

describe("En tant qu'agent DPB, je peux filtrer la liste d'interventions par date de début et date de fin", () => {
    const interventionsListUrl = '/app/interventions/dpb'

    it("Filtre la liste d'interventions et retrouve une intervention comprise dans l'intervalle de date", () => {
        cy.visit(interventionsListUrl)
        cy.get('body').then(($body) => {
            if ($body.find('[data-cy^="filters-chip"]').length) {
                cy.getBySel('filters-chip').click('right')
            }
        })
        cy.getBySel('btn-tune').click()
        cy.getBySel('input-intervalle-date-debut').within(() => {
            cy.getBySel('input-date-debut-intervalle').type('2023-08-12')
            cy.getBySel('input-date-fin-intervalle').type('2023-08-14')
            cy.getBySel('bouton-validation-intervalle').click()
        })

        cy.getBySel('input-intervalle-date-fin').within(() => {
            cy.getBySel('input-date-debut-intervalle').type('2023-08-12')
            cy.getBySel('input-date-fin-intervalle').type('2023-08-14')
            cy.getBySel('bouton-validation-intervalle').click()
        })
        cy.getBySel('listItem-Component-inter-dpb-avec-seed').click()
        cy.getBySel('dates').should(
            'contain',
            'Du 13/08/23 à 08:00 au 14/08/23 à 16:00'
        )
    })

    it("Ne trouve pas une intervention qui n'est pas dans un intervalle de date", () => {
        cy.visit(interventionsListUrl)
        cy.get('body').then(($body) => {
            if ($body.find('[data-cy^="filters-chip"]').length) {
                cy.getBySel('filters-chip').click('right')
            }
        })
        cy.getBySel('btn-tune').click()
        cy.getBySel('input-intervalle-date-debut').within(() => {
            cy.getBySel('input-date-debut-intervalle').type('2000-08-12')
            cy.getBySel('input-date-fin-intervalle').type('2023-08-13')
            cy.getBySel('bouton-validation-intervalle').click()
        })
        cy.getBySel('listItem-Component-inter-dpb-avec-seed').should(
            'not.exist'
        )
    })
})

describe("En tant qu'agent, je peux supprimer une activité action que j'ai créé ", () => {
    it("Supprime une activité qui vient d'être créé", () => {
        cy.visit(solderInterventionUrl)
        cy.getByRootSel('activite-action-faites').should('not.exist')
        cy.getBySel('hours-spend-input').find('input').type('1')
        cy.getBySel('sauvegarder-intervention').click()
        cy.getByRootSel('activite-action-faites').should('have.lengthOf', 1)
        cy.getBySel('supprimer-des-lignes-activites-actions').click()
        cy.getBySel('activite-action-faites-0').within(() => {
            cy.getBySel('bouton-suppression-activite').click()
            cy.get(
                '[data-cy="titre-activite"].text-decoration-line-through'
            ).should('exist')
        })
        cy.getBySel('supprimer-activites-actions').click()
        cy.getByRootSel('activite-action-faites').should('have.lengthOf', 0)
    })
    it("Annuler la suppression d'une activité qui vient d'être créé et voir un message d'avertissement si un élément est coché à supprimer avant d'enregistrer", () => {
        cy.visit(solderInterventionUrl)
        cy.getBySel('hours-spend-input').find('input').type('1')
        cy.getBySel('sauvegarder-intervention').click()
        cy.getByRootSel('activite-action-faites').should('have.lengthOf', 1)
        cy.getBySel('supprimer-des-lignes-activites-actions').click()
        cy.getBySel('activite-action-faites-0').within(() => {
            cy.getBySel('bouton-suppression-activite').click()
            cy.get(
                '[data-cy="titre-activite"].text-decoration-line-through'
            ).should('exist')
        })
        cy.getBySel('avertissement-activité-en-suppression').contains(
            "Vous n'avez pas confirmé la suppression des Temps Passés, ces modifications seront perdues si vous enregistrez ou soldez !"
        )
        cy.getBySel('annuler-suppression-activites-actions').click()
        cy.getBySel('activite-action-faites-0').within(() => {
            cy.get(
                '[data-cy="titre-activite"].text-decoration-line-through'
            ).should('not.exist')
        })
    })
})
