import { scrollTotheEndOfInfiniteScroll as scrollTotheEndOfInfiniteScroll } from './helper'
const solderInterventionUrl =
    '/app/interventions/step/equipements/equipement-avec-enfants/interventions/inter-avec-activite-seed/solder'
describe("Depuis la vue détail d'une intervention, je peux téléphoner à son demandeur", () => {
    it("Accède à une intervention et contacte acterufoacle demandeur à partir d'une liste de numéros", () => {
        const interventionDetails =
            '/app/interventions/step/interventions/inter-avec-activite-seed'
        cy.visit(interventionDetails)
        cy.getBySel('phone-link').should('contain', 'Jean Doux').click()
        cy.getBySel('button-actions-list')
            .find('a')
            .each(($link) => {
                cy.wrap($link).should('have.attr', 'href')
            })
    })
})

describe("En tant qu'agent step, je peux créer une intervention sur un équipement", () => {
    const listeEquipementUrl = '/app/interventions/step'
    it('Peut créer une intervention sur un équipement depuis la vue detail de celui-ci', () => {
        cy.visit(listeEquipementUrl)
        cy.getBySel('tab-équipements').click()
        const description = "Description de l'intervention"
        cy.getByRootSel('listItem-Component').first().click()
        cy.getByRootSel('btn-créer-intervention').click()
        cy.getBySel('select-equipement').click()
        cy.get('.v-autocomplete__content .v-list-item').first().click()
        cy.getBySel('description').type(description)
        cy.getBySel('priority-important').click()
        cy.getBySel('submit-intervention-button').click()

        cy.getBySel('snackbar')
            .invoke('text')
            .should('match', /Intervention ".+" créée/)

        cy.getByRootSel('intervention-column-awaiting_realisation')
            .click()
            .within(() => {
                cy.getByRootSel('listItem-Component')
                    .first()
                    .within(() => {
                        // timebox : ce test passe en local mais pas sur la ci ...
                        // cy.getByRootSel('title').should(
                        //     'contain.text',
                        //     description
                        // )
                        // cy.getByRootSel('subtitle').should(
                        //     'contain.text',
                        //     description
                        // )
                        // cy.getByRootSel('component-chip').should(
                        //     'contain.text',
                        //     'important'
                        // )
                    })
            })
    })
    it.skip('Peut créer une intervention sur un équipement depuis la vue globale et actualiser la liste pour la voir apparaitre', () => {
        // on créé une intervention sur n'importe quel équipement avec une description identifiable
        cy.visit(listeEquipementUrl)
        cy.getBySel('tab-interventions').click()
        cy.getByRootSel('primary-action-button').click()
        cy.getBySel('select-equipement').click()
        cy.get('.v-autocomplete__content .v-list-item').first().click()
        const description = 'description unique s2d1f'
        cy.getBySel('description').type(description)
        cy.getBySel('submit-intervention-button').click()

        // au début on ne trouve pas son inter en début de liste
        cy.getByRootSel('listItem-Component')
            .getBySel('subtitle')
            .should('not.include.text', description)

        // après avoir cliqué sur refresh on la retrouve
        cy.getBySel('cliquer-pour-rafraichir-btn').click()
        cy.getByRootSel('listItem-Component')
            .first()
            .within(() => {
                cy.getBySel('subtitle')
                    .invoke('text')
                    .should('match', new RegExp(`.*${description}.*`))
            })
    })
})
describe("En tant qu'agent je peux gérer le cylce de vie d'une OT sur un équipement.", () => {
    it('Peut voir la liste des interventions sur un équipement', () => {
        cy.visit('/app/interventions/step/equipements/equipement-avec-enfants')
        cy.getByRootSel('intervention-column-awaiting_realisation')
            .click()
            .within(() => {
                cy.getByRootSel('listItem-Component')
                    .first()
                    .within(() => {
                        cy.getBySel('illustration').should('exist')
                        cy.getBySel('title').should('exist')
                        cy.getBySel('subtitle').should('exist')
                        cy.getBySel('anteriority').should('exist')
                    })
            })
    })
})

describe("En tant qu'agent j'ai une vue me permettant de consulter des demandes, des interventions et des équipements", () => {
    const listeEquipementUrl = '/app/interventions/step'

    it('Peut voir la liste des demandes', () => {
        cy.visit(listeEquipementUrl)
        cy.getBySel('tab-demandes').click()

        cy.getBySel('nb-items').should('contain', 'demande')

        cy.getByRootSel('listItem-Component')
            .first()
            .within(() => {
                cy.getBySel('illustration').should('exist')
                cy.getBySel('title').should('exist')
                cy.getBySel('subtitle').should('exist')
                cy.getBySel('anteriority').should('exist')
            })
    })

    it('Peut voir la liste des interventions', () => {
        cy.visit(listeEquipementUrl)
        cy.getBySel('tab-interventions').click()

        cy.getBySel('nb-items').should('contain', 'intervention')

        cy.getByRootSel('listItem-Component')
            .first()
            .within(() => {
                cy.getBySel('illustration').should('exist')
                cy.getBySel('title').should('exist')
                cy.getBySel('subtitle').should('exist')
                cy.getBySel('anteriority').should('exist')
            })
    })

    it('Peut voir la liste des équipements', () => {
        cy.visit(listeEquipementUrl)

        cy.getBySel('tab-équipements').click()
        scrollTotheEndOfInfiniteScroll()
        cy.getBySel('nb-items').should('contain', 'équipement')

        cy.getByRootSel('listItem-Component')
            .first()
            .within(() => {
                cy.getBySel('title').should('exist')
                cy.getBySel('illustration').should('exist')
                cy.getBySel('subtitle').should('exist')
                cy.getBySel('is-not-bookmarked').should('exist')
            })
    })
})

describe("En tant qu'agent step, je peux voir voir les opérations à réaliser lors de la clôture d'une intervention", () => {
    it("Peut voir les opérations à réaliser depuis l'écran de soldage d'une intervention", () => {
        cy.visit(
            '/app/interventions/step/equipements/equipement-avec-enfants/interventions/inter-avec-activite-seed/solder'
        )
        cy.getBySel('titre-activites-diagnostique').should(
            'contain.text',
            'Contrôles'
        )
        cy.getBySel('titre-activites-action').should('contain.text', 'Actions')

        cy.getBySel('list')
            .first()
            .within(() => {
                cy.getByRootSel('title').should('exist')
                cy.getByRootSel('subtitle').should('exist')
            })
    })
})
describe("En tant qu'agent step, je peux cloturer une intervention avec des opérations", () => {
    it('Ne peut pas solder une activite en sans avoir validé toutes les activites prévues', () => {
        cy.visit(solderInterventionUrl)
        cy.getBySel('activite-diagnostique-0').within(() => {
            cy.getBySel('activite-non-conforme').click()
            cy.getBySel('intervention-formulaire').within(() => {
                cy.getBySel('description').type('Changer vanne qui fuit')
            })
        })
        cy.getBySel('activite-diagnostique-1').within(() => {
            cy.getBySel('activite-conforme').click()
        })
        cy.getBySel('activite-diagnostique-2').within(() => {
            cy.getBySel('activite-reserve').click()
            cy.getBySel('demande-intervention-formulaire').within(() => {
                cy.getBySel('title').type('Repeindre la cuve')
                cy.getBySel('description').type("La peinture s'ecaille.")
            })
        })
        cy.getByRootSel('activite-action').click({
            multiple: true,
        })

        cy.getBySel('intervenant-disponible-test.silab@grandpoitiers.fr').click(
            50,
            15
        )

        cy.getBySel('solder-intervention-button').click()
        cy.getBySel('snackbar').should(
            'include.text',
            'Intervention "controle de la cuve de décentation" terminée'
        )

        cy.getBySel('go-to-ods').click()

        cy.getBySel('tab-demandes').click()
        // on ne scrolle pas car la demande créée doit apparaitre en haut

        cy.getBySel('listItem-Component-1').should(
            'contain.text',
            'Repeindre la cuve'
        )

        cy.getBySel('tab-interventions').click()
        // on ne scrolle pas car l'intervention créée doit apparaitre en haut
        cy.getBySel('listItem-Component-1').should(
            'contain.text',
            'Changer vanne qui fuit'
        )

        scrollTotheEndOfInfiniteScroll()
        cy.getBySel('listItem-Component-inter-avec-activite-seed').should(
            'not.exist'
        )
    })
})

describe("En tant qu'agent step, je peux faire des relèves de compteur", () => {
    it("Peut voir les informations d'un compteur", () => {
        const detailsEquipementUrl =
            '/app/interventions/step/equipements/equipement-avec-enfants'
        cy.visit(detailsEquipementUrl)
        cy.getBySel('column-materiel-avec-compteurs-cuve-de-decantation')
            .contains('Cuve de décantation - EQPT-710997')
            .click()
        cy.getBySel('compteur-cpt-1').within(() => {
            cy.getBySel('compteur-titre').should(
                'include.text',
                'capteur de débordement - CP-927381'
            )
            cy.getBySel('compteur-derniere-mesure').should(
                'include.text',
                'Dernier relevé : -aucun-'
            )
        })
        cy.getBySel('compteur-cpt-2').within(() => {
            cy.getBySel('compteur-titre').should(
                'include.text',
                'capteur de co2 - CP-684585'
            )
            cy.getBySel('compteur-derniere-mesure').should(
                'include.text',
                ' Dernier relevé : 974.33 ppm le 01/01/24 à 11:00'
            )
        })
    })
    it('Peut enregistrer une nouvelle mesure sur un compteur', () => {
        const detailsEquipementUrl =
            '/app/interventions/step/equipements/equipement-avec-enfants'
        cy.visit(detailsEquipementUrl)
        cy.getBySel(
            'column-materiel-avec-compteurs-cuve-de-decantation'
        ).click()
        cy.getBySel('compteur-cpt-2').within(() => {
            cy.getBySel('valeur-compteur-float-relevee')
                .type('{selectAll}900')
                .should('contain.text', 'ppmRelever -74.33 ppm en')
            cy.getBySel('compteur-enregistrer-mesure').click()
        })
        cy.getBySel('snackbar').should(
            'include.text',
            '900 ppm de relevé sur capteur de co2'
        )
        cy.getBySel('compteur-cpt-1').within(() => {
            cy.getBySel('compteur-enregistrer-mesure').should('be.disabled')
            cy.getBySel('valeur-compteur-boolean-relevee').click('left')
            cy.getBySel('compteur-enregistrer-mesure').click()
        })
        cy.getBySel('snackbar').should(
            'include.text',
            '900 ppm de relevé sur capteur de co2'
        )
    })
})

describe("Sur un équipement, il y a des détails qu'il le concerne", () => {
    it("Visualise les détails d'un équipement", () => {
        const detailsEquipementUrl =
            '/app/interventions/step/equipements/equipement-avec-enfants'
        cy.visit(detailsEquipementUrl)
        cy.getBySel('equipement-label').should(
            'have.text',
            'Equipement avec enfants'
        )
        cy.getBySel('gmaoObjectViewUrl').should(
            'have.attr',
            'href',
            'https://ceci-est-un-lien-gmao-veuillez-circuler/equipement-avec-enfants'
        )
        cy.getBySel('equipement-comment')
            .find('textarea')
            .should('have.value', 'Equipement STEP - Chasseneuil')
        cy.getBySel('image')
            .scrollIntoView()
            .find('img')
            .each(($image) => {
                cy.wrap($image)
                    .should('have.attr', 'src')
                    .then(() => {
                        cy.getBySel('carousel-next-btn').click()
                        cy.getBySel('carousel-prev-btn').click()
                    })
            })
        cy.getBySel('component-item-map-viewer').scrollIntoView()
    })

    it("Consulte les documents joints d'un équipement", () => {
        const detailsEquipementUrl =
            '/app/interventions/step/equipements/equipement-avec-enfants'
        cy.visit(detailsEquipementUrl)

        // Ouvre la liste des documents
        cy.getBySel('documents-equipement-button')
            .should('contain', 'Documents')
            .click()

        // Vérifie que chaque lien a un attribut href
        cy.getBySel('download-documents-list')
            .find('a')
            .each(($link) => {
                cy.wrap($link).should('have.attr', 'href')
            })

        // Vérifie l'URL d'un document spécifique
        cy.getBySel('download-documents-list')
            .contains('Photo avant')
            .should(
                'have.attr',
                'href',
                'https://step.chasseneuil/photo-avant.jpg'
            )
    })
})

describe("Sur un équipement, je peux modifier le commentaire qu'il le concerne", () => {
    it("Va sur le détail d'un équipement et modifie le commentaire", () => {
        const detailsEquipementUrl =
            '/app/interventions/step/equipements/equipement-avec-enfants'
        cy.visit(detailsEquipementUrl)
        cy.getBySel('equipement-comment')
            .find('textarea')
            .should('have.value', 'Equipement STEP - Chasseneuil')
        cy.getBySel('equipement-comment')
            .find('textarea')
            .eq(0)
            .type(
                "{selectAll}Ceci est un commentaire pour l'équipement STEP - Chasseneuil"
            )
        cy.getBySel('save-equipement-comment').click()
        cy.getBySel('loading-equipement-comment').should('be.visible')
        cy.getBySel('snackbar')
            .invoke('text')
            .should('match', /Commentaire de l'équipement ".+" modifié/)
        cy.getBySel('loading-equipement-comment').should('not.exist')
        cy.getBySel('snackbar').should('not.exist')
        cy.getBySel('equipement-comment')
            .find('textarea')
            .should(
                'have.value',
                "Ceci est un commentaire pour l'équipement STEP - Chasseneuil"
            )
    })
})

describe("Depuis le détail d'une demande d'intervention je peux acceder a la vue détail sur Carl", () => {
    it("Va sur le détail d'une demande d'intervention sur carl depuis silab.", () => {
        const detailDemandeIntervention =
            'app/interventions/step/equipements/equipement-avec-enfants/demandes-interventions/demande-intervention-avec-url-gmao-seed'
        cy.visit(detailDemandeIntervention)
        cy.getBySel('gmaoObjectViewUrl').should(
            'have.attr',
            'href',
            'https://ceci-est-un-lien-gmao-veuillez-circuler/demande-intervention-avec-url-gmao-seed'
        )
    })
})

describe("Depuis le détail d'une demande d'intervention je peux acceder aux documents de la demande", () => {
    it("Consulte la page de détails de l'equipement pour vérifier les documents liés", () => {
        const detailDemandeIntervention =
            '/app/interventions/step/equipements/equipement-avec-enfants/demandes-interventions/demande-intervention-avec-url-gmao-seed'
        cy.visit(detailDemandeIntervention)

        // Ouvre la liste des documents
        cy.getBySel('documents-demande-button')
            .should('contain', 'Documents')
            .click()

        // Vérifie que chaque lien a un attribut href
        cy.getBySel('download-documents-list')
            .find('a')
            .each(($link) => {
                cy.wrap($link).should('have.attr', 'href')
            })

        // Vérifie l'URL d'un document spécifique
        cy.getBySel('download-documents-list')
            .contains('Photo avant')
            .should(
                'have.attr',
                'href',
                'https://step.chasseneuil/photo-avant.jpg'
            )
    })
})

// On ne renvoie plus les interventions terminées pour le moment pour des raisons de performances
describe.skip("Depuis le détail d'un équipement je peux voir les OT términées récement", () => {
    it("Accède a une intervention terminée depuis la vue détails d'un équipement.", () => {
        const detailsEquipementUrl =
            '/app/interventions/step/equipements/equipement-avec-enfants'
        cy.visit(detailsEquipementUrl)
        cy.getBySel('intervention-column-closed')
            .click()
            .within(() => {
                cy.getBySel('listItem-Component-inter-cloturé-seed')
                    .should('exist')
                    .click()
            })
        cy.getBySel('current-status').should('have.text', 'Réalisé')
    })
})

describe("Depuis la vue détail d'une demande d'intervention, je peux téléphoner à son demandeur", () => {
    it("Accède à une demande d'intervention et contacte le demandeur", () => {
        const demandeInterventionDetails =
            '/app/interventions/step/demandes-interventions/demande-intervention-avec-url-gmao-seed'
        cy.visit(demandeInterventionDetails)
        cy.getBySel('phone-link')
            .should('contain', 'John Doe')
            .invoke('attr', 'href')
            .should('not.be.empty')
    })
})

describe("Sur les détails d'une intervention, je peux voir les informations des demandes qui lui sont liés", () => {
    it("Consulte la page de détails de l'intervention pour vérifier les données relatives aux demandes liés", () => {
        const interventionDetails =
            'app/interventions/step/interventions/inter-avec-activite-seed'
        cy.visit(interventionDetails)
        cy.getBySel('linked-demandes-info').should('exist')
        cy.getBySel('linked-demande-info-DI-59487').within(() => {
            cy.getBySel('code').contains('DI-59487')
            cy.getBySel('title').contains(
                'Inspection et maintenance de la cuve de décantation'
            )
            cy.getBySel('description').contains(
                "Vérifier l'état général et assurer le fonctionnement optimal."
            )
            cy.getBySel('date-and-creator').should('exist')
        })
    })

    it("Consulte la page de détails de l'intervention pour vérifier les documents liés", () => {
        const interventionDetails =
            'app/interventions/step/interventions/inter-avec-activite-seed'
        cy.visit(interventionDetails)

        // Ouvre la liste des documents
        cy.getBySel('documents-intervention-button')
            .should('contain', 'Documents')
            .click()

        // Vérifie que chaque lien a un attribut href
        cy.getBySel('download-documents-list')
            .find('a')
            .each(($link) => {
                cy.wrap($link).should('have.attr', 'href')
            })

        // Vérifie l'URL d'un document spécifique
        cy.getBySel('download-documents-list')
            .contains('Photo avant')
            .should(
                'have.attr',
                'href',
                'https://interventions.step/photo-avant.jpg'
            )
    })
})
describe("En tant qu'agent je peux editer le titre d'une intervention", () => {
    const créerInterventionUrl = 'app/interventions/step/interventions/creer'
    const modifierInterventionUrl =
        'app/interventions/step/interventions/inter-avec-activite-seed/modifier'
    it('Peut créer une intervention avec un titre custom', () => {
        cy.visit(créerInterventionUrl)
        cy.getBySel('select-equipement').click()
        cy.get('.v-autocomplete__content .v-list-item').first().click()
        cy.getBySel('switch-auto-title').click()
        cy.getBySel('submit-intervention-button').should('be.disabled')
        cy.getBySel('title').type('Titre Custom')
        cy.getBySel('submit-intervention-button').should('be.enabled')
        cy.getBySel('submit-intervention-button').click()
        cy.getBySel('snackbar').should(
            'include.text',
            'Intervention "Titre Custom"'
        )
    })
    it("Peut modifier le titre d'une intervention avec un titre custom", () => {
        cy.visit(modifierInterventionUrl)
        cy.getBySel('title').type('{selectAll}Titre Custom')
        cy.getBySel('submit-intervention-button').click()
        cy.getBySel('snackbar').should(
            'include.text',
            'Intervention "Titre Custom"'
        )
    })
    it('Peut basculer entre titre custom et auto sans perdre le titre renseigné', () => {
        cy.visit(créerInterventionUrl)
        cy.getBySel('switch-auto-title').click()
        cy.getBySel('title').type('Titre Custom')
        cy.getBySel('title').within(() => {
            cy.get('.v-field__input').should('have.value', 'Titre Custom')
        })
        cy.getBySel('switch-auto-title').click()
        cy.getBySel('title').within(() => {
            cy.get('.v-field__input').should('have.value', '')
        })
        cy.getBySel('switch-auto-title').click()
        cy.getBySel('title').within(() => {
            cy.get('.v-field__input').should('have.value', 'Titre Custom')
        })
    })
})

describe("Depuis le détail d'un équipement je peux voir la liste des équipements enfants", () => {
    it("Accède à la liste des équipements enfants depuis la vue détails d'un équipement et clique sur l'enfant cuve de decantation.", () => {
        const detailsEquipementUrl =
            '/app/interventions/step/equipements/equipement-avec-enfants'
        cy.visit(detailsEquipementUrl)
        cy.getBySel('child-equipements').within(() => {
            cy.getBySel('see-more-child-equipements-btn').click()
            cy.getBySel('child-equipement-cuve-de-decantation')
                .should('contain.text', 'Cuve de décantation - EQPT-710997')
                .click()

            cy.url().should(
                'eq',
                Cypress.config().baseUrl +
                    '/app/interventions/step/equipements/cuve-de-decantation'
            )
        })
    })
})

describe("Depuis le détail d'un équipement je accèder à son arborescence", () => {
    it("Accède à un équipement parent dans le fil d'ariane", () => {
        const detailsEquipementUrl =
            '/app/interventions/step/equipements/equipement-avec-enfants'
        cy.visit(detailsEquipementUrl)
        cy.getBySel('breadcrumb-arborescence-principale').within(() => {
            cy.getBySel('equipement-parent')
                .should('contain.text', 'Parent')
                .click()

            cy.url().should(
                'eq',
                Cypress.config().baseUrl +
                    '/app/interventions/step/equipements/equipement-parent'
            )
        })
    })
})

describe("En tant qu'agent, je peux renseigner la date de début d'une activité", () => {
    it('Ne peux pas renseigner une date de début dans le future', () => {
        cy.visit(solderInterventionUrl)
        cy.getBySel('sauvegarder-intervention').should('be.disabled')
        cy.getBySel('hours-spend-input').find('input').type('1')
        cy.getBySel('sauvegarder-intervention').should('be.enabled')
        cy.getBySel('date-de-début-input-datetime').type('9999-06-01T08:30')
        cy.getBySel('sauvegarder-intervention').should('be.disabled')
    })
})
