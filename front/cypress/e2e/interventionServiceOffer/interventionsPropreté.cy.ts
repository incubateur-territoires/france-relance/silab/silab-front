import { scrollTotheEndOfInfiniteScroll } from './helper'

function clearFilterFromListView() {
    cy.get('body').then(($body) => {
        if ($body.find('[data-cy^="filters-chip"]').length) {
            cy.getBySel('filters-chip').click('right')
        }
    })
}

describe("En tant qu'agent propreté, je peux créer des interventions", () => {
    const interventionCreationUrl =
        '/app/interventions/proprete/interventions/creer'

    it("Peut accéder au formulaire de création d'intervention", () => {
        cy.visit('/')

        cy.contains(
            '[data-cy^="component-ServiceOfferComponent"]',
            'Propreté'
        ).click()
        cy.getBySel('primary-action-button').click()
        cy.url().should(
            'eq',
            Cypress.config().baseUrl + interventionCreationUrl
        )
    })

    it('Peut créer une intervention avec photo avant en étant géolocalisé', () => {
        cy.visit(interventionCreationUrl, {
            // Hook qui s'exécute avant que la page soit chargée, il modifie le comportement de l'objet navigator qui est extrait de l'objet contextuel et utilisé pour modifier le comportement de la géolocalisation
            onBeforeLoad({ navigator }) {
                // Crée un substitut (stub) pour la méthode getCurrentPosition de l'objet navigator.geolocation, qui remplace la fonction d'origine et permet de contrôler son comportement lorsqu'elle est appelée
                cy.stub(
                    navigator.geolocation,
                    'getCurrentPosition'
                    // Index d'argument de 0, cela indique à Cypress de prendre le premier argument du callback et de le remplacer par les valeurs spécifiées
                ).callsArgWith(0, {
                    coords: {
                        latitude: 46.584545,
                        longitude: 0.338348,
                        accuracy: 5,
                    },
                })
                cy.stub(
                    navigator.geolocation,
                    'watchPosition'
                    // Index d'argument de 0, cela indique à Cypress de prendre le premier argument du callback et de le remplacer par les valeurs spécifiées
                ).returns(1)
            },
        })
        cy.getBySel('photos-avant').within(() => {
            cy.getBySel('upload-image-button').selectFile(
                'src/assets/image-test.jpg',
                { force: true }
            )
        })
        cy.getBySel('component-MapPickerComponent').click('left')
        cy.getBySel('description').type('Liquide au sol')
        cy.getBySel('action-type').contains('Laveuse').click()
        cy.getBySel('priority-normal').should('include.text', 'normal')
        cy.getBySel('title').within(() => {
            cy.get('.v-field__input').should(
                'have.value',
                'Laveuse Liquide au sol'
            )
        })
        cy.getBySel('submit-intervention-button').click()
        cy.getBySel('snackbar').contains(
            'Intervention "Laveuse Liquide au sol" créée'
        )

        // On vérifie que l'intervention qui a été créée s'ajoute bien au début de la liste
        clearFilterFromListView()

        cy.getByRootSel('listItem-Component')
            .first()
            .getBySel('illustration')
            .should('exist')
        cy.getByRootSel('listItem-Component')
            .first()
            .getBySel('title')
            .contains('Laveuse Liquide au sol')
        cy.getByRootSel('listItem-Component')
            .first()
            .getBySel('subtitle')
            .should('include.text', 'Liquide au sol')
        cy.getByRootSel('listItem-Component')
            .first()
            .getBySel('priority')
            .should('not.exist')
        cy.getByRootSel('listItem-Component')
            .first()
            .getBySel('anteriority')
            .invoke('text')
            .should('match', /il y a .+ seconde.*/)
    })

    it("Peut créer une intervention sans photo après avoir refusé d'être géolocalisé", () => {
        // Cas où la géolocalisation est prise en charge par le navigateur, mais que l'utilisateur a refusé la géolocalisation
        cy.visit(interventionCreationUrl, {
            onBeforeLoad({ navigator }) {
                cy.stub(navigator.permissions, 'query')
                    .withArgs({ name: 'geolocation' })
                    .resolves({ state: 'denied' })
            },
        })
        cy.getBySel('component-MapPickerComponent').click()
        cy.getBySel('description').type('Liquide au sol')
        cy.getBySel('action-type').contains('Laveuse').click()
        cy.getBySel('priority-normal').should('include.text', 'normal')
        cy.getBySel('submit-intervention-button').click()

        // On vérifie que l'intervention qui a été créée s'ajoute bien au début de la liste
        clearFilterFromListView()

        cy.getByRootSel('listItem-Component')
            .first()
            .getBySel('no-image-icon')
            .should('exist')
        cy.getByRootSel('listItem-Component')
            .first()
            .getBySel('title')
            .contains('Laveuse')
        cy.getByRootSel('listItem-Component')
            .first()
            .getBySel('subtitle')
            .should('include.text', 'Liquide au sol')
        cy.getByRootSel('listItem-Component')
            .first()
            .getBySel('priority')
            .should('not.exist')
        cy.getByRootSel('listItem-Component')
            .first()
            .getBySel('anteriority')
            .invoke('text')
            .should('match', /il y a .+ seconde.*/)
    })

    it('Peut créer une intervention avec photo avant et après', () => {
        cy.visit(interventionCreationUrl, {
            // Hook qui s'exécute avant que la page soit chargée, il modifie le comportement de l'objet navigator qui est extrait de l'objet contextuel et utilisé pour modifier le comportement de la géolocalisation
            onBeforeLoad({ navigator }) {
                // Crée un substitut (stub) pour la méthode getCurrentPosition de l'objet navigator.geolocation, qui remplace la fonction d'origine et permet de contrôler son comportement lorsqu'elle est appelée
                cy.stub(
                    navigator.geolocation,
                    'getCurrentPosition'
                    // Index d'argument de 0, cela indique à Cypress de prendre le premier argument du callback et de le remplacer par les valeurs spécifiées
                ).callsArgWith(0, {
                    coords: {
                        latitude: 46.584545,
                        longitude: 0.338348,
                        accuracy: 5,
                    },
                })
                cy.stub(
                    navigator.geolocation,
                    'watchPosition'
                    // Index d'argument de 0, cela indique à Cypress de prendre le premier argument du callback et de le remplacer par les valeurs spécifiées
                ).returns(1)
            },
        })
        cy.getBySel('photos-avant').within(() => {
            cy.getBySel('upload-image-button').selectFile(
                'src/assets/image-test.jpg',
                { force: true }
            )
        })
        cy.getBySel('photos-après').within(() => {
            cy.getBySel('upload-image-button').selectFile(
                'src/assets/image-test.jpg',
                { force: true }
            )
        })
        cy.getBySel('component-MapPickerComponent').click('left')
        cy.getBySel('description').type('Liquide au sol')
        cy.getBySel('action-type').contains('Laveuse').click()
        cy.getBySel('priority-normal').should('include.text', 'normal')
        cy.getBySel('submit-intervention-button').click()
        cy.getBySel('snackbar').contains(
            'Intervention "Laveuse Liquide au sol" créée'
        )

        // On vérifie que l'intervention qui a été créée s'ajoute bien au début de la liste
        clearFilterFromListView()

        cy.getByRootSel('listItem-Component')
            .first()
            .wait(1000) // quand on appui trop vite leaflet envoie une erreur console
            .click()

        cy.getBySel('image').within(() => {
            cy.getBySel('image-categorie').should('have.text', 'photo avant')
        })
        cy.getBySel('carousel-next-btn').click()
        cy.getBySel('image')
            .last()
            .within(() => {
                cy.getBySel('image-categorie').should(
                    'have.text',
                    'photo après'
                )
            })
    })
})

describe("En tant qu'agent propreté, je peux agir sur le cycle de vie des interventions", () => {
    const interventionsListUrl = '/app/interventions/proprete'

    it("Peut accéder a la liste d'interventions", () => {
        cy.visit('/')

        cy.contains(
            '[data-cy^="component-ServiceOfferComponent"]',
            'Propreté'
        ).click()
        cy.url().should('eq', Cypress.config().baseUrl + interventionsListUrl)
    })

    it("Depuis la liste les informations de l'intervention au triangle d'or sont visibles", () => {
        cy.visit(interventionsListUrl)
        clearFilterFromListView()
        scrollTotheEndOfInfiniteScroll()
        cy.getBySel('listItem-Component-ot-proprete-6').within(() => {
            cy.getBySel('title').contains("OT-150000 Triangle d'or")
            cy.getBySel('subtitle').contains('Enlèvement des tags')
            cy.getBySel('illustration').scrollIntoView()
            cy.getBySel('component-chip').should('not.exist')
        })
    })

    it("Peut accèder aux détails de l'intervention des trois cités", () => {
        cy.visit(interventionsListUrl)
        scrollTotheEndOfInfiniteScroll()
        cy.getBySel('listItem-Component-ot-proprete-1').scrollIntoView().click()
        cy.getBySel('image').should('be.visible')
        cy.getBySel('priority-urgent').should('include.text', 'urgent')
        cy.getBySel('current-status').should('include.text', 'A faire')
        cy.getBySel('action-type').should('include.text', 'Balayage manuel')
        cy.getBySel('title').should('include.text', 'Trois cités')
        cy.getBySel('gmaoObjectViewUrl')
            .should('include.text', 'Carl PROD')
            .should(
                'have.attr',
                'href',
                'https://ceci-est-un-lien-gmao-veuillez-circuler/ot-proprete-1'
            )
        cy.getBySel('date-and-creator').should(
            'include.text',
            '25/08/22 par John Doe'
        )
        cy.getBySel('address').should(
            'include.text',
            '49 Rue Puvis de Chavannes 86000 Poitiers'
        )
        cy.getBySel('description').should('include.text', 'Lavage et balayage')
    })

    it("Dans la vue détail d'une intervention le liens vers la gmao ne s'affiche que en desktop", () => {
        cy.visit('/app/interventions/proprete/interventions/ot-proprete-1')
        cy.get('a[data-cy="gmaoObjectViewUrl"]').should('exist')
        cy.viewport('samsung-s10')
        cy.get('a[data-cy="gmaoObjectViewUrl"]').should('not.exist')
    })

    it("On vérifie qu'une intervention soldée n'apparaît pas dans la liste", () => {
        cy.visit(interventionsListUrl)
        scrollTotheEndOfInfiniteScroll()

        cy.getBySel('listItem-Component-ot-proprete-10').should('not.exist')
    })

    it('Peut solder une intervention depuis la vue détail', () => {
        cy.visit(interventionsListUrl)

        cy.visit('/app/interventions/proprete/interventions/ot-proprete-1')

        cy.getBySel('traiter-intervention-button').click()
        cy.getBySel('activite-conforme').click({
            multiple: true,
        })
        cy.getByRootSel('activite-action').click({
            multiple: true,
        })
        cy.getBySel('intervenant-disponible-test.silab@grandpoitiers.fr').click(
            50,
            15
        )

        cy.getBySel('solder-intervention-button').click()

        cy.getBySel('snackbar').contains('Intervention "Trois cités" terminée')
        cy.getBySel('snackbar-button').should('exist').click()
        cy.getBySel('snackbar').should('not.exist')
        cy.getBySel('listItem-Component-ot-proprete-1').should('not.exist')
    })

    it('Peut voir les interventions à faire sur une carte depuis la page de liste des interventions', () => {
        // Note test abandonné car dépassement timebox
        cy.visit(interventionsListUrl)
        cy.get(`[data-cy^=listItem-Component-]`)
            .first()
            .invoke('attr', 'data-cy')
        // .then((interventionItemCypressIdentifier) => {
        //     const objectId = /-(.+)$/.exec(
        //         interventionItemCypressIdentifier
        //     )[1] as string
        //     //cy.get(`[alt=map-marker-object-${objectId}]`)// ne marche pas car dans iframe, piste : https://www.cypress.io/blog/2020/02/12/working-with-iframes-in-cypress/
        // })
    })

    it("Peut modifier les informations d'une intervention", () => {
        cy.visit(interventionsListUrl)
        scrollTotheEndOfInfiniteScroll()
        cy.getBySel('items-list-desktop-items').then(() => {
            cy.getByRootSel('listItem-Component').last().click()
            cy.getBySel('edit-intervention-button').click()

            cy.getBySel('submit-intervention-button').should('not.be.disabled')

            cy.getBySel('action-type').contains('Soufflage feuille').click()

            cy.getBySel('priority-bas').click()

            cy.getBySel('description').clear().type('Beaucoup de feuilles')
            cy.getBySel('title').clear().type('Trois cités')

            cy.getBySel('component-MapPickerComponent')
                .scrollIntoView()
                .trigger('mousedown', { button: 0 })
                .trigger('mousemove', { clientX: 200, clientY: 200 })
                .trigger('mouseup', { force: true })
                .click(200, 200)

            cy.getBySel('submit-intervention-button').click()

            cy.getBySel('snackbar').should(
                'include.text',
                'Intervention "Trois cités"'
            )

            cy.getBySel('snackbar-button').should('exist').click()
            cy.getBySel('snackbar').should('not.exist')

            cy.getBySel('title').should('include.text', 'Trois cités')
            cy.getBySel('image').should('be.visible')
            cy.getBySel('priority-bas').should('include.text', 'bas')
            cy.getBySel('current-status').should('include.text', 'A faire')
            cy.getBySel('action-type').should(
                'include.text',
                'Soufflage feuille'
            )
            cy.getBySel('description').should(
                'include.text',
                'Beaucoup de feuilles'
            )
        })
    })
})

describe("En tant qu'agent propreté, je peux affiner la liste des interventions", () => {
    const interventionsListUrl = '/app/interventions/proprete'

    it('Peut filtrer la liste', () => {
        let listLenghtOriginal: number
        cy.visit(interventionsListUrl)
        clearFilterFromListView()
        cy.getBySel('nb-items-quantity')
            .invoke('text')
            .then((quantityText) => {
                listLenghtOriginal = Number(quantityText)
            })
        cy.getBySel('btn-tune').click()
        cy.getBySel('status-filter')
            .first()
            .within(() => {
                cy.get('.v-label--clickable').click()
            })
        cy.getBySel('is-loading-item-list').should('exist')
        cy.getBySel('is-loading-item-list').should('not.exist')

        cy.getBySel('nb-items-quantity')
            .invoke('text')
            .then((quantityText) => {
                const newLength = Number(quantityText)
                cy.wrap(newLength).should('be.greaterThan', listLenghtOriginal)
            })
    })

    it('Peut trier la liste par date de modification', () => {
        cy.visit(interventionsListUrl)
        clearFilterFromListView()
        cy.getBySel('btn-tune').click()
        cy.getBySel('order-by-select').scrollIntoView().click()
        cy.get('.v-select__content .v-list-item').eq(1).click()

        cy.getByRootSel('listItem-Component')
            .first()
            .should('have.attr', 'data-cy')
            .and('equal', 'listItem-Component-ot-proprete-3')
    })

    it('Peut trier la liste par priorité', () => {
        cy.visit(interventionsListUrl)
        clearFilterFromListView()
        cy.getBySel('btn-tune').click()
        cy.getBySel('order-by-select').scrollIntoView().click()
        cy.get('.v-select__content .v-list-item').eq(3).click()

        cy.getByRootSel('listItem-Component')
            .first()
            .should('have.attr', 'data-cy')
            .and('equal', 'listItem-Component-ot-proprete-9')
    })
})

// Cette fonctionnalité n'est pas testé car nous n'avons pas configuré Cypress et Mirage afin de mettre à jour des routes directement dans les tests
// describe("En tant que visiteur de l'offre de service Propreté, je n'ai pas accès à l'affichage des actions qui ne me sont pas autorisés", () => {
//     const interventionsListUrl =
//         '/app/interventions/proprete'

//    it('Peut filtrer la liste', () => {
//         cy.visit(interventionsListUrl)

//         cy.request({
//             method: 'PATCH',
//             url: 'https://localhost:5001/api/users/1',
//             body: {
//                 roles: [],
//                 reachableRoles: [],
//             },
//         }).then((response) => {})
//     })
// })
