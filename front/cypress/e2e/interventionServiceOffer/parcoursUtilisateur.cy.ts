describe(
    "Parcours d'un utilisateur d'une offre de service intervention de type STEP",
    { testIsolation: false },
    () => {
        const vueGlobaleStepUrl =
            '/app/interventions/step/map?tab=interventions&filter-panel=false'
        it("Rentre sur l'offre de service", () => {
            cy.clearAllLocalStorage()
            cy.visit('/')
            cy.contains(
                '[data-cy^="component-ServiceOfferComponent"]',
                'STEP - Chasseneuil'
            )
                .scrollIntoView()
                .click()
            cy.url().should('eq', Cypress.config().baseUrl + vueGlobaleStepUrl)
        })
        it('change le type de fond de carte', () => {
            Cypress.on('uncaught:exception', () => {
                // Permet au test de continuer à s'exécuter même s'il y a des erreurs dans la console
                return false
            })
            cy.getBySel('component-item-map-viewer').within(() => {
                cy.get('.leaflet-control-layers-toggle').click()
                // on vérifie que la vue standard est bien activée
                cy.get('.leaflet-control-layers-base label')
                    .contains('Vue Standard')
                    .siblings('.leaflet-control-layers-selector')
                    .should('be.checked')
                // on active la vue satellite
                cy.get('.leaflet-control-layers-base label')
                    .contains('Vue Satellite')
                    .siblings('.leaflet-control-layers-selector')
                    .click()
                // on vérifie que la vue standard est bien desactivée
                cy.get('.leaflet-control-layers-base label')
                    .contains('Vue Standard')
                    .siblings('.leaflet-control-layers-selector')
                    .should('not.be.checked')
            })
            // on rafraichit et on vérifie que la vue standard soit toujours desactivée
            cy.reload()
            cy.get('.leaflet-control-layers-base label')
                .contains('Vue Standard')
                .siblings('.leaflet-control-layers-selector')
                .should('not.be.checked')
        })
    }
)
