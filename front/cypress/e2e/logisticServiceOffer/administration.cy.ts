describe("En tant qu'administrateur d'une ODS de type logistique, je peux modifier le type d'intervention", () => {
    const silabLogisticServiceOfferAdminListUrl =
        '/app/stock/stock-et-magasin-general/admin/type-inventaire'
    it("Peut accéder à l'onglet de l'administration dans l'ODS", () => {
        cy.viewport(1920, 1080)
        cy.visit('/app/stock/stock-et-magasin-general/admin/utilisateurs')

        cy.getBySel(
            'btn-silab-admin-menu-item-logistic-service-offer/admin/type-inventaire'
        )
            .should('be.visible')
            .click()

        cy.url().should(
            'eq',
            Cypress.config().baseUrl + silabLogisticServiceOfferAdminListUrl
        )
    })
    it("Peut changer le type d'inventaire en trimestriel", () => {
        cy.visit(silabLogisticServiceOfferAdminListUrl)

        cy.getBySel('select-inventory-type').should('exist').click()
        cy.contains('annuel').click()

        cy.getBySel('submit-inventory-type').should('exist').click()

        cy.contains("Type d'inventaire modifié en inventaire annuel")

        cy.getBySel('app-name').click()
        cy.getBySel(
            'component-ServiceOfferComponent-stock-et-magasin-general'
        ).click()
        cy.getBySel('btn-stock-faire-inventaire').click()

        cy.contains("Débuter l'inventaire annuel").should('be.visible')
    })
})
