describe("En tant que magasinier je peux faire l'inventaire annuel", () => {
    const offreDeServiceLogistiqueUrl = '/app/stock/stock-et-magasin-general'
    const selectionArticleUrl = '/app/stock/stock-et-magasin-general/inventaire'
    const inventaireArticleUrl =
        '/app/stock/stock-et-magasin-general/inventaire/ART-TEST'

    it("Peut accéder à l'inventaire", () => {
        cy.visit(offreDeServiceLogistiqueUrl)
        cy.getBySel('btn-stock-faire-inventaire').click()
        cy.url().should('eq', Cypress.config().baseUrl + selectionArticleUrl)
    })
    it('Peut selectionner un article à inventorier', () => {
        cy.visit(selectionArticleUrl)
        cy.getBySel('btn-inventorier').should('be.disabled')
        cy.getBySel('champ-de-saisie-de-code-article').type('ART-TEST{enter}')
        cy.url().should('eq', Cypress.config().baseUrl + inventaireArticleUrl)
    })
    it("Peut voir les informations necessaires depuis la vue d'inventaire d'un article", () => {
        cy.visit(inventaireArticleUrl)
        cy.getBySel('titre-article').should('include.text', 'ART-TEST')
        cy.getBySel('emplacement-article').should('exist')
        cy.getBySel('quantité-article').should('exist')
    })
    it('Peut enregistrer la valeur de stock relevé', () => {
        cy.visit(inventaireArticleUrl)
        cy.getBySel('btn-enregistrer-valeur-stock').should('be.disabled')
        cy.getBySel('quantite-initiale').then(($quantiteInitialeElem): void => {
            cy.wrap(Number($quantiteInitialeElem.text())).as('quantitéInitiale')
        })
        cy.getBySel('valeur-stock-relevee').type('12') // Idéalment on devrait taper depuis n'importe ou car le focus est normalement déjà fait, mais on y arrive pas
        cy.getBySel('btn-enregistrer-valeur-stock').should('not.be.disabled')
        cy.getBySel('btn-enregistrer-valeur-stock').click()
        cy.url().should('eq', Cypress.config().baseUrl + selectionArticleUrl)

        cy.get('@quantitéInitiale').then((quantitéInitiale) => {
            const snackbarMessageRegex = new RegExp(
                `${12}.+enregistré pour l'article ART-TEST, ancienne quantité ${quantitéInitiale}.*`
            )

            cy.getBySel('snackbar')
                .should('exist')
                .invoke('text')
                .should('match', snackbarMessageRegex)
        })

        // on vérifie la quantité sortie
        cy.getBySel('champ-de-saisie-de-code-article').type('ART-TEST')
        cy.getBySel('btn-inventorier').click()
        cy.getBySel('quantite-initiale').should('have.text', '12')
    })
    it('Ne peut pas valider une relève avec la touche entrée', () => {
        cy.visit(inventaireArticleUrl)
        cy.getBySel('valeur-stock-relevee').type('20{enter}')
        cy.url().should('eq', Cypress.config().baseUrl + inventaireArticleUrl)
    })
    it('Un article sans emplacement est indiqué explicitement', () => {
        cy.visit(
            '/app/stock/stock-et-magasin-general/inventaire/ART-SANSEMPLACEMENT'
        )
        cy.getBySel('emplacement-article').contains('(sans emplacement)')
    })
    it('Peut configurer un préfixe de code article pour faciliter sa saisie', () => {
        cy.visit(selectionArticleUrl)
        cy.getBySel('champ-de-saisie-de-code-article')
            .get('.v-field__append-inner')
            .click()
        cy.getBySel('codearticle-prefix-input').type('ART-')
        cy.getBySel('close-input-configuration-dialog').click()
        cy.getBySel('champ-de-saisie-de-code-article').type('TEST{enter}')
        cy.url().should('eq', Cypress.config().baseUrl + inventaireArticleUrl)
    })
    it('Conserve le préfixe de code article au cours de la navigation', () => {
        cy.visit(selectionArticleUrl)
        cy.getBySel('champ-de-saisie-de-code-article')
            .get('.v-field__append-inner')
            .click()
        cy.getBySel('codearticle-prefix-input').type('ART-')
        cy.getBySel('close-input-configuration-dialog').click()
        cy.visit(offreDeServiceLogistiqueUrl)
        cy.visit(selectionArticleUrl)
        cy.getBySel('champ-de-saisie-de-code-article')
            .get('.v-text-field__prefix__text')
            .contains('ART-')
    })
    it("Peut voir l'historique des mouvements", () => {
        cy.visit(inventaireArticleUrl)
        cy.getBySel('historique-des-mouvements')
            .getBySel('mouvement-de-l-historique')
            .should('have.length.gte', 1)
            .first()
            .getBySel('titre-du-mouvement')
            .getBySel('qtt-avant-mouvement')
            .getBySel('qtt-mouvement')
            .getBySel('qtt-apres-mouvement')
    })
})
