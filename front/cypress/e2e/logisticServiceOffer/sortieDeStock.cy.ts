const offreDeServiceLogistiqueUrl = '/app/stock/stock-et-magasin-general'
describe("En tant qu'agent de magasin, je peux trouver une demande de fourniture", () => {
    const detailInterventionUrlRegex =
        /.+\/app\/stock\/stock-et-magasin-general\/interventions\/[a-zA-Z0-9-]+/

    it('Acceder à la liste des interventions ayant des demandes de fourniture.', () => {
        cy.visit(offreDeServiceLogistiqueUrl)
        cy.getByRootSel('listItem-Component')
            .first()
            .within(() => {
                cy.getBySel('title').should('exist')
                cy.getBySel('subtitle').should('exist')
                cy.getBySel('anteriority').should('exist')
            })
    })

    it('Acceder à une intervention', () => {
        cy.visit(offreDeServiceLogistiqueUrl)
        cy.getByRootSel('listItem-Component').first().click()
        cy.url().should('match', detailInterventionUrlRegex)
    })

    it('Filtrer la liste des interventions par un code.', () => {
        cy.visit(offreDeServiceLogistiqueUrl)
        cy.getBySel('filtre-code').type('OT-5')
        cy.getByRootSel('listItem-Component').each(($intervention) => {
            cy.wrap($intervention)
                .getBySel('title')
                .should('contain.text', 'OT-5')
        })
    })

    it('Filtrer la liste des interventions par un titre.', () => {
        cy.visit(offreDeServiceLogistiqueUrl)
        cy.getBySel('filtre-titre').type('er')
        cy.getByRootSel('listItem-Component').each(($intervention) => {
            cy.wrap($intervention)
                .getBySel('title')
                .should('contain.text', 'er')
        })
    })
})

describe("En tant qu'agent de magasin, je peux visualiser les détails d'une demande de fourniture", () => {
    const détailIntervention =
        'app/stock/stock-et-magasin-general/interventions/ot-stock-1'
    it("Accède aux détails de l'intervention de l'OT-747641", () => {
        cy.visit(détailIntervention)
        cy.get('[data-cy="view-ProvisionRequestDetailsView-ot-stock-1"]')
            .scrollIntoView()
            .within(() => {
                cy.get('[data-cy="code-and-title"]').should(
                    'include.text',
                    'OT-747641 cuisine centrale andersen'
                )

                cy.get('[data-cy="direction-name"]').should(
                    'include.text',
                    'Direction restauration collective'
                )
                cy.get('[data-cy="creator-name"]').should(
                    'include.text',
                    'Demandé par Jean Doux'
                )
                cy.get('[data-cy="expected-receipt-date"]')
                    .invoke('text')
                    .should('match', /\d\d\/\d\d\/\d\d/)
                cy.get('[data-cy="object-status-label"]').should(
                    'include.text',
                    'En cours'
                )
                cy.get('[data-cy="object-status-createdBy"]').should(
                    'include.text',
                    'Jean Doux'
                )
            })
        cy.get(
            '[data-cy="component-ArticleReservationListItemComponent-ot-stock-1-item-1-1"]'
        )
            .scrollIntoView()
            .within(() => {
                cy.get('[data-cy="article"]').contains(
                    'BOTTE CUISINE P 39 BLANC'
                )
                cy.get('[data-cy="unit"]').contains('Paire')
                cy.get('[data-cy="quantity"]').contains('2')
                cy.get('[data-cy="storage-location"]').contains('I2_02_A')
            })
    })
})

describe('En tant que magasinier, je peux traiter une demande de fourniture et imprimer le rapport', () => {
    const détailIntervention =
        'app/stock/stock-et-magasin-general/interventions/ot-stock-1'
    it("Sort les articles disponible de l'OT-747641, corriger la quantité d'un article (en fonction du stock réel) et imprimer le rapport à la fin", () => {
        cy.visit(détailIntervention)
        // ligne 1
        cy.get(
            '[data-cy="component-ArticleReservationListItemComponent-ot-stock-1-item-1-1"]'
        )
            .scrollIntoView()
            .within(() => {
                cy.get('[data-cy="article"]').contains(
                    'BOTTE CUISINE P 39 BLANC'
                )
                cy.get('[data-cy="quantity"]').contains('2')
                cy.get('[data-cy="quantity-to-issue"] input').should(
                    'have.value',
                    1
                )
                cy.get('[data-cy="stock-issue"]').should('exist').dblclick()
                cy.get('[data-cy="stock-issue"]').should('not.exist')
                cy.get('[data-cy="processing"]').should('exist')
                cy.get('[data-cy="processing"]').should('not.exist')
            })
        cy.get('[data-cy="snackbar-button"]').should('exist').click()
        // ligne 2
        cy.get(
            '[data-cy="component-ArticleReservationListItemComponent-ot-stock-1-item-1-2"]'
        )
            .scrollIntoView()
            .within(() => {
                cy.get('[data-cy="article"]').contains(
                    'BOTTE CUISINE P 42 BLANC'
                )
                cy.get('[data-cy="quantity"]').contains('2')
                cy.get('[data-cy="quantity-to-issue"] input')
                    .should('have.value', 2)
                    .clear()
                    .type('1')
                cy.get('[data-cy="stock-issue"]').should('exist').dblclick()
                cy.get('[data-cy="stock-issue"]').should('not.exist')
                cy.get('[data-cy="processing"]').should('exist')
                cy.get('[data-cy="processing"]').should('not.exist')
            })
        cy.get('[data-cy="snackbar"]')
            .should('exist')
            .contains('1 Paire de BOTTE CUISINE P 42 BLANC ont étés sortis')
        cy.get('[data-cy="snackbar-button"]').should('exist').click()

        cy.window().then((window) => {
            cy.stub(window, 'print').as('printWindow')
        })
        cy.get('[data-cy="print-button"]').click()
        cy.get('@printWindow').should('be.called')
    })
})

describe('En tant que magasinier, je peux réaliser des sortie de stocks', () => {
    const détailIntervention =
        'app/stock/stock-et-magasin-general/interventions/ot-stock-2'
    beforeEach(() => {
        cy.visit(détailIntervention)
    })
    it("Vérifie que l'on ne peut pas sortir l'article CHARLOTTE BLANC NON TISSEE qui a une quantitée en stock de 0", () => {
        cy.get(
            '[data-cy="component-ArticleReservationListItemComponent-ot-stock-2-item-2-1"]'
        )
            .scrollIntoView()
            .within(() => {
                cy.get('[data-cy="article"]').contains(
                    'ART-063085 CHARLOTTE BLANC NON TISSEE'
                )
                cy.get('[data-cy="quantity"]').contains('2')
                cy.get('[data-cy="stock-issue"]').should('not.exist')
                cy.get('[data-cy="quantity-to-issue"] input').should(
                    'have.value',
                    0
                )
            })
    })
    it("Réalise une sortie de stock complète pour l'article GANT ALIMENTAIRE BLEU JETABLE T8.5 TL", () => {
        cy.get(
            '[data-cy="component-ArticleReservationListItemComponent-ot-stock-2-item-2-2"]'
        )
            .scrollIntoView()
            .within(() => {
                cy.get('[data-cy="article"]').contains(
                    'GANT ALIMENTAIRE BLEU JETABLE T8.5 TL'
                )
                cy.get('[data-cy="quantity"]').contains('1')
                cy.get('[data-cy="stock-issue"]').should('exist').dblclick()
                cy.get('[data-cy="stock-issue"]').should('not.exist')
                cy.get('[data-cy="processing"]').should('exist')
                cy.get('[data-cy="processing"]').should('not.exist')
            })

        cy.get('[data-cy="snackbar-button"]').should('exist').click()
    })
    it("Réalise une sortie de stock partielle pour l'article CHAUSSURE CUISINE P 39 UNISEX", () => {
        cy.get(
            '[data-cy="component-ArticleReservationListItemComponent-ot-stock-2-item-2-4"]'
        )
            .scrollIntoView()
            .within(() => {
                cy.get('[data-cy="article"]').contains(
                    'CHAUSSURE CUISINE P 39 UNISEX'
                )
                cy.get('[data-cy="quantity"]').contains('5')
                cy.get('[data-cy="stock-issue"]').should('exist').dblclick()
                cy.get('[data-cy="stock-issue"]').should('not.exist')
                cy.get('[data-cy="processing"]').should('exist')
                cy.get('[data-cy="processing"]').should('not.exist')
            })
        cy.get('[data-cy="snackbar-button"]').should('exist').click()
    })
})
describe("En tant qu'agent de magasin, je peux voir les sorties de stock réalisées sur une intervention.", () => {
    it('Voir les fournitures sorties précédements', () => {
        cy.visit(offreDeServiceLogistiqueUrl)
        cy.getByRootSel('listItem-Component').first().click()
        cy.getBySel('mouvement-de-l-historique').first().should('exist')
    })
    it('La liste des fournitures sorties affiche les informations nécessaires.', () => {
        cy.visit(offreDeServiceLogistiqueUrl)
        cy.getByRootSel('listItem-Component').first().click()
        cy.getBySel('mouvement-de-l-historique')
            .first()
            .within(() => {
                cy.getBySel('titre-du-mouvement-de-sortie')
                    .invoke('text')
                    .should('match', /ART-\d{5}[a-zA-Z\s]+/)
                cy.getBySel('qtt-du-mouvement-de-sortie')
                    .invoke('text')
                    .should('match', /\d+/)
                cy.getBySel('unité-du-mouvement-de-sortie')
                    .invoke('text')
                    .should('match', /[a-zA-Z\s]+/)
                cy.getBySel('emplacement-du-mouvement-de-sortie')
                    .invoke('text')
                    .should('match', /[A-Z]-\d-\d{3}/)
                cy.getBySel('sous-titre-du-mouvement-de-sortie')
                    .invoke('text')
                    .should('match', /\d\d\/\d\d\/\d\d à \d\d:\d\d, par \w+/)
            })
    })
    it('Je réalise une sortie de stock, elle apparait dans la liste fournitures sorties et les reservations sont à jours', () => {
        let articleCode: string
        let quantitéArticleRéservé: string

        cy.visit(offreDeServiceLogistiqueUrl)
        cy.getByRootSel('listItem-Component').first().click()
        cy.getByRootSel('component-ArticleReservationListItemComponent')
            .first()
            .within(() => {
                cy.getBySel('code-article').then(($articleReservation) => {
                    articleCode = $articleReservation.text()
                })
                cy.getBySel('quantity').then(($articleReservation) => {
                    quantitéArticleRéservé = $articleReservation.text()
                })

                cy.get('[data-cy="quantity-to-issue"] input').clear().type('1')
                cy.getBySel('stock-issue').dblclick()
            })
        cy.getBySel('mouvement-de-l-historique')
            .first()
            .within(() => {
                cy.getBySel('titre-du-mouvement-de-sortie').should(
                    'include.text',
                    articleCode
                )
                cy.getBySel('qtt-du-mouvement-de-sortie').should(
                    'include.text',
                    '1'
                )
            })
        cy.getByRootSel('component-ArticleReservationListItemComponent')
            .first()
            .within(() => {
                cy.getBySel('code-article').should('include.text', articleCode)
                cy.getBySel('quantity').should(
                    'include.text',
                    Number(quantitéArticleRéservé) - 1
                )
            })
    })
})
describe("Lorsque l'utilisateur clique sur le bouton de la page des stocks, il est informé qu'il doit double cliquer", () => {
    const urlDUneInterventionAvecDesSortiesAFaire =
        '/app/stock/stock-et-magasin-general/interventions/ot-stock-1'
    it('Une info apparaît quand on clique une fois', () => {
        cy.visit(urlDUneInterventionAvecDesSortiesAFaire)
        cy.getBySel('snackbar').should('not.exist')
        cy.getBySel('stock-issue').first().click()
        cy.getBySel('snackbar').should(
            'include.text',
            'Il est nécessaire de double-cliquer pour sortir des articles'
        )
    })
    it("L'info n'apparaît pas quand on double-clique", () => {
        cy.visit(urlDUneInterventionAvecDesSortiesAFaire)
        cy.getBySel('stock-issue').first().dblclick()
        cy.wait(2000)
        cy.getBySel('snackbar', { timeout: 100 }).should(
            'not.include.text',
            'Il est nécessaire de double-cliquer pour sortir des articles'
        )
    })
    it("L'info n'apparaît pas quand on double-clique avant un délai d'une seconde après avoir cliqué une seule fois", () => {
        cy.visit(urlDUneInterventionAvecDesSortiesAFaire)
        cy.getBySel('snackbar').should('not.exist')
        cy.getBySel('stock-issue').first().click()
        cy.wait(500)
        cy.getBySel('stock-issue').first().dblclick()
        cy.wait(1500)
        cy.getBySel('snackbar', { timeout: 100 }).should(
            'not.include.text',
            'Il est nécessaire de double-cliquer pour sortir des articles'
        )
    })
})
