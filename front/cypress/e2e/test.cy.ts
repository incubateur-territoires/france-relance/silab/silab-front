describe("En tant qu'utilisateur, j'arrive sur une page d'accueil contenant toutes les offres de service", () => {
    it("Vérifie que les éléments essentiels de la page d'accueil soient présents", () => {
        cy.visit('/')
        cy.get('header').within(() => {
            cy.contains('Mes applications')
            // Note : cette assertion est désactivée car elle dépend de l'authent
            // cy.get('[data-cy="logged-in-display-name"]').should(
            //     'include.text',
            //     Cypress.env('azureTestDisplayName')
            // )
            cy.get('img').should('have.attr', 'src').should('include', 'logo')
            cy.getBySel('app-name').contains('Siilab')
        })
        cy.get('main').should('not.be.empty')
        cy.get('footer').should('not.be.empty')
    })
    it("En arrivant sur la page d'accueil, il y a un loader qui m'indique que les offres de service chargent", () => {
        cy.visit('/')
        cy.getBySel('loader').should('exist')
        cy.getBySel('loader').should('be.exist')
    })
})
