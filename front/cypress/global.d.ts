/* eslint-disable @typescript-eslint/no-explicit-any */
// depuis https://github.com/cypress-io/cypress-example-recipes/blob/master/examples/fundamentals__add-custom-command-ts/cypress/support/index.d.ts
// et https://stackoverflow.com/a/73018517/2474460

declare namespace Cypress {
    // eslint-disable-next-line unused-imports/no-unused-vars
    interface Chainable<Subject> {
        /**
         * Nécessite les variables d'environnement suivantes, en variable d'environnement du projet et
         * incluse dans celles de cypress depuis cypress.config.ts :
         *
         * VITE_AZURE_TENANT_ID
         * VITE_AZURE_CLIENT_ID
         * VITE_AZURE_TEST_API_SCOPES
         * VITE_AZURE_TEST_USERNAME
         * VITE_AZURE_TEST_PASSWORD
         * VITE_AZURE_TEST_CLIENT_SECRET_VALUE
         * VITE_AZURE_TEST_DISPLAY_NAME
         */
        login(): void

        /**
         * Permet de récupérer des élément marqués avec l'attribut data-cy
         *
         * from : https://github.com/cypress-io/cypress-realworld-app/blob/a4022baa8dd28a10320f6784d1c3ffe332de00a2/cypress/support/commands.ts#L34
         * plus d'infos : https://youtu.be/PPZSySI5ooc?t=1554
         */
        getBySel(
            dataTestAttribute: string,
            args?: any
        ): Chainable<JQuery<HTMLElement>>

        /**
         * Comme pour la methode getBySel mais matche également si le dataTestAttributePrefix et suivi d'autre caractères
         *
         * from : https://github.com/cypress-io/cypress-realworld-app/blob/a4022baa8dd28a10320f6784d1c3ffe332de00a2/cypress/support/commands.ts#L34
         * plus d'infos : https://youtu.be/PPZSySI5ooc?t=1554
         */
        getByRootSel(
            dataTestAttributePrefix: string,
            args?: any
        ): Chainable<JQuery<HTMLElement>>

        waitMainLoader(): Chainable<JQuery<HTMLElement>>
    }
}
