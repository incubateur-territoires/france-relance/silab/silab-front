Cypress.Commands.add('getBySel', (selector, ...args) => {
    return cy.get(`[data-cy="${selector}"]`, ...args)
})

Cypress.Commands.add('getByRootSel', (selector, ...args) => {
    return cy.get(`[data-cy^="${selector}"]`, ...args)
})

Cypress.Commands.add('waitMainLoader', () => {
    return cy.get(`[data-cy="loader"]`).should('not.exist')
})
