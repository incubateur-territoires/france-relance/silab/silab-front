import '@cypress/code-coverage/support'
// Alternatively you can use CommonJS syntax:
import './commands'

//vérification des erreurs javascript
Cypress.on('window:before:load', (win) => {
    cy.spy(win.console, 'error')
    cy.spy(win.console, 'warn')
})
