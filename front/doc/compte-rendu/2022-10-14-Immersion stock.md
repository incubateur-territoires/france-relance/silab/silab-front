# Observation des magasiniers - 14/10/2022

## Pierre / Magasin Parc de Véhicules

### Le context

Pas de Wifi dans le magasin !
3 Magasiniers, 3 PC desktop
Magasin de 8-10 travées
10 minutes par commande
Préparation sur un chariot "léger"

Evolution du métier : sur les fournitures, de moins en moins de stock GP et de plus en plus de commandes direct fournisseur (4 livraisons par jour)

Volumétries moyennes quotidiennes :

-   15 commandes sur stock
-   10 commandes "direct fournisseur"

Le boulot des agents :

-   65% d'achats (en mode acheteur)
-   35% de magasinage (entrees/sorties/gestion)

### Le boulot

2 types de sorties :

-   sortie sur OT (en fait sur OR Ordre de Réparation)
-   sortie direct comptoire (agent qui veut du fil pour sa debrousailleuse)

Préparation de commande sur stock :

-   le mecano vient avec un bon de commande (ex "un carbu ducato")
-   le magasinier ecrit un bon de sortie (liste de pièces avec ref constructeur)
-   le magasinier va dans les rayonnages avec un chariot pour prendre les pièces
-   le magasinier fait "un tas" au comptoir, en attendant que quelqu'un vienne le chercher
-   le magasinier sort les pièces du stock au niveau du logiciel (même si la commande est toujours en attente au comptoir)

Préparation de commande hors stock :

-   le mecano vient avec un bon de commande (ex "un carbu ducato")
-   le magasinier cherche la pièce dans la base interne, mais ne trouve pas
-   le magasinier cherche la pièce sur le site fournisseur et la commande (ou demande un devis)
-   le magasinier recoit la pièce, créer la pièce dans la base interne si nécessaire, la rentre en stock (logiciel), la rentre en stock (inventaire) et la sort du stock immediatement
-   le magasinier prépare la commande au comptoir

### Les besoins

Photo de la pièces que le mecano demande (besoin ponctuel)

Génération de demande de devis au fournisseur :

-   basée sur des références de pièces
-   génère un PDF
-   envoyé par mail au fournisseur

Actuellement, il y a une complexité pour créer une nouvelle référence produit dans la base (à la reception/commande d'un produit jamais eu/vu) pour être entré en stock et potentiellement (re)commandé.

Le savoir "reference exact de la pièce" est détenu

-   soit par les magasiniers (aidés de catalogues en ligne sur internet, notamment pour les matériels immatriculés ex : Autossimo)
-   soit par le mecano qui a accès à l'eclaté des pièces avec references

### Les questions

-   Est-ce qu'on peut faire un lien entre le code barre des produits et la référence constructeur
-   Comment faire en sorte de faire la différence entre 2 produits de marques différentes correspondant à la même référence constructeur
-   Est-ce qu'on peut avoir/garder l'adressage des produits sur les étagères (allee A, bac B)

## Simon / Magasin général:

-   5 magasiniers
    -   Benoit
    -   guillaume
    -   laurent
    -   jean-michel
    -   christophe
-   +1 réappro
    -   alexandre
-   +1 hors stock
    -   Eric
-   +2 responsables
    -   david
    -   christophe
-   sortie:
    -   stock
    -   hors stock
    -   direct
-   process sortie sur OT
    -   (sur pc, "salle info") : selection des réservations qu'on veut traiter
        -   sur carl pas de regroupenement par OT
        -   remarque perso: ça peut être bien de savoir rapidement quel materiel de transport on a besoin a vant d'y aller (objets lourds/vers la fin ?
    -   ("salle info") impression du bordereau
    -   (hangar) récupération chariot/transportage
    -   (hangar) récupération des produits (en partant du fond en théorie)
        -   correction des quantités sur feuille si reception partielle
    -   (sur pc, "salle info") renseignement dans carl ?
    -   entreposage ou livraison
-   pas de codes-barres sur articles hors stock
-   configuration des stocks
    -   Plus c'est stocké au fond (PB) plus c'est lourd/encombrant et on part au début de la rangée A
    -   les rangéesangées sont nommées et connues des magasiniers
-   inventaires en deux temps
-   problèmes connus:
    -   stock en temps pas-réel
        -   A prend 80 bidules dans un stock de 100 bidules
        -   B veux prendre 50 bidules, mais quand il arrive il n'y en a que 20 au lieu de 10
        -   c'est la merde #confusion
    -   passeurs de commandes qui valident pas
        -   quand on fait un DF et qu'on clique sur le bouton vert ça enregistre juste mais ça valide pas, visiblement c'ets pas très intuitif
            -   propal de david: affichage d'un message du style : "Attention la commande est seulement enregistrée mais n'est pas validée"
    -   création d'articles du style des chaussures avec pleins de pointures
        -   pas possible de dupliquer et c'est chiant ya plein à mettre
    -   problème de quantité totale commandée qui s'affiche pas sur leur BL (en cas de réception partielle)
    -   problème de recherche d'inventaire en antériorité
        -   pour un instant T uniquement les mouvements s'affichent, pas les stocks
    -   en cas de sortie directe, on n'est pas bloqué ou averti si on dépasse les crédits, peut importe le réglage du niveau de contrôle
    -   Les équipiers ne laissent pas de commentaire lors des inventaires ponctuels et c'est pas térrible en tracabilité
        -   propal de david : rendre le commentaire obligatoire
    -   pb de DF ou ça prend dans le magasin avec le plus de stock
    -   lors de commande d'article avec variantes c'est aps pratique
    -   ~ya des DF étalées dans le temps parfois, et c'est pas trop pratique je crois...
    -   problèmes de droit en édition entre magasins/direction un peu trop larges, des fois des erreurs sont faites
        -   ex: un article renommé en ".85" par une direction autre (me rapelle plus)
    -   \*signature de BL, pas pratique

## Jules / Magasin eau :

Etat matos :

-   couverture reseau :
    -   correct interieur,
    -   absente exterieur
-   Equipement pour impression d'étiquettes avec code barre en place et opérationnel.(pas utilisé actuelement car pas d'utilité).

Problèmes rencontrés avec interface CARL :

-   Etat du stock d'un produit pour le magasin visible uniquement sur le detail.
-   Listes des produits affichent le stock tous magasins confondus.
-   Dans la liste des produits a réapro. lorsqu'une commande est faite, l'item n'est pas retiré de la liste si c'est une commandes au magasin général. Pb : le collegue peut refaire la commande.
-   Pas de suivis materiel (historique),
-   Sortie :
    -   pas de photo affiché .
        ex : je sorts une boite de lingette mais il y en a trois type d'enregistré dans CARL. Pas toujours evident de savoir laquel on a.
-   Réappro :
    -   si eau a plus de stock que general demande par défaut a la réappro a l'eau. c'est une source d'erreur, ils se commande une réappro a eux même.
    -   lors du retrait le destinataire est par défaut le magasin générale alors que c'est jamai le cas.
    -   pas de fonction tansfert entre magasins.
-   lors d'une demande de retrait, si pas assez de stock aucune alerte.
-   Pas de fonctionnalité de transfert dans CARL
-   Etat de la commande pas toujours mis a jours ("soldé") par utilisateur du coup une liste des commande avec des relicats de 2016.
-   Si le magasinier remarque une erreur sur la demande de retrait il faut une qtt énorme de clic pour modifier un produit, les magasinier on une bonne connaissance métier de leurs usagers.  
    ex : il est demandé une valve M2 ealors qu'il a besoins une M3.

-   Adressage compliqué à mettre en place et maintenir
-   Lors d'une demande de retrait :
    -   la date de début de chantier définit la date de livraison par défaut --> OK
    -   la date de début est par défaut, celle du jours --> source d'erreur enorme, devrait etre vide pour forcé USER a renseigner une date correcte.
    -   lorsqu'on modifie la date de livraison, il faut le faire pour chaque items un a un -> horrible pas de modif a la volé
