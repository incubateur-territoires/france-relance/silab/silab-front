# Compte-rendu sprint review du 16/11/2022

## Notes Jules

_Agents en Intervention :_

-   Actuellement uniquement de la recherche design
-   Utiliser les maquettes produites pour le front de Silab
-   Prévoir de parler d'une mise en commun du code,

_Stock :_
_Dans la liste_

-   Ajouter le titre,
-   Ordonner de la plus proche à la plus loin,
-   Mettre la date réel,
-   Priorité à ne pas afficher,
-   Nom afficher doit être celui de la personne qui valide,
-   Réservation d'aricle : en moyenne 15 articles (de 5 à 40)
    -   n°article/référence
    -   désignation
    -   quantité demandée
    -   emplacement
    -   quantité en stock
-   Tri : Dates à venir en premier

_Propreté :_

-   prestataire rebond insertion
    -   un accès pour valider la réalisation d'un retrait d'encombrant.
-   tag -> pictavie déclaration (v1 ?)
-   céline pas de création sur silab
-   rebond insertion (en fonction du quartier, si c'est du rammassage "dépôt" sauvage)
-   besoin pictavie déclare, rebond traite, propreté contrôle
-   intervention
    -   l'adresse
    -   ce que c'est (type)
    -   description
    -   priorité
    -   ancienneté

_tag/voirie :_

-   que tout le monde puisse déclarer

_ODS signalement_

les prestas ramasseurs déclarent déjà

_Sprint suivant_

-   gérer la commande entierement sur Silab (ne pas jongler entre Carl et Silab),
-   "Goal" getsion d'une DF,
-   Détail aficher "ordonnée"

-   Silab v2 doit etre en ligne Appli exposable sur internet et sécurisé : bon indicateur

## Notes Johan

-   Les outils ont leurs limite concernant la qualité du code.

"On va faire un outil qui déchire." <3
Pierre

**Notes Simon**

-   Agent en intervention -> branding "état français" obligatoire ?
-   retro -> travail fait en réunion sur les df aurais du être fait en amont -> gaspillage de temps dev
    -   https://gitlab.com/groups/incubateur-territoires/france-relance/silab/-/epics/12
    -   https://gitlab.com/incubateur-territoires/france-relance/silab/silab-front/-/blob/main/doc/fonctionnel/stock/demande_de_fournitures.md
-   besoin de statistiques
-   trop d'"anticipation" sur besoins client à mon sens
-   mélange(inversion) des PO

## Notes Yassir

-   Stock et magasin :
    -   Ajouter le titre sur la liste
    -   Mettre la date complète (Jour-Mois-Année) sur la liste
-   Propreté :
    -   Trier les interventions sur la liste par priorité urgente à basse
