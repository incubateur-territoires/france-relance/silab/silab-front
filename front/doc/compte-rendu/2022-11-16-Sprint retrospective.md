# Compte-rendu sprint retrospective du 16/11/2022

## ☀ Soleil / ce qui rend le travail agréable

-   Bonne ambiance de l'équipe
-   Liberté globale accordée / self-management 👍
-   Appropriation partagée, avancer ensemble 👍

## 🚀 Objectifs

-   Incrément : il faut livrer !
-   Fierté : je voudrais être fier du résultat
-   Ne faire qu'un seul produit avec l'ANCT ?
-   Améliorer espace de travail (coffre-fort à bouger) :sweat_smile:
-   Plus solliciter Johan

## 😀 Eléments positifs

-   Instance de back-end :alien:
-   Connecté à Carl
-   Stakeholders positifs
-   Implication des stakeholders (participation 100% à la review)
-   Reviews quotidiennes des items/issues
-   Paire programmage AKA codévelopping
-   Moins de Figma
-   Prise de repères PO, jouer un peu avec Gitlab (montée en compétence du PO en tant que PO)
-   Le découpage de cartes plus fin
-   Plusieurs développeurs à temps plein
-   Le bon travail d'Hugo
-   Framework = moins de débats techniques
-   Api platform sans orm, c'est facile et ça marche bien
-   Qualité orthographique en forte augmentation sur GitLab
-   Rapidité api/préchauffage

## ⚓ Freins

-   Turn over chez les stakeholders
-   Ecran 1080p trop petit (à préciser la taille souhaitée)
-   Prise au sérieux de l'affinage (demander de l'aide pour faire du pair affinage)
-   Coin dodo 1 (seule) place
-   Retro -> travail fait en réunion sur les df aurais du être fait en amont -> gaspillage de temps dev
    -   https://gitlab.com/groups/incubateur-territoires/france-relance/silab/-/epics/12
    -   https://gitlab.com/incubateur-territoires/france-relance/silab/silab-front/-/blob/main/doc/fonctionnel/stock/demande_de_fournitures.md
-   trop d'"anticipation" sur besoins client à mon sens
-   Task runner (gitlab runners) trop lents (+ de ressoures sur le serveur ?)
-   Template dev necessaire sur les issues
-   Doublon api platform thunder client? (faire un benchmark ?) (https://hoppscotch.io/ equivalent **Postman** open-source)
-   Code coverage pertinent sur symfony? réponse -> faire un atelier sur les test symfony avec Johan
-   Materiel : chaise avec accoudoir réglable -> demander a la logistique/ casque audio -> ramener un perso / écran plus grand
-   Laptop trop lent et crash (par exemple : lors d'utilisation du conteneur front) : commande en cours (matos de 2eme vie)
-   Aller trop loin sur l'utilisation de Figma
-   Epic/issue, pas super lisible... à prendre en main
-   Mise en forme auto du code back naze

-   Non-respect des methodes & process (utiliser les templates de rédaction de carte)
-   Attention au temps de chargement des pages (surtout pourle 1er appel, concept à mesurer)
-   Pierre et Simon font (des fois) des divagations sympa mais superflues
-   Dispos de Fred ou Pierre --> ne pas hésiter à les solliciter
-   Compétences de l'équipe : étape 1, faire une liste des compétences/technos/methodes à maitriser pour se sentir à l'aise dans le projet Silab

## ⛰ Obstacles

-   Crainte de la hierarchie quant à la vélocité globale de l'équipe

## Pour aller plus loin en Symfony...

https://gitlab.com/incubateur-territoires/france-relance/silab/silab-back/-/merge_requests/42/diffs
