## Rôles administrables

> ## Epic goal Sécurité utilisateur & droits
>
> > Un non-dev peut administrer les utilisateurs et leurs rôles au sein de Silab. Les actions sensibles sur Silab sont impossibles sans être authentifié & autorisé spécifiquement. Silab est suffisamment sécurisé pour être exposable sur le réseau externe.
>
> ## Sprint goal
>
> > En tant qu'agent, j'ai accès à différentes ODS en fonction de qui je suis et chaque requête au back est sécurisée.
>
> Si sprint goal atteint, priorisation pressentie :
>
> -   Auth interne silab (aka multifourniseur d'authent)
> -   Interface d'administration, assignation des rôles
> -   Interface d'administration, création d'ODS
>     <https://pad.georgette.party/review-silab-08-02-2023?both>

# Demande a se connecter àu back

/api/login?type=azureAd
/api/login?type=password

## Permission

-   Action = endpoint = VERB HTTP + Route

```mermaid
classDiagram
User<--> Permission : n-n
Ressource <--> PermissionDefinition : n-1
PermissionDefinition <-- Permission
PermissionDefinition <--> ServiceOfferTemplate
PermissionDefinition <--> PermissionActionEnum : n-1
ServiceOfferTemplate <--> ServiceOffer
ServiceOffer <--> Permission
Role <--> User : n-n
Role <--> PermissionDefinition : n-n
%% User
User: string name
User: string password
%% Role (ex: Magasinier)
Role: string name
%% Permission
Permission : PermissionDefinition definition
Permission : ServiceOffer serviceOffer
Permission : User user
%% PermissionTemplate ~= action = endpoint = VERB HTTP + Route
PermissionDefinition: Ressource ressource
PermissionDefinition: PermissionActionEnum action
%% ODS
ServiceOffer: string name
ServiceOfferTemplate: string name
ServiceOfferTemplate: PermissionDefinition[] definitions
```

```mermaid
classDiagram
User<--> Permission : n-n
Ressource <--> PermissionDefinition : n-1
PermissionDefinition <-- Permission
PermissionDefinition <--> ServiceOfferType
PermissionDefinition <--> PermissionActionEnum : n-1
ServiceOffer <--> Permission
Role <--> User : n-n
Role <--> PermissionDefinition : n-n
%% User
User: string name
User: string password
%% Role (ex: Magasinier)
Role: string name
%% Permission
Permission : PermissionDefinition definition
Permission : ServiceOffer serviceOffer
Permission : User user
%% PermissionTemplate ~= action = endpoint = VERB HTTP + Route
PermissionDefinition: Ressource ressource
PermissionDefinition: PermissionActionEnum action
%% ODS
ServiceOffer: string name


ServiceOffer <-- ServiceOfferType : 1-n

ServiceOfferType <|-- LinkServiceOfferType : 1-1
ServiceOfferType <|-- StockServiceOfferType : 1-1
ServiceOfferType <|-- InterventionServiceOfferType : 1-1

Config <-- ServiceOfferType : 1-1
CarlConfig <|-- Config : 1-1

LinkServiceOfferType : string link

ServiceOffer : string title
ServiceOffer : string slug
ServiceOffer : string imageUrl
ServiceOffer : ServiceOfferType type

%% ServiceOfferType
ServiceOfferType: getLink()
ServiceOfferType: getPermissionsModels()
ServiceOfferType: string name
ServiceOfferType: PermissionDefinition[] definitions

Config: zdadzad

CarlConfig : ?[string] costCenter
CarlConfig : ?[string] supervisor
CarlConfig : ?[string] actionType
CarlConfig : ?[string] itemWarehouse


InterventionServiceOfferType: Intervention[] interventions
InterventionServiceOfferType: getInterventions()

StockServiceOfferType: ArticleReservation[] ArticleReservation
StockServiceOfferType: StockIssue[] StockIssue


```

**Exemples :**

-   User
    -   mail : Jhon.Doe@mail.fr
    -   password : 0000
-   Role :
    -   label : Magasinier
-   Ressources :
    -   label : interventionsStatus
-   PermissionActionEnum
    -   read / write
-   PermissionDefinition :
    -   id : WRITE_STOCK_ISSUE
    -   Ressources : stockIssue
    -   PermissionActionEnum : write
-   ServiceOfferTemplate :
    -   stock-et-magasin
    -   Array(PermissionDefinition)
        -   GET : ArticleReservation
        -   POST : ArticleReservation
        -   WRITE : stockIssue
-   ServiceOffer :
    -   eau
-   Permission : administrable
    -   Jhon.Doe@mail.fr
    -   WRITE_STOCK_ISSUE

### Refacto ODS

```mermaid
classDiagram
ServiceOffer <-- Intervention : 1-1
ServiceOffer <-- StockEtMagasin : 1-1
ServiceOffer <-- linkV1 : 1-1

linkV1 : string link

ServiceOffer : string title
ServiceOffer : string template
ServiceOffer : string slug
ServiceOffer : string imageUrl

Intervention : [string] costCenter
Intervention : [string] supervisor
Intervention : [string] actionType
Intervention : FK serviceOffer


StockEtMagasin : string itemWarehouse
StockEtMagasin : FK serviceOffer
```

Mapping inheritance : class table inheritance
<https://www.doctrine-project.org/projects/doctrine-orm/en/2.14/reference/inheritance-mapping.html#class-table-inheritance>

**Proposition 2**

```mermaid
classDiagram
ServiceOffer <-- ServiceOfferType : 1-n

ServiceOfferType <|-- CarlServiceOfferType : 1-1
ServiceOfferType <|-- LinkServiceOfferType : 1-1

LinkServiceOfferType : string link

ServiceOffer : string title
ServiceOffer : string template
ServiceOffer : string slug
ServiceOffer : string imageUrl
ServiceOffer : CarlServiceOfferType carlServiceOfferType

ServiceOfferType: getLink()

CarlServiceOfferType : ?[string] costCenter
CarlServiceOfferType : ?[string] supervisor
CarlServiceOfferType : ?[string] actionType
CarlServiceOfferType : ?[string] itemWarehouse
```

**Proposition 3**

```mermaid
classDiagram
ServiceOffer <|-- InterventionServiceOffer
ServiceOffer: getLink()

ServiceOffer <-- StockEtMagasinServiceOffer : 1-1
ServiceOffer <-- LinkServiceOffer : 1-1

InterventionServiceOffer <-- CarlConfig
StockEtMagasinServiceOffer <-- CarlConfig

ServiceOffer : string title
ServiceOffer : string template
ServiceOffer : string slug
ServiceOffer : string imageUrl

CarlConfig : ?[string] costCenter
CarlConfig : ?[string] supervisor
CarlConfig : ?[string] actionType
CarlConfig : ?[string] itemWarehouse

InterventionServiceOffer: Intervention[] interventions
InterventionServiceOffer: getInterventions()

StockEtMagasinServiceOffer: ArticleReservation[] ArticleReservation
StockEtMagasinServiceOffer: StockIssue[] StockIssue
```

**Proposition 4**

```mermaid
classDiagram
ServiceOffer <-- ServiceOfferType : 1-n

ServiceOfferType <|-- LinkServiceOfferType : 1-1
ServiceOfferType <|-- StockServiceOfferType : 1-1
ServiceOfferType <|-- InterventionServiceOfferType : 1-1

Config <-- ServiceOfferType : 1-1
CarlConfig <|-- Config : 1-1

LinkServiceOfferType : string link

ServiceOffer : string title
ServiceOffer : string slug
ServiceOffer : string imageUrl
ServiceOffer : ServiceOfferType type

%% ServiceOfferType
ServiceOfferType: getLink()
ServiceOfferType: getPermissionsModels()
ServiceOfferType: string name
ServiceOfferType: PermissionDefinition[] definitions

Config:

CarlConfig : ?[string] costCenter
CarlConfig : ?[string] supervisor
CarlConfig : ?[string] actionType
CarlConfig : ?[string] itemWarehouse


InterventionServiceOfferType: Intervention[] interventions
InterventionServiceOfferType: getInterventions()

StockServiceOfferType: ArticleReservation[] ArticleReservation
StockServiceOfferType: StockIssue[] StockIssue
```

**Proposition EAV :**
<https://github.com/msgphp/eav-bundle>

Dans tout les cas créer des modèles !!!

#### Exemples

##### Propreté

    - title : propreté
    - template : intervention,
    - slug : proprete
    - image : path,
    - link : null **OU** intervention/proprete
    - costCenter : ["DAEEP_PROPRETE"]
    - supervisor : ["DAEEP_PROPRETE"]
    - actionType :["SOUFFLAGE_FEUILLE", "ENCOMBRANT", "LAVEUSE", "BALAYEUSE", "BALAYAGE_MANUEL", "RAMASSAGE_HERBE", "CORBEILLE"]

##### Liens vers V1

    - title : UPPEP
    - template : linkV1
    - slug : uppep
    - image : path,
    - link : path,

### Roles

**exemples :**

-   magasinier :uk: warehouseManager ?!
    -   chef
    -   pas chef
-   agent en intervention
-   agent propreté
-   administrateur
    -   gerer les droits
-   super admin

Magasins

-   eau
-   générale (généralite et ptte un peu au dessus des ordres)
-   Station d'épuration des eaux usées
-   etc.

PermissionActionEnum:

-   read (find~, findBy~)
    -   GET
-   write (~save (dans les datas processors)
    -   POST statut de l'intervention
    -   PATCH
-   delete
    -   DELETE

article reservation = meme endpoint pour les différent magasin

Template de l'ods

-   intervention :
    -   type d'action
    -   centre de coût
    -   responsable
-   stock & magasin :
    -   intervention->reserve->item->localstorage->general/eau/step/véhicule

Exemples de permission :

-   Créer une intervention sur un espace vert.
    -   ODS = Espace vert
    -   resource = intervention
    -   action = write
-   Modifier le statut d'une intervention
    -   ODS = Propreté
    -   resource = intervention
    -   action = write

Intervention
=> centre de cout => - propreté - immobilier - voirie

-   nature d'action

    -   soufflage de feuille
    -   balayeuse
    -   taille des haies
    -   tonte

-   locationStorage

STEP : Station d'épuration des eaux usées

generique

# Contrôle des accès

authentication

manager d'offre de service ou de web service ??

-   `GET /api/ods/stock_et_magasin/{:EAU}/articl_reservation`
-   `GET /api/ods/stock_et_magasin/{:GENERAL}`

-   `GET /api/ods/{:intervention}/proprete`
-   `GET /api/ods/{:intervention}/voirie`
-   `GET /api/ods/{:intervention}/espace_vert`
-   `GET /api/ods/{:intervention}/voirie/interventions` Lister les interventions de la voirie
-   `GET /api/ods/{:intervention}/proprete/interventions/:id`

exemple de route qui existe (sur le front) actuellement sur Silab :
`/app/stock/stock-et-magasin/GENERAL`

intervention : template de l'ods
slug : id de l'ods
interventions : ressource(s) demandées

ods de type magasin
ods

<https://gitlab.com/incubateur-territoires/france-relance/silab/silab-back/-/issues/171>
