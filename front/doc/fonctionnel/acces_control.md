# Le contrôle d'accès

## Par authentification

L'accès à certaines ressources nécessitent à minima d'être connecté. Par exemple les informations de l'utilisateur (page non implémentée).

## En fonction des rôles d'accès

Les ressources plus sensibles nécessitent d'avoir des rôles d'accès spécifiques. Ces rôles sont généralement à définir pour chaque offre de service.

### Liste des rôles actuels

-   `ROLE_READ_USERS`,
-   `ROLE_WRITE_USERS`,
-   `ROLE_*serviceOfferId*_READ_SERVICE_OFFER`,
-   `ROLE_*serviceOfferId*_READ_INTERVENTIONS`,
-   `ROLE_*serviceOfferId*_CREATE_INTERVENTIONS`,
-   `ROLE_*serviceOfferId*_READ_CLOSE_INTERVENTION`,
-   `ROLE_*serviceOfferId*_READ_ARTICLE_RESERVATION`,
-   `ROLE_*serviceOfferId*_ISSUE_ARTICLE`.

### Exemple

-   `/api/service-offers/4sdi`

Pour accéder à une offre de service avec l'id `4sdi` il faut posséder le rôle `ROLE_4sdi_READ_SERVICE_OFFER`.

-   `/api/intervention-service-offers/9er3/interventions`

Pour créer une intervention sur l'offre de service ayant pour id `9er3` il faut posséder le rôle `ROLE_9er3_CREATE_INTERVENTIONS`.

## Hiérarchie des rôles

L'implémentation future d'une hiérarchie des rôles permettra de créer des rôles de type _profil_. Un utilisateur pourra avoir un ou plusieurs _profil_. Un _profil_ est un ensemble de rôles d'accès.

### Exemple

Le profil `ROLE_48e9_MAGASINIER` donnera à l'utilisateur tous les rôles d'accès nécessaire a son activité, ROLE_48e9_READ_SERVICE_OFFER, ROLE_48e9_READ_RESERVATIONS, ROLE_48e9_ISSUE_ARTICLE ...

## Administration

Actuellement, seuls les développeurs de l'application ont accès à une interface graphique permettant de paramétrer les rôles d'un utilisateur.

Dans le futur, il pourra être implémenté une interface graphique pour permettre de définir les rôles de type profil et d'associer des rôles à un utilisateur.

> Attention :
> Lors de la première connexion à silab l'utilisateur ne possède aucun droit, il faut se rapprocher de l'équipe de développeur afin de se les faire assigner.
