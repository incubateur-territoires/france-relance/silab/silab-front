# Appels au back par le front

## Types d'URI

Nomenclature [restfullAPI][]

`/api/article-reservations/:id` : **item**
`/api/article-reservations/:id/stock-issue` : **collection _(sub-ressource)_**
`/api/interventions/:id` : **item**
`/api/interventions/:id/article-reservations` : **store**
`/api/interventions/:id/solder` : **controller**
`/api/service-offers` : **collection**
`/api/interventions-service-offers/:slug` : **resource**
`/api/interventions-service-offers/:slug/interventions` : **store**
`/api/logistic-service-offers/:slug` : **item**
`/api/logistic-service-offers/:slug/interventions` : **store**

## Magasin general

Sont présentés les scénarios de requêtes pour les différentes fonctionnalités d'une offre de service logistique. Pour l'exemple, nous avons pris le magasin général.

### Affichage principal (liste d'intervention)

1. GET /api/service-offers
2. GET /api/logistic-service-offers/general/interventions
3. for each interventions from 2.
    - GET /api/interventions/186334406dd-127df?fieldset=logisticSOListItem

### Affichage d'une intervention (click d'un élément de la liste)

1. GET /api/interventions/186334406dd-127df?fieldset=logisticSODetails
2. GET /api/logistic-service-offers/general/article-reservations?filter[interventionId]=186334406dd-127df
3. for each article-reservation from 2.
    - GET /api/article-reservations/186386ab756-547c

### Sortie de stock (click du bouton 🛒)

1. POST /api/article-reservations/186386ab756-547c/issue

## Propreté

### Affichage principal (liste d'intervention)

1. GET /api/service-offers
2. GET /api/intervention-service-offers/proprete/interventions
3. for each interventions from 2.
    - GET /api/interventions/:id?fieldset=interventionSOListItem

### Affichage d'une intervention (click d'un élément de la liste)

1. GET /api/interventions/186378406dd-12gd?fieldset=interventionSODetails

### Solder une intervention

1. POST /api/interventions/186378406dd-12gd/close

### Créer une intervention ou lier une interventions existante à l'ODS

1. POST /api/intervention-service-offers/proprete/interventions

[restfullapi]: https://restfulapi.net/resource-naming/#:~:text=x-,2.%20Best%20Practices,-2.1.%20Use%20nouns
