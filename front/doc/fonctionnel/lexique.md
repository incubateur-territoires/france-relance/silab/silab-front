# Lexique

## Agent de la fonction publique territoriale (Agent)

Désigne n'importe quel salarié d'une collectivité.

## Offre de service (ODS)

Une offre de service c'est :

-   un espace de travail permettant de répondre à un besoin métier spécifique
-   un genre de petite application

Dans sa version première, **[SI]Lab** est capable de répondre aux offres de services de types suivants :

### Lien

Une carte permettant une redirection vers un site externe, contient un titre, une image et le lien de redirection

### Liste d'intervention

Une ODS permettant de consulter une liste d'Intervention, potentiellement filtrée et d'afficher les details des differentes interventions.

### Création de demande

Une ODS de type Liste d'intervention qui permet en plus de créer des demandes d'intervention.

### Création d'intervention

Similaire à l'ODS de type création de demande mais au lieu de créer une demande d'intervention elle permet de créer directement l'intervention au statut validé.

### Gestion d'interventions

Une ODS de type création de demande qui permet en plus de gérer ses interventions, CàD indiquer l'avancement de l'intervention, les travaux réalisés...

### Gestion des Stocks et magasins

Une ODS permettant au magasinier et aux agents de traiter des demandes d'articles.

Voir : [Demande de fourniture]
Voir : [Sortie de stock]

## Demande

> **Synonymes/applications :**
>
> -   :fr: Demande d'intervention (**DI**)
> -   :fr: Demande d'Achat (**DA**)
> -   :gb: Maintenance Request (**MR**)

Dans le cadre des Offres de Service lié à la GMAO, une **demande** est l'expression d'un besoin ou d'un problème, formulée par un **acteur** et à destination d'un **responsable**. Cette demande porte et/ou est liée à un **matériel**, un **équipement** ou une **zone**. Une intervention est nécéssaire pour y répondre.

**Exemple :** Un signalement d'encombrants sur la voie publique.

### Demande de niveaux 1 et 2

> Note : ce concept est potentiellement très spécifique à Grand Poitiers

-   Niveau 1 : problèmes relevant de la sécurité
-   Niveau 2 : projets

## Intervention

> **Synonymes :**
>
> -   :gb: _Working Order_ (WO)
> -   :fr: Ordre de Travail (OT)

**Exemple :** Une équipe d'agent est assignée pour l'enlèvement d'encombrant sigalés sur la voie publique.

### Statuts d'une intervention

1. **En attente de validation :** L'intervention est en attente de validation de la part du responsable
2. **En attente de réalisation :** L'intervention est validée et attent d'être commencée.
3. **En cours :** L'intervention a été commencée.
4. **En attente de récéption :** L'intervention est en attente d'une validation externe (prestataire, responsable, ...).
5. **Terminée :** L'intervention est finie et sa fin est en attente de validation par le responsable.
6. **Soldée :** La fin de l'intervention est validée par le responsable.
7. **Archivée :** L'intervention est archivée et ne pourra plus être réouverte.

Autres :

-   **Refusée :** L'intervention s'est arrétée à la première étape (n'a pas été validée)
-   **Annulée :** l'intervention s'est arrétée après la première étape.

## Demande de fourniture

## **GMAO** : Gestion de maintenance assistée par ordinateur

<https://fr.wikipedia.org/wiki/Gestion_de_maintenance_assist%C3%A9e_par_ordinateur>

-   <https://www.carl-software.fr/>

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
[demande de fourniture]: stock/demande_de_fournitures.md
[sortie de stock]: stock/sortie_de_stock.md
