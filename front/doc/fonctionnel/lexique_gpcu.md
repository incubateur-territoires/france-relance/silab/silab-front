# Lexique Grand Poitiers Communauté Urbaine (GPCu)

## Liste d'ODS Grand Poitiers

-   Elections (Resultat temps réel)
-   Aires de Jeux (des espaces verts)
-   Horodateurs & [PMV](#PMV)
-   [Centres de Ressources](#CDR--Centre-de-ressource)
-   Nettoyage des tags
-   [PR](#PR--Poste-de-Relevage)
-   [UPEP](#UPEP)
-   Stocks
-   Magasins
-   Propreté

### UPEP

Unité de production d'eau potable éventuellement **PEP** ou **PPEP**)

> _Ce sont les gens qui font que on peut boire de l'eau sans mourrir._

### PMV

> cf. [Panneau à message Variable](https://fr.wikipedia.org/wiki/Panneau_%C3%A0_messages_variables)

### CDR : Centre de ressource

> _**Note :** ce concept est potentiellement très spécifique à Grand Poitiers_.

Les 40 communes de **Grand Poitiers** ont délégués la compétence de maintenance de certaines compétences à **Grand Poitiers**. Pour certaines compétences (principalement la voirie) **Grand Poitiers** a découpé la gestion entre plusieurs entités appelées les **centres de ressources** et qui gèrent chacune leur zones géographiques/communes :

-   CDR centre
-   CDR est
-   CDR nord
-   CDR sud-ouest

### PR : Poste de Relevage

cf. [station de relevage](https://fr.wikipedia.org/wiki/Station_de_relevage)

### Intervention propreté

cf. [Définition d'un objet intervention propreté](../fonctionnel//proprete/intervention_proprete.md)

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
