# Process : La demande

## Définition

Dans le cadre des Offres de Service lié à la GMAO, une **demande** est l'expression d'un besoin ou d'un problème, formulée par un **acteur** et à destination d'un **responsable**. Cette demande porte et/ou est liée à un **matériel**, un **équipement** ou une **zone**.

## Cycle de vie d'une demande

```mermaid
flowchart TD
        direction LR
        A([Nouvelle])
        --> B[Instruction]
        B-->C([Acceptée])
        B-->D([Refusée])
        A--->E([Annulée])
        C-->F[Archivage]
        D-->F
        E-->F
        F-->G([Archivée])
```

## Les différents états d'une demande

### Cycle classique

-   `Nouvelle` : Etat de départ d'une demande
-   `Acceptée` : La demande a été acceptée/validée lors de l'étape d'instruction, une **[intervention][]** est alors liée à cette demande
-   `Refusée` : La demande a été refusée lors de l'étape d'instruction
-   `Annulée` : La demande a été annulée par _le demandeur_ et ce avant sa réalisation
-   `Archivée` : Etat final d'une demande

**Attention** à ne pas confondre le suivi de l'état d'une demande avec le suivi de l'état de l'intervention liée à la demande !

### Instruction & états intermédiaires

La demande doit passer par une étape d'**instruction** plus ou moins complexe. Le cas le plus simple étant l'acceptation automatique de la demande, il se peut que des étapes de validation soit nécessaires, apparaissent alors des états intermédiaires contextuels comme `En attente d'informations`, `En préparation budgétaire`, `Arbitrage`...

### Archivage

L'état `Archivée` est un peu particulier car il dépend des règles d'archivage établies par les responsables fonctionnels métiers. L'archivage peut être :

-   **manuel** : un acteur doit procéder à l'archivage de chaque demande
-   **automatique** : dès qu'une demande est dans un état particulier, elle est automatiquement archivée
-   **à déclenchement** : une règle temporelle ou un déclencheur extérieur vient archiver les demandes
-   **hybride** : un mix des règles ci-dessus

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
[intervention]: ./process_intervention.md
