# Process : L'intervention

## Définition

Dans le cadre des Offres de Service lié à la GMAO, une **intervention** est l'ensemble des actions et des ressources employées à la réalisation d'une tâche. Une intervention porte et/ou est liée à un **matériel**, un **équipement** ou une **zone**.

De plus, une ou plusieurs **[demandes][]** peuvent être liées à une intervention.

## Cycle de vie d'une intervention

```mermaid
flowchart TD
        direction LR
        A([Nouvelle])
        --> B[Instruction]
        B-->D([Refusée])
        B-->C([Acceptée])

        C--->E

        C-->G[Réalisation]
        -->H([Réalisée])
        H-->F[Archivage]
        D-->F
        E-->F
        F-->I([Archivée])
        A--->E([Annulée])
```

## Les différents états d'une intervention

### Cycle classique

-   `Nouvelle` : Etat de départ d'une intervention
-   `Acceptée` : L'intervention a été acceptée/validée lors de l'étape **d'instruction**
-   `Réalisée` : L'intervention est considérée comme réalisée & réceptionnée (validée à la fin de l'étape de réalisation) lors de l'étape de réalisation
-   `Refusée` : L'intervention a été refusée lors de l'étape **d'instruction**
-   `Annulée` : L'intervention a été annulée et ce avant sa **réalisation**
-   `Archivée` : Etat final d'une intervention

### Instruction, réalisation & états intermédiaires

L'intervention doit passer par une étape d'**instruction** et un étape de **réalisation**. Il se peut que des états intermédiaires contextuels soient nécessaires comme `En attente d'informations`, `Attente matériel`, `En réception` _(de travaux)_...

#### Instruction

Etape pendant laquelle un **acteur** (technicien ou responsable) s'assure que l'ensemble des informations nécessaires à la réalisation de l'intervention sont bien renseignées et, au besoin, les complète.

A la fin de cette étape, l'acteur **accepte/valide** ou **refuse** l'intervention.

#### Réalisation

Etape pendant laquelle l'intervention est effectivement exécutée. Cette exécution peut être soumise à validation, on parle alors de **réception de travaux**. Tant que la réception n'est pas positive, la réalisation continue.

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
[demandes]: ./process_demande.md
