# [SI]Lab : bureau mobile, logiciels métiers et tableaux de bord dans la poche

L’offre [SI]Lab répond à différents enjeux d’un schéma directeur des systèmes d’information et de la transformation numérique.

## Simplification & modernisation

[SI]Lab favorise la valorisation des équipes et participe à la modernisation du poste de travail des agents mobiles en simplifiant :

-   le traitement d’une liste de tâches à réaliser (gain de temps)
-   la consultation des référentiels documentaires
-   la saisie en mobilité dans des applications conçues pour des usages sédentaires afin de supprimer les ressaisies par l’encadrement intermédiaire

## Décisionnel

[SI]Lab propose une aide à la décision, au pilotage et au suivi d’activité en :

-   permettant à un public ciblé (élus, direction générale), la consultation de tableaux de bord
-   restituant tout ou partie des actions traitées ou planifiées sur le territoire

## Interopérabilité

[SI]Lab favorise l’interopérabilité organisationnelle des processus de gestion du patrimoine et des moyens généraux en s'appuyant sur les fonctionnalités métiers suivantes :

-   la gestion d’une chaîne logistique (achat, commande, stock et magasin)
-   la gestion des référentiels et des inventaires
-   la gestion d’interventions curatives, préventives ou prédictives

[SI]Lab, s’interface avec toutes applications métiers possédant des services web entrants/sortants. Il se positionne comme un orchestrateur garantissant l’interopérabilité d’un écosystème.

A ce jour, les services sont opérationnels avec les applications :

-   de gestion électronique de document (GED)
-   de service de cartographie (OpenStreetMap,…)
-   de gestion de maintenance assisté par ordinateur (GMAO) « CARL Source – Facility »
-   Active Directory pour sécuriser les accès et habiliter les utilisateurs authentifiés, qu’ils soient internes ou externes à une structure
-   natives Android/iPhone : appareil photo, dictaphone, lecture de QR code, géolocalisation

## Open Source

La publication en **Open Source** de cette application web et sa mutualisation avec d’autres structures aideront à la transformation numérique des collectivités territoriales.

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
