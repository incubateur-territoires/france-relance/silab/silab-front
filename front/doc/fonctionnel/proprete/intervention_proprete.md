# Intervention propreté

Une intervention propreté est une **intervention** :

-   rattachée au **centre de cout** DAEEP_PROPRETE
-   dont le **superviseur** est DAEEP_PROPRETE
-   dont la **nature** fait parti de la liste des natures "Voirie - Propreté"
-   qui a besoin de coordonnées X,Y (latitude, longitude)

## Données attachées aux objets GMAO

L'intervention propreté contient tous les champs classiques d'une intervention, dont notamment :

-   code
-   titre
-   domaine d'intervention : Propreté (Poitiers)
-   date debut/date fin
-   **priorité**
-   nature
-   centre de cout : (DAEEP_PROPRETE)
-   localisation : (POITIERS_PROPRETE)
-   Orga / CDR : (CDR CENTRE)
-   superviseur : (DAEEP_PROPRETE)
-   créateur/gestionnaire (**acteur**)
-   description/commentaire

Avec, en plus :

-   une photo avant (= avant intervention)
-   une nature appartenant à la liste des natures "Voirie - Propreté"
-   adresse
-   beneficiaire ("customer", code 813 = Propreté urbaine)
-   des coordonnées X,Y (longitudes & latitude)

## Sémantique CARL

| GÉNÉRIQUE    | Libellé CARL                    | Entité CARL  |
| ------------ | ------------------------------- | ------------ |
| Intervention | Intervention / Ordre de travail | wo           |
| Créateur     | Acteur                          | Actor        |
| Site         | Orga / CDR                      | site         |
| Photo avant  | Lien photo avant                | xtraTxt09    |
| Début        | Date de début                   | WOBeginTime  |
| Fin          | Date de fin                     | WOEndTime    |
| Code         | Intervention                    | code         |
| Priorité     | Priorité                        | workPriority |
| Etat         | Etat                            | statusCode   |
| Description  | Titre                           | description  |
| Commentaire  | Commentaire / Description       | longDesc     |
| Adresse      | Rue                             | xtraTxt10    |
| Mention      | Mention                         | xtraTxt03    |
| N°           | N°                              | xtraNum02    |

[⮪ Lexique spécifique Grand Poitiers Communauté Urbaine][]

[⮪ lexique spécifique grand poitiers communauté urbaine]: ../lexique_gpcu.md
