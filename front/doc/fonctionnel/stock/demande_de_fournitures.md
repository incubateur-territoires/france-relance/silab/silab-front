# Demande de fourniture (DF)

-   Une demande de fourniture est une **intervention** à laquelle sont rattaché une ou plusieurs **réservations**
-   Une réservation exprime un besoin d'une quantité d'un **article**
-   un article est un produit (**matériel** ou **consommable**)
    -   _Note: un matériel relève normalement de l'investissement (en gestion financière)_
    -   _Note: un consommable relève normalement du fonctionnement (en gestion financière)_

## Données attachées aux objets GMAO

### Demande de fourniture

Note: contient tous les champs classiques d'une intervention, dont notamment :

-   titre
-   description
-   créateur/demandeur (**acteur**)
    -   son entité de rattachement
-   _0-N_ réservations
-   localisation
-   **priorité**

### Réservation

-   article
-   quantité
-   **intervention** rattachée
-   date de réception souhaitée

# Sémantique CARL

| GÉNÉRIQUE    | Libellé CARL                    | Entité CARL |
| ------------ | ------------------------------- | ----------- |
| Intervention | Intervention / Ordre de travail | wo          |
| Réservation  | Réservation                     | Reserve     |
| Article      | Article                         | Item        |
| Créateur     | Acteur                          | Actor       |
