# Sortie de stock

-   Une sortie de stock est l'action de sortir des articles de l'inventaire d'un magasin.
    -   Ces articles peuvent être liés à une _réservation_, elle-même liée à une _Intervention_.  
        C'est le cas que nous traitons.
-   Une sortie issue d'une _réservation_ d'article peut être complète ou partielle. Dans ce dernier cas, les quantités de la réservation sont mises à jour et elle reste présente.

## Fonctionnement de CARL pour les sorties de stock

### Demande de fourniture

#### Procédure Carl

Faire une sortie de stock :

-   Trouver une réservation et notez l'OT associée,
-   Allez dans sortie, filtrez par cette OT, cliquez sur le petit lien `réservation`,
-   Allez dans résultats, cochez les réservations que vous voulez traiter et validez (bouton "enregistrer et quitter"),
-   Cocher vos réservation et mettez à jour la quantité sortie si besoin et validez (bouton "enregistrer et quitter").

#### Entités carl (en BDD)

`MOVEMENTTYPE` : type de mouvement, dans notre cas `ISSUE` (il y a par exemple des types de mouvement liés aux inventaires).

`STOCKMOVEMENT` : mouvement de stock effectif, les quantités des stocks sont alors mis à jour grâce à ces entités. Peut être lié à une intervention.

`CSST_RESERVE` : indique un besoin de sortie de stock. Peut (doit?) être lié à une intervention.

Quand une réservation est faite en totalité :

-   L'entrée dans `CSST_RESERVE` est supprimée dans la base de données.
-   Une entrée est créée dans `STOCKMOVEMENT` avec le `MOVEMENTTYPE` = `ISSUE`
    Quand une réservation est partielle : - La quantité est modifiée dans l'entrée dans `CSST_RESERVE` dans la base de données. - Une entrée est créée dans `STOCKMOVEMENT` avec le `MOVEMENTTYPE` = `ISSUE` avec la quantité sortie.

:warning: Il faut visiblement que la date de fin d'intervention (`WOEND`) soit mise à jour si celle-ci est antérieure à la date de sortie des articles, exemple ci-après.

OT avant/sortie de stock :

```
avant :
	**nom masqué**			0	0	0	0	0	0	1491bae6c0d-d816	OT-748770		14d50162a63-38812	16417a333a6-2c6fb35			Colliers de serrage	0	0		0	0	0	0	0	57,833143	0	0		1		183e830cd6f-120d					0	0									0						0	14a627353a2-262a		16417a333a6-2c6fb35	AWAITINGREAL						2		0	NORMAL	0			0	0				DF			DIVERS	FONC	AMG		AUDE.CLABECQ																			0			0																							0			Europe/Paris,ISO;WOBegin=+02:00;WOEnd=+01:00;expWOEnd=+02:00;initWOBegin=+02:00;initWOEnd=+01:00	18/10/22 09:12:52,199000000	18/10/22 09:12:52,199000000	28/10/22 06:00:00,000000000	02/11/22 07:00:00,000000000	18/10/22 09:43:49,141000000							18/10/22 09:43:49,095000000	28/10/22 06:00:00,000000000	02/11/22 07:00:00,000000000				0	0	0	0						0
après:
	**nom masqué**			0	0	0	0	0	42,473143	1491bae6c0d-d816	OT-748770		14d50162a63-38812	16417a333a6-2c6fb35			Colliers de serrage	0	0		0	0	0	0	0	57,833143	0	0		1		183e830cd6f-120d					0	0									0						0	14a627353a2-262a		173e365885f-133fadf	INPROGRESS						4		0	NORMAL	0			0	0				DF			DIVERS	FONC	AMG		AUDE.CLABECQ																			0			0																							0			Europe/Paris,ISO;WOBegin=+02:00;WOEnd=+01:00;expWOEnd=+02:00;initWOBegin=+02:00;initWOEnd=+01:00	18/10/22 09:12:52,199000000	18/10/22 09:12:52,199000000	28/10/22 06:00:00,000000000	02/11/22 07:00:00,000000000	23/11/22 13:59:20,437000000							23/11/22 13:59:20,341000000	28/10/22 06:00:00,000000000	23/11/22 13:55:51,347000000				0	0	0	0						0
```

et voici l'entrée créée dans `STOCKMOVEMENT` :

```
SIMON.JAMAIN				14d50162a63-38812		173e365885f-133fadf		-42,473143	184a1ccb544-17139	148d6b6c5bf-bee4	1437c919b19-52655		-1	ISSUE					-42,473143	USI	1	145dfdd3378-166d	IssueBean	183e830cd6f-120d												1			Europe/Paris,ISO;movementDate=+01:00;valorisationDate=+01:00	23/11/22 13:59:20,437000000	23/11/22 13:55:51,347000000		23/11/22 13:57:02,533000000
```

## Sémantique CARL

| GÉNÉRIQUE   | Libellé CARL                 | Entité CARL   |
| ----------- | ---------------------------- | ------------- |
| Réservation | Réservation                  | Reserve       |
| Article     | Article                      | Item          |
| Créateur    | Acteur                       | Actor         |
| ?           | Mouvement de stock historisé | STOCKMOVEMENT |
| ?           | Types de mouvements de stock | MOVEMENTTYPE  |
