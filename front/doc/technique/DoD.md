# DoD - Definition of Done

> Etat d'un item l'autorisant à être intégré au code source.

## Liste des critères :

-   [ ] approuvé par 2 développeurs
-   [ ] tests de qualités passés (définis par le quality gate sonarcloud)
-   [ ] permet d'atteindre le sprint goal
-   [ ] peut être testé par le client pendant le sprint review
-   [ ] coverage suffisant (géré par sonarcloud)
-   [ ] flow de test figma opérationnel suivant le scénario écrit par le PO
-   [ ] Les tests fonctionnels (E2E) rédigés suivant le scénario écrit par le PO passent
-   [ ] pas de régression de qualité ou de couverture, sauf accord des autres développeurs (en cas de nécessité absolue)
-   [ ] auto-review effectuée
-   [ ] code en anglais
-   [ ] doc en français
-   [ ] commenter :
    -   en français
    -   ses choix (si ce n'est pas évident)
    -   doit être le plus court possible et compréhensible
    -   pour les méthodes/classes
        -   usage de blocs Jsdoc
        -   obligatoire si nécessaire à la compréhension et pertinent
        -   on ne met pas si ça n'apporte rien, pour ne pas polluer
            -   pas descriptif, ex : `@param l'intervention concernée` pour une méthode du style `closeIntervention(intervention: Intervention)`

## Bonus :

-   [ ] tester chaque composant avec cypress
-   [ ] laisser des instructions pour les reviewers si besoin
-   [ ] argumenter ses choix
