# Charte graphique

## Couleurs principales

-   <span style="background-color:#0873bb;color:#fff;padding:2px 4px;border-radius:3px">Couleur principale</span>
    `#0873bb`
    `rgb(255, 194, 71)`
-   <span style="background-color:#fbae3a;color:#000;padding:2px 4px;border-radius:3px">Couleur secondaire</span>
    `#fbae3a`
    `rgb(251,174,58)`
-   <span style="background-color:#f9fbfd;color:#000;padding:2px 4px;border-radius:3px">Couleur de fond</span>
    `#f9fbfd`
    `rgb(249,251,253)`
-   <span style="background-color:#495057;color:#fff;padding:2px 4px;border-radius:3px">Couleur des textes</span>
    `#495057`
    `rgb(73, 80, 87)`

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
