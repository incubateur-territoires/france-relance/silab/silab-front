# Intégration Continue / Déploiement Continu

## Présentation

Pour ce projet, nous utilisons la fonctionnalité [Gitlab CI/CD][].
La configuration des job est dans le fichier [.gitlab-ci.yml][] et une
visualisation est possible depuis [l'onglet CI/CD][] sur gitlab.com.  
Les tests _End-to-End_ sont faits grace à [cypress][].
Nous disposons aussi d'un job permettant de scanner le code avec [sonarcloud][]
et d'avoir un rapport sur le site, en commentaire dans les MR et via l'update des badges du README.

### Timeline

Le premier job sert à construire le cache npm pour le reste des jobs afin de ne pas avoir à le reconstruire à chaque fois. Ensuite des tests sont effectués et finalement le scanner sonarCloud fait son travail.

Lorsque des pipelines sont créées sur la branche principale 2 autres jobs sont alors créés :

-   Si la pipeline concerne le tag d'un commit ces 2 jobs sont exécutés automatiquement.
-   Sinon, il faut les lancer manuellement une fois que les jobs précédents ont réussis.

### SonarCloud

Comme dit précédement, le projet est analysé par un scanner de sonarCloud et le rendu est disponible sur le site [sonarcloud.io][sonar quality gate]

Voici les diférentes valeurs actuelles du code :

Tests :
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=incubateur-territoires_silab-front&metric=bugs)](https://sonarcloud.io/project/issues?resolved=false&types=BUG&id=incubateur-territoires_silab-front)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=incubateur-territoires_silab-front&metric=coverage)](https://sonarcloud.io/component_measures?metric=coverage&id=incubateur-territoires_silab-front)

Sécurité :
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=incubateur-territoires_silab-front&metric=vulnerabilities)](https://sonarcloud.io/project/issues?resolved=false&types=VULNERABILITY&id=incubateur-territoires_silab-front)

Maintenabilité
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=incubateur-territoires_silab-front&metric=code_smells)](https://sonarcloud.io/project/issues?resolved=false&types=CODE_SMELL&sinceLeakPeriod=true&id=incubateur-territoires_silab-front)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=incubateur-territoires_silab-front&metric=duplicated_lines_density)](https://sonarcloud.io/component_measures?id=incubateur-territoires_silab-front&metric=new_duplicated_lines_density&view=list)

> Les bugs, code smells, vulnérabilités detecté sont renseignés par languages [ici][rules sonarcloud].

Certaines de ces règles sont pré-détecté par SonarLint, qui est un linter qui s'intègre dans de nombreux

## Installation d'un _runner_ CI/CD DinD en local

> **Windows :**  
> Vous pouvez installer un _runner_ sur votre propre pc en suivant [ces instructions][instruction 1].
>
> Accédez aux logs de votre runner en powershell avec `Get-WinEvent -ProviderName gitlab-runner`

Puis suivez [ces instructions][instruction 2] pour utiliser le _runner_ avec l'executeur DinD.

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
[gitlab ci/cd]: https://docs.gitlab.com/ee/ci/
[.gitlab-ci.yml]: ../../.gitlab-ci.yml
[l'onglet ci/cd]: https://gitlab.com/incubateur-territoires/france-relance/silab/silab-front/-/ci/editor?branch_name=main&tab=1
[cypress]: https://www.cypress.io/
[sonarcloud]: https://www.sonarcloud.io/
[instruction 1]: https://docs.gitlab.com/runner/install/windows.html
[instruction 2]: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-enabled-in-the-docker-executor
[sonar quality gate]: https://sonarcloud.io/project/overview?id=incubateur-territoires_silab-front
[rules sonarcloud]: https://rules.sonarsource.com/
