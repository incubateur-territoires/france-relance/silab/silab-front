# Déployer [SI]Lab en production

## Fonctionnement général

Le déploiement utilise un fichier docker-compose où sont précisés tous les containers. Une copie d'exemple de ce fichier est disponible dans le dossier `docker/deploy production example`.
Tous ces containers se situent derrière un reverse proxy sur le serveur.

_⚠_ **Ceci est amené à changer en faveur de 2 fichiers docker-compose contenant seulement le back ou le front puisqu'ils n'ont pas à être "physiquement" liés**

## Déploiement manuel

1. Se connecter au _registry_
2. Cloner ou mettre à jour les dépots _front_ et/ou _back_
3. Créer les images _front_ et/ou _back_ en leur ajoutant le tag _stable_
4. Lancer le `/home/silab/production/docker-compose.yml` en précisant comme fichier d'environnement `/home/silab/production/variables.env`

Exemple :

```shell
# 1.
docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD $REGISTRY
# 2. depots inexistant
git clone https://gitlab.com/incubateur-territoires/france-relance/silab/silab-back.git /home/silab/production/silab-back
git clone https://gitlab.com/incubateur-territoires/france-relance/silab/silab-front.git /home/silab/production/silab-front
# 2. depots existant
(cd /home/silab/production/silab-back && git pull)
(cd /home/silab/production/silab-front && git pull)
# 3.
docker build --tag $REGISTRY/silab-back:production /home/silab/production/silab-back
docker push $REGISTRY/silab-back:production
docker build --tag $REGISTRY/silab-front:production /home/silab/production/silab-front
docker push $REGISTRY/silab-front:production
# 4.
docker compose --env-file /home/silab/production/.env --project-directory /home/silab/production up -d
```

## Déploiement automatique

Voir les fichier `.gitlab/ci/build.yml` et `.gitlab/ci/deploy.yml` inclus dans le fichier `.gitlab-ci.yml`.

## En cas de changements

En cas de modification de l'environnement, vérifier le fichier `/home/silab/production/.env`

Si le fichier `/docker/webserver/default.conf` du _repository back_ est modifié, modifier en conséquence le fichier
`/home/silab/production/webserver/default.conf`

## Erreurs connues

Erreur : `Error response from daemon: Cannot start container ... iptables: No chain/target/match by that name.`  
Solution : redémarrer le service docker

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
