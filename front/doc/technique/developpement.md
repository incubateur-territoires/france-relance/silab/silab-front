# Développement

**[SI]Lab** _frontend_ est basé sur un _starter_ [Vite][].

Le choix de la librairie s'est arreté sur **[Vuetify 3][]**.

Au moment d'écrire ces lignes la version 3 est encore en **beta**.

Les fonctionnalités importantes sont notamment :

-   [l'internationalisation (i18n)]
-   [le theme]

> **Beta :** les limitations de la version _beta_ sont décrites dans cette [FAQ]
>
> **Note :** les alternatives considérées étaient :
>
> -   Balm-ui
> -   Quasar
> -   Bootstrap
> -   Ionic

## Coding style

### nom des composants Vue

Suivant [les préconisations Vue][vue noms multi-mots], les noms de nos composants sont tous préfixés par `s` (pour \[SI\]lab), cf. `s-service-offer`).

### Imports typescripts

Utilisez la syntaxe `import { truc } from "@/chemin/du/truc"`

-   Utilisation systématique des accolades (permet de détecter plus facilement certaines "erreurs" de refactorisation)
-   Utilisation du `@/` pour les chemins des modules
-   Pas de `default` dans nos modules

## Structure du projet

```shell
/app
├── .devcontainer/  # dossier contenant la configuration du dev Container
├── .vscode/        # dossier contenant la configuration du workspace et les extensions recommandées
├── dist/           # dossier contenant le projet construit
├── doc/            # documentation
├── docker/         # fichiers nécessaires au container
│   ├── .bash_aliases   # alias pour le bash
│   ├── .bashrc         # script shell d'entrée copié dans /root
│   └── config
│       └── default.conf    # configuration NGINX pour le déploiement
├── node_modules/   # ressources non compilées lors du build
├── public/         # ressources non compilées lors du build
├── src/            # sources du projet
│   ├── assets/         # ressources compilées lors du build
│   ├── components/     # composants vue
│   ├── router/         # vue-routeur
│   ├── views/          # vue des différentes pages
│   └── ...
└── tests/e2e/      # dossier cypress
      ├── specs/        # tests cypress
      ├── videos/       # vidéos générée par cypress
      └── ...
```

## Interaction avec l'API du backend

Dans les cas plus complexe avec de l'héritage (ex: `ServiceOffer`), on ajoute un factory qui permet de détecter et d'instancier les bonnes classe, depuis un champ `type` additionnel envoyé par le backend.

### Structure de fichier des ressources/entités

```shell
/
└── src/
    └── api/
        └──[ressource]/
            ├── [Ressource]             # La classe principale de la ressource, utilisée dans les vues
            ├── [Ressource]Dto          # Le DTO de la ressource
            ├── [Ressource]Api          # Classe permettant de récupérer la resource depuis le backend
            ├── [Ressource]ApiResponse  # Définition la structure de la reponse fournie par l'API, pour en extaire le Dto
            └── [Ressource]Factory      # (Optionel) Utiliser pour instancier des classes filles

```

### Mock API

Une mock API est générée grâce à [Mirage](https://miragejs.com/).

La mock api est utilisée si la variable d'environnement `VITE_USE_MOCK_API` vaut `true` lors de l'appel à _Vite_.

Une variantes du script npm `build` existe pour se brancher sur la mock api :

-   `npm run build:mock-api`.

#### Fichiers concernant Mirage

```shell
/
├── src/
    ├── mock-api/
    │   ├── fixtures/
    │   └── server.ts   # configuration du server mirage
    └── main.ts         # initialisation de mirage selon variable d'environement
```

[⮪ Sommaire][]  
[⮪ Précédent : Environnement][]

[vue noms multi-mots]: https://v2.vuejs.org/v2/style-guide/?redirect=true#Multi-word-component-names-essential
[⮪ sommaire]: ../../README.md
[⮪ précédent : environnement]: ./environnement.md
[vite]: https://vitejs.dev/
[vuetify 3]: https://vuetifyjs.com/en/
[l'internationalisation (i18n)]: https://vuetifyjs.com/en/features/internationalization/#vue-i18n
[le theme]: https://vuetifyjs.com/en/features/theme/
[faq]: https://next.vuetifyjs.com/en/getting-started/installation/#frequently-asked-questions
