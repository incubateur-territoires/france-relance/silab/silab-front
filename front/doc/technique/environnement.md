# Environnement

## Visual Studio Code (VSCode)

### Lancement

Vous venez d'[installer l'environnement de développeur](installation.md), plusieurs choses se sont faites automatiquement lors de la création :

1. Un environnement basé sur l'image [node][] a été créé _[(Voir la liste des packets utilitaires installés)](utils.md)_
2. Les [dépendances de sonarlint][] ont été installées
3. Les extensions _VSCode_ nécessaires ont été installées
4. Les paramètres _VSCode_ obligatoires ont été configurés
5. Les dépendances _nodes_ du projet ont été installées
6. Le [script npm](scripts.md) `dev` s'est exécuté et [SI]Lab est accessible sur <http://localhost:8443>

### Utilisation

Vous pouvez maintenant travailler complètement à l’intérieur du container, sans avoir par exemple à installer _node_ sur votre machine.

Vous pouvez créer un fichier `.vscode\settings.json` ou `.vscode\*.code-workspace` pour rajouter une configuration personnalisée, ces fichiers ne seront pas _commit_.

**_⚠ Attention :_** _Si vous modifier le container, les fichiers dans `docker/`
ou le `.devcontainer/devcontainer.json` vous devrez reconstruire le dev Container :  
Ouvrez la palette de commande (<kbd>F1</kbd> ou <kbd>Ctrl</kbd>+<kbd>⇧ Shift</kbd>+<kbd>P</kbd>)
puis lancez la commande `Remote-Containers: Rebuild Container`._

## Container Docker

### DockerFile

4 _stages_ :

1. `développement` : utilisé pour l'environnement de développement
2. `ci` : utilisé par le _runner_, c'est un environnement _node_ avec _cypress_ installé
3. `build` : étape intermédiaire au déploiement servant à la construction du projet (création du dossier `dist/`)
4. le dernier n'est pas nommé et est un environnement _distroless_ et
   _unprivileged_ pour servir le projet dans un environnement de production.

### Ports

2 ports peuvent être exposés :

-   `8443` : pour le développement (lorsque `src` est servi avec un _live-reload_)
-   `8444` : pour la _preview_ et les tests (lorsque `dist` est servi)

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
[node]: https://hub.docker.com/_/node
[dépendances de sonarlint]: https://marketplace.visualstudio.com/items?itemName=SonarSource.sonarlint-vscode#requirements
