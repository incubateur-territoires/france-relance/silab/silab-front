# SIlab - Documentation technique : Alfresco

## Liaison CMIS

### Protocole

Liste des différentes possibilités de communication :

-   [AtomPub](https://docs.alfresco.com/content-services/5.2/develop/reference/cmis-ref/#atompub-binding) utilise son propre protocole qui retourne du xml0
-   [WebService](https://docs.alfresco.com/content-services/5.2/develop/reference/cmis-ref/#web-service-binding) utilise le protocole SOAP.
-   [Browser](https://docs.alfresco.com/content-services/5.2/develop/reference/cmis-ref/#browser-binding) basé sur du json. Utilisé sur **Silab-v1**

Solution `Browser` retenue car :

-   code legacy de la v1,
-   simple d'usage,
-   format json.

### Méthodes

L'appel des méthodes CMIS peut se faire via :

-   Requête http native
-   ~~[Librairie PHP CMIS Client](https://github.com/dkd/php-cmis-client#php-cmis-client) ; _licence Apache 2.0_. Utilisé sur **Silab-v1**~~ Obselète

## Authentification

[Basic Authentication](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/WWW-Authenticate#basic_authentication)

Les appels se font sur l'url du repository

## Les requêtes

> Les appels se font sur l'url du repository avec le sub-path `browser/root/`.

### Récupération d'un document

#### Par l'url

Il suffit d'indiquer le chemin du document. Cela fonctionne aussi pour les dossiers.
Si l'on souhaite récupérer les propriétés de l'objet, il faut utiliser le paramètre suivant `'cmisselector' => 'properties'`.

Exemple :

-   https://gedtest.grandpoitiers.fr/alfresco/api/-default-/public/cmis/versions/1.1/browser/root/Site/NomDuSite/Folder/Fichier.png
-   https://gedtest.grandpoitiers.fr/alfresco/api/-default-/public/cmis/versions/1.1/browser/root/Site/NomDuSite/Folder/Fichier.png?cmisselector=properties

#### Par l'id

On peut aussi récupérer un objet par son Id avec le filtre `'ObjectId' => '648ds86v4c684-xc4v5sd;1.0'`.
En suffix à l'id on peut rajouter la version de l'objet souhaité avec le séparateur `;`.

Exemple :

-   https://gedtest.grandpoitiers.fr/alfresco/api/-default-/public/cmis/versions/1.1/browser/root?objectId=48dfs41-d16s-ds4qqqf

### Création de document

La requête se fait dans le dossier cible. On peut indiquer le chemin complet vers le dossier ou utiliser sont url comme vue précédemment.

Pour la création d'un document, les données du body sont de type `formdata`.
Il faut d'abord renseigner le type d'action :

-   `'cmisaction ' => 'createDocument'`

Puis les propriétés de l'objet sont déclarées avec le formalisme suivant :

-   `'propertyId[0]' => 'value' `

    `'propertyValue[0]' => 'value'`

On commence par indiquer le type d'objet puis on renseigne les propriétés associées. Seule le `name` est obligatoire, attention il est unique.
Pour rajouter des aspects à l'objet, il faut les déclarer en type d'objet secondaire puis en renseigner les propriétés.
Il faut aussi renseigner le document à télécharger.

Exemple de body :

-   `'propertyId[0]' => 'cmis:objectTypeId'`

    `'propertyValue[0]' => 'cmis:document'`

-   `'propertyId[1]' => 'cmis:name'`

    `'propertyValue[1]' => 'Nom.jpeg'`

-   `'propertyId[2]' => 'cmis:secondaryObjectTypeIds'`

    `'propertyValue[2][0]' => 'P:carl:WO'`

    `'propertyValue[2][1]' => 'P:cm:titled'`

-   `'propertyId[3]' => 'carl:code'`

    `'propertyValue[3]' => 'OT-48126'`

-   `'propertyId[4]' => 'cm:title'`

    `'propertyValue[4]' => 'Je suis un titre'`

-   `'file' => 'path/sub-path/fileName.mime'`

## Data requis

-   url du repository : <https://gedtest.grandpoitiers.fr/alfresco/api/-default-/public/cmis/versions/1.1/browser>
-   nom utilisateur : Vault
-   mot de passe : Vault
-   chemin d'accès au dossier de dépôt de fichier.

## Ressources

[Cmis 1.1 browser binding](http://docs.oasis-open.org/cmis/CMIS/v1.1/errata01/os/CMIS-v1.1-errata01-os-complete.html#x1-5360005)

[Alfresco documentation - Cmis](https://docs.alfresco.com/content-services/5.2/develop/reference/cmis-ref/)

[PHP CMIS Client](https://github.com/dkd/php-cmis-client/tree/master#php-cmis-client)

[PHP CMIS Client - Exemples](https://github.com/dkd/php-cmis-client/tree/master/examples)

[Url du repository poitiers](https://gedtest.grandpoitiers.fr/alfresco/api/-default-/public/cmis/versions/1.1/browser)

Exemples :

-   <https://gist.github.com/aborroy/3f1f2360b0e85067675643aa648a8112>
-   <https://angelborroy.wordpress.com/2019/06/03/using-cmis-browser-protocol/>
