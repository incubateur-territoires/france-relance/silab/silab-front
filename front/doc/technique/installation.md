# Installation de l'environnement de développement

## VScode sur Windows

> **⚠ Attention :**
> Ce projet a été mis en place pour une intégration/installation simple dans VScode sur Windows.  
> Pour le développement sur un autre environnement voir **[Autres environnements](installation.md#autres-environnements)**.

### Prérequis

-   [Visual Studio Code][]
-   [L'agent openSSH ainsi qu'une paire de clefs][]
-   [Docker Desktop][]
-   [Git][]
-   [vcxsrv][]  
     **Lors du lancement l'option `Disable access control` doit être coché.**  
     _Conseil: lancer cet utilitaire automatiquement au [démarrage de windows][], avec le chemin suivant : `"C:\Program Files\VcXsrv\vcxsrv.exe" -multiwindow -ac`_  
     _Note: cet utilitaire va servir à ouvrir la console de test Cypress_

### Installation

1. Cloner le projet (vous pouvez le faire depuis VS Code sans problème)
2. Ouvrir le dossier contenant le projet dans VS Code
3. Une notification devrait apparaître vous disant de ré-ouvrir le dossier dans le devContainer, faites-le
4. L'environnement de développement se créé automatiquement

### Extensions recommandées

Une fois l'environnement créé, une notification devrait apparaître vous proposant
d'installer les extensions recommandées. La liste de ces extensions se trouve dans
`.vscode\extensions.json` et sont particulièrement adaptées au projet
(les extensions considérées nécessaires sont installées automatiquement).

## Autres environnements

> **Note :** Pour l'instant, cet environement de développement offre un service minimum.

```bash
docker-compose up
```

> Voir <https://docs.docker.com/compose/install/> pour installer `docker-compose`.

Le server de développement est accessible depuis l'URL <https://localhost:8443/>

Utiliser `docker-compose exec` pour éxecuter les commandes du projets, par exemple :

```bash
docker-compose exec front npm run lint
```

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
[visual studio code]: https://code.visualstudio.com/
[l'agent openssh ainsi qu'une paire de clefs]: installer_openssh.md
[docker desktop]: https://docs.docker.com/desktop/windows/install/
[git]: https://git-scm.com/download/win
[vcxsrv]: https://sourceforge.net/projects/vcxsrv/
[démarrage de windows]: https://support.microsoft.com/en-us/windows/add-an-app-to-run-automatically-at-startup-in-windows-10-150da165-dcd9-7230-517b-cf3c295d89dd
