# Installer openSSH et ajouter une clef SSH

## Installation

Si vous avez installé le git Bash, OpenSSH devrait être installé. Tester avec la commande suivante :

```powershell
get-service -DisplayName *OpenSSH*
```

Si vous avez une erreur, suivez les sections _Install ..._ et
_Start and configure OpenSSH Server_ de la [doc officielle microsoft][].

## Lancement automatique de l'agent SSH

L'agent SSH est nécessaire pour pouvoir interagir avec git en SSH depuis le devContainer. Afin d'éviter de le lancer à chaque fois, configurer le service pour qu'il soit démarré automatiquement.

Depuis un powershell **administrateur**, lancez la commande suivante :

```powershell
Set-Service -Name ssh-agent -StartupType Automatic
```

Vous pouvez désormais démarrer le service si celui-ci n'est pas déjà lancé avec `Start-Service ssh-agent`

## Ajouter une clef SSH

Enfin, ajoutez votre clef SSH, ex : `ssh-add $HOME/.ssh/id_rsa`

[⮪ Sommaire][]  
[⮪ Parent: installation][]

[⮪ sommaire]: ../../README.md
[⮪ parent: installation]: installation.md
[doc officielle microsoft]: https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse
