# Termes à connaitre pour GitLab

## Repository

Un _repository_ (dépôt) est l’endroit où l'on stocke le code et y apporte des modifications, les modifications sont suivies avec un contrôle de version.

## Issue

Une _issue_ est un problème à résoudre ou une suggestion d'amélioration, les _issues_ permettent la collaboration, les discussions, la planification et le suivi du travail.

### Les _issues_ sont généralement utilisés pour

-   Soutenir une discussion sur un sujet spécifique.
-   Définir les exigences d’une nouvelle fonctionnalité.
-   Organiser le travail sur un livrable spécifique.
-   Gérer un flux de travail spécifique (c.-à-d. demander l’accès à un système, obtenir l’engagement d’une autre équipe, etc.).
-   Planifier l'aspect d’une fonctionnalité.

### Détails des _issues_

-   On peut avoir un suivi du temps pour estimer et suivre le temps que l'on passe sur les _issues_ et les _merge requests_.
-   On peut assigner **un poids** à une _issue_, le fait d'avoir des _issues pondérées_, nous aide pour avoir une meilleure idée du temps, de la valeur ou de **la complexité** d’un problème donné.
-   En étant participant, on reçoit des notifications GitLab qui nous informent des actions qui s’y produisent.
-   On peut mentionner un utilisateur ou un groupe dans notre instance GitLab, tous les utilisateurs mentionnés sont notifiés avec des éléments to-do et des e-mails.
-   Les _issues_ peuvent parfois être liées à d'autres _issues_ bloquantes.
-   Lorsque l'on mentionne une _issue_ dans une description de _merge request_, cela lie l'_issue_ et la _merge request_ ensemble, en outre, on peut également définir un problème pour qu’il se ferme automatiquement dès que la _merge request_ est fusionnée.
-   GitLab encourage la communication par le biais de commentaires, de fils de discussion et de suggestions de code, deux types de commentaires sont disponibles, un commentaire standard, ou un commentaire dans un fil de discussion, qui peut être résolu.

## Merge Request

Les _merge requests_ (demandes de fusion) sont la façon dont on vérifie les modifications du code source dans une branche, lorsque l'on ouvre une _merge request_, on peut visualiser et collaborer sur les modifications de code avant la fusion.

Un _repository_ est composé de sa branche par défaut, qui contient la version principale de la base du code, à partir de laquelle on peut créer des branches mineures, également appelées _feature branches_, pour proposer des modifications à la base du code sans les introduire directement dans la version majeure de la base du code.

Les branches sont particulièrement importantes lors de la collaboration avec d’autres personnes, évitant que les modifications ne soient transmises directement à la branche par défaut sans révisions, tests et approbations préalables.

La branche dans laquelle on a ajouté nos modifications est appelée branche source tandis que la branche dans laquelle on demande de _merge_ nos modifications est appelée branche cible.

La branche cible peut être la branche par défaut ou toute autre branche, selon les stratégies de branche que l'on choisis.

### Ce que les _merge requests_ incluent

-   Description de la demande.
-   Modifications du code et révisions du code en ligne.
-   Informations sur les _pipelines_ CI/CD.
-   Une section de commentaires pour les fils de discussion.
-   Liste des commits.

## CI/CD

Avec la méthode de **développement continue**, on génère, teste et déploie en permanence des modifications de code itératives, ce processus **itératif** permet de réduire le risque que l'on développe un nouveau code basé sur des versions précédentes erronées ou ayant échouées. Avec cette méthode, on s'efforce d’avoir moins d’interventions humaine ou même pas d’intervention du tout, du développement du nouveau code jusqu’à son déploiement.

### Les trois principales approches de la méthode continue sont les suivantes

**CI - _Continuous Integration_ (Intégration continue)**

-   On envoie des modifications de code tous les jours, plusieurs fois par jour, pour chaque push vers le référentiel, on peut créer un ensemble de scripts pour générer et tester automatiquement l'application, ces scripts aident à réduire les risques que l'on introduise des erreurs dans l'application.

**CD - _Continuous Delivery_ (Livraison continue)**

-   La **livraison continue** est une étape au-delà de l’intégration continue, non seulement l'application est créée et testée chaque fois qu’une modification de code est poussée vers le repository du code, mais l’application est également déployée en continu.
    Toutefois, avec la livraison continue, on déclenche les déploiements manuellement, la livraison continue vérifie automatiquement le code, mais elle nécessite une intervention humaine pour déclencher manuellement et stratégiquement le déploiement des modifications.

**CD - _Continuous Deployment_ (Déploiement continu)**

-   Le **déploiement continu** est une autre étape au-delà de l’intégration continue, similaire à la livraison continue, la différence est qu’au lieu de déployer l'application manuellement, on la configure pour qu’elle soit déployée automatiquement, une intervention humaine n’est pas nécessaire.

## Pipeline

Une _pipeline_ est le composant principal de l’intégration, de la livraison et du déploiement continu, il pilote le développement de logiciels en construisant, en testant et en déployant du code par étapes, les _pipelines_ sont composés de tâches, qui définissent ce qui sera fait, comme la compilation ou le test de code, ainsi que des étapes qui précisent quand exécuter les tâches, un exemple serait l’exécution de tests après les étapes qui compilent le code.

### Ce que les _pipelines_ comprennent

-   Les _jobs_, qui définissent ce qu’il faut faire, par exemple, les tâches qui compilent ou testent du code.
-   Les _stages_, qui définissent quand exécuter les _jobs_, par exemple, les stages qui exécutent des tests après les stages qui compilent le code.

## Runner

Le projet comporte un fichier `yaml` qui définit les étapes de notre _pipeline_ CI/CD et ce qu’il faut faire à chaque étape, il s’agit généralement d’étapes de _build_, de _test_ et de déploiement, à l’intérieur de chaque étape, on peut définir plusieurs tâches.

Un _runner_ GitLab clone le projet en lisant le fichier `.gitlab-ci.yml` et en faisant ce qu’on lui demande de faire, donc, fondamentalement, un runner est un processus qui exécute certaines tâches instruites.

## Commit

Les _commits_ capture un instantané des modifications en cours du projet, elles sont l’une des parties clés d’un dépôt.

Les _commits_ peuvent être accompagnés de messages de validation qui sont un journal pour le référentiel, au fur et à mesure que le projet évolue au fil du temps (ajout de nouvelles fonctionnalités, correction de bugs, refactorisation de l’architecture), les messages des commits servent à savoir ce qui a été modifié et comment.
Il est donc important que ces messages reflètent le changement de manière courte et précise.

## Iteration & Milestone

Les itérations permettent de planifier des sprints agiles ou de type agile afin de capturer les actions à terminer dans un délai spécifique.

Les itérations peuvent être utilisées avec des milestones pour le suivi sur différentes périodes.

Les _milestones_ permettent de suivre l’avancement de plusieurs problèmes sur une période de temps spécifique, ainsi que lors de la planification et de la gestion des epics.

_Par exemple :_

-   Les _milestones_ qui s’étendent sur 8 à 12 semaines.
-   Les itérations qui s’étendent sur 2 semaines.

Un _sprint_ représente une période de temps finie au cours de laquelle le travail doit être terminé, qui peut être une semaine, quelques semaines ou peut-être un mois ou plus. Le _product owner_ et l’équipe de développement se rencontrent pour décider du travail qui est en cours pour le _sprint_ à venir.

## Task

Souvent, un besoin ou une attente est séparée en tâches individuelles, on peut créer une liste de tâches dans la description d’une _issue_ dans GitLab, afin d’identifier davantage ces tâches individuelles, et l'assigné à une ou plusieurs personnes.

Par exemple :

-   [x] Mettre à jour les maquettes.
-   [ ] Ajouter le bouton "Valider" en bas du formulaire.
-   [ ] Ajouter un test fonctionnel.

## Epic

L'épique indique un flux d’utilisateurs plus important composé de multiples fonctionnalités, dans GitLab, une _epic_ contient également un titre et une description, un peu comme une _issue_, mais elle permet d’y attacher plusieurs _issues_ enfants pour indiquer cette hiérarchie.

## Label

Au fur et à mesure que les _issues_, les _merge requests_ et que les _epics_ augmente, il devient plus difficile de suivre ces éléments, avec les labels (étiquettes), on peut organiser et baliser le travail, et suivre les éléments de travail qui nous intéressent.

## Code review

La _review_ (revue) de code est l'analyse du code par une ou plusieurs tierces personnes, son but est de déceler les erreurs avant de _merge_, mais aussi d’améliorer la qualité du code, ainsi que pour valider la conception et l’implémentation des fonctionnalités, cela aide aussi les développeurs à maintenir la cohérence entre les styles de conception et de mise en œuvre entre les membres de l’équipe.

## Revue de backlog

Le _product owner_ crée généralement une description simple d’un besoin ou d’une attente exprimée par un utilisateur pour déterminer les fonctionnalités à développer pour refléter les besoins de l’entreprise et des clients.

Ils sont hiérarchisés dans un _backlog_ produit pour capturer l’urgence et l’ordre de développement souhaité, le _product owner_ communique avec les parties prenantes pour déterminer les priorités et affine constamment le _backlog_.

Dans GitLab, il existe des listes d'_issues_ générées dynamiquement que les utilisateurs peuvent afficher pour suivre leur _backlog_, les labels peuvent être créées et affectées à des issues individuelles, ce qui permet ensuite de filtrer les listes d'_issues_ par un ou plusieurs labels, cela permet une plus grande flexibilité.

Les labels de priorité peuvent même être utilisés pour ordonner également les _issues_ de ces listes.

[⮪ Sommaire](../../README.md)
