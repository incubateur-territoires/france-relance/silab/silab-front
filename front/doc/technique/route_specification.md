# Spécification de l'API - Silab Back

## Ressources

-   [API RESTful](https://restfulapi.net/resource-naming/) : ce protocole est implémenté par l'API.
-   [JSON:API](https://jsonapi.org/) : l'API s'inspire des spécifications 'json:api' notamment pour le filtrage.

## Lexique

-   **Collection** : ensemble d'un type de ressources.
-   **Store** : ensemble de ressources rattaché à une autre.
-   **Sub-ressource** : ensemble de ressources appartenant à une autre, et ne pouvant exister sans celle-ci("ON`DELETE` CASCADE"). Elles sont innaccessible a la racine.
-   **controller** : action a réaliser sur une ressource.

## Convention de nommage des routes

### Quelques règles

-   Nommage en anglais.
-   L'emploi du [kebab case](https://fr.wiktionary.org/wiki/kebab_case).
-   Tout en minuscule.
-   Pas de "/" en fin de route.
-   Collection et store au pluriel.
-   Emploi de verbe uniquement pour les controller.

> Exemple :
>
> <http://api.example.com/ressources/:id>
>
> <http://api.example.com/ressources>

### Verbes HTTP

Listes des verbes HTTP acceptés :

-   `GET`
-   `POST`
-   `PATCH`
-   `DELETE`

### les différents type de routes

Chaque route présenté est suivit des verbes HTTP accéptés et d'un exemple.

-   `/api/:collection`

    `GET` `POST` `PATCH` `DELETE`

    > /api/service-offers

-   `/api/:collection/:id`

    `GET` `PATCH` `DELETE`

    > /api/interventions/rez65fg1-d1661s

-   `/api/:collection/:id/:store`

    `GET` `POST` `PATCH` `DELETE`

    > /api/intervention-service-offers/proprete/interventions

-   `/api/:collection/:id/:controller`

    `POST`

    > /api/interventions/rez65fg1-d1661s/close

-   `/api/:collection/:id/:sub-resources`

    `GET` `POST` `PATCH` `DELETE`

    > /api/article-reservations/ds54f-461scqk/issues

-   `/api/:collection/:id/:sub-resource/:id`

    `GET` `POST` `PATCH` `DELETE`

    > /api/article-reservations/ds54f-461scqk/issues/q46sc-qs684c

#### Le filtrage

-   Le filtrage de ressource :

    > ?filter[ressource-type.attribute][activite*]=value

    Si rien n'est préciser pour `ressource-type` c'est la ressource racine par défaut.

-   Filtrage des champs retournés :

    > ?fields[ressource-type]=attribute2,attribute3,attribute7

    > ?fieldset[ressource-type]=template
