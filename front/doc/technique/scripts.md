# Scripts npm

-   `build` : construit le dossier `dist` qui sera le dossier servi par le serveur web en preview et production.
-   `dev` : lance un live-serveur de développement contenant un live-reload sur le port 8443
-   `lint` : lance eslint sur tout les fichiers js, ts et vue
-   `preview` : lance un web-server servant le dossier `dist` sur le port 8444
-   `cypress` : ouvre Cypress
-   `cypress:ci` : execute cypress sans GUI
-   `test:e2e` : lance le script `preview`, attend une réponse de <http://localhost:8444/>, puis lance le script `cypress`
-   `build-and-test:e2e:ci:mock-api` : build le projet et lance les tests cypress en mode console de la même manière que sur la CI
-   `prepare` : ce script est exécuté après la commande `npm install` et permet d'installer cypress.

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
