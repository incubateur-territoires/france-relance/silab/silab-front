# Liste des packages utilitaires installés

|     **Package**     |              **Description**              |      **Commandes** _(liste non exhaustive)_      |
| :-----------------: | :---------------------------------------: | :----------------------------------------------: |
|       [git][]       |              Utilitaire git               |                  git, git-shell                  |
|      [nano][]       |      Editeur de texte intra-console       |                       nano                       |
|     [openssl][]     |         Protocole openSSL et TLS          |                     openssl                      |
| [openssh-client][]  |              Client openSSH               | scp, sftp, ssh, ssh-add, ssh-keygen, ssh-keyscan |
| [openssh-server][]  |              Serveur openSSH              |                       sshd                       |
|      [tree][]       | Listage de répertoires sous forme d'arbre |                       tree                       |
|   [util-linux][]    |            Utilitaires divers             |                  more, whereis                   |
|      [grep][]       |   Recherche de texte dans des fichiers    |                grep, egrep, fgrep                |
| [bind9-dnsutils][]  |              Utilitaire DNS               |                  nslookup, dig                   |
|     [locate][]      |          Indexation de fichiers           |                 updatedb, locate                 |
| [bash-completion][] |           Auto-complétion bash            |                                                  |
|  [iputils-ping][]   |             Test de connexion             |                ping, ping4, ping6                |

[⮪ Sommaire][]

[⮪ sommaire]: ../../README.md
[git]: https://packages.debian.org/bullseye/git
[nano]: https://packages.debian.org/bullseye/nano
[openssl]: https://packages.debian.org/bullseye/openssl
[openssh-client]: https://packages.debian.org/bullseye/openssh-client
[openssh-server]: https://packages.debian.org/bullseye/openssh-server
[tree]: https://packages.debian.org/bullseye/tree
[util-linux]: https://packages.debian.org/bullseye/util-linux
[grep]: https://packages.debian.org/bullseye/grep
[bind9-dnsutils]: https://packages.debian.org/bullseye/bind9-dnsutils
[locate]: https://packages.debian.org/bullseye/locate
[bash-completion]: https://packages.debian.org/bullseye/bash-completion
[iputils-ping]: https://packages.debian.org/bullseye/iputils-ping
