# Flow de développement/déploiement pour les développeurs Silab

Globalement, notre workflow est calqué sur [Gitflow](https://docs.gitlab.com/ee/topics/gitlab_flow.html#github-flow-as-a-simpler-alternative).

## Aperçu du workflow

```mermaid
    flowchart TD;
        SOURCE_BESOIN_METIER(Besoin métier)
        SOURCE_BUG(Bug)
        SOURCE_AMELIORATION_CONTINUE(Amélioration continue)

        CREATION_ISSUE(Issue*)
        SOURCE_BESOIN_METIER--"Epic : (Projet concerné)<br>Description : Expression de besoin, user stories"-->CREATION_ISSUE
        SOURCE_BUG--Type : Incident-->CREATION_ISSUE
        SOURCE_AMELIORATION_CONTINUE--Label : Amélioration continue-->CREATION_ISSUE

        CREATION_MR(Merge request*)
        CREATION_ISSUE-->CREATION_MR

        DEVELOPEMENT(Développement)
        CODE_REVIEW(Code review)
        CREATION_MR-->DEVELOPEMENT--"Assignation des reviewers<br>Retrait du mode draft<br>Passage en mode ready"-->CODE_REVIEW

        MERGE(Merge***)
        DEPLOY(Déploiement)
        CODE_REVIEW--Le dernier à avoir approuvé merge-->MERGE-->DEPLOY
```

## Détail

1. Une personne crée une _issue_\* en donnant la description du problème, le poids (Fibonacci), l'epic et les étiquettes associées.

2. À la revue de _backlog_, cette carte pourra être assignée et ajouté à l'itération à venir.

3. La personne assignée à l'_issue_ travaillera sur une branche de _feature_\*\* associée à une _merge request_.

4. Une fois le travail effectué et que les _pipelines_ ont fonctionnés, la personne passe la _merge request_ en _review_ en assignant des _reviewers_ et en retirant le mode _draft_ de la _merge request_ pour la passer en mode _ready_.

5. La dernière personne à avoir approuvée est encouragée à _merge_ la branche\*\*\*.

6. Après avoir _merge_, la branche de feature est supprimée et l'_issue_ liée à la _merge request_ est clôturée, et les changements sont ensuite déployables par la CI.

\* _Les titres des issues expriment un besoin/problématique, ex : `En tant qu'administrateur Silab, je dois pouvoir couper l'accès à une offre de service`_.  
\*\* _Les titres des merge requests/branches expriment une solution, ex : `Option de coupure ods admin`_.  
\*\*\* _Avant d'être fusionnés avec la branche principale, les commits sont squash_.

[⮪ Sommaire](../../README.md)
