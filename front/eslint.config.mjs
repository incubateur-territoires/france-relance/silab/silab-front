import typescriptEslint from '@typescript-eslint/eslint-plugin'
import prettier from 'eslint-plugin-prettier'
import noOnlyTests from 'eslint-plugin-no-only-tests'
import unusedImports from 'eslint-plugin-unused-imports'
import globals from 'globals'
import parser from 'vue-eslint-parser'
import path from 'node:path'
import { fileURLToPath } from 'node:url'
import js from '@eslint/js'
import { FlatCompat } from '@eslint/eslintrc'

//Fichier généré à partir du .eslintrc avec https://eslint.org/docs/latest/use/configure/migration-guide

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const compat = new FlatCompat({
    baseDirectory: __dirname,
    recommendedConfig: js.configs.recommended,
    allConfig: js.configs.all,
})

export default [
    {
        ignores: [
            'src/shared/swrv/*',
            'node_modules/*',
            'dist/*',
            '.gitignore',
        ],
    },
    ...compat.extends(
        'plugin:vue/vue3-recommended', //comprend aussi les templates essential & strongly-recommanded
        'plugin:@typescript-eslint/recommended',
        'prettier'
    ),
    {
        plugins: {
            '@typescript-eslint': typescriptEslint,
            prettier,
            'no-only-tests': noOnlyTests,
            'unused-imports': unusedImports,
        },

        languageOptions: {
            globals: {
                ...globals.node,
            },

            parser: parser,
            ecmaVersion: 5,
            sourceType: 'commonjs',

            parserOptions: {
                parser: '@typescript-eslint/parser',
            },
        },

        rules: {
            'no-only-tests/no-only-tests': [
                'error',
                {
                    fix: true,
                },
            ],

            'unused-imports/no-unused-imports': 'error',
            '@typescript-eslint/no-unused-vars': 'off',
            'no-unused-vars': 'off',

            'unused-imports/no-unused-vars': [
                'error',
                {
                    vars: 'all',
                    varsIgnorePattern: '^_',
                    args: 'after-used',
                    argsIgnorePattern: '^_',
                },
            ],

            'no-warning-comments': [
                'error',
                {
                    terms: ['todo'],
                    location: 'anywhere',
                },
            ],

            'prettier/prettier': 'warn',

            'no-console': [
                'error',
                {
                    allow: ['warn', 'error'],
                },
            ],

            'vue/component-api-style': 'error',

            'vue/block-lang': [
                'error',
                {
                    script: {
                        lang: 'ts',
                    },
                },
            ],

            'vue/component-name-in-template-casing': [
                'error',
                'kebab-case',
                {
                    registeredComponentsOnly: true,
                    ignores: [],
                },
            ],

            'vue/component-options-name-casing': ['error', 'camelCase'],
            'vue/custom-event-name-casing': 'error',
            'vue/new-line-between-multi-line-property': 'error',
            eqeqeq: 'warn',
            'vue/func-call-spacing': 'error',
            'vue/no-deprecated-filter': 0,
        },
    },
]
