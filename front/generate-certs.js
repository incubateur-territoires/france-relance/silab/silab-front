/* eslint-disable no-console */
import { X509Certificate } from 'crypto'
import fs from 'fs'
import { createCA, createCert } from 'mkcert'

if (certificatesAreValid()) {
    console.log('Les certificats sont à jour.')
    process.exit()
}

generateLocalhostCertificate().then(() => {
    process.exit()
})

function certificatesExists() {
    return (
        fs.existsSync('cert.key') &&
        fs.existsSync('cert.crt') &&
        fs.existsSync('ca.key') &&
        fs.existsSync('ca.crt')
    )
}

function certificatesAreValid() {
    if (!certificatesExists()) {
        return false
    }
    const ca = fs.readFileSync('ca.crt')
    const cert = fs.readFileSync('cert.crt')

    const caValidTo = new X509Certificate(ca).validTo
    const certValidTo = new X509Certificate(cert).validTo

    const caIsValid = new Date(caValidTo).getTime() > Date.now()
    const certIsValid = new Date(certValidTo).getTime() > Date.now()

    return caIsValid && certIsValid
}

async function generateLocalhostCertificate() {
    if (!fs.existsSync('cert.key') || !fs.existsSync('cert.crt')) {
        const ca = await createCA({
            organization: 'Silab',
            countryCode: 'FR',
            state: 'Youpiland',
            locality: 'CoolVille',
            validity: 365,
        })
        fs.writeFileSync('ca.key', ca.key)
        fs.writeFileSync('ca.crt', ca.cert)

        const cert = await createCert({
            ca: { key: ca.key, cert: ca.cert },
            domains: ['127.0.0.1', 'localhost'],
            validity: 365,
        })
        fs.writeFileSync('cert.key', cert.key)
        fs.writeFileSync('cert.crt', cert.cert)
        console.warn(
            "Nouveaux certificats générés, merci d'ajouter le ca.crt à votre magasin d'Autorités de certification racines de confiance."
        )
    }
}
