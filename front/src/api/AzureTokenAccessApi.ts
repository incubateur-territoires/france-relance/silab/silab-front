import {
    getBackendData,
    patchBackendData,
    postBackendData,
    putBackendData,
    deleteBackendData,
    QueryParameters,
} from '@/shared/helpers/backend'
import { loginRequest } from '@/security/authConfig'
import { InteractionRequiredAuthError } from '@azure/msal-browser'
import { MsalContext } from '@/plugins/msal/useMsal'
import { isUndefined } from '@/shared/helpers/types'
import { useUserImpersonationStore } from '@/shared/user/stores/UserImpersonationStore'
import { LoadingManager } from '@/shared/helpers/lifeCycle'

export abstract class AzureTokenAccessApi {
    private userImpersonationStore: ReturnType<typeof useUserImpersonationStore>
    private loadingManager = new LoadingManager()
    public isProcessing = this.loadingManager.isLoading

    public constructor(private msal: MsalContext) {
        this.userImpersonationStore = useUserImpersonationStore()
    }

    protected async getBackendData(
        endpoint: string,
        queryParameters: QueryParameters = new Map(),
        abortSignal?: AbortSignal
    ) {
        const stopLoading = this.loadingManager.startLoading()
        return await getBackendData(
            endpoint,
            queryParameters,
            await this.getAuthenticationHeaders(),
            abortSignal
        ).finally(() => {
            stopLoading()
        })
    }

    protected async postBackendData(endpoint: string, withBody: string) {
        const stopLoading = this.loadingManager.startLoading()
        return await postBackendData(
            endpoint,
            withBody,
            await this.getAuthenticationHeaders()
        ).finally(() => {
            stopLoading()
        })
    }

    protected async patchBackendData(endpoint: string, withBody: string) {
        const stopLoading = this.loadingManager.startLoading()
        return await patchBackendData(
            endpoint,
            withBody,
            await this.getAuthenticationHeaders()
        ).finally(() => {
            stopLoading()
        })
    }

    protected async putBackendData(endpoint: string, withBody: string) {
        const stopLoading = this.loadingManager.startLoading()

        return await putBackendData(
            endpoint,
            withBody,
            await this.getAuthenticationHeaders()
        ).finally(() => {
            stopLoading()
        })
    }

    protected async deleteBackendData(endpoint: string) {
        const stopLoading = this.loadingManager.startLoading()
        return await deleteBackendData(
            endpoint,
            await this.getAuthenticationHeaders()
        ).finally(() => {
            stopLoading()
        })
    }
    /**
     * La méthode renvoie une exception si le token n'a pas pu être obtenu.
     */
    private async tryGetIdToken(): Promise<string> {
        await this.msal.instance.initialize()
        // fix : il y a des soucis avec la durée de validité de l'id token, cf. https://github.com/AzureAD/microsoft-authentication-library-for-js/issues/4206
        // Pour combler à cela, on impose une validité inférieure à la durée de vie classique de l'ID token (normalement 60 minutes) pour s'assurer d'avoir un id token valide systématiquement.
        const lastIdTokenAquisition = localStorage.getItem(
            'lastIdTokenAquisition'
        )
        const forceRefresh =
            lastIdTokenAquisition === null
                ? true
                : new Date().getTime() >
                  new Date(lastIdTokenAquisition).getTime() + 45 * 60 * 1000 // on donne une durée de vie 45 minutes

        const { idToken } = await this.msal.instance
            .acquireTokenSilent({
                ...loginRequest,
                forceRefresh,
            })
            .catch(async (e) => {
                if (e instanceof InteractionRequiredAuthError) {
                    await this.msal.instance.acquireTokenRedirect(loginRequest)
                }
                throw e
            })

        if (forceRefresh) {
            localStorage.setItem(
                'lastIdTokenAquisition',
                new Date().toISOString()
            )
        }
        return idToken
    }

    private async getAuthenticationHeaders(): Promise<Record<string, string>> {
        return {
            ...(await this.generateIdTokenHeaderIfAuthenticated()),
            ...this.getImpersonationHeader(),
        }
    }

    private async generateIdTokenHeaderIfAuthenticated(): Promise<
        Record<string, string>
    > {
        const tokenHeader = <Record<string, string>>{}
        try {
            tokenHeader['authorization'] =
                'Bearer ' + (await this.tryGetIdToken())
        } catch {}

        return tokenHeader
    }

    /**
     * Si on usurpe l'identité d'un user on génère un header qui sera utilisé interprété par le back
     */
    private getImpersonationHeader() {
        if (isUndefined(this.userImpersonationStore.impersonatedEmail)) {
            return
        }

        return {
            'X-Switch-User': this.userImpersonationStore.impersonatedEmail,
        }
    }
}
