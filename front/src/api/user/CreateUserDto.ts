import { CreateDto } from '@/shared/api/entities/Dtos'
import { User } from './User'

export interface CreateUserDto extends CreateDto<User> {
    email: string
    roles: Array<string>
}
