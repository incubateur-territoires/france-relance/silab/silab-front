import { User } from '@/api/user/User'
import { EditDto } from '@/shared/api/entities/Dtos'

export interface EditUserDto extends EditDto<User> {
    email: string
    roles: Array<string>
}
