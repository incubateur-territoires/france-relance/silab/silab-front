import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { UserDto } from './UserDto'

export class User {
    id: string
    email: string
    roles: Array<string>
    reachableRoles: Array<string>

    public hasRole(role: string): boolean {
        return this.reachableRoles.includes(role)
    }

    public getNumberOfMatchingRoles(
        rolesToMatch: ServiceOffer['availableRoles']
    ): number {
        return rolesToMatch
            .map((serviceOfferRole) => serviceOfferRole.value)
            .filter((roleToMatch) => this.roles.includes(roleToMatch)).length
    }
    constructor(userDto: UserDto) {
        this.id = userDto.id ?? 'ANONYMOUS'
        this.email = userDto.email
        // Le rôle ROLE_USER qui est renvoyé par le back-end est retiré car il n'est pas necessaire pour les droits à lié à l'application
        this.roles = userDto.roles.filter((role) => role !== 'ROLE_USER')
        this.reachableRoles = userDto.reachableRoles
    }

    public static readonly ANONYMOUS_USER = new User({
        id: '',
        email: '',
        roles: [],
        reachableRoles: [],
    })
}
