import { EditUserDto } from './EditUserDto'
import { User } from './User'
import { UserDto } from './UserDto'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { CreateUserDto } from './CreateUserDto'
import { EntityApi } from '@/shared/api/entities/EntityApi'
import { isDefined } from '@/shared/helpers/types'

export class UserApi extends AzureTokenAccessApi implements EntityApi<User> {
    async getAll(): Promise<User[]> {
        const usersApiResponse = (await (
            await this.getBackendData(`users`)
        ).json()) as UserDto[]

        return usersApiResponse.map((userDto) => {
            return new User(userDto)
        })
    }

    async getByServiceOffer(
        serviceOfferId: ServiceOffer['id']
    ): Promise<User[]> {
        const usersApiResponse = (await (
            await this.getBackendData(
                `service-offers/${serviceOfferId}/admin/users`
            )
        ).json()) as UserDto[]

        return usersApiResponse.map((userDto) => {
            return new User(userDto)
        })
    }

    async get(id: User['id']) {
        const rawResponse = await this.getBackendData(`users/${id}`)

        const userApiResponse = (await rawResponse.json()) as UserDto

        return new User(userApiResponse)
    }

    async update(id: User['id'], updatedUser: EditUserDto) {
        const rawResponse = await this.patchBackendData(
            `users/${id}`,
            JSON.stringify(updatedUser)
        )

        const editUderDto = (await rawResponse.json()) as UserDto

        return new User(editUderDto)
    }

    async updateUserByServiceOffer(
        serviceOfferId: ServiceOffer['id'],
        id: User['id'],
        updatedUser: EditUserDto
    ) {
        const rawResponse = await this.putBackendData(
            `service-offers/${serviceOfferId}/admin/users/${id}`,
            JSON.stringify(updatedUser)
        )

        return (await rawResponse.json()) as EditUserDto
    }

    async getCurrent(getOriginalUser?: boolean) {
        const queryParameters = isDefined(getOriginalUser)
            ? new Map([['originalUser', getOriginalUser]])
            : new Map()
        const rawResponse = await this.getBackendData('myself', queryParameters)
        const userDto = (await rawResponse.json()) as UserDto

        return new User(userDto)
    }

    async create(newUser: CreateUserDto) {
        const rawResponse = await this.postBackendData(
            'users',
            JSON.stringify(newUser)
        )
        const userDto = (await rawResponse.json()) as UserDto

        return new User(userDto)
    }

    async createUserByServiceOffer(
        serviceOfferId: ServiceOffer['id'],
        newUser: CreateUserDto
    ) {
        const rawResponse = await this.postBackendData(
            `service-offers/${serviceOfferId}/admin/users`,
            JSON.stringify(newUser)
        )
        const userDto = (await rawResponse.json()) as UserDto

        return new User(userDto)
    }
}
