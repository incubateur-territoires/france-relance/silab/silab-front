export interface UserDto {
    id?: string
    email: string
    roles: Array<string>
    reachableRoles: Array<string>
}
