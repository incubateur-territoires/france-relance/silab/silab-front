import { User } from '@/api/user/User'
import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { EditUserDto } from './EditUserDto'
import { isUndefined } from '@/shared/helpers/types'
import { CreateUserDto } from './CreateUserDto'

interface UserMapper extends Omit<EntityMapper<User>, 'toDtoArray' | 'toDto'> {
    toEditDto: (partialEntity: Partial<User>) => EditUserDto
    toCreateDto: (partialEntity: Partial<User>) => CreateUserDto
}

export const userMapper: UserMapper = {
    toEditDto(partialUser: Partial<User>): EditUserDto {
        if (isUndefined(partialUser.email) || isUndefined(partialUser.roles)) {
            throw new IncompleteEntity(
                "L'utilisateur fournit ne possède pas tout les attributs requis."
            )
        }
        return {
            email: partialUser.email,
            roles: partialUser.roles,
        }
    },

    toCreateDto(partialUser: Partial<User>): CreateUserDto {
        if (isUndefined(partialUser.email) || isUndefined(partialUser.roles)) {
            throw new IncompleteEntity(
                "L'utilisateur fournit ne possède pas tout les attributs requis."
            )
        }
        return {
            email: partialUser.email,
            roles: partialUser.roles,
        }
    },
}
