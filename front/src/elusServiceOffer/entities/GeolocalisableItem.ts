import { LatLngLiteral } from 'leaflet'

export interface GeolocalisableItem {
    getCoordinates: () => LatLngLiteral | undefined
}
