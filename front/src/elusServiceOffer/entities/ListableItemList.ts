import { ListableItem } from '@/shared/item/entities/ListableItem'

export interface ListableItemList<T extends ListableItem> {
    title: string
    icon?: string
    items: T[]
}
