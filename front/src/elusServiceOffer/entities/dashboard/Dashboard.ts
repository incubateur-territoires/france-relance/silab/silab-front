import { DashboardDto } from './DashboardDto'

export class Dashboard {
    id: string
    url: string

    public constructor(dashboardDto: DashboardDto) {
        this.id = dashboardDto.id
        this.url = dashboardDto.url
    }
}
