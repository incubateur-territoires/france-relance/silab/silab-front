import { AzureTokenAccessApi } from '../../../api/AzureTokenAccessApi'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { Dashboard } from './Dashboard'
import { DashboardDto } from './DashboardDto'

export class DashboardApi extends AzureTokenAccessApi {
    async get(serviceOfferId: ServiceOffer['id'], id: number) {
        const rawResponse = await this.getBackendData(
            `elus-service-offers/${serviceOfferId}/data-analysis/${id}`
        )

        const dashboardDto = (await rawResponse.json()) as DashboardDto

        return new Dashboard(dashboardDto)
    }
}
