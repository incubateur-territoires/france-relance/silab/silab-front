import { datesHaveSameValues } from '@/elusServiceOffer/helpers/dateComparison'
import { LegacyObjectFiltersInterface } from '@/elusServiceOffer/filters/entities/LegacyObjectFiltersInterface'
import { arraysHaveSameValues } from '@/shared/helpers/arrays'

export class InterventionFilters implements LegacyObjectFiltersInterface {
    beginStatusChangedAt: Date
    endStatusChangedAt: Date
    beginCreatedAt: Date
    endCreatedAt: Date
    statuses: string[]
    regroupment: string[]

    public constructor(
        currentFilters: Map<string, Date | string | string[] | undefined>
    ) {
        this.beginStatusChangedAt = currentFilters.get(
            'beginStatusChangedAt'
        ) as Date
        this.endStatusChangedAt = currentFilters.get(
            'endStatusChangedAt'
        ) as Date
        this.beginCreatedAt = currentFilters.get('beginCreatedAt') as Date
        this.endCreatedAt = currentFilters.get('endCreatedAt') as Date
        this.statuses = currentFilters.get('statuses') as string[]
        this.regroupment = currentFilters.get('regroupment') as string[]
    }

    public static getDefaultFilters(): Map<
        string,
        Date | string | string[] | undefined
    > {
        const oneYearAgo = new Date(
            new Date().setFullYear(new Date().getFullYear() - 1)
        )

        const rollingYear =
            oneYearAgo.getFullYear().toString().padStart(2, '0') +
            '-' +
            (oneYearAgo.getMonth() + 1).toString().padStart(2, '0') +
            '-' +
            oneYearAgo.getDate().toString().padStart(2, '0')

        return new Map<string, Date | string | string[] | undefined>([
            ['beginStatusChangedAt', undefined],
            ['endStatusChangedAt', undefined],
            ['beginCreatedAt', rollingYear],
            ['endCreatedAt', undefined],
            ['statuses', ['A faire', 'En cours', 'Terminé']],
            ['regroupment', []],
        ])
    }

    public reset() {
        this.beginStatusChangedAt = InterventionFilters.getDefaultFilters().get(
            'beginStatusChangedAt'
        ) as Date
        this.endStatusChangedAt = InterventionFilters.getDefaultFilters().get(
            'endStatusChangedAt'
        ) as Date
        this.beginCreatedAt = InterventionFilters.getDefaultFilters().get(
            'beginCreatedAt'
        ) as Date
        this.endCreatedAt = InterventionFilters.getDefaultFilters().get(
            'endCreatedAt'
        ) as Date
        this.statuses = InterventionFilters.getDefaultFilters().get(
            'statuses'
        ) as string[]
        this.regroupment = InterventionFilters.getDefaultFilters().get(
            'regroupment'
        ) as string[]
    }

    public hasFilters() {
        return this.getFilterQuantity() > 0
    }

    public getFilterQuantity() {
        const defaultFilters = InterventionFilters.getDefaultFilters()

        let nbDifferences = 0

        nbDifferences +=
            datesHaveSameValues(
                defaultFilters.get('beginStatusChangedAt') as Date,
                this.beginStatusChangedAt
            ) &&
            datesHaveSameValues(
                defaultFilters.get('endStatusChangedAt') as Date,
                this.endStatusChangedAt
            )
                ? 0
                : 1

        nbDifferences +=
            datesHaveSameValues(
                defaultFilters.get('beginCreatedAt') as Date,
                this.beginCreatedAt
            ) &&
            datesHaveSameValues(
                defaultFilters.get('endCreatedAt') as Date,
                this.endCreatedAt
            )
                ? 0
                : 1

        nbDifferences += arraysHaveSameValues(
            InterventionFilters.getDefaultFilters().get('statuses') as string[],
            this.statuses
        )
            ? 0
            : 1

        nbDifferences += arraysHaveSameValues(
            InterventionFilters.getDefaultFilters().get(
                'regroupment'
            ) as string[],
            this.regroupment
        )
            ? 0
            : 1

        return nbDifferences
    }

    public getFilters() {
        if (
            this.beginStatusChangedAt !== undefined &&
            this.endStatusChangedAt !== undefined &&
            this.beginCreatedAt !== undefined &&
            this.endCreatedAt !== undefined &&
            this.statuses.length === 0 &&
            this.regroupment.length === 0
        ) {
            return new Map()
        }

        return new Map<string, Date | string | string[] | undefined>([
            ['beginStatusChangedAt', this.beginStatusChangedAt],
            ['endStatusChangedAt', this.endStatusChangedAt],
            ['beginCreatedAt', this.beginCreatedAt],
            ['endCreatedAt', this.endCreatedAt],
            ['statuses', this.statuses],
            ['regroupment', this.regroupment],
        ])
    }
}
