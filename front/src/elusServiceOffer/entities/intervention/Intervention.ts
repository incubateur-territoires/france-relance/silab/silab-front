import { GeolocalisableItem } from '../GeolocalisableItem'
import { LatLngLiteral } from 'leaflet'
import { RouteLocationRaw } from 'vue-router'
import { getAnteriorityString } from '@/shared/helpers/dates'
import { InterventionDto } from './InterventionDto'
import {
    ListableItem,
    SecondaryAction,
    getItemThumbnailUrl,
} from '@/shared/item/entities/ListableItem'
import { isDefined } from '@/shared/helpers/types'

export class Intervention implements ListableItem, GeolocalisableItem {
    id: string
    code: string
    title: string
    currentStatus: string
    regroupment: string
    coordinates?: LatLngLiteral
    address?: string
    createdAt: Date
    imageUrl?: URL
    beginDate: Date
    endDate: Date
    comment?: string
    createdBy: string
    history?: Intervention[]
    isCurrentStatus?: boolean
    linkToCarl?: string
    localisation?: string
    statusChangedAt: Date
    private itemRoute?: RouteLocationRaw
    private itemSecondaryAction?: SecondaryAction

    public constructor(interventionDto: InterventionDto) {
        this.id = interventionDto.id
        this.code = interventionDto.code
        this.title = interventionDto.title
        this.currentStatus = interventionDto.currentStatus
        this.regroupment = interventionDto.regroupment
        this.coordinates = interventionDto.coordinates
        this.address = interventionDto.address
        this.createdAt = new Date(interventionDto.createdAt)
        this.imageUrl = isDefined(interventionDto.imageUrl)
            ? new URL(interventionDto.imageUrl)
            : undefined
        this.beginDate = new Date(interventionDto.beginDate)
        this.endDate = new Date(interventionDto.endDate)
        this.comment = interventionDto.comment
        this.createdBy = interventionDto.createdBy
        this.history = interventionDto.history?.map((interventionDto) => {
            return new Intervention(interventionDto)
        })
        this.isCurrentStatus = interventionDto.isCurrentStatus
        this.linkToCarl = interventionDto.linkToCarl
        this.localisation = interventionDto.localisation
        this.statusChangedAt = new Date(interventionDto.statusChangedAt)
    }

    public getItemId() {
        return this.id
    }

    public getItemTitle() {
        return `${this.title.trim()[0].toUpperCase() + this.title.slice(1)}`
    }

    public getItemDescription() {
        return this.code + ' / ' + this.regroupment
    }

    public getItemImageUrl() {
        return this.imageUrl
    }

    public getItemThumbnailUrl = getItemThumbnailUrl

    public getItemChips() {
        return [
            {
                label: this.currentStatus,
                color: 'disabled',
            },
        ]
    }

    public isRequiringAttention() {
        return false
    }

    public getItemTemporalData() {
        return getAnteriorityString(this.createdAt)
    }

    public getCoordinates() {
        return this.coordinates
    }

    public getItemRoute() {
        return this.itemRoute
    }

    public setItemRoute(itemRoute: RouteLocationRaw | undefined) {
        this.itemRoute = itemRoute
        return this
    }

    public getItemSecondaryAction() {
        return this.itemSecondaryAction
    }

    public setItemSecondaryAction(secondaryAction: SecondaryAction) {
        this.itemSecondaryAction = secondaryAction
        return this
    }
}
