import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { Intervention } from './Intervention'
import { InterventionDto } from './InterventionDto'
import { InterventionFilters } from '../filters/InterventionFilters'
import { SilabError } from '@/shared/errors/SilabError'

export class InterventionApi extends AzureTokenAccessApi {
    async getAll(
        serviceOfferId: ServiceOffer['id'],
        filter: InterventionFilters,
        abortSignal?: AbortSignal
    ): Promise<Intervention[]> {
        const queryParameters = filter.getFilters()

        try {
            const rawResponse = await this.getBackendData(
                `elus-service-offers/${serviceOfferId}/interventions`,
                queryParameters,
                abortSignal
            )

            const interventionApiResponse =
                (await rawResponse.json()) as InterventionDto[]

            // On veut instancier les interventions avec leurs classes finales/filles, on utilise pour cela le factory sur chaque Dto reçu
            return interventionApiResponse.map((interventionDto) => {
                return new Intervention(interventionDto)
            })
        } catch (error) {
            if (error instanceof DOMException) {
                return []
            }
            throw new SilabError(
                `Erreur lors du chargement des interventions`,
                error
            )
        }
    }

    async get(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        abortSignal?: AbortSignal
    ) {
        const rawResponse = await this.getBackendData(
            `elus-service-offers/${serviceOfferId}/interventions/${interventionId}`,
            new Map(),
            abortSignal
        )

        const interventionApiResponse =
            (await rawResponse.json()) as InterventionDto

        return new Intervention(interventionApiResponse)
    }
}
