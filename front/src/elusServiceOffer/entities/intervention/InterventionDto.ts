import { CoordinatesDto } from '@/shared/geocoding/entities/coordinate/CoordinatesDto'

export interface InterventionDto {
    id: string
    code: string
    title: string
    currentStatus: string
    regroupment: string
    coordinates?: CoordinatesDto
    address?: string
    createdAt: string
    imageUrl?: string
    beginDate: string
    endDate: string
    comment?: string
    createdBy: string
    history?: InterventionDto[]
    isCurrentStatus?: boolean
    linkToCarl?: string
    localisation?: string
    statusChangedAt: string
}
