export interface LegacyObjectFiltersInterface {
    hasFilters: () => boolean
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    getFilters: () => Map<any, any>
    getFilterQuantity: () => number
    reset: () => void
}
