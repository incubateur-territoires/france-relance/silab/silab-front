import { serviceOfferAdministrationRoutes } from '@/router/administrationRoutes'
import { ServiceOfferNamespaces } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { RouteRecordRaw } from 'vue-router'

export const elusServiceOfferRoutes: RouteRecordRaw[] = [
    {
        path: `${ServiceOfferNamespaces.ElusServiceOffer}/:serviceOfferId`,
        name: 'elus-service-offer-root',
        component: () => import('@/elusServiceOffer/views/RootView.vue'),
        meta: {
            title: 'Mon territoire',
            requiresAuth: true,
        },
        children: [
            {
                path: 'admin',
                component: () =>
                    import('@/serviceOffer/views/administration/RootView.vue'),
                meta: {
                    node: 'ElusServiceOffer/admin-root-route',
                },
                children: [
                    ...serviceOfferAdministrationRoutes(
                        'ElusServiceOffer' //Amelioration : se servir partout des `keyof typeof ServiceOfferNamespaces` ou similaire pour les names des routes ?
                    ),
                ],
            },
            {
                path: '',
                name: 'elus-service-offer/index',
                component: () =>
                    import(
                        '@/elusServiceOffer/views/intervention/InterventionListAndMapView.vue'
                    ),
                meta: {
                    title: 'Interventions',
                    requiresAuth: true,
                    icon: 'mdi-map-outline',
                },
            },
            // {
            //     path: 'statistiques',
            //     name: 'statistiques',
            //     component: () =>import('@/elusServiceOffer/views/dashboard/MetabaseDashboardView.vue),
            //     meta: {
            //         title: 'Statistiques',
            //         requiresAuth: true,
            //         icon: 'mdi-chart-box-outline',
            //     },
            // },
        ],
    },
]
