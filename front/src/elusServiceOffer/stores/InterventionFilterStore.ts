import { defineStore } from 'pinia'
import { ref } from 'vue'
import { InterventionFilters } from '@/elusServiceOffer/entities/filters/InterventionFilters'

export const useInterventionFilterStore = function (storeId: string) {
    const defineViewStore = defineStore(
        storeId,
        () => {
            const interventionFilters = ref<InterventionFilters>(
                new InterventionFilters(InterventionFilters.getDefaultFilters())
            )
            return { interventionFilters }
        },
        {
            persist: {
                pick: ['interventionFilters'],
            },
        }
    )
    return defineViewStore()
}
