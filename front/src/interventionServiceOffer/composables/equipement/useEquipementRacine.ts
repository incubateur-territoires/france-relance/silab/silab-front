import { useEquipement } from '@/interventionServiceOffer/entities/equipement/useEquipement'
import { InterventionServiceOffer } from '@/interventionServiceOffer/entities/interventionServiceOffer/InterventionServiceOffer'
import { useCommunFilterStore } from '@/interventionServiceOffer/stores/CommunFilterStore'
import { useQueryParamBinding } from '@/shared/routing/composables/useQueryParamBinding'
import { computed, onBeforeUnmount, Ref, WritableComputedRef } from 'vue'
import { z } from 'zod'

export function useEquipementRacine(
    serviceOfferId: Ref<InterventionServiceOffer['id']>
) {
    const communFiltersStore = useCommunFilterStore(
        'communFilters',
        serviceOfferId.value
    )
    const equipementRacineId: WritableComputedRef<string | undefined> =
        computed({
            get() {
                return communFiltersStore.communFilters.filters[
                    'equipementRacine.id'
                ]
            },
            set(equipementRacineIdStr: string | undefined) {
                communFiltersStore.communFilters.filters[
                    'equipementRacine.id'
                ] = equipementRacineIdStr
            },
        })
    const equipementRacine = useEquipement(
        equipementRacineId,
        serviceOfferId,
        true
    )
    useQueryParamBinding(
        'equipement-racine',
        equipementRacineId,
        z.string().optional()
    )

    //C'est du bricolage ! On fait ça car cette donnée reste quand on quitte et retourne sur une ODS
    onBeforeUnmount(() => {
        equipementRacineId.value = undefined
    })

    return equipementRacine
}
