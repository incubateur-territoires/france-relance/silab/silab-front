import { InterventionServiceOffer } from '@/interventionServiceOffer/entities/interventionServiceOffer/InterventionServiceOffer'
import { InterventionOrderBy } from '@/interventionServiceOffer/entities/orderBy/InterventionOrderBy'
import { useCommunFilterStore } from '@/interventionServiceOffer/stores/CommunFilterStore'
import { usePaginatedDemandeInterventions } from '@/interventionServiceOffer/stores/usePaginatedDemandeInterventions'
import { usePaginatedEquipements } from '@/interventionServiceOffer/stores/usePaginatedEquipements'
import { usePaginatedInterventions } from '@/interventionServiceOffer/stores/usePaginatedInterventions'
import { ObjectPins } from '@/shared/entities/pins/ObjectPins'
import { ObjectFilters } from '@/shared/filters/entities/ObjectFilters'
import { GeolocalisableItem } from '@/shared/item/entities/GeolocalisableItem'
import { ListableItem } from '@/shared/item/entities/ListableItem'
import { ComputedRef, Ref, computed, onBeforeUnmount, ref } from 'vue'
import { z } from 'zod'

export interface ItemList {
    title: 'demande' | 'intervention' | 'équipement'
    items: Array<GeolocalisableItem & ListableItem>
    maxItems: number | undefined
    allGeolocatedItems: Array<GeolocalisableItem & ListableItem>
    itemHeight: number
    isValidating: boolean
    isStale: boolean
    isLoading: boolean
    hasMore: boolean | undefined
    loadMore: () => Promise<void>
    reLoad: () => void
    updateStaleness: () => void
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    filters: ObjectFilters<any>
    orderBy?: InterventionOrderBy // Amélioration à changer avec un type générique plus tard
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    pins?: ObjectPins<any>
}

export type ItemLists = {
    demandes: ItemList
    interventions: ItemList
    équipements: ItemList
}

export const itemIcons: { [key in keyof ItemLists]: string } = {
    demandes: 'mdi-comment-question-outline',
    interventions: 'mdi-traffic-cone',
    équipements: 'mdi-cube-outline',
}

export const SelectedItemSchema = z.object({
    type: z.enum(['demandes', 'interventions', 'équipements']),
    id: z.string(),
})

export type SelectedItem = z.infer<typeof SelectedItemSchema>

export function useItemLists(
    interventionServiceOfferId: Ref<InterventionServiceOffer['id']>
) {
    const communFiltersStore = useCommunFilterStore(
        'communFilters',
        interventionServiceOfferId.value
    )

    const selectedItem = ref<SelectedItem | undefined>(undefined)

    // equipements
    const paginatedEquipements = usePaginatedEquipements(
        interventionServiceOfferId,
        communFiltersStore.communFilters
    )
    const equipementsItemList: ComputedRef<ItemList> = computed(() => {
        const securedEquipementItems =
            paginatedEquipements.securedData.value?.data ??
            ([] as (GeolocalisableItem & ListableItem)[])

        const equipementsItemsAvecPrimaryAction = securedEquipementItems.map(
            (item) => {
                item.setItemPrimaryAction?.(() => {
                    selectedItem.value = {
                        type: 'équipements',
                        id: item.getItemId(),
                    }
                })
                item.setItemSecondaryAction?.({
                    icon: 'mdi-subdirectory-arrow-right',
                    // mdi-file-tree-outline mdi-open-in-app arrow-bottom-right arrow-down arrow-expand-all arrow-right arrow-u-left-bottom chevron-right chevron-double-right chevron-down-box-outline collage
                    // debug-step-into filter filter-plus filter-variant graph graph-outline merge set-split
                    // location-enter segment subdirectory-arrow-right
                    action: () => {
                        communFiltersStore.communFilters.filters[
                            'equipementRacine.id'
                        ] = item.getItemId()
                    },
                })
                return item
            }
        )
        return {
            title: 'équipement',
            items: equipementsItemsAvecPrimaryAction,
            maxItems: paginatedEquipements.maxResults.value,
            allGeolocatedItems:
                paginatedEquipements.securedDataCoordinates.value?.data ??
                ([] as (GeolocalisableItem & ListableItem)[]),
            itemHeight: 92,
            isValidating: paginatedEquipements.isLoading.value,
            isStale: paginatedEquipements.isStale.value,
            isLoading: paginatedEquipements.isLoading.value,
            hasMore: paginatedEquipements.hasMoreResult.value,
            loadMore: paginatedEquipements.loadNextPage,
            reLoad: () => {
                paginatedEquipements.loadFirstPage()
                paginatedEquipements.loadCoordinates()
            },
            updateStaleness: paginatedEquipements.updateStaleness,
            filters: paginatedEquipements.equipementFilters.value,
            pins: paginatedEquipements.equipementPins.value,
        }
    })

    // demandes
    const paginatedDemandesInterventions = usePaginatedDemandeInterventions(
        interventionServiceOfferId,
        communFiltersStore.communFilters
    )

    const demandesInterventionsItemList: ComputedRef<ItemList> = computed(
        () => {
            const securedDemandesInterventionsItems =
                paginatedDemandesInterventions.securedData.value?.data ??
                ([] as (GeolocalisableItem & ListableItem)[])

            const demandesInterventionsitemsAvecPrimaryAction =
                securedDemandesInterventionsItems.map((item) => {
                    item.setItemPrimaryAction?.(() => {
                        selectedItem.value = {
                            type: 'demandes',
                            id: item.getItemId(),
                        }
                    })
                    return item
                })
            return {
                title: 'demande',
                items: demandesInterventionsitemsAvecPrimaryAction,
                maxItems: paginatedDemandesInterventions.maxResults.value,
                allGeolocatedItems:
                    paginatedDemandesInterventions.securedDataCoordinates.value
                        ?.data ?? ([] as (GeolocalisableItem & ListableItem)[]),
                itemHeight: 92,
                isValidating: paginatedDemandesInterventions.isLoading.value,
                isStale: paginatedDemandesInterventions.isStale.value,
                isLoading: paginatedDemandesInterventions.isLoading.value,
                hasMore: paginatedDemandesInterventions.hasMoreResult.value,
                loadMore: paginatedDemandesInterventions.loadNextPage,
                reLoad: () => {
                    paginatedDemandesInterventions.loadFirstPage()
                    paginatedDemandesInterventions.loadCoordinates()
                },
                updateStaleness: paginatedDemandesInterventions.updateStaleness,
                filters:
                    paginatedDemandesInterventions.demandeInterventionFilters
                        .value,
            }
        }
    )

    // interventions
    const paginatedInterventions = usePaginatedInterventions(
        interventionServiceOfferId,
        communFiltersStore.communFilters
    )

    const interventionsItemList: ComputedRef<ItemList> = computed(() => {
        const securedInterventionsItems =
            paginatedInterventions.securedData.value?.data ??
            ([] as (GeolocalisableItem & ListableItem)[])

        const interventionsitemsAvecPrimaryAction =
            securedInterventionsItems.map((item) => {
                item.setItemPrimaryAction?.(() => {
                    selectedItem.value = {
                        type: 'interventions',
                        id: item.getItemId(),
                    }
                })
                return item
            })
        return {
            title: 'intervention',
            items: interventionsitemsAvecPrimaryAction,
            maxItems: paginatedInterventions.maxResults.value,
            allGeolocatedItems:
                paginatedInterventions.securedDataCoordinates.value?.data ??
                ([] as (GeolocalisableItem & ListableItem)[]),
            itemHeight: 92,
            isValidating: paginatedInterventions.isLoading.value,
            isStale: paginatedInterventions.isStale.value,
            isLoading: paginatedInterventions.isLoading.value,
            hasMore: paginatedInterventions.hasMoreResult.value,
            loadMore: paginatedInterventions.loadNextPage,
            reLoad: () => {
                paginatedInterventions.loadFirstPage()
                paginatedInterventions.loadCoordinates()
            },
            updateStaleness: paginatedInterventions.updateStaleness,
            filters: paginatedInterventions.interventionFilters.value,
            orderBy: paginatedInterventions.interventionOrderBy.value,
        }
    })

    function abortAllItemsListsRequests() {
        paginatedDemandesInterventions.abortAllRequests()
        paginatedInterventions.abortAllRequests()
        paginatedEquipements.abortAllRequests()
    }

    onBeforeUnmount(() => {
        abortAllItemsListsRequests()
    })

    const itemLists = computed(() => {
        return {
            équipements: equipementsItemList.value,
            demandes: demandesInterventionsItemList.value,
            interventions: interventionsItemList.value,
        }
    })

    return { itemLists, selectedItem }
}
