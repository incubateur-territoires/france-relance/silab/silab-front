import { ref, onBeforeUnmount, watch, Ref } from 'vue'
import { MsalContext } from '@/plugins/msal/useMsal'
import { RequestAborter } from '@/shared/api/entities/RequestAborter'
import { isUndefined } from '@/shared/helpers/types'
import { SilabError } from '@/shared/errors/SilabError'
import { EquipementDto } from '@/interventionServiceOffer/entities/equipement/EquipementDto'
import { Equipement } from '@/interventionServiceOffer/entities/equipement/Equipement'
import { EquipementFilters } from '@/interventionServiceOffer/entities/equipement/filters/EquipementFilters'
import { GmaoConfigurationIntervention } from '@/interventionServiceOffer/entities/gmaoConfigurationIntervention/GmaoConfigurationIntervention'
import { GmaoConfigurationInterventionApi } from '@/interventionServiceOffer/entities/gmaoConfigurationIntervention/GmaoConfigurationInterventionApi'

export function useRechercheGmaoConfigurationInterventionEquipement(
    msal: MsalContext,
    gmaoConfigurationInterventionId: Ref<
        GmaoConfigurationIntervention['id'] | undefined
    >
) {
    const gmaoConfigurationInterventionApi =
        new GmaoConfigurationInterventionApi(msal)
    const equipementsFetchAborter: RequestAborter = new RequestAborter()

    const labelRecherché: Ref<EquipementDto['label'] | undefined> =
        ref(undefined)
    const resultatsRecherche: Ref<Equipement[]> = ref([])

    let updateChoixEquipementsTimeout: undefined | number = undefined

    watch(labelRecherché, () => {
        window.clearTimeout(updateChoixEquipementsTimeout)
        resultatsRecherche.value = []
        updateChoixEquipementsTimeout = window.setTimeout(() => {
            updateResultatsRecherche()
        }, 500)
    })

    function updateResultatsRecherche() {
        if (
            isUndefined(gmaoConfigurationInterventionId.value) ||
            isUndefined(labelRecherché.value) ||
            labelRecherché.value === ''
        ) {
            resultatsRecherche.value = []
            return
        }

        equipementsFetchAborter.abortRequestsAndRefreshAborter()
        gmaoConfigurationInterventionApi
            .getAllEquipements(
                gmaoConfigurationInterventionId.value,
                new EquipementFilters({
                    labelOuCode: `*${labelRecherché.value}*`, // Recherche permissive,
                    'currentStatus.code': ['active'],
                    limit: 50,
                }),
                equipementsFetchAborter.getSignal()
            )
            .then((fetchedEquipements) => {
                resultatsRecherche.value = fetchedEquipements
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError((<Error>error).message, 'error')
                }
            })
    }

    onBeforeUnmount(() => {
        equipementsFetchAborter.abortRequests()
    })

    return {
        labelRecherché,
        resultatsRecherche,
        isLoading: gmaoConfigurationInterventionApi.isProcessing,
    }
}
