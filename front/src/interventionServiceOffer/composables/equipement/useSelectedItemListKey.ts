import { toRef } from 'vue'
import { z } from 'zod'
import { useInterventionServiceOfferUserPreference } from '../user/useInterventionServiceOfferUserPreference'
import { useQueryParamBinding } from '@/shared/routing/composables/useQueryParamBinding'

export function useSelectedItemListKey() {
    const allowedItemListKeysSchema = z.enum([
        'demandes',
        'interventions',
        'équipements',
    ])

    const interventionServiceOfferUserPreference =
        useInterventionServiceOfferUserPreference()

    const selectedItemListKey = toRef(
        interventionServiceOfferUserPreference.value,
        'globalListSelectedItemType'
    )

    useQueryParamBinding('tab', selectedItemListKey, allowedItemListKeysSchema)
    return selectedItemListKey
}
