import { ref, onBeforeUnmount, watch, Ref } from 'vue'
import { MsalContext } from '@/plugins/msal/useMsal'
import { GmaoActeurApi } from '@/interventionServiceOffer/entities/gmaoActeur/GmaoActeurApi'
import { RequestAborter } from '@/shared/api/entities/RequestAborter'
import { GmaoActeur } from '@/shared/gmaoActeur/GmaoActeur'
import { GmaoActeurFilters } from '@/interventionServiceOffer/entities/filters/GmaoActeurFilters'
import { isUndefined, requiredInject } from '@/shared/helpers/types'
import { serviceOfferIdInjectionKey } from '@/shared/constants'
import { SilabError } from '@/shared/errors/SilabError'

export function useRechercheGmaoActeur(msal: MsalContext) {
    const gmaoActeurApi = new GmaoActeurApi(msal)
    const gmaoActeurFetchAborter = new RequestAborter()

    const nomCompletRecherché: Ref<string | undefined> = ref(undefined)
    const resultatsRecherche: Ref<GmaoActeur[]> = ref([])

    const serviceOfferId = requiredInject(serviceOfferIdInjectionKey)

    let updateChoixIntervenantsTimeout: undefined | number = undefined

    watch(nomCompletRecherché, () => {
        window.clearTimeout(updateChoixIntervenantsTimeout)
        updateChoixIntervenantsTimeout = window.setTimeout(() => {
            updateResultatsRecherche()
        }, 500)
    })

    function updateResultatsRecherche() {
        if (
            isUndefined(nomCompletRecherché.value) ||
            nomCompletRecherché.value === ''
        ) {
            resultatsRecherche.value = []
            return
        }

        gmaoActeurFetchAborter.abortRequestsAndRefreshAborter()
        gmaoActeurApi
            .getAll(
                serviceOfferId.value,
                new GmaoActeurFilters({
                    nomComplet: `*${nomCompletRecherché.value}*`, // Recherche permissive
                    limit: 50,
                    'currentStatus.code': ['active'],
                }),
                gmaoActeurFetchAborter.getSignal()
            )
            .then((fetchedGmaoActeurs) => {
                resultatsRecherche.value = fetchedGmaoActeurs
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError((<Error>error).message, 'error')
                }
            })
    }

    onBeforeUnmount(() => {
        gmaoActeurFetchAborter.abortRequests()
    })

    return {
        nomCompletRecherché,
        resultatsRecherche,
        isLoading: gmaoActeurApi.isProcessing,
    }
}
