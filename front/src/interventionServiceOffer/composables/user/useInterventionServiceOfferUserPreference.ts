import { requiredInject } from '@/shared/helpers/types'
import { serviceOfferIdInjectionKey } from '@/shared/constants'
import { useUserPreferenceStore } from '@/shared/user/stores/UserPreferenceStore'
import { computed } from 'vue'
import { initializePropertyIfNotExisting } from '@/shared/helpers/objects'
import { InterventionServiceOfferUserPreference } from '@/interventionServiceOffer/entities/user/InterventionServiceOfferUserPreference'
import { useMsal } from '@/plugins/msal/useMsal'

export function useInterventionServiceOfferUserPreference() {
    const msal = useMsal()
    const userPreferenceStore = useUserPreferenceStore(msal)

    const serviceOfferId = requiredInject(serviceOfferIdInjectionKey)

    const interventionServiceOfferUserPreference = computed({
        get() {
            // si le store n'a pas été initialisé, c'est ici qu'on enregistre la valeur par défaut
            initializePropertyIfNotExisting<InterventionServiceOfferUserPreference>(
                userPreferenceStore.interventionServiceOfferPreferences,
                serviceOfferId.value,
                {
                    globalListSelectedItemType: 'interventions',
                }
            )
            return userPreferenceStore.interventionServiceOfferPreferences[
                serviceOfferId.value
            ]
        },
        set(newPreferences: InterventionServiceOfferUserPreference) {
            userPreferenceStore.interventionServiceOfferPreferences[
                serviceOfferId.value
            ] = newPreferences
        },
    })
    return interventionServiceOfferUserPreference
}
