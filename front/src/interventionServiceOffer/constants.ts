import { InterventionServiceOffer } from '@/interventionServiceOffer/entities/interventionServiceOffer/InterventionServiceOffer'
import { InjectionKey, Ref } from 'vue'
import { Equipement } from './entities/equipement/Equipement'
import { VolatileRef, VolatileSecuredData } from '@/shared/helpers/lifeCycle'
import { Intervention } from './entities/intervention/Intervention'
import { DemandeInterventionHistorisedStatus } from './entities/demandeIntervention/status/DemandeInterventionHistorisedStatus'
import { ActiviteDiagnostique } from './entities/activite/ActiviteDiagnostique'
import { ActiviteAction } from './entities/activite/ActiviteAction'
import { Compteur } from './entities/compteur/Compteur'
import { DemandeIntervention } from './entities/demandeIntervention/DemandeIntervention'
import { GmaoActeur } from '@/shared/gmaoActeur/GmaoActeur'
import { InterventionHistorisedStatus } from './entities/intervention/status/InterventionHistorisedStatus'

export const interventionServiceOfferInjectionKey = Symbol() as InjectionKey<
    Ref<VolatileSecuredData<InterventionServiceOffer>>
>

export const interventionIdInjectionKey = Symbol() as InjectionKey<
    Intervention['id']
>

/** @deprecated */
export const legacyInterventionInjectionKey = Symbol() as InjectionKey<
    VolatileRef<Intervention | undefined>
>

export const interventionInjectionKey = Symbol() as InjectionKey<
    Ref<VolatileSecuredData<Intervention>>
>

export const interventionsInjectionKey = Symbol() as InjectionKey<
    VolatileRef<Intervention[]>
>

export const activitesActionInjectionKey = Symbol() as InjectionKey<{
    securedActivitesAction: Ref<VolatileSecuredData<ActiviteAction[]>>
    reload: () => Promise<void>
}>

export const activitesDiagnostiqueInjectionKey = Symbol() as InjectionKey<{
    securedActivitesDiagnostique: Ref<
        VolatileSecuredData<ActiviteDiagnostique[]>
    >
    reload: () => Promise<void>
}>

export const demandeInterventionIdInjectionKey = Symbol() as InjectionKey<
    DemandeIntervention['id']
>

export const demandeInterventionInjectionKey = Symbol() as InjectionKey<
    Ref<VolatileSecuredData<DemandeIntervention>>
>

export const demandesInterventionsInjectionKey = Symbol() as InjectionKey<
    VolatileRef<DemandeIntervention[]>
>

export const compteursInjectionKey = Symbol() as InjectionKey<
    VolatileRef<Compteur[]>
>

export const demandeInterventionStatusHistroyInjectionKey =
    Symbol() as InjectionKey<VolatileRef<DemandeInterventionHistorisedStatus[]>>

export const interventionStatusHistoryInjectionKey = Symbol() as InjectionKey<
    VolatileRef<InterventionHistorisedStatus[]>
>

export const childrenEquipementsInjectionKey = Symbol() as InjectionKey<
    Ref<VolatileSecuredData<Equipement[]>>
>
export const availableEquipementsInjectionKey = Symbol() as InjectionKey<
    Ref<VolatileSecuredData<Equipement[]>>
>
export const isValidatingAvailableEquipementsInjectionKey =
    Symbol() as InjectionKey<Ref<boolean>>

export const equipementInjectionKey = Symbol() as InjectionKey<
    Ref<VolatileSecuredData<Equipement>>
>

export const equipementRacineIdInjectionKey = Symbol() as InjectionKey<
    Ref<Equipement['id']>
>

export const isValidatingDemandesInterventionsInjectionKey =
    Symbol() as InjectionKey<Ref<boolean>>

export const isValidatingInterventionsInjectionKey = Symbol() as InjectionKey<
    Ref<boolean>
>
export const myselfGmaoActeurInjectionKey = Symbol() as InjectionKey<
    Ref<GmaoActeur | undefined>
>
