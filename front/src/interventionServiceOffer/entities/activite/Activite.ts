import { RouteLocationRaw } from 'vue-router'
import {
    ListableItem,
    SecondaryAction,
    getItemThumbnailUrl,
} from '@/shared/item/entities/ListableItem'
import { Equipement } from '../equipement/Equipement'
import { ActiviteDto } from './ActiviteDto'
import { isDefined, isUndefined } from '@/shared/helpers/types'
import { GmaoActeur } from '@/shared/gmaoActeur/GmaoActeur'
import { Operation } from './Operation'

export class Activite implements ListableItem {
    id: string
    description?: string
    operation?: Operation
    equipementCible?: Equipement
    dateDeDebut?: Date
    intervenant?: GmaoActeur
    createdBy?: GmaoActeur
    duree?: number
    isObligatoire: boolean
    protected itemRoute?: RouteLocationRaw
    protected itemSecondaryAction?: SecondaryAction

    public constructor(activiteDto: ActiviteDto) {
        this.id = activiteDto.id
        this.description = activiteDto.description
        this.operation = activiteDto.operation
        this.dateDeDebut = isUndefined(activiteDto.dateDeDebut)
            ? undefined
            : new Date(activiteDto.dateDeDebut)
        this.equipementCible = isDefined(activiteDto.equipementCible)
            ? new Equipement(activiteDto.equipementCible)
            : undefined
        this.intervenant = isDefined(activiteDto.intervenant)
            ? new GmaoActeur(activiteDto.intervenant)
            : undefined
        this.createdBy = isDefined(activiteDto.createdBy)
            ? new GmaoActeur(activiteDto.createdBy)
            : undefined
        this.duree = activiteDto.duree
        this.isObligatoire = activiteDto.isObligatoire
    }
    public getItemId() {
        return this.id
    }
    public getItemTitle() {
        return this.description ?? ''
    }

    public getItemDescription() {
        const description: string[] = []
        if (isDefined(this.operation)) {
            description.push(this.operation.label)
        }
        if (isDefined(this.equipementCible)) {
            description.push(this.equipementCible.label)
            description.push(this.equipementCible.code)
        }
        return description.join(' ')
    }
    public getItemImageUrl() {
        return undefined
    }

    public getItemThumbnailUrl = getItemThumbnailUrl

    public getItemChips() {
        return []
    }

    public isRequiringAttention() {
        return false
    }

    public getItemTemporalData() {
        return this.dateDeDebut
            ? 'le ' +
                  this.dateDeDebut?.toLocaleDateString(undefined, {
                      weekday: 'long',
                      year: 'numeric',
                      month: 'long',
                      day: 'numeric',
                      hour: 'numeric',
                      minute: 'numeric',
                  })
            : ''
    }
    public getItemRoute() {
        return this.itemRoute
    }
    public setItemRoute(itemRoute: RouteLocationRaw | undefined) {
        this.itemRoute = itemRoute
        return this
    }
    public getItemSecondaryAction() {
        return this.itemSecondaryAction
    }

    public setItemSecondaryAction(secondaryAction: SecondaryAction) {
        this.itemSecondaryAction = secondaryAction
        return this
    }

    public isDone() {
        return isDefined(this.dateDeDebut)
    }

    public getFormatedDurée() {
        if (isUndefined(this.duree)) {
            return undefined
        }
        if (this.duree > 0) {
            const heures = Math.floor(this.duree / 60)
            const minutes = this.duree % 60
            return `${heures}h${String(minutes).padStart(2, '0')}`
        }
    }

    public canBeDeletedBy(gmaoActeur: GmaoActeur): boolean {
        return (
            gmaoActeur.id === this.intervenant?.id ||
            gmaoActeur.id === this.createdBy?.id
        )
    }
}
