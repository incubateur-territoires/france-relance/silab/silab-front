import { Activite } from './Activite'
import { ActiviteActionDto } from './ActiviteActionDto'
import { Clonable } from '@/shared/helpers/lifeCycle'
import { activiteActionMapper } from './ActiviteActionMapper'

export class ActiviteAction
    extends Activite
    implements Clonable<ActiviteAction>
{
    public constructor(activiteActionDto: ActiviteActionDto) {
        super(activiteActionDto)
    }

    public clone(): ActiviteAction {
        const clone = new ActiviteAction(
            window.structuredClone(activiteActionMapper.toDto(this))
        )

        clone.itemRoute = this.itemRoute

        return clone
    }
}
