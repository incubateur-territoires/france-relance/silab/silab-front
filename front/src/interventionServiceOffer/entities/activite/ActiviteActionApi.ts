import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { Intervention } from '../intervention/Intervention'
import { ActiviteAction } from './ActiviteAction'
import { ActiviteActionDto, ActiviteActionDtoSchema } from './ActiviteActionDto'
import { EditActiviteActionDto } from './EditActiviteActionDto'
import { CreateActiviteActionDto } from './CreateActiviteActionDto '
import { ActiviteFilters } from '../filters/ActiviteFilters'

export class ActiviteActionApi extends AzureTokenAccessApi {
    async getAll(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        filters: ActiviteFilters,
        abortSignal?: AbortSignal
    ): Promise<ActiviteAction[]> {
        const queryParameters = filters.toQueryParameters()
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions/${interventionId}/activites-action`,
            queryParameters,
            abortSignal
        )

        const activitesActionDto = ActiviteActionDtoSchema.array().parse(
            await rawResponse.json()
        )
        return activitesActionDto.map((activiteDto) => {
            return new ActiviteAction(activiteDto)
        })
    }
    async update(
        serviceOfferId: ServiceOffer['id'],
        activiteId: ActiviteAction['id'],
        updatedActiviteAction: EditActiviteActionDto
    ) {
        const rawResponse = await this.putBackendData(
            `intervention-service-offers/${serviceOfferId}/activites-action/${activiteId}`,
            JSON.stringify(updatedActiviteAction)
        )

        const activiteApiResponse =
            (await rawResponse.json()) as ActiviteActionDto

        return new ActiviteAction(activiteApiResponse)
    }

    async create(
        newActiviteAction: CreateActiviteActionDto,
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id']
    ) {
        const rawResponse = await this.postBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions/${interventionId}/activites-action`,
            JSON.stringify(newActiviteAction)
        )

        const activiteActionDto = ActiviteActionDtoSchema.parse(
            await rawResponse.json()
        )

        return new ActiviteAction(activiteActionDto)
    }

    async delete(
        serviceOfferId: ServiceOffer['id'],
        activiteActionId: ActiviteAction['id']
    ) {
        await this.deleteBackendData(
            `intervention-service-offers/${serviceOfferId}/activites-action/${activiteActionId}`
        )
    }
}
