import { ActiviteDto, ActiviteDtoSchema } from './ActiviteDto'

export const ActiviteActionDtoSchema = ActiviteDtoSchema.extend({})

export type ActiviteActionDto = ActiviteDto
