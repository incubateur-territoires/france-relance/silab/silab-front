import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import {
    EditActiviteActionDto,
    EditActiviteActionDtoSchema,
} from './EditActiviteActionDto'
import { ActiviteActionDto, ActiviteActionDtoSchema } from './ActiviteActionDto'
import { ActiviteAction } from './ActiviteAction'
import { isDefined } from '@/shared/helpers/types'
import {
    CreateActiviteActionDto,
    CreateActiviteActionDtoSchema,
} from './CreateActiviteActionDto '
import { equipementMapper } from '../equipement/EquipementMapper'
import { gmaoActeurMapper } from '@/shared/gmaoActeur/GmaoActeurMapper'
import { ZodError } from 'zod'

interface ActiviteActionMapper
    extends Pick<EntityMapper<ActiviteAction>, 'toEditDto'> {
    toDto: (partialEntity: Partial<ActiviteAction>) => ActiviteActionDto
    toEditDto: (partialEntity: Partial<ActiviteAction>) => EditActiviteActionDto
    toCreateDto: (
        partialEntity: Partial<ActiviteAction>
    ) => CreateActiviteActionDto
}

export const activiteActionMapper: ActiviteActionMapper = {
    toEditDto(partialActivite: Partial<ActiviteAction>): EditActiviteActionDto {
        try {
            return EditActiviteActionDtoSchema.parse({
                dateDeDebut: partialActivite.dateDeDebut?.toISOString(),
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'activité fournit ne possède pas tous les attributs requis.",
                    error
                )
            }
            throw error
        }
    },
    toCreateDto: function (
        partialActivite: Partial<ActiviteAction>
    ): CreateActiviteActionDto {
        try {
            return CreateActiviteActionDtoSchema.parse({
                intervenantId: partialActivite.intervenant?.id,
                duree: partialActivite.duree,
                dateDeDebut: partialActivite.dateDeDebut?.toISOString(),
                operationId: partialActivite?.operation?.id,
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'activité fournit ne possède pas tous les attributs requis.",
                    error
                )
            }
            throw error
        }
    },
    toDto: function (
        partialActiviteAction: Partial<ActiviteAction>
    ): ActiviteActionDto {
        try {
            return ActiviteActionDtoSchema.parse({
                id: partialActiviteAction.id,
                description: partialActiviteAction.description,
                operation: partialActiviteAction.operation,
                equipementCible: isDefined(
                    partialActiviteAction.equipementCible
                )
                    ? equipementMapper.toDto(
                          partialActiviteAction.equipementCible
                      )
                    : undefined,
                dateDeDebut: isDefined(partialActiviteAction.dateDeDebut)
                    ? partialActiviteAction.dateDeDebut.toISOString()
                    : undefined,
                intervenant: isDefined(partialActiviteAction.intervenant)
                    ? gmaoActeurMapper.toDto(partialActiviteAction.intervenant)
                    : undefined,
                createdBy: isDefined(partialActiviteAction.createdBy)
                    ? gmaoActeurMapper.toDto(partialActiviteAction.createdBy)
                    : undefined,

                duree: partialActiviteAction.duree,
                isObligatoire: partialActiviteAction.isObligatoire,
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'activité fournit ne possède pas tous les attributs requis.",
                    error
                )
            }
            throw error
        }
    },
}
