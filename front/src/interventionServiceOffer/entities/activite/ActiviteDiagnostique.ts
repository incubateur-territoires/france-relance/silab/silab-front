import { isDefined } from '@/shared/helpers/types'
import { Activite } from './Activite'
import { ActiviteDiagnostiqueDto } from './ActiviteDiagnostiqueDto'
import { Intervention } from '../intervention/Intervention'
import { DemandeIntervention } from '../demandeIntervention/DemandeIntervention'
import { Clonable } from '@/shared/helpers/lifeCycle'
import { activiteDiagnostiqueMapper } from './ActiviteDiagnostiqueMapper'

export class ActiviteDiagnostique
    extends Activite
    implements Clonable<ActiviteDiagnostique>
{
    evaluation?: 'conforme' | 'reserve' | 'non conforme'
    interventionIdGeneree?: Intervention['id']
    demandeInterventionIdGeneree?: DemandeIntervention['id']

    public constructor(activiteDiagnostiqueDto: ActiviteDiagnostiqueDto) {
        super(activiteDiagnostiqueDto)
        this.evaluation = activiteDiagnostiqueDto.evaluation
        this.interventionIdGeneree =
            activiteDiagnostiqueDto.interventionIdGeneree
        this.demandeInterventionIdGeneree =
            activiteDiagnostiqueDto.demandeInterventionIdGeneree
    }

    public isDone() {
        return isDefined(this.evaluation)
    }

    public clone(): ActiviteDiagnostique {
        const clone = new ActiviteDiagnostique(
            window.structuredClone(activiteDiagnostiqueMapper.toDto(this))
        )

        clone.itemRoute = this.itemRoute

        return clone
    }
}
