import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { Intervention } from '../intervention/Intervention'
import { ActiviteDiagnostique } from './ActiviteDiagnostique'
import {
    ActiviteDiagnostiqueDto,
    ActiviteDiagnostiqueDtoSchema,
} from './ActiviteDiagnostiqueDto'
import { EditActiviteDiagnostiqueDto } from './EditActiviteDagnostiqueDto'
import { ActiviteFilters } from '../filters/ActiviteFilters'

export class ActiviteDiagnostiqueApi extends AzureTokenAccessApi {
    async getAll(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        filters: ActiviteFilters,
        abortSignal?: AbortSignal
    ): Promise<ActiviteDiagnostique[]> {
        const queryParameters = filters.toQueryParameters()
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions/${interventionId}/activites-diagnostique`,
            queryParameters,
            abortSignal
        )

        const activitesApiResponse =
            ActiviteDiagnostiqueDtoSchema.array().parse(
                await rawResponse.json()
            )

        const toto = activitesApiResponse.map((activiteDto) => {
            return new ActiviteDiagnostique(activiteDto)
        })

        return toto
    }
    async update(
        serviceOfferId: ServiceOffer['id'],
        activiteId: ActiviteDiagnostique['id'],
        updatedActiviteDiagnostique: EditActiviteDiagnostiqueDto
    ) {
        const rawResponse = await this.putBackendData(
            `intervention-service-offers/${serviceOfferId}/activites-diagnostique/${activiteId}`,
            JSON.stringify(updatedActiviteDiagnostique)
        )

        const activiteApiResponse =
            (await rawResponse.json()) as ActiviteDiagnostiqueDto

        return new ActiviteDiagnostique(activiteApiResponse)
    }
}
