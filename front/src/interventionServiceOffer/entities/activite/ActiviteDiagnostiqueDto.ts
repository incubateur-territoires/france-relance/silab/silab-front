import { z } from 'zod'
import { DemandeInterventionDtoSchema } from '../demandeIntervention/DemandeInterventionDto'
import { InterventionDtoSchema } from '../intervention/InterventionDto'
import { ActiviteDtoSchema } from './ActiviteDto'

export const ActiviteDiagnostiqueDtoSchema = ActiviteDtoSchema.extend({
    evaluation: z.enum(['conforme', 'reserve', 'non conforme']).optional(),
    interventionIdGeneree: InterventionDtoSchema.shape.id.optional(),
    demandeInterventionIdGeneree:
        DemandeInterventionDtoSchema.shape.id.optional(),
})

export type ActiviteDiagnostiqueDto = z.infer<
    typeof ActiviteDiagnostiqueDtoSchema
>
