import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { ActiviteDiagnostique } from './ActiviteDiagnostique'
import { isDefined, isUndefined } from '@/shared/helpers/types'
import { EditActiviteDiagnostiqueDto } from './EditActiviteDagnostiqueDto'
import {
    ActiviteDiagnostiqueDto,
    ActiviteDiagnostiqueDtoSchema,
} from './ActiviteDiagnostiqueDto'
import { equipementMapper } from '../equipement/EquipementMapper'
import { gmaoActeurMapper } from '@/shared/gmaoActeur/GmaoActeurMapper'
import { ZodError } from 'zod'

interface ActiviteDiagnostiqueMapper
    extends Pick<EntityMapper<ActiviteDiagnostique>, 'toEditDto'> {
    toDto: (
        partialEntity: Partial<ActiviteDiagnostique>
    ) => ActiviteDiagnostiqueDto
    toEditDto: (
        partialEntity: Partial<ActiviteDiagnostique>
    ) => EditActiviteDiagnostiqueDto
}

export const activiteDiagnostiqueMapper: ActiviteDiagnostiqueMapper = {
    toEditDto(
        partialActivite: Partial<ActiviteDiagnostique>
    ): EditActiviteDiagnostiqueDto {
        if (isUndefined(partialActivite.evaluation)) {
            throw new IncompleteEntity(
                'Activite est incomplète',
                'evaluation null'
            )
        }
        if (isUndefined(partialActivite.dateDeDebut)) {
            throw new IncompleteEntity(
                'Activite est incomplète',
                'dateDeRealisation null'
            )
        }
        const editActiviteDiagnostiqueDto: EditActiviteDiagnostiqueDto = {
            dateDeDebut: partialActivite.dateDeDebut.toISOString(),
            evaluation: partialActivite.evaluation,
            interventionIdGeneree: partialActivite.interventionIdGeneree,
            demandeInterventionIdGeneree:
                partialActivite.demandeInterventionIdGeneree,
        }

        switch (editActiviteDiagnostiqueDto.evaluation) {
            case 'non conforme':
                if (
                    isUndefined(
                        editActiviteDiagnostiqueDto.interventionIdGeneree
                    )
                ) {
                    throw new IncompleteEntity(
                        "Absence de l'id de l'intervention générée"
                    )
                }
                break
            case 'reserve':
                if (
                    isUndefined(
                        editActiviteDiagnostiqueDto.demandeInterventionIdGeneree
                    )
                ) {
                    throw new IncompleteEntity(
                        "Absence de l'id de la demande d'intervention générée"
                    )
                }
                break
        }
        return editActiviteDiagnostiqueDto
    },
    toDto: function (
        partialActiviteDiagnostique: Partial<ActiviteDiagnostique>
    ): ActiviteDiagnostiqueDto {
        try {
            return ActiviteDiagnostiqueDtoSchema.parse({
                id: partialActiviteDiagnostique.id,
                description: partialActiviteDiagnostique.description,
                operation: window.structuredClone(
                    partialActiviteDiagnostique.operation
                ),
                equipementCible: isDefined(
                    partialActiviteDiagnostique.equipementCible
                )
                    ? window.structuredClone(
                          equipementMapper.toDto(
                              partialActiviteDiagnostique.equipementCible
                          )
                      )
                    : undefined,
                dateDeDebut: isDefined(partialActiviteDiagnostique.dateDeDebut)
                    ? partialActiviteDiagnostique.dateDeDebut.toISOString()
                    : undefined,
                intervenant: isDefined(partialActiviteDiagnostique.intervenant)
                    ? window.structuredClone(
                          gmaoActeurMapper.toDto(
                              partialActiviteDiagnostique.intervenant
                          )
                      )
                    : undefined,
                duree: partialActiviteDiagnostique.duree,
                isObligatoire: partialActiviteDiagnostique.isObligatoire,
                evaluation: partialActiviteDiagnostique.evaluation,
                interventionIdGeneree:
                    partialActiviteDiagnostique.interventionIdGeneree,
                demandeInterventionIdGeneree:
                    partialActiviteDiagnostique.demandeInterventionIdGeneree,
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'activité fournit ne possède pas tous les attributs requis.",
                    error
                )
            }
            throw error
        }
    },
}
