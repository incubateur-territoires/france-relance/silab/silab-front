import { GmaoActeurDtoSchema } from '@/shared/gmaoActeur/GmaoActeurDto'
import { EquipementDtoSchema } from '../equipement/EquipementDto'
import { z } from 'zod'
import { OperationDtoSchema } from './OperationDto'

export const ActiviteDtoSchema = z.object({
    id: z.string(),
    description: z.string().optional(),
    //amelioration : operation me semble un meilleur nom pour ce membre
    operation: OperationDtoSchema.optional(),
    equipementCible: EquipementDtoSchema.optional(),
    intervenant: GmaoActeurDtoSchema.optional(),
    createdBy: GmaoActeurDtoSchema.optional(),
    duree: z.number().optional(),
    dateDeDebut: z.string().optional(),
    isObligatoire: z.boolean(),
})

export type ActiviteDto = z.infer<typeof ActiviteDtoSchema>
