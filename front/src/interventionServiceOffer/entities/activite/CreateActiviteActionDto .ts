import { CreateActiviteDto, CreateActiviteDtoSchema } from './CreateActiviteDto'

export const CreateActiviteActionDtoSchema = CreateActiviteDtoSchema.extend({})

export type CreateActiviteActionDto = CreateActiviteDto
