import { CreateDto } from '@/shared/api/entities/Dtos'
import { Activite } from './Activite'
import { z } from 'zod'
import { GmaoActeurDtoSchema } from '@/shared/gmaoActeur/GmaoActeurDto'
import { GmaoActeur } from '@/shared/gmaoActeur/GmaoActeur'

export const CreateActiviteDtoSchema = z.object({
    intervenantId: GmaoActeurDtoSchema.shape.id.optional(),
    duree: z.number().optional(),
    dateDeDebut: z.string().optional(),
    operationId: z.string().optional(),
})

export interface CreateActiviteDto
    extends CreateDto<Activite>,
        z.infer<typeof CreateActiviteDtoSchema> {
    intervenantId?: GmaoActeur['id']
    duree?: number
    dateDeDebut?: string
    operationId?: string
}
