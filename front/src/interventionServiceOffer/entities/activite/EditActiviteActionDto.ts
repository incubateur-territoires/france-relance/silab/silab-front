import { EditDto } from '@/shared/api/entities/Dtos'
import { ActiviteAction } from './ActiviteAction'
import { z } from 'zod'

export const EditActiviteActionDtoSchema = z.object({
    dateDeDebut: z.string(),
})

export interface EditActiviteActionDto
    extends EditDto<ActiviteAction>,
        z.infer<typeof EditActiviteActionDtoSchema> {
    dateDeDebut: string
}
