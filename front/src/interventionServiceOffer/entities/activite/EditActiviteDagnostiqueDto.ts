import { EditDto } from '@/shared/api/entities/Dtos'
import { ActiviteDiagnostique } from './ActiviteDiagnostique'
import { InterventionDto } from '../intervention/InterventionDto'
import { DemandeInterventionDto } from '../demandeIntervention/DemandeInterventionDto'

export interface EditActiviteDiagnostiqueDto
    extends EditDto<ActiviteDiagnostique> {
    dateDeDebut: string

    evaluation: 'conforme' | 'reserve' | 'non conforme'
    interventionIdGeneree?: InterventionDto['id']
    demandeInterventionIdGeneree?: DemandeInterventionDto['id']
}
