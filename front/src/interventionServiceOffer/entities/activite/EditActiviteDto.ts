import { EditDto } from '@/shared/api/entities/Dtos'
import { Activite } from './Activite'

export interface EditActiviteDto extends EditDto<Activite> {
    dateDeDebut: string
}
