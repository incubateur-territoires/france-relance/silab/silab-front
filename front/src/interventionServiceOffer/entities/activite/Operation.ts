import { OperationDto } from './OperationDto'

export class Operation {
    id: string
    label: string

    public constructor(operationDto: OperationDto) {
        this.id = operationDto.id
        this.label = operationDto.label
    }
}
