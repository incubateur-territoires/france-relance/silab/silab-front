import { z } from 'zod'

export const OperationDtoSchema = z.object({
    id: z.string(),
    label: z.string(),
})

export type OperationDto = z.infer<typeof OperationDtoSchema>
