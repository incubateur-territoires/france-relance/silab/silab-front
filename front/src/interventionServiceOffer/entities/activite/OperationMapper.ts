import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { OperationDto, OperationDtoSchema } from './OperationDto'
import { Operation } from './Operation'

import { ZodError } from 'zod'

interface OperationMapper extends EntityMapper<Operation> {
    toDto: (partialEntity: Partial<Operation>) => OperationDto
    toDtoArray: (partialEntity: Partial<Operation>[]) => OperationDto[]
}

export const operationMapper: OperationMapper = {
    toDto: function (partialOperation: Partial<Operation>): OperationDto {
        try {
            return OperationDtoSchema.parse({
                id: partialOperation.id,
                label: partialOperation.label,
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'opération fournit ne possède pas tous les attributs requis.",
                    error
                )
            }
            throw error
        }
    },

    toDtoArray(partialOperations: Partial<Operation>[]): OperationDto[] {
        return partialOperations.map((partialOperation) =>
            this.toDto(partialOperation)
        )
    },
}
