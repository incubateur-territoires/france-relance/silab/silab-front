import { onBeforeUnmount, ref, Ref, watch } from 'vue'
import { Intervention } from '../intervention/Intervention'
import { InterventionServiceOffer } from '../interventionServiceOffer/InterventionServiceOffer'
import { ActiviteActionApi } from './ActiviteActionApi'
import { ActiviteAction } from './ActiviteAction'
import { VolatileSecuredData } from '@/shared/helpers/lifeCycle'
import { useMsal } from '@/plugins/msal/useMsal'
import { RequestAborter } from '@/shared/api/entities/RequestAborter'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { isUndefined } from '@/shared/helpers/types'
import { ActiviteFilters } from '../filters/ActiviteFilters'
import { SilabError } from '@/shared/errors/SilabError'

export function useInterventionActivitesAction(
    interventionId: Ref<Intervention['id'] | undefined>,
    serviceOfferId: Ref<InterventionServiceOffer['id']>
) {
    const msal = useMsal()
    const userStore = useUserStore(msal)
    const activiteActionApi = new ActiviteActionApi(msal)
    const activiteActionAbortController: RequestAborter = new RequestAborter()
    const activitesAction: Ref<VolatileSecuredData<ActiviteAction[]>> =
        ref(undefined)

    watch(
        [
            () => msal.accounts,
            () => interventionId.value,
            () => {
                return userStore.currentUserHasRoleForServiceOffer(
                    'INTERVENTION_CONSULTER_ACTIVITE',
                    serviceOfferId.value
                )
            },
            () => serviceOfferId.value,
        ],
        () => {
            loadInterventionActivitesAction()
        },
        { immediate: true }
    )

    async function loadInterventionActivitesAction() {
        activitesAction.value = undefined
        if (isUndefined(interventionId.value)) {
            return
        }

        if (
            !userStore.currentUserHasRoleForServiceOffer(
                //amélioration: piste->composable du type useCurrentUserRight(right, serviceOfferId)
                'INTERVENTION_CONSULTER_ACTIVITE',
                serviceOfferId.value
            )
        ) {
            activitesAction.value = {
                accessGranted: false,
                data: undefined,
            }
            return
        }
        activiteActionAbortController.abortRequestsAndRefreshAborter()
        return activiteActionApi
            .getAll(
                serviceOfferId.value,
                interventionId.value,
                new ActiviteFilters({ isDone: undefined }),
                activiteActionAbortController.getSignal()
            )
            .then((fetchedActivitesAction) => {
                activitesAction.value = {
                    accessGranted: true,
                    data: fetchedActivitesAction,
                }
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError(
                        `Erreur lors de la récupération des actions`,
                        error
                    )
                }
            })
    }

    onBeforeUnmount(() => {
        activiteActionAbortController.abortRequests()
    })

    return { activitesAction, reload: loadInterventionActivitesAction }
}
