import { onBeforeUnmount, ref, Ref, watch } from 'vue'
import { Intervention } from '../intervention/Intervention'
import { InterventionServiceOffer } from '../interventionServiceOffer/InterventionServiceOffer'
import { ActiviteDiagnostiqueApi } from './ActiviteDiagnostiqueApi'
import { ActiviteDiagnostique } from './ActiviteDiagnostique'
import { VolatileSecuredData } from '@/shared/helpers/lifeCycle'
import { useMsal } from '@/plugins/msal/useMsal'
import { RequestAborter } from '@/shared/api/entities/RequestAborter'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { isUndefined } from '@/shared/helpers/types'
import { ActiviteFilters } from '../filters/ActiviteFilters'
import { SilabError } from '@/shared/errors/SilabError'

export function useInterventionActivitesDiagnostique(
    interventionId: Ref<Intervention['id'] | undefined>,
    serviceOfferId: Ref<InterventionServiceOffer['id']>
) {
    const msal = useMsal()
    const userStore = useUserStore(msal)
    const activiteDiagnostiqueApi = new ActiviteDiagnostiqueApi(msal)
    const activiteDiagnostiqueAbortController: RequestAborter =
        new RequestAborter()
    const activitesDiagnostique: Ref<
        VolatileSecuredData<ActiviteDiagnostique[]>
    > = ref(undefined)

    watch(
        [
            () => msal.accounts,
            () => interventionId.value,
            () => {
                return userStore.currentUserHasRoleForServiceOffer(
                    'INTERVENTION_CONSULTER_ACTIVITE',
                    serviceOfferId.value
                )
            },
            () => serviceOfferId.value,
        ],
        () => {
            loadInterventionActivitesDiagnostique()
        },
        { immediate: true }
    )

    async function loadInterventionActivitesDiagnostique() {
        activitesDiagnostique.value = undefined
        if (isUndefined(interventionId.value)) {
            return
        }

        if (
            !userStore.currentUserHasRoleForServiceOffer(
                //amélioration: piste->composable du type useCurrentUserRight(right, serviceOfferId)
                'INTERVENTION_CONSULTER_ACTIVITE',
                serviceOfferId.value
            )
        ) {
            activitesDiagnostique.value = {
                accessGranted: false,
                data: undefined,
            }
            return
        }
        activiteDiagnostiqueAbortController.abortRequestsAndRefreshAborter()
        return activiteDiagnostiqueApi
            .getAll(
                serviceOfferId.value,
                interventionId.value,
                new ActiviteFilters({ isDone: undefined }),
                activiteDiagnostiqueAbortController.getSignal()
            )
            .then((fetchedActivitesDiagnostique) => {
                activitesDiagnostique.value = {
                    accessGranted: true,
                    data: fetchedActivitesDiagnostique,
                }
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError(
                        `Erreur lors de la récupération des diagnostiques`,
                        error
                    )
                }
            })
    }

    onBeforeUnmount(() => {
        activiteDiagnostiqueAbortController.abortRequests()
    })

    return {
        activitesDiagnostique,
        reload: loadInterventionActivitesDiagnostique,
    }
}
