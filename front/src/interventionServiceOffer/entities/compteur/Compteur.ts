import {
    ListableItem,
    SecondaryAction,
    getItemThumbnailUrl,
} from '@/shared/item/entities/ListableItem'
import { Equipement } from '../equipement/Equipement'
import { Mesure } from '../mesure/Mesure'
import { CompteurDto } from './CompteurDto'
import { RouteLocationRaw } from 'vue-router'

export class Compteur implements ListableItem {
    id: string
    code: string
    label: string
    unite: string
    derniereMesure?: Mesure
    equipement: Equipement
    type: CompteurDto['type']
    valeurMinimum?: number
    valeurMaximum?: number
    private itemRoute?: RouteLocationRaw
    private itemSecondaryAction?: SecondaryAction

    public constructor(compteurDto: CompteurDto) {
        this.id = compteurDto.id
        this.code = compteurDto.code
        this.label = compteurDto.label
        this.unite = compteurDto.unite
        this.derniereMesure = compteurDto.derniereMesure
            ? new Mesure(compteurDto.derniereMesure)
            : undefined
        this.equipement = new Equipement(compteurDto.equipement)
        this.type = compteurDto.type
        this.valeurMinimum = compteurDto.valeurMinimum
        this.valeurMaximum = compteurDto.valeurMaximum
    }
    public getItemId() {
        return this.id
    }
    public getItemTitle() {
        return this.label
    }
    public getItemDescription() {
        return (
            this.equipement.label +
            (this.equipement.code ? ` ${this.equipement.code}` : '')
        )
    }
    public getItemImageUrl() {
        return undefined
    }

    public getItemThumbnailUrl = getItemThumbnailUrl

    public getItemChips() {
        return []
    }

    public isRequiringAttention() {
        return false
    }

    public getItemTemporalData() {
        return ''
    }
    public getItemRoute() {
        return this.itemRoute
    }
    public setItemRoute(itemRoute: RouteLocationRaw | undefined) {
        this.itemRoute = itemRoute
        return this
    }
    public getItemSecondaryAction() {
        return this.itemSecondaryAction
    }

    public setItemSecondaryAction(secondaryAction: SecondaryAction) {
        this.itemSecondaryAction = secondaryAction
        return this
    }
}
