import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { Equipement } from '../equipement/Equipement'
import { CompteurDto } from './CompteurDto'
import { Compteur } from './Compteur'
import { CreateMesureDto } from '../mesure/CreateMesureDto'
import { MesureDto } from '../mesure/MesureDto'
import { Mesure } from '../mesure/Mesure'
import {
    extraireNomDeFichierDepuisHeaderContentDisposition,
    FichierNommé,
} from '@/shared/helpers/files'
import { isNull } from '@/shared/helpers/types'
import { SilabError } from '@/shared/errors/SilabError'

export class CompteurApi extends AzureTokenAccessApi {
    async getAllByEquipement(
        serviceOfferId: ServiceOffer['id'],
        equipementId: Equipement['id'],
        includeChildren?: boolean,
        abortSignal?: AbortSignal
    ): Promise<Compteur[]> {
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/equipements/${equipementId}/compteurs`,
            new Map([['includeChildren', !!includeChildren]]),
            abortSignal
        )

        const compteursApiResponse = (await rawResponse.json()) as CompteurDto[]

        return compteursApiResponse.map((compteurDto) => {
            return new Compteur(compteurDto)
        })
    }

    async getFichierHistoriqueDesRelèves(
        serviceOfferId: ServiceOffer['id'],
        compteurId: Compteur['id'],
        abortSignal?: AbortSignal
    ): Promise<FichierNommé> {
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/compteurs/${compteurId}/historique-des-releves`,
            new Map([
                ['timezone', Intl.DateTimeFormat().resolvedOptions().timeZone],
            ]), //Note: on pourrais préciser le type mime attendu avec un header accept, mais YAGNI nous dit que on fait po ça maintenant
            abortSignal
        )

        const headerContentDisposition = rawResponse.headers.get(
            'Content-Disposition'
        )

        if (isNull(headerContentDisposition)) {
            throw new SilabError(
                "Erreur technique : Le serveur ne renvoie pas le nom du fichier d'historique de relève."
            )
        }

        const nomDeFichier = extraireNomDeFichierDepuisHeaderContentDisposition(
            headerContentDisposition
        )

        return {
            blob: await rawResponse.blob(),
            nomDeFichier: nomDeFichier,
        }
    }

    async newMeasure(
        serviceOfferId: ServiceOffer['id'],
        compteurId: Compteur['id'],
        mesure: CreateMesureDto
    ) {
        const rawResponse = await this.postBackendData(
            `intervention-service-offers/${serviceOfferId}/compteurs/${compteurId}/mesures`,
            JSON.stringify(mesure)
        )
        const mesureDto = (await rawResponse.json()) as MesureDto

        return new Mesure(mesureDto)
    }
}
