import { EquipementDto } from '../equipement/EquipementDto'
import { Dto } from '@/shared/api/entities/Dtos'
import { Compteur } from './Compteur'
import { MesureDto } from '../mesure/MesureDto'

export interface CompteurDto extends Dto<Compteur> {
    id: string
    code: string
    label: string
    unite: string
    derniereMesure?: MesureDto
    equipement: EquipementDto
    type: 'float' | 'boolean' | 'integer'
    valeurMinimum?: number
    valeurMaximum?: number
}
