import { ConfigurationTableauDeMesuresDto } from './ConfigurationTableauDeMesuresDto'
import { Ligne } from './Ligne'
import { SousCellule } from './SousCellule'

export class ConfigurationTableauDeMesures {
    id: number
    interventionServiceOffer: string
    titre: string
    lignes: Array<Ligne>
    sousCellules: Array<SousCellule>
    '@id': string

    constructor(
        configurationTableauDeMesuresDto: ConfigurationTableauDeMesuresDto
    ) {
        const configurationTableauDeMesuresDtoClone = structuredClone(
            configurationTableauDeMesuresDto
        )
        this.id = configurationTableauDeMesuresDtoClone.id
        this.interventionServiceOffer =
            configurationTableauDeMesuresDtoClone.interventionServiceOffer
        this.titre = configurationTableauDeMesuresDtoClone.titre
        this.lignes = configurationTableauDeMesuresDtoClone.lignes.map(
            (ligneDto) => new Ligne(ligneDto)
        )
        this.sousCellules =
            configurationTableauDeMesuresDtoClone.sousCellules.map(
                (sousCelluleDto) => new SousCellule(sousCelluleDto)
            )
        this['@id'] =
            `/api/configuration-tableau-de-mesuress/${configurationTableauDeMesuresDtoClone.id}`
    }
}
