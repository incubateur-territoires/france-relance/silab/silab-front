import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { ConfigurationTableauDeMesuresDtoSchema } from './ConfigurationTableauDeMesuresDto'
import { ConfigurationTableauDeMesures } from './ConfigurationTableauDeMesures'

export class ConfigurationTableauDeMesuresApi extends AzureTokenAccessApi {
    async getAll(
        serviceOfferId: ServiceOffer['id'],
        abortSignal?: AbortSignal
    ): Promise<ConfigurationTableauDeMesures[]> {
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/configuration-tableau-de-mesures`,
            undefined,
            abortSignal
        )

        const configurationTableauDeMesuresDto =
            ConfigurationTableauDeMesuresDtoSchema.array().parse(
                await rawResponse.json()
            )

        return configurationTableauDeMesuresDto.map(
            (configurationTableauDeMesuresDto) =>
                new ConfigurationTableauDeMesures(
                    configurationTableauDeMesuresDto
                )
        )
    }
}
