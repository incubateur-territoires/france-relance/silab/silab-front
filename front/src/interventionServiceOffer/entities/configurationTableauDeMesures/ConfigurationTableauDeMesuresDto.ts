import { z } from 'zod'
import { LigneDtoSchema } from './LigneDto'
import { SousCelluleDtoSchema } from './SousCelluleDto'

export const ConfigurationTableauDeMesuresDtoSchema = z.object({
    id: z.number(),
    interventionServiceOffer: z.string(),
    titre: z.string(),
    lignes: LigneDtoSchema.array(),
    sousCellules: SousCelluleDtoSchema.array(),
})

export type ConfigurationTableauDeMesuresDto = z.infer<
    typeof ConfigurationTableauDeMesuresDtoSchema
>
