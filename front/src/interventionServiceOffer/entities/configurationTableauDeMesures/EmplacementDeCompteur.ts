import { EmplacementDeCompteurDto } from './EmplacementDeCompteurDto'
import { SousCellule } from './SousCellule'

export class EmplacementDeCompteur {
    id: number
    sousCellule: SousCellule
    idCompteur: string

    constructor(emplacementDeCompteurDto: EmplacementDeCompteurDto) {
        const emplacementDeCompteurDtoClone = structuredClone(
            emplacementDeCompteurDto
        )
        this.id = emplacementDeCompteurDtoClone.id
        this.sousCellule = new SousCellule(
            emplacementDeCompteurDtoClone.sousCellule
        )
        this.idCompteur = emplacementDeCompteurDtoClone.idCompteur
    }
}
