import { z } from 'zod'
import { SousCelluleDtoSchema } from './SousCelluleDto'

export const EmplacementDeCompteurDtoSchema = z.object({
    id: z.number(),
    sousCellule: SousCelluleDtoSchema,
    idCompteur: z.string(),
})

export type EmplacementDeCompteurDto = z.infer<
    typeof EmplacementDeCompteurDtoSchema
>
