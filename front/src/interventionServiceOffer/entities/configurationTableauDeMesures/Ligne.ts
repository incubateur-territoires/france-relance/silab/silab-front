import { EmplacementDeCompteur } from './EmplacementDeCompteur'
import { LigneDto } from './LigneDto'

export class Ligne {
    id: number
    titre: string
    ordre: number
    configurationTableauDeMesures: string
    emplacementsDeCompteurs: Array<EmplacementDeCompteur>

    constructor(ligneDto: LigneDto) {
        const ligneDtoClone = structuredClone(ligneDto)
        this.id = ligneDtoClone.id
        this.titre = ligneDtoClone.titre
        this.ordre = ligneDtoClone.ordre
        this.configurationTableauDeMesures =
            ligneDtoClone.configurationTableauDeMesures
        this.emplacementsDeCompteurs =
            ligneDtoClone.emplacementsDeCompteurs.map(
                (emplacementDeCompteurDto) =>
                    new EmplacementDeCompteur(emplacementDeCompteurDto)
            )
    }
}
