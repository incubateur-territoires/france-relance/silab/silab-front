import { z } from 'zod'
import { EmplacementDeCompteurDtoSchema } from './EmplacementDeCompteurDto'

export const LigneDtoSchema = z.object({
    id: z.number(),
    titre: z.string(),
    ordre: z.number(),
    configurationTableauDeMesures: z.string(),
    emplacementsDeCompteurs: EmplacementDeCompteurDtoSchema.array(),
})

export type LigneDto = z.infer<typeof LigneDtoSchema>
