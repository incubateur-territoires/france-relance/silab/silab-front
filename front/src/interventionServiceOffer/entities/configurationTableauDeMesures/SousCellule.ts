import { SousCelluleDto } from './SousCelluleDto'

export class SousCellule {
    id: number
    titre: string
    ordre: number

    constructor(sousCelluleDto: SousCelluleDto) {
        const sousCelluleDtoClone = structuredClone(sousCelluleDto)
        this.id = sousCelluleDtoClone.id
        this.titre = sousCelluleDtoClone.titre
        this.ordre = sousCelluleDtoClone.ordre
    }
}
