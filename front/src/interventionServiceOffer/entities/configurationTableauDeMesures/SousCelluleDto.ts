import { z } from 'zod'

export const SousCelluleDtoSchema = z.object({
    id: z.number(),
    titre: z.string(),
    ordre: z.number(),
})

export type SousCelluleDto = z.infer<typeof SousCelluleDtoSchema>
