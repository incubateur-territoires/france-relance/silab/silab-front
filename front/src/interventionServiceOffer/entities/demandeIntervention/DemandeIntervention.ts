import {
    PrimaryAction,
    SecondaryAction,
    getItemThumbnailUrl,
} from '@/shared/item/entities/ListableItem'
import { Equipement } from '../equipement/Equipement'
import { DemandeInterventionDto } from './DemandeInterventionDto'
import { getAnteriorityString } from '@/shared/helpers/dates'
import { DemandeInterventionStatus } from './status/DemandeInterventionStatus'
import { RouteLocationRaw } from 'vue-router'
import { GeolocalisableItem } from '@/shared/item/entities/GeolocalisableItem'
import { isDefined } from '@/shared/helpers/types'
import { SilabImage } from '@/shared/document/entities/SilabImage'
import { GmaoActeur } from '@/shared/gmaoActeur/GmaoActeur'
import { InterventionCycleEntity } from '../interventionCycleEntity/InterventionCycleEntity'
import { Document } from '@/shared/document/entities/Document'
import { LatLngLiteral } from 'leaflet'
import { Clonable } from '@/shared/helpers/lifeCycle'
import { DemandeInterventionMapper } from './DemandeInterventionMapper'
import { toRaw } from 'vue'
import { InterventionServiceOfferItem } from '../item/InterventionServiceOfferItem'
import { ItemLists } from '@/interventionServiceOffer/composables/equipement/useItemLists'
import { Equatable } from '@/shared/helpers/objects'

export class DemandeIntervention
    implements
        InterventionServiceOfferItem,
        GeolocalisableItem,
        InterventionCycleEntity,
        Clonable<DemandeIntervention>,
        Equatable<DemandeIntervention>
{
    id: string
    code: string
    title: string
    currentStatus: DemandeInterventionStatus
    images: SilabImage[]
    createdBy: GmaoActeur
    createdAt: Date
    updatedAt: Date
    description?: string
    relatedEquipement?: Equipement
    address?: string
    coordonnees?: LatLngLiteral
    gmaoObjectViewUrl?: string
    documentsJoints: Document[]
    dateDeDebutSouhaitee?: Date
    dateDeFinSouhaitee?: Date
    raisonSiNonSupprimable?: string
    private itemRoute?: RouteLocationRaw
    private itemSecondaryAction?: SecondaryAction
    private itemPrimaryAction?: PrimaryAction

    public constructor(demandeInterventionDto: DemandeInterventionDto) {
        this.id = demandeInterventionDto.id
        this.code = demandeInterventionDto.code
        this.title = demandeInterventionDto.title
        this.currentStatus = new DemandeInterventionStatus(
            demandeInterventionDto.currentStatus
        )
        this.description = demandeInterventionDto.description
        this.images = SilabImage.fromArray(demandeInterventionDto.images)
        this.relatedEquipement = isDefined(
            demandeInterventionDto.relatedEquipement
        )
            ? new Equipement(demandeInterventionDto.relatedEquipement)
            : undefined
        this.address = demandeInterventionDto.address
        this.coordonnees = demandeInterventionDto.coordonnees
        this.createdBy = new GmaoActeur(demandeInterventionDto.createdBy)
        this.createdAt = new Date(demandeInterventionDto.createdAt)
        this.updatedAt = new Date(demandeInterventionDto.createdAt)
        this.gmaoObjectViewUrl = demandeInterventionDto.gmaoObjectViewUrl
        this.documentsJoints = demandeInterventionDto.documentsJoints.map(
            (documentDto) => new Document(documentDto)
        )
        this.dateDeDebutSouhaitee = demandeInterventionDto.dateDeDebutSouhaitee
            ? new Date(demandeInterventionDto.dateDeDebutSouhaitee)
            : undefined
        this.dateDeFinSouhaitee = demandeInterventionDto.dateDeFinSouhaitee
            ? new Date(demandeInterventionDto.dateDeFinSouhaitee)
            : undefined
        this.raisonSiNonSupprimable =
            demandeInterventionDto.raisonSiNonSupprimable
    }

    public getItemId() {
        return this.id
    }

    public getItemType(): keyof ItemLists {
        return 'demandes'
    }

    public getItemTitle() {
        return `${this.code} ${this.title}`
    }

    public getItemDescription() {
        const itemDescriptionElements = []

        if (isDefined(this.relatedEquipement?.label)) {
            itemDescriptionElements.push(
                this.relatedEquipement.label.toLocaleUpperCase()
            )
        }

        if (isDefined(this.description) && this.description.length > 0) {
            itemDescriptionElements.push(this.description)
        }

        return itemDescriptionElements.join(' - ')
    }

    public getItemImageUrl() {
        return this.images.at(0)?.url
    }

    public getItemThumbnailUrl = getItemThumbnailUrl

    public getItemChips() {
        return []
    }

    public isRequiringAttention() {
        if (this.currentStatus.code === 'awaiting_information') {
            return true
        }

        return false
    }

    public getItemTemporalData() {
        return getAnteriorityString(this.createdAt)
    }

    public getItem(): this {
        return this
    }

    public setItemRoute(route: RouteLocationRaw | undefined) {
        this.itemRoute = route
        return this
    }

    public primaryAction() {
        this.itemPrimaryAction?.()
    }

    public setItemPrimaryAction(primaryAction: PrimaryAction) {
        this.itemPrimaryAction = primaryAction
        return this
    }

    public getItemRoute() {
        return this.itemRoute
    }

    public getCoordinates() {
        return this.coordonnees ?? this.relatedEquipement?.getCoordinates()
    }
    public hasLocation(): boolean {
        return isDefined(this.getCoordinates())
    }

    public isEditable() {
        return !this.readOnly()
    }

    public isDeletable() {
        return !this.readOnly()
    }

    public readOnly() {
        return !['awaiting_validation', 'awaiting_information'].includes(
            this.currentStatus.code
        )
    }

    public static fromArray(
        demandeInterventionDtos: DemandeInterventionDto[]
    ): DemandeIntervention[] {
        return demandeInterventionDtos.map(
            (demandeInterventionDto) =>
                new DemandeIntervention(demandeInterventionDto)
        )
    }

    public getItemSecondaryAction() {
        return this.itemSecondaryAction
    }

    public setItemSecondaryAction(secondaryAction: SecondaryAction) {
        this.itemSecondaryAction = secondaryAction
        return this
    }

    clone(): DemandeIntervention {
        return new DemandeIntervention(
            window.structuredClone(DemandeInterventionMapper.toDto(toRaw(this))) //Attention: le toRaw est important, les structuredClone ne fonctionne pas avec des valeurs issues de Ref.value
        )
    }

    public equals(other: DemandeIntervention): boolean {
        const currentDemandeInterventionDto =
            DemandeInterventionMapper.toDto(this)
        const otherDemandeInterventionDto =
            DemandeInterventionMapper.toDto(other)

        return (
            JSON.stringify(currentDemandeInterventionDto) ===
            JSON.stringify(otherDemandeInterventionDto)
        )
    }
}
