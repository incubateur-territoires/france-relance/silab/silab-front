import { DemandeInterventionHistorisedStatusDto } from './status/DemandeInterventionHistorisedStatusDto'
import { DemandeInterventionHistorisedStatus } from './status/DemandeInterventionHistorisedStatus'
import {
    DemandeInterventionStatusDto,
    DemandeInterventionStatusDtoSchema,
} from '@/interventionServiceOffer/entities/demandeIntervention/status/DemandeInterventionStatusDto'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { CreateDemandeInterventionDto } from './CreateDemandeInterventionDto'
import { DemandeIntervention } from './DemandeIntervention'
import { DemandeInterventionDtoSchema } from './DemandeInterventionDto'
import { DemandeInterventionFilters } from './filters/DemandeInterventionFilters'
import { EditDemandeInterventionDto } from './EditDemandeInterventionDto'
import { SilabImage } from '@/shared/document/entities/SilabImage'
import { DemandeInterventionStatus } from './status/DemandeInterventionStatus'
import { SilabImageDtoSchema } from '@/shared/document/entities/SilabImageDto'
import { Equipement } from '@/interventionServiceOffer/entities/equipement/Equipement'
import { DemandeInterventionCoordinates } from './DemandeInterventionCoordinates'
import { DemandeInterventionCoordinatesDtoSchema } from './DemandeInterventionCoordinatesDto'
import { ResultPage } from '@/shared/helpers/backend'

export class DemandeInterventionApi extends AzureTokenAccessApi {
    async create(
        serviceOfferId: ServiceOffer['id'],
        newDemandeIntervention: CreateDemandeInterventionDto
    ) {
        const rawResponse = await this.postBackendData(
            `intervention-service-offers/${serviceOfferId}/demandes-interventions`,
            JSON.stringify(newDemandeIntervention)
        )

        const demandeInterventionDto = DemandeInterventionDtoSchema.parse(
            await rawResponse.json()
        )

        return new DemandeIntervention(demandeInterventionDto)
    }

    async update(
        serviceOfferId: ServiceOffer['id'],
        demandeInterventionId: DemandeIntervention['id'],
        editedDemandeInterventionDto: EditDemandeInterventionDto
    ) {
        const rawResponse = await this.putBackendData(
            `intervention-service-offers/${serviceOfferId}/demandes-interventions/${demandeInterventionId}`,
            JSON.stringify(editedDemandeInterventionDto)
        )

        const demandeInterventionApiResponse =
            DemandeInterventionDtoSchema.parse(await rawResponse.json())

        return new DemandeIntervention(demandeInterventionApiResponse)
    }

    async delete(
        serviceOfferId: ServiceOffer['id'],
        demandeInterventionId: DemandeIntervention['id']
    ) {
        await this.deleteBackendData(
            `intervention-service-offers/${serviceOfferId}/demandes-interventions/${demandeInterventionId}`
        )
    }

    async getAll(
        serviceOfferId: ServiceOffer['id'],
        filter: DemandeInterventionFilters,
        // orderBy: InterventionOrderBy,
        page: number,
        abortSignal?: AbortSignal
    ): Promise<ResultPage<DemandeIntervention>> {
        const queryParameters = filter
            .toQueryParameters()
            .set('page', page.toString())

        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/demandes-interventions`,
            queryParameters,
            abortSignal
        )

        const demandeInterventionApiResponse =
            DemandeInterventionDtoSchema.array().parse(await rawResponse.json())

        return {
            data: demandeInterventionApiResponse.map(
                (demandeInterventionDto) => {
                    return new DemandeIntervention(demandeInterventionDto)
                }
            ),
            maxResults: Number(rawResponse.headers.get('X-Total-Count')),
        }
    }

    async getAllCoordinates(
        serviceOfferId: ServiceOffer['id'],
        filters: DemandeInterventionFilters,
        abortSignal?: AbortSignal
    ): Promise<DemandeInterventionCoordinates[]> {
        const queryParameters = filters.toQueryParameters()

        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/demandes-interventions-coordinates`,
            queryParameters,
            abortSignal
        )

        const demandeInterventionsCoordinatesDto =
            DemandeInterventionCoordinatesDtoSchema.array().parse(
                await rawResponse.json()
            )

        return demandeInterventionsCoordinatesDto.map(
            (demandeInterventionCoordinatesDto) => {
                return new DemandeInterventionCoordinates(
                    demandeInterventionCoordinatesDto
                )
            }
        )
    }

    async get(
        serviceOfferId: ServiceOffer['id'],
        demandeInterventionId: DemandeIntervention['id'],
        abortSignal?: AbortSignal
    ) {
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/demandes-interventions/${demandeInterventionId}`,
            undefined,
            abortSignal
        )

        const demandeInterventionApiResponse =
            DemandeInterventionDtoSchema.parse(await rawResponse.json())

        return new DemandeIntervention(demandeInterventionApiResponse)
    }

    async transitionTo(
        serviceOfferId: ServiceOffer['id'],
        demandeInterventionId: DemandeIntervention['id'],
        newStatusCode: DemandeInterventionStatusDto['code']
    ) {
        const rawResponse = await this.postBackendData(
            `intervention-service-offers/${serviceOfferId}/demandes-interventions/${demandeInterventionId}/statuses`,
            JSON.stringify(<Pick<DemandeInterventionStatusDto, 'code'>>{
                code: newStatusCode,
            })
        )
        const demandeInterventionStatusDto =
            DemandeInterventionStatusDtoSchema.parse(await rawResponse.json())

        return new DemandeInterventionStatus(demandeInterventionStatusDto)
    }

    async getStatusHistory(
        serviceOfferId: ServiceOffer['id'],
        demandeInterventionId: DemandeIntervention['id'],
        abortSignal?: AbortSignal
    ) {
        const statusHistoryResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/demandes-interventions/${demandeInterventionId}/statusHistory`,
            undefined,
            abortSignal
        )

        const statusHistoryDto =
            (await statusHistoryResponse.json()) as DemandeInterventionHistorisedStatusDto[]

        // On veux instancier les interventions avec leur classes finales/filles, on utilise pour cela le factory sur chaque Dto reçu
        return statusHistoryDto.map(
            (demandeInterventionHistorisedStatusDto) => {
                return new DemandeInterventionHistorisedStatus(
                    demandeInterventionHistorisedStatusDto
                )
            }
        )
    }

    async addImage(
        serviceOfferId: ServiceOffer['id'],
        demandeInterventionId: DemandeIntervention['id'],
        imageBase64: string
    ): Promise<SilabImage> {
        const rawResponse = await this.postBackendData(
            `intervention-service-offers/${serviceOfferId}/demandes-interventions/${demandeInterventionId}/images`,
            JSON.stringify({
                base64: imageBase64,
            })
        )

        const addedImageDto = SilabImageDtoSchema.parse(
            await rawResponse.json()
        )

        return new SilabImage(addedImageDto)
    }

    async addImages(
        serviceOfferId: ServiceOffer['id'],
        demandeInterventionId: DemandeIntervention['id'],
        imagesB64: string[]
    ): Promise<SilabImage[]> {
        const addedImages: SilabImage[] = []
        for (const imageB64 of imagesB64) {
            addedImages.push(
                await this.addImage(
                    serviceOfferId,
                    demandeInterventionId,
                    imageB64
                )
            )
        }

        return addedImages
    }

    async deleteImage(
        serviceOfferId: ServiceOffer['id'],
        demandeInterventionId: DemandeIntervention['id'],
        id: SilabImage['id']
    ) {
        await this.deleteBackendData(
            `intervention-service-offers/${serviceOfferId}/demandes-interventions/${demandeInterventionId}/images/${id}`
        )
    }

    async deleteImages(
        serviceOfferId: ServiceOffer['id'],
        demandeInterventionId: DemandeIntervention['id'],
        imagesId: SilabImage['id'][]
    ) {
        for (const imageId of imagesId) {
            await this.deleteImage(
                serviceOfferId,
                demandeInterventionId,
                imageId
            )
        }
    }

    async getAllByEquipement(
        serviceOfferId: ServiceOffer['id'],
        equipementId: Equipement['id'],
        filter?: DemandeInterventionFilters,
        abortSignal?: AbortSignal
    ): Promise<DemandeIntervention[]> {
        const queryParameters = filter?.toQueryParameters()

        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/equipements/${equipementId}/demandes-interventions`,
            queryParameters,
            abortSignal
        )

        const demandeInterventionApiResponse =
            DemandeInterventionDtoSchema.array().parse(await rawResponse.json())

        return demandeInterventionApiResponse.map((demandeInterventionDto) => {
            return new DemandeIntervention(demandeInterventionDto)
        })
    }
}
