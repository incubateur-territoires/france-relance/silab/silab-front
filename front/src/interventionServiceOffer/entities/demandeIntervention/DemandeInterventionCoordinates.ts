import {
    PrimaryAction,
    SecondaryAction,
    getItemThumbnailUrl,
} from '@/shared/item/entities/ListableItem'
import { GeolocalisableItem } from '@/shared/item/entities/GeolocalisableItem'

import { LatLngLiteral } from 'leaflet'
import { RouteLocationRaw } from 'vue-router'
import { DemandeInterventionCoordinatesDto } from './DemandeInterventionCoordinatesDto'
import { InterventionServiceOfferItem } from '../item/InterventionServiceOfferItem'
import { ItemLists } from '@/interventionServiceOffer/composables/equipement/useItemLists'

export class DemandeInterventionCoordinates
    implements InterventionServiceOfferItem, GeolocalisableItem
{
    id: string
    label: string
    coordinates: LatLngLiteral
    private itemRoute?: RouteLocationRaw
    private itemSecondaryAction?: SecondaryAction

    public constructor(
        DemandeInterventionCoordinatesDto: DemandeInterventionCoordinatesDto
    ) {
        this.id = DemandeInterventionCoordinatesDto.id
        this.label = DemandeInterventionCoordinatesDto.label
        this.coordinates = {
            lat: DemandeInterventionCoordinatesDto.latitude,
            lng: DemandeInterventionCoordinatesDto.longitude,
        }
    }
    primaryAction?: PrimaryAction | undefined
    setItemPrimaryAction?: ((primaryAction: PrimaryAction) => this) | undefined

    public hasLocation(): boolean {
        return true
    }

    public hasImage(): boolean {
        return false
    }

    public getItemId() {
        return this.id
    }
    public getItemType(): keyof ItemLists {
        return 'demandes'
    }
    public getItemTitle() {
        return `${this.label}`
    }

    public getItemDescription() {
        return ''
    }
    public getItemImageUrl() {
        return undefined
    }

    public getItemThumbnailUrl = getItemThumbnailUrl

    public getItemChips() {
        return []
    }
    public isRequiringAttention() {
        return false
    }

    public getItemTemporalData() {
        return ''
    }
    public getItemRoute() {
        return this.itemRoute
    }
    public setItemRoute(itemRoute: RouteLocationRaw | undefined) {
        this.itemRoute = itemRoute
        return this
    }
    public getCoordinates() {
        return this.coordinates
    }
    public getItemSecondaryAction() {
        return this.itemSecondaryAction
    }

    public setItemSecondaryAction(secondaryAction: SecondaryAction) {
        this.itemSecondaryAction = secondaryAction
        return this
    }
}
