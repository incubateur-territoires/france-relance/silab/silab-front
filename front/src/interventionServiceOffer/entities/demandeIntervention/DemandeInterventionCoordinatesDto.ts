import { z } from 'zod'
export const DemandeInterventionCoordinatesDtoSchema = z.object({
    id: z.string(),
    label: z.string(),
    latitude: z.number(),
    longitude: z.number(),
})

export type DemandeInterventionCoordinatesDto = z.infer<
    typeof DemandeInterventionCoordinatesDtoSchema
>
