import { EquipementDto, EquipementDtoSchema } from '../equipement/EquipementDto'
import {
    DemandeInterventionStatusDto,
    DemandeInterventionStatusDtoSchema,
} from './status/DemandeInterventionStatusDto'
import {
    GmaoActeurDto,
    GmaoActeurDtoSchema,
} from '@/shared/gmaoActeur/GmaoActeurDto'
import {
    DocumentDto,
    DocumentDtoSchema,
} from '@/shared/document/entities/DocumentDto'
import {
    SilabImageDto,
    SilabImageDtoSchema,
} from '@/shared/document/entities/SilabImageDto'
import { z } from 'zod'
import {
    CoordinatesDto,
    CoordinatesDtoSchema,
} from '@/shared/geocoding/entities/coordinate/CoordinatesDto'

export const DemandeInterventionDtoSchema = z.object({
    id: z.string(),
    code: z.string(),
    title: z.string(),
    coordonnees: CoordinatesDtoSchema.optional(),
    currentStatus: DemandeInterventionStatusDtoSchema,
    description: z.string().optional(),
    images: SilabImageDtoSchema.array(),
    relatedEquipement: EquipementDtoSchema.optional(),
    address: z.string().optional(),
    createdBy: GmaoActeurDtoSchema,
    createdAt: z.string(),
    updatedAt: z.string(),
    gmaoObjectViewUrl: z.string().optional(),
    documentsJoints: DocumentDtoSchema.array(),
    dateDeDebutSouhaitee: z.string().optional(),
    dateDeFinSouhaitee: z.string().optional(),
    raisonSiNonSupprimable: z.string().optional(),
})

// export type DemandeInterventionDto = z.infer<typeof DemandeInterventionDtoSchema>
// amelioration: abandonner les interfaces natives
export interface DemandeInterventionDto
    extends z.infer<typeof DemandeInterventionDtoSchema> {
    id: string
    code: string
    title: string
    currentStatus: DemandeInterventionStatusDto
    description?: string
    images: SilabImageDto[]
    relatedEquipement?: EquipementDto
    address?: string
    coordonnees?: CoordinatesDto
    createdBy: GmaoActeurDto
    createdAt: string
    updatedAt: string
    gmaoObjectViewUrl?: string
    documentsJoints: DocumentDto[]
    dateDeDebutSouhaitee?: string
    dateDeFinSouhaitee?: string
    raisonSiNonSupprimable?: string
}
