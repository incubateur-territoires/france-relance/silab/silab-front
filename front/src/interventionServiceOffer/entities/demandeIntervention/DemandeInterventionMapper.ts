import { isDefined, isUndefined } from '@/shared/helpers/types'
import { DemandeIntervention } from './DemandeIntervention'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { EditDemandeInterventionDto } from './EditDemandeInterventionDto'
import { CreateDemandeInterventionDto } from './CreateDemandeInterventionDto'
import {
    DemandeInterventionDto,
    DemandeInterventionDtoSchema,
} from './DemandeInterventionDto'
import { Dto } from '@/shared/api/entities/Dtos'
import { silabImageMapper } from '@/shared/document/entities/SilabImageMapper'
import { equipementMapper } from '../equipement/EquipementMapper'
import { gmaoActeurMapper } from '@/shared/gmaoActeur/GmaoActeurMapper'
import { documentMapper } from '@/shared/document/entities/DocumentMapper'
import { ZodError } from 'zod'

interface DemandeInterventionMapper {
    toDto: (
        partialDemandeIntervention: Partial<DemandeIntervention>
    ) => DemandeInterventionDto
    toEditDto: (
        partialEntity: Partial<DemandeIntervention>
    ) => EditDemandeInterventionDto
    toCreateDto: (
        partialEntity: Partial<DemandeIntervention>
    ) => CreateDemandeInterventionDto
    toDtoArray: (
        partialDemandeIntervention: Partial<DemandeIntervention>[]
    ) => DemandeInterventionDto[]
    fromDtoArray: (
        demandeInterventionDtos:
            | Dto<DemandeIntervention>[]
            | Dto<DemandeIntervention[]>
    ) => DemandeIntervention[]
    fromUnsafeDtoArray: (
        demandeInterventionDtos: unknown
    ) => DemandeIntervention[]
}

export const DemandeInterventionMapper: DemandeInterventionMapper = {
    toDto(
        partialDemandeIntervention: Partial<DemandeIntervention>
    ): DemandeInterventionDto {
        try {
            return DemandeInterventionDtoSchema.parse({
                id: partialDemandeIntervention.id,
                code: partialDemandeIntervention.code,
                title: partialDemandeIntervention.title,
                currentStatus:
                    partialDemandeIntervention.currentStatus?.toDto(),
                description: partialDemandeIntervention.description,
                images: isDefined(partialDemandeIntervention.images)
                    ? silabImageMapper.toDtoArray(
                          partialDemandeIntervention.images
                      )
                    : undefined,
                coordonnees: partialDemandeIntervention.coordonnees,
                relatedEquipement: isUndefined(
                    partialDemandeIntervention.relatedEquipement
                )
                    ? undefined
                    : equipementMapper.toDto(
                          partialDemandeIntervention.relatedEquipement
                      ),
                address: partialDemandeIntervention.address,
                createdBy: isDefined(partialDemandeIntervention.createdBy)
                    ? gmaoActeurMapper.toDto(
                          partialDemandeIntervention.createdBy
                      )
                    : undefined,
                createdAt: isDefined(partialDemandeIntervention.createdAt)
                    ? partialDemandeIntervention.createdAt.toISOString()
                    : undefined,
                updatedAt: isDefined(partialDemandeIntervention.updatedAt)
                    ? partialDemandeIntervention.updatedAt.toISOString()
                    : undefined,
                gmaoObjectViewUrl: partialDemandeIntervention.gmaoObjectViewUrl,
                documentsJoints: isDefined(
                    partialDemandeIntervention.documentsJoints
                )
                    ? partialDemandeIntervention.documentsJoints.map(
                          documentMapper.toDto
                      )
                    : undefined,
                dateDeDebutSouhaitee:
                    partialDemandeIntervention.dateDeDebutSouhaitee?.toISOString(),
                dateDeFinSouhaitee:
                    partialDemandeIntervention.dateDeFinSouhaitee?.toISOString(),
                raisonSiNonSupprimable:
                    partialDemandeIntervention.raisonSiNonSupprimable,
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "La demande d'intervention fournit ne possède pas tous les attributs requis.",
                    error
                )
            }
            throw error
        }
    },
    toEditDto(partialDemandeIntervention) {
        if (partialDemandeIntervention.title === undefined) {
            throw new IncompleteEntity(
                "La demande d'intervention fournie ne possède pas tous les attributs requis."
            )
        }

        return {
            title: partialDemandeIntervention.title,
            description: partialDemandeIntervention.description,
            relatedEquipement: isDefined(
                partialDemandeIntervention.relatedEquipement
            )
                ? {
                      id: partialDemandeIntervention.relatedEquipement.id,
                  }
                : undefined,
            coordonnees: partialDemandeIntervention.coordonnees,
            dateDeDebutSouhaitee:
                partialDemandeIntervention.dateDeDebutSouhaitee?.toISOString(),
            dateDeFinSouhaitee:
                partialDemandeIntervention.dateDeFinSouhaitee?.toISOString(),
        }
    },
    toCreateDto(partialDemandeIntervention) {
        return this.toEditDto(partialDemandeIntervention)
    },

    toDtoArray(
        partialDemandeInterventions: Partial<DemandeIntervention>[]
    ): DemandeInterventionDto[] {
        return partialDemandeInterventions.map((partialDemandeIntervention) =>
            this.toDto(partialDemandeIntervention)
        )
    },

    fromDtoArray: function (
        demandeInterventionDtos:
            | Dto<DemandeIntervention>[]
            | Dto<DemandeIntervention[]>
    ): DemandeIntervention[] {
        return DemandeIntervention.fromArray(
            demandeInterventionDtos as DemandeInterventionDto[]
        ) // amelioration : peut-on faire + propre ?
    },

    fromUnsafeDtoArray: function (
        demandeInterventionDtos: unknown
    ): DemandeIntervention[] {
        return DemandeIntervention.fromArray(
            DemandeInterventionDtoSchema.array().parse(demandeInterventionDtos)
        ) // amelioration : peut-on faire + propre ?
    },
}
