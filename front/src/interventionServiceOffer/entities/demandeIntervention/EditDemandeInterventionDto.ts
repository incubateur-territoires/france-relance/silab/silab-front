import { EditDto } from '@/shared/api/entities/Dtos'
import { DemandeIntervention } from './DemandeIntervention'
import { EquipementDto } from '../equipement/EquipementDto'
import { CoordinatesDto } from '@/shared/geocoding/entities/coordinate/CoordinatesDto'

export interface EditDemandeInterventionDto
    extends EditDto<DemandeIntervention> {
    title: string
    description?: string
    coordonnees?: CoordinatesDto
    relatedEquipement?: Pick<EquipementDto, 'id'>
    dateDeDebutSouhaitee?: string
    dateDeFinSouhaitee?: string
}
