import { z } from 'zod'
import {
    EquipementDto,
    EquipementDtoSchema,
} from '../../equipement/EquipementDto'
import { ObjectFilters } from '@/shared/filters/entities/ObjectFilters'

export const DemandeInterventionFiltersDtoSchema = z.object({
    statuses: z.string().array(),
    'relatedEquipement.id': EquipementDtoSchema.shape.id.array(),
})

export type DemandeInterventionFiltersDto = z.infer<
    typeof DemandeInterventionFiltersDtoSchema
>

export class DemandeInterventionFilters extends ObjectFilters<{
    statuses: string[]
    'relatedEquipement.id': Array<EquipementDto['id']>
}> {
    public getDefaultFilters() {
        return {
            statuses: [
                'awaiting_validation',
                'awaiting_information',
                'accepted',
            ],
            'relatedEquipement.id': [],
        }
    }
}
