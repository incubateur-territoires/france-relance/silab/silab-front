import {
    DemandeInterventionFilters,
    DemandeInterventionFiltersDto,
} from './DemandeInterventionFilters'

export const demandeInterventionFiltersMapper = {
    toDto: function (
        demandeInterventionFilters: DemandeInterventionFilters
    ): DemandeInterventionFiltersDto {
        return {
            statuses: demandeInterventionFilters.filters.statuses,
            'relatedEquipement.id':
                demandeInterventionFilters.filters['relatedEquipement.id'],
        }
    },
    fromDto: (
        demandeInterventionFiltersDto: DemandeInterventionFiltersDto
    ): DemandeInterventionFilters => {
        return new DemandeInterventionFilters({
            statuses: demandeInterventionFiltersDto.statuses,
            'relatedEquipement.id':
                demandeInterventionFiltersDto['relatedEquipement.id'],
        })
    },
}
