import { DemandeInterventionHistorisedStatusDto } from './DemandeInterventionHistorisedStatusDto'
import { ObjectHistorisedStatus } from '@/shared/objectStatus/entities/ObjectHistorisedStatus'

export class DemandeInterventionHistorisedStatus extends ObjectHistorisedStatus<
    DemandeInterventionHistorisedStatusDto['code']
> {
    public constructor(
        demandeInterventionHistorisedStatusDto: DemandeInterventionHistorisedStatusDto
    ) {
        super(demandeInterventionHistorisedStatusDto)
    }
}
