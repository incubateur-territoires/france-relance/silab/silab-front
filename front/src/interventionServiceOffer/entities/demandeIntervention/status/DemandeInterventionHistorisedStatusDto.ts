import { ObjectHistorisedStatusDto } from '@/shared/objectStatus/entities/ObjectHistorisedStatusDto'
import { DemandeInterventionStatusDto } from './DemandeInterventionStatusDto'

export type DemandeInterventionHistorisedStatusDto = ObjectHistorisedStatusDto<
    DemandeInterventionStatusDto['code']
>
