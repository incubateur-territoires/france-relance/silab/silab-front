import { ObjectStatus } from '@/shared/objectStatus/entities/ObjectStatus'
import { DemandeInterventionStatusDto } from './DemandeInterventionStatusDto'

export class DemandeInterventionStatus extends ObjectStatus<
    DemandeInterventionStatusDto['code']
> {
    public constructor(
        demandeInterventionStatusDto: DemandeInterventionStatusDto
    ) {
        super(demandeInterventionStatusDto)
    }
}
