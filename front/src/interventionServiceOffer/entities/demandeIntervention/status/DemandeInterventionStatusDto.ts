import { ObjectStatusDto } from '@/shared/objectStatus/entities/ObjectStatusDto'
import { z } from 'zod'

export const DemandeInterventionStatusAllowedCodesDtoSchema = z.enum([
    'awaiting_validation',
    'awaiting_information',
    'accepted',
    'closed',
])

export const DemandeInterventionStatusDtoSchema = z.object({
    code: DemandeInterventionStatusAllowedCodesDtoSchema,
    label: z.string(),
    createdBy: z.string(),
    createdAt: z.string(),
})

export interface DemandeInterventionStatusDto
    extends ObjectStatusDto<
            z.infer<typeof DemandeInterventionStatusAllowedCodesDtoSchema>
        >,
        z.infer<typeof DemandeInterventionStatusDtoSchema> {}
