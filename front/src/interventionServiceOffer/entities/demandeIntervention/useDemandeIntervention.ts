import { onBeforeUnmount, ref, Ref, watch } from 'vue'
import { DemandeIntervention } from './DemandeIntervention'
import { InterventionServiceOffer } from '../interventionServiceOffer/InterventionServiceOffer'
import { useMsal } from '@/plugins/msal/useMsal'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { RequestAborter } from '@/shared/api/entities/RequestAborter'
import { DemandeInterventionApi } from './DemandeInterventionApi'
import { VolatileSecuredData } from '@/shared/helpers/lifeCycle'
import { DemandeInterventionHistorisedStatus } from './status/DemandeInterventionHistorisedStatus'
import { isUndefined } from '@/shared/helpers/types'
import { SilabError } from '@/shared/errors/SilabError'

export function useDemandeIntervention(
    demandeInterventionId: Ref<DemandeIntervention['id'] | undefined>,
    serviceOfferId: Ref<InterventionServiceOffer['id']>,
    loadStatusHistory: boolean = false
) {
    const msal = useMsal()
    const userStore = useUserStore(msal)

    const demandeInterventionAbortController: RequestAborter =
        new RequestAborter()

    const demandeInterventionApi = new DemandeInterventionApi(msal)
    const demandeIntervention: Ref<VolatileSecuredData<DemandeIntervention>> =
        ref(undefined)
    const demandeInterventionStatusHistory: Ref<
        VolatileSecuredData<DemandeInterventionHistorisedStatus[]>
    > = ref(undefined)

    watch(
        [
            () => msal.accounts,
            () => demandeInterventionId.value,
            () => {
                return userStore.currentUserHasRoleForServiceOffer(
                    'INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS',
                    serviceOfferId.value
                )
            },
            () => serviceOfferId.value,
        ],
        () => {
            loadDemandeInterventionsEtStatusHistory()
        },
        { immediate: true }
    )

    async function loadDemandeInterventionsEtStatusHistory() {
        demandeIntervention.value = undefined
        if (isUndefined(demandeInterventionId.value)) {
            return
        }
        if (
            !userStore.currentUserHasRoleForServiceOffer(
                'INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS',
                serviceOfferId.value
            )
        ) {
            demandeIntervention.value = {
                accessGranted: false,
                data: undefined,
            }
            demandeInterventionStatusHistory.value = {
                accessGranted: false,
                data: undefined,
            }
            return
        }

        demandeInterventionAbortController.abortRequestsAndRefreshAborter()
        demandeInterventionApi
            .get(
                serviceOfferId.value,
                demandeInterventionId.value,
                demandeInterventionAbortController.getSignal()
            )
            .then(async (fetchedDemandeIntervention) => {
                demandeIntervention.value = {
                    accessGranted: true,
                    data: fetchedDemandeIntervention,
                }
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError(
                        "Erreur lors du chargement de la demande d'intervention",
                        error
                    )
                }
            })

        if (loadStatusHistory) {
            demandeInterventionApi
                .getStatusHistory(
                    serviceOfferId.value,
                    demandeInterventionId.value,
                    demandeInterventionAbortController.getSignal()
                )
                .then(async (fetchedDemandeInterventionStatusHistory) => {
                    demandeInterventionStatusHistory.value = {
                        accessGranted: true,
                        data: fetchedDemandeInterventionStatusHistory,
                    }
                })
                .catch((error) => {
                    if (error.name !== 'AbortError') {
                        throw new SilabError(
                            "Erreur lors du chargement de l'historique des états de la demande",
                            error
                        )
                    }
                })
        }
    }

    onBeforeUnmount(() => {
        demandeInterventionAbortController.abortRequests()
    })

    return {
        demandeIntervention,
        demandeInterventionStatusHistory,
        reload: loadDemandeInterventionsEtStatusHistory,
    }
}
