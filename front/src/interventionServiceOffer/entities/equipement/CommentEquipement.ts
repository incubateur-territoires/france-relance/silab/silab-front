import { CommentEquipementDto } from './CommentEquipementDto'

export class CommentEquipement {
    comment: string

    public constructor(commentEquipementDto: CommentEquipementDto) {
        this.comment = commentEquipementDto.comment
    }
}
