import { EquipementDto } from './EquipementDto'

export type CommentEquipementDto = Pick<EquipementDto, 'comment'>
