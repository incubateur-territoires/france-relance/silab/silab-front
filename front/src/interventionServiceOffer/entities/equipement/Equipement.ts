import { GeolocalisableItem } from '@/shared/item/entities/GeolocalisableItem'

import { EquipementDto } from './EquipementDto'
import { RouteLocationRaw } from 'vue-router'
import { LatLngLiteral } from 'leaflet'
import {
    PrimaryAction,
    SecondaryAction,
    getItemThumbnailUrl,
} from '@/shared/item/entities/ListableItem'
import { SilabImage } from '@/shared/document/entities/SilabImage'
import { isDefined } from '@/shared/helpers/types'
import { Document } from '@/shared/document/entities/Document'
import { EquipementStatus } from './status/EquipementStatus'
import { InterventionServiceOfferItem } from '../item/InterventionServiceOfferItem'
import { ItemLists } from '@/interventionServiceOffer/composables/equipement/useItemLists'

export class Equipement
    implements InterventionServiceOfferItem, GeolocalisableItem
{
    id: string
    label: string
    comment: string
    imagesIllustration: SilabImage[]
    code: string
    coordinates?: LatLngLiteral
    currentStatus: EquipementStatus
    gmaoObjectViewUrl?: string
    documentsJoints: Document[]
    ancetres?: {
        [key: string]: { id: Equipement['id']; label: Equipement['label'] }[]
    }
    interventionsAutorisees: boolean
    private itemRoute?: RouteLocationRaw
    private itemSecondaryAction?: SecondaryAction
    private itemPrimaryAction?: PrimaryAction

    public constructor(equipementDto: EquipementDto) {
        this.id = equipementDto.id
        this.label = equipementDto.label
        this.comment = equipementDto.comment
        this.imagesIllustration = SilabImage.fromArray(
            equipementDto.imagesIllustration
        )
        this.code = equipementDto.code
        this.coordinates = equipementDto.coordinates
        this.currentStatus = new EquipementStatus(equipementDto.currentStatus)
        this.gmaoObjectViewUrl = equipementDto.gmaoObjectViewUrl
        this.documentsJoints = equipementDto.documentsJoints.map(
            (documentDto) => new Document(documentDto)
        )
        this.ancetres = equipementDto.ancetres
        this.interventionsAutorisees = equipementDto.interventionsAutorisees
    }
    public getItemId() {
        return this.id
    }
    public getItemType(): keyof ItemLists {
        return 'équipements'
    }
    public getItemTitle() {
        return this.label
    }
    public getItemDescription() {
        return this.code ?? ''
    }
    public getItemImageUrl() {
        return this.imagesIllustration.at(0)?.url
    }

    public getItemThumbnailUrl = getItemThumbnailUrl

    public getItemChips() {
        return []
    }
    public isRequiringAttention() {
        return false
    }

    public getItemTemporalData() {
        return ''
    }
    public getItemRoute() {
        return this.itemRoute
    }
    public setItemRoute(itemRoute: RouteLocationRaw | undefined) {
        this.itemRoute = itemRoute
        return this
    }

    public primaryAction() {
        this.itemPrimaryAction?.()
    }

    public setItemPrimaryAction(primaryAction: PrimaryAction) {
        this.itemPrimaryAction = primaryAction
        return this
    }

    public getItemSecondaryAction() {
        return this.itemSecondaryAction
    }

    public setItemSecondaryAction(secondaryAction: SecondaryAction) {
        this.itemSecondaryAction = secondaryAction
        return this
    }

    public getCoordinates() {
        return this.coordinates
    }
    public hasLocation(): boolean {
        return isDefined(this.getCoordinates())
    }

    public static fromArray(equipementDtos: EquipementDto[]): Equipement[] {
        return equipementDtos.map(
            (equipementDto) => new Equipement(equipementDto)
        )
    }

    public hasLignéeAncetres() {
        return isDefined(this.ancetres) && Object.keys(this.ancetres).length > 0
    }
}
