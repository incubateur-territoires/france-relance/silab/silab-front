import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { Equipement } from './Equipement'
import { EquipementDtoSchema } from './EquipementDto'
import { CommentEquipementDto } from './CommentEquipementDto'
import { CommentEquipement } from './CommentEquipement'
import { ResultPage } from '@/shared/helpers/backend'
import { useRouter } from 'vue-router'
import { InterventionServiceOffer } from '../interventionServiceOffer/InterventionServiceOffer'
import { EquipementFilters } from './filters/EquipementFilters'
import { EquipementCoordinates } from './EquipementCoordinates'
import { EquipementCoordinatesDtoSchema } from './EquipementCoordinatesDto'
import { ObjectPins } from '@/shared/entities/pins/ObjectPins'

export class EquipementApi extends AzureTokenAccessApi {
    router = useRouter()

    async get(
        serviceOfferId: ServiceOffer['id'],
        equipementId: Equipement['id'],
        includeAncetres?: boolean,
        abortSignal?: AbortSignal
    ): Promise<Equipement> {
        includeAncetres ??= false

        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/equipements/${equipementId}`,
            new Map([['includeAncetres', includeAncetres]]),
            abortSignal
        )

        const equipementDto = EquipementDtoSchema.parse(
            await rawResponse.json()
        )

        return new Equipement(equipementDto)
    }

    async getAll(
        id: InterventionServiceOffer['id'],
        filters: EquipementFilters,
        pins: ObjectPins<Equipement>,
        page: number,
        abortSignal?: AbortSignal
    ): Promise<ResultPage<Equipement>> {
        const queryParameters = filters
            .toQueryParameters()
            .set('pinnedIds', pins.pinnedIds)
            .set('page', page.toString())
            .set('includeAncetres', false)
            .set('parentDirectUniquement', true)
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${id}/equipements`,
            queryParameters,
            abortSignal
        )

        const equipementDtos = EquipementDtoSchema.array().parse(
            await rawResponse.json()
        )

        return {
            data: equipementDtos.map((childEquipementDto) => {
                return new Equipement(childEquipementDto)
            }),
            maxResults: Number(rawResponse.headers.get('X-Total-Count')),
        }
    }

    async getAllCoordinates(
        id: InterventionServiceOffer['id'],
        filters: EquipementFilters,
        abortSignal?: AbortSignal
    ): Promise<Array<EquipementCoordinates>> {
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${id}/equipements-coordinates`,
            filters.toQueryParameters().set('parentDirectUniquement', true),
            abortSignal
        )

        const equipementCoordinatesDtos =
            EquipementCoordinatesDtoSchema.array().parse(
                await rawResponse.json()
            )

        return equipementCoordinatesDtos.map((childEquipementDto) => {
            return new EquipementCoordinates(childEquipementDto)
        })
    }

    async comment(
        serviceOfferId: ServiceOffer['id'],
        equipementId: Equipement['id'],
        updatedEquipement: CommentEquipementDto
    ) {
        const rawResponse = await this.postBackendData(
            `intervention-service-offers/${serviceOfferId}/equipements/${equipementId}/comment`,
            JSON.stringify(updatedEquipement)
        )

        const commentEquipementDto =
            (await rawResponse.json()) as CommentEquipementDto

        return new CommentEquipement(commentEquipementDto)
    }
}
