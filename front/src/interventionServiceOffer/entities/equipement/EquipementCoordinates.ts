import { GeolocalisableItem } from '@/shared/item/entities/GeolocalisableItem'

import { RouteLocationRaw } from 'vue-router'
import { LatLngLiteral } from 'leaflet'
import {
    SecondaryAction,
    getItemThumbnailUrl,
} from '@/shared/item/entities/ListableItem'
import { EquipementCoordinatesDto } from './EquipementCoordinatesDto'
import { InterventionServiceOfferItem } from '../item/InterventionServiceOfferItem'
import { ItemLists } from '@/interventionServiceOffer/composables/equipement/useItemLists'

export class EquipementCoordinates
    implements InterventionServiceOfferItem, GeolocalisableItem
{
    id: string
    label: string
    coordinates: LatLngLiteral
    private itemRoute?: RouteLocationRaw
    private itemSecondaryAction?: SecondaryAction

    public constructor(equipementCoordinatesDto: EquipementCoordinatesDto) {
        this.id = equipementCoordinatesDto.id
        this.label = equipementCoordinatesDto.label
        this.coordinates = {
            lat: equipementCoordinatesDto.coordinates.lat,
            lng: equipementCoordinatesDto.coordinates.lng,
        }
    }

    public hasLocation(): boolean {
        return true
    }

    public hasImage(): boolean {
        return false
    }

    public getItemId() {
        return this.id
    }
    public getItemType(): keyof ItemLists {
        return 'équipements'
    }
    public getItemTitle() {
        return `${this.label}`
    }

    public getItemDescription() {
        return ''
    }
    public getItemImageUrl() {
        return undefined
    }

    public getItemThumbnailUrl = getItemThumbnailUrl

    public getItemChips() {
        return []
    }

    public isRequiringAttention() {
        return false
    }

    public getItemTemporalData() {
        return ''
    }
    public getItemRoute() {
        return this.itemRoute
    }
    public setItemRoute(itemRoute: RouteLocationRaw | undefined) {
        this.itemRoute = itemRoute
        return this
    }
    public getCoordinates() {
        return this.coordinates
    }
    public getItemSecondaryAction() {
        return this.itemSecondaryAction
    }

    public setItemSecondaryAction(secondaryAction: SecondaryAction) {
        this.itemSecondaryAction = secondaryAction
        return this
    }
}
