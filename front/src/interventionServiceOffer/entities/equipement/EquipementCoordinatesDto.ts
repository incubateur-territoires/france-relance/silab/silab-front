import { z } from 'zod'

export const EquipementCoordinatesDtoSchema = z.object({
    id: z.string(),
    label: z.string(),
    coordinates: z.object({
        lat: z.number(),
        lng: z.number(),
    }),
})

export type EquipementCoordinatesDto = z.infer<
    typeof EquipementCoordinatesDtoSchema
>
