import {
    SilabImageDto,
    SilabImageDtoSchema,
} from '@/shared/document/entities/SilabImageDto'
import { Equipement } from './Equipement'
import { Dto } from '@/shared/api/entities/Dtos'
import {
    DocumentDto,
    DocumentDtoSchema,
} from '@/shared/document/entities/DocumentDto'
import { z } from 'zod'
import {
    EquipementStatusDto,
    EquipementStatusDtoSchema,
} from './status/EquipementStatusDto'
import { CoordinatesDto } from '@/shared/geocoding/entities/coordinate/CoordinatesDto'

export const EquipementDtoSchema = z.object({
    id: z.string(),
    label: z.string(),
    comment: z.string(),
    imagesIllustration: SilabImageDtoSchema.array(),
    code: z.string(),
    coordinates: z
        .object({
            lat: z.number(),
            lng: z.number(),
        })
        .optional(),
    currentStatus: EquipementStatusDtoSchema,
    gmaoObjectViewUrl: z.string().optional(),
    documentsJoints: DocumentDtoSchema.array(),
    ancetres: z
        .record(
            z
                .object({
                    id: z.string(),
                    label: z.string(),
                })
                .array()
        )
        .optional()
        .catch({}), // amelioration : faire en sorte que le back ne renvoie pas de tableau au lieu d'un object quand c'est vide
    interventionsAutorisees: z.boolean(),
})

// export type EquipementDto = z.infer<typeof EquipementDtoSchema>
// amelioration: abandonner les interfaces natives
export interface EquipementDto
    extends Dto<Equipement>,
        z.infer<typeof EquipementDtoSchema> {
    id: string
    label: string
    comment: string
    imagesIllustration: SilabImageDto[]
    code: string
    coordinates?: CoordinatesDto
    currentStatus: EquipementStatusDto
    gmaoObjectViewUrl?: string
    documentsJoints: DocumentDto[]
    ancetres?: {
        [key: string]: {
            id: EquipementDto['id']
            label: EquipementDto['label']
        }[]
    }
    interventionsAutorisees: boolean
}
