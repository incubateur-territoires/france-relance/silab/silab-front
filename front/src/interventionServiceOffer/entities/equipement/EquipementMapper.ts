import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { Equipement } from './Equipement'
import { EquipementDto, EquipementDtoSchema } from './EquipementDto'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { silabImageMapper } from '@/shared/document/entities/SilabImageMapper'
import { Dto } from '@/shared/api/entities/Dtos'
import { documentMapper } from '@/shared/document/entities/DocumentMapper'
import { toRaw } from 'vue'
import { ZodError } from 'zod'
import { isDefined } from '@/shared/helpers/types'

interface EquipementMapper extends EntityMapper<Equipement> {
    toDto: (partialEquipement: Partial<Equipement>) => EquipementDto
    toDtoArray: (partialEquipements: Partial<Equipement>[]) => EquipementDto[]
    fromDto: (equipementDto: Dto<Equipement>) => Equipement
    fromUnsafeDto: (entityDto: unknown) => Equipement
    fromDtoArray: (
        equipementDtos: Dto<Equipement>[] | Dto<Equipement[]>
    ) => Equipement[]
    fromUnsafeDtoArray: (equipementDtos: unknown) => Equipement[]
}

export const equipementMapper: EquipementMapper = {
    toDto(partialEquipement: Partial<Equipement>): EquipementDto {
        try {
            return EquipementDtoSchema.parse({
                id: partialEquipement.id,
                label: partialEquipement.label,
                comment: partialEquipement.comment,
                imagesIllustration: isDefined(
                    partialEquipement.imagesIllustration
                )
                    ? silabImageMapper.toDtoArray(
                          partialEquipement.imagesIllustration
                      )
                    : undefined,
                code: partialEquipement.code,
                coordinates: partialEquipement.coordinates,
                currentStatus: partialEquipement.currentStatus?.toDto(),
                gmaoObjectViewUrl: partialEquipement.gmaoObjectViewUrl,
                documentsJoints: partialEquipement.documentsJoints?.map(
                    toRaw(documentMapper.toDto)
                ),
                ancetres: partialEquipement.ancetres,
                interventionsAutorisees:
                    partialEquipement.interventionsAutorisees,
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'equipement fournit ne possède pas tout les attributs requis.",
                    error
                )
            }
            throw error
        }
    },

    fromDto: function (equipementDto: Dto<Equipement>): Equipement {
        return new Equipement(equipementDto as EquipementDto) // amelioration : peut-on faire + propre ?
    },

    fromUnsafeDto: function (entityDto: unknown): Equipement {
        return new Equipement(EquipementDtoSchema.parse(entityDto)) // amelioration : peut-on faire + propre ?
    },

    toDtoArray(partialEquipements: Partial<Equipement>[]): EquipementDto[] {
        return partialEquipements.map((partialEquipement) =>
            this.toDto(partialEquipement)
        )
    },

    fromDtoArray: function (
        equipementDtos: Dto<Equipement>[] | Dto<Equipement[]>
    ): Equipement[] {
        return Equipement.fromArray(equipementDtos as EquipementDto[]) // amelioration : peut-on faire + propre ?
    },

    fromUnsafeDtoArray: function (equipementDtos: unknown): Equipement[] {
        return Equipement.fromArray(
            EquipementDtoSchema.array().parse(equipementDtos)
        )
    },
}
