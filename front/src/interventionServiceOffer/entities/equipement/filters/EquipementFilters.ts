import { ObjectFilters } from '@/shared/filters/entities/ObjectFilters'
import {
    EquipementStatusDto,
    EquipementStatusDtoSchema,
} from '../status/EquipementStatusDto'
import { EquipementDto, EquipementDtoSchema } from '../EquipementDto'
import { z } from 'zod'

export const EquipementFiltersDtoSchema = z.object({
    'currentStatus.code': EquipementStatusDtoSchema.shape.code.array(),
    labelOuCode: z
        .union([
            EquipementDtoSchema.shape.code,
            EquipementDtoSchema.shape.label,
        ])
        .optional(),
    limit: z.number().optional(),
    id: EquipementDtoSchema.shape.id.array(),
    'ancetre.id': EquipementDtoSchema.shape.id.optional(),
})

export type EquipementFiltersDto = z.infer<typeof EquipementFiltersDtoSchema>

export class EquipementFilters extends ObjectFilters<{
    'currentStatus.code': EquipementStatusDto['code'][]
    labelOuCode: (EquipementDto['label'] | EquipementDto['code']) | undefined
    limit: number | undefined
    id: EquipementDto['id'][]
    'ancetre.id': EquipementDto['id'] | undefined
}> {
    public getDefaultFilters() {
        return {
            'currentStatus.code': ['active'] as EquipementStatusDto['code'][],
            labelOuCode: undefined,
            limit: undefined,
            id: [],
            'ancetre.id': undefined,
        }
    }
}
