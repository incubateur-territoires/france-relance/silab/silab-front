import { EquipementFilters, EquipementFiltersDto } from './EquipementFilters'

export const equipementFiltersMapper = {
    toDto: (equipementFilters: EquipementFilters): EquipementFiltersDto => {
        return {
            'currentStatus.code':
                equipementFilters.filters['currentStatus.code'],
            labelOuCode: equipementFilters.filters.labelOuCode,
            limit: equipementFilters.filters.limit,
            id: equipementFilters.filters.id,
            'ancetre.id': equipementFilters.filters['ancetre.id'],
        }
    },
    fromDto: (
        equipementfiltersDto: EquipementFiltersDto
    ): EquipementFilters => {
        return new EquipementFilters({
            'currentStatus.code': equipementfiltersDto['currentStatus.code'],
            labelOuCode: equipementfiltersDto.labelOuCode,
            limit: equipementfiltersDto.limit,
            id: equipementfiltersDto.id,
            'ancetre.id': equipementfiltersDto['ancetre.id'],
        })
    },
}
