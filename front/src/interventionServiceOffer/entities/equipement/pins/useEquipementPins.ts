import { Ref } from 'vue'
import { InterventionServiceOffer } from '../../interventionServiceOffer/InterventionServiceOffer'
import { useMsal } from '@/plugins/msal/useMsal'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { useLocalStoragePersistedState } from '@/shared/composables/useLocalStoragePersistedState'
import { ObjectPins } from '@/shared/entities/pins/ObjectPins'
import { Equipement } from '../Equipement'
import { EquipementDtoSchema } from '../EquipementDto'

export const useEquipementPins = function (
    interventionServiceOfferId: Ref<InterventionServiceOffer['id']>
) {
    const msal = useMsal()
    const userStore = useUserStore(msal)

    return useLocalStoragePersistedState(
        () => {
            return `${userStore.currentUser.id}|${interventionServiceOfferId.value}|equipementPins`
        },
        new ObjectPins<Equipement>(),
        EquipementDtoSchema.shape.id.array(),
        {
            fromDto: (pinsDto: Array<Equipement['id']>) => {
                return new ObjectPins<Equipement>(pinsDto)
            },
            toDto: (pins: ObjectPins<Equipement>) => {
                return pins.pinnedIds
            },
        }
    )
}
