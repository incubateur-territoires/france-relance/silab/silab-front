import { ObjectStatus } from '@/shared/objectStatus/entities/ObjectStatus'
import { EquipementStatusDto } from './EquipementStatusDto'

export class EquipementStatus extends ObjectStatus<
    EquipementStatusDto['code']
> {
    public constructor(equipmentDto: EquipementStatusDto) {
        super(equipmentDto)
    }
}
