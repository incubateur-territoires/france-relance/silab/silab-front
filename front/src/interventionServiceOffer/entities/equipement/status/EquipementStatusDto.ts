import { ObjectStatusDto } from '@/shared/objectStatus/entities/ObjectStatusDto'
import { z } from 'zod'

export const EquipementStatusAllowedCodesDtoSchema = z.enum([
    'active',
    'inactive',
])

export const EquipementStatusDtoSchema = z.object({
    code: EquipementStatusAllowedCodesDtoSchema,
    label: z.string(),
    createdBy: z.string(),
    createdAt: z.string(),
})

export interface EquipementStatusDto
    extends ObjectStatusDto<
            z.infer<typeof EquipementStatusAllowedCodesDtoSchema>
        >,
        z.infer<typeof EquipementStatusDtoSchema> {}
