import { Ref, onBeforeUnmount, ref, watch } from 'vue'
import { EquipementDto } from './EquipementDto'
import { Equipement } from './Equipement'
import { VolatileSecuredData } from '@/shared/helpers/lifeCycle'
import { EquipementApi } from './EquipementApi'
import { useMsal } from '@/plugins/msal/useMsal'
import { RequestAborter } from '@/shared/api/entities/RequestAborter'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { InterventionServiceOffer } from '../interventionServiceOffer/InterventionServiceOffer'
import { isUndefined } from '@/shared/helpers/types'
import { SilabError } from '@/shared/errors/SilabError'

export function useEquipement(
    equipementId: Ref<EquipementDto['id'] | undefined>,
    serviceOfferId: Ref<InterventionServiceOffer['id']>,
    includeAncetres: boolean = false
) {
    const msal = useMsal()

    const equipement: Ref<VolatileSecuredData<Equipement>> = ref(undefined)
    const userStore = useUserStore(msal)

    const equipementApi = new EquipementApi(msal)
    const equipementAbortController: RequestAborter = new RequestAborter()

    watch(
        [
            () => msal.accounts,
            () => equipementId.value,
            () => {
                return userStore.currentUserHasRoleForServiceOffer(
                    'INTERVENTION_CONSULTER_EQUIPEMENT',
                    serviceOfferId.value
                )
            },
            () => serviceOfferId.value,
        ],
        () => {
            equipement.value = undefined
            if (isUndefined(equipementId.value)) {
                return
            }
            if (
                !userStore.currentUserHasRoleForServiceOffer(
                    //amélioration: piste->composable du type useCurrentUserRight(right, serviceOfferId)
                    'INTERVENTION_CONSULTER_EQUIPEMENT',
                    serviceOfferId.value
                )
            ) {
                equipement.value = {
                    accessGranted: false,
                    data: undefined,
                }
                return
            }

            equipementAbortController.abortRequestsAndRefreshAborter()
            equipementApi
                .get(
                    serviceOfferId.value,
                    equipementId.value,
                    includeAncetres,
                    equipementAbortController.getSignal()
                )
                .then(async (fetchedEquipement) => {
                    equipement.value = {
                        accessGranted: true,
                        data: fetchedEquipement,
                    }
                })
                .catch((error) => {
                    if (error.name !== 'AbortError') {
                        throw new SilabError(
                            "Erreur lors du chargement de l'équipement'",
                            error
                        )
                    }
                })
        },
        { immediate: true }
    )

    onBeforeUnmount(() => {
        equipementAbortController.abortRequests()
    })

    return equipement
}
