import { ObjectFilters } from '@/shared/filters/entities/ObjectFilters'

export class ActiviteFilters extends ObjectFilters<{
    isDone?: boolean
}> {
    public getDefaultFilters() {
        return {
            isDone: undefined,
        }
    }
}
