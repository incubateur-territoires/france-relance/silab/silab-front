import { ObjectFilters } from '@/shared/filters/entities/ObjectFilters'
import { Equipement } from '../equipement/Equipement'
export class CommunFilters extends ObjectFilters<{
    'equipementRacine.id': Equipement['id'] | undefined
}> {
    public getDefaultFilters() {
        return {
            'equipementRacine.id': undefined,
        }
    }
}
