import { ObjectFilters } from '@/shared/filters/entities/ObjectFilters'
import { EquipementStatusDto } from '../equipement/status/EquipementStatusDto'

export class EquipementDescendantFilters extends ObjectFilters<{
    onlyDirectChildren: boolean
    'currentStatus.code': EquipementStatusDto['code'][]
}> {
    public getDefaultFilters() {
        return {
            onlyDirectChildren: false,
            'currentStatus.code': ['active'] as EquipementStatusDto['code'][],
        }
    }
}
