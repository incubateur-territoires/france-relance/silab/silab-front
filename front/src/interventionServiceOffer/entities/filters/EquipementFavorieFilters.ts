import { ObjectFilters } from '@/shared/filters/entities/ObjectFilters'

export class EquipementFavorisFilters extends ObjectFilters<{
    favorisOnly: boolean
}> {
    public getDefaultFilters() {
        return {
            favorisOnly: false,
        }
    }
}
