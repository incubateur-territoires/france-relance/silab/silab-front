import { GmaoActeur } from '@/shared/gmaoActeur/GmaoActeur'
import { ObjectFilters } from '@/shared/filters/entities/ObjectFilters'
import { GmaoActeurStatusDto } from '@/shared/gmaoActeur/status/GmaoActeurStatusDto'

export class GmaoActeurFilters extends ObjectFilters<{
    nomComplet: GmaoActeur['nomComplet'] | undefined
    limit: number | undefined
    'currentStatus.code': GmaoActeurStatusDto['code'][]
    include: 'operationsHabilitees'[]
}> {
    public getDefaultFilters() {
        return {
            nomComplet: undefined,
            limit: undefined,
            'currentStatus.code': ['active'] as GmaoActeurStatusDto['code'][],
            include: [] as 'operationsHabilitees'[],
        }
    }
}
