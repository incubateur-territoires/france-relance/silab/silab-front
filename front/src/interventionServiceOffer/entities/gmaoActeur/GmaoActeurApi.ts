import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { GmaoActeurDtoSchema } from '@/shared/gmaoActeur/GmaoActeurDto'
import { GmaoActeur } from '@/shared/gmaoActeur/GmaoActeur'
import { InterventionServiceOffer } from '../interventionServiceOffer/InterventionServiceOffer'
import { GmaoActeurFilters } from '../filters/GmaoActeurFilters'

export class GmaoActeurApi extends AzureTokenAccessApi {
    async getMyself(
        serviceOfferId: InterventionServiceOffer['id'],
        abortSignal?: AbortSignal
    ): Promise<GmaoActeur> {
        const filters = new GmaoActeurFilters({
            include: ['operationsHabilitees'],
            'currentStatus.code': undefined,
            limit: undefined,
            nomComplet: undefined,
        })

        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/myself`,
            filters.toQueryParameters(),
            abortSignal
        )

        const gmaoActeurDto = GmaoActeurDtoSchema.parse(
            await rawResponse.json()
        )

        return new GmaoActeur(gmaoActeurDto)
    }

    async getTeammates(
        serviceOfferId: InterventionServiceOffer['id'],
        abortSignal?: AbortSignal
    ): Promise<GmaoActeur[]> {
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/myself/equipiers`,
            undefined,
            abortSignal
        )

        const gmaoActeurDtos = GmaoActeurDtoSchema.array().parse(
            await rawResponse.json()
        )

        return GmaoActeur.fromArray(gmaoActeurDtos)
    }

    async getAll(
        serviceOfferId: InterventionServiceOffer['id'],
        filters: GmaoActeurFilters,
        abortSignal?: AbortSignal
    ): Promise<GmaoActeur[]> {
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/gmao-acteurs`,
            filters.toQueryParameters(),
            abortSignal
        )

        const gmaoActeurDtos = GmaoActeurDtoSchema.array().parse(
            await rawResponse.json()
        )

        return GmaoActeur.fromArray(gmaoActeurDtos)
    }
}
