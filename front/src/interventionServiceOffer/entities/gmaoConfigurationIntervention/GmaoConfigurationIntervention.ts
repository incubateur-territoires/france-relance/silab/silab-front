import { GmaoConfigurationInterventionDto } from './GmaoConfigurationInterventionDto'

export class GmaoConfigurationIntervention
    implements GmaoConfigurationInterventionDto
{
    id: string | number
    notes?: string
    title: string
    environnement?: string
    actionTypesMap: { [key: string]: string }
    '@id': string

    public constructor(
        gmaoConfigurationInterventionDto: GmaoConfigurationInterventionDto
    ) {
        this.id = gmaoConfigurationInterventionDto.id
        this.notes = gmaoConfigurationInterventionDto.notes
        this.title = gmaoConfigurationInterventionDto.title
        this.environnement = gmaoConfigurationInterventionDto.environnement
        this.actionTypesMap = gmaoConfigurationInterventionDto.actionTypesMap
        this['@id'] =
            `/api/gmao-configuration-interventions/${gmaoConfigurationInterventionDto.id}`
    }
}
