import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { GmaoConfigurationIntervention } from './GmaoConfigurationIntervention'
import { GmaoConfigurationInterventionDto } from './GmaoConfigurationInterventionDto'
import { EquipementFilters } from '../equipement/filters/EquipementFilters'
import { Equipement } from '../equipement/Equipement'
import { EquipementDtoSchema } from '../equipement/EquipementDto'
import { equipementMapper } from '../equipement/EquipementMapper'

export class GmaoConfigurationInterventionApi extends AzureTokenAccessApi {
    async getAll(
        abortSignal?: AbortSignal
    ): Promise<GmaoConfigurationIntervention[]> {
        const rawResponse = await this.getBackendData(
            'gmao-configuration-interventions',
            new Map(),
            abortSignal
        )

        const gmaoConfigurationInterventionDtos =
            (await rawResponse.json()) as GmaoConfigurationInterventionDto[]

        // On veux instancier les interventions avec leur classes finales/filles, on utilise pour cela le factory sur chaque Dto reçu
        return gmaoConfigurationInterventionDtos.map(
            (gmaoConfigurationInterventionDto) => {
                return new GmaoConfigurationIntervention(
                    gmaoConfigurationInterventionDto
                )
            }
        )
    }

    async getAllEquipements(
        gmaoConfigurationInterventionId: GmaoConfigurationInterventionDto['id'],
        filters: EquipementFilters,
        abortSignal?: AbortSignal
    ): Promise<Equipement[]> {
        const rawResponse = await this.getBackendData(
            `gmao-configuration-interventions/${gmaoConfigurationInterventionId}/equipements`,
            filters.toQueryParameters(),
            abortSignal
        )

        const equipementDtos = EquipementDtoSchema.array().parse(
            await rawResponse.json()
        )

        return equipementMapper.fromDtoArray(equipementDtos)
    }
}
