import { z } from 'zod'

export const GmaoConfigurationInterventionDtoSchema = z.object({
    id: z.number().or(z.string()),
    notes: z.string().optional(),
    title: z.string(),
    environnement: z.string().optional(),
    //ce catch n'est pas ideal mais le back renvoi un tableau quand c'est vide
    // le `normalizationContext: [Serializer::EMPTY_ARRAY_AS_OBJECT => true]` de GmaoConfigurationIntervention.php ne semble pas fonctionner...
    actionTypesMap: z.record(z.string()).catch({}),
})

export type GmaoConfigurationInterventionDto = z.infer<
    typeof GmaoConfigurationInterventionDtoSchema
>
