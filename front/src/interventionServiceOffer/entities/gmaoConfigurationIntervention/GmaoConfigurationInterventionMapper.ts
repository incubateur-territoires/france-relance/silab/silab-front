import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { GmaoConfigurationIntervention } from './GmaoConfigurationIntervention'
import { GmaoConfigurationInterventionDto } from './GmaoConfigurationInterventionDto'
import { Dto } from '@/shared/api/entities/Dtos'
import { isUndefined } from '@/shared/helpers/types'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'

interface GmaoConfigurationInterventionMapper
    extends EntityMapper<GmaoConfigurationIntervention> {
    toDto: (
        partialGmaoConfigurationIntervention: Partial<GmaoConfigurationIntervention>
    ) => GmaoConfigurationInterventionDto
    fromDto: (
        gmaoConfigurationInterventionDto: Dto<GmaoConfigurationIntervention>
    ) => GmaoConfigurationIntervention
}

export const gmaoConfigurationInterventionMapper: GmaoConfigurationInterventionMapper =
    {
        toDto(
            partialGmaoConfigurationIntervention: Partial<GmaoConfigurationIntervention>
        ): GmaoConfigurationInterventionDto {
            if (
                isUndefined(partialGmaoConfigurationIntervention.id) ||
                isUndefined(partialGmaoConfigurationIntervention.title) ||
                isUndefined(
                    partialGmaoConfigurationIntervention.environnement
                ) ||
                isUndefined(partialGmaoConfigurationIntervention.actionTypesMap)
            ) {
                throw new IncompleteEntity(
                    'La configuration GMAO (pour ODS de type intervention) ne possède pas tout les attributs requis.'
                )
            }

            return {
                id: partialGmaoConfigurationIntervention.id,
                notes: partialGmaoConfigurationIntervention.notes,
                title: partialGmaoConfigurationIntervention.title,
                environnement:
                    partialGmaoConfigurationIntervention.environnement,
                actionTypesMap:
                    partialGmaoConfigurationIntervention.actionTypesMap,
            }
        },
        fromDto(
            gmaoConfigurationInterventionDto: Dto<GmaoConfigurationIntervention>
        ): GmaoConfigurationIntervention {
            return new GmaoConfigurationIntervention(
                gmaoConfigurationInterventionDto as GmaoConfigurationInterventionDto
            )
        },
    }
