import { CarlConfigurationInterventionDto } from './CarlConfigurationInterventionDto'
import {
    ListableItem,
    PrimaryAction,
    SecondaryAction,
} from '@/shared/item/entities/ListableItem'
import { GmaoConfigurationIntervention } from '../GmaoConfigurationIntervention'
import { CarlClient } from '@/shared/carl/entitites/carlClient/CarlClient'
export class CarlConfigurationIntervention
    extends GmaoConfigurationIntervention
    implements ListableItem
{
    interventionCostCenterIdsFilter: Array<string>
    costCenterIdForInterventionCreation?: string
    useEquipmentCostCenterForInterventionCreation: boolean
    directions: Array<string>
    actionTypesMap: { [key: string]: string }
    carlAttributesPresetForInterventionCreation: {
        [key: string]: string
    }
    carlRelationshipsPresetForInterventionCreation: {
        [key: string]: { id: string; type: string }
    }
    carlAttributesPresetForDemandeInterventionCreation: {
        [key: string]: string
    }
    carlRelationshipsPresetForDemandeInterventionCreation: {
        [key: string]: { id: string; type: string }
    }
    photoAvantInterventionDoctypeId: string
    photoApresInterventionDoctypeId: string
    carlClient: CarlClient
    woViewUrlSubPath?: string
    mrViewUrlSubPath?: string
    boxViewUrlSubPath?: string
    materialViewUrlSubPath?: string
    includeEquipementsChildrenWithType: Array<string>
    private itemSecondaryAction?: SecondaryAction

    constructor(
        carlConfigurationInterventionDto: CarlConfigurationInterventionDto
    ) {
        super(carlConfigurationInterventionDto)
        this.interventionCostCenterIdsFilter =
            carlConfigurationInterventionDto.interventionCostCenterIdsFilter
        this.costCenterIdForInterventionCreation =
            carlConfigurationInterventionDto.costCenterIdForInterventionCreation
        this.useEquipmentCostCenterForInterventionCreation =
            carlConfigurationInterventionDto.useEquipmentCostCenterForInterventionCreation
        this.directions = carlConfigurationInterventionDto.directions
        this.actionTypesMap = carlConfigurationInterventionDto.actionTypesMap
        this.carlAttributesPresetForInterventionCreation =
            carlConfigurationInterventionDto.carlAttributesPresetForInterventionCreation
        this.carlRelationshipsPresetForInterventionCreation =
            carlConfigurationInterventionDto.carlRelationshipsPresetForInterventionCreation
        this.carlAttributesPresetForDemandeInterventionCreation =
            carlConfigurationInterventionDto.carlAttributesPresetForDemandeInterventionCreation
        this.carlRelationshipsPresetForDemandeInterventionCreation =
            carlConfigurationInterventionDto.carlRelationshipsPresetForDemandeInterventionCreation
        this.photoAvantInterventionDoctypeId =
            carlConfigurationInterventionDto.photoAvantInterventionDoctypeId
        this.photoApresInterventionDoctypeId =
            carlConfigurationInterventionDto.photoApresInterventionDoctypeId
        this.carlClient = new CarlClient(
            carlConfigurationInterventionDto.carlClient
        )
        this.woViewUrlSubPath =
            carlConfigurationInterventionDto.woViewUrlSubPath
        this.mrViewUrlSubPath =
            carlConfigurationInterventionDto.mrViewUrlSubPath
        this.boxViewUrlSubPath =
            carlConfigurationInterventionDto.boxViewUrlSubPath
        this.materialViewUrlSubPath =
            carlConfigurationInterventionDto.materialViewUrlSubPath
        this.includeEquipementsChildrenWithType =
            carlConfigurationInterventionDto.includeEquipementsChildrenWithType
        this['@id'] = `/api/carl-clients/${carlConfigurationInterventionDto.id}`
    }
    primaryAction?: PrimaryAction | undefined
    setItemPrimaryAction?: ((primaryAction: PrimaryAction) => this) | undefined
    isRequiringAttention() {
        return false
    }
    public getItemId() {
        return this.id.toString()
    }
    public getItemTitle() {
        return this.title
    }
    public getItemDescription() {
        return `Client Carl : ${this.carlClient.title}`
    }
    public getItemImageUrl() {
        return undefined
    }
    getItemThumbnailUrl() {
        return undefined
    }
    getItemChips() {
        return []
    }
    getItemTemporalData() {
        return ''
    }
    getItemRoute() {
        return undefined
    }
    setItemRoute() {
        return this
    }

    public getItemSecondaryAction() {
        return this.itemSecondaryAction
    }

    public setItemSecondaryAction(secondaryAction: SecondaryAction) {
        this.itemSecondaryAction = secondaryAction
        return this
    }
}
