import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { CreateCarlConfigurationInterventionDto } from './CreateCarlConfigurationInterventionDto'
import { CarlConfigurationInterventionDtoSchema } from './CarlConfigurationInterventionDto'
import { CarlConfigurationIntervention } from './CarlConfigurationIntervention'
import { EntityApi } from '@/shared/api/entities/EntityApi'
import { EditCarlConfigurationInterventionDto } from './EditCarlConfigurationInterventionDto'

export class CarlConfigurationInterventionApi
    extends AzureTokenAccessApi
    implements EntityApi<CarlConfigurationIntervention>
{
    async getAll(
        abortSignal?: AbortSignal
    ): Promise<CarlConfigurationIntervention[]> {
        const rawResponse = await this.getBackendData(
            `carl-configurations-intervention`,
            new Map(),
            abortSignal
        )

        const carlConfigurationsInterventionDto =
            CarlConfigurationInterventionDtoSchema.array().parse(
                await rawResponse.json()
            )

        return carlConfigurationsInterventionDto.map(
            (carlConfigurationInterventionDto) => {
                return new CarlConfigurationIntervention(
                    carlConfigurationInterventionDto
                )
            }
        )
    }

    async get(
        carlConfigurationInterventionId: CarlConfigurationIntervention['id']
    ) {
        const rawResponse = await this.getBackendData(
            `carl-configurations-intervention/${carlConfigurationInterventionId}`
        )

        const carlConfigurationInterventionDto =
            CarlConfigurationInterventionDtoSchema.parse(
                await rawResponse.json()
            )

        return new CarlConfigurationIntervention(
            carlConfigurationInterventionDto
        )
    }

    async create(
        newCarlConfigurationIntervention: CreateCarlConfigurationInterventionDto
    ) {
        const rawResponse = await this.postBackendData(
            `carl-configurations-intervention`,
            JSON.stringify(newCarlConfigurationIntervention)
        )

        const carlConfigurationInterventionDto =
            CarlConfigurationInterventionDtoSchema.parse(
                await rawResponse.json()
            )

        return new CarlConfigurationIntervention(
            carlConfigurationInterventionDto
        )
    }

    async update(
        carlConfigurationInterventionId: CarlConfigurationIntervention['id'],
        updatedCarlConfigurationIntervention: EditCarlConfigurationInterventionDto
    ) {
        const rawResponse = await this.putBackendData(
            `carl-configurations-intervention/${carlConfigurationInterventionId}`,
            JSON.stringify(updatedCarlConfigurationIntervention)
        )

        const carlConfigurationInterventionDto =
            CarlConfigurationInterventionDtoSchema.parse(
                await rawResponse.json()
            )

        return new CarlConfigurationIntervention(
            carlConfigurationInterventionDto
        )
    }

    async delete(
        carlConfigurationInterventionId: CarlConfigurationIntervention['id']
    ) {
        await this.deleteBackendData(
            `carl-configurations-intervention/${carlConfigurationInterventionId}`
        )
    }
}
