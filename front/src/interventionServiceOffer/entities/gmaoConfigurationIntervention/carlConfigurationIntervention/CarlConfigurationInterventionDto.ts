import { Dto } from '@/shared/api/entities/Dtos'
import { CarlConfigurationIntervention } from './CarlConfigurationIntervention'
import {
    CarlClientDto,
    CarlClientDtoSchema,
} from '@/shared/carl/entitites/carlClient/CarlClientDto'
import {
    GmaoConfigurationInterventionDto,
    GmaoConfigurationInterventionDtoSchema,
} from '../GmaoConfigurationInterventionDto'
import { z } from 'zod'

export const CarlConfigurationInterventionDtoSchema =
    GmaoConfigurationInterventionDtoSchema.extend({
        interventionCostCenterIdsFilter: z.string().array(),
        costCenterIdForInterventionCreation: z.string().optional(),
        useEquipmentCostCenterForInterventionCreation: z.boolean(),
        directions: z.string().array(),
        carlAttributesPresetForInterventionCreation: z
            .record(z.string(), z.string())
            .catch({}),
        carlRelationshipsPresetForInterventionCreation: z
            .record(
                z.string(),
                z.object({
                    id: z.string(),
                    type: z.string(),
                })
            )
            .catch({}),
        carlAttributesPresetForDemandeInterventionCreation: z
            .record(z.string(), z.string())
            .catch({}),
        carlRelationshipsPresetForDemandeInterventionCreation: z
            .record(
                z.string(),
                z.object({
                    id: z.string(),
                    type: z.string(),
                })
            )
            .catch({}),

        photoAvantInterventionDoctypeId: z.string(),
        photoApresInterventionDoctypeId: z.string(),
        carlClient: CarlClientDtoSchema,
        woViewUrlSubPath: z.string().optional(),
        mrViewUrlSubPath: z.string().optional(),
        boxViewUrlSubPath: z.string().optional(),
        materialViewUrlSubPath: z.string().optional(),
        includeEquipementsChildrenWithType: z.string().array(),
    })

export interface CarlConfigurationInterventionDto
    extends GmaoConfigurationInterventionDto,
        Dto<CarlConfigurationIntervention>,
        z.infer<typeof CarlConfigurationInterventionDtoSchema> {
    interventionCostCenterIdsFilter: Array<string>
    costCenterIdForInterventionCreation?: string
    useEquipmentCostCenterForInterventionCreation: boolean
    directions: Array<string>
    carlAttributesPresetForInterventionCreation: {
        [key: string]: string
    }
    carlRelationshipsPresetForInterventionCreation: {
        [key: string]: { id: string; type: string }
    }
    carlAttributesPresetForDemandeInterventionCreation: {
        [key: string]: string
    }
    carlRelationshipsPresetForDemandeInterventionCreation: {
        [key: string]: { id: string; type: string }
    }
    photoAvantInterventionDoctypeId: string
    photoApresInterventionDoctypeId: string
    carlClient: CarlClientDto
    woViewUrlSubPath?: string
    mrViewUrlSubPath?: string
    boxViewUrlSubPath?: string
    materialViewUrlSubPath?: string
    includeEquipementsChildrenWithType: Array<string>
}
