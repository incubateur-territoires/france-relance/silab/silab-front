import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { CarlConfigurationIntervention } from './CarlConfigurationIntervention'
import {
    CreateCarlConfigurationInterventionDto,
    CreateCarlConfigurationInterventionDtoSchema,
} from './CreateCarlConfigurationInterventionDto'
import { EditCarlConfigurationInterventionDto } from './EditCarlConfigurationInterventionDto'
import { ZodError } from 'zod'

interface CarlConfigurationInterventionMapper
    extends Omit<
        EntityMapper<CarlConfigurationIntervention>,
        'toDto' | 'toDtoArray'
    > {
    toCreateDto: (
        partialEntity: Partial<CarlConfigurationIntervention>
    ) => CreateCarlConfigurationInterventionDto
    toEditDto: (
        partialEntity: Partial<CarlConfigurationIntervention>
    ) => EditCarlConfigurationInterventionDto
}

export const carlConfigurationInterventionMapper: CarlConfigurationInterventionMapper =
    {
        toCreateDto: function (
            carlConfigurationInterventionPartial: Partial<CarlConfigurationIntervention>
        ): CreateCarlConfigurationInterventionDto {
            try {
                return CreateCarlConfigurationInterventionDtoSchema.parse({
                    notes: carlConfigurationInterventionPartial.notes,
                    title: carlConfigurationInterventionPartial.title,
                    interventionCostCenterIdsFilter:
                        carlConfigurationInterventionPartial.interventionCostCenterIdsFilter,
                    costCenterIdForInterventionCreation:
                        carlConfigurationInterventionPartial.costCenterIdForInterventionCreation,
                    useEquipmentCostCenterForInterventionCreation:
                        carlConfigurationInterventionPartial.useEquipmentCostCenterForInterventionCreation,
                    directions: carlConfigurationInterventionPartial.directions,
                    actionTypesMap:
                        carlConfigurationInterventionPartial.actionTypesMap,
                    carlAttributesPresetForInterventionCreation:
                        carlConfigurationInterventionPartial.carlAttributesPresetForInterventionCreation,
                    carlRelationshipsPresetForInterventionCreation:
                        carlConfigurationInterventionPartial.carlRelationshipsPresetForInterventionCreation,
                    carlAttributesPresetForDemandeInterventionCreation:
                        carlConfigurationInterventionPartial.carlAttributesPresetForDemandeInterventionCreation,
                    carlRelationshipsPresetForDemandeInterventionCreation:
                        carlConfigurationInterventionPartial.carlRelationshipsPresetForDemandeInterventionCreation,
                    photoAvantInterventionDoctypeId:
                        carlConfigurationInterventionPartial.photoAvantInterventionDoctypeId,
                    photoApresInterventionDoctypeId:
                        carlConfigurationInterventionPartial.photoApresInterventionDoctypeId,
                    carlClient:
                        carlConfigurationInterventionPartial.carlClient?.[
                            '@id'
                        ],
                    woViewUrlSubPath:
                        carlConfigurationInterventionPartial.woViewUrlSubPath,
                    mrViewUrlSubPath:
                        carlConfigurationInterventionPartial.mrViewUrlSubPath,
                    boxViewUrlSubPath:
                        carlConfigurationInterventionPartial.boxViewUrlSubPath,
                    materialViewUrlSubPath:
                        carlConfigurationInterventionPartial.materialViewUrlSubPath,
                    includeEquipementsChildrenWithType:
                        carlConfigurationInterventionPartial.includeEquipementsChildrenWithType,
                })
            } catch (error) {
                if (error instanceof ZodError) {
                    throw new IncompleteEntity(
                        'La configuration Carl ne possède pas tout les attributs requis.',
                        error
                    )
                }
                throw error
            }
        },
        toEditDto: function (
            carlConfigurationInterventionPartial: Partial<CarlConfigurationIntervention>
        ): EditCarlConfigurationInterventionDto {
            return this.toCreateDto(carlConfigurationInterventionPartial)
        },
    }
