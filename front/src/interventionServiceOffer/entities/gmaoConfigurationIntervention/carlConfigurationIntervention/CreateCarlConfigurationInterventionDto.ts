import { CreateDto } from '@/shared/api/entities/Dtos'
import { CarlConfigurationIntervention } from './CarlConfigurationIntervention'
import {
    CarlConfigurationInterventionDto,
    CarlConfigurationInterventionDtoSchema,
} from './CarlConfigurationInterventionDto'
import { z } from 'zod'

export const CreateCarlConfigurationInterventionDtoSchema =
    CarlConfigurationInterventionDtoSchema.omit({
        id: true,
        environnement: true,
    }).extend({
        carlClient: z.string(),
    })

export interface CreateCarlConfigurationInterventionDto
    extends CreateDto<CarlConfigurationIntervention>,
        Omit<
            CarlConfigurationInterventionDto,
            'id' | 'carlClient' | 'environnement'
        >,
        z.infer<typeof CreateCarlConfigurationInterventionDtoSchema> {
    carlClient: string
}
