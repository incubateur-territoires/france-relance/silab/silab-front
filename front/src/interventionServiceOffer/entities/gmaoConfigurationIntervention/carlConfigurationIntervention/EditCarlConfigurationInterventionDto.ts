import { EditDto } from '@/shared/api/entities/Dtos'
import { CarlConfigurationIntervention } from './CarlConfigurationIntervention'
import { CarlConfigurationInterventionDto } from './CarlConfigurationInterventionDto'

export interface EditCarlConfigurationInterventionDto
    extends EditDto<CarlConfigurationIntervention>,
        Omit<
            CarlConfigurationInterventionDto,
            'id' | 'carlClient' | 'environnement'
        > {
    carlClient: string
}
