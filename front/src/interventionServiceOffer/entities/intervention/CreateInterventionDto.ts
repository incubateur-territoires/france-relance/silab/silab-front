import { InterventionDtoSchema } from '@/interventionServiceOffer/entities/intervention/InterventionDto'
import { CreateDto } from '@/shared/api/entities/Dtos'
import { Intervention } from './Intervention'
import { z } from 'zod'
import { EquipementDtoSchema } from '../equipement/EquipementDto'

export const CreateInterventionDtoSchema = InterventionDtoSchema.pick({
    title: true,
    priority: true,
    description: true,
    actionType: true,
    latitude: true,
    longitude: true,
    materielConsomme: true,
    dateDeDebut: true,
    dateDeFin: true,
}).extend({
    relatedEquipement: EquipementDtoSchema.pick({ id: true }).optional(),
})

export interface CreateInterventionDto
    extends CreateDto<Intervention>,
        z.infer<typeof CreateInterventionDtoSchema> {}
