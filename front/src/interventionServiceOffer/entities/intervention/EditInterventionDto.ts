import { EditDto } from '@/shared/api/entities/Dtos'
import { Intervention } from './Intervention'
import { z } from 'zod'
import { CreateInterventionDtoSchema } from './CreateInterventionDto'

export const EditInterventionDtoSchema = CreateInterventionDtoSchema

export interface EditInterventionDto
    extends EditDto<Intervention>,
        z.infer<typeof EditInterventionDtoSchema> {}
