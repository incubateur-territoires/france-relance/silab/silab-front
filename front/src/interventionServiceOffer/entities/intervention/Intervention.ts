import {
    PrimaryAction,
    SecondaryAction,
    getItemThumbnailUrl,
} from '@/shared/item/entities/ListableItem'
import { GeolocalisableItem } from '@/shared/item/entities/GeolocalisableItem'
import {
    InterventionDto,
    InterventionPriority,
} from '@/interventionServiceOffer/entities/intervention/InterventionDto'

import { SilabColor } from '@/plugins/vuetify'
import { LatLngLiteral } from 'leaflet'
import { RouteLocationRaw } from 'vue-router'
import { getAnteriorityString } from '@/shared/helpers/dates'
import { SilabImage } from '@/shared/document/entities/SilabImage'
import { Equipement } from '../equipement/Equipement'
import { isDefined, isUndefined } from '@/shared/helpers/types'
import { GmaoActeur } from '@/shared/gmaoActeur/GmaoActeur'
import { Document } from '@/shared/document/entities/Document'
import { InterventionCycleEntity } from '../interventionCycleEntity/InterventionCycleEntity'
import { interventionMapper } from './InterventionMapper'
import { toRaw } from 'vue'
import { Clonable } from '@/shared/helpers/lifeCycle'
import { InterventionHistorisedStatus } from './status/InterventionHistorisedStatus'
import { InterventionServiceOfferItem } from '../item/InterventionServiceOfferItem'
import { ItemLists } from '@/interventionServiceOffer/composables/equipement/useItemLists'
import { Equatable } from '@/shared/helpers/objects'

export class Intervention
    implements
        InterventionServiceOfferItem,
        GeolocalisableItem,
        InterventionCycleEntity,
        Clonable<Intervention>,
        Equatable<Intervention>
{
    id: string
    title: string
    priority: InterventionPriority
    coordinates?: LatLngLiteral
    description?: string
    imagesAvant: SilabImage[]
    imagesApres: SilabImage[]
    actionType?: InterventionDto['actionType']
    relatedEquipement?: Equipement
    createdAt: Date
    address?: string
    createdBy: GmaoActeur
    dateDeDebut: Date
    dateDeFin: Date
    code: string
    currentStatus?: InterventionHistorisedStatus
    gmaoObjectViewUrl?: string
    demandeInterventionIds?: string[]
    documentsJoints: Document[]
    intervenantsAffectes: GmaoActeur[]
    materielConsomme?: string
    private itemRoute?: RouteLocationRaw
    private itemSecondaryAction?: SecondaryAction
    private itemPrimaryAction?: PrimaryAction

    private static HEURE_DE_DEBUT_PAR_DEFAUT = 8
    private static HEURE_DE_FIN_PAR_DEFAUT = 17

    protected static priorityColors: {
        [key in InterventionPriority]: keyof typeof SilabColor
    } = {
        urgent: 'error',
        important: 'warning',
        normal: 'primary',
        bas: 'disabled',
    }
    public constructor(interventionDto: InterventionDto) {
        this.id = interventionDto.id
        this.title = interventionDto.title
        this.priority = interventionDto.priority
        this.description = interventionDto.description
        this.imagesAvant = SilabImage.fromArray(interventionDto.imagesAvant)
        this.imagesApres = SilabImage.fromArray(interventionDto.imagesApres)
        // il nous faut lat et lng définies pour pouvoir créer des coordonées
        this.coordinates =
            interventionDto.latitude === undefined ||
            interventionDto.longitude === undefined
                ? undefined
                : {
                      lat: interventionDto.latitude,
                      lng: interventionDto.longitude,
                  }
        this.actionType = interventionDto.actionType
        this.relatedEquipement = isDefined(interventionDto.relatedEquipement)
            ? new Equipement(interventionDto.relatedEquipement)
            : undefined
        this.createdAt = new Date(interventionDto.createdAt)
        this.address = interventionDto.address
        this.createdBy = new GmaoActeur(interventionDto.createdBy)
        this.dateDeDebut = new Date(interventionDto.dateDeDebut)
        this.dateDeFin = new Date(interventionDto.dateDeFin)
        this.code = interventionDto.code
        this.currentStatus =
            interventionDto.currentStatus &&
            new InterventionHistorisedStatus(interventionDto.currentStatus)
        this.gmaoObjectViewUrl = interventionDto.gmaoObjectViewUrl
        this.demandeInterventionIds = interventionDto.demandeInterventionIds
        this.documentsJoints = interventionDto.documentsJoints.map(
            (documentDto) => new Document(documentDto)
        )
        this.intervenantsAffectes = interventionDto.intervenantsAffectes.map(
            (gmaoActeurDto) => new GmaoActeur(gmaoActeurDto)
        )
        this.materielConsomme = interventionDto.materielConsomme
    }

    public isClosable(): boolean {
        if (isUndefined(this.currentStatus)) {
            return false
        }

        return 'awaiting_realisation' === this.currentStatus.code
    }

    public hasLocation(): boolean {
        return isDefined(this.getCoordinates())
    }

    public hasImage(): boolean {
        return [...this.imagesApres, ...this.imagesAvant].length > 0
    }

    public static getPriorityColor(priority: InterventionPriority): string {
        return Intervention.priorityColors[priority]
    }

    public getItemId() {
        return this.id
    }

    public getItemType(): keyof ItemLists {
        return 'interventions'
    }

    public getItemTitle() {
        return `${this.code} ${this.title}`
    }

    public getItemDescription() {
        const itemDescriptionElements = []

        if (isDefined(this.relatedEquipement?.label)) {
            itemDescriptionElements.push(
                this.relatedEquipement.label.toLocaleUpperCase()
            )
        }

        if (isDefined(this.description) && this.description.length > 0) {
            itemDescriptionElements.push(this.description)
        }

        return itemDescriptionElements.join(' - ')
    }
    public getItemImageUrl() {
        // On donne par défault la première photo avant, sinon la première photo après
        if (this.imagesAvant.length > 0) {
            return this.imagesAvant[0].url
        }

        return this.imagesApres.at(0)?.url
    }

    public getItemThumbnailUrl = getItemThumbnailUrl

    public getItemChips() {
        if (this.priority === InterventionPriority.Normal) {
            return []
        }

        return [
            {
                label: this.priority,
                color: Intervention.getPriorityColor(this.priority),
            },
        ]
    }

    public isRequiringAttention() {
        return false
    }

    public getItemTemporalData() {
        return getAnteriorityString(this.createdAt)
    }
    public getItemRoute() {
        return this.itemRoute
    }
    public setItemRoute(itemRoute: RouteLocationRaw | undefined) {
        this.itemRoute = itemRoute
        return this
    }
    public primaryAction() {
        this.itemPrimaryAction?.()
    }

    public setItemPrimaryAction(primaryAction: PrimaryAction) {
        this.itemPrimaryAction = primaryAction
        return this
    }
    public getItemSecondaryAction() {
        return this.itemSecondaryAction
    }

    public setItemSecondaryAction(secondaryAction: SecondaryAction) {
        this.itemSecondaryAction = secondaryAction
        return this
    }
    public getCoordinates() {
        return this.coordinates ?? this.relatedEquipement?.getCoordinates()
    }

    public getToutesLesImagesCatégorisées(): Array<{
        image: SilabImage
        catégorie: string
    }> {
        return [
            ...this.imagesAvant.map((image) => {
                return {
                    image: image,
                    catégorie: 'photo avant',
                }
            }),
            ...this.imagesApres.map((image) => {
                return {
                    image: image,
                    catégorie: 'photo après',
                }
            }),
        ]
    }

    public static fromArray(
        interventionDtos: InterventionDto[]
    ): Intervention[] {
        return interventionDtos.map(
            (interventionDto) => new Intervention(interventionDto)
        )
    }

    public clone(): Intervention {
        return new Intervention(
            window.structuredClone(interventionMapper.toDto(toRaw(this))) //Attention: le toRaw est important, les structuredClone ne fonctionne pas avec des valeures issues de Ref.value
        )
    }

    public equals(other: Intervention): boolean {
        const currentInterventionDto = interventionMapper.toDto(this)
        const otherInterventionDto = interventionMapper.toDto(other)

        return (
            JSON.stringify(currentInterventionDto) ===
            JSON.stringify(otherInterventionDto)
        )
    }

    public static getDateParDefautPourCreationNouvelleIntervention(): {
        dateDeDebut: Date
        dateDeFin: Date
    } {
        const dateDeDebut = new Date()
        const dateDeFin = new Date()
        dateDeDebut.setHours(Intervention.HEURE_DE_DEBUT_PAR_DEFAUT, 0, 0, 0)
        dateDeFin.setHours(Intervention.HEURE_DE_FIN_PAR_DEFAUT, 0, 0, 0)
        return { dateDeDebut, dateDeFin }
    }
}
