import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { Intervention } from '@/interventionServiceOffer/entities/intervention/Intervention'
import { InterventionDtoSchema } from './InterventionDto'
import { CreateInterventionDto } from '@/interventionServiceOffer/entities/intervention/CreateInterventionDto'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { InterventionFilters } from '@/interventionServiceOffer/entities/intervention/filters/InterventionFilters'
import { InterventionOrderBy } from '../orderBy/InterventionOrderBy'
import { SilabImage } from '@/shared/document/entities/SilabImage'
import { EditInterventionDto } from './EditInterventionDto'
import { SilabImageDtoSchema } from '@/shared/document/entities/SilabImageDto'
import { NewInterventionHistorizedStatusDto } from './status/NewInterventionStatusDto'
import { InterventionHistorisedStatusDtoSchema } from './status/InterventionHistorisedStatusDto'
import { Equipement } from '@/interventionServiceOffer/entities/equipement/Equipement'
import { InterventionCoordinates } from './InterventionCoordinates'
import { InterventionCoordinatesDtoSchema } from './InterventionCoordinatesDto'
import { ResultPage } from '@/shared/helpers/backend'
import { InterventionHistorisedStatus } from './status/InterventionHistorisedStatus'
import { InterventionPourSoldage } from './pourSoldage/InterventionPourSoldage'
import {
    UpdateInterventionPourSoldageDto,
    UpdateInterventionPourSoldageDtoSchema,
} from './pourSoldage/UpdateInterventionPourSoldageDto'

export type InterventionImageType = 'images-avant' | 'images-apres'

export class InterventionApi extends AzureTokenAccessApi {
    async getAll(
        serviceOfferId: ServiceOffer['id'],
        filters: InterventionFilters,
        orderBy: InterventionOrderBy,
        page: number,
        abortSignal?: AbortSignal
    ): Promise<ResultPage<Intervention>> {
        const queryParameters = filters
            .toQueryParameters()
            .set('orderBy', orderBy.getOrderByValues())
            .set('page', page.toString())
            .set('includeDemandes', false)

        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions`,
            queryParameters,
            abortSignal
        )

        const interventionsDto = InterventionDtoSchema.array().parse(
            await rawResponse.json()
        )

        // On veux instancier les interventions avec leur classes finales/filles, on utilise pour cela le factory sur chaque Dto reçu
        return {
            data: interventionsDto.map((interventionDto) => {
                return new Intervention(interventionDto)
            }),
            maxResults: Number(rawResponse.headers.get('X-Total-Count')),
        }
    }

    async getAllCoordinates(
        serviceOfferId: ServiceOffer['id'],
        filters: InterventionFilters,
        abortSignal?: AbortSignal
    ): Promise<InterventionCoordinates[]> {
        const queryParameters = filters.toQueryParameters()

        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions-coordinates`,
            queryParameters,
            abortSignal
        )

        const interventionsCoordinatesDto =
            InterventionCoordinatesDtoSchema.array().parse(
                await rawResponse.json()
            )

        return interventionsCoordinatesDto.map((interventionCoordinatesDto) => {
            return new InterventionCoordinates(interventionCoordinatesDto)
        })
    }

    async create(
        serviceOfferId: ServiceOffer['id'],
        newIntervention: CreateInterventionDto
    ) {
        const rawResponse = await this.postBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions`,
            JSON.stringify(newIntervention)
        )

        const interventionDto = InterventionDtoSchema.parse(
            await rawResponse.json()
        )

        return new Intervention(interventionDto)
    }

    async close(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id']
    ): Promise<InterventionHistorisedStatus> {
        const rawResponse = await this.postBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions/${interventionId}/statuses`,
            JSON.stringify(<NewInterventionHistorizedStatusDto>{
                code: 'closed',
            })
        )
        const interventionStatusDto =
            InterventionHistorisedStatusDtoSchema.parse(
                await rawResponse.json()
            )

        return new InterventionHistorisedStatus(interventionStatusDto)
    }

    async get(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        abortSignal?: AbortSignal
    ) {
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions/${interventionId}`,
            undefined, //note: c'est pas tout à fait comme ça pour les includes coté includeAncetres des eqpt (includeDemandes=true est fait directement dans le back ici)
            abortSignal
        )

        const interventionApiResponse = InterventionDtoSchema.parse(
            await rawResponse.json()
        )

        return new Intervention(interventionApiResponse)
    }

    async update(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        updatedIntervention: EditInterventionDto
    ) {
        const rawResponse = await this.putBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions/${interventionId}`,
            JSON.stringify(updatedIntervention)
        )

        const interventionApiResponse = InterventionDtoSchema.parse(
            await rawResponse.json()
        )

        return new Intervention(interventionApiResponse)
    }

    async addImage(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        imageType: InterventionImageType,
        imageBase64: string
    ): Promise<SilabImage> {
        const rawResponse = await this.postBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions/${interventionId}/${imageType}`,
            JSON.stringify({
                base64: imageBase64,
            })
        )

        const addedImageDto = SilabImageDtoSchema.parse(
            await rawResponse.json()
        )

        return new SilabImage(addedImageDto)
    }

    async addImages(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        imagesB64: string[],
        imageType: InterventionImageType
    ): Promise<SilabImage[]> {
        const addedImages: SilabImage[] = []
        for (const imageB64 of imagesB64) {
            addedImages.push(
                await this.addImage(
                    serviceOfferId,
                    interventionId,
                    imageType,
                    imageB64
                )
            )
        }
        return addedImages
    }

    async deleteImage(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        imageType: InterventionImageType,
        id: SilabImage['id']
    ) {
        await this.deleteBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions/${interventionId}/${imageType}/${id}`
        )
    }

    async deleteImages(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        imagesId: SilabImage['id'][],
        imageType: InterventionImageType
    ) {
        for (const imageId of imagesId) {
            await this.deleteImage(
                serviceOfferId,
                interventionId,
                imageType,
                imageId
            )
        }
    }

    async getAllByEquipement(
        serviceOfferId: ServiceOffer['id'],
        equipementId: Equipement['id'],
        filters: InterventionFilters,
        orderBy: InterventionOrderBy,
        abortSignal?: AbortSignal
    ): Promise<Intervention[]> {
        const queryParameters = filters
            .toQueryParameters()
            .set('orderBy', orderBy.getOrderByValues())

        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/equipements/${equipementId}/interventions`,
            queryParameters,
            abortSignal
        )

        const interventionApiResponse = InterventionDtoSchema.array().parse(
            await rawResponse.json()
        )

        return interventionApiResponse.map((interventionDto) => {
            return new Intervention(interventionDto)
        })
    }

    async getStatusHistory(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        abortSignal?: AbortSignal
    ) {
        const statusHistoryResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions/${interventionId}/statusHistory`,
            undefined,
            abortSignal
        )

        const statusHistoryDto =
            InterventionHistorisedStatusDtoSchema.array().parse(
                await statusHistoryResponse.json()
            )

        // On veux instancier les interventions avec leur classes finales/filles, on utilise pour cela le factory sur chaque Dto reçu
        return statusHistoryDto.map((interventionHistorisedStatusDto) => {
            return new InterventionHistorisedStatus(
                interventionHistorisedStatusDto
            )
        })
    }

    async putInterventionPourSoldage(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        interventionPourSoldage: UpdateInterventionPourSoldageDto
    ) {
        const rawResponse = await this.putBackendData(
            `intervention-service-offers/${serviceOfferId}/interventions/${interventionId}/attributs-lies-au-soldage`,
            JSON.stringify(interventionPourSoldage)
        )

        const interventionPourSoldageDto =
            UpdateInterventionPourSoldageDtoSchema.parse(
                await rawResponse.json()
            )

        return new InterventionPourSoldage(interventionPourSoldageDto)
    }
}
