import { z } from 'zod'
export const InterventionCoordinatesDtoSchema = z.object({
    id: z.string(),
    label: z.string(),
    latitude: z.number(),
    longitude: z.number(),
})

export type InterventionCoordinatesDto = z.infer<
    typeof InterventionCoordinatesDtoSchema
>
