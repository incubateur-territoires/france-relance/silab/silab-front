import { Intervention } from './Intervention'
import { Dto } from '@/shared/api/entities/Dtos'
import {
    SilabImageDto,
    SilabImageDtoSchema,
} from '@/shared/document/entities/SilabImageDto'
import { EquipementDto, EquipementDtoSchema } from '../equipement/EquipementDto'
import {
    GmaoActeurDto,
    GmaoActeurDtoSchema,
} from '@/shared/gmaoActeur/GmaoActeurDto'
import {
    DocumentDto,
    DocumentDtoSchema,
} from '@/shared/document/entities/DocumentDto'
import { z } from 'zod'
import {
    InterventionHistorisedStatusDto,
    InterventionHistorisedStatusDtoSchema,
} from './status/InterventionHistorisedStatusDto'

export enum InterventionPriority {
    Urgent = 'urgent',
    Important = 'important',
    Normal = 'normal',
    Bas = 'bas',
}

export const InterventionDtoSchema = z.object({
    id: z.string(),
    title: z.string(),
    priority: z.nativeEnum(InterventionPriority),
    description: z.string().optional(),
    imagesAvant: SilabImageDtoSchema.array(),
    imagesApres: SilabImageDtoSchema.array(),
    actionType: z
        .object({
            id: z.string(),
            label: z.string(),
        })
        .optional(),
    relatedEquipement: EquipementDtoSchema.optional(), //EquipementDto
    latitude: z.number().optional(),
    longitude: z.number().optional(),
    address: z.string().optional(),
    createdAt: z.string(),
    createdBy: GmaoActeurDtoSchema,
    dateDeDebut: z.string(),
    dateDeFin: z.string(),
    code: z.string(),
    currentStatus: InterventionHistorisedStatusDtoSchema.optional(),
    gmaoObjectViewUrl: z.string().optional(),
    demandeInterventionIds: z.string().array().optional(),
    documentsJoints: DocumentDtoSchema.array(),
    intervenantsAffectes: GmaoActeurDtoSchema.array(),
    materielConsomme: z.string().optional(),
})

// export type InterventionDto = z.infer<typeof InterventionDtoSchema>
// amelioration: abandonner les interfaces natives
export interface InterventionDto
    extends Dto<Intervention>,
        z.infer<typeof InterventionDtoSchema> {
    id: string
    title: string
    priority: InterventionPriority
    description?: string
    imagesAvant: SilabImageDto[]
    imagesApres: SilabImageDto[]
    actionType?: { id: string; label: string }
    relatedEquipement?: EquipementDto
    latitude?: number
    longitude?: number
    address?: string
    createdAt: string
    createdBy: GmaoActeurDto
    dateDeDebut: string
    dateDeFin: string
    code: string
    currentStatus?: InterventionHistorisedStatusDto
    gmaoObjectViewUrl?: string
    demandeInterventionIds?: string[]
    documentsJoints: DocumentDto[]
    intervenantsAffectes: GmaoActeurDto[]
    materielConsomme?: string
}
