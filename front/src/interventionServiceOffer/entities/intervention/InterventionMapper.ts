import {
    EditInterventionDto,
    EditInterventionDtoSchema,
} from './EditInterventionDto'
import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { Intervention } from './Intervention'
import { InterventionDto, InterventionDtoSchema } from './InterventionDto'
import {
    CreateInterventionDto,
    CreateInterventionDtoSchema,
} from './CreateInterventionDto'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { silabImageMapper } from '@/shared/document/entities/SilabImageMapper'
import { Dto } from '@/shared/api/entities/Dtos'
import { gmaoActeurMapper } from '@/shared/gmaoActeur/GmaoActeurMapper'
import { isDefined } from '@/shared/helpers/types'
import { equipementMapper } from '../equipement/EquipementMapper'
import { documentMapper } from '@/shared/document/entities/DocumentMapper'
import { ZodError } from 'zod'

interface InterventionMapper extends EntityMapper<Intervention> {
    toDto: (partialIntervention: Partial<Intervention>) => InterventionDto
    toEditDto: (partialEntity: Partial<Intervention>) => EditInterventionDto
    toCreateDto: (partialEntity: Partial<Intervention>) => CreateInterventionDto
    toDtoArray: (
        partialEquipements: Partial<Intervention>[]
    ) => InterventionDto[]
    fromDtoArray: (
        interventionDtos: Dto<Intervention>[] | Dto<Intervention[]>
    ) => Intervention[]
    fromUnsafeDtoArray: (interventionDtos: unknown) => Intervention[]
}

export const interventionMapper: InterventionMapper = {
    toDto(partialIntervention: Partial<Intervention>): InterventionDto {
        try {
            return InterventionDtoSchema.parse({
                id: partialIntervention.id,
                title: partialIntervention.title,
                priority: partialIntervention.priority,
                description: partialIntervention.description,
                imagesAvant: isDefined(partialIntervention.imagesAvant)
                    ? silabImageMapper.toDtoArray(
                          partialIntervention.imagesAvant
                      )
                    : partialIntervention.imagesAvant,
                imagesApres: isDefined(partialIntervention.imagesApres)
                    ? silabImageMapper.toDtoArray(
                          partialIntervention.imagesApres
                      )
                    : partialIntervention.imagesApres,
                latitude: partialIntervention.coordinates?.lat,
                longitude: partialIntervention.coordinates?.lng,
                actionType: partialIntervention.actionType,
                relatedEquipement: isDefined(
                    partialIntervention.relatedEquipement
                )
                    ? equipementMapper.toDto(
                          partialIntervention.relatedEquipement
                      )
                    : undefined,
                createdAt: partialIntervention.createdAt?.toISOString(),
                address: partialIntervention.address,
                createdBy: isDefined(partialIntervention.createdBy)
                    ? gmaoActeurMapper.toDto(partialIntervention.createdBy)
                    : undefined,
                dateDeDebut: partialIntervention.dateDeDebut?.toISOString(),
                dateDeFin: partialIntervention.dateDeFin?.toISOString(),
                code: partialIntervention.code,
                currentStatus: partialIntervention.currentStatus?.toDto(),
                gmaoObjectViewUrl: partialIntervention.gmaoObjectViewUrl,
                demandeInterventionIds:
                    partialIntervention.demandeInterventionIds,
                documentsJoints: isDefined(partialIntervention.documentsJoints)
                    ? partialIntervention.documentsJoints.map(
                          documentMapper.toDto
                      )
                    : undefined,
                intervenantsAffectes: isDefined(
                    partialIntervention.intervenantsAffectes
                )
                    ? gmaoActeurMapper.toDtoArray(
                          partialIntervention.intervenantsAffectes
                      )
                    : undefined,
                materielConsomme: partialIntervention.materielConsomme,
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'intervention fournit ne possède pas tous les attributs requis.",
                    error
                )
            }
            throw error
        }
    },
    toEditDto(partialIntervention: Partial<Intervention>): EditInterventionDto {
        try {
            return EditInterventionDtoSchema.parse({
                title: partialIntervention.title,
                priority: partialIntervention.priority,
                description: partialIntervention.description,
                actionType: partialIntervention.actionType,
                latitude: partialIntervention.coordinates?.lat,
                longitude: partialIntervention.coordinates?.lng,
                relatedEquipement: partialIntervention.relatedEquipement,
                materielConsomme: partialIntervention.materielConsomme,
                dateDeDebut: partialIntervention.dateDeDebut?.toISOString(),
                dateDeFin: partialIntervention.dateDeFin?.toISOString(),
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'intervention fournit ne possède pas tous les attributs requis.",
                    error
                )
            }
            throw error
        }
    },

    toCreateDto(
        partialIntervention: Partial<Intervention>
    ): CreateInterventionDto {
        try {
            return CreateInterventionDtoSchema.parse({
                title: partialIntervention.title,
                priority: partialIntervention.priority,
                description: partialIntervention.description,
                actionType: partialIntervention.actionType,
                latitude: partialIntervention.coordinates?.lat,
                longitude: partialIntervention.coordinates?.lng,
                relatedEquipement: partialIntervention.relatedEquipement,
                materielConsomme: partialIntervention.materielConsomme,
                dateDeDebut: partialIntervention.dateDeDebut?.toISOString(),
                dateDeFin: partialIntervention.dateDeFin?.toISOString(),
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'intervention fournit ne possède pas tous les attributs requis.",
                    error
                )
            }
            throw error
        }
    },

    toDtoArray(
        partialInterventions: Partial<Intervention>[]
    ): InterventionDto[] {
        return partialInterventions.map((partialIntervention) =>
            this.toDto(partialIntervention)
        )
    },

    fromDtoArray: function (
        interventionDtos: Dto<Intervention>[] | Dto<Intervention[]>
    ): Intervention[] {
        return Intervention.fromArray(interventionDtos as InterventionDto[]) // amelioration : peut-on faire + propre ?
    },

    fromUnsafeDtoArray: function (interventionDtos: unknown): Intervention[] {
        return Intervention.fromArray(
            InterventionDtoSchema.array().parse(interventionDtos)
        )
    },
}
