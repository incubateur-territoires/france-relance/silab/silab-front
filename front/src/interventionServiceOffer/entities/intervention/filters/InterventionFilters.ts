import {
    EquipementDto,
    EquipementDtoSchema,
} from '../../equipement/EquipementDto'
import { ObjectFilters } from '@/shared/filters/entities/ObjectFilters'
import { GmaoActeur } from '@/shared/gmaoActeur/GmaoActeur'
import { InterventionPriority } from '../InterventionDto'
import { GmaoActeurDtoSchema } from '@/shared/gmaoActeur/GmaoActeurDto'
import { z } from 'zod'

export const InterventionFiltersDtoSchema = z.object({
    statuses: z.string().array(),
    'relatedEquipement.id': EquipementDtoSchema.shape.id.array(),
    'intervenantsAffectes.id': GmaoActeurDtoSchema.array(),
    priority: z.nativeEnum(InterventionPriority).array(),
    'dateDeDebut[before]': z.string().optional(),
    'dateDeDebut[after]': z.string().optional(),
    'dateDeFin[before]': z.string().optional(),
    'dateDeFin[after]': z.string().optional(),
})

export type InterventionFiltersDto = z.infer<
    typeof InterventionFiltersDtoSchema
>

export class InterventionFilters extends ObjectFilters<{
    statuses: string[]
    'relatedEquipement.id': EquipementDto['id'][]
    'intervenantsAffectes.id': GmaoActeur[]
    priority: InterventionPriority[]
    'dateDeDebut[before]': Date | undefined
    'dateDeDebut[after]': Date | undefined
    'dateDeFin[before]': Date | undefined
    'dateDeFin[after]': Date | undefined
}> {
    public getDefaultFilters() {
        return {
            statuses: ['awaiting_realisation'],
            'relatedEquipement.id': [],
            'intervenantsAffectes.id': [],
            priority: [],
            'dateDeDebut[before]': undefined,
            'dateDeDebut[after]': undefined,
            'dateDeFin[before]': undefined,
            'dateDeFin[after]': undefined,
        }
    }
}
