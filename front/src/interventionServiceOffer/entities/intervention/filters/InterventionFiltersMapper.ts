import { gmaoActeurMapper } from '@/shared/gmaoActeur/GmaoActeurMapper'
import {
    InterventionFilters,
    InterventionFiltersDto,
} from './InterventionFilters'
import { isDefined } from '@/shared/helpers/types'

export const interventionFiltersMapper = {
    toDto: (
        interventionFilters: InterventionFilters
    ): InterventionFiltersDto => {
        return {
            statuses: interventionFilters.filters.statuses,
            'relatedEquipement.id':
                interventionFilters.filters['relatedEquipement.id'],
            'intervenantsAffectes.id': gmaoActeurMapper.toDtoArray(
                interventionFilters.filters['intervenantsAffectes.id']
            ),
            priority: interventionFilters.filters.priority,
            'dateDeDebut[before]': isDefined(
                interventionFilters.filters['dateDeDebut[before]']
            )
                ? interventionFilters.filters[
                      'dateDeDebut[before]'
                  ].toISOString()
                : undefined,
            'dateDeDebut[after]': isDefined(
                interventionFilters.filters['dateDeDebut[after]']
            )
                ? interventionFilters.filters[
                      'dateDeDebut[after]'
                  ].toISOString()
                : undefined,
            'dateDeFin[before]': isDefined(
                interventionFilters.filters['dateDeFin[before]']
            )
                ? interventionFilters.filters['dateDeFin[before]'].toISOString()
                : undefined,
            'dateDeFin[after]': isDefined(
                interventionFilters.filters['dateDeFin[after]']
            )
                ? interventionFilters.filters['dateDeFin[after]'].toISOString()
                : undefined,
        }
    },
    fromDto(
        interventionFiltersDto: InterventionFiltersDto
    ): InterventionFilters {
        return new InterventionFilters({
            statuses: interventionFiltersDto.statuses,
            'relatedEquipement.id':
                interventionFiltersDto['relatedEquipement.id'],
            'intervenantsAffectes.id': gmaoActeurMapper.fromDtoArray(
                interventionFiltersDto['intervenantsAffectes.id']
            ),
            priority: interventionFiltersDto.priority,
            'dateDeDebut[before]': isDefined(
                interventionFiltersDto['dateDeDebut[before]']
            )
                ? new Date(interventionFiltersDto['dateDeDebut[before]'])
                : undefined,
            'dateDeDebut[after]': isDefined(
                interventionFiltersDto['dateDeDebut[after]']
            )
                ? new Date(interventionFiltersDto['dateDeDebut[after]'])
                : undefined,
            'dateDeFin[before]': isDefined(
                interventionFiltersDto['dateDeFin[before]']
            )
                ? new Date(interventionFiltersDto['dateDeFin[before]'])
                : undefined,
            'dateDeFin[after]': isDefined(
                interventionFiltersDto['dateDeFin[after]']
            )
                ? new Date(interventionFiltersDto['dateDeFin[after]'])
                : undefined,
        })
    },
}
