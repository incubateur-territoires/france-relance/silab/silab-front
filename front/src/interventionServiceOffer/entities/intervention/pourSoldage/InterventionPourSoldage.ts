import { Intervention } from '../Intervention'
import { UpdateInterventionPourSoldageDto } from './UpdateInterventionPourSoldageDto'

export class InterventionPourSoldage
    implements
        Pick<Intervention, 'dateDeDebut' | 'dateDeFin' | 'materielConsomme'>
{
    dateDeDebut: Date
    dateDeFin: Date
    materielConsomme?: string
    public constructor(
        interventionPourSoldageDto: UpdateInterventionPourSoldageDto
    ) {
        this.dateDeDebut = new Date(interventionPourSoldageDto.dateDeDebut)
        this.dateDeFin = new Date(interventionPourSoldageDto.dateDeFin)
        this.materielConsomme = interventionPourSoldageDto.materielConsomme
    }
}
