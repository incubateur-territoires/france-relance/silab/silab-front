import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import {
    UpdateInterventionPourSoldageDto,
    UpdateInterventionPourSoldageDtoSchema,
} from './UpdateInterventionPourSoldageDto'
import { ZodError } from 'zod'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { InterventionPourSoldage } from './InterventionPourSoldage'

interface InterventionPourSoldageMapper
    extends EntityMapper<InterventionPourSoldage> {
    toEditDto: (
        partialInterventionPourSoldage: Partial<InterventionPourSoldage>
    ) => UpdateInterventionPourSoldageDto
}
export const interventionPourSoldageMapper: InterventionPourSoldageMapper = {
    toEditDto(
        partialInterventionPourSoldage: Partial<InterventionPourSoldage>
    ) {
        try {
            return UpdateInterventionPourSoldageDtoSchema.parse({
                dateDeDebut:
                    partialInterventionPourSoldage.dateDeDebut?.toISOString(),
                dateDeFin:
                    partialInterventionPourSoldage.dateDeFin?.toISOString(),
                materielConsomme:
                    partialInterventionPourSoldage.materielConsomme,
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "Les attributs liés au soldage de l'intervention fournit ne possèdent pas tous ceux requis.",
                    error
                )
            }
            throw error
        }
    },
}
