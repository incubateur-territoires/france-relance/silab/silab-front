import { z } from 'zod'
import { InterventionDtoSchema } from '../InterventionDto'

export const UpdateInterventionPourSoldageDtoSchema =
    InterventionDtoSchema.pick({
        dateDeDebut: true,
        dateDeFin: true,
        materielConsomme: true,
    })

//eslint-disable-next-line @typescript-eslint/no-empty-object-type
export interface UpdateInterventionPourSoldageDto
    extends z.infer<typeof UpdateInterventionPourSoldageDtoSchema> {}
