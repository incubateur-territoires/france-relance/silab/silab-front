import { ObjectHistorisedStatus } from '@/shared/objectStatus/entities/ObjectHistorisedStatus'
import { InterventionHistorisedStatusDto } from './InterventionHistorisedStatusDto'

export class InterventionHistorisedStatus extends ObjectHistorisedStatus<
    InterventionHistorisedStatusDto['code']
> {
    public constructor(
        interventionHistorisedStatusDto: InterventionHistorisedStatusDto
    ) {
        super(interventionHistorisedStatusDto)
    }
}
