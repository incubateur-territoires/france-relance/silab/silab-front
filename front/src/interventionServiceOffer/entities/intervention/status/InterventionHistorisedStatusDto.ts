import { ObjectHistorisedStatusDto } from '@/shared/objectStatus/entities/ObjectHistorisedStatusDto'
import { z } from 'zod'
export const InterventionStatusAllowedCodesDtoSchema = z.enum([
    'closed',
    'awaiting_validation',
    'awaiting_realisation',
])
export const InterventionHistorisedStatusDtoSchema = z.object({
    code: InterventionStatusAllowedCodesDtoSchema,
    label: z.string(),
    createdBy: z.string(),
    createdAt: z.string(),
    comment: z.string().optional(),
})

// export type InterventionHistorisedStatusDto = z.infer<typeof InterventionHistorisedStatusDtoSchema>
// amelioration: abandonner les interfaces natives
export interface InterventionHistorisedStatusDto
    extends ObjectHistorisedStatusDto<
            z.infer<typeof InterventionStatusAllowedCodesDtoSchema>
        >,
        z.infer<typeof InterventionHistorisedStatusDtoSchema> {}
