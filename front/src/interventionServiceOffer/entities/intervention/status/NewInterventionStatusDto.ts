import { InterventionHistorisedStatusDto } from './InterventionHistorisedStatusDto'

export type NewInterventionHistorizedStatusDto = Pick<
    InterventionHistorisedStatusDto,
    'code'
>
