import { Ref, onBeforeUnmount, ref, watch } from 'vue'

import { VolatileSecuredData } from '@/shared/helpers/lifeCycle'
import { useMsal } from '@/plugins/msal/useMsal'
import { RequestAborter } from '@/shared/api/entities/RequestAborter'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { InterventionServiceOffer } from '../interventionServiceOffer/InterventionServiceOffer'
import { isUndefined } from '@/shared/helpers/types'
import { SilabError } from '@/shared/errors/SilabError'
import { Intervention } from './Intervention'
import { InterventionApi } from './InterventionApi'
import { InterventionHistorisedStatus } from './status/InterventionHistorisedStatus'
import { DemandeIntervention } from '../demandeIntervention/DemandeIntervention'
import { DemandeInterventionApi } from '../demandeIntervention/DemandeInterventionApi'

export function useIntervention(
    interventionId: Ref<Intervention['id'] | undefined>,
    serviceOfferId: Ref<InterventionServiceOffer['id']>,
    loadStatusHistory: boolean = false,
    loadDemandesOriginales: boolean = false
) {
    const msal = useMsal()
    const userStore = useUserStore(msal)

    const interventionApi = new InterventionApi(msal)
    const interventionAbortController: RequestAborter = new RequestAborter()
    const interventionStatusHistoryAbortController: RequestAborter =
        new RequestAborter()
    const demandesInterventionsOriginalesAbortController: RequestAborter =
        new RequestAborter()
    const intervention: Ref<VolatileSecuredData<Intervention>> = ref(undefined)
    const interventionStatusHistory: Ref<
        VolatileSecuredData<InterventionHistorisedStatus[]>
    > = ref(undefined)
    const demandeInterventionApi = new DemandeInterventionApi(msal)
    const demandesInterventionsOriginales: Ref<
        VolatileSecuredData<DemandeIntervention[]>
    > = ref(undefined)

    async function loadInterventions() {
        intervention.value = undefined
        if (isUndefined(interventionId.value)) {
            return
        }
        if (
            !userStore.currentUserHasRoleForServiceOffer(
                //amélioration: piste->composable du type useCurrentUserRight(right, serviceOfferId)
                'INTERVENTION_CONSULTER_INTERVENTIONS',
                serviceOfferId.value
            )
        ) {
            intervention.value = {
                accessGranted: false,
                data: undefined,
            }
            return
        }

        interventionAbortController.abortRequestsAndRefreshAborter()
        interventionApi
            .get(
                serviceOfferId.value,
                interventionId.value,
                interventionAbortController.getSignal()
            )
            .then((fetchedIntervention) => {
                intervention.value = {
                    accessGranted: true,
                    data: fetchedIntervention,
                }
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError(
                        "Erreur lors du chargement de l'intervention'",
                        error
                    )
                }
            })
    }

    async function loadInterventionStatusHistory() {
        if (loadStatusHistory) {
            interventionStatusHistory.value = undefined
            if (isUndefined(interventionId.value)) {
                return
            }
            if (
                !userStore.currentUserHasRoleForServiceOffer(
                    //amélioration: piste->composable du type useCurrentUserRight(right, serviceOfferId)
                    'INTERVENTION_CONSULTER_INTERVENTIONS',
                    serviceOfferId.value
                )
            ) {
                interventionStatusHistory.value = {
                    accessGranted: false,
                    data: undefined,
                }
                return
            }

            interventionStatusHistoryAbortController.abortRequestsAndRefreshAborter()
            interventionApi
                .getStatusHistory(
                    serviceOfferId.value,
                    interventionId.value,
                    interventionStatusHistoryAbortController.getSignal()
                )
                .then(async (fetchedInterventionStatusHistory) => {
                    interventionStatusHistory.value = {
                        accessGranted: true,
                        data: fetchedInterventionStatusHistory,
                    }
                })
                .catch((error) => {
                    if (error.name !== 'AbortError') {
                        throw new SilabError(
                            "Erreur lors du chargement de l'historique des états de l'intervention",
                            error
                        )
                    }
                })
        }
    }

    async function loadDemandesInterventionsOriginales() {
        if (loadDemandesOriginales) {
            demandesInterventionsOriginales.value = undefined
            if (isUndefined(intervention.value)) {
                return
            }
            if (
                intervention.value.accessGranted === false ||
                !userStore.currentUserHasRoleForServiceOffer(
                    //amélioration: piste->composable du type useCurrentUserRight(right, serviceOfferId)
                    'INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS',
                    serviceOfferId.value
                )
            ) {
                demandesInterventionsOriginales.value = {
                    accessGranted: false,
                    data: undefined,
                }
                return
            }

            demandesInterventionsOriginalesAbortController.abortRequestsAndRefreshAborter()
            Promise.all(
                (intervention.value.data.demandeInterventionIds ?? []).map(
                    (id) =>
                        demandeInterventionApi.get(
                            serviceOfferId.value,
                            id,
                            demandesInterventionsOriginalesAbortController.getSignal()
                        )
                )
            )
                .then(async (fetchedDemandesInterventionsOriginales) => {
                    demandesInterventionsOriginales.value = {
                        accessGranted: true,
                        data: fetchedDemandesInterventionsOriginales,
                    }
                })
                .catch((error) => {
                    if (error.name !== 'AbortError') {
                        throw new SilabError(
                            "Erreur lors du chargement des demandes à l'origine de l'intervention'",
                            error
                        )
                    }
                })
        }
    }

    watch(
        [
            () => serviceOfferId.value,
            () => interventionId.value,
            () => {
                return userStore.currentUserHasRoleForServiceOffer(
                    'INTERVENTION_CONSULTER_INTERVENTIONS',
                    serviceOfferId.value
                )
            },
        ],
        () => {
            loadInterventions()
            loadInterventionStatusHistory()
        },
        { immediate: true }
    )

    watch(
        [
            intervention,
            () => {
                return userStore.currentUserHasRoleForServiceOffer(
                    'INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS',
                    serviceOfferId.value
                )
            },
        ],
        () => {
            loadDemandesInterventionsOriginales()
        }
    )

    onBeforeUnmount(() => {
        interventionAbortController.abortRequests()
        interventionStatusHistoryAbortController.abortRequests()
        demandesInterventionsOriginalesAbortController.abortRequests()
    })

    return {
        intervention,
        interventionStatusHistory,
        demandesInterventionsOriginales,
        reload: async () => {
            return Promise.all([
                loadInterventions(),
                loadInterventionStatusHistory(),
            ])
        },
    }
}
