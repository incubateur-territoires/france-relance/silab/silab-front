import { GmaoActeur } from '@/shared/gmaoActeur/GmaoActeur'
import { Equipement } from '../equipement/Equipement'
import { LatLngLiteral } from 'leaflet'

export interface InterventionCycleEntity {
    id: string
    title: string
    description?: string
    relatedEquipement?: Equipement
    address?: string
    coordinates?: LatLngLiteral
    createdAt: Date
    createdBy: GmaoActeur
    code: string
    gmaoObjectViewUrl?: string
}
