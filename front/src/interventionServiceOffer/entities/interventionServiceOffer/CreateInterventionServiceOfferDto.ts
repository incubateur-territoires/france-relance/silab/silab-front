import { z } from 'zod'
import { InterventionServiceOfferDtoSchema } from './InterventionServiceOfferDto'

export const CreateInterventionServiceOfferDtoSchema =
    InterventionServiceOfferDtoSchema.omit({
        id: true,
        availableRoles: true,
        template: true,
    }).extend({
        gmaoConfiguration: z.string(),
    })
export type CreateInterventionServiceOfferDto = z.infer<
    typeof CreateInterventionServiceOfferDtoSchema
>
