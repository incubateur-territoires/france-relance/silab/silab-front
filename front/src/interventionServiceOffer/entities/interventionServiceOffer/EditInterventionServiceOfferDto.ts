import {
    CreateInterventionServiceOfferDto,
    CreateInterventionServiceOfferDtoSchema,
} from './CreateInterventionServiceOfferDto'

export const EditInterventionServiceOfferDtoSchema =
    CreateInterventionServiceOfferDtoSchema

export type EditInterventionServiceOfferDto = CreateInterventionServiceOfferDto
