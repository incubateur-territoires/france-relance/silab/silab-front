import { Equipement } from '@/interventionServiceOffer/entities/equipement/Equipement'
import { InterventionServiceOfferDto } from './InterventionServiceOfferDto'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { GmaoConfigurationIntervention } from '../gmaoConfigurationIntervention/GmaoConfigurationIntervention'
import { isDefined } from '@/shared/helpers/types'

export class InterventionServiceOffer extends ServiceOffer {
    availableEquipementsIds: Equipement['id'][]
    gmaoConfiguration: GmaoConfigurationIntervention
    gestionRecompletementActif: boolean
    configurationFormulaireActivite: InterventionServiceOfferDto['configurationFormulaireActivite']
    modeleDeDescriptionPourDemandeIntervention?: string
    gestionDatesSouhaiteesPourDemandeInterventionActif: boolean
    gestionDatesPourInterventionActif: boolean

    public constructor(
        interventionServiceOfferDto: InterventionServiceOfferDto
    ) {
        super(interventionServiceOfferDto)
        this.availableEquipementsIds =
            interventionServiceOfferDto.availableEquipementsIds
        this.gmaoConfiguration = new GmaoConfigurationIntervention(
            interventionServiceOfferDto.gmaoConfiguration
        )
        this.gestionRecompletementActif =
            interventionServiceOfferDto.gestionRecompletementActif
        this.configurationFormulaireActivite =
            interventionServiceOfferDto.configurationFormulaireActivite
        this.modeleDeDescriptionPourDemandeIntervention =
            interventionServiceOfferDto.modeleDeDescriptionPourDemandeIntervention
        this.gestionDatesSouhaiteesPourDemandeInterventionActif =
            interventionServiceOfferDto.gestionDatesSouhaiteesPourDemandeInterventionActif
        this.gestionDatesPourInterventionActif =
            interventionServiceOfferDto.gestionDatesPourInterventionActif
    }

    public getItemDescription() {
        return (
            `Gmao : ${this.gmaoConfiguration.title} (${this.gmaoConfiguration.environnement})` +
            (isDefined(this.description) ? ` - ${this.description}` : '')
        )
    }
}
