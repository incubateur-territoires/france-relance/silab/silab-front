import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { InterventionServiceOffer } from './InterventionServiceOffer'
import { InterventionServiceOfferDtoSchema } from './InterventionServiceOfferDto'
import { CreateInterventionServiceOfferDto } from './CreateInterventionServiceOfferDto'
import { EditInterventionServiceOfferDto } from './EditInterventionServiceOfferDto'

export class InterventionServiceOfferApi extends AzureTokenAccessApi {
    async getAll(
        abortSignal?: AbortSignal
    ): Promise<InterventionServiceOffer[]> {
        const rawResponse = await this.getBackendData(
            `intervention-service-offers`,
            new Map(),
            abortSignal
        )

        const interventionServiceOfferDto =
            InterventionServiceOfferDtoSchema.array().parse(
                await rawResponse.json()
            )

        return interventionServiceOfferDto.map(
            (interventionServiceOfferDto) => {
                return new InterventionServiceOffer(interventionServiceOfferDto)
            }
        )
    }

    async get(
        id: InterventionServiceOffer['id'],
        abortSignal?: AbortSignal
    ): Promise<InterventionServiceOffer> {
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${id}`,
            undefined,
            abortSignal
        )

        const interventionServiceOfferDto =
            InterventionServiceOfferDtoSchema.parse(await rawResponse.json())

        return new InterventionServiceOffer(interventionServiceOfferDto)
    }

    async create(
        newInterventionServiceOffer: CreateInterventionServiceOfferDto
    ): Promise<InterventionServiceOffer> {
        const rawResponse = await this.postBackendData(
            'intervention-service-offers',
            JSON.stringify(newInterventionServiceOffer)
        )

        const interventionServiceOfferDto =
            InterventionServiceOfferDtoSchema.parse(await rawResponse.json())

        return new InterventionServiceOffer(interventionServiceOfferDto)
    }

    async update(
        interventionServiceOfferId: InterventionServiceOffer['id'],
        updatedInterventionServiceOffer: EditInterventionServiceOfferDto
    ) {
        const rawResponse = await this.putBackendData(
            `intervention-service-offers/${interventionServiceOfferId}`,
            JSON.stringify(updatedInterventionServiceOffer)
        )

        const interventionServiceOfferDto =
            InterventionServiceOfferDtoSchema.parse(await rawResponse.json())

        return new InterventionServiceOffer(interventionServiceOfferDto)
    }

    async delete(interventionServiceOfferId: InterventionServiceOffer['id']) {
        await this.deleteBackendData(
            `intervention-service-offers/${interventionServiceOfferId}`
        )
    }
}
