import { EquipementDtoSchema } from '@/interventionServiceOffer/entities/equipement/EquipementDto'
import { ServiceOfferDtoSchema } from '@/serviceOffer/entities/serviceOffer/ServiceOfferDto'
import { GmaoConfigurationInterventionDtoSchema } from '../gmaoConfigurationIntervention/GmaoConfigurationInterventionDto'
import { z } from 'zod'

export const InterventionServiceOfferDtoSchema = ServiceOfferDtoSchema.extend({
    availableEquipementsIds: EquipementDtoSchema.shape.id.array(),
    gmaoConfiguration: GmaoConfigurationInterventionDtoSchema,
    gestionRecompletementActif: z.boolean(),
    configurationFormulaireActivite: z.object({
        dateDeDebut: z.enum(['jour', 'heure', 'automatique']),
        heureParDefautEnMinute: z.number(),
    }),
    modeleDeDescriptionPourDemandeIntervention: z.string().optional(),
    gestionDatesSouhaiteesPourDemandeInterventionActif: z.boolean(),
    gestionDatesPourInterventionActif: z.boolean(),
})

export type InterventionServiceOfferDto = z.infer<
    typeof InterventionServiceOfferDtoSchema
>
