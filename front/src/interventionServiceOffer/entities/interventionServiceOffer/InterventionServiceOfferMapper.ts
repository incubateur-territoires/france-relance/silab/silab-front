import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { InterventionServiceOffer } from './InterventionServiceOffer'
import {
    EditInterventionServiceOfferDto,
    EditInterventionServiceOfferDtoSchema,
} from './EditInterventionServiceOfferDto'
import { CreateInterventionServiceOfferDto } from './CreateInterventionServiceOfferDto'
import { Dto } from '@/shared/api/entities/Dtos'
import {
    InterventionServiceOfferDto,
    InterventionServiceOfferDtoSchema,
} from './InterventionServiceOfferDto'
import { gmaoConfigurationInterventionMapper } from '../gmaoConfigurationIntervention/GmaoConfigurationInterventionMapper'
import { DeepPartial, isDefined } from '@/shared/helpers/types'
import { ZodError } from 'zod'

interface InterventionServiceOfferMapper
    extends EntityMapper<InterventionServiceOffer> {
    toDto: (
        partialInterventionServiceOffer: Partial<InterventionServiceOffer>
    ) => InterventionServiceOfferDto
    toEditDto: (
        partialEntity: DeepPartial<InterventionServiceOffer>
    ) => EditInterventionServiceOfferDto
    toCreateDto: (
        partialEntity: DeepPartial<InterventionServiceOffer>
    ) => CreateInterventionServiceOfferDto
    fromDto: (
        interventionServiceOfferDto: Dto<InterventionServiceOffer>
    ) => InterventionServiceOffer
    fromUnsafeDto: (
        interventionServiceOfferDto: unknown
    ) => InterventionServiceOffer
}

export const interventionServiceOfferMapper: InterventionServiceOfferMapper = {
    toDto(partialInterventionServiceOffer: Partial<InterventionServiceOffer>) {
        try {
            return InterventionServiceOfferDtoSchema.parse({
                id: partialInterventionServiceOffer.id,
                notes: partialInterventionServiceOffer.notes,
                template: partialInterventionServiceOffer.template,
                availableRoles: partialInterventionServiceOffer.availableRoles,
                title: partialInterventionServiceOffer.title,
                description: partialInterventionServiceOffer.description,
                link: partialInterventionServiceOffer.link,
                image: partialInterventionServiceOffer.image,
                configurationFormulaireActivite:
                    partialInterventionServiceOffer.configurationFormulaireActivite,
                availableEquipementsIds:
                    partialInterventionServiceOffer.availableEquipementsIds,
                environnement: partialInterventionServiceOffer.environnement,
                gmaoConfiguration: isDefined(
                    partialInterventionServiceOffer.gmaoConfiguration
                )
                    ? gmaoConfigurationInterventionMapper.toDto(
                          partialInterventionServiceOffer.gmaoConfiguration
                      )
                    : undefined,
                gestionRecompletementActif:
                    partialInterventionServiceOffer.gestionRecompletementActif,
                modeleDeDescriptionPourDemandeIntervention:
                    partialInterventionServiceOffer.modeleDeDescriptionPourDemandeIntervention,
                gestionDatesSouhaiteesPourDemandeInterventionActif:
                    partialInterventionServiceOffer.gestionDatesSouhaiteesPourDemandeInterventionActif,
                gestionDatesPourInterventionActif:
                    partialInterventionServiceOffer.gestionDatesPourInterventionActif,
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'ODS intervention fournit ne possède pas tous les attributs requis.",
                    error
                )
            }
            throw error
        }
    },
    toEditDto(
        partialInterventionServiceOffer: DeepPartial<InterventionServiceOffer>
    ) {
        try {
            return EditInterventionServiceOfferDtoSchema.parse({
                notes: partialInterventionServiceOffer.notes,
                title: partialInterventionServiceOffer.title,
                description: partialInterventionServiceOffer.description,
                link: partialInterventionServiceOffer.link,
                image: partialInterventionServiceOffer.image,
                configurationFormulaireActivite:
                    partialInterventionServiceOffer.configurationFormulaireActivite,
                availableEquipementsIds:
                    partialInterventionServiceOffer.availableEquipementsIds ??
                    [], //amelioration: ça ne devrait pas être protégé à mon sens, non ?
                environnement: partialInterventionServiceOffer.environnement,
                gmaoConfiguration: isDefined(
                    partialInterventionServiceOffer.gmaoConfiguration?.['@id']
                )
                    ? partialInterventionServiceOffer.gmaoConfiguration['@id']
                    : undefined,
                gestionRecompletementActif:
                    partialInterventionServiceOffer.gestionRecompletementActif,
                modeleDeDescriptionPourDemandeIntervention:
                    partialInterventionServiceOffer.modeleDeDescriptionPourDemandeIntervention,
                gestionDatesSouhaiteesPourDemandeInterventionActif:
                    partialInterventionServiceOffer.gestionDatesSouhaiteesPourDemandeInterventionActif,
                gestionDatesPourInterventionActif:
                    partialInterventionServiceOffer.gestionDatesPourInterventionActif,
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'ODS intervention fournit ne possède pas tous les attributs requis.",
                    error
                )
            }
            throw error
        }
    },
    toCreateDto(
        partialInterventionServiceOffer: DeepPartial<InterventionServiceOffer>
    ) {
        return this.toEditDto(partialInterventionServiceOffer)
    },

    fromDto(
        interventionServiceOfferDto: Dto<InterventionServiceOffer>
    ): InterventionServiceOffer {
        return new InterventionServiceOffer(
            interventionServiceOfferDto as InterventionServiceOfferDto
        )
    },
    fromUnsafeDto(
        interventionServiceOfferDto: unknown
    ): InterventionServiceOffer {
        return new InterventionServiceOffer(
            InterventionServiceOfferDtoSchema.parse(interventionServiceOfferDto)
        )
    },
}
