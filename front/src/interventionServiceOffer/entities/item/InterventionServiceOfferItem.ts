import { ItemLists } from '@/interventionServiceOffer/composables/equipement/useItemLists'
import { ListableItem } from '@/shared/item/entities/ListableItem'

export interface InterventionServiceOfferItem extends ListableItem {
    getItemType: () => keyof ItemLists
}
