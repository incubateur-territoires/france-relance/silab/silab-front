import { CreateDto } from '@/shared/api/entities/Dtos'
import { Compteur } from '../compteur/Compteur'
import { MesureDto } from './MesureDto'

export interface CreateMesureDto
    extends CreateDto<Compteur>,
        Pick<MesureDto, 'valeur'> {}
