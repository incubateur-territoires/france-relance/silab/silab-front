import { MesureDto } from './MesureDto'

export class Mesure {
    readonly valeur: number
    readonly createdBy: string
    readonly createdAt: Date

    public constructor(mesureDto: MesureDto) {
        this.valeur = mesureDto.valeur
        this.createdBy = mesureDto.createdBy
        this.createdAt = new Date(mesureDto.createdAt)
    }

    public clone() {
        return new Mesure(this.toDto())
    }

    public toDto() {
        return {
            valeur: this.valeur,
            createdBy: this.createdBy,
            createdAt: this.createdAt.toISOString(),
        }
    }
}
