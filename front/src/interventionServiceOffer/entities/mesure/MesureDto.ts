import { Dto } from '@/shared/api/entities/Dtos'
import { Compteur } from '../compteur/Compteur'

export interface MesureDto extends Dto<Compteur> {
    valeur: number
    createdBy: string
    createdAt: string
}
