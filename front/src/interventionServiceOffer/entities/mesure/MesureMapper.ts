import { EntityMapper } from '@/shared/api/entities/EntityMapper'

import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { Mesure } from './Mesure'
import { CreateMesureDto } from './CreateMesureDto'
import { isUndefined } from '@/shared/helpers/types'

interface MesureMapper extends Pick<EntityMapper<Mesure>, 'toCreateDto'> {
    toCreateDto: (partialEntity: Partial<Mesure>) => CreateMesureDto
}

export const mesureMapper: MesureMapper = {
    toCreateDto(partialMesure: Partial<Mesure>): CreateMesureDto {
        if (isUndefined(partialMesure.valeur)) {
            throw new IncompleteEntity(
                'La mesure fournit ne possède pas tout les attributs requis.'
            )
        }
        return {
            valeur: partialMesure.valeur,
        }
    },
}
