import { IdentityStringProducer } from '@/shared/helpers/identifiable'

export class InterventionOrderBy implements IdentityStringProducer {
    orderByValue: string

    public constructor(orderByValue: string) {
        this.orderByValue = orderByValue
    }

    public getOrderByValues() {
        return this.orderByValue.split(',')
    }

    public static getDefaultSort() {
        return '-createdAt'
    }

    getIdentityString(): string {
        return this.orderByValue
    }
}
