import { ItemLists } from '@/interventionServiceOffer/composables/equipement/useItemLists'
import { SearchEntityDto } from './SearchEntityDto'

export class SearchEntity implements SearchEntityDto {
    public type: keyof ItemLists
    public id: string
    public code?: string
    public label?: string

    public constructor(searchEntityDto: SearchEntityDto) {
        this.type = searchEntityDto.type
        this.id = searchEntityDto.id
        this.code = searchEntityDto.code
        this.label = searchEntityDto.label
    }
}
