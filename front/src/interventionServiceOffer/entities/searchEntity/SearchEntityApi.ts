import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { SearchEntity } from './SearchEntity'
import { SearchEntityDtoSchema } from './SearchEntityDto'

export class SearchEntityAPi extends AzureTokenAccessApi {
    async search(
        serviceOfferId: ServiceOffer['id'],
        query: string,
        abortSignal?: AbortSignal
    ): Promise<Array<SearchEntity>> {
        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/search-entity/search`,
            new Map([['q', query]]),
            abortSignal
        )
        const searchEntityDto = SearchEntityDtoSchema.array().parse(
            await rawResponse.json()
        )
        return searchEntityDto.map((searchEntityDto) => {
            return new SearchEntity(searchEntityDto)
        })
    }
}
