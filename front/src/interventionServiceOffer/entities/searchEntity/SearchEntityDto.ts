import { z } from 'zod'

export const SearchEntityDtoSchema = z.object({
    type: z.enum(['équipements', 'interventions', 'demandes']),
    id: z.string(),
    code: z.string().optional(),
    label: z.string().optional(),
})

export type SearchEntityDto = z.infer<typeof SearchEntityDtoSchema>
