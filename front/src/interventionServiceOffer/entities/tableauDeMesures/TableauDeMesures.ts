import { TableauDeMesuresDto } from './TableauDeMesuresDto'

export class TableauDeMesures {
    titre: string
    sousCellules: Array<string>
    dates: Array<Date>
    lignes: TableauDeMesuresDto['lignes']

    constructor(tableauDeMesuresDto: TableauDeMesuresDto) {
        const tableauDeMesuresDtoClone = structuredClone(tableauDeMesuresDto)
        this.titre = tableauDeMesuresDtoClone.titre
        this.sousCellules = tableauDeMesuresDtoClone.sousCellules
        this.dates = tableauDeMesuresDtoClone.dates.map(
            (dateString) => new Date(dateString)
        )
        this.lignes = tableauDeMesuresDtoClone.lignes
    }
}
