import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { TableauDeMesuresDtoSchema } from './TableauDeMesuresDto'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { TableauDeMesures } from './TableauDeMesures'

export class TableauDeMesuresApi extends AzureTokenAccessApi {
    async getPeriode(
        serviceOfferId: ServiceOffer['id'],
        configurationTableauxDeMesuresId: number, //note: remplacer par config.........['id']
        dateDeDebut: Date,
        nombreDeColonnes: number,
        intervallesDeTempsEntreLesColonnesEnSecondes: number,
        abortSignal?: AbortSignal
    ) {
        const queryParameters = new Map([
            ['dateDeDebut', dateDeDebut.toISOString()],
            ['nombreDeColonnes', nombreDeColonnes.toString()],
            [
                'intervallesDeTempsEntreLesColonnesEnSecondes',
                intervallesDeTempsEntreLesColonnesEnSecondes.toString(),
            ],
        ])

        const rawResponse = await this.getBackendData(
            `intervention-service-offers/${serviceOfferId}/configuration-tableau-de-mesures/${configurationTableauxDeMesuresId}/tableau-de-mesures`,
            queryParameters,
            abortSignal
        )

        const tableauDeMesuresDto = TableauDeMesuresDtoSchema.parse(
            await rawResponse.json()
        )

        return new TableauDeMesures(tableauDeMesuresDto)
    }
}
