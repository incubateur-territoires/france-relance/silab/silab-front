import { z } from 'zod'

const ligneDtoSchema = z.object({
    titre: z.string(),
    idCompteurs: z.string().array(),
    mesures: z
        .number()
        .optional()
        .catch(undefined) // spoiler alert : php connais pas le undefuned
        .array()
        .array(),
})

export const TableauDeMesuresDtoSchema = z.object({
    titre: z.string(),
    sousCellules: z.string().array(),
    dates: z.string().array(),
    lignes: ligneDtoSchema.array(),
})

export type TableauDeMesuresDto = z.infer<typeof TableauDeMesuresDtoSchema>
