import { ItemLists } from '@/interventionServiceOffer/composables/equipement/useItemLists'

export interface InterventionServiceOfferUserPreference {
    globalListSelectedItemType: keyof ItemLists
}
