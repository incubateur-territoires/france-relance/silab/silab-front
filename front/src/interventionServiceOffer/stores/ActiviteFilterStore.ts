import { defineStore } from 'pinia'
import { ref } from 'vue'
import { ActiviteFilters } from '../entities/filters/ActiviteFilters'

export const useActiviteFilterStore = function (
    storeId: string,
    defaultValues: ActiviteFilters['filters'] | undefined
) {
    const defineViewStore = defineStore(
        storeId,
        () => {
            const activiteFilters = ref<ActiviteFilters>(
                new ActiviteFilters(defaultValues)
            )
            return { activiteFilters }
        },
        {
            persist: {
                pick: ['activiteFilters'],
            },
        }
    )
    return defineViewStore()
}
