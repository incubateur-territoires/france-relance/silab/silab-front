import { defineStore } from 'pinia'
import { ref } from 'vue'
import { CommunFilters } from '../entities/filters/CommunFilters'
import { InterventionServiceOffer } from '../entities/interventionServiceOffer/InterventionServiceOffer'

export const useCommunFilterStore = function (
    storeId: string,
    interventionServiceOfferId: InterventionServiceOffer['id']
) {
    const defineViewStore = defineStore(
        `${storeId}|${interventionServiceOfferId}`,
        () => {
            const communFilters = ref<CommunFilters>(new CommunFilters())
            return { communFilters }
        },
        {
            persist: {
                pick: ['communFilters'],
            },
        }
    )
    return defineViewStore()
}
