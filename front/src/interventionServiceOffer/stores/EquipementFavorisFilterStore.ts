import { defineStore } from 'pinia'
import { EquipementFavorisFilters } from '../entities/filters/EquipementFavorieFilters'
import { ref } from 'vue'

export const useEquipementFavorisFilterStore = function (storeId: string) {
    const defineViewStore = defineStore(
        storeId,
        () => {
            const equipementFavorisFilters = ref<EquipementFavorisFilters>(
                new EquipementFavorisFilters()
            )
            return { equipementFavorisFilters: equipementFavorisFilters }
        },
        {
            persist: {
                pick: ['equipementFilters'],
            },
        }
    )
    return defineViewStore()
}
