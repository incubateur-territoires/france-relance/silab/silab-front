import { StateTree, defineStore } from 'pinia'
import { ref } from 'vue'
import {
    EquipementFilters,
    EquipementFiltersDtoSchema,
} from '../entities/equipement/filters/EquipementFilters'
import { InterventionServiceOffer } from '../entities/interventionServiceOffer/InterventionServiceOffer'
import { useCommunFilterStore } from './CommunFilterStore'
import { z } from 'zod'
import { equipementFiltersMapper } from '../entities/equipement/filters/EquipementFiltersMapper'

export const useEquipementFilterStore = function (
    storeId: string,
    interventionServiceOfferId: InterventionServiceOffer['id']
) {
    return defineStore(
        `${storeId}|${interventionServiceOfferId}`,
        () => {
            const communFiltersStore = useCommunFilterStore(
                'communFilters',
                interventionServiceOfferId
            )
            const equipementFilters = ref<EquipementFilters>(
                new EquipementFilters(undefined, [
                    communFiltersStore.communFilters,
                ])
            )
            return { equipementFilters, communFiltersStore }
        },
        {
            persist: {
                pick: ['equipementFilters'],
                afterHydrate: (context) => {
                    context.store.equipementFilters.parentFilters = [
                        context.store.communFiltersStore.communFilters,
                    ]
                },
                serializer: {
                    deserialize: (
                        equipementFiltersStoreJson: string
                    ): StateTree => {
                        const equipementFiltersStoreDto = z
                            .object({
                                equipementFilters: EquipementFiltersDtoSchema,
                            })
                            .parse(JSON.parse(equipementFiltersStoreJson))

                        return {
                            equipementFilters: equipementFiltersMapper.fromDto(
                                equipementFiltersStoreDto.equipementFilters
                            ),
                        }
                    },
                    serialize: (equipementFiltersStore: StateTree): string => {
                        return JSON.stringify({
                            equipementFilters: equipementFiltersMapper.toDto(
                                equipementFiltersStore.equipementFilters
                            ),
                        })
                    },
                },
            },
        }
    )()
}
