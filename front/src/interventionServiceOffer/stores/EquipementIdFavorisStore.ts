import { defineStore } from 'pinia'
import { ref } from 'vue'
import { Equipement } from '../entities/equipement/Equipement'

export const useEquipementIdFavorisStore = function (storeId: string) {
    const defineViewStore = defineStore(
        storeId,
        () => {
            const equipementIdFavoris = ref<Equipement['id'][]>([])
            return { equipementIdFavoris }
        },
        {
            persist: {
                pick: ['equipementIdFavoris'],
            },
        }
    )
    return defineViewStore()
}

export const equipementIdFavorisStoreName = 'EquipementIdFavoris'
