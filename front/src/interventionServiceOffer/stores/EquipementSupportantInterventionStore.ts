import { defineStore } from 'pinia'
import { ref } from 'vue'
import { MsalContext } from '@/plugins/msal/useMsal'
import { Equipement } from '../entities/equipement/Equipement'
import { InterventionServiceOffer } from '../entities/interventionServiceOffer/InterventionServiceOffer'
import { useStoreKeys } from '@/shared/user/composables/useStoreKeys'

export const useEquipementSupportantInterventionStore = function (
    interventionServiceOfferId: InterventionServiceOffer['id'],
    équipementRacineId: Equipement['id'] | undefined,
    msal: MsalContext
) {
    const { generateServiceOfferScopedStoreKey } = useStoreKeys(msal)
    const storeId =
        generateServiceOfferScopedStoreKey(
            'EquipementSupportantInterventionStore',
            interventionServiceOfferId
        ) +
        '|' +
        équipementRacineId
    const defineViewStore = defineStore(
        storeId,
        () => {
            const isLoading = ref<boolean>(false)
            const equipementsSupprotantIntervention = ref<
                Array<Equipement & { interventionsAutorisees: true }>
            >([])
            return { equipementsSupprotantIntervention, isLoading }
        },
        {
            persist: {
                pick: ['equipementsSupprotantIntervention'],
            },
        }
    )
    return defineViewStore()
}
