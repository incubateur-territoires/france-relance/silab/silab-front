import { defineStore } from 'pinia'
import { ref } from 'vue'
import { InterventionOrderBy } from '../entities/orderBy/InterventionOrderBy'

export const useInterventionOrderByStore = function (storeId: string) {
    const defineViewStore = defineStore(
        storeId,
        () => {
            const interventionOrderBy = ref<InterventionOrderBy>(
                new InterventionOrderBy(InterventionOrderBy.getDefaultSort())
            )
            return { interventionOrderBy: interventionOrderBy }
        },
        {
            persist: {
                pick: ['interventionOrderBy'],
            },
        }
    )
    return defineViewStore()
}
