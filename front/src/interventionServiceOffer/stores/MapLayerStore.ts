import { defineStore } from 'pinia'

interface MapLayerStoreState {
    mapLayer: 'Vue Standard' | 'Vue Satellite'
}

export const useMapLayerStore = function (storeId: string) {
    const defineViewStore = defineStore(storeId, {
        state: (): MapLayerStoreState => {
            return { mapLayer: 'Vue Standard' }
        },
        persist: {
            pick: ['mapLayer'],
        },
    })
    return defineViewStore()
}
