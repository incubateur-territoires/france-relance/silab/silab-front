import { CommunFilters } from '../entities/filters/CommunFilters'
import { Ref } from 'vue'
import {
    DemandeInterventionFilters,
    DemandeInterventionFiltersDtoSchema,
} from '../entities/demandeIntervention/filters/DemandeInterventionFilters'
import { InterventionServiceOffer } from '../entities/interventionServiceOffer/InterventionServiceOffer'
import { useMsal } from '@/plugins/msal/useMsal'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { useLocalStoragePersistedState } from '@/shared/composables/useLocalStoragePersistedState'
import { demandeInterventionFiltersMapper } from '../entities/demandeIntervention/filters/DemandeInterventionFiltersMapper'

export const useDemandeInterventionFilters = function (
    interventionServiceOfferId: Ref<InterventionServiceOffer['id']>,
    communFilters: CommunFilters
) {
    const msal = useMsal()
    const userStore = useUserStore(msal)

    return useLocalStoragePersistedState(
        () => {
            return `${userStore.currentUser.id}|${interventionServiceOfferId.value}|demandeInterventionFilters`
        },
        new DemandeInterventionFilters(),
        DemandeInterventionFiltersDtoSchema,
        demandeInterventionFiltersMapper,
        (loadedDemandeInterventionFilters: Ref<DemandeInterventionFilters>) => {
            loadedDemandeInterventionFilters.value.parentFilters = [
                communFilters,
            ]
        }
    )
}
