import { Ref } from 'vue'
import { InterventionServiceOffer } from '../entities/interventionServiceOffer/InterventionServiceOffer'

import { CommunFilters } from '../entities/filters/CommunFilters'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { useMsal } from '@/plugins/msal/useMsal'
import { useLocalStoragePersistedState } from '@/shared/composables/useLocalStoragePersistedState'
import {
    EquipementFilters,
    EquipementFiltersDtoSchema,
} from '../entities/equipement/filters/EquipementFilters'
import { equipementFiltersMapper } from '../entities/equipement/filters/EquipementFiltersMapper'

export const useEquipementFilters = function (
    interventionServiceOfferId: Ref<InterventionServiceOffer['id']>,
    communFilters: CommunFilters
) {
    const msal = useMsal()
    const userStore = useUserStore(msal)

    return useLocalStoragePersistedState(
        () => {
            return `${userStore.currentUser.id}|${interventionServiceOfferId.value}|equipementFilters`
        },
        new EquipementFilters(),
        EquipementFiltersDtoSchema,
        equipementFiltersMapper,
        (loadedEquipementFilters: Ref<EquipementFilters>) => {
            loadedEquipementFilters.value.parentFilters = [communFilters]
        }
    )
}
