import { Ref } from 'vue'
import { InterventionServiceOffer } from '../entities/interventionServiceOffer/InterventionServiceOffer'
import { interventionFiltersMapper } from '../entities/intervention/filters/InterventionFiltersMapper'
import {
    InterventionFilters,
    InterventionFiltersDtoSchema,
} from '../entities/intervention/filters/InterventionFilters'

import { CommunFilters } from '../entities/filters/CommunFilters'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { useMsal } from '@/plugins/msal/useMsal'
import { useLocalStoragePersistedState } from '@/shared/composables/useLocalStoragePersistedState'

export const useInterventionFilters = function (
    interventionServiceOfferId: Ref<InterventionServiceOffer['id']>,
    communFilters: CommunFilters
) {
    const msal = useMsal()
    const userStore = useUserStore(msal)

    return useLocalStoragePersistedState(
        () => {
            return `${userStore.currentUser.id}|${interventionServiceOfferId.value}|interventionFilters`
        },
        new InterventionFilters(),
        InterventionFiltersDtoSchema,
        interventionFiltersMapper,
        (loadedInterventionsFilters: Ref<InterventionFilters>) => {
            loadedInterventionsFilters.value.parentFilters = [communFilters]
        }
    )
}
