import { computed, onBeforeUnmount, Ref, ref, watch } from 'vue'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { isDefined, isUndefined } from '@/shared/helpers/types'
import { RequestAborter } from '@/shared/api/entities/RequestAborter'
import { SilabError } from '@/shared/errors/SilabError'
import { allElementsAreIdentical } from '@/shared/helpers/identifiable'
import { InterventionServiceOffer } from '../entities/interventionServiceOffer/InterventionServiceOffer'
import { useMsal } from '@/plugins/msal/useMsal'
import { useRoute } from 'vue-router'
import { DemandeInterventionApi } from '../entities/demandeIntervention/DemandeInterventionApi'
import { DemandeIntervention } from '../entities/demandeIntervention/DemandeIntervention'
import { DemandeInterventionCoordinates } from '../entities/demandeIntervention/DemandeInterventionCoordinates'
import { ResultPage } from '@/shared/helpers/backend'
import { LoadingManager, SecuredData } from '@/shared/helpers/lifeCycle'
import { CommunFilters } from '../entities/filters/CommunFilters'
import { useDemandeInterventionFilters } from './useDemandeInterventionFilters'
import { useUnmounted } from '@/shared/composables/useUnmounted'

export function usePaginatedDemandeInterventions(
    interventionServiceOfferId: Ref<InterventionServiceOffer['id']>,
    communFilters: CommunFilters
) {
    const msal = useMsal()
    const route = useRoute()
    const userStore = useUserStore(msal)
    const demandeInterventionFetchAborter: RequestAborter = new RequestAborter()
    const demandeInterventionApi = new DemandeInterventionApi(msal)
    const demandeInterventionLoadingManager = new LoadingManager()
    const isStale = ref<boolean>(false)
    const isUnmounted = useUnmounted()
    const demandeInterventionStalenessFetchAborter: RequestAborter =
        new RequestAborter()

    const demandesInterventions = ref<DemandeIntervention[]>([])
    let demandesInterventionsFirstPage: DemandeIntervention[] | undefined =
        undefined
    const maxResults = ref<number | undefined>()
    const currentPage = ref<number>(1)
    const demandeInterventionFilters = useDemandeInterventionFilters(
        interventionServiceOfferId,
        communFilters
    )

    const accessGranted = computed(() => {
        return userStore.currentUserHasRoleForServiceOffer(
            'INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS',
            interventionServiceOfferId.value
        )
    })

    const securedData = computed<
        SecuredData<DemandeIntervention[] | undefined> | undefined
    >(() => {
        if (!accessGranted.value)
            return {
                accessGranted: false,
            }
        return {
            accessGranted: true,
            data: demandesInterventions.value,
        }
    })

    const hasMoreResult = computed<boolean | undefined>(() => {
        if (isUndefined(maxResults.value)) {
            return undefined
        }
        return demandesInterventions.value.length < maxResults.value
    })

    async function loadNextPage() {
        demandeInterventionFetchAborter.abortRequestsAndRefreshAborter()
        if (!accessGranted.value) return
        if (isDefined(hasMoreResult.value) && !hasMoreResult.value) return
        const stopLoading = demandeInterventionLoadingManager.startLoading()
        await fetchDemandesInterventions(
            currentPage.value,
            demandeInterventionFetchAborter.getSignal()
        )
            .then((fetchedDemandesInterventions) => {
                maxResults.value = fetchedDemandesInterventions.maxResults
                demandesInterventions.value.push(
                    ...fetchedDemandesInterventions.data
                )
                // on conserve la première page pour tester la fraîcheur des données et on (re)initialise le checkup de la fraicheur
                if (currentPage.value === 1) {
                    demandesInterventionsFirstPage =
                        fetchedDemandesInterventions.data
                    isStale.value = false
                }
                currentPage.value++
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError(
                        "Erreur lors de la récupération des demandes d'interventions interventions",
                        error
                    )
                }
            })
            .finally(() => {
                stopLoading()
            })
    }

    async function loadFirstPage() {
        if (!accessGranted.value) return
        isStale.value = false
        demandeInterventionFetchAborter.abortRequestsAndRefreshAborter()
        maxResults.value = undefined
        demandesInterventions.value = []
        currentPage.value = 1

        await loadNextPage()
    }

    async function fetchDemandesInterventions(
        page: number,
        abortSignal: AbortSignal
    ): Promise<ResultPage<DemandeIntervention>> {
        return demandeInterventionApi.getAll(
            interventionServiceOfferId.value,
            demandeInterventionFilters.value,
            page,
            abortSignal
        )
    }

    const demandeInterventionCoordinatesFetchAborter: RequestAborter =
        new RequestAborter()
    const coordinatesLoadingManager = new LoadingManager()
    const demandesInterventionsCoordinates = ref<
        DemandeInterventionCoordinates[]
    >([])
    const securedDataCoordinates = computed<
        SecuredData<DemandeInterventionCoordinates[] | undefined> | undefined
    >(() => {
        if (!accessGranted.value)
            return {
                accessGranted: false,
            }
        return {
            accessGranted: true,
            data: demandesInterventionsCoordinates.value,
        }
    })
    async function loadCoordinates() {
        demandeInterventionCoordinatesFetchAborter.abortRequestsAndRefreshAborter()
        if (!accessGranted.value) return
        const stopLoading = coordinatesLoadingManager.startLoading()
        await demandeInterventionApi
            .getAllCoordinates(
                interventionServiceOfferId.value,
                demandeInterventionFilters.value,
                demandeInterventionCoordinatesFetchAborter.getSignal()
            )
            .then((fetchedDemandesInterventionsCoordinates) => {
                demandesInterventionsCoordinates.value =
                    fetchedDemandesInterventionsCoordinates
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError(
                        "Erreur lors de la récupération de la localisation des demandes d'interventions",
                        error
                    )
                }
            })
            .finally(() => {
                stopLoading()
            })
    }

    const isCurrentRouteOnSameServiceOffer = computed(() => {
        return (
            <string>route.params.serviceOfferId ===
            interventionServiceOfferId.value
        )
    })

    // on va comparer régulièrement la première page stockée avec un nouvel appel pour savoir si les données sont périmées
    let updateStalenessTimeout: number | undefined = undefined
    function updateStalenessLater() {
        updateStalenessTimeout = window.setTimeout(
            updateStaleness,
            Number(
                import.meta.env.VITE_ITEMS_FRESHNESS_CHECK_INTERVAL_MILLISECONDS
            )
        )
    }
    async function updateStaleness() {
        if (isUnmounted.value) {
            return
        }

        window.clearTimeout(updateStalenessTimeout)

        try {
            if (!accessGranted.value) return
            demandeInterventionStalenessFetchAborter.abortRequestsAndRefreshAborter()

            // si on a pas la première page, on ne peut pas vérifier la fraicheur des données
            if (isUndefined(demandesInterventionsFirstPage)) {
                return
            }

            const freshDemandesInterventions = (
                await fetchDemandesInterventions(
                    1,
                    demandeInterventionStalenessFetchAborter.getSignal()
                )
            ).data
            isStale.value = !allElementsAreIdentical(
                demandesInterventionsFirstPage,
                freshDemandesInterventions
            )

            //si les données sont périmées on ne relance pas la boucle de refresh
            if (isStale.value) {
                return
            }
        } catch (error) {
            if ((error as Error).name !== 'AbortError') {
                throw new SilabError(
                    "Erreur lors de la vérification périodique de la liste des demandes d'intervetions.",
                    error
                )
            }
        } finally {
            updateStalenessLater()
        }
    }

    watch(
        [demandeInterventionFilters, accessGranted],
        (newValue, oldValue) => {
            if (allElementsAreIdentical(newValue, oldValue)) {
                return
            }
            demandesInterventions.value = []
            currentPage.value = 1
            maxResults.value = undefined
            loadNextPage()
            loadCoordinates()
        },
        { deep: true, immediate: true }
    )

    loadCoordinates()

    watch(
        isCurrentRouteOnSameServiceOffer,
        (
            newIsCurrentRouteOnSameServiceOffer,
            oldIsCurrentRouteOnSameServiceOffer
        ) => {
            const onVientDeQuitterLoffreDeService =
                oldIsCurrentRouteOnSameServiceOffer &&
                !newIsCurrentRouteOnSameServiceOffer
            if (onVientDeQuitterLoffreDeService) {
                abortAllRequests()
            }
        }
    )

    function abortAllRequests() {
        demandeInterventionFetchAborter.abortRequestsAndRefreshAborter()
        demandeInterventionCoordinatesFetchAborter.abortRequestsAndRefreshAborter()
        demandeInterventionStalenessFetchAborter.abortRequestsAndRefreshAborter()
    }

    updateStalenessLater()

    onBeforeUnmount(() => {
        abortAllRequests()
        window.clearTimeout(updateStalenessTimeout)
    })

    return {
        securedData,
        isLoading: demandeInterventionLoadingManager.isLoading,
        maxResults,
        hasMoreResult,
        loadNextPage,
        loadCoordinates,
        loadFirstPage,
        securedDataCoordinates,
        isLoadingCoordinates: coordinatesLoadingManager.isLoading,
        demandeInterventionFilters,
        isStale,
        updateStaleness,
        abortAllRequests,
    }
}
