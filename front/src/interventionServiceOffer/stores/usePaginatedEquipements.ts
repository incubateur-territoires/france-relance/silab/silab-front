import { computed, Ref, ref, triggerRef, watch } from 'vue'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { isDefined, isUndefined } from '@/shared/helpers/types'
import { RequestAborter } from '@/shared/api/entities/RequestAborter'
import { SilabError } from '@/shared/errors/SilabError'
import { allElementsAreIdentical } from '@/shared/helpers/identifiable'
import { InterventionServiceOffer } from '../entities/interventionServiceOffer/InterventionServiceOffer'
import { useMsal } from '@/plugins/msal/useMsal'
import { useRoute } from 'vue-router'
import { ResultPage } from '@/shared/helpers/backend'
import { LoadingManager, SecuredData } from '@/shared/helpers/lifeCycle'
import { EquipementApi } from '../entities/equipement/EquipementApi'
import { Equipement } from '../entities/equipement/Equipement'
import { EquipementCoordinates } from '../entities/equipement/EquipementCoordinates'
import { CommunFilters } from '../entities/filters/CommunFilters'
import { useEquipementFilters } from './useEquipementFilters'
import { useEquipementPins } from '../entities/equipement/pins/useEquipementPins'

export function usePaginatedEquipements(
    interventionServiceOfferId: Ref<InterventionServiceOffer['id']>,
    communFilters: CommunFilters
) {
    const msal = useMsal()

    const route = useRoute()
    const userStore = useUserStore(msal)
    const équipementFetchAborter: RequestAborter = new RequestAborter()
    const équipementApi = new EquipementApi(msal)
    const équipementLoadingManager = new LoadingManager()
    const isStale = ref<boolean>(false)
    const équipementStalenessFetchAborter: RequestAborter = new RequestAborter()
    const équipements = ref<Equipement[]>([])
    let équipementsFirstPage: Equipement[] | undefined = undefined
    const maxResults = ref<number | undefined>()
    const currentPage = ref<number>(1)
    const equipementFilters = useEquipementFilters(
        interventionServiceOfferId,
        communFilters
    )
    const equipementPins = useEquipementPins(interventionServiceOfferId)

    const accessGranted = computed(() => {
        return userStore.currentUserHasRoleForServiceOffer(
            'INTERVENTION_CONSULTER_EQUIPEMENT',
            interventionServiceOfferId.value
        )
    })

    const securedData = computed<
        SecuredData<Equipement[] | undefined> | undefined
    >(() => {
        if (!accessGranted.value)
            return {
                accessGranted: false,
            }
        return {
            accessGranted: true,
            data: équipements.value,
        }
    })

    const hasMoreResult = computed<boolean | undefined>(() => {
        if (isUndefined(maxResults.value)) {
            return undefined
        }

        return équipements.value.length < maxResults.value
    })

    async function loadNextPage() {
        équipementFetchAborter.abortRequestsAndRefreshAborter()
        if (!accessGranted.value) return
        if (isDefined(hasMoreResult.value) && !hasMoreResult.value) {
            return
        }
        const stopLoading = équipementLoadingManager.startLoading()
        await fetchEquipements(
            currentPage.value,
            équipementFetchAborter.getSignal()
        )
            .then((fetchedEquipements) => {
                maxResults.value = fetchedEquipements.maxResults
                équipements.value.push(...fetchedEquipements.data)
                triggerRef(équipements) // on trigger manuellement la réactivité car le simple Array.push ne suffit pas
                // on conserve la première page pour tester la fraîcheur des données et on (re)initialise le checkup de la fraicheur
                if (currentPage.value === 1) {
                    équipementsFirstPage = fetchedEquipements.data
                    isStale.value = false
                }
                currentPage.value++
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError(
                        'Erreur lors de la récupération des équipements',
                        error
                    )
                }
            })
            .finally(() => {
                stopLoading()
            })
    }

    async function loadFirstPage() {
        if (!accessGranted.value) return
        isStale.value = false
        équipementFetchAborter.abortRequestsAndRefreshAborter()
        maxResults.value = undefined
        équipements.value = []
        currentPage.value = 1

        await loadNextPage()
    }

    async function fetchEquipements(
        page: number,
        abortSignal: AbortSignal
    ): Promise<ResultPage<Equipement>> {
        return équipementApi.getAll(
            interventionServiceOfferId.value,
            equipementFilters.value,
            equipementPins.value,
            page,
            abortSignal
        )
    }

    const équipementCoordinatesFetchAborter: RequestAborter =
        new RequestAborter()
    const coordinatesLoadingManager = new LoadingManager()
    const équipementsCoordinates = ref<EquipementCoordinates[]>([])
    const securedDataCoordinates = computed<
        SecuredData<EquipementCoordinates[] | undefined> | undefined
    >(() => {
        if (!accessGranted.value)
            return {
                accessGranted: false,
            }
        return {
            accessGranted: true,
            data: équipementsCoordinates.value,
        }
    })
    async function loadCoordinates() {
        équipementCoordinatesFetchAborter.abortRequestsAndRefreshAborter()
        if (!accessGranted.value) return
        const stopLoading = coordinatesLoadingManager.startLoading()
        await équipementApi
            .getAllCoordinates(
                interventionServiceOfferId.value,
                equipementFilters.value,
                équipementCoordinatesFetchAborter.getSignal()
            )
            .then((fetchedEquipementsCoordinates) => {
                équipementsCoordinates.value = fetchedEquipementsCoordinates
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError(
                        'Erreur lors de la récupération de la localisation des équipements',
                        error
                    )
                }
            })
            .finally(() => {
                stopLoading()
            })
    }

    const isCurrentRouteOnSameServiceOffer = computed(() => {
        return (
            <string>route.params.serviceOfferId ===
            interventionServiceOfferId.value
        )
    })

    // on va comparer régulièrement la première page stockée avec un nouvel appel pour savoir si les données sont périmées
    async function updateStaleness() {
        //Le rafraichissement des données ne se fait pas car les eqpt n'évoluent pas souvent
        if (!accessGranted.value) return
        équipementStalenessFetchAborter.abortRequestsAndRefreshAborter()
        // si on a pas la première page, on ne peut pas vérifier la fraicheur des données

        if (isUndefined(équipementsFirstPage)) {
            return
        }

        try {
            const freshEquipements = (
                await fetchEquipements(
                    1,
                    équipementStalenessFetchAborter.getSignal()
                )
            ).data
            isStale.value = !allElementsAreIdentical(
                équipementsFirstPage,
                freshEquipements
            )

            //si les données sont périmées on ne relance pas la boucle de refresh
            if (isStale.value) {
                return
            }
        } catch (error) {
            if ((error as Error).name !== 'AbortError') {
                throw new SilabError(
                    "Erreur lors de la vérification périodique de la liste d'équipement",
                    error
                )
            }
        }
    }

    watch(
        [equipementFilters, accessGranted],
        (newValue, oldValue) => {
            if (allElementsAreIdentical(newValue, oldValue)) {
                return
            }
            équipements.value = []
            currentPage.value = 1
            maxResults.value = undefined
            loadNextPage()
            loadCoordinates()
        },
        { deep: true, immediate: true }
    )

    loadCoordinates()

    watch(
        isCurrentRouteOnSameServiceOffer,
        (
            newIsCurrentRouteOnSameServiceOffer,
            oldIsCurrentRouteOnSameServiceOffer
        ) => {
            const onVientDeQuitterLoffreDeService =
                oldIsCurrentRouteOnSameServiceOffer &&
                !newIsCurrentRouteOnSameServiceOffer
            if (onVientDeQuitterLoffreDeService) {
                abortAllRequests()
            }
        }
    )

    async function getAll(): Promise<Equipement[]> {
        const équipements: Equipement[] = []
        let page = 1
        let hasMoreResults = true

        while (hasMoreResults) {
            try {
                const fetchedEquipements = await fetchEquipements(
                    page,
                    équipementFetchAborter.getSignal()
                )
                équipements.push(...fetchedEquipements.data)

                hasMoreResults =
                    fetchedEquipements.data.length > 0 &&
                    équipements.length < fetchedEquipements.maxResults
                page++
            } catch (error) {
                if ((error as Error).name !== 'AbortError') {
                    throw new SilabError(
                        'Erreur lors de la récupération des équipements disponnibles',
                        error
                    )
                }
                break
            }
        }
        return équipements
    }

    function abortAllRequests() {
        équipementFetchAborter.abortRequestsAndRefreshAborter()
        équipementCoordinatesFetchAborter.abortRequestsAndRefreshAborter()
        équipementStalenessFetchAborter.abortRequestsAndRefreshAborter()
    }

    return {
        securedData,
        isLoading: équipementLoadingManager.isLoading,
        maxResults,
        hasMoreResult,
        loadNextPage,
        loadCoordinates,
        loadFirstPage,
        securedDataCoordinates,
        isLoadingCoordinates: coordinatesLoadingManager.isLoading,
        equipementFilters,
        equipementPins,
        isStale,
        updateStaleness,
        abortAllRequests,
        getAll,
    }
}
