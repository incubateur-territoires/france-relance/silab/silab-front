import { storeToRefs } from 'pinia'
import { Intervention } from '../entities/intervention/Intervention'
import { Ref, computed, onBeforeUnmount, ref, triggerRef, watch } from 'vue'
import { InterventionApi } from '../entities/intervention/InterventionApi'
import { useUserStore } from '@/shared/user/stores/UserStore'
import { isDefined, isUndefined } from '@/shared/helpers/types'
import { useStoreKeys } from '@/shared/user/composables/useStoreKeys'
import { useInterventionOrderByStore } from './InterventionOrderByStore'
import { RequestAborter } from '@/shared/api/entities/RequestAborter'
import { SilabError } from '@/shared/errors/SilabError'
import { allElementsAreIdentical } from '@/shared/helpers/identifiable'
import { InterventionServiceOffer } from '../entities/interventionServiceOffer/InterventionServiceOffer'
import { useMsal } from '@/plugins/msal/useMsal'
import { InterventionCoordinates } from '../entities/intervention/InterventionCoordinates'
import { useRoute } from 'vue-router'
import { ResultPage } from '@/shared/helpers/backend'
import { LoadingManager, SecuredData } from '@/shared/helpers/lifeCycle'
import { CommunFilters } from '../entities/filters/CommunFilters'
import { useInterventionFilters } from './useInterventionFilters'
import { useUnmounted } from '@/shared/composables/useUnmounted'

export function usePaginatedInterventions(
    interventionServiceOfferId: Ref<InterventionServiceOffer['id']>,
    communFilters: CommunFilters
) {
    const msal = useMsal()
    const { generateUserServiceOfferScopedStoreKey } = useStoreKeys(msal)
    const route = useRoute()
    const userStore = useUserStore(msal)
    const interventionFetchAborter: RequestAborter = new RequestAborter()
    const interventionApi = new InterventionApi(msal)
    const interventionLoadingManager = new LoadingManager()
    const isStale = ref<boolean>(false)
    const isUnmounted = useUnmounted()
    const interventionStalenessFetchAborter: RequestAborter =
        new RequestAborter()

    const interventions = ref<Intervention[]>([])
    let interventionsFirstPage: Intervention[] | undefined = undefined
    const maxResults = ref<number | undefined>()
    const currentPage = ref<number>(1)
    const interventionFilters = useInterventionFilters(
        interventionServiceOfferId,
        communFilters
    )

    const { interventionOrderBy } = storeToRefs(
        useInterventionOrderByStore(
            generateUserServiceOfferScopedStoreKey(
                'InterventionsOrderBy',
                interventionServiceOfferId.value
            )
        )
    )
    const accessGranted = computed(() => {
        return userStore.currentUserHasRoleForServiceOffer(
            'INTERVENTION_CONSULTER_INTERVENTIONS',
            interventionServiceOfferId.value
        )
    })

    const securedData = computed<
        SecuredData<Intervention[] | undefined> | undefined
    >(() => {
        if (!accessGranted.value)
            return {
                accessGranted: false,
            }
        return {
            accessGranted: true,
            data: interventions.value,
        }
    })

    const hasMoreResult = computed<boolean | undefined>(() => {
        if (isUndefined(maxResults.value)) {
            return undefined
        }

        return interventions.value.length < maxResults.value
    })

    async function loadNextPage() {
        interventionFetchAborter.abortRequestsAndRefreshAborter()
        if (!accessGranted.value) return
        if (isDefined(hasMoreResult.value) && !hasMoreResult.value) return
        const stopLoading = interventionLoadingManager.startLoading()
        await fetchInterventions(
            currentPage.value,
            interventionFetchAborter.getSignal()
        )
            .then((fetchedInterventions) => {
                maxResults.value = fetchedInterventions.maxResults
                interventions.value.push(...fetchedInterventions.data)
                triggerRef(interventions) //Note : on sait pas trop pourquoi mais sans ça le hasMoreResult ne se délenchait pas correctement lors de la modification de certains filtres
                // on conserve la première page pour tester la fraîcheur des données et on (re)initialise le checkup de la fraicheur
                if (currentPage.value === 1) {
                    interventionsFirstPage = fetchedInterventions.data
                    isStale.value = false
                }
                currentPage.value++
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError(
                        'Erreur lors de la récupération des interventions',
                        error
                    )
                }
            })
            .finally(() => {
                stopLoading()
            })
    }

    async function loadFirstPage() {
        if (!accessGranted.value) return
        isStale.value = false
        interventionFetchAborter.abortRequestsAndRefreshAborter()
        maxResults.value = undefined
        interventions.value = []
        currentPage.value = 1

        await loadNextPage()
    }

    async function fetchInterventions(
        page: number,
        abortSignal: AbortSignal
    ): Promise<ResultPage<Intervention>> {
        return interventionApi.getAll(
            interventionServiceOfferId.value,
            interventionFilters.value,
            interventionOrderBy.value,
            page,
            abortSignal
        )
    }

    const interventionCoordinatesFetchAborter: RequestAborter =
        new RequestAborter()
    const coordinatesLoadingManager = new LoadingManager()
    const interventionsCoordinates = ref<InterventionCoordinates[]>([])
    const securedDataCoordinates = computed<
        SecuredData<InterventionCoordinates[] | undefined> | undefined
    >(() => {
        if (!accessGranted.value)
            return {
                accessGranted: false,
            }
        return {
            accessGranted: true,
            data: interventionsCoordinates.value,
        }
    })
    async function loadCoordinates() {
        interventionCoordinatesFetchAborter.abortRequestsAndRefreshAborter()
        if (!accessGranted.value) return
        const stopLoading = coordinatesLoadingManager.startLoading()
        await interventionApi
            .getAllCoordinates(
                interventionServiceOfferId.value,
                interventionFilters.value,
                interventionCoordinatesFetchAborter.getSignal()
            )
            .then((fetchedInterventionsCoordinates) => {
                interventionsCoordinates.value = fetchedInterventionsCoordinates
            })
            .catch((error) => {
                if (error.name !== 'AbortError') {
                    throw new SilabError(
                        'Erreur lors de la récupération de la localisation des interventions',
                        error
                    )
                }
            })
            .finally(() => {
                stopLoading()
            })
    }

    const isCurrentRouteOnSameServiceOffer = computed(() => {
        return route.params.serviceOfferId === interventionServiceOfferId.value
    })

    // on va comparer régulièrement la première page stockée avec un nouvel appel pour savoir si les données sont périmées
    let updateStalenessTimeout: number | undefined = undefined
    function updateStalenessLater() {
        updateStalenessTimeout = window.setTimeout(
            updateStaleness,
            Number(
                import.meta.env.VITE_ITEMS_FRESHNESS_CHECK_INTERVAL_MILLISECONDS
            )
        )
    }
    async function updateStaleness() {
        if (isUnmounted.value) {
            return
        }

        window.clearTimeout(updateStalenessTimeout)

        try {
            if (!accessGranted.value) {
                return
            }
            interventionStalenessFetchAborter.abortRequestsAndRefreshAborter()
            // si on a pas la première page, on ne peut pas vérifier la fraicheur des données

            if (isUndefined(interventionsFirstPage)) {
                return
            }

            const freshInterventions = (
                await fetchInterventions(
                    1,
                    interventionStalenessFetchAborter.getSignal()
                )
            ).data
            isStale.value = !allElementsAreIdentical(
                interventionsFirstPage,
                freshInterventions
            )

            //si les données sont périmées on ne relance pas la boucle de refresh
            if (isStale.value) {
                return
            }
        } catch (error) {
            if ((error as Error).name !== 'AbortError') {
                throw new SilabError(
                    'Erreur lors de la vérification périodique de la liste dintervetions.',
                    error
                )
            }
        } finally {
            updateStalenessLater()
        }
    }

    watch(
        [interventionFilters, interventionOrderBy, accessGranted],
        (newValue, oldValue) => {
            if (allElementsAreIdentical(newValue, oldValue)) {
                return
            }
            interventions.value = []
            currentPage.value = 1
            maxResults.value = undefined
            loadNextPage()
            loadCoordinates()
        },
        { deep: true, immediate: true }
    )

    loadCoordinates()

    watch(
        isCurrentRouteOnSameServiceOffer,
        (
            newIsCurrentRouteOnSameServiceOffer,
            oldIsCurrentRouteOnSameServiceOffer
        ) => {
            const onVientDeQuitterLoffreDeService =
                oldIsCurrentRouteOnSameServiceOffer &&
                !newIsCurrentRouteOnSameServiceOffer
            if (onVientDeQuitterLoffreDeService) {
                abortAllRequests()
            }
        }
    )

    function abortAllRequests() {
        interventionFetchAborter.abortRequestsAndRefreshAborter()
        interventionCoordinatesFetchAborter.abortRequestsAndRefreshAborter()
        interventionStalenessFetchAborter.abortRequestsAndRefreshAborter()
    }

    updateStalenessLater()

    onBeforeUnmount(() => {
        abortAllRequests()
        window.clearTimeout(updateStalenessTimeout)
    })

    return {
        securedData,
        isLoading: interventionLoadingManager.isLoading,
        maxResults,
        hasMoreResult,
        loadNextPage,
        loadCoordinates,
        loadFirstPage,
        securedDataCoordinates,
        isLoadingCoordinates: coordinatesLoadingManager.isLoading,
        interventionFilters,
        interventionOrderBy,
        isStale,
        updateStaleness,
        abortAllRequests,
    }
}
