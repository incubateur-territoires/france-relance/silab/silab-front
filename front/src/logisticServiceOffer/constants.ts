import { InjectionKey, Ref } from 'vue'
import { LogisticServiceOffer } from './entities/logisticServiceOffer/LogisticServiceOffer'

export const logisticServiceOfferInjectionKey = Symbol() as InjectionKey<
    Ref<LogisticServiceOffer | undefined>
>
