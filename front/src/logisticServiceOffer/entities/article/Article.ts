import { ArticleDto } from './ArticleDto'

export class Article implements ArticleDto {
    id: string
    code: string
    titre: string
    unité: string
    lots: ArticleDto['lots']

    constructor(articleDto: ArticleDto) {
        this.id = articleDto.id
        this.code = articleDto.code
        this.titre = articleDto.titre
        this.unité = articleDto.unité ?? ''
        this.lots = articleDto.lots
    }

    getQuantitéTotale() {
        let quantitéTotale = 0
        this.lots.forEach((lot) => {
            quantitéTotale += lot.quantité
        })
        return quantitéTotale
    }
}
