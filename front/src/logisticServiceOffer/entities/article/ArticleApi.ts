import { ArticleDto } from './ArticleDto'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { Article } from './Article'
import { MultipleArticleFoundError } from '@/logisticServiceOffer/exceptions/MultipleArticleFoundError'
import { NoArticleFoundError } from '@/logisticServiceOffer/exceptions/NoArticleFoundError'

export class ArticleApi extends AzureTokenAccessApi {
    async getByCode(
        serviceOfferId: ServiceOffer['id'],
        articleCode: Article['code'],
        abortSignal?: AbortSignal
    ) {
        const rawResponse = await this.getBackendData(
            `logistic-service-offers/${serviceOfferId}/articles`,
            new Map([['code', articleCode]]),
            abortSignal
        )

        const articleDtos = (await rawResponse.json()) as ArticleDto[]

        if (articleDtos.length > 1) {
            throw new MultipleArticleFoundError(
                `Plusieurs articles correspondent au code ${articleCode}`
            )
        }

        if (articleDtos.length < 1) {
            throw new NoArticleFoundError(
                `Aucun article ne correspond au code ${articleCode}`
            )
        }

        return new Article(articleDtos[0])
    }
}
