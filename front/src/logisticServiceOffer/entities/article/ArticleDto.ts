export interface ArticleDto {
    id: string
    code: string
    titre: string
    unité?: string
    lots: Array<{
        quantité: number
        magasinId: string
        emplacement?: {
            id: string
            code: string
        }
    }>
}
