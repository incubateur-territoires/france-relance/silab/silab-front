import { ArticleReservationDto } from '@/logisticServiceOffer/entities/articleReservation/ArticleReservationDto'

export class ArticleReservation {
    id: string
    articleId: string
    articleCode: string
    articleDescription: string
    quantity: number
    availableQuantity: number
    unit: string
    warehouseId?: string
    storageLocationId?: string
    storageLocationCode?: string
    creatorName?: string
    creatorDirectionName?: string
    dateAttendueDeReception?: Date
    interventionId?: string
    interventionCode?: string
    interventionTitle?: string

    public constructor(articleReservationDto: ArticleReservationDto) {
        this.id = articleReservationDto.id
        this.articleId = articleReservationDto.articleId
        this.articleCode = articleReservationDto.articleCode
        this.articleDescription = articleReservationDto.articleDescription
        this.quantity = articleReservationDto.quantity
        this.availableQuantity = articleReservationDto.availableQuantity
        this.unit = articleReservationDto.unit
        this.warehouseId = articleReservationDto.warehouseId
        this.storageLocationId = articleReservationDto.storageLocationId
        this.storageLocationCode = articleReservationDto.storageLocationCode
        this.creatorName = articleReservationDto.creatorName
        this.creatorDirectionName = articleReservationDto.creatorDirectionName
        this.dateAttendueDeReception =
            articleReservationDto.dateAttendueDeReception === undefined
                ? undefined
                : new Date(articleReservationDto.dateAttendueDeReception)
        this.interventionId = articleReservationDto.interventionId
        this.interventionCode = articleReservationDto.interventionCode
        this.interventionTitle = articleReservationDto.interventionTitle
    }
}
