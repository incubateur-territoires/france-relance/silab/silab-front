import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { ArticleReservation } from '@/logisticServiceOffer/entities/articleReservation/ArticleReservation'
import { ArticleReservationIssueDto } from '../articleReservationIssue/ArticleReservationIssueDto'
import { Intervention } from '../../../interventionServiceOffer/entities/intervention/Intervention'
import { ArticleReservationDto } from './ArticleReservationDto'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'

export class ArticleReservationApi extends AzureTokenAccessApi {
    async getAll(
        serviceOfferId: ServiceOffer['id'],
        filters?: Map<string, string[]>,
        abortSignal?: AbortSignal
    ): Promise<ArticleReservation[]> {
        const articleReservationsDto = (await (
            await this.getBackendData(
                `logistic-service-offers/${serviceOfferId}/article-reservations`,
                filters,
                abortSignal
            )
        ).json()) as ArticleReservationDto[]

        return articleReservationsDto
            .map((articleReservationDto) => {
                return new ArticleReservation(articleReservationDto)
            })
            .sort(this.defaultSort)
    }

    async findArticleReservationsByIntervention(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id']
    ) {
        const articleReservationApiResponse = (await (
            await this.getBackendData(
                `logistic-service-offers/${serviceOfferId}/interventions/${interventionId}/article-reservations`
            )
        ).json()) as ArticleReservationDto[]

        return articleReservationApiResponse
            .map((articleReservationDto) => {
                return new ArticleReservation(articleReservationDto)
            })
            .sort(this.defaultSort)
    }

    private defaultSort(a: ArticleReservation, b: ArticleReservation) {
        if (a.storageLocationCode === undefined) {
            return -1
        }
        if (b.storageLocationCode === undefined) {
            return 1
        }
        if (a.storageLocationCode > b.storageLocationCode) {
            return 1
        }
        if (a.storageLocationCode < b.storageLocationCode) {
            return -1
        }
        return 0
    }

    /**
     * Sort la quantité maximum sortable de la reservation donnée
     *
     * @param articleReservationIssue
     * @returns true si la sortie de stock s'est bien passée
     */
    async issueArticleReservation(
        serviceOfferId: ServiceOffer['id'],
        articleReservationId: ArticleReservationDto['articleId'],
        articleReservationIssue: ArticleReservationIssueDto
    ): Promise<ArticleReservationIssueDto> {
        const rawResponse = await this.postBackendData(
            `logistic-service-offers/${serviceOfferId}/article-reservations/${articleReservationId}/article-reservation-issues`,
            JSON.stringify(articleReservationIssue)
        )

        return (await rawResponse.json()) as ArticleReservationIssueDto
    }
}
