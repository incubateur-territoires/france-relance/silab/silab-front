export interface ArticleReservationDto {
    id: string
    articleId: string
    articleCode: string
    articleDescription: string
    quantity: number
    availableQuantity: number
    unit: string
    warehouseId?: string
    storageLocationId?: string
    warehouseCode?: string
    storageLocationCode?: string
    creatorName?: string
    creatorDirectionName?: string
    dateAttendueDeReception?: string
    interventionId?: string
    interventionCode?: string
    interventionTitle?: string
}
