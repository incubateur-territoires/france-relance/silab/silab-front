export interface ArticleReservationIssueDto {
    articleId: string
    quantity: number
    warehouseClerkEmail: string
    interventionId?: string
    warehouseId?: string
    storageLocationId?: string
}
