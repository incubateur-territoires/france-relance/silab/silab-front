import { ObjectFilters } from '@/shared/filters/entities/ObjectFilters'

export class InterventionFilters extends ObjectFilters<{
    likeCode: string | undefined
    likeTitre: string | undefined
    likeCreatedBy: string | undefined
}> {
    public getDefaultFilters() {
        return {
            likeCode: undefined,
            likeTitre: undefined,
            likeCreatedBy: undefined,
        }
    }
}
