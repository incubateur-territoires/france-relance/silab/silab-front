import {
    ListableItem,
    SecondaryAction,
    getItemThumbnailUrl,
} from '@/shared/item/entities/ListableItem'
import { InterventionDto } from './InterventionDto'
import { RouteLocationRaw } from 'vue-router'
import { Etat } from '../interventionStatus/Etat'
import { ArticleReservation } from '../articleReservation/ArticleReservation'
import { isDefined } from '@/shared/helpers/types'

export class Intervention implements ListableItem {
    id: string
    code: string
    titre: string
    centreDeCouts?: string
    reservations?: Array<ArticleReservation>
    createdBy: string
    historiqueDesEtats?: Array<Etat>
    dateAttendueDeReceptionLaPlusUrgente?: Date
    equipes: Array<string>
    private itemRoute?: RouteLocationRaw
    private itemSecondaryAction?: SecondaryAction

    constructor(interventionDto: InterventionDto) {
        this.id = interventionDto.id
        this.code = interventionDto.code
        this.titre = interventionDto.titre
        this.centreDeCouts = interventionDto.centreDeCouts
        this.reservations = isDefined(interventionDto.reservations)
            ? interventionDto.reservations.map((articleReservationDto) => {
                  return new ArticleReservation(articleReservationDto)
              })
            : undefined
        this.createdBy = interventionDto.createdBy
        this.historiqueDesEtats = isDefined(interventionDto.historiqueDesEtats)
            ? interventionDto.historiqueDesEtats.map(
                  (etatDto) => new Etat(etatDto)
              )
            : undefined
        this.dateAttendueDeReceptionLaPlusUrgente = isDefined(
            interventionDto.dateAttendueDeReceptionLaPlusUrgente
        )
            ? new Date(interventionDto.dateAttendueDeReceptionLaPlusUrgente)
            : undefined
        this.equipes = interventionDto.equipes
    }
    public getItemId() {
        return this.id
    }
    public getItemTitle() {
        return this.code + ' - ' + this.titre
    }

    public getItemDescription() {
        const description: string[] = []
        if (isDefined(this.centreDeCouts)) description.push(this.centreDeCouts)
        description.push('Créée par ' + this.createdBy)
        return description
    }
    public getItemImageUrl() {
        return undefined
    }
    public getItemThumbnailUrl = getItemThumbnailUrl
    public getItemChips() {
        return []
    }
    public isRequiringAttention() {
        return false
    }

    public getItemTemporalData() {
        const date =
            this.dateAttendueDeReceptionLaPlusUrgente?.toLocaleDateString(
                'fr',
                {
                    day: '2-digit',
                    month: '2-digit',
                    year: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit',
                }
            )

        return date ? 'livraison le ' + date : ''
    }

    public getItemRoute() {
        return this.itemRoute
    }
    public setItemRoute(itemRoute: RouteLocationRaw | undefined) {
        this.itemRoute = itemRoute
        return this
    }
    public getItemSecondaryAction() {
        return this.itemSecondaryAction
    }

    public setItemSecondaryAction(secondaryAction: SecondaryAction) {
        this.itemSecondaryAction = secondaryAction
        return this
    }
}
