import { ArticleReservationDto } from '@/logisticServiceOffer/entities/articleReservation/ArticleReservationDto'
import { EtatDto } from '../interventionStatus/EtatDto'

// export const InterventionDtoSchema = z.object({
//     id: z.string(),
//     code: z.string(),
//     titre: z.string(),
//     service:
// })

export interface InterventionDto {
    id: string
    code: string
    titre: string
    centreDeCouts?: string
    reservations: Array<ArticleReservationDto>
    createdBy: string
    historiqueDesEtats?: Array<EtatDto>
    dateAttendueDeReceptionLaPlusUrgente: string
    equipes: Array<string>
}
