import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { InterventionDto } from './InterventionDto'
import { Intervention } from './Intervention'
import { InterventionFilters } from '../filters/InterventionFilters'

export class InterventionApi extends AzureTokenAccessApi {
    async getAll(
        serviceOfferId: ServiceOffer['id'],
        filter: InterventionFilters,
        abortSignal?: AbortSignal
    ) {
        const rawResponse = await this.getBackendData(
            `logistic-service-offers/${serviceOfferId}/interventions`,
            filter.toQueryParameters(),
            abortSignal
        )

        const interventionsApiResponse =
            (await rawResponse.json()) as InterventionDto[]

        return interventionsApiResponse.map((interventionDto) => {
            return new Intervention(interventionDto)
        })
    }

    async getAllDemandesDeFournitures(
        serviceOfferId: ServiceOffer['id'],
        filter: InterventionFilters,
        abortSignal?: AbortSignal
    ) {
        const rawResponse = await this.getBackendData(
            `logistic-service-offers/${serviceOfferId}/demandes-de-fournitures`,
            filter.toQueryParameters(),
            abortSignal
        )

        const interventionsApiResponse =
            (await rawResponse.json()) as InterventionDto[]

        return interventionsApiResponse.map((interventionDto) => {
            return new Intervention(interventionDto)
        })
    }

    async get(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        abortSignal?: AbortSignal
    ) {
        const rawResponse = await this.getBackendData(
            `logistic-service-offers/${serviceOfferId}/interventions/${interventionId}`,
            new Map().set('includeHistoriqueDesEtats', true),
            abortSignal
        )

        const interventionDto = (await rawResponse.json()) as InterventionDto

        return new Intervention(interventionDto)
    }
}
