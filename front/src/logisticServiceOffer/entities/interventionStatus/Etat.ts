import { ObjectStatus } from '@/shared/objectStatus/entities/ObjectStatus'
import { EtatDto } from './EtatDto'

export class Etat extends ObjectStatus<EtatDto['code']> {
    public constructor(etatDto: EtatDto) {
        super(etatDto)
    }
}
