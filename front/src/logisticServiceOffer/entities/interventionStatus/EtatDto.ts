import { ObjectHistorisedStatusDto } from '@/shared/objectStatus/entities/ObjectHistorisedStatusDto'

export type EtatDto = ObjectHistorisedStatusDto<
    'closed' | 'awaiting_realisation' | 'awaiting_validation'
>
