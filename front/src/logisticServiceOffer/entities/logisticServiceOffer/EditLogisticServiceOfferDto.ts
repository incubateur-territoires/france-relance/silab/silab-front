import { MouvementDto } from '../mouvement/MouvementDto'
import { EditDto } from '@/shared/api/entities/Dtos'
import { LogisticServiceOffer } from './LogisticServiceOffer'

export interface EditLogisticServiceOfferDto
    extends EditDto<LogisticServiceOffer> {
    typeDInventaireId: MouvementDto['typeDeMouvement']['id']
}
