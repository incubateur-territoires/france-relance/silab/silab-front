import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { MouvementDto } from '../mouvement/MouvementDto'
import { LogisticServiceOfferDto } from './LogisticServiceOfferDto'

export class LogisticServiceOffer extends ServiceOffer {
    warehouseId: string
    typeDInventaireId: MouvementDto['typeDeMouvement']['id']
    gmaoConfiguration: {
        typeDInventaires: Array<MouvementDto['typeDeMouvement']>
    }

    constructor(logisticServiceOfferDto: LogisticServiceOfferDto) {
        super({
            id: logisticServiceOfferDto.id,
            title: logisticServiceOfferDto.title,
            image: logisticServiceOfferDto.image,
            link: logisticServiceOfferDto.link,
            availableRoles: logisticServiceOfferDto.availableRoles,
            template: logisticServiceOfferDto.template,
            environnement: logisticServiceOfferDto.environnement,
        })
        this.warehouseId = logisticServiceOfferDto.warehouseId
        this.typeDInventaireId = logisticServiceOfferDto.typeDInventaireId
        this.gmaoConfiguration = logisticServiceOfferDto.gmaoConfiguration
    }
}
