import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { LogisticServiceOffer } from './LogisticServiceOffer'
import { LogisticServiceOfferDto } from './LogisticServiceOfferDto'
import { EditLogisticServiceOfferDto } from './EditLogisticServiceOfferDto'

export class LogisticServiceOfferApi extends AzureTokenAccessApi {
    async get(logisticServiceOfferId: LogisticServiceOffer['id']) {
        const rawResponse = await this.getBackendData(
            `logistic-service-offers/${logisticServiceOfferId}`
        )

        const LogisticServiceOfferDto =
            (await rawResponse.json()) as LogisticServiceOfferDto

        return new LogisticServiceOffer(LogisticServiceOfferDto)
    }

    async update(
        logisticServiceOfferId: LogisticServiceOffer['id'],
        updatedLogisticServiceOffer: EditLogisticServiceOfferDto
    ) {
        const rawResponse = await this.putBackendData(
            `logistic-service-offers/${logisticServiceOfferId}`,
            JSON.stringify(updatedLogisticServiceOffer)
        )

        const logisticServiceOfferDto =
            (await rawResponse.json()) as LogisticServiceOfferDto

        return new LogisticServiceOffer(logisticServiceOfferDto)
    }
}
