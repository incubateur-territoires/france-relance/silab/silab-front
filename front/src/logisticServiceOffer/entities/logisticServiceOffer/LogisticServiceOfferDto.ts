import { ServiceOfferDto } from '@/serviceOffer/entities/serviceOffer/ServiceOfferDto'
import { MouvementDto } from '../mouvement/MouvementDto'
import { Dto } from '@/shared/api/entities/Dtos'
import { LogisticServiceOffer } from './LogisticServiceOffer'

export interface LogisticServiceOfferDto
    extends ServiceOfferDto,
        Dto<LogisticServiceOffer> {
    warehouseId: string
    typeDInventaireId: MouvementDto['typeDeMouvement']['id']
    gmaoConfiguration: {
        typeDInventaires: Array<MouvementDto['typeDeMouvement']>
    }
}
