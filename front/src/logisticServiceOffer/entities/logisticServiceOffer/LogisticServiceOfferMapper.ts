import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { LogisticServiceOffer } from './LogisticServiceOffer'
import { EditLogisticServiceOfferDto } from './EditLogisticServiceOfferDto'

interface LogisticServiceOfferMapper
    extends Omit<
        EntityMapper<LogisticServiceOffer>,
        'toDto' | 'toCreateDto' | 'toDtoArray'
    > {
    toEditDto: (
        partialEntity: Partial<LogisticServiceOffer>
    ) => EditLogisticServiceOfferDto
}

export const logisticServiceOfferMapper: LogisticServiceOfferMapper = {
    toEditDto: function (
        logisticServiceOfferPartial: Partial<LogisticServiceOffer>
    ): EditLogisticServiceOfferDto {
        if (logisticServiceOfferPartial.typeDInventaireId === undefined) {
            throw new IncompleteEntity(
                "L'ODS logistique ne possède pas tout les attributs requis."
            )
        }

        return {
            typeDInventaireId: logisticServiceOfferPartial.typeDInventaireId,
        }
    },
}
