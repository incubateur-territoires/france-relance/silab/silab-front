import { MouvementDto } from './MouvementDto'

export interface CreateMouvementDto
    extends Omit<
        MouvementDto,
        'createdBy' | 'createdAt' | 'typeDeMouvement' | 'quantité' | 'id'
    > {
    typeDeMouvement: {
        id: string
    }
    quantité: number
}
