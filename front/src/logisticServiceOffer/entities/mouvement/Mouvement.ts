import { Article } from '../article/Article'
import { MouvementDto } from './MouvementDto'

export class Mouvement {
    quantité?: number
    article: Pick<Article, 'id'>
    emplacement?: {
        id: string
        code: string
    }
    typeDeMouvement: {
        id: string
        libele: string
    }
    createdBy?: string
    createdAt: Date
    id: string

    constructor(mouvementDto: MouvementDto) {
        this.quantité = mouvementDto.quantité
        this.article = mouvementDto.article
        this.emplacement = mouvementDto.emplacement
        this.typeDeMouvement = mouvementDto.typeDeMouvement
        this.createdBy = mouvementDto.createdBy
        this.createdAt = new Date(mouvementDto.createdAt)
        this.id = mouvementDto.id
    }
}
