import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { CreateMouvementDto } from './CreateMouvementDto'
import { MouvementDto } from './MouvementDto'
import { Mouvement } from './Mouvement'
import { MouvementFilters } from './MouvementFilters'

export class MouvementApi extends AzureTokenAccessApi {
    async create(
        serviceOfferId: ServiceOffer['id'],
        newMouvement: CreateMouvementDto
    ) {
        const rawResponse = await this.postBackendData(
            `logistic-service-offers/${serviceOfferId}/mouvements`,
            JSON.stringify(newMouvement)
        )

        const mouvementDto = (await rawResponse.json()) as MouvementDto

        return new Mouvement(mouvementDto)
    }

    async getAll(
        serviceOfferId: ServiceOffer['id'],
        filter?: MouvementFilters,
        // orderBy: InterventionOrderBy,
        abortSignal?: AbortSignal
    ): Promise<Mouvement[]> {
        const queryParameters = filter?.toQueryParameters()

        const rawResponse = await this.getBackendData(
            `logistic-service-offers/${serviceOfferId}/mouvements`,
            queryParameters,
            abortSignal
        )

        const mouvementsDto = (await rawResponse.json()) as MouvementDto[]

        // On veux instancier les interventions avec leur classes finales/filles, on utilise pour cela le factory sur chaque Dto reçu
        return mouvementsDto.map((mouvementDto) => {
            return new Mouvement(mouvementDto)
        })
    }
}
