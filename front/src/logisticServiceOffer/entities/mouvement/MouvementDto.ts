export interface MouvementDto {
    quantité?: number
    article: {
        id: string
    }
    emplacement?: {
        id: string
        code: string
    }
    typeDeMouvement: {
        id: string
        libele: string
    }
    createdBy?: string
    id: string
    createdAt: string
}
