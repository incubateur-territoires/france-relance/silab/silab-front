import { ObjectFilters } from '@/shared/filters/entities/ObjectFilters'

export class MouvementFilters extends ObjectFilters<{
    'article.code': string[] | undefined
}> {
    public getDefaultFilters(): MouvementFilters['filters'] {
        return { 'article.code': undefined }
    }
}
