import { Article } from '../article/Article'
import { Intervention } from '../intervention/Intervention'
import { MouvementDeSortieDto } from './MouvementDeSortieDto'

export class MouvementDeSortie {
    id: string
    article: Omit<Article, 'lots' | 'getQuantitéTotale'>
    quantité: number
    emplacement?: {
        id: string
        code: string
    }
    interventionId?: Intervention['id']
    createdBy?: string
    createdAt: Date

    constructor(mouvementDeSortieDto: MouvementDeSortieDto) {
        this.quantité = mouvementDeSortieDto.quantite

        this.article = {
            id: mouvementDeSortieDto.article.id,
            code: mouvementDeSortieDto.article.code,
            titre: mouvementDeSortieDto.article.titre,
            unité: mouvementDeSortieDto.article.unité ?? '',
        }
        this.emplacement = mouvementDeSortieDto.emplacement
        this.interventionId = mouvementDeSortieDto.interventionId
        this.createdBy = mouvementDeSortieDto.createdBy
        this.createdAt = new Date(mouvementDeSortieDto.createdAt)
        this.id = mouvementDeSortieDto.id
    }
}
