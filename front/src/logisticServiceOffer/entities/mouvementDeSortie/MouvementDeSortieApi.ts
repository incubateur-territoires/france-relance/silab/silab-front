import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { MouvementDeSortie } from './MouvementDeSortie'
import { MouvementDeSortieDto } from './MouvementDeSortieDto'
import { Intervention } from '../intervention/Intervention'

export class MouvementDeSortieApi extends AzureTokenAccessApi {
    async getAll(
        serviceOfferId: ServiceOffer['id'],
        interventionId: Intervention['id'],
        abortSignal?: AbortSignal
    ): Promise<MouvementDeSortie[]> {
        const rawResponse = await this.getBackendData(
            `logistic-service-offers/${serviceOfferId}/interventions/${interventionId}/sorties-de-stock`,
            new Map(),
            abortSignal
        )

        const mouvementsDeSortieDto =
            (await rawResponse.json()) as MouvementDeSortieDto[]

        return mouvementsDeSortieDto.map((mouvementDeSortieDto) => {
            return new MouvementDeSortie(mouvementDeSortieDto)
        })
    }
}
