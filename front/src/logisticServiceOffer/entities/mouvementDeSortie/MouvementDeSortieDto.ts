import { InterventionDto } from '../intervention/InterventionDto'

export interface MouvementDeSortieDto {
    id: string
    article: {
        id: string
        code: string
        titre: string
        unité?: string
    }
    quantite: number
    emplacement?: {
        id: string
        code: string
    }
    createdBy?: string
    createdAt: string
    interventionId?: InterventionDto['id']
}
