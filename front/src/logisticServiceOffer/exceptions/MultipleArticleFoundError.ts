import { SilabError } from '@/shared/errors/SilabError'

export class MultipleArticleFoundError extends SilabError {
    constructor(message: string, cause?: unknown) {
        super(message, { cause })
        this.name = 'MultipleArticleFoundError'
    }
}
