import { SilabError } from '@/shared/errors/SilabError'

export class NoArticleFoundError extends SilabError {
    constructor(message: string, cause?: unknown) {
        super(message, { cause })
        this.name = 'NoArticleFoundError'
    }
}
