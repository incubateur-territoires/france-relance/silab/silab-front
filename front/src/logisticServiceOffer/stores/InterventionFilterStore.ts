import { defineStore } from 'pinia'

import { ref } from 'vue'
import { InterventionFilters } from '../entities/filters/InterventionFilters'

export const useInterventionFilterStore = function (storeId: string) {
    const defineViewStore = defineStore(
        storeId,
        () => {
            const interventionFilters = ref<InterventionFilters>(
                new InterventionFilters()
            )
            return { interventionFilters }
        },
        {
            persist: false,
        }
    )
    return defineViewStore()
}
