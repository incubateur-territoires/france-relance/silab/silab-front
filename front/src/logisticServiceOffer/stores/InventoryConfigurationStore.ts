import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useInventoryConfigurationStore = function (storeId: string) {
    const defineViewStore = defineStore(
        storeId,
        () => {
            const codeArticlePrefix = ref<string>('')
            return { codeArticlePrefix }
        },
        {
            persist: {
                pick: ['codeArticlePrefix'],
            },
        }
    )
    return defineViewStore()
}

export const inventoryConfigurationStoreName = 'InventoryConfiguration'
