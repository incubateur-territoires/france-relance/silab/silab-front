import { createApp } from 'vue'
import App from '@/App.vue'
import router from '@/router'
import vuetify from '@/plugins/vuetify'
import { msalInstance } from './security/authConfig'
import { msalPlugin } from './plugins/msal'
import { CustomNavigationClient } from './router/NavigationClient'
import { AuthenticationResult, EventType } from '@azure/msal-browser'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

const navigationClient = new CustomNavigationClient(router)
msalInstance.setNavigationClient(navigationClient)
/* sets activeAccount from cache */
const accounts = msalInstance.getAllAccounts()
if (accounts.length > 0) {
    msalInstance.setActiveAccount(accounts[0])
}
msalInstance.addEventCallback((event) => {
    if (event.eventType === EventType.LOGIN_SUCCESS && event.payload) {
        const payload = event.payload as AuthenticationResult
        const account = payload.account
        msalInstance.setActiveAccount(account)
    }
})

const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)
const app = createApp(App)

const promisesPrérequisesAuMount = []

if (import.meta.env.VITE_USE_MOCK_API === 'true') {
    // on attend également que mirage soit monté pour éviter des appels trop tôt
    promisesPrérequisesAuMount.push(
        import('@/mock-api/server').then((server) => {
            server.makeServer()
        })
    )
}

app.use(router)
app.use(vuetify)
app.use(msalPlugin, msalInstance)
app.use(pinia)

promisesPrérequisesAuMount.push(router.isReady())

Promise.all(promisesPrérequisesAuMount).then(() => {
    // Waiting for the router to be ready prevents race conditions when returning from a loginRedirect or acquireTokenRedirect
    app.mount('#app')
})
