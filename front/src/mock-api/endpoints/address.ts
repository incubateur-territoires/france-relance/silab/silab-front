import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'
import { AnyResponse } from 'miragejs/-types'

export function useAddressEndpoints(this: AppServer) {
    this.get('geocoding/reverse', (schema, request) => {
        return authenticationProtectedResponse(
            schema,
            request,
            async (): Promise<AnyResponse> => {
                const address = (
                    await (
                        await fetch(
                            'https://wxs.ign.fr/essentiels/geoportail/geocodage/rest/0.1/reverse?' +
                                new URLSearchParams({
                                    lat: request.queryParams.lat as string,
                                    lon: request.queryParams.lng as string,
                                    index: 'address',
                                    limit: '1',
                                })
                        )
                    ).json()
                ).features.at(0)?.properties.name as string | undefined

                return { address: address }
            }
        )
    })
}
