import { AppServer } from '@/mock-api/types'
import { isNull } from '@/shared/helpers/types'
import { Response } from 'miragejs'
import { authenticationProtectedResponse } from '../helpers'

export function useCarlClientEndpoints(this: AppServer) {
    this.resource('carlClient', { path: '/carl-clients', only: ['index'] })
    this.get('/carl-clients/:carlClientId/cost-centers', (schema, request) => {
        return authenticationProtectedResponse(schema, request, () => {
            const carlClientId = request.params.carlClientId
            const carlClient = schema.find('carlClient', carlClientId)
            if (isNull(carlClient)) {
                return new Response(404)
            }
            return carlClient.costCenters
        })
    })
}
