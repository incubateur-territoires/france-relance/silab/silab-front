import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppSchema, AppServer } from '@/mock-api/types'
import { AnyResponse } from 'miragejs/-types'

export function useGedClientsEndpoints(this: AppServer) {
    this.get('ged-clients', (schema, request) => {
        return authenticationProtectedResponse(schema, request, getAllGedClient)
    })
}
function getAllGedClient(schema: AppSchema): AnyResponse {
    return schema.all('gedClient')
}
