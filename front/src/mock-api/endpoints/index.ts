import { AppServer } from '@/mock-api/types'
import { useCarlClientEndpoints } from './carlClient'
import { useGedClientsEndpoints } from './gedClient'
import { useMessageEndpoints } from './message'
import { useServiceOfferEndpoints } from './serviceOffer'
import { useUserEndpoints } from './users'
import { useAddressEndpoints } from './address'
import { useSiilabConfigurationEndpoints } from './siilabConfiguration'

export function useBackendEndpoints(this: AppServer) {
    useCarlClientEndpoints.call(this)
    useGedClientsEndpoints.call(this)
    useMessageEndpoints.call(this)
    useServiceOfferEndpoints.call(this)
    useUserEndpoints.call(this)
    useAddressEndpoints.call(this)
    useSiilabConfigurationEndpoints.call(this)
}
