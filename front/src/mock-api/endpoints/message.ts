import { CreateMessageDto } from '@/shared/administration/entities/messages/CreateMessageDto'
import { AppServer } from '@/mock-api/types'
import { Response } from 'miragejs'
import { isNull } from '@/shared/helpers/types'

export function useMessageEndpoints(this: AppServer) {
    this.resource('message', { only: ['index', 'show', 'delete'] })
    this.post('/messages', (schema, request) => {
        const attrs: CreateMessageDto = JSON.parse(request.requestBody)

        return schema.create('message', attrs)
    })
    this.put('/messages/:messageid', (schema, request) => {
        const messageId = request.params.messageid
        const newAttrs = JSON.parse(request.requestBody)

        const message = schema.find('message', messageId)
        if (isNull(message)) {
            return new Response(404)
        }
        message?.update(newAttrs)

        return message
    })
}
