import { AppServer, PolymorphicCollection } from '@/mock-api/types'
import { useInterventionServiceOfferEndpoints } from './intervention'
import { useLogistiqueServiceOffersEndpoints } from './logistic'
import { getUserFromRequest } from '@/mock-api/helpers'
import { isNull } from '@/shared/helpers/types'
import { Response } from 'miragejs'

type ServiceOfferCollection = PolymorphicCollection<
    'interventionServiceOffer' | 'logisticServiceOffer' | 'serviceOffer'
>

export function useServiceOfferEndpoints(this: AppServer) {
    useInterventionServiceOfferEndpoints.call(this)
    useLogistiqueServiceOffersEndpoints.call(this)

    this.get('service-offers', (schema) => {
        const interventionServiceOffers: ServiceOfferCollection =
            schema.none('serviceOffer')
        return interventionServiceOffers
            .mergeCollection(schema.all('interventionServiceOffer'))
            .mergeCollection(schema.all('logisticServiceOffer'))
            .sort((a, b) => (a.title < b.title ? -1 : 1))
    })
    this.get('service-offers/:serviceOfferId', (schema, request) => {
        const user = getUserFromRequest(request)

        if (isNull(user)) {
            return new Response(
                401,
                {},
                { error: 'Utilisateur non authentifié' }
            )
        }

        const serviceOfferId = request.params.serviceOfferId
        const leRoleAppartienALODSregex = new RegExp(
            `^SERVICEOFFER_${serviceOfferId}_ROLE_(?<serviceOfferType>.[^_]+).*`
        )

        const hasRequiredRole = user?.reachableRoles.some((role) =>
            leRoleAppartienALODSregex.test(role)
        )

        if (!hasRequiredRole) {
            return new Response(
                403,
                {},
                { error: 'Accès refusé : droits insuffisants' }
            )
        }

        const interventionServiceOffer = schema.find(
            'interventionServiceOffer',
            serviceOfferId
        )
        const logisticServiceOffer = schema.find(
            'logisticServiceOffer',
            serviceOfferId
        )

        if (isNull(interventionServiceOffer) && isNull(logisticServiceOffer)) {
            return new Response(
                404,
                {},
                { error: 'Aucune offre de service trouvée' }
            )
        }

        return (
            schema.find('interventionServiceOffer', serviceOfferId) ??
            schema.find('logisticServiceOffer', serviceOfferId)
        )
    })
}
