import { CreateActiviteActionDto } from '@/interventionServiceOffer/entities/activite/CreateActiviteActionDto '
import { EditActiviteDto } from '@/interventionServiceOffer/entities/activite/EditActiviteDto'
import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'
import { isDefined, isNull, isUndefined } from '@/shared/helpers/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Response } from 'miragejs'

export function useActiviteActionEndpoints(this: AppServer) {
    this.get(
        'intervention-service-offers/:serviceOfferId/interventions/:interventionId/activites-action',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema) => {
                    const intervention = schema.find(
                        'intervention',
                        request.params.interventionId
                    )
                    return intervention?.activitesAction ?? new Response(404)
                }
            )
        }
    )
    this.put(
        'intervention-service-offers/:serviceOfferId/activites-action/:activiteId',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema) => {
                    if (
                        request.requestHeaders?.['content-type'] !==
                        'application/json'
                    ) {
                        return new Response(415)
                    }
                    {
                        const activiteInitiale = schema.find(
                            'activiteAction',
                            request.params.activiteId
                        )
                        if (isNull(activiteInitiale)) {
                            return new Response(404)
                        }
                        const editActiviteDto: EditActiviteDto = JSON.parse(
                            request.requestBody
                        )

                        activiteInitiale.update(editActiviteDto)

                        return activiteInitiale
                    }
                }
            )
        }
    )
    this.delete(
        'intervention-service-offers/:serviceOfferId/activites-action/:activiteId',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema) => {
                    const activiteInitiale = schema.find(
                        'activiteAction',
                        request.params.activiteId
                    )
                    if (isNull(activiteInitiale)) {
                        return new Response(404)
                    }

                    activiteInitiale.destroy()

                    return new Response(204)
                }
            )
        }
    )
    this.post(
        'intervention-service-offers/:serviceOfferId/interventions/:interventionId/activites-action',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema) => {
                    if (
                        request.requestHeaders?.['content-type'] !==
                        'application/json'
                    ) {
                        return new Response(415)
                    }

                    const interventionId = request.params.interventionId
                    const intervention = schema.find(
                        'intervention',
                        interventionId
                    )
                    if (isNull(intervention)) {
                        return new Response(
                            404,
                            {},
                            { error: 'Intervention non trouvée' }
                        )
                    }

                    const createActiviteDto: CreateActiviteActionDto =
                        JSON.parse(request.requestBody)
                    if (isUndefined(createActiviteDto.intervenantId)) {
                        return new Response(404)
                    }

                    const intervenant = schema.find(
                        'gmaoActeur',
                        createActiviteDto.intervenantId
                    )

                    let operation = null
                    if (isDefined(createActiviteDto.operationId)) {
                        operation = schema.find(
                            'operation',
                            createActiviteDto.operationId
                        )
                        if (operation === null) {
                            return new Response(500)
                        }
                    }

                    const newActiviteDto = schema.create('activiteAction', {
                        id: faker.string.uuid(),
                        dateDeDebut: createActiviteDto.dateDeDebut,
                        duree: createActiviteDto.duree,
                        intervention: intervention,
                        intervenant: intervenant ?? undefined,
                        operation: operation,
                        isObligatoire: false,
                    })

                    return newActiviteDto ?? new Response(500)
                }
            )
        }
    )
}
