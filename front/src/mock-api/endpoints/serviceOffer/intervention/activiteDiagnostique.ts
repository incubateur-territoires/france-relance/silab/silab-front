import { EditActiviteDto } from '@/interventionServiceOffer/entities/activite/EditActiviteDto'
import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'
import { isNull } from '@/shared/helpers/types'
import { Response } from 'miragejs'

export function useActiviteDiagnostiqueEndpoints(this: AppServer) {
    this.get(
        'intervention-service-offers/:serviceOfferId/interventions/:interventionId/activites-diagnostique',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema) => {
                    const intervention = schema.find(
                        'intervention',
                        request.params.interventionId
                    )
                    return (
                        intervention?.activitesDiagnostique ?? new Response(404)
                    )
                }
            )
        }
    )
    this.put(
        'intervention-service-offers/:serviceOfferId/activites-diagnostique/:activiteId',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema) => {
                    if (
                        request.requestHeaders?.['content-type'] !==
                        'application/json'
                    ) {
                        return new Response(415)
                    }
                    {
                        const activiteInitiale = schema.find(
                            'activiteDiagnostique',
                            request.params.activiteId
                        )
                        if (isNull(activiteInitiale)) {
                            return new Response(404)
                        }
                        const editActiviteDto: EditActiviteDto = JSON.parse(
                            request.requestBody
                        )

                        activiteInitiale.update(editActiviteDto)

                        return activiteInitiale
                    }
                }
            )
        }
    )
}
