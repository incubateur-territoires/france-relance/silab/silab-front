import { CreateMesureDto } from '@/interventionServiceOffer/entities/mesure/CreateMesureDto'
import {
    authenticationProtectedResponse,
    extractLoggedEmail,
} from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'
import { isDefined, isNull, isUndefined } from '@/shared/helpers/types'
import { Response } from 'miragejs'

export function useCompteurEndPoints(this: AppServer) {
    this.get(
        'intervention-service-offers/:serviceOfferId/equipements/:id/compteurs',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const availableEquipementsIds = [request.params.id]

                    if (request.queryParams.includeChildren) {
                        const eqpt = schema.find(
                            'equipement',
                            request.params.id
                        )
                        if (isDefined(eqpt?.children)) {
                            eqpt.children.models.forEach((child) => {
                                availableEquipementsIds.push(child.id)
                            })
                        }
                    }
                    return schema.all('compteur').filter((compteur) => {
                        if (isUndefined(compteur?.equipement?.id)) {
                            return false
                        }
                        return availableEquipementsIds.includes(
                            compteur.equipement.id
                        )
                    })
                }
            )
        }
    )
    this.post(
        `intervention-service-offers/:serviceOfferId/compteurs/:compteurId/mesures`,
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    if (
                        request.requestHeaders['content-type'] !==
                        'application/json'
                    ) {
                        return new Response(415)
                    }

                    const compteur = schema.find(
                        'compteur',
                        request.params.compteurId
                    )

                    if (isNull(compteur)) {
                        return new Response(404)
                    }

                    const nouvelleMesure: CreateMesureDto = JSON.parse(
                        request.requestBody
                    )

                    const nouvelleMesureCrée = {
                        valeur: nouvelleMesure.valeur,
                        createdAt: new Date().toISOString(),
                        createdBy: extractLoggedEmail(request),
                    }
                    compteur.update({
                        derniereMesure: nouvelleMesureCrée,
                    })
                    return nouvelleMesureCrée
                }
            )
        }
    )
}
