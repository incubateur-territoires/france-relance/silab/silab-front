import {
    authenticationProtectedResponse,
    uploadFileToLocalFileServer,
} from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'
import { isNull } from '@/shared/helpers/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Response } from 'miragejs'

export function useDemandeInterventionImagesEndpoints(this: AppServer) {
    this.post(
        'intervention-service-offers/:serviceOfferId/demandes-interventions/:demandeInterventionId/images',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                async (schema, request) => {
                    const demandeIntervention = schema.find(
                        'demandeIntervention',
                        request.params.demandeInterventionId
                    )
                    if (isNull(demandeIntervention)) {
                        return new Response(404)
                    }
                    const existingImages = demandeIntervention.images
                    const body: { base64: string } = JSON.parse(
                        request.requestBody
                    )
                    const uploadedFile = await uploadFileToLocalFileServer(
                        body.base64
                    )

                    const addedImage = {
                        id: faker.string.uuid(),
                        url: uploadedFile.url,
                    }
                    demandeIntervention.update({
                        images: [...existingImages, addedImage],
                    })
                    return addedImage
                }
            )
        }
    )
    this.delete(
        'intervention-service-offers/:serviceOfferId/demandes-interventions/:demandeInterventionId/images/:id',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const demandeIntervention = schema.find(
                        'demandeIntervention',
                        request.params.demandeInterventionId
                    )
                    if (isNull(demandeIntervention)) {
                        return new Response(404)
                    }
                    const imagesActualisées = demandeIntervention.images.filter(
                        (image) => {
                            return image.id !== request.params.id
                        }
                    )

                    demandeIntervention.update({
                        images: imagesActualisées,
                    })

                    return new Response(204)
                }
            )
        }
    )
}
