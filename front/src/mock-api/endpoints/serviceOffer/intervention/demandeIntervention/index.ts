import { CreateDemandeInterventionDto } from '@/interventionServiceOffer/entities/demandeIntervention/CreateDemandeInterventionDto'
import { EditDemandeInterventionDto } from '@/interventionServiceOffer/entities/demandeIntervention/EditDemandeInterventionDto'
import {
    authenticationProtectedResponse,
    extractLoggedEmail,
    extractLoggedGmaoActeurDto,
} from '@/mock-api/helpers'
import { AppSchema, AppServer } from '@/mock-api/types'
import {
    isDefined,
    isNull,
    isNullOrUndefined,
    isUndefined,
} from '@/shared/helpers/types'
import { Request, Response } from 'miragejs'
import { useDemandeInterventionImagesEndpoints } from './image'
import {
    availableDemandeInterventionStatusLabels,
    useDemandeInterventionStatusEndpoints,
} from './status'
import { DemandeInterventionCoordinatesDto } from '@/interventionServiceOffer/entities/demandeIntervention/DemandeInterventionCoordinatesDto'

export function useDemandeInterventionEndpoints(this: AppServer) {
    useDemandeInterventionImagesEndpoints.call(this)
    useDemandeInterventionStatusEndpoints.call(this)
    this.post(
        'intervention-service-offers/:serviceOfferId/demandes-interventions',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                postDemandeIntervention
            )
        }
    )
    this.put(
        'intervention-service-offers/:serviceOfferId/demandes-interventions/:id',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                putDemandeIntervention
            )
        }
    )
    this.get(
        'intervention-service-offers/:serviceOfferId/demandes-interventions',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                getAllDemandeInterventions
            )
        }
    )
    this.get(
        'intervention-service-offers/:serviceOfferId/demandes-interventions-coordinates',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                getAllDemandeInterventionsCoordinates
            )
        }
    )
    this.get(
        'intervention-service-offers/:serviceOfferId/demandes-interventions/:id',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    return schema.find('demandeIntervention', request.params.id)
                }
            )
        }
    )
    this.delete(
        'intervention-service-offers/:serviceOfferId/demandes-interventions/:id',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const demandeIntervention = schema.find(
                        'demandeIntervention',
                        request.params.id
                    )
                    if (isNull(demandeIntervention)) {
                        return new Response(404)
                    }
                    demandeIntervention?.destroy()

                    return new Response(204)
                }
            )
        }
    )

    this.get(
        'intervention-service-offers/:serviceOfferId/equipements/:id/demandes-interventions',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const equipement = schema.find(
                        'equipement',
                        request.params.id
                    )
                    if (isNull(equipement)) {
                        return new Response(
                            404,
                            {},
                            { message: "Cet équipement n'existe pas" }
                        )
                    }

                    const availableEquipementsIds = [request.params.id]

                    if (isDefined(equipement.children)) {
                        equipement.children.models.forEach((child) => {
                            availableEquipementsIds.push(child.id)
                        })
                    }

                    const demandesIntervention = schema
                        .all('demandeIntervention')
                        .filter((demandeIntervention) => {
                            return (
                                isNullOrUndefined(
                                    request.queryParams.statuses
                                ) ||
                                request.queryParams.statuses.includes(
                                    demandeIntervention.currentStatus.code
                                )
                            )
                        })
                        .filter((demandeIntervention) => {
                            if (
                                isUndefined(
                                    demandeIntervention.relatedEquipement?.id
                                )
                            ) {
                                return false
                            }
                            return availableEquipementsIds.includes(
                                demandeIntervention.relatedEquipement.id
                            )
                        })

                    return demandesIntervention
                }
            )
        }
    )
}

async function postDemandeIntervention(schema: AppSchema, request: Request) {
    if (request.requestHeaders['content-type'] !== 'application/json') {
        return new Response(415)
    }

    const attrs: CreateDemandeInterventionDto = JSON.parse(request.requestBody)
    const createdAt = new Date().toISOString()
    const createdBy = extractLoggedGmaoActeurDto(request, schema)

    return schema.create('demandeIntervention', {
        ...attrs,
        code: 'DI-01',
        createdBy,
        createdAt,
        updatedAt: createdAt,
        images: [],
        relatedEquipement: isDefined(attrs.relatedEquipement)
            ? schema.find('equipement', attrs.relatedEquipement.id)
            : null,
        statusHistory: [
            {
                code: 'awaiting_validation',
                createdBy: extractLoggedEmail(request),
                createdAt,
                label: availableDemandeInterventionStatusLabels[
                    'awaiting_validation'
                ],
            },
        ],
        currentStatus: {
            code: 'awaiting_validation',
            createdBy: extractLoggedEmail(request),
            createdAt,
            label: availableDemandeInterventionStatusLabels[
                'awaiting_validation'
            ],
        },
        documentsJoints: [],
    })
}
function getAllDemandeInterventions(schema: AppSchema, request: Request) {
    const demandeInterventions = schema
        .all('demandeIntervention')
        .filter((demandeIntervention) => {
            return (
                isNullOrUndefined(request.queryParams.statuses) ||
                request.queryParams.statuses.includes(
                    demandeIntervention.currentStatus.code
                )
            )
        })
        .filter((demandeIntervention) => {
            return (
                isNullOrUndefined(request.queryParams['equipementRacine.id']) ||
                (!isNullOrUndefined(demandeIntervention.relatedEquipement) &&
                    request.queryParams['equipementRacine.id'].includes(
                        demandeIntervention.relatedEquipement.id
                    ))
            )
        })
        .filter((demandeIntervention) => {
            return (
                isNullOrUndefined(
                    request.queryParams['relatedEquipement.id']
                ) ||
                (!isNullOrUndefined(demandeIntervention.relatedEquipement) &&
                    request.queryParams['relatedEquipement.id'].includes(
                        demandeIntervention.relatedEquipement.id
                    ))
            )
        })
        .sort(
            (a, b) =>
                new Date(b.createdAt).valueOf() -
                new Date(a.createdAt).valueOf()
        )

    const page = Number(request.queryParams.page)
    const pageSize = 25
    const begin = (page - 1) * pageSize
    const demandesInterventionsResponse = demandeInterventions.slice(
        begin,
        begin + pageSize
    )
    return new Response(
        200,
        { 'X-Total-Count': String(demandeInterventions.length) },
        demandesInterventionsResponse
    )
}

function getAllDemandeInterventionsCoordinates(
    schema: AppSchema,
    request: Request
) {
    const demandeInterventions = schema
        .all('demandeIntervention')
        .filter((demandeIntervention) => {
            return (
                isNullOrUndefined(request.queryParams.statuses) ||
                request.queryParams.statuses.includes(
                    demandeIntervention.currentStatus.code
                )
            )
        })
        .filter((demandeIntervention) => {
            return (
                isNullOrUndefined(
                    request.queryParams['relatedEquipement.id']
                ) ||
                (!isNullOrUndefined(demandeIntervention.relatedEquipement) &&
                    request.queryParams['relatedEquipement.id'].includes(
                        demandeIntervention.relatedEquipement.id
                    ))
            )
        })

    const demandeInterventionCoordinates = demandeInterventions.models.reduce(
        (demandeInterventionCoordinates, demandeIntervention) => {
            if (
                isUndefined(demandeIntervention.relatedEquipement?.coordinates)
            ) {
                return demandeInterventionCoordinates
            }
            demandeInterventionCoordinates.push({
                id: demandeIntervention.id,
                label: `${demandeIntervention.code} ${demandeIntervention.title}`.trim(),
                latitude: demandeIntervention.relatedEquipement.coordinates.lat,
                longitude:
                    demandeIntervention.relatedEquipement.coordinates.lng,
            })
            return demandeInterventionCoordinates
        },
        [] as DemandeInterventionCoordinatesDto[]
    )
    return new Response(
        200,
        { 'X-Total-Count': String(demandeInterventions.length) },
        demandeInterventionCoordinates
    )
}

async function putDemandeIntervention(schema: AppSchema, request: Request) {
    if (request.requestHeaders['content-type'] !== 'application/json') {
        return new Response(415)
    }

    const attrs: EditDemandeInterventionDto = JSON.parse(request.requestBody)

    const demandeIntervention = schema.find(
        'demandeIntervention',
        request.params.id
    )

    demandeIntervention?.update({
        title: `${attrs.title}`,
        description: attrs.description,
        coordonnees: attrs.coordonnees,
        relatedEquipement: isDefined(attrs.relatedEquipement)
            ? schema.find('equipement', attrs.relatedEquipement.id)
            : null,
        dateDeDebutSouhaitee: attrs.dateDeDebutSouhaitee,
        dateDeFinSouhaitee: attrs.dateDeFinSouhaitee,
    })

    return demandeIntervention
}
