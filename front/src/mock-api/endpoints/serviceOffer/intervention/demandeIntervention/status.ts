import { DemandeInterventionStatusDto } from '@/interventionServiceOffer/entities/demandeIntervention/status/DemandeInterventionStatusDto'
import { AppServer, AppSchema } from '@/mock-api/types'
import {
    authenticationProtectedResponse,
    extractLoggedEmail,
} from '@/mock-api/helpers'
import { Request, Response } from 'miragejs'
import { isNullOrUndefined } from '@/shared/helpers/types'

export const availableDemandeInterventionStatusLabels: {
    [k in DemandeInterventionStatusDto['code']]: DemandeInterventionStatusDto['label']
} = {
    accepted: 'Attente Réalisation',
    awaiting_information: 'Attente information',
    awaiting_validation: 'Attente prise en compte',
    closed: 'Soldée',
}

export function useDemandeInterventionStatusEndpoints(this: AppServer) {
    this.post(
        'intervention-service-offers/:serviceOfferId/demandes-interventions/:id/statuses',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                postDemandeInterventionStatus
            )
        }
    )

    this.get(
        'intervention-service-offers/:serviceOfferId/demandes-interventions/:id/statusHistory',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const demandeIntervention = schema.find(
                        'demandeIntervention',
                        request.params.id
                    )
                    if (isNullOrUndefined(demandeIntervention)) {
                        return new Response(404)
                    }
                    return demandeIntervention.statusHistory
                }
            )
        }
    )
}
function postDemandeInterventionStatus(schema: AppSchema, request: Request) {
    if (request.requestHeaders['content-type'] !== 'application/json') {
        return new Response(415)
    }

    const receivedCode: DemandeInterventionStatusDto['code'] = JSON.parse(
        request.requestBody
    ).code
    const date = new Date().toISOString()
    const demandeInterventionStatus: DemandeInterventionStatusDto = {
        code: receivedCode,
        label: availableDemandeInterventionStatusLabels[receivedCode],
        createdAt: date,
        createdBy: extractLoggedEmail(request),
    }

    const demandeIntervention = schema.find(
        'demandeIntervention',
        request.params.id
    )

    demandeIntervention?.update({
        currentStatus: demandeInterventionStatus,
        updatedAt: date,
        statusHistory: [
            demandeInterventionStatus,
            ...demandeIntervention.statusHistory,
        ],
    })

    return demandeInterventionStatus
}
