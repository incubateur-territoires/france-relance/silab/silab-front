import { EquipementCoordinatesDto } from '@/interventionServiceOffer/entities/equipement/EquipementCoordinatesDto'
import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'
import {
    isNotNull,
    isNull,
    isNullOrUndefined,
    isUndefined,
} from '@/shared/helpers/types'
import { Response } from 'miragejs'

export function useEquipementEndpoints(this: AppServer) {
    this.get(
        'intervention-service-offers/:serviceOfferId/equipements/:id',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    return schema.find('equipement', request.params.id)
                }
            )
        }
    )
    this.post(
        'intervention-service-offers/:serviceOfferId/equipements/:id/comment',
        (schema, request) => {
            return authenticationProtectedResponse(schema, request, () => {
                const equipement = schema.find('equipement', request.params.id)
                if (isNull(equipement)) {
                    return new Response(
                        404,
                        {},
                        { message: "Cet équipement n'éxiste pas" }
                    )
                }

                const updatedEquipement = JSON.parse(request.requestBody)

                equipement.update(updatedEquipement)

                return equipement
            })
        }
    )
    this.get(
        'intervention-service-offers/:serviceOfferId/equipements',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const equipementRacineId =
                        request.queryParams['equipementRacine.id']
                    const equipements = schema
                        .all('equipement')
                        .filter((equipement) => {
                            if (isNullOrUndefined(equipementRacineId)) {
                                return true
                            }
                            const parentEquipement = schema.find(
                                'equipement',
                                equipementRacineId[0]
                            )
                            if (
                                isNotNull(parentEquipement) &&
                                Array.isArray(parentEquipement.children)
                            ) {
                                return parentEquipement.children.some(
                                    (child) => child.id === equipement.id
                                )
                            }

                            return false
                        })

                    const page = Number(request.queryParams.page)
                    const pageSize = 25

                    const begin = (page - 1) * pageSize
                    const equipementsResponse = equipements.slice(
                        begin,
                        begin + pageSize
                    )

                    return new Response(
                        200,
                        { 'X-Total-Count': String(equipements.length) },
                        equipementsResponse
                    )
                }
            )
        }
    )
    this.get(
        'intervention-service-offers/:serviceOfferId/equipements-coordinates',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const equipementRacineId =
                        request.queryParams['equipementRacine.id']
                    const equipements = schema
                        .all('equipement')
                        .filter((equipement) => {
                            if (isNullOrUndefined(equipementRacineId)) {
                                return true
                            }
                            const parentEquipement = schema.find(
                                'equipement',
                                equipementRacineId[0]
                            )
                            if (
                                isNotNull(parentEquipement) &&
                                Array.isArray(parentEquipement.children)
                            ) {
                                return parentEquipement.children.some(
                                    (child) => child.id === equipement.id
                                )
                            }

                            return false
                        })

                    const equipementsCoordinates = equipements?.models.reduce(
                        (équipementsCoordinates, équipement) => {
                            if (
                                isUndefined(équipement.coordinates?.lng) ||
                                isUndefined(équipement.coordinates.lat)
                            ) {
                                return équipementsCoordinates
                            }
                            équipementsCoordinates.push({
                                id: équipement.id,
                                label: `${équipement.code} ${équipement.label}`.trim(),
                                coordinates: {
                                    lat: équipement.coordinates.lat,
                                    lng: équipement.coordinates.lng,
                                },
                            })

                            return équipementsCoordinates
                        },
                        [] as EquipementCoordinatesDto[]
                    )

                    return new Response(200, {}, equipementsCoordinates)
                }
            )
        }
    )
    this.get(
        'gmao-configuration-interventions/:gmaoConfigurationInterventionId/equipements',
        (schema, request) => {
            return authenticationProtectedResponse(schema, request, () => {
                const carlConfigurationIntervention = schema.find(
                    'carlConfigurationIntervention',
                    request.params.gmaoConfigurationInterventionId
                )
                if (isNull(carlConfigurationIntervention)) {
                    return new Response(404)
                }
                const equipements = schema
                    .all('equipement')
                    .filter(
                        (equipement) =>
                            isNullOrUndefined(request.queryParams.id) ||
                            (!isUndefined(equipement.id) &&
                                request.queryParams.id.includes(equipement.id))
                    )
                    .filter(
                        (equipement) =>
                            isNullOrUndefined(
                                request.queryParams['currentStatus.code']
                            ) ||
                            (!isUndefined(equipement.currentStatus.code) &&
                                request.queryParams[
                                    'currentStatus.code'
                                ].includes(equipement.currentStatus.code))
                    )
                    .filter((equipement) => {
                        if (
                            isNullOrUndefined(
                                request.queryParams.labelOuCode
                            ) ||
                            !(
                                typeof request.queryParams.labelOuCode ===
                                'string'
                            )
                        ) {
                            return true
                        }

                        if (
                            equipement.label
                                .toLowerCase()
                                .match(
                                    request.queryParams.labelOuCode
                                        .toLowerCase()
                                        .replace(/\*/g, '.*')
                                ) !== null
                        ) {
                            return true
                        }

                        if (
                            equipement.code
                                .toLowerCase()
                                .match(
                                    request.queryParams.labelOuCode
                                        .toLowerCase()
                                        .replace(/\*/g, '.*')
                                ) !== null
                        ) {
                            return true
                        }
                        return false
                    })

                return equipements
            })
        }
    )
}
