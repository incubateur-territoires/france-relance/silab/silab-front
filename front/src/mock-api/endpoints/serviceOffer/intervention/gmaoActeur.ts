import {
    authenticationProtectedResponse,
    extractLoggedGmaoActeurDto,
} from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'

export function useGmaoActeurEndPoints(this: AppServer) {
    this.get(
        'intervention-service-offers/:serviceOfferId/myself',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const res = extractLoggedGmaoActeurDto(request, schema)
                    return res
                }
            )
        }
    )
    this.get(
        `intervention-service-offers/:serviceOfferId/myself/equipiers`,
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    return extractLoggedGmaoActeurDto(request, schema).team
                }
            )
        }
    )
    this.get(
        'intervention-service-offers/:serviceOfferId/gmao-acteurs',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema) => {
                    return schema.all('gmaoActeur')
                }
            )
        }
    )
}
