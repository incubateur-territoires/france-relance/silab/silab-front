import { CreateInterventionServiceOfferDto } from '@/interventionServiceOffer/entities/interventionServiceOffer/CreateInterventionServiceOfferDto'
import { EditInterventionServiceOfferDto } from '@/interventionServiceOffer/entities/interventionServiceOffer/EditInterventionServiceOfferDto'
import {
    INTERVENTION_SERVICE_OFFER_AVAILABLE_ROLES,
    scopeRoleFromId,
} from '@/mock-api/factories/ServiceOffer/intervention'
import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppSchema, AppServer } from '@/mock-api/types'
import { Request, Response } from 'miragejs'
import { useCompteurEndPoints } from './compteur'
import { useDemandeInterventionEndpoints } from './demandeIntervention'
import { useEquipementEndpoints } from './equipement'
import { useInterventionEndpoints } from './intervention'
import { useGmaoActeurEndPoints } from './gmaoActeur'
import { useActiviteActionEndpoints } from './activiteAction'
import { useActiviteDiagnostiqueEndpoints } from './activiteDiagnostique'
import { useOperationEndpoints } from './operation'

export function useInterventionServiceOfferEndpoints(this: AppServer) {
    useEquipementEndpoints.call(this)
    useActiviteActionEndpoints.call(this)
    useActiviteDiagnostiqueEndpoints.call(this)
    useInterventionEndpoints.call(this)
    useDemandeInterventionEndpoints.call(this)
    useCompteurEndPoints.call(this)
    useGmaoActeurEndPoints.call(this)
    useOperationEndpoints.call(this)
    this.get('intervention-service-offers', (schema) => {
        return schema
            .all('interventionServiceOffer')
            .sort((a, b) => (a.title < b.title ? -1 : 1))
    })

    this.get(
        'intervention-service-offers/:serviceOfferId',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                getInterventionServiceOffer
            )
        }
    )

    this.get(
        'intervention-service-offers/:serviceOfferId/available-equipements',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const interventionServiceOffer = schema.find(
                        'interventionServiceOffer',
                        request.params.serviceOfferId
                    )
                    return schema.all('equipement').filter((equipment) => {
                        return interventionServiceOffer?.availableEquipementsIds.includes(
                            equipment.id
                        )
                    })
                }
            )
        }
    )

    this.post('intervention-service-offers', (schema, request) => {
        return authenticationProtectedResponse(
            schema,
            request,
            postInterventionServiceOffer
        )
    })

    this.put(
        'intervention-service-offers/:interventionServiceOfferId',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                putInterventionServiceOffer
            )
        }
    )

    this.delete(
        'intervention-service-offers/:interventionServiceOfferId',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                deleteInterventionServiceOffer
            )
        }
    )
}

function getInterventionServiceOffer(schema: AppSchema, request: Request) {
    return schema.find(
        'interventionServiceOffer',
        request.params.serviceOfferId
    )
}

function putInterventionServiceOffer(schema: AppSchema, request: Request) {
    if (request.requestHeaders['content-type'] !== 'application/json') {
        return new Response(415)
    }

    const interventionServiceOffer: EditInterventionServiceOfferDto =
        JSON.parse(request.requestBody)

    const gmaoConfigurationId =
        interventionServiceOffer.gmaoConfiguration.split('/').at(-1) ?? ''

    const availableEquipements = schema.where('equipement', (equipement) =>
        interventionServiceOffer.availableEquipementsIds.includes(equipement.id)
    )

    const newInterventionServiceOffer = schema.find(
        'interventionServiceOffer',
        request.params.interventionServiceOfferId
    )
    newInterventionServiceOffer?.update({
        ...interventionServiceOffer,
        gmaoConfiguration: schema.findOrCreateBy(
            'carlConfigurationIntervention',
            { id: gmaoConfigurationId }
        ),
        availableEquipements,
    })

    return newInterventionServiceOffer
}

function postInterventionServiceOffer(schema: AppSchema, request: Request) {
    if (request.requestHeaders['content-type'] !== 'application/json') {
        return new Response(415)
    }

    const interventionServiceOffer: CreateInterventionServiceOfferDto =
        JSON.parse(request.requestBody)

    const gmaoConfigurationId =
        interventionServiceOffer.gmaoConfiguration.split('/').at(-1) ?? ''

    const availableEquipements = schema.where('equipement', (equipement) =>
        interventionServiceOffer.availableEquipementsIds.includes(equipement.id)
    )
    const newInterventionServiceOffer = schema.create(
        'interventionServiceOffer',
        {
            ...interventionServiceOffer,
            template: 'InterventionServiceOffer',
            gmaoConfiguration: schema.findOrCreateBy(
                'carlConfigurationIntervention',
                { id: gmaoConfigurationId }
            ),
            availableEquipements,
        }
    )

    newInterventionServiceOffer.update({
        availableRoles: getScopedAvailableRoles(newInterventionServiceOffer.id),
    })

    return newInterventionServiceOffer
}

function getScopedAvailableRoles(interventionServiceOfferId: string) {
    return INTERVENTION_SERVICE_OFFER_AVAILABLE_ROLES.map(
        scopeRoleFromId(interventionServiceOfferId)
    )
}

function deleteInterventionServiceOffer(schema: AppSchema, request: Request) {
    schema
        .find(
            'interventionServiceOffer',
            request.params.interventionServiceOfferId
        )
        ?.destroy()

    return new Response(204)
}
