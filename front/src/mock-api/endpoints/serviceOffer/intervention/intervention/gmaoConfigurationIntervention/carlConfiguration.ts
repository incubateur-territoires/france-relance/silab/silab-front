import { CreateCarlConfigurationInterventionDto } from '@/interventionServiceOffer/entities/gmaoConfigurationIntervention/carlConfigurationIntervention/CreateCarlConfigurationInterventionDto'
import { AppServer } from '@/mock-api/types'
import { isNull } from '@/shared/helpers/types'
import { Response } from 'miragejs'

export function useCarlConfigurationInterventionEndpoints(this: AppServer) {
    this.get('/carl-configurations-intervention', (schema) => {
        return schema
            .all('carlConfigurationIntervention')
            .sort((a, b) => (a.title < b.title ? -1 : 1))
    })

    this.get(
        '/carl-configurations-intervention/:carlConfigurationInterventionId',
        (schema, request) => {
            const carlConfigurationInterventionId =
                request.params.carlConfigurationInterventionId

            return schema.find(
                'carlConfigurationIntervention',
                carlConfigurationInterventionId
            )
        }
    )

    this.post('/carl-configurations-intervention', (schema, request) => {
        const attrs: CreateCarlConfigurationInterventionDto = JSON.parse(
            request.requestBody
        )

        //On convertie l'IRI stocké dans l'attribut carlClient du dto en id et on supprime cet attribut puis on stock l'id dans un attribut carlClientId
        const carlClientId = attrs.carlClient.split('/').at(-1) ?? ''

        return schema.create('carlConfigurationIntervention', {
            ...attrs,
            carlClient: schema.findOrCreateBy('carlClient', {
                id: carlClientId,
            }),
        })
    })

    this.put(
        '/carl-configurations-intervention/:carlConfigurationInterventionId',
        (schema, request) => {
            const attrs: CreateCarlConfigurationInterventionDto = JSON.parse(
                request.requestBody
            )

            //On convertie l'IRI stocké dans l'attribut carlClient du dto en id et on supprime cet attribut puis on stock l'id dans un attribut carlClientId
            const carlClientId = attrs.carlClient.split('/').at(-1) ?? ''

            const carlConfigurationIntervention = schema.find(
                'carlConfigurationIntervention',
                request.params.carlConfigurationInterventionId
            )
            carlConfigurationIntervention?.update({
                ...attrs,
                carlClient: schema.findOrCreateBy('carlClient', {
                    id: carlClientId,
                }),
            })

            return carlConfigurationIntervention
        }
    )

    this.delete(
        '/carl-configurations-intervention/:carlConfigurationInterventionId',
        (schema, request) => {
            const carlConfigurationIntervention = schema.find(
                'carlConfigurationIntervention',
                request.params.carlConfigurationInterventionId
            )
            if (isNull(carlConfigurationIntervention)) {
                return new Response(404)
            }
            carlConfigurationIntervention.destroy()

            return new Response(204)
        }
    )
}
