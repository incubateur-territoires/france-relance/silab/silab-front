import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppSchema, AppServer } from '@/mock-api/types'
import { useCarlConfigurationInterventionEndpoints } from './carlConfiguration'

export function useGmaoConfigurationInterventionEndpoints(this: AppServer) {
    useCarlConfigurationInterventionEndpoints.call(this)
    this.get('gmao-configuration-interventions', (schema, request) => {
        return authenticationProtectedResponse(
            schema,
            request,
            getAllGmaoConfigurationIntervention
        )
    })
}
function getAllGmaoConfigurationIntervention(schema: AppSchema) {
    return schema.all('carlConfigurationIntervention')
}
