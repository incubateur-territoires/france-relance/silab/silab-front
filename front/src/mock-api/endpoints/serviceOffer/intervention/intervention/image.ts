import { InterventionImageType } from '@/interventionServiceOffer/entities/intervention/InterventionApi'
import {
    authenticationProtectedResponse,
    uploadFileToLocalFileServer,
} from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'
import { SilabImageDto } from '@/shared/document/entities/SilabImageDto'
import { isNull } from '@/shared/helpers/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Response } from 'miragejs'

function getInterventionAttributeKeyFromRequestParam(
    requestParam: InterventionImageType
) {
    return requestParam === 'images-avant' ? 'imagesAvant' : 'imagesApres'
}

export function useInterventionImagesEndpoints(this: AppServer) {
    this.post(
        'intervention-service-offers/:serviceOfferId/interventions/:interventionId/:imagesType',
        (schema, request) => {
            const imagesAttributeKey =
                getInterventionAttributeKeyFromRequestParam(
                    request.params.imagesType as InterventionImageType
                )
            const intervention = schema.find(
                'intervention',
                request.params.interventionId
            )
            const existingImages = intervention?.[imagesAttributeKey] ?? []

            return authenticationProtectedResponse(
                schema,
                request,
                async (schema, request) => {
                    const uploadedFile = await uploadFileToLocalFileServer(
                        JSON.parse(request.requestBody).base64
                    )
                    if (isNull(intervention)) {
                        return new Response(404)
                    }

                    const addedImage: SilabImageDto = {
                        id: faker.string.uuid(),
                        url: uploadedFile.url,
                    }
                    intervention.update({
                        [imagesAttributeKey]: [...existingImages, addedImage],
                    })
                    return addedImage
                }
            )
        }
    )
    this.delete(
        'intervention-service-offers/:serviceOfferId/interventions/:interventionId/:imagesType/:id',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const imagesAttributeKey =
                        getInterventionAttributeKeyFromRequestParam(
                            request.params.imagesType as InterventionImageType
                        )
                    const intervention = schema.find(
                        'intervention',
                        request.params.interventionId
                    )
                    const imagesActualisées =
                        intervention?.[imagesAttributeKey]?.filter((image) => {
                            return image.id !== request.params.id
                        }) ?? []

                    intervention?.update({
                        [imagesAttributeKey]: imagesActualisées,
                    })
                    return new Response(204)
                }
            )
        }
    )
}
