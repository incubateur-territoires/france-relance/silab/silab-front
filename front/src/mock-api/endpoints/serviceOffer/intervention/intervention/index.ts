import { CreateInterventionDto } from '@/interventionServiceOffer/entities/intervention/CreateInterventionDto'
import { EditInterventionDto } from '@/interventionServiceOffer/entities/intervention/EditInterventionDto'
import { InterventionPriority } from '@/interventionServiceOffer/entities/intervention/InterventionDto'
import {
    authenticationProtectedResponse,
    extractLoggedEmail,
    extractLoggedGmaoActeurDto,
} from '@/mock-api/helpers'
import { AppRegistry, AppSchema, AppServer } from '@/mock-api/types'
import {
    isDefined,
    isNull,
    isNullOrUndefined,
    isUndefined,
} from '@/shared/helpers/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Collection, Instantiate, Request, Response } from 'miragejs'
import { AnyResponse } from 'miragejs/-types'
import { useGmaoConfigurationInterventionEndpoints } from './gmaoConfigurationIntervention'
import { useInterventionImagesEndpoints } from './image'
import { useInterventionStatusEndpoints } from './status'
import { InterventionHistorisedStatusDto } from '@/interventionServiceOffer/entities/intervention/status/InterventionHistorisedStatusDto'
import { InterventionCoordinatesDto } from '@/interventionServiceOffer/entities/intervention/InterventionCoordinatesDto'
import { UpdateInterventionPourSoldageDtoSchema } from '@/interventionServiceOffer/entities/intervention/pourSoldage/UpdateInterventionPourSoldageDto'

export function useInterventionEndpoints(this: AppServer) {
    useInterventionImagesEndpoints.call(this)
    useGmaoConfigurationInterventionEndpoints.call(this)
    useInterventionStatusEndpoints.call(this)
    this.get(
        'intervention-service-offers/:serviceOfferId/interventions/:id',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    return schema.find('intervention', request.params.id)
                }
            )
        }
    )
    this.get(
        'intervention-service-offers/:serviceOfferId/interventions',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                getAllInterventions
            )
        }
    )
    this.get(
        'intervention-service-offers/:serviceOfferId/interventions-coordinates',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                getAllInterventionsCoordinates
            )
        }
    )
    this.post(
        'intervention-service-offers/:serviceOfferId/interventions',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                postNewIntervention
            )
        }
    )
    this.put(
        'intervention-service-offers/:serviceOfferId/interventions/:id/attributs-lies-au-soldage',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    if (
                        request.requestHeaders?.['content-type'] !==
                        'application/json'
                    ) {
                        return new Response(415)
                    }

                    const intervention = schema.find(
                        'intervention',
                        request.params.id
                    )
                    if (isNull(intervention)) {
                        return new Response(
                            404,
                            {},
                            { error: 'Intervention non trouvée' }
                        )
                    }

                    const interventionPourSoldageDto =
                        UpdateInterventionPourSoldageDtoSchema.parse(
                            JSON.parse(request.requestBody)
                        )

                    intervention.materielConsomme =
                        interventionPourSoldageDto.materielConsomme
                    intervention.dateDeDebut =
                        interventionPourSoldageDto.dateDeDebut
                    intervention.dateDeFin =
                        interventionPourSoldageDto.dateDeFin

                    intervention.update(intervention.attrs)

                    return intervention
                }
            )
        }
    )

    this.put(
        'intervention-service-offers/:serviceOfferId/interventions/:id',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                putIntervention
            )
        }
    )

    this.get(
        'intervention-service-offers/:serviceOfferId/equipements/:id/interventions',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const equipement = schema.find(
                        'equipement',
                        request.params.id
                    )
                    if (isNull(equipement)) {
                        return new Response(
                            404,
                            {},
                            { message: "Cet équipement n'existe pas" }
                        )
                    }

                    const availableEquipementsIds = [request.params.id]

                    if (isDefined(equipement.children)) {
                        equipement.children.models.forEach((child) => {
                            availableEquipementsIds.push(child.id)
                        })
                    }

                    const interventions = schema
                        .all('intervention')
                        .filter((intervention) => {
                            return (
                                isNullOrUndefined(
                                    request.queryParams.statuses
                                ) ||
                                (!isUndefined(intervention.currentStatus) &&
                                    request.queryParams.statuses.includes(
                                        intervention.currentStatus.code
                                    ))
                            )
                        })
                        .filter((intervention) => {
                            if (
                                isUndefined(intervention.relatedEquipement?.id)
                            ) {
                                return false
                            }
                            return availableEquipementsIds.includes(
                                intervention.relatedEquipement.id
                            )
                        })

                    return interventions
                }
            )
        }
    )
}

function getAllInterventions(schema: AppSchema, request: Request): AnyResponse {
    const unsortedInterventions = filtrerLesInterventionSelonLesQueryParams(
        schema.all('intervention'),
        request.queryParams
    )

    const page = Number(request.queryParams.page)
    const pageSize = 25
    const sortedInterventions = sortInterventions(
        unsortedInterventions,
        request.queryParams.orderBy
    )
    const begin = (page - 1) * pageSize
    const interventionsResponse = sortedInterventions.slice(
        begin,
        begin + pageSize
    )

    return new Response(
        200,
        { 'X-Total-Count': String(sortedInterventions.length) },
        interventionsResponse
    )
}

function getAllInterventionsCoordinates(
    schema: AppSchema,
    request: Request
): AnyResponse {
    const unsortedInterventions = filtrerLesInterventionSelonLesQueryParams(
        schema.all('intervention'),
        request.queryParams
    )

    const interventionCoordinates = unsortedInterventions.models.reduce(
        (interventionCoordinates, intervention) => {
            if (
                isUndefined(intervention.longitude) ||
                isUndefined(intervention.latitude)
            ) {
                return interventionCoordinates
            }
            interventionCoordinates.push({
                id: intervention.id,
                label: `${intervention.code} ${intervention.title}`.trim(),
                latitude: intervention.latitude,
                longitude: intervention.longitude,
            })

            return interventionCoordinates
        },
        [] as InterventionCoordinatesDto[]
    )

    return new Response(200, {}, interventionCoordinates)
}

function filtrerLesInterventionSelonLesQueryParams(
    interventions: Collection<Instantiate<AppRegistry, 'intervention'>>,
    queryParams: Record<string, string[] | string | null | undefined>
): Collection<Instantiate<AppRegistry, 'intervention'>> {
    return interventions
        .filter((intervention) => {
            // Filtrer par statut si spécifié dans les queryParams
            return (
                isNullOrUndefined(queryParams.statuses) ||
                (!isUndefined(intervention.currentStatus) &&
                    queryParams.statuses.includes(
                        intervention.currentStatus.code
                    ))
            )
        })
        .filter((intervention) => {
            // Filtrer par équipement parent si spécifié dans les queryParams
            return (
                isNullOrUndefined(queryParams['equipementRacine.id']) ||
                (!isNullOrUndefined(intervention.relatedEquipement) &&
                    queryParams['equipementRacine.id'].includes(
                        intervention.relatedEquipement.id
                    ))
            )
        })
        .filter((intervention) => {
            // Filtrer par équipement si spécifié dans les queryParams
            return (
                //amélioration: ca doit faire partie de la liste des équipement de l'ods si yen a
                isNullOrUndefined(queryParams['relatedEquipement.id']) ||
                (!isNullOrUndefined(intervention.relatedEquipement) &&
                    queryParams['relatedEquipement.id'].includes(
                        intervention.relatedEquipement.id
                    ))
            )
        })
        .filter((intervention) => {
            // Filtrer par plusieurs intervenants affectés si spécifié dans les queryParams
            const intervenantsAffectesIds =
                queryParams['intervenantsAffectes.id']
            return (
                isNullOrUndefined(intervenantsAffectesIds) ||
                intervention.intervenantsAffectes.models.some((intervenant) =>
                    intervenantsAffectesIds.includes(intervenant.attrs.id)
                )
            )
        })
        .filter((intervention) => {
            return (
                isNullOrUndefined(queryParams.priority) ||
                (isDefined(intervention.priority) &&
                    queryParams.priority.includes(intervention.priority))
            )
        })
        .filter((intervention) => {
            // Filtrer avant la date de début si spécifié dans les queryParams
            const avantDateDeDebut = queryParams['dateDeDebut[before]']
            return (
                isNullOrUndefined(avantDateDeDebut) ||
                typeof avantDateDeDebut !== 'string' ||
                new Date(intervention.dateDeDebut) < new Date(avantDateDeDebut)
            )
        })
        .filter((intervention) => {
            // Filtrer après la date de début si spécifié dans les queryParams
            const afterBeginDate = queryParams.afterBeginDate
            return (
                isNullOrUndefined(afterBeginDate) ||
                typeof afterBeginDate !== 'string' ||
                new Date(intervention.dateDeDebut) > new Date(afterBeginDate)
            )
        })
        .filter((intervention) => {
            // Filtrer avant la date de fin si spécifié dans les queryParams
            const beforeEndDate = queryParams.beforeEndDate
            return (
                isNullOrUndefined(beforeEndDate) ||
                typeof beforeEndDate !== 'string' ||
                new Date(intervention.dateDeFin) < new Date(beforeEndDate)
            )
        })
        .filter((intervention) => {
            // Filtrer après la date de fin si spécifié dans les queryParams
            const afterEndDate = queryParams.afterEndDate
            return (
                isNullOrUndefined(afterEndDate) ||
                typeof afterEndDate !== 'string' ||
                new Date(intervention.dateDeFin) > new Date(afterEndDate)
            )
        })
}

function sortInterventions(
    unsortedInterventions: Collection<Instantiate<AppRegistry, 'intervention'>>,
    orderBy: Request['queryParams']['orderBy']
): typeof unsortedInterventions {
    if (!orderBy) {
        return unsortedInterventions
    }
    const orderByArray = typeof orderBy === 'string' ? [orderBy] : orderBy
    orderBy = orderByArray.shift()
    const orderedPriorities = Object.values(InterventionPriority)
    switch (orderBy) {
        case 'createdAt':
            return unsortedInterventions.sort((a, b) => {
                return (
                    new Date(a.createdAt).getTime() -
                    new Date(b.createdAt).getTime()
                )
            })
        case '-createdAt':
            return unsortedInterventions.sort((a, b) => {
                return (
                    new Date(b.createdAt).getTime() -
                    new Date(a.createdAt).getTime()
                )
            })
        case 'updatedAt':
            return unsortedInterventions.sort((a, b) => {
                return (
                    new Date(a.currentStatus?.createdAt ?? 0).getTime() -
                    new Date(b.currentStatus?.createdAt ?? 0).getTime()
                )
            })
        case '-updatedAt':
            return unsortedInterventions.sort((a, b) => {
                return (
                    new Date(b.currentStatus?.createdAt ?? 0).getTime() -
                    new Date(a.currentStatus?.createdAt ?? 0).getTime()
                )
            })
        case 'priority':
            return unsortedInterventions.sort((a, b) => {
                if (
                    orderedPriorities.indexOf(a.priority) !==
                    orderedPriorities.indexOf(b.priority)
                ) {
                    return (
                        orderedPriorities.indexOf(b.priority) -
                        orderedPriorities.indexOf(a.priority)
                    )
                }
                return (
                    new Date(a.createdAt).getTime() -
                    new Date(b.createdAt).getTime()
                )
            })
        case '-priority':
            return unsortedInterventions.sort((a, b) => {
                if (
                    orderedPriorities.indexOf(b.priority) !==
                    orderedPriorities.indexOf(a.priority)
                ) {
                    return (
                        orderedPriorities.indexOf(a.priority) -
                        orderedPriorities.indexOf(b.priority)
                    )
                }
                return (
                    new Date(a.createdAt).getTime() -
                    new Date(b.createdAt).getTime()
                )
            })
        default:
            return unsortedInterventions
    }
}

function postNewIntervention(schema: AppSchema, request: Request) {
    if (request.requestHeaders['content-type'] !== 'application/json') {
        return new Response(415)
    }
    const interventionActionTypesMap = schema.find(
        'interventionServiceOffer',
        request.params.serviceOfferId
    )?.gmaoConfiguration?.actionTypesMap

    if (isUndefined(interventionActionTypesMap)) {
        return new Response(404)
    }

    const createInterventionDto: CreateInterventionDto = JSON.parse(
        request.requestBody
    )

    const actionType = {
        id: createInterventionDto.actionType?.id ?? faker.string.uuid(),
        label: createInterventionDto.actionType
            ? interventionActionTypesMap[createInterventionDto.actionType.id]
            : faker.word.adjective(),
    }
    const intervention = schema.create('intervention', {
        ...createInterventionDto,
        description: createInterventionDto.description,
        createdAt: new Date().toISOString(),
        createdBy: extractLoggedGmaoActeurDto(request, schema),
        code: 'OT-01',
        actionType,
        priority: createInterventionDto.priority,
        userEmail: extractLoggedEmail(request),
        imagesAvant: [],
        imagesApres: [],
        demandeInterventionIds: [],
        relatedEquipement: isDefined(createInterventionDto.relatedEquipement)
            ? schema.find(
                  'equipement',
                  createInterventionDto.relatedEquipement.id
              )
            : null,
        documentsJoints: [],
        intervenantsAffectes: [],
    })
    const interventionStatus: InterventionHistorisedStatusDto = {
        code: 'awaiting_realisation',
        label: 'A faire',
        createdBy: extractLoggedEmail(request),
        createdAt: new Date().toISOString(),
    }
    intervention.update({
        code:
            'OT-' +
            faker.string.numeric({
                length: 6,
                allowLeadingZeros: true,
            }),
        statusHistory: [interventionStatus],
        currentStatus: interventionStatus,
        activitesAction: [],
        activitesDiagnostique: [],
    })

    return intervention
}

function putIntervention(schema: AppSchema, request: Request) {
    if (request.requestHeaders['content-type'] !== 'application/json') {
        return new Response(415)
    }

    const interventionActionTypesMap = schema.find(
        'interventionServiceOffer',
        request.params.serviceOfferId
    )?.gmaoConfiguration?.actionTypesMap

    if (isUndefined(interventionActionTypesMap)) {
        return new Response(404)
    }

    const attrs: EditInterventionDto = JSON.parse(request.requestBody)

    const intervention = schema.find('intervention', request.params.id)

    const actionType = isNullOrUndefined(attrs.actionType)
        ? undefined
        : {
              id: attrs.actionType.id,
              label: interventionActionTypesMap[attrs.actionType.id],
          }

    intervention?.update({
        title: attrs.title,
        description: attrs.description,
        actionType,
        priority: attrs.priority,
        latitude: attrs.latitude,
        longitude: attrs.longitude,
        materielConsomme: attrs.materielConsomme,
        dateDeDebut: attrs.dateDeDebut,
        dateDeFin: attrs.dateDeFin,
    })

    return intervention
}
