import { InterventionHistorisedStatusDto } from '@/interventionServiceOffer/entities/intervention/status/InterventionHistorisedStatusDto'
import {
    authenticationProtectedResponse,
    extractLoggedEmail,
} from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'
import { isDefined, isNull, isNullOrUndefined } from '@/shared/helpers/types'
import { Response } from 'miragejs'

export function useInterventionStatusEndpoints(this: AppServer) {
    this.post(
        `intervention-service-offers/:serviceOfferId/interventions/:interventionId/statuses`,
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    if (
                        request.requestHeaders['content-type'] !==
                        'application/json'
                    ) {
                        return new Response(415)
                    }

                    const createdInterventionStatusDto: InterventionHistorisedStatusDto =
                        {
                            ...JSON.parse(request.requestBody),
                            label: 'Réalisé',
                            createdBy: extractLoggedEmail(request),
                            createdAt: new Date().toISOString(),
                        }

                    const intervention = schema.find(
                        'intervention',
                        request.params.interventionId
                    )

                    if (isNull(intervention)) {
                        return new Response(404)
                    }

                    if (isDefined(intervention.materielConsomme)) {
                        createdInterventionStatusDto.label =
                            'Attente de recomplètement'
                        createdInterventionStatusDto.comment = `${intervention.materielConsomme} Via Siilab par ${extractLoggedEmail(request)}`
                    }

                    intervention.update({
                        statusHistory: [
                            createdInterventionStatusDto,
                            ...intervention.statusHistory,
                        ],
                        currentStatus: createdInterventionStatusDto,
                    })
                    return createdInterventionStatusDto
                }
            )
        }
    )

    this.get(
        'intervention-service-offers/:serviceOfferId/interventions/:id/statusHistory',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const intervention = schema.find(
                        'intervention',
                        request.params.id
                    )
                    if (isNullOrUndefined(intervention)) {
                        return new Response(404)
                    }
                    return intervention.statusHistory
                }
            )
        }
    )
}
