import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'

export function useOperationEndpoints(this: AppServer) {
    this.get(
        'intervention-service-offers/:serviceOfferId/myself/operations',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema) => {
                    return schema.all('operation')
                }
            )
        }
    )
}
