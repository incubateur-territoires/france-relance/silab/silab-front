import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'

export function useArticleEndpoints(this: AppServer) {
    this.get(
        'logistic-service-offers/:serviceOfferId/articles',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    return schema.all('article').filter((article) => {
                        return (
                            typeof request.queryParams.code !== 'string' ||
                            article.code.includes(request.queryParams.code)
                        )
                    })
                }
            )
        }
    )
}
