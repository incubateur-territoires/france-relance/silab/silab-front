import { AnyResponse } from 'miragejs/-types'
import { ArticleReservationIssueDto } from '@/logisticServiceOffer/entities/articleReservationIssue/ArticleReservationIssueDto'
import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppSchema, AppServer } from '@/mock-api/types'
import { isDefined } from '@/shared/helpers/types'
import { Request, Response } from 'miragejs'

export function useArticleReservationsEndpoints(this: AppServer) {
    this.get(
        'logistic-service-offers/:serviceOfferId/article-reservations',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const logisticServiceOffer = schema.find(
                        'articleReservation',
                        request.params.serviceOfferId
                    )

                    return schema.where('articleReservation', {
                        ...request.queryParams,
                        warehouseId: logisticServiceOffer?.warehouseId,
                    })
                }
            )
        }
    )
    this.get(
        'logistic-service-offers/:serviceOfferId/interventions/:interventionId/article-reservations',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    return schema.where('articleReservation', {
                        interventionId: request.params.interventionId,
                    })
                }
            )
        }
    )
    this.post(
        'logistic-service-offers/:serviceOfferId/article-reservations/:articleReservationId/article-reservation-issues',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                postArticleReservationIssue
            )
        }
    )
}

function postArticleReservationIssue(
    schema: AppSchema,
    request: Request
): AnyResponse {
    if (request.requestHeaders['content-type'] !== 'application/json') {
        return new Response(415)
    }

    const articleReservationIssue: ArticleReservationIssueDto = JSON.parse(
        request.requestBody
    )
    const articleReservation = schema.findBy('articleReservation', {
        articleId: articleReservationIssue.articleId,
        interventionId: articleReservationIssue.interventionId,
    })

    if (
        // s'il n'y a pas de réservation correspondante
        articleReservation === null ||
        // si on nous demande de ne rien sortir ou même a "rentrer" du stock
        articleReservationIssue.quantity <= 0 ||
        // si on nous demande de sortir plus que prévu par la résa
        articleReservationIssue.quantity > articleReservation.quantity ||
        // si pas assez de stock
        articleReservationIssue.quantity > articleReservation.availableQuantity
    ) {
        // on ne sort rien
        articleReservationIssue.quantity = 0
        return articleReservationIssue
    }

    if (articleReservationIssue.quantity === articleReservation.quantity) {
        // sortie complète, on supprime la réservation
        articleReservation.destroy()
    } else {
        // sortie partielle, on met à jour la réservation
        articleReservation.update({
            quantity:
                articleReservation.quantity - articleReservationIssue.quantity,
            availableQuantity:
                articleReservation.availableQuantity -
                articleReservationIssue.quantity,
        })
    }

    schema.create('mouvementDeSortie', {
        article: {
            id: articleReservation.articleId,
            code: articleReservation.articleCode,
            titre: articleReservation.articleDescription,
            unité: articleReservation.unit,
        },
        quantite: articleReservationIssue.quantity,
        emplacement:
            isDefined(articleReservation?.storageLocationId) &&
            isDefined(articleReservation?.storageLocationCode)
                ? {
                      id: articleReservation.storageLocationId,
                      code: articleReservation.storageLocationCode,
                  }
                : undefined,
        createdBy: articleReservationIssue.warehouseClerkEmail,
        createdAt: new Date().toISOString(),
        intervention: isDefined(articleReservation.interventionId)
            ? {
                  id: articleReservation.interventionId,
              }
            : undefined,
    })
    return articleReservationIssue
}
