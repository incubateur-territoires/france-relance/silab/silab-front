import { AppServer } from '@/mock-api/types'
import { useArticleEndpoints } from './article'
import { useArticleReservationsEndpoints } from './articleReservation'
import { useInterventionEndpoints } from './intervention'
import { useMouvementEndpoints } from './mouvement'
import { useMouvementDeSortieEndpoints } from './mouvementDeSortie'

export function useLogistiqueServiceOffersEndpoints(this: AppServer) {
    useArticleEndpoints.call(this)
    useArticleReservationsEndpoints.call(this)
    useInterventionEndpoints.call(this)
    useMouvementEndpoints.call(this)
    useMouvementDeSortieEndpoints.call(this)

    this.get('/logistic-service-offers/:serviceofferid', (schema, request) => {
        return schema.find(
            'logisticServiceOffer',
            request.params.serviceofferid
        )
    })
    this.put('/logistic-service-offers/:serviceofferid', (schema, request) => {
        const logisticServiceOfferId = request.params.serviceofferid
        const newAttrs = JSON.parse(request.requestBody)

        const logisticServiceOffer = schema.find(
            'logisticServiceOffer',
            logisticServiceOfferId
        )
        logisticServiceOffer?.update(newAttrs)

        return logisticServiceOffer
    })
}
