import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'

export function useInterventionEndpoints(this: AppServer) {
    this.get(
        'logistic-service-offers/:serviceOfferId/interventions',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    return schema
                        .all('logisticIntervention')
                        .filter((intervention) => {
                            return (
                                typeof request.queryParams.code !== 'string' ||
                                intervention.code.includes(
                                    request.queryParams.code
                                )
                            )
                        })
                        .filter((intervention) => {
                            return (
                                typeof request.queryParams.titre !== 'string' ||
                                intervention.titre.includes(
                                    request.queryParams.titre
                                )
                            )
                        })
                }
            )
        }
    )

    this.get(
        'logistic-service-offers/:serviceOfferId/interventions/:id',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    return schema.find(
                        'logisticIntervention',
                        request.params.id
                    )
                }
            )
        }
    )
}
