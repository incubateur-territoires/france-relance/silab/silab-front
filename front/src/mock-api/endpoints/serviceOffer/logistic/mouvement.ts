import { CreateMouvementDto } from '@/logisticServiceOffer/entities/mouvement/CreateMouvementDto'
import { movementTypes } from '@/mock-api/factories/ServiceOffer/logistic/mouvement'
import {
    authenticationProtectedResponse,
    extractLoggedEmail,
} from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'
import { isNullOrUndefined } from '@/shared/helpers/types'
import { Response } from 'miragejs'

export function useMouvementEndpoints(this: AppServer) {
    this.post(
        'logistic-service-offers/:serviceOfferId/mouvements',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const createMouvementDto: CreateMouvementDto = JSON.parse(
                        request.requestBody
                    )
                    const articleDto = schema.find(
                        'article',
                        createMouvementDto.article.id
                    )

                    articleDto?.update({
                        lots: articleDto?.lots.map((lot) => {
                            if (
                                lot.emplacement?.id ===
                                createMouvementDto.emplacement?.id
                            ) {
                                lot.quantité += createMouvementDto.quantité
                            }
                            return lot
                        }),
                    })
                    return schema.create('mouvement', {
                        ...createMouvementDto,
                        typeDeMouvement: movementTypes.find(
                            (m) =>
                                m.id === createMouvementDto.typeDeMouvement.id
                        ),
                        createdBy: extractLoggedEmail(request),
                        createdAt: new Date().toISOString(),
                    })
                }
            )
        }
    )

    this.get(
        'logistic-service-offers/:serviceOfferId/mouvements',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const articleCodeParam = request.queryParams['article.code']
                    if (isNullOrUndefined(articleCodeParam)) {
                        return new Response(400)
                    }
                    const articleCode = Array.isArray(articleCodeParam)
                        ? articleCodeParam[0]
                        : articleCodeParam

                    const mouvements = schema.where(
                        'mouvement',
                        (mouvement) => {
                            return (
                                mouvement.article.id ===
                                schema.findBy('article', { code: articleCode })
                                    ?.id
                            )
                        }
                    )

                    return mouvements.sort((mouvementA, mouvementB) => {
                        return (
                            new Date(mouvementA.createdAt).getTime() -
                            new Date(mouvementB.createdAt).getTime()
                        )
                    })
                }
            )
        }
    )
}
