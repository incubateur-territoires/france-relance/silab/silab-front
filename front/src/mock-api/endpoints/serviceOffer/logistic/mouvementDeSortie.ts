import { authenticationProtectedResponse } from '@/mock-api/helpers'
import { AppServer } from '@/mock-api/types'

export function useMouvementDeSortieEndpoints(this: AppServer) {
    this.get(
        'logistic-service-offers/:serviceOfferId/interventions/:id/sorties-de-stock',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                (schema, request) => {
                    const mouvementsDeSortie = schema
                        .all('mouvementDeSortie')
                        .filter((mouvementDeSortie) => {
                            return (
                                mouvementDeSortie.interventionId ===
                                request.params.id
                            )
                        })

                    return mouvementsDeSortie.sort(
                        (mouvementDeSortieA, mouvementDeSortieB) => {
                            return (
                                new Date(
                                    mouvementDeSortieB.createdAt
                                ).getTime() -
                                new Date(mouvementDeSortieA.createdAt).getTime()
                            )
                        }
                    )
                }
            )
        }
    )
}
