import { AppServer } from '@/mock-api/types'
import { isNull } from '@/shared/helpers/types'
import { Response } from 'miragejs'

export function useSiilabConfigurationEndpoints(this: AppServer) {
    this.get('/siilab-configuration', (schema) => {
        const siilabConfiguration = schema.find('siilabConfiguration', '1')

        if (isNull(siilabConfiguration)) {
            return new Response(404)
        }

        return siilabConfiguration
    })

    this.put('/siilab-configuration', (schema, request) => {
        const attrs = JSON.parse(request.requestBody)
        const siilabConfiguration = schema.find('siilabConfiguration', '1')

        if (isNull(siilabConfiguration)) {
            return new Response(404)
        }

        siilabConfiguration.update(attrs)

        return siilabConfiguration
    })
}
