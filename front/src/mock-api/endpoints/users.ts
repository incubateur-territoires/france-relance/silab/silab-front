import { CreateUserDto } from '@/api/user/CreateUserDto'
import { EditUserDto } from '@/api/user/EditUserDto'
import { UserDto } from '@/api/user/UserDto'
import {
    authenticationProtectedResponse,
    extractLoggedEmail,
} from '@/mock-api/helpers'
import { AppSchema, AppServer } from '@/mock-api/types'
import { Request, Response } from 'miragejs'
import { AnyResponse } from 'miragejs/-types'

function userExistsWithEmail(schema: AppSchema, email: UserDto['email']) {
    return schema.findBy('user', { email }) !== null
}

export function useUserEndpoints(this: AppServer) {
    this.get('myself', (schema, request) => {
        return authenticationProtectedResponse(schema, request, (schema) => {
            return schema.findBy('user', {
                email: extractLoggedEmail(request),
            })
        })
    })
    this.get('users', (schema) => {
        return schema.all('user').sort((a, b) => (a.email < b.email ? -1 : 1))
    })
    this.get('users/:id')
    this.patch('users/:id', (schema, request) => {
        return authenticationProtectedResponse(schema, request, patchUser)
    })
    this.post('users', (schema, request) => {
        return authenticationProtectedResponse(schema, request, postUser)
    })
    this.post(
        'service-offers/:serviceOfferId/admin/users',
        (schema, request) => {
            return authenticationProtectedResponse(schema, request, postUser)
        }
    )
    this.put(
        'service-offers/:serviceOfferId/admin/users/:id',
        (schema, request) => {
            return authenticationProtectedResponse(
                schema,
                request,
                putUserByServiceOffer
            )
        }
    )
    this.get(
        'service-offers/:serviceOfferId/admin/users',
        (schema, request) => {
            const serviceOfferId = request.params.serviceOfferId
            const leRoleAppartienALODSregex = new RegExp(
                `^SERVICEOFFER_${serviceOfferId}_ROLE_(?<serviceOfferType>.[^_]+).*`
            )

            return schema
                .all('user')
                .filter((user) => {
                    return user.roles.some((role: string) =>
                        leRoleAppartienALODSregex.test(role)
                    )
                })
                .sort((a, b) => (a.email < b.email ? -1 : 1))
        }
    )
}

function postUser(schema: AppSchema, request: Request): AnyResponse {
    if (request.requestHeaders['content-type'] !== 'application/json') {
        return new Response(415)
    }

    const attrs: CreateUserDto = JSON.parse(request.requestBody)

    if (userExistsWithEmail(schema, attrs.email)) {
        return new Response(
            409,
            {},
            { detail: 'Un utilisateur avec cette adresse e-mail existe déjà.' }
        )
    }

    return schema.create('user', {
        ...attrs,
        reachableRoles: attrs.roles,
    })
}

function patchUser(schema: AppSchema, request: Request): AnyResponse {
    if (
        request.requestHeaders['content-type'] !==
        'application/merge-patch+json'
    ) {
        return new Response(415)
    }
    const userToUpdateId = request.params.id
    const userToUpdate = schema.find('user', userToUpdateId)

    const attrs: EditUserDto = JSON.parse(request.requestBody)

    userToUpdate?.update(attrs)

    return userToUpdate
}

function putUserByServiceOffer(
    schema: AppSchema,
    request: Request
): AnyResponse {
    if (request.requestHeaders['content-type'] !== 'application/json') {
        return new Response(415)
    }

    const existingUserId = request.params.id
    const existingUser = schema.find('user', existingUserId)

    if (!existingUser) {
        return new Response(404)
    }

    const attrs: EditUserDto = JSON.parse(request.requestBody)
    const existingRoles = existingUser.roles
    const existingRolesWithServiceOfferRolesRemoved = existingRoles.filter(
        (role) =>
            !role.startsWith(`SERVICEOFFER_${request.params.serviceOfferId}`)
    )
    attrs.roles.push(...existingRolesWithServiceOfferRolesRemoved)

    existingUser.update(attrs)

    return attrs
}
