import { interventionServiceOfferFactories } from './intervention'
import { logisticServiceOfferFactories } from './logistic'

export const serviceOfferFactories = {
    ...logisticServiceOfferFactories,
    ...interventionServiceOfferFactories,
}
