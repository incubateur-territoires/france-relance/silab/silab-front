import { ActiviteActionDto } from '@/interventionServiceOffer/entities/activite/ActiviteActionDto'
import { ActiviteActionForeignKeys } from '@/mock-api/models'
import { AfterCreate } from '@/mock-api/types'
import { isNull } from '@/shared/helpers/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'

export type ActiviteActionFactoryDTO = Omit<
    ActiviteActionDto,
    keyof ActiviteActionForeignKeys
>
const afterCreate: AfterCreate<'activiteAction'> = (activite, server) => {
    activite.update({
        intervenant: faker.helpers.arrayElement(
            server.schema.all('gmaoActeur').models
        ),
    })

    // une chance sur deux de créer une opération s'il n'y en a pas
    if (isNull(activite.operation) && faker.number.float() > 0.5) {
        activite.update({
            operation: server.create('operation'),
        })
    }
}
export const activiteActionFactory = Factory.extend<ActiviteActionFactoryDTO>({
    id() {
        return faker.string.uuid()
    },
    description() {
        return faker.lorem.sentence()
    },
    duree() {
        return undefined
    },
    dateDeDebut() {
        return undefined
    },
    isObligatoire() {
        return faker.datatype.boolean()
    },
    // @ts-expect-error voir: https://github.com/miragejs/miragejs/pull/1019
    afterCreate,
})
