import { ActiviteDiagnostiqueDto } from '@/interventionServiceOffer/entities/activite/ActiviteDiagnostiqueDto'
import { ActiviteDiagnostiqueForeignKeys } from '@/mock-api/models'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'

export type ActiviteDiagnostiqueFactoryDTO = Omit<
    ActiviteDiagnostiqueDto,
    keyof ActiviteDiagnostiqueForeignKeys
>

export const activiteDiagnostiqueFactory =
    Factory.extend<ActiviteDiagnostiqueFactoryDTO>({
        id() {
            return faker.string.uuid()
        },
        description() {
            return faker.lorem.sentence()
        },
        operation() {
            return {
                id: faker.string.uuid(),
                label: faker.lorem.sentence(),
            }
        },
        isObligatoire() {
            return faker.datatype.boolean()
        },
    })
