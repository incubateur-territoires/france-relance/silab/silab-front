import { CarlConfigurationInterventionDto } from '@/interventionServiceOffer/entities/gmaoConfigurationIntervention/carlConfigurationIntervention/CarlConfigurationInterventionDto'
import { CarlConfigurationInterventionForeignKeys } from '@/mock-api/models'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'
import { FlattenFactoryMethods } from 'miragejs/-types'

export type CarlConfigurationInterventionFactoryDto = Omit<
    CarlConfigurationInterventionDto,
    keyof CarlConfigurationInterventionForeignKeys
>

export const carlConfigurationInterventionFactory =
    Factory.extend<CarlConfigurationInterventionFactoryDto>({
        id: faker.string.uuid(),
        title: faker.lorem.words(),
        interventionCostCenterIdsFilter: [],
        costCenterIdForInterventionCreation: undefined,
        useEquipmentCostCenterForInterventionCreation: true,
        directions: [faker.lorem.words()],
        actionTypesMap: {},
        carlAttributesPresetForInterventionCreation: {
            attribute: faker.lorem.word(),
        },
        carlRelationshipsPresetForInterventionCreation: {
            relationship1: {
                id: faker.lorem.words(),
                type: faker.lorem.word(),
            },
            relationship2: {
                id: faker.lorem.words(),
                type: faker.lorem.word(),
            },
        },
        carlAttributesPresetForDemandeInterventionCreation: {
            attribute1: faker.lorem.word(),
            attribute2: faker.lorem.word(),
        },
        carlRelationshipsPresetForDemandeInterventionCreation: {
            relationship1: {
                id: faker.lorem.words(),
                type: faker.lorem.word(),
            },
            relationship2: {
                id: faker.lorem.words(),
                type: faker.lorem.word(),
            },
        },
        environnement(
            this: FlattenFactoryMethods<CarlConfigurationInterventionDto>
        ) {
            return this.carlClient.title
        },
        photoAvantInterventionDoctypeId: faker.lorem.words(),
        photoApresInterventionDoctypeId: faker.lorem.words(),
        woViewUrlSubPath: undefined,
        mrViewUrlSubPath: undefined,
        boxViewUrlSubPath: undefined,
        materialViewUrlSubPath: undefined,
        includeEquipementsChildrenWithType: [faker.lorem.word()],
    })
