import { CompteurDto } from '@/interventionServiceOffer/entities/compteur/CompteurDto'
import { CompteurForeignKeys } from '@/mock-api/models'
import { AfterCreate } from '@/mock-api/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'

export const compteurUnitsFloat = ['m³/s', 'ppm', 'mg/L', 'kW/h', 'm³']
export const compteurUnitsBoolean = [
    'Bouteilles',
    'Débordement',
    'Seuil critique',
    'Surchauffe',
    'Vide',
    'Défaut',
]

export type CompteurFactoryDto = Omit<CompteurDto, keyof CompteurForeignKeys>

const afterCreate: AfterCreate<'compteur'> = (compteur, server) => {
    compteur.update({
        // unlike HasMany, BelongsTo relationships are initialised to null
        // so we can do this
        equipement: compteur.equipement ?? server.create('equipement'),
    })
}

export const compteurFactory = Factory.extend<CompteurFactoryDto>({
    id() {
        return faker.string.uuid()
    },
    label() {
        return faker.person.lastName() + ' ' + faker.location.direction()
    },
    code() {
        return (
            'CP-' + faker.string.numeric({ length: 6, allowLeadingZeros: true })
        )
    },
    unite() {
        return this.type === 'boolean'
            ? faker.helpers.arrayElement(compteurUnitsBoolean)
            : faker.helpers.arrayElement(compteurUnitsFloat)
    },
    type() {
        const generateBinaryCompteur = faker.number.float() < 0.3

        return generateBinaryCompteur ? 'boolean' : 'float'
    },
    derniereMesure() {
        const generateWithoutLMesure = faker.number.float() < 0.3
        if (generateWithoutLMesure) {
            return
        }
        if (this.type === 'boolean') {
            return {
                createdAt: faker.date.recent().toISOString(),
                createdBy: faker.internet.email(),
                valeur: faker.number.int({ min: 0, max: 1 }),
            }
        }
        return {
            createdAt: faker.date.recent().toISOString(),
            createdBy: faker.internet.email(),
            valeur: faker.number.float({ fractionDigits: 2 }),
        }
    },
    // @ts-expect-error voir: https://github.com/miragejs/miragejs/pull/1019
    afterCreate,
})
