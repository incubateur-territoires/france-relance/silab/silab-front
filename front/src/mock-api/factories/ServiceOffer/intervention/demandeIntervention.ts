import { DemandeInterventionDto } from '@/interventionServiceOffer/entities/demandeIntervention/DemandeInterventionDto'
import { DemandeInterventionForeignKeys } from '@/mock-api/models'
import { AfterCreate } from '@/mock-api/types'
import { DocumentDto } from '@/shared/document/entities/DocumentDto'
import { isNullOrUndefined } from '@/shared/helpers/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'
import { Assign, FlattenFactoryMethods } from 'miragejs/-types'

export type DemandeInterventionFactoryDto = Assign<
    Omit<DemandeInterventionDto, keyof DemandeInterventionForeignKeys>,
    {
        statusHistory: DemandeInterventionDto['currentStatus'][]
    }
>

const afterCreate: AfterCreate<'demandeIntervention'> = (
    demandeIntervention,
    server
) => {
    if (isNullOrUndefined(demandeIntervention.createdBy)) {
        demandeIntervention.update({
            createdBy: server.create('gmaoActeur'),
        })
    }
}

export const demandeInterventionFactory =
    Factory.extend<DemandeInterventionFactoryDto>({
        id(n) {
            return `di-ecole-${n}`
        },
        code() {
            return (
                'DI-' +
                faker.string.numeric({ length: 6, allowLeadingZeros: true })
            )
        },
        title() {
            return faker.lorem.sentence(4)
        },
        description() {
            return faker.lorem.paragraph()
        },
        createdAt() {
            return faker.date.past().toISOString()
        },
        updatedAt(this: FlattenFactoryMethods<DemandeInterventionFactoryDto>) {
            return faker.date
                .between({
                    from: new Date(this.createdAt),
                    to: new Date(),
                })
                .toISOString()
        },
        address() {
            const générerUneAddress = faker.number.float() < 0.7
            if (générerUneAddress) {
                return `${faker.location.streetAddress()} ${faker.location.zipCode()} ${faker.location.city()}`
            }
            return undefined
        },
        coordonnees() {
            return undefined
        },

        images() {
            const nbImages = faker.number.int(3)
            const images = []
            for (let imageIndex = 0; imageIndex < nbImages; imageIndex++) {
                images.push({
                    id: faker.string.uuid(),
                    url: faker.image.url(),
                })
            }
            return images
        },
        statusHistory(
            this: FlattenFactoryMethods<DemandeInterventionFactoryDto>,
            i
        ) {
            const creationDate = new Date(this.createdAt)
            const awaitingInformationDate = faker.date.between({
                from: creationDate,
                to: Date.now(),
            })
            const informationSuppliedDate = faker.date.between({
                from: awaitingInformationDate,
                to: Date.now(),
            })
            const validationDate = faker.date.between({
                from: informationSuppliedDate,
                to: Date.now(),
            })
            const closedDate = faker.date.between({
                from: validationDate,
                to: Date.now(),
            })
            const statusHistory = [
                {
                    code: 'awaiting_validation',
                    label: 'En attente de validation',
                    createdAt: creationDate.toISOString(),
                    createdBy: faker.internet.email(),
                },
                {
                    code: 'awaiting_information',
                    label: 'Attente information',
                    createdAt: awaitingInformationDate.toISOString(),
                    createdBy: faker.internet.email(),
                    comment: 'Merci de préciser ' + faker.word.words(10),
                },
                {
                    code: 'awaiting_validation',
                    label: 'En attente de validation',
                    createdAt: informationSuppliedDate.toISOString(),
                    createdBy: faker.internet.email(),
                },
                {
                    code: 'accepted',
                    label: 'En cours de réalisation',
                    createdAt: validationDate.toISOString(),
                    createdBy: faker.internet.email(),
                },
                {
                    code: 'closed',
                    label: 'Soldée',
                    createdAt: closedDate.toISOString(),
                    createdBy: faker.internet.email(),
                },
            ] as const

            const numberOfStatuses = 1 + (i % statusHistory.length) // on s'assure d'alterner tous les types de status
            return statusHistory.slice(0, numberOfStatuses).reverse() //pour présenter en ordre décroissant
        },
        currentStatus(
            this: FlattenFactoryMethods<DemandeInterventionFactoryDto>
        ) {
            return this.statusHistory[0]
        },
        gmaoObjectViewUrl() {
            return faker.internet.url()
        },
        documentsJoints() {
            const nbDocuments = faker.number.int({ min: 0, max: 2 })
            const documents: DocumentDto[] = []
            for (
                let documentIndex = 0;
                documentIndex < nbDocuments;
                documentIndex++
            ) {
                documents.push({
                    id: faker.string.uuid(),
                    name: faker.word.words(),
                    url: faker.internet.url(),
                })
            }
            return documents
        },
        dateDeDebutSouhaitee() {
            return faker.date.past().toISOString()
        },
        dateDeFinSouhaitee() {
            return faker.date.past().toISOString()
        },
        // @ts-expect-error typage afterCreate cf: https://github.com/miragejs/miragejs/pull/1019
        afterCreate,
    })
