import { Equipement } from '@/interventionServiceOffer/entities/equipement/Equipement'
import { EquipementDto } from '@/interventionServiceOffer/entities/equipement/EquipementDto'
import { EquipementForeignKeys } from '@/mock-api/models'
import { AfterCreate } from '@/mock-api/types'
import { DocumentDto } from '@/shared/document/entities/DocumentDto'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'

export type EquipementFactoryDto = Omit<
    EquipementDto,
    keyof EquipementForeignKeys
>

const afterCreate: AfterCreate<'equipement'> = (equipement, server) => {
    if (
        // HasMany relationships are always initialised as empty collection
        // so test if it is empty before generating interventions
        equipement.compteurs.length === 0
    ) {
        equipement.update({
            compteurs: server.createList(
                'compteur',
                faker.number.int({ min: 0, max: 2 }),
                {
                    equipement: equipement,
                }
            ),
        })
    }

    server.createList('intervention', faker.number.int({ min: 1, max: 5 }), {
        relatedEquipement: equipement,
    })

    server.createList(
        'demandeIntervention',
        faker.number.int({ min: 1, max: 2 }),
        {
            relatedEquipement: equipement,
        }
    )
}

export const equipementFactory = Factory.extend<EquipementFactoryDto>({
    id() {
        return faker.string.uuid()
    },
    label() {
        return faker.person.lastName() + ' ' + faker.location.direction()
    },
    code() {
        return 'EQPT-' + faker.string.numeric(6)
    },
    coordinates() {
        const isGeolocated = faker.number.float() < 0.8
        const fakerCoordinates = faker.location.nearbyGPSCoordinate({
            origin: [46.58464530116541, 0.34471313835438444],
            radius: 10,
        })
        return isGeolocated
            ? {
                  lat: fakerCoordinates[0],
                  lng: fakerCoordinates[1],
              }
            : undefined
    },
    currentStatus() {
        return {
            code: 'active',
            label: 'Actif',
            createdAt: faker.date.past().toISOString(),
            createdBy: faker.person.fullName(),
        }
    },
    gmaoObjectViewUrl() {
        return faker.internet.url()
    },
    comment() {
        return faker.lorem.sentence()
    },
    imagesIllustration() {
        const nbImages = faker.number.int(2)
        const images = []
        for (let imageIndex = 0; imageIndex < nbImages; imageIndex++) {
            images.push({
                id: faker.string.uuid(),
                url: faker.image.urlLoremFlickr({ category: 'building' }),
            })
        }
        return images
    },
    documentsJoints() {
        const nbDocuments = faker.number.int({ min: 0, max: 2 })
        const documents: DocumentDto[] = []
        for (
            let documentIndex = 0;
            documentIndex < nbDocuments;
            documentIndex++
        ) {
            documents.push({
                id: faker.string.uuid(),
                name: faker.word.words(),
                url: faker.internet.url(),
            })
        }
        return documents
    },
    ancetres() {
        const lignéesAncetres: Equipement['ancetres'] = {}

        const numberOfLignées = 1

        for (let i = 0; i < numberOfLignées; i++) {
            const lignéeKey = faker.string.uuid()
            const numberOfAncetresInLignée = faker.number.int({
                min: 1,
                max: 6,
            })

            const lignéeItems = Array.from(
                { length: numberOfAncetresInLignée },
                () => ({
                    id: faker.string.uuid(),
                    label: `${faker.person.lastName()} ${faker.location.direction()}`,
                })
            )

            lignéesAncetres[lignéeKey] = lignéeItems
        }

        return lignéesAncetres
    },
    interventionsAutorisees() {
        return true
    },
    // @ts-expect-error typage afterCreate cf: https://github.com/miragejs/miragejs/pull/1019
    afterCreate,
})
