import { InterventionServiceOfferDto } from '@/interventionServiceOffer/entities/interventionServiceOffer/InterventionServiceOfferDto'
import { InterventionServiceOfferForeignKeys } from '@/mock-api/models'
import { AfterCreate } from '@/mock-api/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'
import { FlattenFactoryMethods } from 'miragejs/-types'
import { compteurFactory } from './compteur'
import { demandeInterventionFactory } from './demandeIntervention'
import { equipementFactory } from './equipement'
import { interventionFactory } from './intervention'
import { activiteDiagnostiqueFactory } from './activiteDiagnostique'
import { activiteActionFactory } from './activiteAction'
import { ServiceOfferDto } from '@/serviceOffer/entities/serviceOffer/ServiceOfferDto'
import { carlConfigurationInterventionFactory } from './carlConfiguration'
import { operationFactory } from './operation'

export type InterventionServiceOfferFactoryDto = Omit<
    InterventionServiceOfferDto,
    keyof InterventionServiceOfferForeignKeys
>

export const INTERVENTION_SERVICE_OFFER_AVAILABLE_ROLES: InterventionServiceOfferFactoryDto['availableRoles'] =
    [
        {
            value: 'ROLE_INTERVENTION_SUPERVISEUR',
            libelle: 'Superviseur',
            description:
                "Peut réaliser l'ensemble des tâches des déclarants, intervenants et demandeurs.",
        },
        {
            value: 'ROLE_INTERVENTION_DECLARANT',
            libelle: 'Déclarant',
            description:
                'Peut consulter, et créer directement des interventions.',
        },
        {
            value: 'ROLE_INTERVENTION_INTERVENANT',
            libelle: 'Intervenant',
            description: 'Peut consulter et solder les interventions.',
        },
        {
            value: 'ROLE_INTERVENTION_DEMANDEUR',
            libelle: 'Demandeur',
            description: "Peut faire des demandes d'intervention.",
        },
        {
            value: 'ROLE_INTERVENTION_OBSERVATEUR',
            libelle: 'Observateur',
            description: 'Peut consulter les interventions et les demandes.',
        },
        {
            value: 'ROLE_SERVICEOFFER_ADMIN',
            libelle: 'Administrateur',
            description:
                'Peut administrer les agents ayant accès à cette offre de service ainsi que leurs rôles associés.',
        },
    ]

export function scopeRoleFromId(serviceOfferId: ServiceOfferDto['id']) {
    return (
        unscopedRole: InterventionServiceOfferFactoryDto['availableRoles']['0']
    ) => {
        return {
            ...unscopedRole,
            value: `SERVICEOFFER_${serviceOfferId}_${unscopedRole.value}`,
        }
    }
}

const afterCreate: AfterCreate<'interventionServiceOffer'> = (
    interventionServiceOffer,
    server
) => {
    interventionServiceOffer.update({
        gmaoConfiguration:
            // unlike HasMany, BelongsTo relationships are initialised to null
            // so we can do this
            interventionServiceOffer.gmaoConfiguration ??
            server.create('carlConfigurationIntervention'),
    })
}

export const interventionServiceOfferFactory =
    Factory.extend<InterventionServiceOfferFactoryDto>({
        id() {
            return faker.string.uuid()
        },
        title() {
            return faker.word.noun() + ' ' + faker.word.adjective()
        },
        description() {
            if (faker.number.float() < 0.4) {
                return faker.lorem.paragraph()
            }
            return undefined
        },
        image() {
            return faker.image.url()
        },
        link() {
            return 'intervention-service-offer/index'
        },
        template() {
            return 'InterventionServiceOffer'
        },
        availableEquipementsIds() {
            return []
        },
        availableRoles(
            this: FlattenFactoryMethods<InterventionServiceOfferFactoryDto>
        ) {
            return INTERVENTION_SERVICE_OFFER_AVAILABLE_ROLES.map(
                scopeRoleFromId(this.id)
            )
        },
        environnement() {
            return faker.helpers.arrayElement(['INTEG', 'TEST', 'PROD'])
        },
        gestionRecompletementActif() {
            return false
        },
        configurationFormulaireActivite() {
            return {
                dateDeDebut: faker.helpers.arrayElement([
                    'jour',
                    'heure',
                    'automatique',
                ]),
                heureParDefautEnMinute: 8,
            }
        },
        modeleDeDescriptionPourDemandeIntervention() {
            return faker.lorem.paragraph()
        },
        gestionDatesSouhaiteesPourDemandeInterventionActif() {
            return false
        },
        gestionDatesPourInterventionActif() {
            return false
        },
        // @ts-expect-error typage afterCreate cf: https://github.com/miragejs/miragejs/pull/1019
        afterCreate,
    })

export const interventionServiceOfferFactories = {
    compteur: compteurFactory,
    intervention: interventionFactory,
    carlConfigurationIntervention: carlConfigurationInterventionFactory,
    demandeIntervention: demandeInterventionFactory,
    equipement: equipementFactory,
    interventionServiceOffer: interventionServiceOfferFactory,
    activiteAction: activiteActionFactory,
    activiteDiagnostique: activiteDiagnostiqueFactory,
    operation: operationFactory,
}
