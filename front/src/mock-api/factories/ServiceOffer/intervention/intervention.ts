import {
    InterventionDto,
    InterventionPriority,
} from '@/interventionServiceOffer/entities/intervention/InterventionDto'
import { InterventionHistorisedStatusDto } from '@/interventionServiceOffer/entities/intervention/status/InterventionHistorisedStatusDto'
import { InterventionForeignKeys } from '@/mock-api/models'
import { AfterCreate } from '@/mock-api/types'
import { DocumentDto } from '@/shared/document/entities/DocumentDto'
import { isNullOrUndefined, isUndefined } from '@/shared/helpers/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'
import { Assign, FlattenFactoryMethods } from 'miragejs/-types'

type InterventionFactoryDto = Assign<
    Omit<InterventionDto, keyof InterventionForeignKeys>,
    {
        statusHistory: InterventionDto['currentStatus'][]
    }
>

const afterCreate: AfterCreate<'intervention'> = (intervention, server) => {
    const générerDesOpérations = faker.number.float() > 0.7

    if (
        // HasMany relationships are always initialised as empty collection
        // so test if it is empty before generating interventions
        intervention.activitesAction.length === 0 &&
        (générerDesOpérations || intervention.relatedEquipement !== undefined)
    ) {
        intervention.update({
            activitesAction: server.createList(
                'activiteAction',
                faker.number.int({ min: 1, max: 3 }),
                {
                    intervention: intervention,
                    equipementCible:
                        intervention.relatedEquipement ??
                        server.create('equipement'),
                }
            ),
        })
    }
    if (
        // HasMany relationships are always initialised as empty collection
        // so test if it is empty before generating interventions
        intervention.activitesDiagnostique.length === 0 &&
        (générerDesOpérations || intervention.relatedEquipement !== undefined)
    ) {
        intervention.update({
            activitesDiagnostique: server.createList(
                'activiteDiagnostique',
                faker.number.int({ min: 1, max: 3 }),
                {
                    intervention: intervention,
                    equipementCible:
                        intervention.relatedEquipement ??
                        server.create('equipement'),
                }
            ),
        })
    }

    if (isNullOrUndefined(intervention.createdBy)) {
        intervention.update({
            createdBy: server.create('gmaoActeur'),
        })
    }

    if (intervention.intervenantsAffectes.length === 0) {
        intervention.update({
            intervenantsAffectes: server.createList(
                'gmaoActeur',
                faker.number.int({ min: 0, max: 3 })
            ),
        })
    }

    if (isUndefined(intervention.demandeInterventionIds)) {
        intervention.update({
            demandeInterventionIds: server
                .createList(
                    'demandeIntervention',
                    faker.number.int({ min: 0, max: 2 })
                )
                .map((demandeIntervention) => demandeIntervention.id),
        })
    }
}

export const interventionFactory = Factory.extend<InterventionFactoryDto>({
    id() {
        return faker.string.uuid()
    },
    code() {
        return (
            'OT-' +
            faker.string.numeric({
                length: 6,
                allowLeadingZeros: true,
            })
        )
    },
    title() {
        return faker.word.noun() + ' ' + faker.word.adjective()
    },
    priority() {
        return faker.helpers.enumValue(InterventionPriority)
    },
    description() {
        return faker.lorem.paragraph()
    },
    imagesAvant() {
        const nbImages = faker.number.int(2)
        const images = []
        for (let imageIndex = 0; imageIndex < nbImages; imageIndex++) {
            images.push({
                id: faker.string.uuid(),
                url: faker.image.url(),
            })
        }
        return images
    },
    imagesApres() {
        const nbImages = faker.number.int(2)
        const images = []
        for (let imageIndex = 0; imageIndex < nbImages; imageIndex++) {
            images.push({
                id: faker.string.uuid(),
                url: faker.image.url(),
            })
        }
        return images
    },
    actionType() {
        return undefined
    },
    latitude() {
        return undefined
    },
    longitude() {
        return undefined
    },
    address() {
        const générerUneAddress = faker.number.float() < 0.7
        if (générerUneAddress) {
            return `${faker.location.streetAddress()} ${faker.location.zipCode()} ${faker.location.city()}`
        }
        return undefined
    },
    createdAt() {
        return faker.date.past().toISOString()
    },
    dateDeDebut() {
        return faker.date.recent().toISOString()
    },
    dateDeFin() {
        return faker.date.future().toISOString()
    },
    gmaoObjectViewUrl() {
        return faker.internet.url()
    },
    statusHistory(this: FlattenFactoryMethods<InterventionFactoryDto>, i) {
        const creationDate = new Date(this.createdAt as string)
        const awaitingInformationDate = faker.date.between({
            from: creationDate,
            to: Date.now(),
        })
        const validationDate = faker.date.between({
            from: awaitingInformationDate,
            to: Date.now(),
        })
        const closedDate = faker.date.between({
            from: validationDate,
            to: Date.now(),
        })
        const statusHistory: InterventionHistorisedStatusDto[] = [
            {
                code: 'awaiting_validation',
                label: 'A faire',
                createdAt: creationDate.toISOString(),
                createdBy: faker.internet.email(),
            },
            {
                code: 'awaiting_validation',
                label: 'A faire',
                createdAt: awaitingInformationDate.toISOString(),
                createdBy: faker.internet.email(),
            },
            {
                code: 'awaiting_realisation',
                label: 'A faire',
                createdAt: validationDate.toISOString(),
                createdBy: faker.internet.email(),
            },
            {
                code: 'closed',
                label: 'Réalisé',
                createdAt: closedDate.toISOString(),
                createdBy: faker.internet.email(),
            },
        ]

        const numberOfStatuses = 1 + (i % statusHistory.length) // on s'assure d'alterner tous les types de status
        return statusHistory.slice(0, numberOfStatuses).reverse() //pour présenter en ordre décroissant
    },
    currentStatus(this: FlattenFactoryMethods<InterventionFactoryDto>) {
        return this.statusHistory[0]
    },
    documentsJoints() {
        const nbDocuments = faker.number.int({ min: 0, max: 2 })
        const documents: DocumentDto[] = []
        for (
            let documentIndex = 0;
            documentIndex < nbDocuments;
            documentIndex++
        ) {
            documents.push({
                id: faker.string.uuid(),
                name: faker.word.words(),
                url: faker.internet.url(),
            })
        }
        return documents
    },
    materielConsomme() {
        return undefined
    },

    // @ts-expect-error typage afterCreate cf: https://github.com/miragejs/miragejs/pull/1019/files
    afterCreate,
})
