import { OperationDto } from '@/interventionServiceOffer/entities/activite/OperationDto'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'

export const operationFactory = Factory.extend<OperationDto>({
    id() {
        return faker.string.uuid()
    },
    label() {
        return faker.word.verb()
    },
})
