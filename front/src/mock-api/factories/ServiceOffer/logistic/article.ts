import { ArticleDto } from '@/logisticServiceOffer/entities/article/ArticleDto'
import { AfterCreate } from '@/mock-api/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'

const afterCreate: AfterCreate<'article'> = (article, server) => {
    server.createList('mouvement', faker.number.int({ min: 1, max: 5 }), {
        article: { id: article.id },
        emplacement: faker.helpers.arrayElement(article.lots).emplacement,
    })
}

export const articleFactory = Factory.extend<ArticleDto>({
    id() {
        return faker.string.uuid()
    },
    code() {
        return (
            'ART-' +
            faker.string.numeric({ length: 7, allowLeadingZeros: true })
        )
    },
    titre() {
        return faker.commerce.productName().split(' ').reverse().join(' ')
    },
    lots() {
        return [
            {
                magasinId: 'warehouse-id-magasin-general',
                emplacement: {
                    id: faker.string.uuid(),
                    code: `${faker.string
                        .alpha(1)
                        .toUpperCase()}-${faker.string.numeric(
                        1
                    )}-${faker.string.numeric(3)}`,
                },
                quantité: faker.number.int(200),
            },
        ]
    },
    unité() {
        return faker.helpers.arrayElement(articleUnits)
    },
    // @ts-expect-error typage afterCreate cf: https://github.com/miragejs/miragejs/pull/1019
    afterCreate,
})

export const articleUnits = [
    'unité',
    'paire',
    'litre',
    'mètre',
    'kilo',
    'kilomètre',
]
