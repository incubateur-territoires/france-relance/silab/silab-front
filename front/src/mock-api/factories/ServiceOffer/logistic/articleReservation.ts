import { ArticleReservationDto } from '@/logisticServiceOffer/entities/articleReservation/ArticleReservationDto'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'
import { articleUnits } from './article'

export const articleReservationFactory = Factory.extend<ArticleReservationDto>({
    id() {
        return faker.string.uuid()
    },
    articleId() {
        return faker.string.uuid()
    },
    articleCode() {
        return 'ART-' + faker.string.numeric(7) // Amélioration : mettre une relation
    },
    articleDescription() {
        return faker.commerce.productName().split(' ').reverse().join(' ')
    },
    quantity() {
        return faker.number.int(100)
    },
    availableQuantity() {
        return faker.number.int(1000)
    },
    unit() {
        return faker.helpers.arrayElement(articleUnits)
    },
    warehouseId() {
        return faker.string.uuid()
    },
    storageLocationCode() {
        return `${faker.string.alpha(1).toUpperCase()}-${faker.string.numeric(
            1
        )}-${faker.string.numeric(3)}`
    },
    creatorName() {
        return faker.person.fullName()
    },
    creatorDirectionName() {
        return faker.word.noun() + ' ' + faker.word.noun()
    },
    dateAttendueDeReception() {
        return faker.date.soon().toISOString()
    },
    interventionId() {
        return faker.string.uuid() // Doit être précisé à la création du reservation
    },
    interventionCode() {
        return 'OT-' + faker.string.numeric(5) // Doit être précisé à la création du reservation
    },
    interventionTitle() {
        return faker.commerce.productName().split(' ').reverse().join(' ') // Doit être précisé à la création du reservation
    },
})
