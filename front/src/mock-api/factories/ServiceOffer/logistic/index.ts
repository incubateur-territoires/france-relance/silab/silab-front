import { articleFactory } from './article'
import { articleReservationFactory } from './articleReservation'
import { interventionFactory } from './intervention'
import { mouvementFactory } from './mouvement'
import { mouvementDeSortieFactory } from './mouvementDeSortie'

export const logisticServiceOfferFactories = {
    article: articleFactory,
    articleReservation: articleReservationFactory,
    logisticIntervention: interventionFactory,
    mouvement: mouvementFactory,
    mouvementDeSortie: mouvementDeSortieFactory,
}
