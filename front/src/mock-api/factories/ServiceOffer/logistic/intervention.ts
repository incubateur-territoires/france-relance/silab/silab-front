import { InterventionDto } from '@/logisticServiceOffer/entities/intervention/InterventionDto'
import { AfterCreate } from '@/mock-api/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'

const afterCreate: AfterCreate<'logisticIntervention'> = (
    intervention,
    server
) => {
    server.createList(
        'mouvementDeSortie',
        faker.number.int({ min: 1, max: 5 }),
        {
            interventionId: intervention.id,
        }
    )

    intervention.reservations.forEach((reservation) => {
        server.create('articleReservation', {
            id: reservation.id,
            expectedReceiptDate: reservation.dateAttendueDeReception,
            interventionId: intervention.id,
            interventionCode: intervention.code,
            interventionTitle: intervention.titre,
        })
    })
}

export const interventionFactory = Factory.extend<InterventionDto>({
    id() {
        return faker.string.uuid()
    },
    code() {
        return 'OT-' + faker.string.numeric(5)
    },
    titre() {
        return faker.word.noun() + ' ' + faker.word.adjective()
    },
    centreDeCouts() {
        return faker.word.noun()
    },
    reservations() {
        const reservations = []
        for (
            let index = 0;
            index < faker.number.int({ min: 1, max: 5 });
            index++
        ) {
            reservations.push({
                id: faker.string.uuid(),
                dateAttendueDeReception: faker.date.soon().toISOString(),
                articleId: this.id as string,
                articleCode: this.code as string,
                articleDescription: faker.lorem.paragraph(),
                quantity: faker.number.int(),
                availableQuantity: faker.number.int(),
                unit: faker.science.unit().name,
            })
        }
        return reservations
    },
    createdBy() {
        return faker.person.fullName()
    },
    historiqueDesEtats(i) {
        const creationDate = faker.date.past()
        const awaitingInformationDate = faker.date.between({
            from: creationDate,
            to: Date.now(),
        })
        const informationSuppliedDate = faker.date.between({
            from: awaitingInformationDate,
            to: Date.now(),
        })
        const validationDate = faker.date.between({
            from: informationSuppliedDate,
            to: Date.now(),
        })
        const closedDate = faker.date.between({
            from: validationDate,
            to: Date.now(),
        })
        const statusHistory: InterventionDto['historiqueDesEtats'] = [
            {
                code: 'awaiting_validation',
                label: 'En attente de validation',
                createdAt: creationDate.toISOString(),
                createdBy: this.createdBy as string,
            },
            {
                code: 'awaiting_realisation',
                label: 'A faire',
                createdAt: creationDate.toISOString(),
                createdBy: this.createdBy as string,
            },
            {
                code: 'awaiting_realisation',
                label: 'En cours de réalisation',
                createdAt: validationDate.toISOString(),
                createdBy: faker.internet.email(),
            },
            {
                code: 'awaiting_realisation',
                label: 'En attente de fournitures',
                createdAt: validationDate.toISOString(),
                createdBy: faker.internet.email(),
            },
            {
                code: 'closed',
                label: 'Soldée',
                createdAt: closedDate.toISOString(),
                createdBy: faker.internet.email(),
            },
        ]

        const numberOfStatuses = 1 + (i % statusHistory.length) // on s'assure d'alterner tous les types de status
        return statusHistory.slice(0, numberOfStatuses).reverse() //pour présenter en ordre décroissant
    },
    // @ts-expect-error typage afterCreate cf: https://github.com/miragejs/miragejs/pull/1019
    afterCreate,
})
