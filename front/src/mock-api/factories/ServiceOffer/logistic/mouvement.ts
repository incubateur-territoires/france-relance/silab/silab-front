import { MouvementDto } from '@/logisticServiceOffer/entities/mouvement/MouvementDto'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'

export const movementTypes: MouvementDto['typeDeMouvement'][] = [
    {
        id: 'idannnuel',
        libele: 'Inventaire annuel',
    },
    {
        id: 'idponctuel',
        libele: 'Inventaire ponctuel',
    },
]

export const mouvementFactory = Factory.extend<MouvementDto>({
    id() {
        return faker.string.uuid()
    },
    article() {
        return {
            id: faker.string.uuid(), // Doit être précisé à la création du mouvement
        }
    },
    createdAt() {
        return faker.date.past({ years: 2 }).toISOString()
    },
    createdBy() {
        return faker.internet.email()
    },
    quantité() {
        return faker.number.int(100)
    },
    typeDeMouvement() {
        return faker.helpers.arrayElement(movementTypes)
    },
    emplacement() {
        return undefined // Doit être précisé à la création du mouvement
    },
})
