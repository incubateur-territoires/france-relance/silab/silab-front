import { MouvementDeSortieDto } from '@/logisticServiceOffer/entities/mouvementDeSortie/MouvementDeSortieDto'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'
import { articleUnits } from './article'

export const mouvementDeSortieFactory = Factory.extend<MouvementDeSortieDto>({
    id() {
        return faker.string.uuid()
    },
    article() {
        return {
            id: faker.string.uuid(),
            code: 'ART-' + faker.string.numeric(5),
            titre: faker.commerce.productName().split(' ').reverse().join(' '),
            unité: faker.helpers.arrayElement(articleUnits),
        }
    },
    createdAt() {
        return faker.date.recent().toISOString()
    },
    createdBy() {
        return faker.internet.email()
    },
    quantite() {
        return faker.number.int(100)
    },
    emplacement() {
        return {
            id: faker.string.uuid(),
            code: `${faker.string
                .alpha(1)
                .toUpperCase()}-${faker.string.numeric(
                1
            )}-${faker.string.numeric(3)}`,
        }
    },
})
