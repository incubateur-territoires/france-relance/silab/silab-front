import { CarlClientDto } from '@/shared/carl/entitites/carlClient/CarlClientDto'
import { CostCenterDto } from '@/shared/carl/entitites/configurationCarlObject/CostCenterDto'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'
import { Assign } from 'miragejs/-types'

export type CarlClientFactoryDto = Assign<
    CarlClientDto,
    { id: string; costCenters: Array<CostCenterDto> }
>

export const carlClientFactory = Factory.extend<CarlClientFactoryDto>({
    id() {
        return faker.string.uuid()
    },
    title() {
        return faker.lorem.sentence()
    },
    availableActionTypes() {
        return {
            '1843fd73855-5ff': 'Balayeuse',
            '1843fd73855-5fe': 'Balayage manuel',
            '1843fd73855-5a6': 'Corbeille',
            '1843fd73855-682': 'Encombrant',
            '1843fd73855-656': 'Laveuse',
            '843fd73855-5d2': 'Ramassage herbe',
            '1843fd73855-6ae': 'Soufflage feuille',
            '18892f54ae2-41c': 'Conteneur brulé',
        }
    },
    costCenters() {
        return [
            {
                id: '14a0ebc0e45-19d12',
                label: 'Achats - Moyens Généraux - EPI',
                type: 'costcenter',
                code: 'AMG_EPI',
            },
            {
                id: '14a0ebc0e45-19d14',
                label: "Achats - Moyens Généraux - Produits d'Entretien",
                type: 'costcenter',
                code: 'AMG_ENT',
            },
            {
                id: '14a0ebc0e45-19bcc',
                label: "Construction - Produits d'Entretien",
                type: 'costcenter',
                code: 'CONST_ENT',
            },
            {
                id: '14a531914f0-6ceb',
                label: "Réglementation Gestion l'Espace Public - Fournitures Diverse",
                type: 'costcenter',
                code: 'RGEP_DIVERS',
            },
            {
                id: '14a4e1cf52c-1231',
                label: 'Construction - Fournitures Diverses',
                type: 'costcenter',
                code: 'CONST_DIVERS_GP',
            },
            {
                id: '158f8296f7d-67d9',
                label: 'Politique Sportive Pour EEC',
                type: 'costcenter',
                code: 'SPORTS_EEC',
            },

            {
                id: '1590068d5db-c55db',
                label: 'Energie Climat - Investissement',
                type: 'costcenter',
                code: 'ENERCLIM_INVEST_GP',
            },
            {
                id: '15a7902d01d-296a11',
                label: 'Secteur Externe UPEP',
                type: 'costcenter',
                code: 'UPEP_EXT',
            },
            {
                id: '15aa37ad708-8a505',
                label: 'Petite enfance-EPI',
                type: 'costcenter',
                code: 'ENFANCE_EPI',
            },
        ]
    },
    woViewUrlSubPath: faker.internet.url() + '?woId=',
    mrViewUrlSubPath: faker.internet.url() + '?mrId=',
    boxViewUrlSubPath: faker.internet.url() + '?boxId=',
    materialViewUrlSubPath: faker.internet.url() + '?materialId=',
})
