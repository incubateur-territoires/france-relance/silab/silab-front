import { DocumentDto } from '@/shared/document/entities/DocumentDto'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'

export const documentsJointsFactory = Factory.extend<DocumentDto>({
    id() {
        return faker.string.uuid()
    },
    url() {
        return faker.internet.url()
    },
    name() {
        return faker.word.words()
    },
})
