import { GedClientDto } from '@/shared/entities/gedClient/GedClientDto'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'
import { Assign } from 'miragejs/-types'

export type GedClientFactoryDto = Assign<GedClientDto, { id: string }>

export const gedClientFactory = Factory.extend<GedClientFactoryDto>({
    id() {
        return faker.string.uuid()
    },
    title() {
        return faker.lorem.sentence()
    },
    configurationDocumentation() {
        return `{
                "folderPath": "root/sites/NOM_DU_SITE/documentLibrary/DOSSIER_1/DOSSIER_N",// Attention, ne pas mettre de "/" au début de l'url sauf en connaissance de cause, voir : https://symfony.com/doc/current/reference/configuration/framework.html#base-uri
                "objectTypeProperties": {// cmis:objectTypeId et cmis:name n'ont pas besoin d'êtres surchargés ici, ils seront définis par l'implémentation de base
                    "cmis:description": "{{Object.decription}}"
                },
                "secondaryObjectTypes" : {// Assi appelés "aspects" sur Alfresco
                    "P:cm:titled": {// aspect natif alfresco
                        "cm:title":"{{Object.title}}"
                    },
                    "P:carl:WO": {// aspect personalisé
                        "carl:woCode": "{{Object.code}}"
                    },
                    "P:silab2:main": {// aspect personalisé
                        "silab2:mainApp": "{{ServiceOffer.title}}",
                        "silab2:example1": "valeur example1",
                        "silab2:example2": "valeur example2 avec valeur dynamique {{Class.property}}"
                    }
                }
            }`
    },
})
