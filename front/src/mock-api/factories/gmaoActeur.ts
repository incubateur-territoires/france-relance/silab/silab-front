import { GmaoActeurDto } from '@/shared/gmaoActeur/GmaoActeurDto'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'
import { AfterCreate } from '../types'

const afterCreate: AfterCreate<'gmaoActeur'> = (gmaoActeur, server) => {
    const nombreDOperationsExistantesMinimum = 4
    const opérationsExistantes = server.schema.all('operation').length

    const nombreDOpérationsAAjouter = Math.max(
        0,
        nombreDOperationsExistantesMinimum - opérationsExistantes
    )

    server.createList('operation', nombreDOpérationsAAjouter)
    if (gmaoActeur.operationsHabilitees.length === 0) {
        gmaoActeur.update({
            operationsHabilitees: server.schema.all('operation').slice(
                0,
                faker.number.int({
                    min: 2,
                    max: nombreDOperationsExistantesMinimum,
                })
            ),
        })
    }
}

export const gmaoActeurFactory = Factory.extend<GmaoActeurDto>({
    nomComplet() {
        return faker.person.fullName()
    },
    id() {
        return faker.string.uuid()
    },
    telephones() {
        const telephones = []
        for (let i = 0; i < faker.number.int({ min: 1, max: 3 }); i++) {
            telephones.push(faker.phone.number())
        }
        return telephones
    },
    emails() {
        const emails = []
        for (let i = 0; i < faker.number.int({ min: 0, max: 3 }); i++) {
            emails.push(faker.internet.email())
        }
        return emails
    },
    currentStatus() {
        return {
            code: 'active',
            label: 'Actif',
            createdAt: faker.date.past().toISOString(),
            createdBy: faker.person.fullName(),
        }
    },
    // @ts-expect-error voir: https://github.com/miragejs/miragejs/pull/1019
    afterCreate,
})
