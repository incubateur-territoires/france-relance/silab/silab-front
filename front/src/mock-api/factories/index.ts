import { serviceOfferFactories } from './ServiceOffer'
import { carlClientFactory } from './carlClient'
import { documentsJointsFactory } from './documentsJoints'
import { gedClientFactory } from './gedClient'
import { gmaoActeurFactory } from './gmaoActeur'
import { siilabConfigurationFactory } from './siilabConfiguration'

export const factories = {
    ...serviceOfferFactories,
    carlClient: carlClientFactory,
    gedClient: gedClientFactory,
    gmaoActeur: gmaoActeurFactory,
    documentJoint: documentsJointsFactory,
    siilabConfiguration: siilabConfigurationFactory,
}
