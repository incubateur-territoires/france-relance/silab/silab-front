import { SiilabConfigurationDto } from '@/shared/administration/entities/siilabConfiguration/SiilabConfigurationDto'
import { faker } from '@faker-js/faker/locale/fr'
import { Factory } from 'miragejs'

export const siilabConfigurationFactory =
    Factory.extend<SiilabConfigurationDto>({
        mapCenterLatitude() {
            return faker.location.latitude()
        },
        mapCenterLongitude() {
            return faker.location.longitude()
        },
    })
