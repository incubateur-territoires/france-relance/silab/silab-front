/* eslint-disable no-console */
import express from 'express'
const app = express()
import dotenv from 'dotenv'
import { randomUUID } from 'crypto'
import cors from 'cors'
import fs from 'fs'
import https from 'https'

dotenv.config()
dotenv.config({ path: `.env.local`, override: true })

const port =
    process.env.VITE_MOCK_FILES_SERVER_PORT !== undefined
        ? Number(process.env.VITE_MOCK_FILES_SERVER_PORT)
        : 8445

const files = new Map()
app.use(cors())
app.use(
    express.json({
        limit: '10mb',
    })
)
app.get('/files/:id', (req, res) => {
    // inspiré de https://stackoverflow.com/a/28440633/2474460
    const fileBase64 = files.get(req.params.id)

    if (fileBase64 === undefined) {
        res.status(404)
        res.send('Fichier introuvable')
    } else {
        const fileBuffer = Buffer.from(fileBase64, 'base64')

        res.writeHead(200, {
            'Content-Type': extractMimeTypeFromBase64(fileBase64),
            'Content-Length': fileBuffer.length,
        })
        res.end(fileBuffer)
    }
})

app.post('/files', (req, res) => {
    const base64 = req.body.base64

    const fileId = randomUUID()

    files.set(fileId, base64)

    res.send({
        id: fileId,
        url: `https://localhost:${process.env.VITE_MOCK_FILES_SERVER_PORT}/files/${fileId}`,
    })
})

const server = https.createServer(
    {
        key: fs.readFileSync('cert.key'),
        cert: fs.readFileSync('cert.crt'),
    },
    app
)
server.listen(port, () => {
    console.log(`Serveur de fichiers pour mock-api lancé sur le port ${port}`)
})

process.on('SIGTERM', () => {
    console.info('Fermeture du serveur de fichier pour mock-api')
    server.close(() => {
        process.exit()
    })
})
function extractMimeTypeFromBase64(base64) {
    const signatures = {
        R0lGODdh: 'image/gif',
        R0lGODlh: 'image/gif',
        iVBORw0KGgo: 'image/png',
        '/9j/': 'image/jpg',
    }
    for (const signature in signatures) {
        if (base64.startsWith(signature)) {
            return signatures[signature]
        }
    }
    throw new Error('type mime inconnu')
}
