import { logisticServiceOfferFixtures } from './logisticServiceOffer'
import { userFixtures } from './users'

export const fixtures = {
    logisticServiceOffers: logisticServiceOfferFixtures,
    users: userFixtures,
}
