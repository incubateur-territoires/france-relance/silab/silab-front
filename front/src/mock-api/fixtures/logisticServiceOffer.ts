import { LogisticServiceOfferDto } from '@/logisticServiceOffer/entities/logisticServiceOffer/LogisticServiceOfferDto'
import { movementTypes } from '@/mock-api/factories/ServiceOffer/logistic/mouvement'

export const logisticServiceOfferFixtures: LogisticServiceOfferDto[] = [
    {
        id: 'stock-et-magasin-general',
        title: 'Magasin Général',
        image: 'https://media.istockphoto.com/id/1208067405/fr/photo/b%C3%A2timent-dentrep%C3%B4t-3d.jpg?s=612x612&w=0&k=20&c=M548T9SwBh4w79NQCnfaA3ZppNi0PxlrI_UHXLGhTjI=',
        link: 'logistic-service-offer/index',
        template: 'LogisticServiceOffer',
        warehouseId: 'warehouse-id-magasin-general',
        typeDInventaireId: 'idponctuel',
        gmaoConfiguration: {
            typeDInventaires: movementTypes,
        },
        availableRoles: [
            {
                value: 'SERVICEOFFER_stock-et-magasin-general_ROLE_LOGISTIQUE_SUPERVISEUR',
                libelle: 'Superviseur',
                description:
                    "Peut réaliser l'ensemble des tâches des magasiniers et inventoristes",
            },
            {
                value: 'SERVICEOFFER_stock-et-magasin-general_ROLE_LOGISTIQUE_MAGASINIER',
                libelle: 'Magasinier',
                description: 'Peut réaliser des sorties de stock',
            },
            {
                value: 'SERVICEOFFER_stock-et-magasin-general_ROLE_LOGISTIQUE_MAGASINIER',
                libelle: 'Inventoriste',
                description: 'Peut réaliser des inventaires',
            },
            {
                value: 'SERVICEOFFER_stock-et-magasin-general_ROLE_SERVICEOFFER_ADMIN',
                libelle: 'Administrateur',
                description:
                    'Peut administrer les agents ayant accès à cette offre de service ainsi que leurs rôles associés',
            },
        ],
        environnement: 'INTEG',
    },
]
