import { isNull } from '@/shared/helpers/types'
import { AppRegistry, AppSchema, AppServer } from './types'
import { jwtDecode, JwtPayload } from 'jwt-decode'
import { Instantiate, Request, Response } from 'miragejs'
import { RouteHandler } from 'miragejs/server'
import { userFixtures } from './fixtures/users'
import { UserDto } from '@/api/user/UserDto'
import { faker } from '@faker-js/faker/locale/fr'

export function getUserFromRequest(request: Request): UserDto | null {
    const email = extractLoggedEmail(request)
    if (isNull(email)) {
        return null
    }
    return userFixtures.find((user) => user.email === email) || null
}

export function extractLoggedEmail(request: Request): string {
    if (import.meta.env.VITE_DISABLE_MOCK_AUTHENTICATION === 'true') {
        return import.meta.env.VITE_AZURE_TEST_USERNAME
    }
    const idToken =
        request.requestHeaders.authorization.match(/^Bearer (.+)$/)?.[1]
    if (idToken === undefined) {
        throw new Error('Aucun utilisateur connecté')
    }
    return jwtDecode<JwtPayload & { upn: string }>(idToken).upn
}

export function extractLoggedGmaoActeurDto(
    request: Request,
    schema: AppSchema
): Instantiate<AppRegistry, 'gmaoActeur'> {
    const email = extractLoggedEmail(request)

    let loggedGmaoActor = schema.find('gmaoActeur', email)

    if (loggedGmaoActor) return loggedGmaoActor

    loggedGmaoActor = schema.create('gmaoActeur', {
        id: email,
        nomComplet: (email.match(/^.*?(?=@)/gm) as Array<string>)[0]
            .split('.')
            .join(' '),
        telephones: [],
        emails: [email],
        currentStatus: {
            code: 'active',
            label: 'Actif',
            createdAt: faker.date.past().toISOString(),
            createdBy: faker.person.fullName(),
        },
    })

    const jeanDoux = schema.findBy('gmaoActeur', {
        id: 'jean-doux-uniq666',
    }) as Instantiate<AppRegistry, 'gmaoActeur'>
    const johnDoe = schema.findBy('gmaoActeur', {
        id: 'john-doe-uniq666',
    }) as Instantiate<AppRegistry, 'gmaoActeur'>

    loggedGmaoActor.update(
        'team',
        loggedGmaoActor.team.add(jeanDoux).add(johnDoe)
    )
    return loggedGmaoActor
}

export function extractMimeTypeFromBase64(base64: string) {
    const signatures: { [key: string]: string } = {
        R0lGODdh: 'image/gif',
        R0lGODlh: 'image/gif',
        iVBORw0KGgo: 'image/png',
        '/9j/': 'image/jpg',
    }
    for (const signature in signatures) {
        if (base64.startsWith(signature)) {
            return signatures[signature]
        }
    }
    throw new Error('type mime inconnu')
}

export function base64ToDataUrl(base64: string) {
    return `data:${extractMimeTypeFromBase64(base64)};base64,${base64}`
}

export async function uploadFileToLocalFileServer(base64: string): Promise<{
    id: string
    url: string
}> {
    return await fetch(
        `https://localhost:${
            import.meta.env.VITE_MOCK_FILES_SERVER_PORT
        }/files`,
        {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                base64,
            }),
        }
    ).then((response) => response.json())
}

export const authenticationProtectedResponse = (
    schema: AppSchema,
    request: Request,
    handler: RouteHandler<AppRegistry>
) => {
    if (import.meta.env.VITE_DISABLE_MOCK_AUTHENTICATION === 'true') {
        return handler(schema, request)
    }

    const noIdTokenInHeaders = !('authorization' in request.requestHeaders)
    if (noIdTokenInHeaders) {
        return new Response(
            401,
            {},
            {
                errors: [
                    'Vous devez être authentifié pour accéder à cette ressource',
                ],
            }
        )
    }

    const authorizationHeader = request.requestHeaders.authorization
    const idTokenRegex = /^Bearer (.+)$/
    const idToken = authorizationHeader.match(idTokenRegex)?.[1]
    const idTokenIsNotRFC6750Compliant = idToken === undefined
    if (idTokenIsNotRFC6750Compliant) {
        return new Response(401, {}, { errors: ["Token d'accès non trouvé"] })
    }

    const tokenExpirationDate =
        idToken === undefined ? undefined : jwtDecode<JwtPayload>(idToken)?.exp
    const tokenExpirationDateNotFound = tokenExpirationDate === undefined
    if (tokenExpirationDateNotFound) {
        return new Response(
            401,
            {},
            { errors: ["Date d'expiration non trouvée dans le token"] }
        )
    }

    const tokenIsExpired = tokenExpirationDate * 1000 < Date.now() //Note: exp is in seconds https://www.rfc-editor.org/rfc/rfc7519#section-4.1.4
    if (tokenIsExpired) {
        const tokenExpirationDateString = new Date(
            tokenExpirationDate * 1000
        ).toLocaleString()
        return new Response(
            401,
            {},
            {
                errors: [`Token d'accès expiré : ${tokenExpirationDateString}`],
            }
        )
    }

    // on créé l'utilisateur s'il pas dans les fixtures
    const loggedInUserEmail = extractLoggedEmail(request)
    if (isNull(schema.findBy('user', { email: loggedInUserEmail }))) {
        schema.create('user', {
            email: loggedInUserEmail,
            roles: [],
            reachableRoles: [],
        })
    }

    return handler(schema, request)
}

/**
 * workaround pour les images de faker
 * @see {@link https://github.com/miragejs/ember-cli-mirage/issues/1915#issuecomment-613335639 the issue comment on the subject}
 */
export function mirageFakerImagesWorkaround(this: AppServer) {
    Object.defineProperty(window.WebAssembly, 'instantiateStreaming', {
        value: null,
    })
    const oldPassthroughRequests = this.pretender.passthroughRequest.bind(
        this.pretender
    )
    this.pretender.passthroughRequest = (
        verb: Parameters<typeof this.pretender.passthroughRequest>['0'],
        path: Parameters<typeof this.pretender.passthroughRequest>['1'],
        request: Request &
            Parameters<typeof this.pretender.passthroughRequest>['2']
    ) => {
        // Needed because responseType is not set correctly in Mirages passthrough
        // for more details see: https://github.com/miragejs/ember-cli-mirage/issues/1915
        if (
            verb === 'GET' &&
            (request.url.match(/https:\/\/picsum.photos\/.*$/) ||
                request.url.match(/https:\/\/loremflickr.com\/.*$/) ||
                request.url.match(
                    `https://localhost:${
                        import.meta.env.VITE_MOCK_FILES_SERVER_PORT
                    }/.*$`
                ))
        ) {
            request.responseType = 'arraybuffer'
        }
        return oldPassthroughRequests(verb, path, request)
    }
}
