// DTOs
import type { UserDto } from '@/api/user/UserDto'
import { LogisticServiceOfferDto } from '@/logisticServiceOffer/entities/logisticServiceOffer/LogisticServiceOfferDto'
import { ServiceOfferDto } from '@/serviceOffer/entities/serviceOffer/ServiceOfferDto'
import type { MessageDto } from '@/shared/administration/entities/messages/MessageDto'
// others
import { Model, belongsTo, hasMany } from 'miragejs'
import { BelongsTo, HasMany, ModelDefinition } from 'miragejs/-types'

export type CarlConfigurationInterventionForeignKeys = {
    carlClient: BelongsTo<'carlClient'>
}
export type DemandeInterventionForeignKeys = {
    createdBy: BelongsTo<'gmaoActeur'>
    relatedEquipement?: BelongsTo<'equipement'>
}
export type EquipementForeignKeys = {
    children: HasMany<'equipement'>
    compteurs: HasMany<'compteur'>
}
export type CompteurForeignKeys = {
    equipement: BelongsTo<'equipement'>
}
export type InterventionForeignKeys = {
    relatedEquipement: BelongsTo<'equipement'>
    createdBy: BelongsTo<'gmaoActeur'>
    intervenantsAffectes: HasMany<'gmaoActeur'>
    activitesDiagnostique: HasMany<'activiteDiagnostique'>
    activitesAction: HasMany<'activiteAction'>
}

export type GmaoActeurForeignKeys = {
    team: HasMany<'gmaoActeur'>
    operationsHabilitees: HasMany<'operation'>
}

export type InterventionServiceOfferForeignKeys = {
    availableEquipements: HasMany<'equipement'>
    gmaoConfiguration: BelongsTo<'carlConfigurationIntervention'>
}
export type ActiviteActionForeignKeys = {
    intervention: BelongsTo<'intervention'>
    equipementCible: BelongsTo<'equipement'>
    intervenant: BelongsTo<'gmaoActeur'>
    operation: BelongsTo<'operation'>
}

export type ActiviteDiagnostiqueForeignKeys = {
    intervention: BelongsTo<'intervention'>
    equipementCible: BelongsTo<'equipement'>
    intervenant: BelongsTo<'gmaoActeur'>
}

export const models = {
    // dummy models
    serviceOffer: Model as ModelDefinition<ServiceOfferDto>,
    // without factories
    logisticServiceOffer: Model as ModelDefinition<LogisticServiceOfferDto>,
    user: Model as ModelDefinition<UserDto>,
    message: Model as ModelDefinition<MessageDto>,
    // with factories (types are defined in their respective factories)
    siilabConfiguration: Model,
    article: Model,
    articleReservation: Model,
    carlClient: Model,
    carlConfigurationIntervention:
        Model.extend<CarlConfigurationInterventionForeignKeys>({
            carlClient: belongsTo('carlClient'),
        }),
    compteur: Model.extend<CompteurForeignKeys>({
        equipement: belongsTo(),
    }),
    costCenter: Model,
    demandeIntervention: Model.extend<DemandeInterventionForeignKeys>({
        relatedEquipement: belongsTo('equipement'),
        createdBy: belongsTo('gmaoActeur'),
    }),
    equipement: Model.extend<EquipementForeignKeys>({
        children: hasMany('equipement'),
        compteurs: hasMany('compteur'),
    }),
    intervention: Model.extend<InterventionForeignKeys>({
        activitesDiagnostique: hasMany('activiteDiagnostique'),
        activitesAction: hasMany('activiteAction'),
        relatedEquipement: belongsTo('equipement'),
        createdBy: belongsTo('gmaoActeur'),
        intervenantsAffectes: hasMany('gmaoActeur'),
    }),
    interventionServiceOffer: Model.extend<InterventionServiceOfferForeignKeys>(
        {
            availableEquipements: hasMany('equipement'),
            gmaoConfiguration: belongsTo('carlConfigurationIntervention'),
        }
    ),
    logisticIntervention: Model,
    mouvement: Model,
    mouvementDeSortie: Model,
    activiteAction: Model.extend<ActiviteActionForeignKeys>({
        equipementCible: belongsTo('equipement'),
        intervention: belongsTo('intervention'),
        intervenant: belongsTo('gmaoActeur'),
        operation: belongsTo('operation'),
    }),
    operation: Model,
    action: Model,
    activiteDiagnostique: Model.extend<ActiviteDiagnostiqueForeignKeys>({
        equipementCible: belongsTo('equipement'),
        intervention: belongsTo('intervention'),
        intervenant: belongsTo('gmaoActeur'),
    }),
    gmaoActeur: Model.extend<GmaoActeurForeignKeys>({
        team: hasMany('gmaoActeur'),
        operationsHabilitees: hasMany('operation'),
    }),
}
