import { AppServer } from '@/mock-api/types'

export function useCarlClientSeed(server: AppServer) {
    server.create('carlClient', {
        id: 'carl-client-test',
        title: 'Carl TEST',
    })

    server.create('carlClient', {
        id: 'carl-client-integ',
        title: 'Carl INTEG',
    })

    server.create('carlClient', {
        id: 'carl-client-prod',
        title: 'Carl PROD',
    })
}
