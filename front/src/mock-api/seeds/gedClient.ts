import { AppServer } from '@/mock-api/types'

export function useGedClientSeed(server: AppServer) {
    server.create('gedClient', {
        id: 'gedClient_alfresco',
        title: 'Alfresco',
    })
}
