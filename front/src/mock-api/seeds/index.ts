import { AppServer } from '@/mock-api/types'
import { useCarlClientSeed } from './carlClient'
import { useGedClientSeed } from './gedClient'
import { useServiceOfferSeed } from './serviceOffer'
import { useSiilabConfigurationSeed } from './siilabConfiguration'

export function seeds(server: AppServer) {
    // shared
    useCarlClientSeed(server)
    useGedClientSeed(server)
    useSiilabConfigurationSeed(server)
    server.loadFixtures()

    // all serviceOffers
    useServiceOfferSeed(server)
}
