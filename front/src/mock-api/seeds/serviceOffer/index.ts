import { AppServer } from '@/mock-api/types'
import { useInterventionServiceOfferSeed } from './intervention'
import { useLogisticServiceOfferSeed } from './logistic'

export function useServiceOfferSeed(server: AppServer) {
    useInterventionServiceOfferSeed(server)
    useLogisticServiceOfferSeed(server)
}
