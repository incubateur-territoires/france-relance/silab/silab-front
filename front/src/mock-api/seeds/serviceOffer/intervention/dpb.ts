import { AppRegistry, AppServer } from '@/mock-api/types'
import { isNull } from '@/shared/helpers/types'
import { faker } from '@faker-js/faker/locale/fr'
import { Instantiate } from 'miragejs'

export function useDPBInterventionServiceOfferSeed(server: AppServer) {
    const silabTest = server.schema.find(
        'gmaoActeur',
        import.meta.env.VITE_AZURE_TEST_USERNAME
    )
    const jeanDoux = server.schema.find('gmaoActeur', 'jean-doux-uniq666')
    const janeDoe = server.schema.find('gmaoActeur', 'jane-doe-uniq666')
    const johnDoe = server.schema.find('gmaoActeur', 'john-doe-uniq666')
    const intervenantAffectes: Instantiate<AppRegistry, 'gmaoActeur'>[] = []
    if (
        isNull(johnDoe) ||
        isNull(janeDoe) ||
        isNull(jeanDoux) ||
        isNull(silabTest)
    ) {
        throw new Error('Des gmaoActeur nécessaires manquent.')
    }
    intervenantAffectes.push(johnDoe)
    intervenantAffectes.push(janeDoe)
    intervenantAffectes.push(jeanDoux)
    intervenantAffectes.push(silabTest)

    server.create('interventionServiceOffer', {
        id: 'dpb',
        title: 'DPB - Supervision',
        image: 'https://loremflickr.com/320/240/construction,workers',
        link: 'intervention-service-offer/index',
        configurationFormulaireActivite: {
            dateDeDebut: 'jour',
            heureParDefautEnMinute: 8 * 60,
        },
        availableEquipementsIds: [
            server.create('equipement', {
                id: 'ancienne-comedie',
                label: 'Ancienne Comédie',
                coordinates: {
                    lat: faker.location.nearbyGPSCoordinate({
                        origin: [46.58464530116541, 0.34471313835438444],
                        radius: 10,
                    })[0],
                    lng: faker.location.nearbyGPSCoordinate({
                        origin: [46.58464530116541, 0.34471313835438444],
                        radius: 10,
                    })[1],
                },
                imagesIllustration: [
                    {
                        id: faker.string.uuid(),
                        url: faker.image.urlLoremFlickr({
                            category: 'building',
                        }),
                    },
                    {
                        id: faker.string.uuid(),
                        url: faker.image.urlLoremFlickr({
                            category: 'building',
                        }),
                    },
                ],
                comment: 'DPB - Supervision',
                gmaoObjectViewUrl:
                    'https://ceci-est-un-lien-gmao-veuillez-circuler/ancienne-comedie',
                documentsJoints: [
                    {
                        id: faker.string.uuid(),
                        url: 'https://dpb.supervision/photo-avant.jpg',
                        name: 'Photo avant',
                    },
                    {
                        id: faker.string.uuid(),
                        url: 'https://dpb.supervision/photo-apres.jpg',
                        name: 'Photo après',
                    },
                ],
                interventionsAutorisees: false,
            }),
            ...server.createList(
                'equipement',
                faker.number.int({ min: 2, max: 4 })
            ),
        ].map((eqpt) => {
            return eqpt.id
        }),
        template: 'InterventionServiceOffer',
        gmaoConfiguration: server.create('carlConfigurationIntervention', {
            id: 'carlConfigurationIntervention_dpb',
            title: 'DPB - Supervision',
            carlClient: server.schema.find('carlClient', 'carl-client-integ'),
        }),
        environnement: 'INTEG',
        gestionRecompletementActif: true,
        gestionDatesPourInterventionActif: true,
    })
    server.create('intervention', {
        id: 'inter-dpb-avec-seed',
        title: "Remplacement d'un détecteur d'éclairage",
        code: 'OT-0082123',
        statusHistory: [
            {
                code: 'awaiting_realisation',
                label: 'A faire',
                createdAt: faker.date.recent().toISOString(),
                createdBy: 'John Doe',
            },
        ],
        createdBy: jeanDoux,
        dateDeDebut: '2023-08-13T08:00:00.000',
        dateDeFin: '2023-08-14T16:00:00.000',
        documentsJoints: [
            {
                id: faker.string.uuid(),
                url: 'https://dpb.supervision/photo-avant.jpg',
                name: 'Photo avant',
            },
            {
                id: faker.string.uuid(),
                url: 'https://dpb.supervisionp/photo-apres.jpg',
                name: 'Photo après',
            },
        ],
        intervenantsAffectes: intervenantAffectes,
    })
}
