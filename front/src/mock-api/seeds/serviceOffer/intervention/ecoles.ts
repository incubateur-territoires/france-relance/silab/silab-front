import { AppServer } from '@/mock-api/types'
import { faker } from '@faker-js/faker/locale/fr'

export function useEcolesInterventionServiceOfferSeed(server: AppServer) {
    const ecolesEquipements = [
        server.create('equipement', {
            id: '{"id":"ecole-batiment-1","type":"buildingset"}',
            label: 'EE des petits canailloux',
            coordinates: {
                lat: 46.58464530116541,
                lng: 0.34471313835438444,
            },
            gmaoObjectViewUrl:
                'https://ceci-est-un-lien-gmao-veuillez-circuler/ecole-batiment-1',
            comment: 'Équipement des canailloux',
            children: server.createList(
                'equipement',
                faker.number.int({ min: 2, max: 4 })
            ),
        }),
        server.create('equipement', {
            id: '{"id":"ecole-batiment-2","type":"buildingset"}',
            label: 'Ecole Maternelle F. Pignon',
            coordinates: {
                lat: 46.58380183966383,
                lng: 0.3256227684300361,
            },
            gmaoObjectViewUrl:
                'https://ceci-est-un-lien-gmao-veuillez-circuler/ecole-batiment-2',
            children: server.createList(
                'equipement',
                faker.number.int({ min: 2, max: 4 })
            ),
        }),
        server.create('equipement', {
            id: '{"id":"ecole-batiment-3","type":"buildingset"}',
            label: 'Ecole Maxime Renard',
            coordinates: {
                lat: 46.57436300021305,
                lng: 0.330210377284077,
            },
            gmaoObjectViewUrl:
                'https://ceci-est-un-lien-gmao-veuillez-circuler/ecole-batiment-3',
            children: server.createList(
                'equipement',
                faker.number.int({ min: 2, max: 4 })
            ),
        }),
        server.create('equipement', {
            id: '{"id":"ecole-batiment-4","type":"building"}',
            label: 'Ecole George moustachequi',
            coordinates: {
                lat: 46.571026938571,
                lng: 0.34981869888021766,
            },
            gmaoObjectViewUrl:
                'https://ceci-est-un-lien-gmao-veuillez-circuler/ecole-batiment-4',
            children: server.createList(
                'equipement',
                faker.number.int({ min: 2, max: 4 })
            ),
        }),
    ]

    server.create('interventionServiceOffer', {
        id: 'ecoles-poitiers',
        title: 'Ecoles de Poitiers',
        image: 'https://loremflickr.com/320/240/schoolbuilding?lock=8',
        link: 'intervention-service-offer/index',
        template: 'InterventionServiceOffer',
        configurationFormulaireActivite: {
            dateDeDebut: 'jour',
            heureParDefautEnMinute: 8 * 60,
        },
        availableEquipementsIds: ecolesEquipements.map((eqpt) => {
            return eqpt.id
        }),
        gmaoConfiguration: server.create('carlConfigurationIntervention', {
            id: 'carlConfigurationIntervention_ecoles',
            title: 'Ecoles de Poitiers',
            carlClient: server.schema.find('carlClient', 'carl-client-integ'),
        }),
        environnement: 'INTEG',
    })

    server.create('demandeIntervention', {
        id: 'di-ecole-415',
        code: 'DI-00666',
        title: 'Trou dans le préau',
        createdBy: server.create('gmaoActeur', {
            emails: ['rene.lataupe@gp.fr'],
            nomComplet: 'Rene Lataupe',
        }),
        relatedEquipement: ecolesEquipements[0],
        address: '666 rue des mignons 8600 Pasla',
        description:
            'Au fond un trou diamètre 50 à reboucher avec panneua métallique',
    })
    server.create('demandeIntervention', {
        id: 'di-ecole-416',
        code: 'DI-00667',
        title: 'Préau dans le trou',
        currentStatus: {
            code: 'awaiting_information',
            label: 'En attente de renseignement',
            createdAt: new Date().toISOString(),
            createdBy: 'Jean Doux',
        },
        createdBy: server.create('gmaoActeur', {
            emails: ['rene.lataupe@gp.fr'],
            nomComplet: 'Rene Lataupe',
        }),
        relatedEquipement: ecolesEquipements[0],
        address: '666 rue des mignons 8600 Pasla',
        description: 'Préau à retirer du trou',
    })
}
