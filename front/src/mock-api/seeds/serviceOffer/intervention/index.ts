import { AppServer } from '@/mock-api/types'
import { useEcolesInterventionServiceOfferSeed } from './ecoles'
import { usePropreteInterventionServiceOfferSeed } from './proprete'
import { useStepInterventionServiceOfferSeed } from './step'
import { useDPBInterventionServiceOfferSeed } from './dpb'

export function useInterventionServiceOfferSeed(server: AppServer) {
    const acteurConnecté = server.create('gmaoActeur', {
        id: import.meta.env.VITE_AZURE_TEST_USERNAME,
        nomComplet: 'silab test',
    })

    acteurConnecté.operationsHabilitees.add(
        server.create('operation', {
            id: 'operation-deboucher',
            label: 'déboucher',
        })
    )
    server.create('gmaoActeur', {
        id: 'jean-doux-uniq666',
        nomComplet: 'Jean Doux',
        telephones: ['06 66 69 00 57', '06 66 69 00 99'],
    })
    server.create('gmaoActeur', {
        id: 'jane-doe-uniq666',
        nomComplet: 'Jane Doe',
        telephones: ['06 66 69 00 57', '06 66 69 00 99'],
    })
    server.create('gmaoActeur', {
        id: 'jana-doux-uniq666',
        nomComplet: 'Jana Doux',
        telephones: ['06 66 69 00 57', '06 66 69 00 99'],
    })
    server.create('gmaoActeur', {
        id: 'john-doe-uniq666',
        nomComplet: 'John Doe',
        telephones: ['06 66 69 00 57', '06 66 69 00 99'],
    })
    server.create('gmaoActeur', {
        id: 'jana-doe-uniq666',
        nomComplet: 'Jana Doe',
        telephones: ['06 66 69 00 57', '06 66 69 00 99'],
    })

    server.createList('operation', 4)

    usePropreteInterventionServiceOfferSeed(server)
    useStepInterventionServiceOfferSeed(server)
    useEcolesInterventionServiceOfferSeed(server)
    useDPBInterventionServiceOfferSeed(server)
}
