import { AppServer } from '@/mock-api/types'
import { usePropreteInterventionSeed } from './intervention'

export function usePropreteInterventionServiceOfferSeed(server: AppServer) {
    usePropreteInterventionSeed(server)
    server.create('interventionServiceOffer', {
        id: 'proprete',
        title: 'Propreté',
        image: 'https://static6.depositphotos.com/1003620/572/i/450/depositphotos_5728710-stock-photo-green-business.jpg',
        link: 'intervention-service-offer/index',
        template: 'InterventionServiceOffer',
        availableEquipementsIds: [],
        configurationFormulaireActivite: {
            dateDeDebut: 'automatique',
            heureParDefautEnMinute: 8 * 60,
        },
        gmaoConfiguration: server.create('carlConfigurationIntervention', {
            id: 'carlConfigurationIntervention_propreté',
            title: 'Propreté',
            carlClient: server.schema.find('carlClient', 'carl-client-prod'),
            actionTypesMap: {
                ID_ACTIONTYPE_BALAYAGE_MANUELLE: 'Balayage manuelle',
                ID_ACTIONTYPE_BALAYEUSE: 'Balayeuse',
                ID_ACTIONTYPE_CORBEILLE: 'Corbeille',
                ID_ACTIONTYPE_ENCOMBRANT: 'Encombrant',
                ID_ACTIONTYPE_LAVEUSE: 'Laveuse',
                ID_ACTIONTYPE_RAMASSAGE_HERBE: 'Ramassage herbe',
                ID_ACTIONTYPE_SOUFFLAGE_FEUILLE: 'Soufflage feuille',
            },
        }),
        environnement: 'PROD',
    })
}
