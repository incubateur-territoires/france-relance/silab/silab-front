import { InterventionPriority } from '@/interventionServiceOffer/entities/intervention/InterventionDto'
import { AppServer } from '@/mock-api/types'
import { faker } from '@faker-js/faker/locale/fr'

export function usePropreteInterventionSeed(server: AppServer) {
    const jeanDoux = server.schema.findBy('gmaoActeur', {
        id: 'jean-doux-uniq666',
    })
    const janeDoe = server.schema.findBy('gmaoActeur', {
        id: 'jane-doe-uniq666',
    })
    const janaDoux = server.schema.findBy('gmaoActeur', {
        id: 'jana-doux-uniq666',
    })
    const johnDoe = server.schema.findBy('gmaoActeur', {
        id: 'john-doe-uniq666',
    })
    const janaDoe = server.schema.findBy('gmaoActeur', {
        id: 'jana-doe-uniq666',
    })
    server.create('intervention', {
        id: 'ot-proprete-1',
        title: 'Trois cités',
        address: '49 Rue Puvis de Chavannes 86000 Poitiers',
        description: 'Lavage et balayage',
        relatedEquipement: null,
        imagesAvant: [
            {
                id: faker.string.uuid(),
                url: faker.image.url(),
            },
            {
                id: faker.string.uuid(),
                url: faker.image.url(),
            },
        ],
        imagesApres: [
            {
                id: faker.string.uuid(),
                url: faker.image.url(),
            },
        ],
        actionType: {
            id: 'ID_ACTIONTYPE_BALAYAGE_MANUELLE',
            label: 'Balayage manuelle',
        },
        code: 'OT-000123',
        createdBy: johnDoe,
        statusHistory: [
            {
                code: 'awaiting_realisation',
                label: 'A faire',
                createdBy: 'John Doe',
                createdAt: '2022-08-25T10:00:00.000Z',
            },
        ],
        priority: InterventionPriority.Urgent,
        createdAt: '2022-08-25T10:00:00.000Z',
        latitude: 46.5667544,
        longitude: 0.3411735,
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/ot-proprete-1',
    })
    server.create('intervention', {
        id: 'ot-proprete-2',
        title: 'Mariéville Foucaudière',
        address: '2 rue mariéville foucaudière',
        description: 'Lavage et balayage',
        relatedEquipement: null,
        imagesAvant: [],
        imagesApres: [],
        actionType: { id: 'ID_ACTIONTYPE_LAVEUSE', label: 'Laveuse' },
        code: 'OT-000150',
        createdBy: janeDoe,
        statusHistory: [
            {
                code: 'closed',
                label: 'Réalisé',
                createdBy: 'Jane Doe',
                createdAt: '2022-08-15T08:00:00.000Z',
            },
        ],
        priority: InterventionPriority.Urgent,
        createdAt: '2022-08-15T08:00:00.000Z',
        latitude: 46.5737885,
        longitude: 0.3896336,
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/ot-proprete-2',
    })
    server.create('intervention', {
        id: 'ot-proprete-3',
        title: 'Rue la vallée monaie',
        address: '3 rue de la vallée monaie',
        description: 'Lavage et balayage',
        relatedEquipement: null,
        imagesAvant: [
            {
                id: faker.string.uuid(),
                url: faker.image.url(),
            },
        ],
        imagesApres: [],
        actionType: { id: 'ID_ACTIONTYPE_LAVEUSE', label: 'Laveuse' },
        code: 'OT-000200',
        createdBy: janeDoe,
        statusHistory: [
            {
                code: 'awaiting_realisation',
                label: 'A faire',
                createdBy: 'Jane Doe',
                createdAt: '2022-07-01T08:00:00.000Z',
            },
        ],
        priority: InterventionPriority.Important,
        createdAt: '2022-07-01T08:00:00.000Z',
        latitude: 46.5655334,
        longitude: 0.3405952,
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/ot-proprete-3',
    })
    server.create('intervention', {
        id: 'ot-proprete-4',
        title: 'Place des templiers',
        address: '4 Place des Templiers',
        description: 'Enlèvement des affiches',
        relatedEquipement: null,
        imagesAvant: [
            {
                id: faker.string.uuid(),
                url: faker.image.url(),
            },
        ],
        imagesApres: [],
        actionType: { id: 'ID_ACTIONTYPE_BALAYEUSE', label: 'Balayeuse' },
        code: 'OT-123000',
        createdBy: jeanDoux,
        statusHistory: [
            {
                code: 'awaiting_realisation',
                label: 'A faire',
                createdBy: 'Jean Doux',
                createdAt: '2022-07-10T08:00:00.000Z',
            },
        ],
        priority: InterventionPriority.Important,
        createdAt: '2022-07-10T08:00:00.000Z',
        latitude: 46.5748922,
        longitude: 0.3759499,
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/ot-proprete-4',
    })
    server.create('intervention', {
        id: 'ot-proprete-5',
        title: 'Rue gabriel morain',
        address: '5 rue de gabriel morain',
        description: 'Lavage et balayage',
        relatedEquipement: null,
        imagesAvant: [
            {
                id: faker.string.uuid(),
                url: faker.image.url(),
            },
        ],
        imagesApres: [],
        actionType: { id: 'ID_ACTIONTYPE_LAVEUSE', label: 'Laveuse' },
        code: 'OT-200000',
        createdBy: jeanDoux,
        statusHistory: [
            {
                code: 'awaiting_realisation',
                label: 'A faire',
                createdBy: 'Jean Doux',
                createdAt: '2022-07-20T08:00:00.000Z',
            },
        ],
        priority: InterventionPriority.Important,
        createdAt: '2022-07-20T08:00:00.000Z',
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/ot-proprete-5',
    })
    server.create('intervention', {
        id: 'ot-proprete-6',
        title: "Triangle d'or",
        address: '7 Rue Jean de la Fontaine',
        description: 'Enlèvement des tags',
        relatedEquipement: null,
        imagesAvant: [
            {
                id: faker.string.uuid(),
                url: faker.image.url(),
            },
        ],
        imagesApres: [],
        actionType: { id: 'ID_ACTIONTYPE_BALAYEUSE', label: 'Balayeuse' },
        code: 'OT-150000',
        createdBy: janaDoux,
        statusHistory: [
            {
                code: 'awaiting_realisation',
                label: 'A faire',
                createdBy: 'Jana Doux',
                createdAt: '2022-08-01T08:00:00.000Z',
            },
        ],
        priority: InterventionPriority.Normal,
        createdAt: '2022-08-01T08:00:00.000Z',
        latitude: 46.5651747,
        longitude: 0.3439021,
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/ot-proprete-6',
    })
    server.create('intervention', {
        id: 'ot-proprete-7',
        title: '20 rue émile duclaux',
        address: '20 rue émile duclaux',
        description: 'Ramassage des feuilles mortes',
        relatedEquipement: null,
        imagesAvant: [],
        imagesApres: [],
        actionType: {
            id: 'ID_ACTIONTYPE_RAMASSAGE_HERBE',
            label: 'Ramassage herbe',
        },
        code: 'OT-500000',
        createdBy: janaDoux,
        statusHistory: [
            {
                code: 'closed',
                label: 'Réalisé',
                createdBy: 'Jana Doux',
                createdAt: '2022-08-10T08:00:00.000Z',
            },
        ],
        priority: InterventionPriority.Normal,
        createdAt: '2022-08-10T08:00:00.000Z',
        latitude: 46.577041,
        longitude: 0.351548,
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/ot-proprete-7',
    })
    server.create('intervention', {
        id: 'ot-proprete-8',
        title: 'Pont rochereuil',
        address: '6 Rue des 4 Roues',
        description: 'Corbeilles vidées',
        relatedEquipement: null,
        imagesAvant: [],
        imagesApres: [],
        actionType: {
            id: 'ID_ACTIONTYPE_RAMASSAGE_HERBE',
            label: 'Ramassage herbe',
        },
        code: 'OT-321000',
        createdBy: johnDoe,
        statusHistory: [
            {
                code: 'awaiting_realisation',
                label: 'A faire',
                createdBy: 'John Doe',
                createdAt: '2022-08-10T08:00:00.000Z',
            },
        ],
        priority: InterventionPriority.Bas,
        createdAt: '2022-08-10T08:00:00.000Z',
        latitude: 46.5911901,
        longitude: 0.3413963,
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/ot-proprete-8',
    })
    server.create('intervention', {
        id: 'ot-proprete-9',
        title: '20 Bd Solférino',
        address: '20 Bd Solférino',
        description: 'Désherbage',
        relatedEquipement: null,
        imagesAvant: [
            {
                id: faker.string.uuid(),
                url: faker.image.url(),
            },
        ],
        imagesApres: [],
        actionType: { id: 'ID_ACTIONTYPE_ENCOMBRANT', label: 'Encombrant' },
        code: 'OT-100123',
        createdBy: janaDoe,
        statusHistory: [
            {
                code: 'awaiting_realisation',
                label: 'A faire',
                createdBy: 'Jana Doe',
                createdAt: '2022-08-07T08:00:00.000Z',
            },
        ],
        priority: InterventionPriority.Bas,
        createdAt: '2022-08-07T08:00:00.000Z',
        latitude: 46.5842296,
        longitude: 0.3356835,
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/ot-proprete-9',
    })
    server.create('intervention', {
        id: 'ot-proprete-10',
        title: '13 Rue des Pouples',
        address: '13 Rue des Pouples',
        description: 'Enlèvement des graffitis',
        relatedEquipement: null,
        imagesAvant: [
            {
                id: faker.string.uuid(),
                url: faker.image.url(),
            },
        ],
        imagesApres: [],
        actionType: { id: 'ID_ACTIONTYPE_BALAYEUSE', label: 'Balayeuse' },
        code: 'OT-123456',
        createdBy: janaDoe,
        statusHistory: [
            {
                code: 'closed',
                label: 'Réalisé',
                createdBy: 'Jana Doe',
                createdAt: '2022-08-10T08:00:00.000Z',
            },
        ],
        priority: InterventionPriority.Bas,
        createdAt: '2022-08-10T08:00:00.000Z',
        latitude: 46.5828224,
        longitude: 0.348105,
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/ot-proprete-10',
    })
}
