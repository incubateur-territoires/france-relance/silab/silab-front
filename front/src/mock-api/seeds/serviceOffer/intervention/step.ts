import { AppServer } from '@/mock-api/types'
import { faker } from '@faker-js/faker/locale/fr'

export function useStepInterventionServiceOfferSeed(server: AppServer) {
    const jeanDoux = server.schema.findBy('gmaoActeur', {
        id: 'jean-doux-uniq666',
    })
    const johnDoe = server.schema.findBy('gmaoActeur', {
        id: 'john-doe-uniq666',
    })
    const parentEquipement = server.create('equipement', {
        id: 'equipement-parent',
        label: 'Parent',
    })
    const grandParentEquipement = server.create('equipement', {
        id: 'equipement-grandparent',
        label: 'Grand parent',
    })
    const equipementEnfant = server.create('equipement', {
        id: 'cuve-de-decantation',
        label: 'Cuve de décantation',
        code: 'EQPT-710997',
        compteurs: [
            server.create('compteur', {
                id: 'cpt-1',
                code: 'CP-927381',
                label: 'capteur de débordement',
                type: 'boolean',
                unite: 'débordement',
                derniereMesure: undefined,
            }),
            server.create('compteur', {
                id: 'cpt-2',
                code: 'CP-684585',
                label: 'capteur de co2',
                type: 'float',
                unite: 'ppm',
                derniereMesure: {
                    valeur: 974.33,
                    createdAt: '2024/01/01 11:00',
                    createdBy: 'Jean Doux',
                },
            }),
            server.create('compteur', {
                id: 'cpt-3',
                code: 'CP-189616',
                label: "compteur d'heures",
                type: 'float',
                unite: 'heure',
                derniereMesure: undefined,
            }),
        ],
    })
    server.create('interventionServiceOffer', {
        id: 'step',
        title: 'STEP - Chasseneuil',
        image: 'https://loremflickr.com/320/240/treatment-plant?lock=3',
        link: 'intervention-service-offer/index',
        configurationFormulaireActivite: {
            dateDeDebut: 'heure',
            heureParDefautEnMinute: 8 * 60,
        },
        availableEquipementsIds: [
            server.create('equipement', {
                id: 'equipement-avec-enfants',
                label: 'Equipement avec enfants',
                coordinates: {
                    lat: faker.location.nearbyGPSCoordinate({
                        origin: [46.58464530116541, 0.34471313835438444],
                        radius: 10,
                    })[0],
                    lng: faker.location.nearbyGPSCoordinate({
                        origin: [46.58464530116541, 0.34471313835438444],
                        radius: 10,
                    })[1],
                },
                imagesIllustration: [
                    {
                        id: faker.string.uuid(),
                        url: faker.image.urlLoremFlickr({
                            category: 'building',
                        }),
                    },
                    {
                        id: faker.string.uuid(),
                        url: faker.image.urlLoremFlickr({
                            category: 'building',
                        }),
                    },
                ],
                comment: 'Equipement STEP - Chasseneuil',
                gmaoObjectViewUrl:
                    'https://ceci-est-un-lien-gmao-veuillez-circuler/equipement-avec-enfants',
                children: [
                    ...server.createList('equipement', 5), // doit être au moins 5 pour avoir le bouton "voir plus"
                    equipementEnfant,
                ],
                documentsJoints: [
                    {
                        id: faker.string.uuid(),
                        url: 'https://step.chasseneuil/photo-avant.jpg',
                        name: 'Photo avant',
                    },
                    {
                        id: faker.string.uuid(),
                        url: 'https://step.chasseneuil/photo-apres.jpg',
                        name: 'Photo après',
                    },
                ],
                ancetres: {
                    'arborescence-principale': [
                        {
                            id: grandParentEquipement.id,
                            label: grandParentEquipement.label,
                        },
                        {
                            id: parentEquipement.id,
                            label: parentEquipement.label,
                        },
                    ],
                },
            }),
            ...server.createList(
                'equipement',
                faker.number.int({ min: 1, max: 2 })
            ),
        ].map((eqpt) => {
            return eqpt.id
        }),
        gmaoConfiguration: server.create('carlConfigurationIntervention', {
            id: 'carlConfigurationIntervention_step',
            title: 'STEP - Chasseneuil',
            carlClient: server.schema.find('carlClient', 'carl-client-test'),
        }),
        environnement: 'TEST',
    })
    const linkedDemandeIntervention = server.create('demandeIntervention', {
        id: 'demande-intervention-lié-a-cuve-de-decantation',
        code: 'DI-59487',
        relatedEquipement: equipementEnfant,
        statusHistory: [
            {
                code: 'awaiting_validation',
                label: 'En attente de validation',
                createdAt: faker.date.recent().toISOString(),
                createdBy: faker.internet.email(),
            },
        ],
        createdAt: faker.date.recent().toISOString(),
        createdBy: jeanDoux,
        title: 'Inspection et maintenance de la cuve de décantation',
        description:
            "Vérifier l'état général et assurer le fonctionnement optimal.",
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/demande-intervention-avec-url-gmao-seed',
    })
    server.create('intervention', {
        id: 'inter-avec-activite-seed',
        title: 'controle de la cuve de décentation',
        code: 'OT-0078495',
        relatedEquipement: equipementEnfant,
        statusHistory: [
            {
                code: 'awaiting_realisation',
                label: 'A faire',
                createdAt: faker.date.recent().toISOString(),
                createdBy: faker.internet.email(),
            },
        ],
        createdBy: jeanDoux,
        activitesDiagnostique: server.createList('activiteDiagnostique', 3, {
            equipementCible: equipementEnfant,
        }),
        activitesAction: server.createList('activiteAction', 2, {
            equipementCible: equipementEnfant,
        }),
        demandeInterventionIds: [linkedDemandeIntervention.id],
        documentsJoints: [
            {
                id: faker.string.uuid(),
                url: 'https://interventions.step/photo-avant.jpg',
                name: 'Photo avant',
            },
            {
                id: faker.string.uuid(),
                url: 'https://interventions.step/photo-apres.jpg',
                name: 'Photo après',
            },
        ],
    })
    server.create('intervention', {
        id: 'inter-cloturé-seed',
        title: 'Curré la cuve',
        code: 'OT-0078495',
        relatedEquipement: equipementEnfant,
        statusHistory: [
            {
                code: 'closed',
                label: 'Réalisé',
                createdAt: faker.date.recent().toISOString(),
                createdBy: faker.internet.email(),
            },
        ],
        createdBy: jeanDoux,
        activitesDiagnostique: server.createList('activiteDiagnostique', 3, {
            equipementCible: equipementEnfant,
        }),
        activitesAction: server.createList('activiteAction', 2, {
            equipementCible: equipementEnfant,
        }),
    })
    server.create('demandeIntervention', {
        id: 'demande-intervention-avec-url-gmao-seed',
        code: 'DI-05447',
        relatedEquipement: equipementEnfant,
        statusHistory: [
            {
                code: 'awaiting_validation',
                label: 'En attente de validation',
                createdAt: faker.date.recent().toISOString(),
                createdBy: faker.internet.email(),
            },
        ],
        createdAt: faker.date.recent().toISOString(),
        createdBy: johnDoe,
        title: 'Repeindre les murs',
        description: 'les murs du hall sont décrepit',
        gmaoObjectViewUrl:
            'https://ceci-est-un-lien-gmao-veuillez-circuler/demande-intervention-avec-url-gmao-seed',
        documentsJoints: [
            {
                id: faker.string.uuid(),
                url: 'https://step.chasseneuil/photo-avant.jpg',
                name: 'Photo avant',
            },
            {
                id: faker.string.uuid(),
                url: 'https://step.chasseneuil/photo-apres.jpg',
                name: 'Photo après',
            },
        ],
    })
}
