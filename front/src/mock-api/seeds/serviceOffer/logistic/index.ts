import { AppServer } from '@/mock-api/types'
import { useMagasinGeneralLogisticServiceOfferSeed } from './magasinGeneral'

export function useLogisticServiceOfferSeed(server: AppServer) {
    useMagasinGeneralLogisticServiceOfferSeed(server)
}
