import { AppServer } from '@/mock-api/types'
import { faker } from '@faker-js/faker/locale/fr'

export function useMagasinGeneralArticleSeed(server: AppServer) {
    server.createList('article', 100)
    // on inclus des articles avec des codes connus pour pouvoir jouer avec l'application
    server.create('article', {
        code: 'ART-TEST',
    })
    server.create('article', {
        code: 'ART-SANSEMPLACEMENT',
        lots: [
            {
                magasinId: faker.string.uuid(),
                emplacement: undefined,
                quantité: faker.number.int(200),
            },
        ],
    })
    server.create('article', {
        code: 'ART-TITRELONG',
        titre: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vulputate dolor ac purus vehicula, ac mattis nisl placerat',
    })

    server.create('article', {
        code: 'ART-MULTIEMPLACEMENTS',
        lots: [
            {
                magasinId: faker.string.uuid(),
                emplacement: {
                    id: faker.string.uuid(),
                    code: `${faker.string
                        .alpha(1)
                        .toUpperCase()}-${faker.string.numeric(
                        1
                    )}-${faker.string.numeric(3)}`,
                },
                quantité: faker.number.int(200),
            },
            {
                magasinId: faker.string.uuid(),
                emplacement: undefined,
                quantité: faker.number.int(200),
            },
        ],
    })
}
