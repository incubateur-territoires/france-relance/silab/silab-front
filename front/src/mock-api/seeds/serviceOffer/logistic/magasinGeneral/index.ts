import { AppServer } from '@/mock-api/types'
import { useMagasinGeneralArticleSeed } from './article'
import { useMagasinGeneralArticleReservationSeed } from './articleReservation'
import { useMagasinGeneralInterventionSeed } from './intervention'

export function useMagasinGeneralLogisticServiceOfferSeed(server: AppServer) {
    useMagasinGeneralInterventionSeed(server)
    useMagasinGeneralArticleReservationSeed(server)
    useMagasinGeneralArticleSeed(server)
    //Note : on utilise encore des fixture pour les ods logistiques, la factory est à créer
}
