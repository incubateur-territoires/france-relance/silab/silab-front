import { AppServer } from '@/mock-api/types'

export function useMagasinGeneralInterventionSeed(server: AppServer) {
    server.createList('logisticIntervention', 20)
    server.create('logisticIntervention', {
        id: 'ot-stock-1',
        titre: 'cuisine centrale andersen',
        centreDeCouts: 'Direction restauration collective',
        createdBy: 'Jean Doux',
        code: 'OT-747641',
        historiqueDesEtats: [
            {
                code: 'awaiting_realisation',
                label: 'En cours',
                createdBy: 'Jean Doux',
                createdAt: '2023-01-09T16:28:34.926Z',
            },
        ],
    })
    server.create('logisticIntervention', {
        id: 'ot-stock-2',
        titre: 'BREUIL MINGOT EPI',
        centreDeCouts: 'Direction restauration collective',
        createdBy: 'Jean Doux',
        code: 'OT-748494',
        historiqueDesEtats: [
            {
                code: 'awaiting_realisation',
                label: 'En cours',
                createdBy: 'Jean Doux',
                createdAt: '2023-01-09T16:28:34.926Z',
            },
            {
                code: 'awaiting_realisation',
                label: 'Attente fournitures',
                createdBy: 'Jean Doux',
                createdAt: '2023-01-08T16:28:34.926Z',
            },
            {
                code: 'awaiting_realisation',
                label: 'En préparation',
                createdBy: 'Jean Doux',
                createdAt: '2023-01-01T15:22:34.926Z',
            },
            {
                code: 'awaiting_realisation',
                label: 'Validé - Pris en compte',
                createdBy: 'El Chapeau',
                createdAt: '2023-01-02T15:22:34.926Z',
            },
        ],
    })
    server.create('logisticIntervention', {
        id: 'ot-stock-3',
        titre: 'cuisine centrale andersen',
        centreDeCouts: 'Direction Education et Accueil Périscolaire',
        createdBy: 'Robert De Nitro',
        code: 'OT-738614',
        historiqueDesEtats: [
            {
                code: 'awaiting_realisation',
                label: 'En cours',
                createdBy: 'Robert De Nitro',
                createdAt: '2023-01-03T16:28:34.926Z',
            },
            {
                code: 'awaiting_realisation',
                label: 'Attente fournitures',
                createdBy: 'Robert De Nitro',
                createdAt: '2023-01-02T16:28:34.926Z',
            },
            {
                code: 'awaiting_realisation',
                label: 'Validé - Pris en compte',
                createdBy: 'Megusta',
                createdAt: '2023-01-02T15:22:34.926Z',
            },
            {
                code: 'awaiting_realisation',
                label: 'En préparation',
                createdBy: 'Robert De Nitro',
                createdAt: '2023-01-01T09:10:34.926Z',
            },
        ],
    })
}
