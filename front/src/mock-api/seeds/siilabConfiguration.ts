import { AppServer } from '@/mock-api/types'

export function useSiilabConfigurationSeed(server: AppServer) {
    server.create('siilabConfiguration', {
        id: '1',
        mapCenterLatitude: 46.584545,
        mapCenterLongitude: 0.338348,
    })
}
