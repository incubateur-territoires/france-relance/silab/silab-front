import { faker } from '@faker-js/faker/locale/fr'
// Note: si vous voulez des données répétables, ajoutez une variable VITE_FAKER_SEED avec un nombre dans votre .env.local
faker.seed(
    import.meta.env.VITE_FAKER_SEED !== '' ||
        import.meta.env.VITE_FAKER_SEED === undefined
        ? Number(import.meta.env.VITE_FAKER_SEED)
        : undefined
)

import { createServer, Serializer } from 'miragejs'
import { SerializerInterface } from 'miragejs/serializer'
import { useBackendEndpoints } from './endpoints'
import { factories } from './factories'
import { fixtures } from './fixtures'
import { mirageFakerImagesWorkaround } from './helpers'
import { models } from './models'
import { seeds } from './seeds'
import { AppServer } from './types'

export function makeServer({ environment = 'development' } = {}) {
    const silabSerializer: SerializerInterface = Serializer.extend({
        root: false,
        embed: true,
    })

    const server: AppServer = createServer({
        serializers: {
            application: silabSerializer,
            carlConfigurationIntervention: silabSerializer.extend?.({
                include: ['carlClient'],
            }),
            interventionServiceOffer: silabSerializer.extend?.({
                include: ['availableEquipements', 'gmaoConfiguration'],
            }),
            activiteAction: silabSerializer.extend?.({
                include: ['equipementCible', 'intervenant', 'operation'],
            }),
            gmaoActeur: silabSerializer.extend?.({
                include: ['operationsHabilitees'],
            }),
            activiteDiagnostique: silabSerializer.extend?.({
                include: ['equipementCible', 'intervenant'],
            }),
            compteur: silabSerializer.extend?.({
                include: ['equipement'],
            }),
            demandeIntervention: silabSerializer.extend?.({
                include: ['relatedEquipement', 'createdBy'],
            }),
            intervention: silabSerializer.extend?.({
                include: [
                    'relatedEquipement',
                    'intervenantsAffectes',
                    'createdBy',
                ],
            }),
        },
        environment,
        models,
        fixtures,
        factories,
        seeds,

        routes() {
            this.urlPrefix = import.meta.env.VITE_BACKEND_URL
            this.timing = Number(import.meta.env.VITE_MOCK_RESPONSE_DELAY_MS)
            useBackendEndpoints.call(this)

            mirageFakerImagesWorkaround.call(this)
        },
    })

    /** Azure AD Authentication-related urls */
    server.passthrough(
        // authentification
        'https://login.microsoftonline.com/**',
        'https://graph.microsoft.com/**',
        // serveurs d'images/fichiers
        'https://loremflickr.com/**',
        'https://picsum.photos/**',
        `https://localhost:${import.meta.env.VITE_MOCK_FILES_SERVER_PORT}/**`,
        // reverse geocoding
        'https://wxs.ign.fr/**'
    )
    return server
}
