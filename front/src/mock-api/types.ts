import { Instantiate } from 'miragejs/-types'
import type { Collection, Registry, Server } from 'miragejs'
import type Schema from 'miragejs/orm/schema'

import type { factories } from './factories'
import type { models } from './models'

export type AppRegistry = Registry<typeof models, typeof factories>
export type AppSchema = Schema<AppRegistry>
export type AppServer = Server<AppRegistry>

export type AfterCreate<K extends keyof AppRegistry> = (
    entity: Instantiate<AppRegistry, K>,
    server: AppServer
) => void

export type PolymorphicCollection<K extends keyof AppRegistry> = Collection<
    Instantiate<AppRegistry, K>
>
