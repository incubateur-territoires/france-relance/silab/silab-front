export const emitLoadingStarted = () => {
    const event = new CustomEvent('loadingStarted')
    document.dispatchEvent(event)
}
export const emitLoadingEnded = () => {
    const event = new CustomEvent('loadingEnded')
    document.dispatchEvent(event)
}
