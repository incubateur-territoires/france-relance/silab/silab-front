/**
 * source : https://github.com/AzureAD/microsoft-authentication-library-for-js/tree/7654f44cae72e6f7b476fcd7e14b273e661d9008/samples/msal-browser-samples/vue3-sample-app
 */
import { Ref, ref, watch } from 'vue'
import { useMsal } from './useMsal'

export function useIsAuthenticated(): Ref<boolean> {
    const msal = useMsal()
    const isAuthenticated = ref(msal.accounts.value.length > 0)

    watch(
        () => msal.accounts,
        () => {
            isAuthenticated.value = msal.accounts.value.length > 0
        }
    )

    return isAuthenticated
}
