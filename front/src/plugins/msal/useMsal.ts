/**
 * source : https://github.com/AzureAD/microsoft-authentication-library-for-js/tree/7654f44cae72e6f7b476fcd7e14b273e661d9008/samples/msal-browser-samples/vue3-sample-app
 */
import {
    AccountInfo,
    InteractionStatus,
    PublicClientApplication,
} from '@azure/msal-browser'
import { getCurrentInstance, Ref, toRefs } from 'vue'

export type MsalContext = {
    instance: PublicClientApplication
    accounts: Ref<AccountInfo[]>
    inProgress: Ref<InteractionStatus>
}

export function useMsal(): MsalContext {
    const internalInstance = getCurrentInstance()
    if (!internalInstance) {
        throw new Error(
            'useMsal() cannot be called outside the setup() function of a component'
        )
    }
    const { instance, accounts, inProgress } = toRefs(
        internalInstance.appContext.config.globalProperties.$msal
    )

    if (!instance.value || !accounts.value || !inProgress.value) {
        throw new Error('Please install the msalPlugin')
    }

    if (inProgress.value === InteractionStatus.Startup) {
        instance.value.initialize().then(() => {
            instance.value.handleRedirectPromise().catch(() => {
                // Errors should be handled by listening to the LOGIN_FAILURE event
                return
            })
        })
    }

    return {
        instance: instance.value,
        accounts,
        inProgress,
    }
}
