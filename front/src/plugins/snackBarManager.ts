import { SilabColor } from './vuetify'

export const emitSnackBar = (
    message: string,
    color?: keyof typeof SilabColor
) => {
    const event = new CustomEvent('snackBarToDisplay', {
        detail: {
            message,
            color,
        },
    })
    document.dispatchEvent(event)
}
