// Styles
import '@fontsource/montserrat'
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Vuetify
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import * as labsComponents from 'vuetify/labs/components'
/**
 * Les couleurs doivents être forcément définies ici avant d'être utilisées
 */
export enum SilabColor {
    'background',
    'surface',
    'primary',
    'secondary',
    'error',
    'warning',
    'disabled',
    'success',
    'info',
    'secondary-info',
}

export const silabColors: { [key in keyof typeof SilabColor]: string } = {
    background: '#f9fbfd',
    surface: '#f9fbfd',
    primary: '#0873bb',
    secondary: '#fbae3a',
    error: '#cc3300',
    warning: '#ffcc00',
    disabled: '#dfe1e4',
    success: '#339900',
    info: '#5BC0DE',
    'secondary-info': '#616161',
}

export default createVuetify({
    components: {
        ...components,
        ...labsComponents,
    },
    directives,
    theme: {
        defaultTheme: 'silab',
        themes: {
            silab: {
                dark: false,
                colors: silabColors,
            },
        },
    },
    defaults: {
        VBtn: {
            color: 'primary',
            rounded: 'pill',
        },
        VSwitch: {
            inset: true,
        },
        VAutocomplete: {
            variant: 'outlined',
        },
        VTextField: {
            variant: 'outlined',
        },
    },
})
// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
