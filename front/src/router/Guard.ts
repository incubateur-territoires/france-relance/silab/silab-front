// source : https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/7654f44cae72e6f7b476fcd7e14b273e661d9008/samples/msal-browser-samples/vue3-sample-app/src/router/Guard.ts
import { RouteLocationNormalized, Router } from 'vue-router'
import { msalInstance, loginRequest } from '@/security/authConfig'
import {
    InteractionType,
    PopupRequest,
    PublicClientApplication,
    RedirectRequest,
} from '@azure/msal-browser'

export function registerGuard(router: Router) {
    router.beforeEach(async (to: RouteLocationNormalized) => {
        if (to.meta.requiresAuth) {
            const request = {
                ...loginRequest,
                redirectStartPage: to.fullPath,
            }
            const shouldProceed = await isAuthenticated(
                msalInstance,
                InteractionType.Redirect,
                request
            )
            return shouldProceed || '/droits-insuffisants'
        }

        return true
    })
}

export async function isAuthenticated(
    instance: PublicClientApplication,
    interactionType: InteractionType,
    loginRequest: PopupRequest | RedirectRequest
): Promise<boolean> {
    await msalInstance.initialize()
    // If your application uses redirects for interaction, handleRedirectPromise must be called and awaited on each page load before determining if a user is signed in or not
    return instance
        .handleRedirectPromise()
        .then(() => {
            const accounts = instance.getAllAccounts()
            if (accounts.length > 0) {
                return true
            }

            // on précise avec state l'url post redirection
            loginRequest.state = window.location.href // cf. https://learn.microsoft.com/fr-fr/azure/active-directory/develop/msal-js-pass-custom-state-authentication-request

            // User is not signed in and attempting to access protected route. Sign them in.
            if (interactionType === InteractionType.Popup) {
                return instance
                    .loginPopup(loginRequest)
                    .then(() => {
                        return true
                    })
                    .catch(() => {
                        return false
                    })
            } else if (interactionType === InteractionType.Redirect) {
                return instance
                    .loginRedirect(loginRequest)
                    .then(() => {
                        return true
                    })
                    .catch(() => {
                        return false
                    })
            }

            return false
        })
        .catch(() => {
            return false
        })
}
