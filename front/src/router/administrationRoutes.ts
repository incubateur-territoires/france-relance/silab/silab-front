import { ServiceOfferNamespaces } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { RouteRecordRaw } from 'vue-router'

export const globalAdministrationRoutes: RouteRecordRaw[] = [
    {
        path: '/admin',
        name: 'admin',
        component: () => import('@/shared/administration/views/RootView.vue'),
        meta: {
            title: 'Administration',
            requiresAuth: true,
        },
        children: [
            {
                path: 'general',
                children: [
                    {
                        path: '',
                        name: 'admin/general',
                        component: () =>
                            import(
                                '@/shared/administration/views/siilabConfiguration/SiilabConfigurationView.vue'
                            ),
                        meta: {
                            title: 'Général',
                            requiresAuth: true,
                            icon: 'mdi-application-cog-outline',
                        },
                    },
                ],
            },
            {
                path: 'utilisateurs',
                children: [
                    {
                        path: '',
                        name: 'admin/utilisateurs',
                        component: () =>
                            import(
                                '@/shared/administration/views/users/UserListView.vue'
                            ),
                        meta: {
                            title: 'Utilisateurs',
                            requiresAuth: true,
                            icon: 'mdi-account-multiple',
                        },
                    },
                    {
                        path: 'creer',
                        name: 'admin/utilisateurs/creer',
                        component: () =>
                            import(
                                '@/shared/administration/views/users/UserCreateView.vue'
                            ),
                        meta: {
                            title: 'Administration - Ajouter un utilisateur',
                            requiresAuth: true,
                        },
                    },
                    {
                        path: ':id',
                        children: [
                            {
                                path: 'modifier',
                                name: 'admin/utilisateurs/modifier',
                                component: () =>
                                    import(
                                        '@/shared/administration/views/users/UserEditView.vue'
                                    ),
                                meta: {
                                    title: 'Administration - Modifier un utilisateur',
                                    requiresAuth: true,
                                },
                            },
                        ],
                    },
                ],
            },
            {
                path: 'messages',
                children: [
                    {
                        path: '',
                        name: 'admin/message',
                        component: () =>
                            import(
                                '@/shared/administration/views/messages/MessageListView.vue'
                            ),
                        meta: {
                            title: 'Messages',
                            requiresAuth: true,
                            icon: 'mdi-message-draw',
                        },
                    },
                    {
                        path: 'creer',
                        name: 'admin/message/créer',
                        component: () =>
                            import(
                                '@/shared/administration/views/messages/MessageCreateView.vue'
                            ),
                        meta: {
                            title: 'Administration - Ajouter un message',
                            requiresAuth: true,
                        },
                    },
                    {
                        path: ':id',
                        children: [
                            {
                                path: 'modifier',
                                name: 'admin/message/modifier',
                                component: () =>
                                    import(
                                        '@/shared/administration/views/messages/MessageEditView.vue'
                                    ),
                                meta: {
                                    title: 'Administration - Modifier un message',
                                    requiresAuth: true,
                                },
                            },
                        ],
                    },
                ],
            },
            {
                path: 'ods-intervention',
                children: [
                    {
                        path: '',
                        name: 'admin/ods-intervention',
                        component: () =>
                            import(
                                '@/shared/administration/views/interventionServiceOffer/InterventionServiceOfferListView.vue'
                            ),
                        meta: {
                            title: 'Offre de service intervention',
                            requiresAuth: true,
                            icon: 'mdi-account-hard-hat',
                        },
                    },
                    {
                        path: 'creer',
                        name: 'admin/ods-intervention/créer',
                        component: () =>
                            import(
                                '@/shared/administration/views/interventionServiceOffer/InterventionServiceOfferCreateView.vue'
                            ),
                        meta: {
                            title: 'Administration - Ajouter une offre de service intervention',
                            requiresAuth: true,
                        },
                    },
                    {
                        path: ':id',
                        children: [
                            {
                                path: 'modifier',
                                name: 'admin/ods-intervention/modifier',
                                component: () =>
                                    import(
                                        '@/shared/administration/views/interventionServiceOffer/InterventionServiceOfferEditView.vue'
                                    ),
                                meta: {
                                    title: 'Administration - Modifier une offre de service intervention',
                                    requiresAuth: true,
                                },
                            },
                        ],
                    },
                ],
            },
            {
                path: 'configuration-carl-intervention',
                children: [
                    {
                        path: '',
                        name: 'admin/configuration-carl-intervention',
                        component: () =>
                            import(
                                '@/shared/administration/views/interventionServiceOffer/gmaoConfigurationIntervention/CarlConfigurationInterventionListView.vue'
                            ),
                        meta: {
                            title: "Configuration CARL d'intervention",
                            requiresAuth: true,
                            icon: 'mdi-cog',
                        },
                    },
                    {
                        path: 'creer',
                        name: 'admin/configuration-carl-intervention/créer',
                        component: () =>
                            import(
                                '@/shared/administration/views/interventionServiceOffer/gmaoConfigurationIntervention/CarlConfigurationInterventionCreateView.vue'
                            ),
                        meta: {
                            title: 'Administration - Ajouter une configuration CARL',
                            requiresAuth: true,
                        },
                    },
                    {
                        path: ':id',
                        children: [
                            {
                                path: 'modifier',
                                name: 'admin/configuration-carl-intervention/modifier',
                                component: () =>
                                    import(
                                        '@/shared/administration/views/interventionServiceOffer/gmaoConfigurationIntervention/CarlConfigurationInterventionEditView.vue'
                                    ),
                                meta: {
                                    title: 'Administration - Modifier une configuration CARL',
                                    requiresAuth: true,
                                },
                            },
                        ],
                    },
                ],
            },
        ],
    },
]

export function serviceOfferAdministrationRoutes(
    serviceOfferType: keyof typeof ServiceOfferNamespaces
): RouteRecordRaw[] {
    return [
        {
            path: 'utilisateurs',
            meta: {
                title: 'Utilisateurs',
                icon: 'mdi-account-multiple',
                indexViewName: `${serviceOfferType}/administration/utilisateurs/liste`,
            },
            children: [
                {
                    path: '',
                    name: `${serviceOfferType}/administration/utilisateurs/liste`,
                    component: () =>
                        import(
                            '@/serviceOffer/views/administration/UserListView.vue'
                        ),
                    meta: {
                        title: 'Utilisateurs',
                        requiresAuth: true,
                    },
                },
                {
                    path: 'creer',
                    name: `${serviceOfferType}/administration/utilisateurs/créer`,
                    component: () =>
                        import(
                            '@/serviceOffer/views/administration/UserCreateView.vue'
                        ),
                    meta: {
                        title: 'Administration - Ajouter un utilisateur',
                        requiresAuth: true,
                    },
                },
            ],
        },
    ]
}
