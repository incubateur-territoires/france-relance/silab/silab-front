import { registerGuard } from '@/router/Guard'
import { globalAdministrationRoutes } from '@/router/administrationRoutes'
import { serviceOfferRoutes } from '@/router/serviceOfferRoutes'
import { getAnalyticsProvider } from '@/shared/analytics/AnalyticsProvider'
import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('@/views/HomeView.vue'),
            meta: {
                title: 'Mes applications',
                requiresAuth: true,
            },
        },
        {
            path: '/mentions-legales',
            name: 'legalnotice',
            component: () => import('@/views/LegalNoticeView.vue'),
            meta: {
                title: 'Mentions légales',
            },
        },
        {
            path: '/droits-insuffisants',
            name: 'forbidden',
            component: () => import('@/views/errors/ForbiddenView.vue'),
            meta: {
                title: 'Page interdite',
            },
        },
        {
            path: '/:catchAll(.*)',
            name: 'notfound',
            component: () => import('@/views/errors/NotFoundView.vue'),
            meta: {
                title: 'Page introuvable',
            },
        },
        ...serviceOfferRoutes,
        ...globalAdministrationRoutes,
    ],
})

router.beforeEach((to, from, next) => {
    document.title = `Siilab - ${to.meta.title}`
    getAnalyticsProvider().trackPageView(
        (to.meta.title as string) ?? '',
        from.path,
        to.path
    )

    next()
})

if (import.meta.env.VITE_DISABLE_AUTHENTICATION_GUARDS !== 'true') {
    registerGuard(router)
}

export default router
