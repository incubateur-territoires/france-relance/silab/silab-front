import { elusServiceOfferRoutes } from '@/elusServiceOffer/router/elusServiceOfferRoutes'
import { RouteRecordRaw } from 'vue-router'
import { interventionServiceOfferRoutes } from './serviceOfferRoutes/interventionServiceOfferRoutes'
import { logisticServiceOfferRoutes } from './serviceOfferRoutes/logisticServiceOfferRoutes'

export const serviceOfferRoutes: RouteRecordRaw[] = [
    {
        path: '/app',
        name: 'offre-de-service',
        component: () => import('@/shared/views/ServiceOfferRootView.vue'),
        children: [
            ...interventionServiceOfferRoutes,
            ...logisticServiceOfferRoutes,
            ...elusServiceOfferRoutes,
        ],
    },
]
