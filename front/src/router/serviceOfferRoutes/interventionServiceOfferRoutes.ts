import { ServiceOfferNamespaces } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { RouteRecordRaw } from 'vue-router'
import { serviceOfferAdministrationRoutes } from '../administrationRoutes'

export const interventionServiceOfferRoutes: RouteRecordRaw[] = [
    {
        path: `${ServiceOfferNamespaces.InterventionServiceOffer}/:serviceOfferId`,
        component: () =>
            import(
                '@/interventionServiceOffer/views/InterventionServiceOfferRootView.vue'
            ),
        children: [
            {
                path: 'admin',
                component: () =>
                    import('@/serviceOffer/views/administration/RootView.vue'),
                meta: {
                    node: 'InterventionServiceOffer/admin-root-route',
                },
                children: [
                    ...serviceOfferAdministrationRoutes(
                        'InterventionServiceOffer' //Amelioration : se servir partout des `keyof typeof ServiceOfferNamespaces` ou similaire pour les names des routes ?
                    ),
                ],
            },
            {
                path: 'map',
                children: [
                    {
                        path: '',
                        name: 'intervention-service-offer/index',
                        component: () =>
                            import(
                                '@/interventionServiceOffer/views/global/AccueilListAndMapView.vue'
                            ),
                        meta: {
                            title: 'Liste et carte',
                            requiresAuth: true,
                        },
                    },
                ],
            },
            {
                path: 'tableaux-de-mesures',
                name: 'intervention-service-offer/tableaux-de-mesures',
                component: () =>
                    import(
                        '@/interventionServiceOffer/views/tableauxDeMesures/TableauxDeMesuresView.vue'
                    ),
                meta: {
                    title: 'Tableaux de relèves',
                    requiresAuth: true,
                },
            },
        ],
    },
]
