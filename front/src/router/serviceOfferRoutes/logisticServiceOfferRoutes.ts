import { ServiceOfferNamespaces } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { RouteRecordRaw } from 'vue-router'
import { serviceOfferAdministrationRoutes } from '../administrationRoutes'

export const logisticServiceOfferRoutes: RouteRecordRaw[] = [
    {
        path: `${ServiceOfferNamespaces.LogisticServiceOffer}/:serviceOfferId`,
        component: () =>
            import(
                '@/logisticServiceOffer/views/LogisticServiceOfferRootView.vue'
            ),
        children: [
            {
                path: '',
                name: 'logistic-service-offer/index',
                component: () =>
                    import(
                        '@/logisticServiceOffer/views/intervention/ListeInterventionsView.vue'
                    ),
                meta: {
                    title: 'Liste des interventions',
                    requiresAuth: true,
                },
            },
            {
                path: 'inventaire',
                children: [
                    {
                        path: '',
                        name: 'logistic-service-offer/inventaire/démarrer',
                        component: () =>
                            import(
                                '@/logisticServiceOffer/views/article/InventoryStartView.vue'
                            ),
                        meta: {
                            title: 'Démarrer Inventaire',
                            requiresAuth: true,
                        },
                    },
                    {
                        path: ':codeArticle',
                        name: 'logistic-service-offer/inventaire/inventorier',
                        component: () =>
                            import(
                                '@/logisticServiceOffer/views/article/ArticleInventoryView.vue'
                            ),
                        meta: {
                            title: 'Inventaire article',
                            requiresAuth: true,
                        },
                    },
                ],
            },
            {
                path: 'interventions',
                children: [
                    {
                        path: ':interventionId',
                        children: [
                            {
                                path: ``,
                                name: 'logistic-service-offer/interventions/détails',
                                component: () =>
                                    import(
                                        '@/logisticServiceOffer/views/intervention/DetailInterventionView.vue'
                                    ),
                                meta: {
                                    title: "Détails d'une intervention",
                                    requiresAuth: true,
                                },
                            },
                        ],
                    },
                ],
            },
            {
                path: 'admin',
                component: () =>
                    import('@/shared/administration/views/RootView.vue'),
                meta: {
                    node: 'LogisticServiceOffer/admin-root-route',
                },
                children: [
                    ...serviceOfferAdministrationRoutes('LogisticServiceOffer'),
                    {
                        path: 'type-inventaire',
                        name: 'logistic-service-offer/admin/type-inventaire',
                        component: () =>
                            import(
                                '@/logisticServiceOffer/views/logisticServiceOffer/InventoryTypeView.vue'
                            ),
                        meta: {
                            title: "Type d'inventaire",
                            requiresAuth: true,
                            icon: 'mdi-clipboard-list-outline',
                        },
                    },
                ],
            },
        ],
    },
]
