import { useRouter } from 'vue-router'

export function useRoute(routeName: string) {
    return useRouter()
        .getRoutes()
        .find(({ name }) => name === routeName)
}
