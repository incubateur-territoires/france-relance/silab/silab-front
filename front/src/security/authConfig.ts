/* Plus d'information :
https://github.com/AzureAD/microsoft-authentication-library-for-js/tree/7654f44cae72e6f7b476fcd7e14b273e661d9008/samples/msal-browser-samples/vue3-sample-app */
import {
    PublicClientApplication,
    Configuration,
    RedirectRequest,
} from '@azure/msal-browser'
export const msalConfig: Configuration = {
    auth: {
        clientId: import.meta.env.VITE_AZURE_CLIENT_ID,
        authority: `https://login.microsoftonline.com/${
            import.meta.env.VITE_AZURE_TENANT_ID
        }`,
        redirectUri: window.location.origin, // cf. https://learn.microsoft.com/en-us/azure/active-directory/develop/scenario-spa-sign-in?tabs=javascript2
    },
    cache: {
        cacheLocation: 'localStorage',
    },
}

export const msalInstance: PublicClientApplication =
    new PublicClientApplication(msalConfig)

// Add here scopes for id token to be used at MS Identity Platform endpoints.
export const loginRequest: RedirectRequest = {
    scopes: ['profile', 'openid', 'email'],
}

// Add here the endpoints for MS Graph API services you would like to use.
export const graphConfig = {
    graphMeEndpoint: 'https://graph.microsoft.com/v1.0',
}
