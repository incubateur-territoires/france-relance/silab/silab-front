import { ref, watch } from 'vue'
import { ServiceOfferApi } from '@/serviceOffer/entities/serviceOffer/ServiceOfferApi'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { useConfigurationStore } from '@/shared/administration/stores/ConfigurationStore'
import { MsalContext } from '@/plugins/msal/useMsal'

export function useServiceOffers() {
    const globalRoles = useConfigurationStore().globalRoles
    const allAvailableRoles = ref<
        Array<
            ServiceOffer['availableRoles'][0] & {
                serviceOfferTitle: string
                serviceOfferTemplate: string
            }
        >
    >([])

    const allAvailableRolesMap = ref<
        Map<
            ServiceOffer['availableRoles'][0]['value'],
            Omit<ServiceOffer['availableRoles'][0], 'value'> & {
                serviceOfferTitle: string
                serviceOfferTemplate: string
            }
        >
    >(new Map())

    const serviceOffers = ref<ServiceOffer[]>([])

    async function loadServiceOffers(msal: MsalContext): Promise<void> {
        const serviceOfferApi = new ServiceOfferApi(msal)
        serviceOffers.value = await serviceOfferApi.getAll()
    }

    watch(serviceOffers, () => {
        allAvailableRoles.value = globalRoles.map((globalRole) => {
            return {
                ...globalRole,
                serviceOfferTitle: 'Silab',
                serviceOfferTemplate: 'Admin',
            }
        })

        for (const serviceOffer of serviceOffers.value) {
            allAvailableRoles.value = allAvailableRoles.value.concat(
                serviceOffer.availableRoles.map((role) => {
                    return {
                        value: role.value,
                        libelle: role.libelle,
                        description: role.description,
                        serviceOfferTitle: serviceOffer.title,
                        serviceOfferTemplate: serviceOffer.template,
                    }
                })
            )
        }

        // partir du allAvailableRoles pour créer la allAvailableRolesMap correspondante
        allAvailableRolesMap.value = new Map()
        allAvailableRoles.value.forEach((role) => {
            allAvailableRolesMap.value.set(role.value, {
                libelle: role.libelle,
                description: role.description,
                serviceOfferTitle: role.serviceOfferTitle,
                serviceOfferTemplate: role.serviceOfferTemplate,
            })
        })
    })

    return {
        serviceOffers,
        allAvailableRoles,
        allAvailableRolesMap,
        loadServiceOffers,
    }
}
