import { ServiceOfferDto } from '@/serviceOffer/entities/serviceOffer/ServiceOfferDto'
import {
    ListableItem,
    SecondaryAction,
    getItemThumbnailUrl,
} from '@/shared/item/entities/ListableItem'
import { SilabColor } from '@/plugins/vuetify'

export class ServiceOffer implements ListableItem {
    id: string | number
    notes?: string
    title: string
    description?: string
    image: string
    link: string
    availableRoles: ServiceOfferDto['availableRoles']
    template: ServiceOfferDto['template']
    environnement: ServiceOfferDto['environnement']
    private itemSecondaryAction?: SecondaryAction

    protected static environmentColors: {
        [key in string]: keyof typeof SilabColor
    } = {
        TEST: 'warning',
        INTEG: 'info',
        PROD: 'disabled',
        default: 'background',
    }

    public constructor(serviceOfferDto: ServiceOfferDto) {
        this.id = serviceOfferDto.id
        this.notes = serviceOfferDto.notes
        this.title = serviceOfferDto.title
        this.description = serviceOfferDto.description
        this.image = serviceOfferDto.image
        this.link = serviceOfferDto.link
        this.availableRoles = serviceOfferDto.availableRoles
        this.template = serviceOfferDto.template
        this.environnement = serviceOfferDto.environnement
    }

    public getEnvironnementColor() {
        if (['INTEG', 'TEST', 'PROD'].includes(this.environnement)) {
            return ServiceOffer.environmentColors[this.environnement]
        }

        return ServiceOffer.environmentColors.default
    }

    public getEnvironnementIcon() {
        switch (this.environnement) {
            case 'INTEG':
                return 'mdi-code-braces'
            case 'TEST':
                return 'mdi-flask'
            case 'PROD':
                return 'disabled'
            default:
                return 'mdi-server'
        }
    }

    public getRootPageIcon() {
        switch (this.template) {
            case 'InterventionServiceOffer':
                return 'mdi-map-outline'
            case 'LogisticServiceOffer':
                return 'mdi-invoice-list-outline'
            case 'ElusServiceOffer':
                return 'mdi-map-outline'
        }
    }

    public getItemId() {
        return this.id.toString()
    }
    public getItemTitle() {
        return this.title
    }

    public getItemDescription(): string {
        return this.description ?? ''
    }

    public getItemImageUrl() {
        try {
            return new URL(this.image)
        } catch {
            return undefined
        }
    }

    public getItemThumbnailUrl = getItemThumbnailUrl

    public getItemChips() {
        return [
            {
                label: this.environnement,
                color: this.getEnvironnementColor(),
            },
        ]
    }
    public isRequiringAttention() {
        return false
    }

    getItemTemporalData() {
        return ''
    }
    getItemRoute() {
        return {
            name: this.link,
            params: {
                serviceOfferId: this.id,
            },
        }
    }
    setItemRoute() {
        return this
    }
    public getItemSecondaryAction() {
        return this.itemSecondaryAction
    }

    public setItemSecondaryAction(secondaryAction: SecondaryAction) {
        this.itemSecondaryAction = secondaryAction
        return this
    }

    public static fromArray(
        serviceOfferDtos: ServiceOfferDto[]
    ): ServiceOffer[] {
        return serviceOfferDtos.map(
            (serviceOfferDto) => new ServiceOffer(serviceOfferDto)
        )
    }
}

export enum ServiceOfferNamespaces {
    InterventionServiceOffer = 'interventions',
    LogisticServiceOffer = 'stock',
    ElusServiceOffer = 'elus',
}
