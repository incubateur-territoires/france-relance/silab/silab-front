import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { ServiceOfferDtoSchema } from '@/serviceOffer/entities/serviceOffer/ServiceOfferDto'
import { EntityApi } from '@/shared/api/entities/EntityApi'

export class ServiceOfferApi
    extends AzureTokenAccessApi
    implements EntityApi<ServiceOffer>
{
    async getAll(): Promise<ServiceOffer[]> {
        const rawResponse = await this.getBackendData('service-offers')

        const serviceOffersDto = ServiceOfferDtoSchema.array().parse(
            await rawResponse.json()
        )

        return serviceOffersDto.map((dto) => {
            return new ServiceOffer(dto)
        })
    }

    async get(
        id: ServiceOffer['id'],
        abortSignal?: AbortSignal
    ): Promise<ServiceOffer> {
        const rawResponse = await this.getBackendData(
            `service-offers/${id}`,
            undefined,
            abortSignal
        )

        const serviceOfferDto = ServiceOfferDtoSchema.parse(
            await rawResponse.json()
        )

        return new ServiceOffer(serviceOfferDto)
    }
}
