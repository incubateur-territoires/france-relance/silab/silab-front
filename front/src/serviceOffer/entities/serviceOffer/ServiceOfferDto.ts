import { z } from 'zod'

export const ServiceOfferDtoSchema = z.object({
    id: z.number().or(z.string()),
    notes: z.string().optional(),
    title: z.string(),
    description: z.string().optional(),
    image: z.string(),
    link: z.string(),
    availableRoles: z.array(
        z.object({
            value: z.string(),
            libelle: z.string(),
            description: z.string(),
        })
    ),
    template: z.enum([
        'InterventionServiceOffer',
        'LogisticServiceOffer',
        'ElusServiceOffer',
    ]),
    environnement: z.enum(['INTEG', 'TEST', 'PROD']),
})

export type ServiceOfferDto = z.infer<typeof ServiceOfferDtoSchema>
