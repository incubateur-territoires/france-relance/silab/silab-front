import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { ServiceOfferDto, ServiceOfferDtoSchema } from './ServiceOfferDto'
import { ServiceOffer } from './ServiceOffer'
import { Dto } from '@/shared/api/entities/Dtos'
import { ZodError } from 'zod'

interface ServiceOfferMapper {
    toDto: (partialServiceOffer: Partial<ServiceOffer>) => ServiceOfferDto
    toDtoArray: (
        partialServiceOffer: Partial<ServiceOffer>[]
    ) => ServiceOfferDto[]
    fromDtoArray: (
        serviceOfferDtos: Dto<ServiceOffer>[] | Dto<ServiceOffer[]>
    ) => ServiceOffer[]
}

export const serviceOfferMapper: ServiceOfferMapper = {
    toDto(partialServiceOffer: Partial<ServiceOffer>): ServiceOfferDto {
        try {
            return ServiceOfferDtoSchema.parse({
                id: partialServiceOffer.id,
                notes: partialServiceOffer.notes,
                title: partialServiceOffer.title,
                description: partialServiceOffer.description,
                image: partialServiceOffer.image,
                link: partialServiceOffer.link,
                availableRoles: partialServiceOffer.availableRoles,
                template: partialServiceOffer.template,
                environnement: partialServiceOffer.environnement,
            })
        } catch (error) {
            if (error instanceof ZodError) {
                throw new IncompleteEntity(
                    "L'offre de service fournit ne possède pas tout les attributs requis.",
                    error
                )
            }
            throw error
        }
    },

    toDtoArray(
        partialServiceOffers: Partial<ServiceOffer>[]
    ): ServiceOfferDto[] {
        return partialServiceOffers.map((partialServiceOffer) =>
            this.toDto(partialServiceOffer)
        )
    },

    fromDtoArray: function (
        serviceOfferDtos: Dto<ServiceOffer>[] | Dto<ServiceOffer[]>
    ): ServiceOffer[] {
        return ServiceOffer.fromArray(serviceOfferDtos as ServiceOfferDto[]) // amelioration : peut-on faire + propre ?
    },
}
