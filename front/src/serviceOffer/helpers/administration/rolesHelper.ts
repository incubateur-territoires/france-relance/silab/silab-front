import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'

const scopedRoleRegex =
    /^SERVICEOFFER_(?<serviceOfferId>[^_]+)_ROLE_(?<serviceOfferType>[^_]+)_(?<profileOrActionOnEntity>.+)$/

export function scopeRoleWithGivenServiceOfferId(
    role: string,
    serviceOfferId: ServiceOffer['id']
) {
    return role.replace(
        scopedRoleRegex,
        (
            _match,
            _serviceOfferIdPlaceholder,
            serviceOfferType,
            profileOrActionOnEntity
        ) => {
            return `SERVICEOFFER_${serviceOfferId}_ROLE_${serviceOfferType}_${profileOrActionOnEntity}`
        }
    )
}
