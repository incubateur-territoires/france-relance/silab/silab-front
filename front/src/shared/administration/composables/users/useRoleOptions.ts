import { ref } from 'vue'
import { ServiceOfferApi } from '@/serviceOffer/entities/serviceOffer/ServiceOfferApi'
import { MsalContext } from '@/plugins/msal/useMsal'

export function useRoleOptions() {
    const availableRoles = ref<
        {
            value: string
            libelle: string
            description: string
            serviceOfferTitle: string
            serviceOfferTemplate: string
        }[]
    >([])

    async function loadRoleOptions(msal: MsalContext): Promise<void> {
        // Note: pour l'instant, la back ne renvoie pas les rôles non scopés, il faudra l'ajouter dans la conf silab
        availableRoles.value = [
            {
                value: 'ROLE_GOD',
                libelle: 'Administrateur global',
                description: "Possède tous les droits sur l'application",
                serviceOfferTitle: 'Silab',
                serviceOfferTemplate: 'Admin',
            },
        ]
        const serviceOfferApi = new ServiceOfferApi(msal)
        const serviceOffers = await serviceOfferApi.getAll()

        for (const serviceOffer of serviceOffers) {
            availableRoles.value = availableRoles.value.concat(
                serviceOffer.availableRoles.map((role) => {
                    return {
                        value: role.value,
                        libelle: role.libelle,
                        description: role.description,
                        serviceOfferTitle: serviceOffer.title,
                        serviceOfferTemplate: serviceOffer.template,
                    }
                })
            )
        }
    }

    return { availableRoles, loadRoleOptions }
}
