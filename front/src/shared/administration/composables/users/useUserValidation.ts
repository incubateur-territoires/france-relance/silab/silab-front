export const useUserValidation = () => {
    const rules = {
        hasEmail: (input: string | undefined) => {
            return !!input || 'Vous devez renseigner un email'
        },
        hasValidEmail: (input: string | undefined) => {
            // from https://emailregex.com/index.html
            const validEmailRegex =
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

            return validEmailRegex.test(input || '') || 'Adresse email invalide'
        },
        hasRole: (input: string[] | undefined) => {
            return (
                (!!input && input.length > 0) ||
                'Vous devez choisir au moins un rôle'
            )
        },
    }

    return { rules }
}
