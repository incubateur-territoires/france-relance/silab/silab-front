import { CreateDto } from '@/shared/api/entities/Dtos'
import { Message } from './Message'

export interface CreateMessageDto extends CreateDto<Message> {
    title: string
    body?: string
    color: 'info' | 'success' | 'error' | 'warning'
    startDate?: string
    endDate?: string
}
