import { EditDto } from '@/shared/api/entities/Dtos'
import { Message } from './Message'

export interface EditMessageDto extends EditDto<Message> {
    title: string
    body?: string
    color: 'info' | 'success' | 'error' | 'warning'
    startDate?: string
    endDate?: string
}
