import { MessageDto } from './MessageDto'

export class Message {
    id: number
    title: string
    body?: string
    color: 'info' | 'success' | 'error' | 'warning'
    startDate?: Date
    /**
     * La date de fin est inclusive
     */
    endDate?: Date

    constructor(messageDto: MessageDto) {
        this.id = messageDto.id
        this.title = messageDto.title
        this.body = messageDto.body
        this.color = messageDto.color
        this.startDate = messageDto.startDate
            ? new Date(messageDto.startDate)
            : undefined
        this.endDate = messageDto.endDate
            ? new Date(messageDto.endDate)
            : undefined
    }
}
