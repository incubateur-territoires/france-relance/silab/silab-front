import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { Message } from './Message'
import { MessageDto } from './MessageDto'
import { CreateMessageDto } from './CreateMessageDto'
import { EditMessageDto } from './EditMessageDto'
import { EntityApi } from '@/shared/api/entities/EntityApi'

export class MessageApi
    extends AzureTokenAccessApi
    implements EntityApi<Message>
{
    async getAll(abortSignal?: AbortSignal): Promise<Message[]> {
        const rawResponse = await this.getBackendData(
            `messages`,
            new Map(),
            abortSignal
        )

        const messagesDto = (await rawResponse.json()) as MessageDto[]

        return messagesDto.map((messageDto) => {
            return new Message(messageDto)
        })
    }

    async get(messageId: Message['id']) {
        const rawResponse = await this.getBackendData(`messages/${messageId}`)

        const messageDto = (await rawResponse.json()) as MessageDto

        return new Message(messageDto)
    }

    async create(newMessage: CreateMessageDto) {
        const rawResponse = await this.postBackendData(
            `messages`,
            JSON.stringify(newMessage)
        )

        const messageDto = (await rawResponse.json()) as MessageDto

        return new Message(messageDto)
    }

    async update(messageId: Message['id'], updatedMessage: EditMessageDto) {
        const rawResponse = await this.putBackendData(
            `messages/${messageId}`,
            JSON.stringify(updatedMessage)
        )

        const messageDto = (await rawResponse.json()) as MessageDto

        return new Message(messageDto)
    }

    async delete(messageId: Message['id']) {
        await this.deleteBackendData(`messages/${messageId}`)
    }
}
