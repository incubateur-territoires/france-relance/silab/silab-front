import { Dto } from '@/shared/api/entities/Dtos'
import { CreateMessageDto } from './CreateMessageDto'
import { Message } from './Message'

export interface MessageDto extends CreateMessageDto, Dto<Message> {
    id: number
    title: string
    body?: string
    color: 'info' | 'success' | 'error' | 'warning'
    startDate?: string
    endDate?: string
}
