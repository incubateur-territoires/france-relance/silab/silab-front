import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { Message } from './Message'
import { CreateMessageDto } from './CreateMessageDto'
import { EditMessageDto } from './EditMessageDto'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'

interface MessageMapper
    extends Omit<EntityMapper<Message>, 'toDto' | 'toDtoArray'> {
    toEditDto: (partialEntity: Partial<Message>) => EditMessageDto
    toCreateDto: (partialEntity: Partial<Message>) => CreateMessageDto
}

export const messageMapper: MessageMapper = {
    toCreateDto: function (messagePartial: Partial<Message>): CreateMessageDto {
        if (
            messagePartial.title === undefined ||
            messagePartial.color === undefined
        ) {
            throw new IncompleteEntity(
                'Le message ne possède pas tout les attributs requis.'
            )
        }

        return {
            title: messagePartial.title,
            body: messagePartial.body,
            color: messagePartial.color,
            startDate: messagePartial.startDate?.toISOString(),
            endDate: messagePartial.endDate?.toISOString(),
        }
    },
    toEditDto: function (messagePartial: Partial<Message>): EditMessageDto {
        if (
            messagePartial.title === undefined ||
            messagePartial.color === undefined
        ) {
            throw new IncompleteEntity(
                'Le message ne possède pas tout les attributs requis.'
            )
        }

        return {
            title: messagePartial.title,
            body: messagePartial.body,
            color: messagePartial.color,
            startDate: messagePartial.startDate?.toISOString(),
            endDate: messagePartial.endDate?.toISOString(),
        }
    },
}
