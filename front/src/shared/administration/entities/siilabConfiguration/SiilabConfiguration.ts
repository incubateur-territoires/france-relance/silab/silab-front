import { SiilabConfigurationDto } from './SiilabConfigurationDto'
import { siilabConfigurationMapper } from './SiilabConfigurationMapper'

export class SiilabConfiguration implements SiilabConfigurationDto {
    mapCenterLatitude: number
    mapCenterLongitude: number

    public constructor(siilabConfigurationDto: SiilabConfigurationDto) {
        this.mapCenterLatitude = siilabConfigurationDto.mapCenterLatitude
        this.mapCenterLongitude = siilabConfigurationDto.mapCenterLongitude
    }

    public clone(): SiilabConfiguration {
        return new SiilabConfiguration(siilabConfigurationMapper.toDto(this))
    }
}
