import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { SiilabConfiguration } from './SiilabConfiguration'
import {
    SiilabConfigurationDto,
    SiilabConfigurationDtoSchema,
} from './SiilabConfigurationDto'
import { EntityApi } from '@/shared/api/entities/EntityApi'

export class SiilabConfigurationApi
    extends AzureTokenAccessApi
    implements Omit<EntityApi<SiilabConfiguration>, 'update'>
{
    async get() {
        const rawResponse = await this.getBackendData('siilab-configuration')

        const siilabConfigurationDto = SiilabConfigurationDtoSchema.parse(
            await rawResponse.json()
        )

        return new SiilabConfiguration(siilabConfigurationDto)
    }

    async update(updatedSiilabConfiguration: SiilabConfigurationDto) {
        const rawResponse = await this.putBackendData(
            'siilab-configuration',
            JSON.stringify(updatedSiilabConfiguration)
        )

        const siilabConfigurationDto = SiilabConfigurationDtoSchema.parse(
            await rawResponse.json()
        )

        return new SiilabConfiguration(siilabConfigurationDto)
    }
}
