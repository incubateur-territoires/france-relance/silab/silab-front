import { z } from 'zod'

export const SiilabConfigurationDtoSchema = z.object({
    mapCenterLatitude: z.number(),
    mapCenterLongitude: z.number(),
})

export type SiilabConfigurationDto = z.infer<
    typeof SiilabConfigurationDtoSchema
>
