import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import {
    SiilabConfigurationDto,
    SiilabConfigurationDtoSchema,
} from './SiilabConfigurationDto'
import { Dto } from '@/shared/api/entities/Dtos'
import { isUndefined } from '@/shared/helpers/types'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { SiilabConfiguration } from './SiilabConfiguration'

interface SiilabConfigurationMapper extends EntityMapper<SiilabConfiguration> {
    toDto: (
        partialSiilabConfiguration: Partial<SiilabConfiguration>
    ) => SiilabConfigurationDto
    fromDto: (
        siilabConfigurationDto: Dto<SiilabConfiguration>
    ) => SiilabConfiguration
}

export const siilabConfigurationMapper: SiilabConfigurationMapper = {
    toDto(
        partialSiilabConfiguration: Partial<SiilabConfiguration>
    ): SiilabConfigurationDto {
        if (
            isUndefined(partialSiilabConfiguration.mapCenterLatitude) ||
            isUndefined(partialSiilabConfiguration.mapCenterLongitude)
        ) {
            throw new IncompleteEntity(
                'La configuration ne possède pas tout les attributs requis.'
            )
        }

        return {
            mapCenterLatitude: partialSiilabConfiguration.mapCenterLatitude,
            mapCenterLongitude: partialSiilabConfiguration.mapCenterLongitude,
        }
    },
    fromDto(
        siilabConfigurationDto: Dto<SiilabConfiguration>
    ): SiilabConfiguration {
        return new SiilabConfiguration(
            SiilabConfigurationDtoSchema.parse(siilabConfigurationDto)
        )
    },
}
