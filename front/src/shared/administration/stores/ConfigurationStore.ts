import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useConfigurationStore = defineStore('configuration', () => {
    // Note: pour l'instant, la back ne renvoie pas les rôles non scopés, il faudra l'ajouter dans la conf silab
    const globalRoles = ref<ServiceOffer['availableRoles']>([
        {
            value: 'ROLE_GOD',
            libelle: 'Administrateur global',
            description: "Possède tous les droits sur l'application",
        },
    ])

    const templateNames: {
        [key in ServiceOffer['template']]: string
    } = {
        InterventionServiceOffer: 'Intervention',
        LogisticServiceOffer: 'Stock',
        ElusServiceOffer: 'Décisionnel',
    }

    return {
        globalRoles,
        templateNames,
    }
})
