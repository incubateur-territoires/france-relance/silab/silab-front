import { defineStore } from 'pinia'
import { useMsal } from '@/plugins/msal/useMsal'
import { SiilabConfigurationApi } from '../entities/siilabConfiguration/SiilabConfigurationApi'
import { ref } from 'vue'
import { SiilabConfiguration } from '../entities/siilabConfiguration/SiilabConfiguration'
import { SilabError } from '@/shared/errors/SilabError'
import { emitSnackBar } from '@/plugins/snackBarManager'
import { siilabConfigurationMapper } from '../entities/siilabConfiguration/SiilabConfigurationMapper'
import { isUndefined } from '@/shared/helpers/types'

export const useSiilabConfigurationStore = defineStore(
    'siilabConfiguration',
    () => {
        const msal = useMsal()
        const siilabConfigurationApi = new SiilabConfigurationApi(msal)

        const siilabConfiguration = ref<SiilabConfiguration | undefined>(
            undefined
        )

        fetchSiilabConfiguration()

        async function fetchSiilabConfiguration() {
            try {
                siilabConfiguration.value = await siilabConfigurationApi.get()
            } catch (error) {
                throw new SilabError(
                    `Erreur lors de la récupération de la configuration`,
                    error
                )
            }
        }

        async function persist() {
            if (isUndefined(siilabConfiguration.value)) {
                return
            }
            try {
                const siilabConfigurationDto = siilabConfigurationMapper.toDto(
                    siilabConfiguration.value
                )
                siilabConfiguration.value = await siilabConfigurationApi.update(
                    siilabConfigurationDto
                )
                emitSnackBar(`Configuration modifiée`, 'primary')
            } catch (error) {
                throw new SilabError(
                    `Erreur lors de la mise à jour de la configuration`,
                    error
                )
            }
        }

        return { siilabConfiguration, persist }
    }
)
