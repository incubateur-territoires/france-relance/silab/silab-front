import { matomoAnalyticsProvider } from './providers/MatomoAnalyticsProvider'

export interface AnalyticsProvider {
    trackPageView(pageTitle: string, fromPath: string, toPath: string): void
    sendEvent(
        eventCategory: string,
        eventAction: string,
        eventName?: string,
        eventValue?: number
    ): void
    setUserId(userId: string): void
}

export function getAnalyticsProvider(): AnalyticsProvider {
    switch (import.meta.env.VITE_ANALYTICS_PROVIDER) {
        case 'matomo':
            return matomoAnalyticsProvider
        case 'debug':
            return {
                trackPageView(
                    pageTitle: string,
                    fromPath: string,
                    toPath: string
                ) {
                    // eslint-disable-next-line no-console
                    console.log('analytics provider : trackPageView', {
                        pageTitle,
                        fromPath,
                        toPath,
                    })
                    return
                },
                sendEvent(
                    eventCategory: string,
                    eventAction: string,
                    eventName?: string,
                    eventValue?: number
                ) {
                    // eslint-disable-next-line no-console
                    console.log('analytics provider : sendEvent', {
                        eventCategory,
                        eventAction,
                        eventName,
                        eventValue,
                    })
                    return
                },
                setUserId(userId: string) {
                    // eslint-disable-next-line no-console
                    console.log('analytics provider : setUserId', { userId })
                    return
                },
            }
        default:
            return {
                trackPageView(
                    _pageTitle: string,

                    _fromPath: string,

                    _toPath: string
                ) {
                    return
                },
                sendEvent(
                    _eventCategory: string,

                    _eventAction: string,

                    _eventName?: string,

                    _eventValue?: number
                ) {
                    return
                },

                setUserId(_userId: string) {
                    return
                },
            }
    }
}
