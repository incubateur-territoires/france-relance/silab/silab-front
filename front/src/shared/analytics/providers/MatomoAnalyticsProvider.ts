import { SilabError } from '../../errors/SilabError'
import { AnalyticsProvider } from '../AnalyticsProvider'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
declare const _paq: any
const throwExceptionIfMatomoIsUnavailable = function () {
    if (typeof _paq === 'undefined') {
        throw new SilabError(
            "Erreur technique : Matomo n'est pas disponible, avez-vous inclus le script de suivi ? (VITE_ANALYTICS_SCRIPT_TAG). Merci de transmettre aux techniciens silab."
        )
    }
}
export const matomoAnalyticsProvider: AnalyticsProvider = {
    trackPageView(pageTitle: string, fromPath: string, toPath: string): void {
        throwExceptionIfMatomoIsUnavailable()
        _paq.push(['setReferrerUrl', fromPath])
        _paq.push(['setDocumentTitle', pageTitle])
        _paq.push(['setCustomUrl', toPath])
        _paq.push(['trackPageView'])
    },
    sendEvent: function (
        eventCategory: string,
        eventAction: string,
        eventName?: string | undefined,
        eventValue?: number
    ): void {
        throwExceptionIfMatomoIsUnavailable()
        const eventParams: Array<string | number> = [
            'trackEvent',
            eventCategory,
            eventAction,
        ]

        if (eventName !== undefined) {
            eventParams.push(eventName)
            if (eventValue !== undefined) {
                eventParams.push(eventValue)
            }
        }
        _paq.push(eventParams)
    },
    setUserId: function (userId: string): void {
        throwExceptionIfMatomoIsUnavailable()
        _paq.push(['setUserId', userId])
    },
}
