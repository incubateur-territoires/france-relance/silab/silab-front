import { useMsal } from '@/plugins/msal/useMsal'
import { serviceOfferIdInjectionKey } from '@/shared/constants'
import { isDefined, isUndefined, requiredInject } from '@/shared/helpers/types'
import { useUserStore } from '@/shared/user/stores/UserStore'

import { Ref, WritableComputedRef, computed, ref, unref, watch } from 'vue'
import { Dto } from '../entities/Dtos'
import LocalStorageCache from '@/shared/swrv/cache/adapters/localStorage'
import { IKey, fetcherFn, IResponse, IConfig } from '@/shared/swrv/types'
import useSWRV from '@/shared/swrv/use-swrv'
import { RequestAborter } from '../entities/RequestAborter'
import { ZodError } from 'zod'
import { SecuredData } from '@/shared/helpers/lifeCycle'

/**
 *
 * @param key ATTENTION: swrv n'utilise que le paramètre `key` il est alors important pour nous de différencier avec l'endpoint,
 * surtout lorsques les paramètres sont les même (ex, get equipement et getChildren). Il est conseillé d'ajouter le nom de l'endpoint en dernier paramètre (il sera ignoré).
 * https://github.com/Kong/swrv/issues/239
 * ATTENTION: Les objets passés dans la key doivent avoir une méthode toString() pour la construire
 * Amélioration: Forcer les objets à avoir cette méthode
 *
 */
export function useServiceOfferSWR<Data = unknown, Error = unknown>(
    key: IKey,
    fn: fetcherFn<Data>,
    toDtoMapper: (data: Data) => Dto<Data>,
    fromDtoMapper: (dataDto: unknown) => Data, // Note on type unknown pour forcer les méthodes à gérer des données avec des structures obsolètes
    droitNecessaire?: string,
    config?: IConfig,
    apiFetchAborter?: RequestAborter
): Omit<IResponse<Data, Error>, 'data' | 'mutate' | 'setCache'> & {
    securedData: Ref<SecuredData<Data> | undefined>
} {
    const msal = useMsal()
    const userStore = useUserStore(msal)
    const serviceOfferId = requiredInject(serviceOfferIdInjectionKey)
    let swrvResponse: IResponse<Dto<Data>, Error> | undefined = undefined

    swrvResponse = useSWRV(
        key,
        async (...key) => {
            /**
             * Problème: La valeur de isValidating passe à false quand on envoi une quête et abort la précédente
             * Solution : lors d'un abort d'une requête, il y a deux requête en même temps. La nouvelle requête fait passer isValidating à true.
             * Juste après, notre abort va kill l'ancienne requête et isValidating va passer à false.
             * Cette fonction sert donc à faire repasser isValidating à true car il y a toujours la première requête en cours
             * Amélioration : Voir comment faire autrement
             */
            apiFetchAborter?.getSignal().addEventListener('abort', () => {
                if (isDefined(swrvResponse)) {
                    const unwatch = watch(isValidating, () => {
                        if (isValidating.value === false) {
                            isValidating.value = true
                            unwatch()
                        }
                    })
                }
            })

            apiFetchAborter?.abortRequestsAndRefreshAborter()

            return toDtoMapper(await fn(...unref(key)))
        },
        {
            cache: new LocalStorageCache('swrv'),
            revalidateDebounce: 500,
            revalidateOnFocus: false,
            ...config,
        }
    ) as ReturnType<typeof useSWRV<Dto<Data>, Error>>

    const {
        data: dataDto,
        error,
        isValidating,
        mutate: mutateDto,
    } = swrvResponse

    watch(error, () => {
        const unRefError = unref(error)
        if (unRefError instanceof Error) {
            if (unRefError.name !== 'AbortError') {
                console.error(unRefError)
            }
        }
    })

    watch(dataDto, () => {
        try {
            data.value = isDefined(dataDto.value)
                ? fromDtoMapper(dataDto.value)
                : undefined
        } catch (error) {
            // Note: on laisse passer les Dto incomplet car cela veut normalement dire que la structure de donnée à évolué
            // et que les données issues du cache sont incompatible, il faut simplement attendre l'appel réel
            if (!(error instanceof ZodError)) {
                throw error
            }
            console.warn(
                'Les données récupérées depuis le cache ne sont pas compatibles (cela peut être dû à une montée de version de Siilab).'
            )
        }
    })

    const data = ref<Data | undefined>()

    const securedData: WritableComputedRef<SecuredData<Data> | undefined> =
        computed({
            get() {
                if (isUndefined(data.value)) {
                    return undefined
                }

                if (
                    isDefined(droitNecessaire) &&
                    !userStore.currentUserHasRoleForServiceOffer(
                        droitNecessaire,
                        serviceOfferId.value
                    )
                ) {
                    return {
                        accessGranted: false,
                        data: undefined,
                    }
                }
                return {
                    accessGranted: true,
                    data: data.value,
                }
            },
            set(inboundSecuredData) {
                if (
                    isUndefined(inboundSecuredData) ||
                    !inboundSecuredData.accessGranted
                ) {
                    return
                }
                // on insère une fois nos données directement puis on revalide le cache avec un appel réel
                mutateDto(toDtoMapper(inboundSecuredData.data))
                mutateDto()
            },
        })

    watch(
        [
            () => msal.accounts,
            () => userStore.currentUser,
            () => userStore.isImpersonating,
        ],
        () => {
            mutateDto()
        }
    )

    mutateDto()

    return { securedData, error, isValidating }
}
