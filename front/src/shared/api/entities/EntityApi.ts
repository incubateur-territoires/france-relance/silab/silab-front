import { CreateDto, EditDto } from '@/shared/api/entities/Dtos'
export interface EntityApi<EntityType> {
    get?(entityId: string | number): Promise<EntityType>
    getAll?(abortSignal?: AbortSignal): Promise<Array<EntityType>>
    create?(createDto: CreateDto<EntityType>): Promise<EntityType>
    update?(
        entityId: string | number,
        editDto: EditDto<EntityType>
    ): Promise<EntityType>
    delete?(entityId: string | number): void
}
