import { Dto, CreateDto, EditDto } from './Dtos'

export interface EntityMapper<Entity> {
    toDto?: (partialEntity: Partial<Entity>) => Dto<Entity>
    toDtoArray?: (partialEntities: Partial<Entity>[]) => Dto<Entity>[]
    toCreateDto?: (partialEntity: Partial<Entity>) => CreateDto<Entity>
    toEditDto?: (partialEntity: Partial<Entity>) => EditDto<Entity>
    fromDto?: (entityDto: Dto<Entity>) => Entity
    fromUnsafeDto?: (entityDto: unknown) => Entity
    fromDtoArray?: (entityDto: Dto<Entity>[] | Dto<Entity[]>) => Entity[]
    /**
     * Cette méthode est particulièrement utile pour les Dto récupérés depuis du cache,
     * et dont la structure à peut-être évolué (donc incompatible)
     *
     * @throws {ZodError} quand l'entityDto n'est pas compatible
     */
    fromUnsafeDtoArray?: (entityDto: unknown) => Entity[]
}
