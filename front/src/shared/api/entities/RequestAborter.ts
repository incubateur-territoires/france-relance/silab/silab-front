export class RequestAborter {
    private abortcontroler: AbortController

    constructor() {
        this.abortcontroler = new AbortController()
    }

    abortRequestsAndRefreshAborter() {
        this.abortRequests()
        this.abortcontroler = new AbortController()
    }

    abortRequests() {
        this.abortcontroler.abort()
    }

    getSignal() {
        return this.abortcontroler.signal
    }
}
