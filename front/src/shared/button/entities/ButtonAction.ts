import { RouteLocationRaw } from 'vue-router'

export interface ButtonAction {
    label: string
    shortLabel?: string
    icon?: string
    route: RouteLocationRaw
    cypressSelector?: string
}
