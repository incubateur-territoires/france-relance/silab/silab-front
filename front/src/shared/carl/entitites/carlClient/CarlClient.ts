import { CarlClientDto } from './CarlClientDto'
export interface ActionTypeMapObject {
    id: string
    label: string
}
export class CarlClient {
    id: number | string
    title: string
    availableActionTypes: { [key: string]: string }
    '@id': string
    woViewUrlSubPath?: string
    mrViewUrlSubPath?: string
    boxViewUrlSubPath?: string
    materialViewUrlSubPath?: string

    constructor(carlClientDto: CarlClientDto) {
        this.id = carlClientDto.id
        this.title = carlClientDto.title
        this.availableActionTypes = carlClientDto.availableActionTypes
        this['@id'] = `/api/carl-clients/${carlClientDto.id}`
        this.woViewUrlSubPath = carlClientDto.woViewUrlSubPath
        this.mrViewUrlSubPath = carlClientDto.mrViewUrlSubPath
        this.boxViewUrlSubPath = carlClientDto.boxViewUrlSubPath
        this.materialViewUrlSubPath = carlClientDto.materialViewUrlSubPath
    }
}
