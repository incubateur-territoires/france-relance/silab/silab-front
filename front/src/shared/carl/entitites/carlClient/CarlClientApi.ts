import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { CarlClient } from './CarlClient'
import { CarlClientDto } from './CarlClientDto'
import { EntityApi } from '@/shared/api/entities/EntityApi'
import { CostCenter } from '../configurationCarlObject/CostCenter'
import { CostCenterDto } from '../configurationCarlObject/CostCenterDto'

export class CarlClientApi
    extends AzureTokenAccessApi
    implements EntityApi<CarlClient>
{
    async getAll(abortSignal?: AbortSignal): Promise<CarlClient[]> {
        const rawResponse = await this.getBackendData(
            `carl-clients`,
            new Map(),
            abortSignal
        )

        const carlclientsDto = (await rawResponse.json()) as CarlClientDto[]

        return carlclientsDto.map((carlclientsDto) => {
            return new CarlClient(carlclientsDto)
        })
    }

    async getAllCostCenters(
        carlClientId: string,
        abortSignal?: AbortSignal
    ): Promise<CostCenter[]> {
        const rawResponse = await this.getBackendData(
            `carl-clients/${carlClientId}/cost-centers`,
            new Map(),
            abortSignal
        )

        const costCentersDto = (await rawResponse.json()) as CostCenterDto[]

        return costCentersDto.map((costCentersDto) => {
            return new CostCenter(costCentersDto)
        })
    }
}
