import { Dto } from '@/shared/api/entities/Dtos'
import { CarlClient } from './CarlClient'
import { z } from 'zod'

export const CarlClientDtoSchema = z.object({
    id: z.number().or(z.string()),
    title: z.string(),
    availableActionTypes: z.record(z.string()),
    woViewUrlSubPath: z.string().optional(),
    mrViewUrlSubPath: z.string().optional(),
    boxViewUrlSubPath: z.string().optional(),
    materialViewUrlSubPath: z.string().optional(),
})

export type CarlClientDto = Dto<CarlClient> &
    z.infer<typeof CarlClientDtoSchema>
