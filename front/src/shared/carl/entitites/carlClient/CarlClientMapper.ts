import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { CarlClient } from './CarlClient'
import { CarlClientDto } from './CarlClientDto'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { isUndefined } from '@/shared/helpers/types'

interface CarlClientMapper
    extends Omit<
        EntityMapper<CarlClient>,
        'toDtoArray' | 'toEditDto' | 'toCreateDto'
    > {
    toDto: (partielEntity: Partial<CarlClient>) => CarlClientDto
}

export const carlClientMapper: CarlClientMapper = {
    toDto(partialCarlClient: Partial<CarlClient>): CarlClientDto {
        if (
            isUndefined(partialCarlClient.id) ||
            isUndefined(partialCarlClient.title) ||
            isUndefined(partialCarlClient.availableActionTypes)
        ) {
            throw new IncompleteEntity(
                'Le carl client fournie ne possède pas tous les attributs requis.'
            )
        }

        return {
            id: partialCarlClient.id,
            title: partialCarlClient.title,
            availableActionTypes: partialCarlClient.availableActionTypes,
            woViewUrlSubPath: partialCarlClient.woViewUrlSubPath,
            mrViewUrlSubPath: partialCarlClient.mrViewUrlSubPath,
            boxViewUrlSubPath: partialCarlClient.boxViewUrlSubPath,
            materialViewUrlSubPath: partialCarlClient.materialViewUrlSubPath,
        }
    },
}
