import { CostCenterDto } from './CostCenterDto'

export class CostCenter {
    id: string
    label: string
    type: string
    code: string

    constructor(costCenterDto: CostCenterDto) {
        this.id = costCenterDto.id
        this.label = costCenterDto.label
        this.type = costCenterDto.type
        this.code = costCenterDto.code
    }
}
