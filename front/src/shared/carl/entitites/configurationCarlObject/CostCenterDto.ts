import { Dto } from '@/shared/api/entities/Dtos'
import { CostCenter } from './CostCenter'

export interface CostCenterDto extends Dto<CostCenter> {
    id: string
    label: string
    type: string
    code: string
}
