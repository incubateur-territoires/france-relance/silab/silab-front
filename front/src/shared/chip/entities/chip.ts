export interface Chip {
    label: string
    color?: string
}
