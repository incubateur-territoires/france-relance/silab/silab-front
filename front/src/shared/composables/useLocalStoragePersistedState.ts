import { ref, Ref, watch, watchEffect } from 'vue'
import { z } from 'zod'
import { isDefined, isNull } from '../helpers/types'

export function useLocalStoragePersistedState<
    StateType,
    StateDto extends object,
>(
    generateLocalStorageKey: () => string,
    defaultState: StateType,
    stateDtoSchema: z.ZodType<StateDto>,
    stateMapper: {
        fromDto(dto: StateDto): StateType
        toDto(state: StateType): StateDto
    },
    afterLoad?: (state: Ref<StateType>) => void
): Ref<StateType> {
    const state: Ref<StateType> = ref(defaultState) as Ref<StateType>

    function loadStateFromStorage() {
        const stateDto = stateDtoSchema
            .nullable()
            .parse(
                JSON.parse(
                    window.localStorage.getItem(generateLocalStorageKey()) ??
                        'null'
                )
            )
        state.value = isNull(stateDto)
            ? defaultState
            : stateMapper.fromDto(stateDto)

        if (isDefined(afterLoad)) {
            afterLoad(state)
        }
    }

    function persistStateToStorage() {
        window.localStorage.setItem(
            generateLocalStorageKey(),
            JSON.stringify(stateMapper.toDto(state.value))
        )
    }

    watchEffect(() => {
        loadStateFromStorage()
    })

    watch(
        state,
        () => {
            persistStateToStorage()
        },
        { deep: true }
    )

    return state
}
