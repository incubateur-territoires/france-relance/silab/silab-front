import { onUnmounted, ref } from 'vue'

export function useUnmounted() {
    const isUnmounted = ref(false)
    onUnmounted(() => {
        isUnmounted.value = true
    })
    return isUnmounted
}
