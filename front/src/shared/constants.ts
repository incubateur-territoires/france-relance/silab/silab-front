import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { InjectionKey, Ref } from 'vue'
export const serviceOfferIdInjectionKey = Symbol() as InjectionKey<
    Ref<ServiceOffer['id']>
>
export const serviceOfferInjectionKey = Symbol() as InjectionKey<
    Ref<ServiceOffer | undefined>
>

export const serviceOfferTrackingCategoryInjectionKey =
    Symbol() as InjectionKey<Ref<string>>
