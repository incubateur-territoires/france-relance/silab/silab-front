import { DocumentDto } from './DocumentDto'

export class Document {
    public id: string
    public url: URL
    public name: string

    public static fromArray(documentDtos: DocumentDto[]): Document[] {
        return documentDtos.map((documentDto) => new Document(documentDto))
    }

    public constructor(documentDto: DocumentDto) {
        this.id = documentDto.id
        this.url = new URL(documentDto.url)
        this.name = documentDto.name
    }
}
