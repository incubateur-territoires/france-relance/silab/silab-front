import { Dto } from '@/shared/api/entities/Dtos'
import { Document } from './Document'
import { z } from 'zod'

export const DocumentDtoSchema = z.object({
    id: z.string(),
    url: z.string(),
    name: z.string(),
})

// export type DocumentDto = z.infer<typeof documentDtoSchema>
// amelioration: abandonner les interfaces natives
export interface DocumentDto
    extends Dto<Document>,
        z.infer<typeof DocumentDtoSchema> {
    id: string
    url: string
    name: string
}
