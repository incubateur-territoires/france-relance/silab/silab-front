import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { Dto } from '@/shared/api/entities/Dtos'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { Document } from './Document'
import { DocumentDto } from './DocumentDto'

interface DocumentMapper extends EntityMapper<Document> {
    toDto: (partialDocument: Partial<Document>) => DocumentDto
    toDtoArray: (partialDocuments: Partial<Document>[]) => DocumentDto[]
    fromDto: (documentDto: Dto<Document>) => Document
    fromDtoArray: (
        documentDtos: Dto<Document>[] | Dto<Document[]>
    ) => Document[]
}

export const documentMapper: DocumentMapper = {
    toDto(partialDocument: Partial<Document>): DocumentDto {
        if (
            partialDocument.id === undefined ||
            partialDocument.url === undefined ||
            partialDocument.name === undefined
        ) {
            throw new IncompleteEntity(
                'Le document fournit ne possède pas tout les attributs requis.'
            )
        }

        return {
            id: partialDocument.id,
            url: partialDocument.url.href,
            name: partialDocument.name,
        }
    },

    fromDto: function (documentDto: Dto<Document>): Document {
        return new Document(documentDto as DocumentDto) // amelioration : peut-on faire + propre ?
    },

    toDtoArray(partialDocuments: Partial<Document>[]): DocumentDto[] {
        return partialDocuments.map((partialDocument) =>
            this.toDto(partialDocument)
        )
    },

    fromDtoArray: function (
        documentDtos: Dto<Document>[] | Dto<Document[]>
    ): Document[] {
        return Document.fromArray(documentDtos as DocumentDto[]) // amelioration : peut-on faire + propre ?
    },
}
