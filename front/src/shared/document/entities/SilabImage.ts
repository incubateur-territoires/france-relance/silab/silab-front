import { SilabImageDto } from './SilabImageDto'

export class SilabImage {
    public id: string
    public url: URL

    public static fromArray(silabImageDtos: SilabImageDto[]): SilabImage[] {
        return silabImageDtos.map(
            (silabImageDto) => new SilabImage(silabImageDto)
        )
    }

    public constructor(silabImageDto: SilabImageDto) {
        this.id = silabImageDto.id
        this.url = new URL(silabImageDto.url)
    }
}
