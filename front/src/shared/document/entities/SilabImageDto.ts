import { Dto } from '@/shared/api/entities/Dtos'
import { SilabImage } from './SilabImage'
import { z } from 'zod'

export const SilabImageDtoSchema = z.object({ id: z.string(), url: z.string() })

//export type SilabImageDto = z.infer<typeof SilabImageDtoSchema>
// amelioration: abandonner les interfaces natives
export interface SilabImageDto
    extends Dto<SilabImage>,
        z.infer<typeof SilabImageDtoSchema> {
    id: string
    url: string
}
