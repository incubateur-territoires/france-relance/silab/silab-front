import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { SilabImage } from './SilabImage'
import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { isUndefined } from '@/shared/helpers/types'
import { SilabImageDto } from './SilabImageDto'

// Note: declarer cette interface semble nécessaire pour surcharger correctment les types de retour
// (eg. ne pas avoir des retours uniquement de type Dto<SilabImage>)
interface SilabImageMapper
    extends Omit<EntityMapper<SilabImage>, 'toEditDto' | 'toCreateDto'> {
    toDto: (partialSilabImage: Partial<SilabImage>) => SilabImageDto
    toDtoArray: (partialEntities: Partial<SilabImage>[]) => SilabImageDto[]
}

export const silabImageMapper: SilabImageMapper = {
    toDto(partialSilabImage: Partial<SilabImage>): SilabImageDto {
        if (
            isUndefined(partialSilabImage.id) ||
            isUndefined(partialSilabImage.url)
        ) {
            throw new IncompleteEntity(
                "L'image fournie ne possède pas tous les attributs requis."
            )
        }

        return {
            id: partialSilabImage.id,
            url: partialSilabImage.url.href,
        }
    },
    toDtoArray: function (
        partialSilabImages: Partial<SilabImage>[]
    ): SilabImageDto[] {
        return partialSilabImages.map((partialSilabImage) =>
            this.toDto(partialSilabImage)
        )
    },
}
