import { GedClientDto } from './GedClientDto'

export class GedClient implements GedClientDto {
    id: string
    title: string
    configurationDocumentation: string
    '@id': string

    public constructor(gedClientDto: GedClientDto) {
        this.id = gedClientDto.id
        this.title = gedClientDto.title
        this.configurationDocumentation =
            gedClientDto.configurationDocumentation
        this['@id'] = `/api/ged-clients/${gedClientDto.id}`
    }
}
