import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { GedClient } from './GedClient'
import { GedClientDto } from './GedClientDto'
import { EntityApi } from '@/shared/api/entities/EntityApi'

export class GedClientApi
    extends AzureTokenAccessApi
    implements EntityApi<GedClient>
{
    async getAll(abortSignal?: AbortSignal): Promise<GedClient[]> {
        const rawResponse = await this.getBackendData(
            'ged-clients',
            new Map(),
            abortSignal
        )

        const gedClientDtos = (await rawResponse.json()) as GedClientDto[]

        // On veux instancier les interventions avec leur classes finales/filles, on utilise pour cela le factory sur chaque Dto reçu
        return gedClientDtos.map((gedClientDto) => {
            return new GedClient(gedClientDto)
        })
    }
}
