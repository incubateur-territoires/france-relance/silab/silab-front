export interface GedClientDto {
    id: string
    title: string
    configurationDocumentation: string
}
