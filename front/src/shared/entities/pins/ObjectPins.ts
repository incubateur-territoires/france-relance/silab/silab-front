import {
    ElementWithId,
    IdentityStringProducer,
} from '@/shared/helpers/identifiable'

export class ObjectPins<PinType extends ElementWithId>
    implements IdentityStringProducer
{
    public pinnedIds: Array<PinType['id']>

    constructor(pinnedIds: Array<PinType['id']> = []) {
        this.pinnedIds = pinnedIds
    }

    getIdentityString(): string {
        return this.pinnedIds.join('|')
    }

    addPin(pinId: PinType['id']) {
        if (this.pinnedIds.includes(pinId)) {
            return
        }

        this.pinnedIds.push(pinId)
    }

    removePin(pinId: PinType['id']) {
        this.pinnedIds = this.pinnedIds.filter(
            (currentPinId) => pinId !== currentPinId
        )
    }
}
