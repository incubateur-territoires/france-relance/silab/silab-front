export class IncompleteEntity extends Error {
    constructor(message: string, cause?: unknown) {
        super(message, { cause })
        this.name = 'IncompleteEntity'
    }
}
