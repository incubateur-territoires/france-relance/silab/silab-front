export class SilabError extends Error {
    constructor(message: string, cause?: unknown) {
        super(message, { cause })
        this.name = 'SilabError'
    }
}
