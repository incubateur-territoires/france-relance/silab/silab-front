export class UndefinedObjectError extends Error {
    constructor(message: string, cause?: unknown) {
        super(message, { cause })
        this.name = 'UndefinedObjectError'
    }
}
