export interface FilterChoice {
    label: string
    value: string
}
