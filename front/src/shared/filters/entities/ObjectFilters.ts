import {
    QueryParameters,
    AllowedQueryParameterValue,
    AllowedQueryParameterValueSchema,
} from '@/shared/helpers/backend'
import { ObjectFiltersInterface } from './ObjectFiltersInterface'
import { isArray } from '@/shared/helpers/types'
import {
    ElementWithId,
    IdentityStringProducer,
    allElementsAreIdentical,
} from '@/shared/helpers/identifiable'

export abstract class ObjectFilters<
        FiltersType extends {
            [key: string]: AllowedQueryParameterValue
        },
    >
    implements ObjectFiltersInterface<FiltersType>, IdentityStringProducer
{
    public filters: FiltersType
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public parentFilters: ObjectFilters<any>[]

    public constructor(
        filters?: Partial<FiltersType>,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        parentFilters?: ObjectFilters<any>[]
    ) {
        this.filters = { ...this.getDefaultFilters(), ...filters }
        this.parentFilters = parentFilters ?? []
    }
    getIdentityString(): string {
        return [
            ...this.parentFilters.map((parentFilter) =>
                parentFilter.getIdentityString()
            ),
            ...Object.values(this.filters).map((value) => {
                return String(value)
            }),
        ].join('|')
    }
    public setFilters(filters: FiltersType) {
        this.filters = filters
    }
    public patchFilters(filters: Partial<FiltersType>) {
        this.filters = { ...this.filters, ...filters }
    }
    public abstract getDefaultFilters(): FiltersType

    public getChangedFiltersQuantity(): number {
        const defaultFilters = this.getDefaultFilters()
        let nbDifferences = 0
        for (const [filterKey, filter] of Object.entries(this.filters)) {
            if (isArray(filter)) {
                nbDifferences += allElementsAreIdentical(
                    filter,
                    defaultFilters[filterKey] as string[] | ElementWithId[]
                )
                    ? 0
                    : 1
                continue
            }

            nbDifferences += filter === defaultFilters[filterKey] ? 0 : 1
        }

        return nbDifferences
    }
    public resetFilters(): void {
        this.setFilters(this.getDefaultFilters())
    }
    public hasChangedFilters(): boolean {
        return this.getChangedFiltersQuantity() > 0
    }
    public toQueryParameters(): QueryParameters {
        const queryParameters: QueryParameters = new Map()

        Object.keys(this.filters).forEach((key) => {
            queryParameters.set(
                key,
                AllowedQueryParameterValueSchema.parse(this.filters[key])
            )
        })

        // on ajoute les filtres parents
        return this.parentFilters.reduce(
            (mergedQueryParameters, parentFilter) => {
                return new Map([
                    ...mergedQueryParameters,
                    ...parentFilter.toQueryParameters(),
                ])
            },
            queryParameters
        )
    }
}
