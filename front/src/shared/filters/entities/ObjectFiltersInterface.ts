import {
    QueryParameters,
    AllowedQueryParameterValue,
} from '@/shared/helpers/backend'

export interface ObjectFiltersInterface<
    FiltersType extends {
        [key: string]: AllowedQueryParameterValue
    },
> {
    filters: FiltersType
    setFilters(filters: FiltersType): void
    getDefaultFilters(): FiltersType
    toQueryParameters():
        | Map<keyof FiltersType, AllowedQueryParameterValue>
        | QueryParameters
    getChangedFiltersQuantity(): number
    hasChangedFilters(): boolean
    resetFilters(): void
}
