import { Ref, onUnmounted, ref, watch } from 'vue'
import { VolatileRef } from '@/shared/helpers/lifeCycle'
import { AddressApi } from '../entities/address/AddressApi'
import { SilabError } from '@/shared/errors/SilabError'
import { LatLngLiteral } from 'leaflet'
import { RequestAborter } from '@/shared/api/entities/RequestAborter'
import { isUndefined } from '@/shared/helpers/types'
import { MsalContext } from '@/plugins/msal/useMsal'

export function useApproximateAddress(
    coordinates: Ref<LatLngLiteral | undefined>,
    msal: MsalContext
) {
    const approximateAddress: VolatileRef<string | undefined> = ref({
        complete: true,
        data: undefined,
    })
    const addressApiFetchAborter: RequestAborter = new RequestAborter()
    watch(
        coordinates,
        () => {
            if (isUndefined(coordinates.value)) {
                approximateAddress.value = {
                    complete: true,
                    data: undefined,
                }
                return
            }

            approximateAddress.value = {
                complete: false,
                data: undefined,
            }

            const addressApi = new AddressApi(msal)
            addressApiFetchAborter.abortRequestsAndRefreshAborter()
            addressApi
                .getReverse(
                    coordinates.value.lat,
                    coordinates.value.lng,
                    addressApiFetchAborter.getSignal()
                )
                .then((fetchedApproximateAddress) => {
                    approximateAddress.value = {
                        data:
                            fetchedApproximateAddress.address === ''
                                ? undefined
                                : fetchedApproximateAddress.address,
                        complete: true,
                    }
                })
                .catch((error) => {
                    // dans le cas d'une annulation, on ne fait rien
                    if (error.name !== 'AbortError') {
                        throw new SilabError(
                            `Erreur lors de la récupération de l'adresse approximative`,
                            error
                        )
                    }
                })
                .finally(() => {
                    approximateAddress.value.complete = true
                })
        },
        { immediate: true }
    )
    onUnmounted(() => {
        addressApiFetchAborter.abortRequests()
    })
    return { approximateAddress }
}
