import { AddressDto } from './AddressDto'

export class Address {
    address: string

    public constructor(addressDto: AddressDto) {
        this.address = addressDto.address
    }
}
