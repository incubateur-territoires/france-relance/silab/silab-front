import { AzureTokenAccessApi } from '@/api/AzureTokenAccessApi'
import { Address } from './Address'
import { AddressDto } from './AddressDto'

export class AddressApi extends AzureTokenAccessApi {
    async getReverse(
        lat: number,
        lng: number,
        abortSignal?: AbortSignal
    ): Promise<Address> {
        const rawResponse = await this.getBackendData(
            'geocoding/reverse',
            new Map([
                ['lat', lat.toString()],
                ['lng', lng.toString()],
            ]),
            abortSignal
        )
        const AddressDto = (await rawResponse.json()) as AddressDto
        return new Address(AddressDto)
    }
}
