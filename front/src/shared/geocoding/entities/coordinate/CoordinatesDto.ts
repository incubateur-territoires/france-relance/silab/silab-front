import { z } from 'zod'

export const CoordinatesDtoSchema = z.object({
    lat: z.number(),
    lng: z.number(),
})

export type CoordinatesDto = z.infer<typeof CoordinatesDtoSchema>
