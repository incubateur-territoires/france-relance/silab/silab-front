import { ButtonAction } from '../button/entities/ButtonAction'
import { GmaoActeurDto } from './GmaoActeurDto'
import { GmaoActeurStatus } from './status/GmaoActeurStatus'
import { Operation } from '@/interventionServiceOffer/entities/activite/Operation'

export class GmaoActeur {
    id: string
    nomComplet: string
    telephones: string[]
    emails: string[]
    currentStatus: GmaoActeurStatus
    operationsHabilitees?: Operation[]

    public constructor(gmaoActeurDto: GmaoActeurDto) {
        this.id = gmaoActeurDto.id
        this.nomComplet = gmaoActeurDto.nomComplet
        this.telephones = gmaoActeurDto.telephones
        this.emails = gmaoActeurDto.emails
        this.currentStatus = new GmaoActeurStatus(gmaoActeurDto.currentStatus)
        this.operationsHabilitees = gmaoActeurDto.operationsHabilitees?.map(
            (operationDto) => new Operation(operationDto)
        )
    }

    public static fromArray(gmaoActeurDtos: GmaoActeurDto[]): GmaoActeur[] {
        return gmaoActeurDtos.map(
            (gmaoActeurDto) => new GmaoActeur(gmaoActeurDto)
        )
    }

    public getTelephonesButtonActions(): ButtonAction[] {
        return this.telephones.map((telephone) => {
            return {
                label: telephone,
                route: `tel:${telephone}`,
                icon: 'mdi-phone-outline',
            }
        })
    }

    public getOpérationsHabiliteesTriéesParOrdreAlphabétique(): Operation[] {
        return this.operationsHabilitees
            ? this.operationsHabilitees.sort((a, b) =>
                  a.label.localeCompare(b.label)
              )
            : []
    }
}
