import { Dto } from '@/shared/api/entities/Dtos'
import { GmaoActeur } from './GmaoActeur'
import { z } from 'zod'
import {
    GmaoActeurStatusDto,
    GmaoActeurStatusDtoSchema,
} from './status/GmaoActeurStatusDto'

import {
    OperationDto,
    OperationDtoSchema,
} from '@/interventionServiceOffer/entities/activite/OperationDto'

export const GmaoActeurDtoSchema = z.object({
    id: z.string(),
    nomComplet: z.string(),
    telephones: z.array(z.string()),
    emails: z.array(z.string()),
    currentStatus: GmaoActeurStatusDtoSchema,
    operationsHabilitees: OperationDtoSchema.array().optional(),
})

// export type GmaoActeurDto = z.infer<typeof GmaoActeurDtoSchema>
// amelioration: abandonner les interfaces natives
export interface GmaoActeurDto
    extends Dto<GmaoActeur>,
        z.infer<typeof GmaoActeurDtoSchema> {
    id: string
    nomComplet: string
    telephones: string[]
    emails: string[]
    currentStatus: GmaoActeurStatusDto
    operationsHabilitees?: OperationDto[]
}
