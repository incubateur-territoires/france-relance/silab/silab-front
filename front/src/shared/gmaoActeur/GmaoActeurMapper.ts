import { EntityMapper } from '@/shared/api/entities/EntityMapper'
import { GmaoActeur } from './GmaoActeur'
import { GmaoActeurDto } from './GmaoActeurDto'
import { Dto } from '@/shared/api/entities/Dtos'
import { IncompleteEntity } from '@/shared/errors/IncompleteEntity'
import { operationMapper } from '@/interventionServiceOffer/entities/activite/OperationMapper'
import { isDefined } from '../helpers/types'

interface GmaoActeurMapper extends EntityMapper<GmaoActeur> {
    toDto: (partialGmaoActeur: Partial<GmaoActeur>) => GmaoActeurDto
    toDtoArray: (partialGmaoActeurs: Partial<GmaoActeur>[]) => GmaoActeurDto[]
    fromDto: (gmaoActeurDto: Dto<GmaoActeur>) => GmaoActeur
    fromDtoArray: (
        gmaoActeurDtos: Dto<GmaoActeur>[] | Dto<GmaoActeur[]>
    ) => GmaoActeur[]
}

export const gmaoActeurMapper: GmaoActeurMapper = {
    toDto(partialGmaoActeur: Partial<GmaoActeur>): GmaoActeurDto {
        if (
            partialGmaoActeur.id === undefined ||
            partialGmaoActeur.nomComplet === undefined ||
            partialGmaoActeur.telephones === undefined ||
            partialGmaoActeur.emails === undefined ||
            partialGmaoActeur.currentStatus === undefined
        ) {
            throw new IncompleteEntity(
                "L'acteur GMAO fournit ne possède pas tout les attributs requis."
            )
        }

        return {
            id: partialGmaoActeur.id,
            nomComplet: partialGmaoActeur.nomComplet,
            telephones: partialGmaoActeur.telephones,
            emails: partialGmaoActeur.emails,
            currentStatus: partialGmaoActeur.currentStatus.toDto(),
            operationsHabilitees: isDefined(
                partialGmaoActeur.operationsHabilitees
            )
                ? operationMapper.toDtoArray(
                      partialGmaoActeur.operationsHabilitees
                  )
                : undefined,
        }
    },

    fromDto: function (gmaoActeurDto: Dto<GmaoActeur>): GmaoActeur {
        return new GmaoActeur(gmaoActeurDto as GmaoActeurDto) // amelioration : peut-on faire + propre ?
    },

    toDtoArray(partialEquipements: Partial<GmaoActeur>[]): GmaoActeurDto[] {
        return partialEquipements.map((partialEquipement) =>
            this.toDto(partialEquipement)
        )
    },

    fromDtoArray: function (
        gmaoActeurDtos: Dto<GmaoActeur>[] | Dto<GmaoActeur[]>
    ): GmaoActeur[] {
        return GmaoActeur.fromArray(gmaoActeurDtos as GmaoActeurDto[]) // amelioration : peut-on faire + propre ?
    },
}
