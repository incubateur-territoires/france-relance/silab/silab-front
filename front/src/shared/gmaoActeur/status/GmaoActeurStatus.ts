import { ObjectStatus } from '@/shared/objectStatus/entities/ObjectStatus'
import { GmaoActeurStatusDto } from './GmaoActeurStatusDto'

export class GmaoActeurStatus extends ObjectStatus<
    GmaoActeurStatusDto['code']
> {
    public constructor(gmaoActeurStatusDto: GmaoActeurStatusDto) {
        super(gmaoActeurStatusDto)
    }
}
