import { ObjectStatusDto } from '@/shared/objectStatus/entities/ObjectStatusDto'
import { z } from 'zod'

export const GmaoActeurtatusAllowedCodesDtoSchema = z.enum([
    'active',
    'inactive',
])

export const GmaoActeurStatusDtoSchema = z.object({
    code: GmaoActeurtatusAllowedCodesDtoSchema,
    label: z.string(),
    createdBy: z.string(),
    createdAt: z.string(),
})

export interface GmaoActeurStatusDto
    extends ObjectStatusDto<
            z.infer<typeof GmaoActeurtatusAllowedCodesDtoSchema>
        >,
        z.infer<typeof GmaoActeurStatusDtoSchema> {}
