import { ElementWithId } from './identifiable'

/**
 * Garde la première occurence rencontrée de chaque élément
 */
export function removeDuplicateElementsWithId<T extends ElementWithId>(
    array: T[]
): T[] {
    const encounteredIds = new Set<ElementWithId['id']>()
    const distinctElements: T[] = []

    for (const element of array) {
        const id = element.id
        if (!encounteredIds.has(id)) {
            encounteredIds.add(id)
            distinctElements.push(element)
        }
    }

    return distinctElements
}

export function arraysHaveSameValues(a: string[], b: string[]): boolean {
    const aCopy = a
    const bCopy = b
    return aCopy.sort().join(',') === bCopy.sort().join(',')
}

export function removeIdentifiableElementFromArray<
    T extends { id: string | number },
>(array: T[], elementToRemove: T) {
    array.splice(
        array.findIndex((element) => element.id === elementToRemove.id),
        1
    )
}

export function arraysHaveSameSortedIdentifiableElements(
    arrayA: ElementWithId[],
    arrayB: ElementWithId[]
) {
    if (arrayA.length !== arrayB.length) {
        return false
    }

    return arrayA.every(
        (_identifiableElement, identifiableElementIndex) =>
            arrayA[identifiableElementIndex].id ===
            arrayB[identifiableElementIndex].id
    )
}

export function replaceIdentifiableElementFromArray<
    T extends { id: string | number },
>(array: T[], replacingElement: T) {
    array[array.findIndex((element) => element.id === replacingElement.id)] =
        replacingElement
}
