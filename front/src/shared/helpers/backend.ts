import { SilabError } from '@/shared/errors/SilabError'
import { emitLoadingEnded, emitLoadingStarted } from '@/plugins/loaderManager'
// eslint-disable-next-line vue/prefer-import-from-vue
import { isString } from '@vue/shared'
import {
    isArrayOf,
    isBoolean,
    isDate,
    isElementWithId,
    isNumber,
    isUndefined,
} from './types'
import { z } from 'zod'
import { ElementWithIdSchema } from './identifiable'

async function throwSilabExceptionIfResponseError(
    response: Response,
    message: string
) {
    if (!response.ok) {
        const responseObject = await response.json().catch(() => ({}))
        const responseMessage = responseObject.hasOwnProperty('detail')
            ? responseObject.detail // on privilégie le détail s'il est présent
            : `${message} : ${response.status}`

        throw new SilabError(responseMessage)
    }
}

export const AllowedQueryParameterValueSchema = z.union([
    z.string(),
    z.string().array(),
    z.boolean(),
    z.undefined(),
    z.date(),
    z.number(),
    ElementWithIdSchema.array(),
])

export type AllowedQueryParameterValue = z.infer<
    typeof AllowedQueryParameterValueSchema
>
export type QueryParameters = Map<string, AllowedQueryParameterValue>
export type ResultPage<T> = { data: T[]; maxResults: number }

export function parametersToQuerystring(
    parameters: QueryParameters // Note utiliser la classe URLSearchParams serait intéressant mais tant qu'on utilise "[]=" pour faire des tableaux plutôt ques des "," ça n'est pas pertinent
): string {
    const urlParametersArray: string[] = []

    for (const [parameterKey, parameterValue] of parameters) {
        if (isUndefined(parameterValue)) {
            continue
        }
        if (isString(parameterValue) || isNumber(parameterValue)) {
            urlParametersArray.push(
                encodeURIComponent(parameterKey) +
                    '=' +
                    encodeURIComponent(parameterValue)
            )
            continue
        }
        if (isDate(parameterValue)) {
            urlParametersArray.push(
                encodeURIComponent(parameterKey) +
                    '=' +
                    encodeURIComponent(parameterValue.toISOString())
            )
            continue
        }
        if (isBoolean(parameterValue)) {
            urlParametersArray.push(
                encodeURIComponent(parameterKey) + '=' + parameterValue
            )
            continue
        }
        if (isArrayOf(parameterValue, isString)) {
            const encodedValues = parameterValue.map((value) => {
                return (
                    encodeURIComponent(parameterKey) +
                    '[]=' +
                    encodeURIComponent(value)
                )
            })
            urlParametersArray.push(...encodedValues)
            continue
        }
        if (isArrayOf(parameterValue, isElementWithId)) {
            const encodedValues = parameterValue.map((value) => {
                return (
                    encodeURIComponent(parameterKey) +
                    '[]=' +
                    encodeURIComponent(value.id)
                )
            })
            urlParametersArray.push(...encodedValues)
            continue
        }
    }

    return urlParametersArray.join('&')
}

async function callBackend(
    method: string,
    endpoint: string,
    headers: Headers,
    queryParameters: QueryParameters = new Map(),
    body?: string,
    abortSignal?: AbortSignal
) {
    const queryString =
        queryParameters.size === 0
            ? ''
            : '?' + parametersToQuerystring(queryParameters)

    emitLoadingStarted()
    const response = await fetch(
        `${import.meta.env.VITE_BACKEND_URL}${endpoint}${queryString}`,
        {
            method,
            headers,
            body,
            signal: abortSignal,
        }
    )
        .catch((error) => {
            if (error.name === 'AbortError') {
                throw error
            }
            throw new SilabError(error.message, error)
        })
        .finally(() => {
            emitLoadingEnded()
        })

    await throwSilabExceptionIfResponseError(
        response,
        `Erreur lors du ${method} des données`
    )

    return response
}

/**
 *
 * @param endpoint l'endpoint de l'api ne doit pas commencer par un /
 * @returns le retour brute de la requête, en théorie compatible avec une collection de Dto
 */
export async function getBackendData(
    endpoint: string,
    queryParameters: QueryParameters = new Map(),
    additionalHeaders: Record<string, string> = {},
    abortSignal?: AbortSignal
): Promise<Response> {
    const headers = new Headers({
        accept: 'application/json',
        ...additionalHeaders,
    })

    return await callBackend(
        'GET',
        endpoint,
        headers,
        queryParameters,
        undefined,
        abortSignal
    )
}

export async function postBackendData(
    endpoint: string,
    body: string,
    additionalHeaders: Record<string, string> = {}
): Promise<Response> {
    const headers = new Headers({
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...additionalHeaders,
    })

    return await callBackend('POST', endpoint, headers, undefined, body)
}

export async function patchBackendData(
    endpoint: string,
    body: string,
    additionalHeaders: Record<string, string> = {}
): Promise<Response> {
    const headers = new Headers({
        Accept: 'application/json',
        'Content-Type': 'application/merge-patch+json',
        ...additionalHeaders,
    })

    return await callBackend('PATCH', endpoint, headers, undefined, body)
}

export async function putBackendData(
    endpoint: string,
    body: string,
    additionalHeaders: Record<string, string> = {}
): Promise<Response> {
    const headers = new Headers({
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...additionalHeaders,
    })

    return await callBackend('PUT', endpoint, headers, undefined, body)
}

export async function deleteBackendData(
    endpoint: string,
    additionalHeaders: Record<string, string> = {}
): Promise<Response> {
    const headers = new Headers({
        Accept: 'application/json',
        ...additionalHeaders,
    })

    return await callBackend('DELETE', endpoint, headers)
}
