import { SilabError } from '@/shared/errors/SilabError'
import { EmptyFileError } from '@/shared/errors/EmptyFileError'

// inspiré par https://stackoverflow.com/a/50463054
export async function fetchFileAsDataUrl(url: string): Promise<string> {
    const response = await fetch(url)
    if (response.status === 204) {
        throw new EmptyFileError()
    }
    const blob = await response.blob()

    return new Promise((resolve) => {
        const reader = new FileReader()
        reader.onloadend = function () {
            resolve(this.result as string)
        }
        reader.readAsDataURL(blob)
    })
}

export async function fetchFileAsBase64(url: string) {
    return dataUrlToBase64(await fetchFileAsDataUrl(url))
}

export function dataUrlToBase64(dataUrl: string) {
    const base64Index = dataUrl.indexOf(';base64,')
    if (base64Index === -1) {
        throw new SilabError('Format base64 en entrée non compatible.')
    }

    return dataUrl.slice(base64Index + 8)
}

export function extractMimeTypeFromBase64(base64: string) {
    const signatures: { [key: string]: string } = {
        R0lGODdh: 'image/gif',
        R0lGODlh: 'image/gif',
        iVBORw0KGgo: 'image/png',
        '/9j/': 'image/jpg',
    }
    for (const signature in signatures) {
        if (base64.startsWith(signature)) {
            return signatures[signature]
        }
    }
    throw new Error('type mime inconnu')
}

export function base64ToDataUrl(base64: string) {
    return `data:${extractMimeTypeFromBase64(base64)};base64,${base64}`
}

export async function getBase64FromUrlOrDataUrl(urlOrDataUrl: string) {
    if (/data:.+/.exec(urlOrDataUrl)) {
        return dataUrlToBase64(urlOrDataUrl)
    }

    return await fetchFileAsBase64(urlOrDataUrl)
}
