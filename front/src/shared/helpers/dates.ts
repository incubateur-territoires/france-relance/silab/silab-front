export function dateArrondieAuJour(date: Date): Date {
    const msInADay = 24 * 3600 * 1000
    return new Date(Math.floor(date.getTime() / msInADay) * msInADay)
}

/**
 * Cette fonction transforme un objet 'date' ou 'undefined' en string au format 'jj/mm/aa à hh:mm', on peut définir la précision.
 * @param date Si undefined est fournit la fonction retourne une chaine vide.
 */
export function formatageDatePourAffichage(
    date: Date | undefined,
    precision: 'minute' | 'jour' = 'minute'
): string {
    if (date === undefined) {
        return ''
    }
    switch (precision) {
        case 'minute':
            return (
                date.toLocaleDateString('fr', {
                    day: '2-digit',
                    month: '2-digit',
                    year: '2-digit',
                }) +
                ' à ' +
                date.toLocaleTimeString('fr', {
                    hour: '2-digit',
                    minute: '2-digit',
                })
            )
        case 'jour':
            return date.toLocaleDateString('fr', {
                day: '2-digit',
                month: '2-digit',
                year: '2-digit',
            })
    }
}

/**
 * @returns La date au format YYYY-MM-DDTHH:MM , telle que acceptée par les input de type date, cf. https://developer.mozilla.org/fr/docs/Web/HTML/Element/input/datetime-local#valeur
 */
export function dateTimeToDateInputValue(date: Date): string {
    const year = date.getFullYear()
    const month = String(date.getMonth() + 1).padStart(2, '0') // Les mois commencent à 0 en JavaScript
    const day = String(date.getDate()).padStart(2, '0')
    const hours = String(date.getHours()).padStart(2, '0')
    const minutes = String(date.getMinutes()).padStart(2, '0')

    return `${year}-${month}-${day}T${hours}:${minutes}`
}

export function getDateLocalIsoString(date: Date): string {
    const year = date.getFullYear()
    const month = String(date.getMonth() + 1).padStart(2, '0') // Les mois commencent à 0 en JavaScript
    const day = String(date.getDate()).padStart(2, '0')

    return `${year}-${month}-${day}`
}

/**
 * Return a string representing the anteriotity of an event, like : "5 days ago"
 * inspired by https://momentjscom.readthedocs.io/en/latest/moment/04-displaying/02-fromnow/
 *
 * @param fromDate if not specified, we assume current date (from now)
 */
export function getAnteriorityString(eventDate: Date, fromDate?: Date): string {
    // if not specified, we assume current date
    fromDate ??= new Date()

    const rtf1 = new Intl.RelativeTimeFormat(undefined, { style: 'long' })

    // code from https://blog.webdevsimplified.com/2020-07/relative-time-format/

    const DIVISIONS: { amount: number; name: Intl.RelativeTimeFormatUnit }[] = [
        { amount: 60, name: 'seconds' },
        { amount: 60, name: 'minutes' },
        { amount: 24, name: 'hours' },
        { amount: 7, name: 'days' },
        { amount: 4.34524, name: 'weeks' },
        { amount: 12, name: 'months' },
        { amount: Number.POSITIVE_INFINITY, name: 'years' },
    ]

    let anteriorityString = ''
    let duration = (eventDate.getTime() - fromDate.getTime()) / 1000
    DIVISIONS.some((currentDivision) => {
        if (Math.abs(duration) < currentDivision.amount) {
            anteriorityString = rtf1.format(
                Math.round(duration),
                currentDivision.name
            )
            return true
        }
        duration /= currentDivision.amount
    })

    return anteriorityString
}

/**
 * retourne (en francais) un écart de temps relatif, ex : "3 mois"
 */
export function getRelativeTimeString(
    eventDate: Date,
    fromDate?: Date
): string {
    // if not specified, we assume current date
    fromDate ??= new Date()
    const DIVISIONS: { amount: number; name: string }[] = [
        { amount: 60, name: 'secondes' },
        { amount: 60, name: 'minutes' },
        { amount: 24, name: 'heures' },
        { amount: 7, name: 'jours' },
        { amount: 4.34524, name: 'semaines' },
        { amount: 12, name: 'mois' },
        { amount: Number.POSITIVE_INFINITY, name: 'années' },
    ]

    let anteriorityString = ''
    let duration = (eventDate.getTime() - fromDate.getTime()) / 1000
    DIVISIONS.some((currentDivision) => {
        if (Math.abs(duration) < currentDivision.amount) {
            anteriorityString = `${~~duration} ${currentDivision.name}`
            return true
        }
        duration /= currentDivision.amount
    })

    return anteriorityString
}
