// from https://www.tutorialspoint.com/How-to-detect-a-mobile-device-with-JavaScript
export function isMobile() {
    if (
        navigator.userAgent.match(/Android/i) ||
        navigator.userAgent.match(/webOS/i) ||
        navigator.userAgent.match(/iP(hone|od|ad)/i) ||
        navigator.userAgent.match(/BlackBerry/i) ||
        navigator.userAgent.match(/Windows Phone/i) ||
        navigator.userAgent.match(/Opera M(obi|ini)/i) ||
        navigator.userAgent.match(/Samsung/i)
    ) {
        return true
    } else {
        return false
    }
}

export function isIphone() {
    if (navigator.userAgent.match(/iPhone/i)) {
        return true
    } else {
        return false
    }
}

export function isDesktop() {
    return !isMobile()
}

/**
 * https://developer.mozilla.org/en-US/docs/Web/API/Permissions_API#browser_compatibility
 * On reimplémente cette feature car ce n'est pas encore supportée sur tout les navigateurs
 */
export async function queryCameraPermission() {
    const stream = await navigator.mediaDevices.getUserMedia({
        video: true,
    })

    stream.getTracks().forEach((track) => track.stop())
}
