export function getElementVisibleHeigth(element: HTMLElement) {
    const {
        top: distanceFromTop,
        bottom,
        height,
    } = element.getBoundingClientRect()

    const distanceFromBottom = window.innerHeight - bottom

    return Math.max(
        Math.min(
            height,
            window.innerHeight - distanceFromTop,
            window.innerHeight - distanceFromBottom
        ),
        0
    )
}
