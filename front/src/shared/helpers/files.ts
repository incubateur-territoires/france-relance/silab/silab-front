import { isUndefined } from './types'

export interface FichierNommé {
    blob: Blob
    nomDeFichier: string
}

export function téléchargerFichierNommé(FichierNommé: FichierNommé) {
    const objectUrl = window.URL.createObjectURL(FichierNommé.blob)

    const lienDomElement = document.createElement('a')
    lienDomElement.setAttribute('href', objectUrl)
    lienDomElement.setAttribute('download', FichierNommé.nomDeFichier)
    lienDomElement.style.display = 'none'
    document.body.appendChild(lienDomElement)
    lienDomElement.click()
    document.body.removeChild(lienDomElement)
    window.URL.revokeObjectURL(objectUrl)
}

export function extraireNomDeFichierDepuisHeaderContentDisposition(
    headerContentDisposition: string
) {
    const nomDeFichier = headerContentDisposition.match(
        /filename="?(?<filename>[^"]*?)"?(;|$)/
    )?.groups?.filename

    if (isUndefined(nomDeFichier)) {
        throw new Error('nom de fichier impossible à décoder')
    }

    return nomDeFichier
}
