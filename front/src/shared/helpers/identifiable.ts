import { z } from 'zod'

const primitiveValueSchema = z.union([
    z.string(),
    z.number(),
    z.boolean(),
    z.null(),
    z.undefined(),
    z.symbol(),
    z.bigint(),
])

export const ElementWithIdSchema = z.object({
    id: z.string().or(z.number()),
})

const IdentityStringProducerSchema = z.object({
    getIdentityString: z.function().returns(z.string()),
})

const _IdentifiableElementSchema = z.union([
    primitiveValueSchema,
    ElementWithIdSchema,
    IdentityStringProducerSchema,
])

export type IdentifiableElement = z.infer<typeof _IdentifiableElementSchema>
export type ElementWithId = z.infer<typeof ElementWithIdSchema>
export type IdentityStringProducer = z.infer<
    typeof IdentityStringProducerSchema
>

export function areElementsIdentical(
    elementA: IdentifiableElement,
    elementB: IdentifiableElement
): boolean {
    try {
        return (
            primitiveValueSchema.parse(elementA) ===
            primitiveValueSchema.parse(elementB)
        )
    } catch {}
    try {
        return (
            ElementWithIdSchema.parse(elementA).id ===
            ElementWithIdSchema.parse(elementB).id
        )
    } catch {}
    try {
        return (
            IdentityStringProducerSchema.parse(elementA).getIdentityString() ===
            IdentityStringProducerSchema.parse(elementB).getIdentityString()
        )
    } catch {}

    return false
}

export function allElementsAreIdentical(
    elementsA: IdentifiableElement[],
    elementsB: IdentifiableElement[]
): boolean {
    return (
        elementsA.length === elementsB.length &&
        elementsA.every((_identifiableElement, identifiableElementIndex) =>
            areElementsIdentical(
                elementsA[identifiableElementIndex],
                elementsB[identifiableElementIndex]
            )
        )
    )
}

export function arrayContainsAllElements(
    arrayOfElements: IdentifiableElement[],
    elementsThatShouldBeContainedInArray: IdentifiableElement[]
): boolean {
    return elementsThatShouldBeContainedInArray.every(
        (
            elementThatShouldBeContainedInArray,
            _elementThatShouldBeContainedInArrayIndex
        ) =>
            arrayOfElements.some((someElementFromArray) =>
                areElementsIdentical(
                    elementThatShouldBeContainedInArray,
                    someElementFromArray
                )
            )
    )
}
