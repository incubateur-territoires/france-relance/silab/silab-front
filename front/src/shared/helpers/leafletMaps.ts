import L from 'leaflet'
import { Ref } from 'vue'

export function displayCurrentPositionMarkerLocationOnMap(
    currentPosition: GeolocationPosition,
    currentPositionMarker: Ref<L.CircleMarker | undefined>,
    currentPositionAccuracyCircle: Ref<L.Circle | undefined>,
    map: L.Map,
    markerGroup?: L.FeatureGroup,
    isEnabledAccuracyCircle?: boolean
) {
    const currentPositionLocation = {
        lat: currentPosition.coords.latitude,
        lng: currentPosition.coords.longitude,
    }

    if (currentPositionMarker.value) {
        currentPositionMarker.value.setLatLng(currentPositionLocation)
    } else {
        currentPositionMarker.value = L.circleMarker(currentPositionLocation, {
            radius: 10,
            fillOpacity: 0.9,
        }).addTo(map)
        if (markerGroup !== undefined) {
            markerGroup.addLayer(currentPositionMarker.value)
        }
    }

    if (isEnabledAccuracyCircle) {
        if (currentPositionAccuracyCircle.value) {
            currentPositionAccuracyCircle.value
                .setLatLng(currentPositionLocation)
                .setRadius(currentPosition.coords.accuracy)
        } else {
            currentPositionAccuracyCircle.value = L.circle(
                currentPositionLocation,
                { radius: currentPosition.coords.accuracy }
            ).addTo(map)
        }
    }
}
