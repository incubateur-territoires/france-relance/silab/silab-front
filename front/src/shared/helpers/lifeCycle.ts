import { computed, ref, Ref } from 'vue'
import { isUndefined } from './types'

export type VolatileRef<Type> = Ref<{ complete: boolean; data: Type }>

export type SecuredData<T> =
    | { accessGranted: true; data: T }
    | { accessGranted: false; data: undefined }

export type VolatileSecuredData<T> = SecuredData<T> | undefined

/**
 * @returns un nouveau securedData sur lequel les éléments on été filtrés avec le fonciton donnée
 */
export function securedDataArrayFilter<T>(
    securedData: VolatileSecuredData<T[]>,
    dataFilter: (securedData: T) => boolean
): VolatileSecuredData<T[]> {
    if (isUndefined(securedData)) {
        return undefined
    }

    if (securedData.accessGranted === false) {
        return securedData
    }

    return {
        accessGranted: true,
        data: securedData.data.filter(dataFilter),
    }
}

export interface Clonable<T> {
    clone(): T
}

export function buildOriginalAwareClone<T extends Clonable<T>>(clonable: T) {
    return {
        original: clonable,
        clone: clonable.clone(),
    }
}

export function buildOriginalAwareClones<T extends Clonable<T>>(
    clonables: T[]
): OriginalAwareClone<T>[] {
    return clonables.map((clonable) => buildOriginalAwareClone(clonable))
}

/**
 * @returns un nouveau securedData sur lequel les éléments on été filtrés avec le fonciton donnée
 */
export function securedDataArrayMap<T, T2>(
    securedData: VolatileSecuredData<T[]>,
    dataMapCallback: (securedData: T) => T2
): VolatileSecuredData<T2[]> {
    if (isUndefined(securedData)) {
        return undefined
    }

    if (securedData.accessGranted === false) {
        return securedData
    }

    return {
        accessGranted: true,
        data: securedData.data.map(dataMapCallback),
    }
}

/**
 * @returns un nouveau securedData avec la data transformée via un callback
 */
export function securedDataTransform<T, T2>(
    securedData: VolatileSecuredData<T>,
    dataTransformCallback: (securedData: T) => T2
): VolatileSecuredData<T2> {
    if (isUndefined(securedData)) {
        return undefined
    }

    if (securedData.accessGranted === false) {
        return securedData
    }

    return {
        accessGranted: true,
        data: dataTransformCallback(securedData.data),
    }
}

export type OriginalAwareClone<Type> = { original: Readonly<Type>; clone: Type }

/**
 * Permet de savoir si des requêtes sont en cours en s' affranchissant de la temporalité (conflict dans la séquence de début/arrêts de requêtes).
 * Notamment utile dans les requêtes ayant un aborter.
 */
export class LoadingManager {
    private loadingRequests = ref<symbol[]>([])

    public isLoading = computed(() => {
        return this.loadingRequests.value.length > 0
    })

    public startLoading() {
        const id = Symbol()
        this.loadingRequests.value.push(id)
        return () => {
            this.stopLoading(id)
        }
    }

    private stopLoading(id: symbol): void {
        this.loadingRequests.value = this.loadingRequests.value.filter(
            (currentId) => currentId !== id
        )
    }
}
