export function moduloPositif(
    nombrePotentiellementNégatif: number,
    modulo: number
) {
    return ((nombrePotentiellementNégatif % modulo) + modulo) % modulo
}

/**
 * @returns rien si zero
 */
export function nombreAvecSigneString(nombre: number): string {
    return nombre === 0 ? '' : `${nombre > 0 ? '+' : ''}${nombre}`
}

export function getSérieDeValeurEntreDeuxEntiersInclus(
    entierLePlusPetit: number,
    entierLePlusGrand: number
) {
    if (
        !Number.isInteger(entierLePlusPetit) ||
        !Number.isInteger(entierLePlusGrand) ||
        entierLePlusPetit > entierLePlusGrand
    ) {
        throw new Error(
            'Les valeurs données ne respectent pas les contraintes (entiers, ordre)'
        )
    }

    const sérieDeValeurs = []
    for (
        let valeurDeLaSérie = entierLePlusPetit;
        valeurDeLaSérie <= entierLePlusGrand;
        valeurDeLaSérie++
    ) {
        sérieDeValeurs.push(valeurDeLaSérie)
    }

    return sérieDeValeurs
}
