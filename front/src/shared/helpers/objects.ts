/**
 * @returns true if the property was initialized by this function
 */
export function initializePropertyIfNotExisting<T>(
    object: { [propertyKeys: string | number | symbol]: unknown },
    property: string | number,
    initialValue: T
) {
    if (object.hasOwnProperty(property)) {
        return false
    }

    object[property] = initialValue
    return true
}

export interface Equatable<T> {
    equals(other: T): boolean
}

//Source: https://medium.com/analytics-vidhya/javascript-deep-comparison-of-objects-with-a-recursive-call-f67a8f37a343
export function deepEquals(a: unknown, b: unknown): boolean {
    if (a === b) {
        return true
    }
    if (
        typeof a !== 'object' ||
        a === null ||
        typeof b !== 'object' ||
        b === null
    ) {
        return false
    }

    const aKeys = Object.keys(a) as (keyof typeof a)[]
    const bKeys = Object.keys(b) as (keyof typeof b)[]

    if (aKeys.length !== bKeys.length) {
        return false
    }

    for (const key of aKeys) {
        if (!bKeys.includes(key) || !deepEquals(a[key], b[key])) {
            return false
        }
    }

    return true
}
