export function confirmBeforeCancel(
    cancelAction: () => void,
    message: string = 'Si vous annulez, vous perdrez vos modifications.',
    bypassConfirmation: boolean = false
) {
    if (bypassConfirmation) {
        return cancelAction()
    }

    if (window.confirm(message)) {
        cancelAction()
    }
}
