export function getResizedDimensionsWithinBounds(
    currentWidth: number,
    currentHeight: number,
    maxWidth: number,
    maxHeight: number
) {
    const compressionFactor = Math.min(
        Math.min(maxWidth / currentWidth, maxHeight / currentHeight),
        1
    )
    const newWidth = Math.floor(currentWidth * compressionFactor)
    const newHeight = Math.floor(currentHeight * compressionFactor)

    return { width: newWidth, height: newHeight }
}
