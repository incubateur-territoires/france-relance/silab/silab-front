/**
 *  Ne prend en compte que les mots s'accordant avec un 's' a la forme pluriel.
 */
export function pluralForm(nom: string, count: number): string {
    if (nom === '') {
        return ''
    }

    return nom + (Math.abs(count) > 1 ? 's' : '')
}

export function textFromNumericBooleanValue(value: number | undefined) {
    if (value === 1) {
        return 'oui'
    }
    if (value === 0) {
        return 'non'
    }
    return '¯_(ಠ_ಠ)_/¯'
}

export function truncateWithEllipsis(string: string, maxLength: number) {
    if (string.length > maxLength) {
        return string.substring(0, maxLength - 3) + '...'
    }
    return string
}
