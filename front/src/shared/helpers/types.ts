import { InjectionKey, inject } from 'vue'
import { UndefinedObjectError } from '../errors/UndefinedObjectError'
import { SilabError } from '../errors/SilabError'
import { ElementWithId, ElementWithIdSchema } from './identifiable'

export type HiddenFields<Entity extends object> = {
    [Field in keyof Entity]?: true
}

// credits :  https://github.com/joonhocho/tsdef/blob/master/src/index.ts
// make all properties optional recursively including nested objects.
// keep in mind that this should be used on json / plain objects only.
// otherwise, it will make class methods optional as well.
export type DeepPartial<T> = {
    [P in keyof T]?: T[P] extends Array<infer I>
        ? Array<DeepPartial<I>>
        : DeepPartial<T[P]>
}

export function assertDefined<T>(
    object: T
): asserts object is Exclude<T, undefined> {
    if (object === undefined) {
        throw new UndefinedObjectError("L'object est undefined")
    }
}

export function isDefined<T>(
    unkownTypeVariable: T
): unkownTypeVariable is Exclude<T, undefined> {
    return unkownTypeVariable !== undefined
}

export function isDefinedAndNotNull<T>(
    unkownTypeVariable: T
): unkownTypeVariable is Exclude<T, undefined | null> {
    return isDefined(unkownTypeVariable) && !isNull(unkownTypeVariable)
}

export function isUndefined(
    unkownTypeVariable: unknown
): unkownTypeVariable is undefined {
    return unkownTypeVariable === undefined
}

export function isNull(
    unkownTypeVariable: unknown
): unkownTypeVariable is null {
    return unkownTypeVariable === null
}

export function isNotNull<T>(
    unkownTypeVariable: T
): unkownTypeVariable is Exclude<T, null> {
    return !isNull(unkownTypeVariable)
}
export function isArray(
    unkownTypeVariable: unknown
): unkownTypeVariable is Array<unknown> {
    return Array.isArray(unkownTypeVariable)
}

export function isArrayOf<T>(
    unkownTypeVariable: unknown,
    checkType: (value: unknown) => value is T
): unkownTypeVariable is Array<T> {
    return (
        Array.isArray(unkownTypeVariable) && unkownTypeVariable.every(checkType)
    )
}

export function isNumber(
    unkownTypeVariable: unknown
): unkownTypeVariable is number {
    return typeof unkownTypeVariable === 'number'
}

export function isDate(
    unkownTypeVariable: unknown
): unkownTypeVariable is Date {
    return unkownTypeVariable instanceof Date
}

export function isBoolean(
    unkownTypeVariable: unknown
): unkownTypeVariable is boolean {
    return typeof unkownTypeVariable === 'boolean'
}

export function isElementWithId(
    unkownTypeVariable: unknown
): unkownTypeVariable is ElementWithId {
    return ElementWithIdSchema.safeParse(unkownTypeVariable).success
}

export function isNullOrUndefined(
    unkownTypeVariable: unknown
): unkownTypeVariable is undefined | null {
    return isNull(unkownTypeVariable) || isUndefined(unkownTypeVariable)
}

// cette fonction est inspirée de https://vuedose.tips/the-new-provide-inject-in-vue-3#default-values-and-guaranteed-provision-of-data (mais en mieux)
export function requiredInject<T>(key: InjectionKey<T>): T {
    const defaultUnprovidedValue = Symbol()
    const resolved = inject<T | typeof defaultUnprovidedValue>(
        key,
        defaultUnprovidedValue
    )
    if (resolved === defaultUnprovidedValue) {
        throw new SilabError(
            `Erreur technique: l'injection avec la clef ${
                key.description ?? '(symbole sans description)'
            } à échoué car elle n'a pas été fournie (provide) en amont.`
        )
    }
    return resolved
}

export function requireValue<T>(unkownValueVariable: T | undefined): T {
    if (isUndefined(unkownValueVariable)) {
        throw new SilabError(
            'La variable fournie est obligatoire, "undefined" donné'
        )
    }
    return unkownValueVariable
}
