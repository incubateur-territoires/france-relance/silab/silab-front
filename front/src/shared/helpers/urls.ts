/**
 * examples :
 * - https://example.com/test -> true
 * - http://example.com/test -> true
 * - HTTP://example.com/test -> true
 * - //example.com/test -> true (oui c'est une url absolue valide)
 * - ftp://example.com/test -> false
 * - example.com/test -> false
 * - /test -> false
 * - /test?redirect=https://example.com -> false
 */
export function isAbsoluteUrl(testString: string): boolean {
    const regex = /^(https?:)?\/\//i
    return regex.test(testString)
}
