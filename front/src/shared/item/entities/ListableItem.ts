import { Chip } from '@/shared/chip/entities/chip'
import { RouteLocationRaw } from 'vue-router'

export const DEFAULT_LISTABLE_ITEM_MAX_COVER_SIZE = 100

export interface SecondaryAction {
    icon: string
    action: () => void
}

export type PrimaryAction = () => void

export interface ListableItem {
    getItemId: () => string
    getItemTitle: () => string
    getItemDescription: () => string | string[]
    getItemImageUrl: () => URL | undefined
    getItemThumbnailUrl: (maxSize?: number) => URL | undefined
    getItemChips: () => Chip[]
    getItemTemporalData: () => string
    getItemRoute: () => RouteLocationRaw | undefined
    setItemRoute: (route: RouteLocationRaw | undefined) => this
    primaryAction?: PrimaryAction | undefined
    setItemPrimaryAction?: (primaryAction: PrimaryAction) => this
    getItemSecondaryAction?: () => SecondaryAction | undefined
    setItemSecondaryAction?: (secondaryAction: SecondaryAction) => this
    isRequiringAttention: () => boolean
}

export function getItemThumbnailUrl(
    this: ListableItem,
    maxSize?: number
): URL | undefined {
    const imageUrl = this.getItemImageUrl()

    if (imageUrl === undefined) {
        return undefined
    }
    return getThumbnailUrl(imageUrl, maxSize)
}

export function getThumbnailUrl(
    imageUrl: URL,
    maxSize: number = DEFAULT_LISTABLE_ITEM_MAX_COVER_SIZE
): URL {
    imageUrl.searchParams.set('maxSize', maxSize.toString())

    return imageUrl
}
