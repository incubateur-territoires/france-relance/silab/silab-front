import { ListableItem } from './ListableItem'

export interface ListableItemList<T extends ListableItem> {
    title: string
    items: T[]
    icon?: string
    itemHeight?: number
}
