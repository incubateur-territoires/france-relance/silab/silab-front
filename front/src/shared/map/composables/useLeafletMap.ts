import L, { ZoomPanOptions } from 'leaflet'
import markerIconUrl from 'leaflet/dist/images/marker-icon.png'
import markerIconRetinaUrl from 'leaflet/dist/images/marker-icon-2x.png'
import markerShadowUrl from 'leaflet/dist/images/marker-shadow.png'
import { GestureHandling } from 'leaflet-gesture-handling'
import { useMapLayerStore } from '@/interventionServiceOffer/stores/MapLayerStore'
import { useStoreKeys } from '@/shared/user/composables/useStoreKeys'
import { displayCurrentPositionMarkerLocationOnMap } from '@/shared/helpers/leafletMaps'
import { Ref, onMounted, onUnmounted, shallowRef, watchEffect } from 'vue'
import { isDefined, isUndefined } from '@/shared/helpers/types'
import { useSiilabConfigurationStore } from '@/shared/administration/stores/SiilabConfigurationStore'
import { useMsal } from '@/plugins/msal/useMsal'

export function useLeafletMap(
    options: {
        containerId?: string
        viewOptions?: {
            zoom?: number
            options?: ZoomPanOptions
            maxZoom?: number
        }
        preventDrag?: boolean
    } = {}
): {
    map: Ref<L.Map | undefined>
    currentPositionMarker: Ref<L.CircleMarker | undefined>
    changeMapLayer: (mapLayer?: typeof mapLayerStore.mapLayer) => void
} {
    const siilabConfigurationStore = useSiilabConfigurationStore()
    const defaultZoom = 22
    const mapOptions: L.MapOptions & { gestureHandling: boolean } = {
        ...options.viewOptions,
        gestureHandling: options.preventDrag ?? false,
        zoomControl: false,
    }
    const tileLayerStandard = L.tileLayer(
        import.meta.env.VITE_MAP_STANDARD_TILE_LAYER_URL,
        {
            attribution: import.meta.env.VITE_MAP_STANDARD_TILE_ATTRIBUTION,
            maxZoom: mapOptions.maxZoom ?? defaultZoom,
        }
    )
    const tileLayerOrthophotography = L.tileLayer.wms(
        import.meta.env.VITE_MAP_SATELLITE_TILE_LAYER_URL,
        {
            layers: '0',
            format: 'image/jpeg',
            transparent: false,
            attribution: import.meta.env.VITE_MAP_SATELLITE_TILE_ATTRIBUTION,
            maxZoom: mapOptions.maxZoom ?? defaultZoom,
        }
    )

    const msal = useMsal()
    const { generateUserScopedStoreKey } = useStoreKeys(msal)

    const mapLayerStore = useMapLayerStore(
        generateUserScopedStoreKey('MapLayer')
    )

    const mapContainerId =
        options.containerId ?? 'mapPickerContainer-' + self.crypto.randomUUID()

    const currentPositionMarker = shallowRef<L.CircleMarker | undefined>(
        undefined
    )

    const currentPositionAccuracyCircle = shallowRef<L.Circle | undefined>(
        undefined
    )

    const map = shallowRef<L.Map | undefined>(undefined)

    function changeMapLayer(mapLayer?: typeof mapLayerStore.mapLayer) {
        if (isDefined(mapLayer)) {
            mapLayerStore.mapLayer = mapLayer
            return
        }

        mapLayerStore.mapLayer =
            mapLayerStore.mapLayer === 'Vue Satellite'
                ? 'Vue Standard'
                : 'Vue Satellite'
    }
    watchEffect(() => {
        if (isUndefined(map.value)) {
            return
        }
        switch (mapLayerStore.mapLayer) {
            case 'Vue Standard':
                tileLayerOrthophotography.removeFrom(map.value)
                tileLayerStandard.addTo(map.value)
                break
            case 'Vue Satellite':
                tileLayerStandard.removeFrom(map.value)
                tileLayerOrthophotography.addTo(map.value)
                break
            default:
                tileLayerOrthophotography.removeFrom(map.value)
                tileLayerStandard.addTo(map.value)
                break
        }
    })

    let geolocationWatchHandlerId: number | undefined = undefined
    function createMap(): void {
        L.Icon.Default.imagePath = ''

        L.Icon.Default.mergeOptions({
            iconUrl: markerIconUrl,
            iconRetinaUrl: markerIconRetinaUrl,
            shadowUrl: markerShadowUrl,
        })
        L.Map.addInitHook('addHandler', 'gestureHandling', GestureHandling)

        map.value = L.map(mapContainerId, mapOptions).setView(
            mapOptions.center ?? [
                siilabConfigurationStore.siilabConfiguration
                    ?.mapCenterLatitude ?? 46.584545,
                siilabConfigurationStore.siilabConfiguration
                    ?.mapCenterLongitude ?? 0.338348,
            ],
            mapOptions.zoom ?? 10,
            options.viewOptions?.options
        )

        map.value.attributionControl.setPrefix(false)

        // gestion de l'affichage de la position de l'utilisateur
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                displayCurrentPositionMarkerLocation(position)
                geolocationWatchHandlerId = navigator.geolocation.watchPosition(
                    displayCurrentPositionMarkerLocation,
                    undefined,
                    { enableHighAccuracy: true }
                )
            })
        }

        function displayCurrentPositionMarkerLocation(
            currentPosition: GeolocationPosition
        ) {
            if (isUndefined(map.value)) {
                return
            }
            displayCurrentPositionMarkerLocationOnMap(
                currentPosition,
                currentPositionMarker,
                currentPositionAccuracyCircle,
                map.value
            )
        }
    }

    onMounted(() => {
        createMap()
    })
    onUnmounted(() => {
        if (isDefined(geolocationWatchHandlerId)) {
            navigator.geolocation.clearWatch(geolocationWatchHandlerId)
        }
    })

    return {
        map,
        currentPositionMarker,
        changeMapLayer,
    }
}
