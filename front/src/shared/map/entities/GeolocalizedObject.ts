import { LatLngLiteral } from 'leaflet'

export interface GeolocalizedObject extends LatLngLiteral {
    key: string
    title: string
    description?: string
    href?: string
}
