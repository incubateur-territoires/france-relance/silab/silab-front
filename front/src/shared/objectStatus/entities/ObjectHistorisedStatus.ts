import { ObjectHistorisedStatusDto } from './ObjectHistorisedStatusDto'
import { ObjectStatus } from './ObjectStatus'

export class ObjectHistorisedStatus<T extends string> extends ObjectStatus<T> {
    comment?: string

    public constructor(
        objectHistorisedStatusDto: ObjectHistorisedStatusDto<T>
    ) {
        super(objectHistorisedStatusDto)
        this.comment = objectHistorisedStatusDto.comment
    }

    public toDto(): ObjectHistorisedStatusDto<T> {
        return {
            ...super.toDto(),
            comment: this.comment,
        }
    }
}
