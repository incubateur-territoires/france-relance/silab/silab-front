import { ObjectStatusDto } from './ObjectStatusDto'

export interface ObjectHistorisedStatusDto<T> extends ObjectStatusDto<T> {
    /**
     * Note : c'est un peu curieux, mais en fait ça vient d'un détail d'implementation côté back (qui pourrait être revu car ça n'a pas vraiement de logique métier)
     */
    comment?: string
}
