import { ObjectStatusDto } from './ObjectStatusDto'

export class ObjectStatus<AllowedCodes extends string = string> {
    code: AllowedCodes
    label: string
    createdBy: string
    createdAt: Date

    public constructor(objectStatusDto: ObjectStatusDto<AllowedCodes>) {
        if (objectStatusDto.createdAt === undefined) {
            throw new Error(
                'Un statut doit obligatoirement avoir une date de création.'
            )
        }
        this.code = objectStatusDto.code
        this.label = objectStatusDto.label
        this.createdBy = objectStatusDto.createdBy
        this.createdAt = new Date(objectStatusDto.createdAt)
    }

    public toDto(): ObjectStatusDto<AllowedCodes> {
        return {
            code: this.code,
            label: this.label,
            createdBy: this.createdBy,
            createdAt: this.createdAt.toISOString(),
        }
    }
}
