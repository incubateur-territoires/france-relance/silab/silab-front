export interface ObjectStatusDto<AllowedCodes = string> {
    code: AllowedCodes
    label: string
    createdBy: string
    createdAt: string
}
