import {
    RouteLocationMatched,
    RouteLocationNormalizedLoaded,
    RouteLocationRaw,
    RouteRecordRaw,
    useRouter,
} from 'vue-router'
import { SilabError } from '@/shared/errors/SilabError'
import { isDefined, isUndefined } from '@/shared/helpers/types'

export const useBreadcrumb = () => {
    const { currentRoute, resolve } = useRouter()

    const getSiblingRoute = (
        route: RouteLocationNormalizedLoaded,
        subPath: string
    ) => {
        const siblingRoutes = route.matched.at(-2)
        const siblingRoute = siblingRoutes?.children.find((siblingRoute) => {
            if (siblingRoute.path === subPath) {
                return siblingRoute
            }
        })
        if (isUndefined(siblingRoute)) {
            throw new SilabError(`Route "${subPath}" introuvable.`)
        }

        return {
            name: siblingRoute.name,
            params: route.params,
        }
    }
    /**
     * N'est considéré comme une route uniquement celles qui possèdent un nom et un composant c-à-d qui est prévue pour être rendu.
     * @var depth exmeple :  0 retourne le premier parent et 1 retourne le grand parent
     */
    const getParentRoute = (depth: number = 0): RouteLocationRaw => {
        // Extrait toutes les routes correspondantes à la route actuelle
        const matchedRoutes = currentRoute.value.matched

        const getFirstParentRouteWithNameRecursive = (
            matchedRoutes: RouteLocationMatched[],
            currentDepth: number,
            targetDepth: number
        ): RouteLocationMatched | RouteRecordRaw | undefined => {
            // Crée une copie du tableau matchedRoutes
            const matchedRoutesTmp = [...matchedRoutes]
            // Extrait et supprime la dernière route du tableau copié à l'aide de la méthode pop(), cette route extraite est la route d'origine
            const originRoute = matchedRoutesTmp.pop()
            // Récupère la dernière route restante dans le tableau copié à l'aide de la méthode at(-1), cette route restante est la route parente directe
            const directParent = matchedRoutesTmp.at(-1)

            if (isUndefined(directParent)) {
                return undefined
            }

            // Attention, avec cet algo, on part du principe qu'on ne nomme pas les routes inutilisables
            if (directParent.name !== undefined) {
                if (targetDepth === currentDepth) {
                    return directParent
                }
                return getFirstParentRouteWithNameRecursive(
                    matchedRoutesTmp,
                    currentDepth + 1,
                    depth
                )
            }

            //On essaye de récupérer la vue dans ses enfant.
            // Recherche dans les enfants de la route parente directe (directParent) le premier enfant qui a un chemin vide ('').
            const routeViewFromChildren = directParent.children.find(
                (child) => {
                    return child.path === ''
                }
            )
            // Vérifie si routeViewFromChildren est défini et si son nom est différent de celui de originRoute; Si ces conditions sont satisfaites, cela signifie que routeViewFromChildren est une vue enfant distincte de celle où se trouve actuellement l'utilisateur, et donc cette vue enfant est retournée, sinon, undefined est retourné.
            if (
                isDefined(routeViewFromChildren) &&
                routeViewFromChildren.name !== originRoute?.name
            ) {
                if (targetDepth === currentDepth) {
                    return routeViewFromChildren
                }
                return getFirstParentRouteWithNameRecursive(
                    matchedRoutesTmp,
                    currentDepth + 1,
                    depth
                )
            }

            return getFirstParentRouteWithNameRecursive(
                matchedRoutesTmp,
                currentDepth, // le noeud n'est une route  parente valide donc on incrémente pas.
                depth
            )
        }
        if (isUndefined(matchedRoutes)) {
            throw new SilabError('Aucune route match avec la route courante.')
        }

        // Appelle récursivement la fonction getFirstParentRouteWithNameRecursive pour trouver la première route parente avec un nom défini dans la liste matchedRoutes, si aucune route parente avec un nom défini n'est trouvée, il retourne /, sinon, il retourne un objet contenant le nom de la première route parente avec des paramètres éventuels provenant de la route actuelle
        const firstParentRouteWithName = getFirstParentRouteWithNameRecursive(
            matchedRoutes,
            0,
            depth
        )

        if (isUndefined(firstParentRouteWithName)) {
            return '/'
        }
        return resolve(firstParentRouteWithName)
    }
    return { getSiblingRoute, getParentRoute }
}
