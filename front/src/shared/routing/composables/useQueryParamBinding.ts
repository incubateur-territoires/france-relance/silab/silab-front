import { isDefined } from '@/shared/helpers/types'
import { useRouteQuery } from '@vueuse/router'
import { Ref, watch } from 'vue'
import { useRoute } from 'vue-router'
import { ZodEnum, ZodLiteral, ZodOptional, ZodString, z } from 'zod'

type OptionalStringLikeSchema =
    | ZodString
    | ZodLiteral<string>
    | ZodEnum<[string, ...string[]]>
    | ZodOptional<ZodString>
    | ZodOptional<ZodLiteral<string>>
    | ZodOptional<ZodEnum<[string, ...string[]]>>

type TransformOptions<QueryType, ReturnType> = {
    queryReadTransform: (queryParam: QueryType) => ReturnType
    queryWriteTransform: (queryParam: ReturnType) => QueryType
}

export const stringToBooleanQueryTransformOption: TransformOptions<
    string | undefined,
    boolean
> = {
    queryReadTransform: (booleanString: string | undefined) =>
        booleanString === 'true',
    queryWriteTransform: (boolean: boolean) => (boolean ? 'true' : 'false'),
}

export const stringToOptionalNumberQueryTransformOption: TransformOptions<
    string | undefined,
    number | undefined
> = {
    queryReadTransform: (numberString: string | undefined) =>
        isDefined(numberString) ? Number(numberString) : undefined,
    queryWriteTransform: (number: number | undefined) => number?.toString(),
}

/**
 * Synchronise la valeur de refToBind avec le query parameter de l'url queryParameterName :
 *
 * - Depuis query parameter -> refToBind uniquement à l'initialisation (et seulement si la valeur correspond au queryParameterValidationSchema)
 * - Depuis refToBind -> query parameter pendant la durée de vie du composable.
 *
 * @param queryParameterValidationSchema Le schema Zod permettant de valider le type de données acceptée,
 * particulièrement utile pour les valeurs distinctes, ex: 'demandes' | 'equipements' | 'interventions'
 */
export function useQueryParamBinding<
    QueryTypeSchema extends OptionalStringLikeSchema,
>(
    queryParameterName: string,
    refToBind: Ref<z.infer<QueryTypeSchema>>,
    queryParameterValidationSchema: QueryTypeSchema
) {
    const route = useRoute()

    const reactiveQueryParam = useRouteQuery<z.infer<QueryTypeSchema> | null>(
        queryParameterName,
        null,
        { mode: 'push' }
    )

    // on récupère la valeur du query param en prio pour l'initialisation
    // il est donc important que ce watch soit en premier
    watch(
        () => route.query[queryParameterName],
        (newQueryParameterValue, oldQueryParameterValue) => {
            if (newQueryParameterValue === oldQueryParameterValue) {
                return
            }

            const securedNewQueryParameterValue =
                queryParameterValidationSchema.safeParse(newQueryParameterValue)

            if (!securedNewQueryParameterValue.success) {
                return
            }

            refToBind.value = securedNewQueryParameterValue.data
        },
        { immediate: true }
    )

    watch(
        refToBind,
        (newRefValue, oldRefValue) => {
            if (newRefValue === oldRefValue) {
                return
            }
            reactiveQueryParam.value = newRefValue ?? null
        },
        { immediate: true }
    )
}

/**
 * Synchronise la valeur de refToBind avec le query parameter de l'url queryParameterName :
 * Depuis query parameter -> refToBind uniquement à l'initialisation (et seulement si la valeur correspond au queryParameterValidationSchema)
 * Depuis refToBind -> query parameter pendant la durée de vie du composable.
 *
 * @param queryParameterValidationSchema Le schema Zod permettant de valider le type de données acceptée,
 * particulièrement utile pour les valeurs distinctes, ex: 'demandes' | 'equipements' | 'interventions'
 */
export function useQueryParamBindingWithtransform<
    QueryTypeSchema extends OptionalStringLikeSchema,
    ReturnType = z.infer<QueryTypeSchema>,
>(
    queryParameterName: string,
    refToBind: Ref<ReturnType>,
    queryParameterValidationSchema: QueryTypeSchema,
    transformOptions: TransformOptions<z.infer<QueryTypeSchema>, ReturnType>
) {
    const route = useRoute()

    const reactiveQueryParam = useRouteQuery<z.infer<QueryTypeSchema> | null>(
        queryParameterName,
        null,
        { mode: 'push' }
    )

    // on récupère la valeur du query param en prio pour l'initialisation
    // il est donc important que ce watch soit en premier
    watch(
        () => route.query[queryParameterName],
        (newQueryParameterValue, oldQueryParameterValue) => {
            if (newQueryParameterValue === oldQueryParameterValue) {
                return
            }

            const securedNewQueryParameterValue =
                queryParameterValidationSchema.safeParse(newQueryParameterValue)

            if (!securedNewQueryParameterValue.success) {
                return
            }

            refToBind.value = transformOptions.queryReadTransform(
                securedNewQueryParameterValue.data
            )
        },
        { immediate: true }
    )

    watch(
        refToBind,
        (newRefValue, oldRefValue) => {
            if (newRefValue === oldRefValue) {
                return
            }
            reactiveQueryParam.value =
                transformOptions.queryWriteTransform(newRefValue) ?? null
        },
        { immediate: true }
    )
}
