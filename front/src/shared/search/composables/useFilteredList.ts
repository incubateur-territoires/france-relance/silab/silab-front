import { computed, Ref, toValue } from 'vue'

export const useFilteredList = <T>(
    listRef: Ref<T[]>,
    filterQuery: Ref<string> | string,
    callbackFnCreator: (
        filterQuery: string
    ) => (value: T, index: number, array: T[]) => unknown
) => {
    return computed(() =>
        listRef.value
            ? listRef.value.filter(
                  callbackFnCreator(toValue(filterQuery).toLowerCase())
              )
            : []
    )
}
