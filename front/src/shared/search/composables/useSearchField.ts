import { Ref, ref } from 'vue'
import SearchField from '@/shared/search/components/SearchField.vue'
import { useDisplay } from 'vuetify'

export const useSearchField = (
    searchField: Ref<InstanceType<typeof SearchField> | undefined>
) => {
    const { mobile } = useDisplay()
    const expanded = ref<boolean>(!mobile.value)
    const toggleExpanded = () => {
        expanded.value = !expanded.value
        if (expanded.value && searchField.value) {
            searchField.value?.focus()
        }
    }
    const loseFocus = () => {
        if (mobile.value && expanded.value) {
            expanded.value = false
        }
    }

    return { expanded, toggleExpanded, loseFocus }
}
