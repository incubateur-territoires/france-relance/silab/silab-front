// From https://github.com/vercel/swr/blob/master/src/libs/hash.ts

// hashes an array of objects and returns a string
export default function hash(args: any[]): string {
    if (!args.length) return ''
    let key = 'arg'
    for (let i = 0; i < args.length; ++i) {
        let _hash
        if (
            args[i] === null ||
            (typeof args[i] !== 'object' && typeof args[i] !== 'function')
        ) {
            // need to consider the case that args[i] is a string:
            // args[i]        _hash
            // "undefined" -> '"undefined"'
            // undefined   -> 'undefined'
            // 123         -> '123'
            // null        -> 'null'
            // "null"      -> '"null"'
            if (typeof args[i] === 'string') {
                _hash = '"' + args[i] + '"'
            } else {
                _hash = String(args[i])
            }
        } else {
            _hash = args[i].getIdentityString() ?? args[i].toString()
        }
        key += '@' + _hash
    }
    return key
}
