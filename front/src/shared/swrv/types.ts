import { Ref, WatchSource } from 'vue'
import SWRVCache from './cache'
import LocalStorageCache from './cache/adapters/localStorage'
import { IdentityStringProducer } from '../helpers/identifiable'

export type fetcherFn<Data> = (...args: any) => Data | Promise<Data>

export interface IConfig<
    Data = any,
    Fn extends fetcherFn<Data> = fetcherFn<Data>,
> {
    refreshInterval?: number
    cache?: LocalStorageCache | SWRVCache<any>
    dedupingInterval?: number
    ttl?: number
    serverTTL?: number
    revalidateOnFocus?: boolean
    revalidateDebounce?: number
    shouldRetryOnError?: boolean
    errorRetryInterval?: number
    errorRetryCount?: number
    fetcher?: Fn
    isOnline?: () => boolean
    isDocumentVisible?: () => boolean
}

export interface revalidateOptions {
    shouldRetryOnError?: boolean
    errorRetryCount?: number
    forceRevalidate?: boolean
}

export interface IResponse<Data = any, Error = any> {
    data: Ref<Data | undefined>
    error: Ref<Error | undefined>
    isValidating: Ref<boolean>
    mutate: ((data?: Data, opts?: revalidateOptions) => void) &
        ((data?: fetcherFn<Data>, opts?: revalidateOptions) => Promise<void>)
}

export type keyType =
    | (string | null | number | undefined | boolean | IdentityStringProducer)[]
    | string
    | null
    | undefined
    | boolean
    | number
    | IdentityStringProducer

export type IKey = keyType | WatchSource<keyType>
