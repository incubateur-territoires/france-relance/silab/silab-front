import { MsalContext } from '@/plugins/msal/useMsal'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { useRoute } from 'vue-router'

export function useStoreKeys(msal: MsalContext) {
    const route = useRoute()

    function getCurrentUserName() {
        //amelioration: à mon sens ça devrait être une ref
        if (import.meta.env.VITE_DISABLE_MOCK_AUTHENTICATION === 'true') {
            return import.meta.env.VITE_AZURE_TEST_USERNAME
        }

        return msal.accounts.value[0].username
    }

    function generateServiceOfferScopedStoreKey(
        storeName: string,
        serviceOfferId: ServiceOffer['id']
    ) {
        return storeName + '|' + serviceOfferId
    }

    function generateUserScopedStoreKey(storeName: string) {
        return storeName + '|' + getCurrentUserName()
    }

    function generateUserUrlScopedStoreKey(storeName: string) {
        return storeName + '|' + route.path + '|' + getCurrentUserName()
    }

    function generateUserServiceOfferScopedStoreKey(
        storeName: string,
        serviceOfferId: ServiceOffer['id']
    ) {
        return (
            generateServiceOfferScopedStoreKey(storeName, serviceOfferId) +
            getCurrentUserName()
        )
    }

    return {
        generateServiceOfferScopedStoreKey,
        generateUserScopedStoreKey,
        generateUserUrlScopedStoreKey,
        generateUserServiceOfferScopedStoreKey,
    }
}
