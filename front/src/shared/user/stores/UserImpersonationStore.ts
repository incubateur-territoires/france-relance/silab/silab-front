import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useUserImpersonationStore = function () {
    const defineUserImpersonationStore = defineStore(
        'userImpersonation',
        () => {
            const impersonatedEmail = ref<string | undefined>(undefined)

            return {
                impersonatedEmail,
            }
        },
        {
            persist: {
                pick: ['impersonatedEmail'], // Note: cette config est partagée entre tous les users, amelioration: 🤷‍♂️
            },
        }
    )

    return defineUserImpersonationStore()
}
