import { InterventionServiceOffer } from '@/interventionServiceOffer/entities/interventionServiceOffer/InterventionServiceOffer'
import { InterventionServiceOfferUserPreference } from '@/interventionServiceOffer/entities/user/InterventionServiceOfferUserPreference'
import { defineStore } from 'pinia'
import { Ref, ref } from 'vue'
import { useStoreKeys } from '../composables/useStoreKeys'
import { MsalContext } from '@/plugins/msal/useMsal'

export function useUserPreferenceStore(msal: MsalContext) {
    const { generateUserScopedStoreKey } = useStoreKeys(msal)

    return defineStore(
        generateUserScopedStoreKey('userPreference'),
        () => {
            const interventionServiceOfferPreferences: Ref<{
                [
                    interventionServiceOfferId: InterventionServiceOffer['id']
                ]: InterventionServiceOfferUserPreference
            }> = ref({})
            return { interventionServiceOfferPreferences }
        },
        {
            persist: {
                pick: ['interventionServiceOfferPreferences'],
            },
        }
    )()
}
