import { User } from '@/api/user/User'
import { UserApi } from '@/api/user/UserApi'
import { MsalContext } from '@/plugins/msal/useMsal'
import { ServiceOffer } from '@/serviceOffer/entities/serviceOffer/ServiceOffer'
import { getAnalyticsProvider } from '@/shared/analytics/AnalyticsProvider'
import { SilabError } from '@/shared/errors/SilabError'
import { isDefined } from '@/shared/helpers/types'
import { defineStore } from 'pinia'
import { computed, ref, watch } from 'vue'
import { emitSnackBar } from '@/plugins/snackBarManager'
import { useRouter } from 'vue-router'
import { useUserImpersonationStore } from './UserImpersonationStore'

export const useUserStore = function (msal: MsalContext) {
    const defineUserStore = defineStore(
        'user',
        () => {
            const AuthenticatedUser = ref<User>(User.ANONYMOUS_USER)
            const impersonatedUser = ref<User | undefined>(undefined)
            const userImpersonationStore = useUserImpersonationStore()
            const isImpersonating = computed(() =>
                isDefined(userImpersonationStore.impersonatedEmail)
            )
            const router = useRouter()
            const userApi = new UserApi(msal)
            watch(
                () => msal.accounts,
                () => {
                    userApi.getCurrent(true).then((fetchedUser) => {
                        AuthenticatedUser.value = fetchedUser
                        getAnalyticsProvider().setUserId(
                            AuthenticatedUser.value.email
                        )
                    })
                },
                { immediate: true }
            )

            watch(
                [
                    () => msal.accounts,
                    () => userImpersonationStore.impersonatedEmail,
                ],
                () => {
                    userApi.getCurrent().then((fetchedImpersonatedUser) => {
                        impersonatedUser.value = fetchedImpersonatedUser
                    })
                },
                { immediate: true }
            )

            const currentUser = computed<User>(() => {
                if (isDefined(impersonatedUser.value)) {
                    return impersonatedUser.value
                }
                return AuthenticatedUser.value
            })

            function currentUserHasRoleForServiceOffer(
                role: string,
                serviceOfferId: ServiceOffer['id']
            ) {
                return userHasRoleForServiceOffer(
                    currentUser.value,
                    role,
                    serviceOfferId
                )
            }

            function userHasRoleForServiceOffer(
                user: User,
                role: string,
                serviceOfferId: ServiceOffer['id']
            ) {
                return userHasRole(
                    user,
                    `SERVICEOFFER_${serviceOfferId}_ROLE_${role}`
                )
            }

            function userHasRole(user: User, role: string) {
                return user.reachableRoles.includes(role)
            }

            async function impersonateUser(userEmail: string) {
                if (
                    !userHasRole(
                        AuthenticatedUser.value,
                        'ROLE_ALLOWED_TO_SWITCH'
                    )
                ) {
                    throw new SilabError(
                        "Vous n'avez pas le droit d'usurper l'identité d'un autre utilisateur"
                    )
                }

                await router.push({ name: 'home' }) // on redirige en preimier lieu pour éviter des erreurs 403
                userImpersonationStore.impersonatedEmail = userEmail

                emitSnackBar(`Usurpation de ${userEmail} activée`, 'success')
            }

            function stopImpersonating() {
                userImpersonationStore.impersonatedEmail = undefined
                impersonatedUser.value = undefined
            }

            return {
                currentUser,
                impersonatedEmail: userImpersonationStore.impersonatedEmail,
                isImpersonating,
                currentUserHasRoleForServiceOffer,
                userHasRoleForServiceOffer,
                userHasRole,
                impersonateUser,
                stopImpersonating,
            }
        },
        {
            persist: {
                pick: ['impersonatedEmail'], // Note: cette config est partagée entre tous les users, amelioration: 🤷‍♂️
            },
        }
    )

    return defineUserStore()
}
