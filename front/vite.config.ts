import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import fs from 'fs'
import istanbul from 'vite-plugin-istanbul'
// https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vite-plugin
import vuetify from 'vite-plugin-vuetify'
import legacy from '@vitejs/plugin-legacy'

const httpsOptions = {
    key: fs.readFileSync('cert.key'),
    cert: fs.readFileSync('cert.crt'),
}

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        vuetify({
            autoImport: true,
            styles: { configFile: 'src/styles/settings.scss' },
        }),
        istanbul({
            // amélioration: change plugin version in package.json once issue 96 is resolved see PR 180
            include: 'src/*',
            exclude: ['node_modules', 'test/'],
            cypress: true,
            requireEnv: true,
            forceBuildInstrument: true,
        }),
        legacy({
            targets: ['defaults', 'not IE 11'],
        }),
    ],
    server: {
        host: '0.0.0.0',
        port: 8443,
        https: httpsOptions,
        watch: {
            usePolling: true,
        },
    },
    preview: {
        port: 8444,
        https: httpsOptions,
    },
    resolve: {
        alias: {
            '@': resolve(__dirname, 'src'),
        },
    },
    build: {
        watch: Boolean(process.env.BUILD_WATCH)
            ? {
                  chokidar: {
                      usePolling: true,
                  },
              }
            : null,
        rollupOptions: {
            treeshake: 'safest',
            output: {
                manualChunks(id: string) {
                    // after analyzing the bundle with `npm run visualize:deps:treemap`, separate the biggest packages
                    // and those that should be ignored for production build
                    // for now the 3 biggest packages are faker, azure and vuetify.
                    // what we want is to only pull these in the browser if they are needed

                    // creating a chunk to @faker-js as it takes more than half of the vendor chunk
                    if (id.includes('@faker-js')) {
                        return '@faker'
                    }

                    // the mock-api should not be included in the main bundle to lighten the prodution index file
                    if (id.includes('@/mock-api')) {
                        return '@mirage'
                    }

                    // creating a chunk to @azure deps. Reducing the vendor chunk size
                    if (id.includes('@azure')) {
                        return '@azure'
                    }
                    // creating a chunk to vuetify deps. Reducing the vendor chunk size
                    if (id.includes('vuetify')) {
                        return '@vuetify'
                    }
                },
            },
        },
        sourcemap: true,
    },
})
